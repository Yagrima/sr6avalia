/**
 *
 */
package org.prelle.rpgframework.shadowrun6.data;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.character.RulePluginFeatures;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.CustomDataHandler;
import de.rpgframework.core.CustomDataHandler.CustomDataPackage;
import de.rpgframework.core.CustomDataHandlerLoader;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class Shadowrun6DataPlugin implements RulePlugin<ShadowrunCharacter> {

	private final static Logger logger = LogManager.getLogger("shadowrun6.data");

	//-------------------------------------------------------------------
	public Shadowrun6DataPlugin() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getID()
	 */
	@Override
	public String getID() {
		return "Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#getReadableName()
	 */
	@Override
	public String getReadableName() {
		if (this.getClass().getPackage().getImplementationTitle()!=null)
			return this.getClass().getPackage().getImplementationTitle();
		return "Shadowrun 6 Data";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return RoleplayingSystem.SHADOWRUN6;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getRequiredPlugins()
	 */
	@Override
	public Collection<String> getRequiredPlugins() {
		return Arrays.asList("CORE");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getSupportedFeatures()
	 */
	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return Arrays.asList(new RulePluginFeatures[]{RulePluginFeatures.DATA});
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getConfiguration()
	 */
	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#willProcessCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public boolean willProcessCommand(Object src, CommandType type, Object... values) {
			return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.CommandBusListener#handleCommand(java.lang.Object, de.rpgframework.core.CommandType, java.lang.Object[])
	 */
	@Override
	public CommandResult handleCommand(Object src, CommandType type, Object... values) {
		return new CommandResult(type, false);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#init()
	 */
	@Override
	public void init(RulePluginProgessListener callback) {
		Class<Shadowrun6DataPlugin> clazz = Shadowrun6DataPlugin.class;

		double totalPlugins = 4.0;
		double count = 0;
		logger.info("START -------------------------------Core-----------------------------------------------");
		PluginSkeleton CORE = new PluginSkeleton("CORE", "Shadowrun 6 Core Rules");
		ShadowrunCore.loadSkills        (CORE, clazz.getResourceAsStream("core/data/skills.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpellFeatures (CORE, clazz.getResourceAsStream("core/data/spellfeatures.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpells        (CORE, clazz.getResourceAsStream("core/data/spells.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadRitualFeatures(CORE, clazz.getResourceAsStream("core/data/ritualfeatures.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadRituals       (CORE, clazz.getResourceAsStream("core/data/rituals.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_auto.xml"), CORE.getResources(), CORE.getHelpResources());// Unarmed weapons
		ShadowrunCore.loadQualities     (CORE, clazz.getResourceAsStream("core/data/qualities.xml"), CORE.getResources(), CORE.getHelpResources()); 
		ShadowrunCore.loadMagicOrResonanceTypes(CORE, clazz.getResourceAsStream("core/data/magicOrResonance.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadAmmoTypes     (CORE, clazz.getResourceAsStream("core/data/ammunition_types.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_melee.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_firearms_accessories.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_firearms.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_ammunition.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_armor.xml"), CORE.getResources(), CORE.getHelpResources());// Dermal Deposits need it
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_armor_accessories.xml"), CORE.getResources(), CORE.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_electronics.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_sensors_and_co.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_security_survival.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_cyberware.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_cyberware_pass2.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_bioware.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_magical.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_vehicles.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_drones.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_drones_accessories.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadEquipment     (CORE, clazz.getResourceAsStream("core/data/gear_software.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetaTypes     (CORE, clazz.getResourceAsStream("core/data/metatypes.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadPriorityTableEntries(CORE, clazz.getResourceAsStream("core/data/priorities.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadAdeptPowers(  CORE, clazz.getResourceAsStream("core/data/adeptpowers.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadComplexForms  (CORE, clazz.getResourceAsStream("core/data/complexforms.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadLifestyles    (CORE, clazz.getResourceAsStream("core/data/lifestyles.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSpirits       (CORE, clazz.getResourceAsStream("core/data/spirits.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSprites       (CORE, clazz.getResourceAsStream("core/data/sprites.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadTraditions    (CORE, clazz.getResourceAsStream("core/data/traditions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMentorSpirits (CORE, clazz.getResourceAsStream("core/data/mentorspirits.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadSensorFunctions(CORE, clazz.getResourceAsStream("core/data/sensorfunctions.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetamagicOrEchos(CORE, clazz.getResourceAsStream("core/data/metamagics.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadMetamagicOrEchos(CORE, clazz.getResourceAsStream("core/data/echoes.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadLicenseTypes  (CORE, clazz.getResourceAsStream("core/data/licensetypes.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadFoci          (CORE, clazz.getResourceAsStream("core/data/foci.xml"), CORE.getResources(), CORE.getHelpResources());
		
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_major.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_minor.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_edge.xml"), CORE.getResources(), CORE.getHelpResources());
		ShadowrunCore.loadActions(CORE, clazz.getResourceAsStream("core/data/actions_matrix.xml"), CORE.getResources(), CORE.getHelpResources());
		count++; callback.progressChanged( (count/totalPlugins) );
		BasePluginData.flushMissingKeys();

		logger.info("START -------------------------------Schattenloads-----------------------------------------");
		PluginSkeleton SLOAD = new PluginSkeleton("SCHATTENLOAD", "Schattenload");
		SLOAD.setLanguages(Locale.GERMAN);
		if (!SLOAD.getLanguages().contains(Locale.getDefault().getLanguage()) &&  ShadowrunCore.DATA_IGNORE_PLUGIN_LANGUAGE!=null && !ShadowrunCore.DATA_IGNORE_PLUGIN_LANGUAGE.getValue()) {
			logger.warn("Don't load plugin="+SLOAD.getID()+" because language does not match (allowed="+SLOAD.getLanguages()+")");
		} else {
			ShadowrunCore.loadEquipment     (SLOAD, clazz.getResourceAsStream("schattenload/data/gear_vehicles.xml"), SLOAD.getResources(), SLOAD.getHelpResources());
		}
		count++; callback.progressChanged( (count/totalPlugins) );

		logger.info("START -------------------------------Berlin 2080-----------------------------------------");
		PluginSkeleton BERLIN = new PluginSkeleton("BERLIN2080", "Berlin");
		BERLIN.setLanguages(Locale.GERMAN);
		if (!BERLIN.getLanguages().contains(Locale.getDefault().getLanguage()) &&  ShadowrunCore.DATA_IGNORE_PLUGIN_LANGUAGE!=null && !ShadowrunCore.DATA_IGNORE_PLUGIN_LANGUAGE.getValue()) {
			logger.warn("Don't load plugin="+BERLIN.getID()+" because language does not match (allowed="+BERLIN.getLanguages()+")");
		} else {
			ShadowrunCore.loadEquipment     (BERLIN, clazz.getResourceAsStream("berlin2080/data/gear_firearms.xml"), BERLIN.getResources(), BERLIN.getHelpResources());
			ShadowrunCore.loadEquipment     (BERLIN, clazz.getResourceAsStream("berlin2080/data/gear_vehicles.xml"), BERLIN.getResources(), BERLIN.getHelpResources());
		}
		count++; callback.progressChanged( (count/totalPlugins) );

		/*
		 * Load custom data
		 */
		logger.info("START -------------------------------Custom------------------------------------------");
		if (CustomDataHandlerLoader.getInstance()!=null) {
			CustomDataHandler custom = CustomDataHandlerLoader.getInstance();
			List<String> customIDs = custom.getAvailableCustomIDs(RoleplayingSystem.SHADOWRUN6);
			PluginSkeleton CUSTOM = new PluginSkeleton("Custom", "Custom Data");
			for (String id : customIDs) {
				CustomDataPackage  bundle = custom.getCustomData(RoleplayingSystem.SHADOWRUN6, id);
				if (bundle!=null) {
					if (bundle.properties==null) {
						logger.fatal("\n\nNo properties file for custom data\n\n");
						continue;
					}
					
					try (InputStream datastream = new FileInputStream(bundle.datafile.toFile())) {
						if (id.startsWith("item") || id.startsWith("equipment") || id.startsWith("gear")) {
							ShadowrunCore.loadEquipment(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("skills")) {
							ShadowrunCore.loadSkills(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("qualities")) {
							ShadowrunCore.loadQualities(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("spell")) {
							ShadowrunCore.loadSpells(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("complex")) {
							ShadowrunCore.loadComplexForms(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("power")) {
							ShadowrunCore.loadAdeptPowers(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("tradition")) {
							ShadowrunCore.loadTraditions(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else if (id.startsWith("mentor")) {
							ShadowrunCore.loadMentorSpirits(CUSTOM, datastream, bundle.properties, bundle.helpProperties);
						} else  {
							logger.warn("Don't know how to deal with custom data "+bundle.datafile);
							continue;
						}
						logger.info("Loaded custom data  "+bundle.datafile);
					} catch (IOException e) {
						logger.error("Failed for custom data "+bundle.datafile+" and its properties",e);
					}
				}
			}
		}

		BasePluginData.flushMissingKeys();
		callback.progressChanged( 1.0f );

//		logger.fatal("Stop here");
//		System.exit(0);
		logger.debug("STOP : -----Init "+getID()+"---------------------------");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return Shadowrun6DataPlugin.class.getResourceAsStream("data-plugin.html");
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getLanguages()
	 */
	@Override
	public List<String> getLanguages() {
		return Arrays.asList(Locale.GERMAN.getLanguage(), Locale.ENGLISH.getLanguage());
	}

}

/**
 *
 */
package org.prelle.rpgframework.shadowrun6.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.ItemTemplate;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class LoadDataTest {

	//-------------------------------------------------------------------
	@Test
	public void loadDataTest() {
		System.setProperty("logdir", System.getProperty("user.home"));
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		
		ItemTemplate ares = ShadowrunCore.getItem("ares_predator_vi");
		assertEquals(3,ares.getSlots().size());
	}

}

/**
 *
 */
package org.prelle.rpgframework.shadowrun6.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.actions.ShadowrunAction.Type;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class PrintCombatActions2 {

	private final static String NORMAL  = "%26s   %26s";
	
	//-------------------------------------------------------------------
	@Test
	public void loadDataTest() {
		System.setProperty("logdir", System.getProperty("user.home"));
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		
		System.out.println(String.format(NORMAL, "MINOR ACTIONS", "MAJOR ACTIONS"));
		
		List<ShadowrunAction> minor = new ArrayList<>();
		List<ShadowrunAction> major = new ArrayList<>();

		ShadowrunAction headEMPTY = new ShadowrunAction("EMPTY");
		ShadowrunAction headINI = new ShadowrunAction("INI");
		ShadowrunAction headANY = new ShadowrunAction("ANY");
		
		List<ShadowrunAction> all = ShadowrunCore.getActions(ShadowrunAction.Category.COMBAT, ShadowrunAction.Category.POSITION);
		// Sort: INITIATIVE before ANYTIME
		Collections.sort(all, new Comparator<ShadowrunAction>() {
			public int compare(ShadowrunAction o1, ShadowrunAction o2) {
				if (o1.getTime()==null && o2.getTime()!=null) return 1;
				if (o1.getTime()!=null && o2.getTime()==null) return -1;
				if (o1.getTime()==null && o2.getTime()==null) return 0;
				return o1.getTime().compareTo(o2.getTime());
			}
		});
		
		int posLeftANY = -1;
		int posRightANY = -1;
		for (ShadowrunAction act : all) {
			if (act.getType()!=Type.BOOST && act.getType()!=Type.EDGE) {
				if (act.getType()==Type.MINOR) {
					ShadowrunAction last = (minor.isEmpty())?null:minor.get(minor.size()-1);
					if (last==null) {
						minor.add(headINI);
					} else if (last.getTime()!=act.getTime()) {
						posLeftANY = minor.size();
						minor.add(headEMPTY);
						minor.add(headANY);
					}
					minor.add(act);
				}
				if (act.getType()==Type.MAJOR) {
					ShadowrunAction last = (major.isEmpty())?null:major.get(major.size()-1);
					if (last==null) {
						major.add(headINI);
					} else if (last.getTime()!=act.getTime()) {
						posRightANY = major.size();
						major.add(headEMPTY);
						major.add(headANY);
					}
					major.add(act);
				}
			}
		}		
		
		int max = Math.max(minor.size(), major.size());
		int idxLeft = 0;
		int idxRight = 0;
		for (int i=0; i<max; i++) {
			ShadowrunAction leftA  = (minor.size()>idxLeft)?minor.get(idxLeft):null;
			ShadowrunAction rightA = (major.size()>idxRight)?major.get(idxRight):null;
//			System.out.println(idxLeft+"/"+posLeftANY+"/"+leftA+"   vs  "+idxRight+"/"+posRightANY+"/"+rightA);
			String left = "";
			if (leftA==headINI)
				left="---In Initiative phase--";
			else if (leftA==headANY)
				left="---------------Anytime--";
			else if (leftA==headEMPTY) {
				left="";
				if (i<posRightANY)
					idxLeft--;
			} else if (leftA!=null) 
				left = leftA.getName()+" ("+leftA.getProductNameShort()+" "+leftA.getPage()+")";
			idxLeft++;
			
			String right = "";
			if (rightA==headINI)
				right="---In Initiative phase--";
			else if (rightA==headANY)
				right="---------------Anytime--";
			else if (rightA==headEMPTY) {
				right="";
				if (i<posLeftANY)
					idxRight--;
			} else if (rightA!=null) {
				right = rightA.getName()+" ("+rightA.getProductNameShort()+" "+rightA.getPage()+")";
			}
			idxRight++;
			
			System.out.println(String.format(NORMAL, left, right));
		}
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.chargen.FreePointsModification;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.chargen.PriorityAttributeGenerator;
import org.prelle.shadowrun6.chargen.PrioritySkillController;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SkillGenCostTest {

	private PrioritySkillController skills;
	private ShadowrunCharacter model;
	private List<Modification> mods;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		mods = new ArrayList<>();
		NewPriorityCharacterGenerator charGen = new NewPriorityCharacterGenerator();
		charGen.start(new ShadowrunCharacter());
		skills = new PrioritySkillController(charGen);
	}

	//-------------------------------------------------------------------
	@Test
	public void testRegularPointsOnly() {
		mods.add(new FreePointsModification(Type.SKILLS, 12));
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("language"), SkillValue.LANGLEVEL_NATIVE));
		model.setKarmaFree(0);
		model.getAttribute(Attribute.LOGIC   ).setPoints(1);
		assertNotNull("Missing skill", ShadowrunCore.getSkill("firearms"));
		assertNotNull("Missing model", model);
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("firearms"), 4));
		model.addSkill(new SkillValue(ShadowrunCore.getSkill(     "con"), 3));
		skills.process(model, mods);
		assertEquals(1, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(5, skills.getPointsLeftSkills());
		assertEquals(2, skills.getToDos().size());
		
		// Spend remaining points
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("close_combat"), 5));
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("language"), 1)); // Spend minimum knowledge
		model.setKarmaFree(0);
		skills.process(model, mods);
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(0, model.getKarmaFree());
		System.out.println(skills.getToDos());
		assertEquals(0, skills.getToDos().size());
		
		// Spend more points than possible
		model.setKarmaFree(0);
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("electronics"), 2));
		skills.process(model, mods);
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(-15, model.getKarmaFree());
	}

	//-------------------------------------------------------------------
	@Test
	public void testRegularKarmaOnly() {
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("language"), SkillValue.LANGLEVEL_NATIVE));
		model.setKarmaFree(100);
		model.getAttribute(Attribute.LOGIC   ).setPoints(1);
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("firearms"), 4)); // 50 karma
		model.addSkill(new SkillValue(ShadowrunCore.getSkill(     "con"), 3)); // 30 karma
		skills.process(model, mods);
		assertEquals(1, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(20, model.getKarmaFree());
		assertEquals(1, skills.getToDos().size()); // Knowledge points
		
		// Spend remaining points
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("close_combat"), 5)); // 75 karma
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("language"), 1)); // Spend minimum knowledge
		model.setKarmaFree(100);
		skills.process(model, mods);
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(-55, model.getKarmaFree());
		assertEquals(0, skills.getToDos().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testRegularPointsAndKarma() {
		mods.add(new FreePointsModification(Type.SKILLS, 8));
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("language"), SkillValue.LANGLEVEL_NATIVE));
		model.setKarmaFree(100);
		model.getAttribute(Attribute.LOGIC   ).setPoints(1);
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("firearms"), 4));
		model.addSkill(new SkillValue(ShadowrunCore.getSkill(     "con"), 3));
		skills.process(model, mods);
		assertEquals(1, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(1, skills.getPointsLeftSkills());
		assertEquals(100, model.getKarmaFree());
		assertEquals(2, skills.getToDos().size()); // Skill points, Knowledge points
		
		// Spend remaining points
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("close_combat"), 5)); // 70 karma
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("language"), 1)); // Spend minimum knowledge
		model.setKarmaFree(100);
		// Pay with points: 5=close_combat, 3 of 4 for firearms
		// Pay with karma: firearms=4 (20), con=3 =30
		skills.process(model, mods);
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(50, model.getKarmaFree());
		System.out.println(skills.getToDos());
		assertEquals(0, skills.getToDos().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testNormalSpecializationsPointsOnly() {
		mods.add(new FreePointsModification(Type.SKILLS, 6));
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("language"), SkillValue.LANGLEVEL_NATIVE));
		model.setKarmaFree(30);
		SkillValue firearms = new SkillValue(ShadowrunCore.getSkill("firearms"), 4);
		firearms.addSpecialization(new SkillSpecializationValue(ShadowrunCore.getSkill("firearms").getSpecialization("pistols")));
		model.addSkill(firearms);
		skills.process(model, mods);
		assertEquals(1, skills.getPointsLeftSkills());
		assertEquals(1, skills.getToDos().size()); // 1 skill point left
	}

	//-------------------------------------------------------------------
	/**
	 * From 8 points for skills the 5 and 3 skill values should be paid,
	 * while the specialization should be payed with karma
	 */
	@Test
	public void testPreferSkillsBeforeSpecializations() {
		mods.add(new FreePointsModification(Type.SKILLS, 8));
		model.setKarmaFree(10);
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("con"), 3));
		SkillValue firearms = new SkillValue(ShadowrunCore.getSkill("firearms"), 5);
		firearms.addSpecialization(new SkillSpecializationValue(ShadowrunCore.getSkill("firearms").getSpecialization("pistols")));
		model.addSkill(firearms);
		skills.process(model, mods);
		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(5, model.getKarmaFree());
		assertEquals(0, skills.getToDos().size()); 
	}
	
}

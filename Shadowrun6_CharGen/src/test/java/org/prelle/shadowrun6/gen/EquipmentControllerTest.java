package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.CommonEquipmentController;
import org.prelle.shadowrun6.items.BodytechQuality;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.modifications.AttributeModification;

import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class EquipmentControllerTest {

	private CommonEquipmentController control;
	private ShadowrunCharacter model;
	private List<Modification> mods;
	private int baseKarma;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		model.setNuyen(999999);
		mods = new ArrayList<>();
		ProcessorRunner charGen = new ProcessorRunner() {
			public void runProcessors() {
				model.setKarmaFree(baseKarma);
			}
		};
		model.getAttribute(Attribute.MAGIC).clearModifications();
		control = new CommonEquipmentController(charGen, model, CharGenMode.CREATING) {
			
			@Override
			public List<ConfigOption<?>> getConfigOptions() {
				// TODO Auto-generated method stub
				return null;
			}
			
			@Override
			public boolean increaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public int getBoughtNuyen() {
				// TODO Auto-generated method stub
				return 0;
			}
			
			@Override
			public boolean decreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean canIncreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public boolean canDecreaseBoughtNuyen() {
				// TODO Auto-generated method stub
				return false;
			}
		};
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() {
		assertEquals(0, model.getItems(false).size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleSelection() {
		CarriedItem item = control.select(ShadowrunCore.getItem("tag_eraser"));
		assertNotNull(item); 
		assertEquals(1, model.getItems(false).size());
		CarriedItem verify = model.getItems(false).get(0);
		assertSame(item, verify);
		
		// Check some data of instantiated item
		assertEquals(ShadowrunCore.getItem("tag_eraser").getPrice(), item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(0, item.getSlots().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleSelection2() {
		ItemTemplate template = ShadowrunCore.getItem("flare_compensation");
		CarriedItem item = control.select(template);
		assertNotNull(item); 
		assertEquals(1, model.getItems(false).size());
		CarriedItem verify = model.getItems(false).get(0);
		assertSame(item, verify);
		
		// Check some data of instantiated item
		assertEquals(template.getPrice(), item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(0, item.getSlots().size());
		assertEquals(100, item.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleSelectionWithQuality() {
		ItemTemplate template = ShadowrunCore.getItem("flare_compensation");
		CarriedItem item = control.select(template, new SelectionOption(SelectionOptionType.BODYTECH_QUALITY, BodytechQuality.BETA));
		assertNotNull(item); 
		assertEquals(1, model.getItems(false).size());
		CarriedItem verify = model.getItems(false).get(0);
		assertSame(item, verify);
		
		// Check some data of instantiated item
		assertEquals(template.getPrice()*1.5, item.getAsValue(ItemAttribute.PRICE).getModifiedValue(), 0);
		assertEquals(0, item.getSlots().size());
		assertEquals(70, item.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleSelectionWithAccessorySlots() {
		ItemTemplate template = ShadowrunCore.getItem("imaging_scope_single");
		CarriedItem item = control.select(template);
		assertNotNull(item); 
		assertEquals(1, model.getItems(false).size());
		CarriedItem verify = model.getItems(false).get(0);
		assertSame(item, verify);
		
		// Check some data of instantiated item
		assertEquals(template.getPrice(), item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(1, item.getSlots().size());
		assertEquals(ItemHook.OPTICAL, item.getSlots().iterator().next().getSlot());
		assertEquals(3, item.getSlots().iterator().next().getCapacity());
		assertEquals(3, item.getSlots().iterator().next().getFreeCapacity());
		assertEquals(0, item.getSlots().iterator().next().getUsedCapacity());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleSelectionWithAccessorySlotsAndRating() {
		ItemTemplate template = ShadowrunCore.getItem("goggles");
		int choosenRating = 4;
		// This should fail, because of missing rating
		try {
			control.select(template);
			fail("Missing rating not detected");
		} catch (IllegalArgumentException e) {
		}
		CarriedItem item = control.select(template, new SelectionOption(SelectionOptionType.RATING, choosenRating));
		assertNotNull(item); 
		assertEquals(1, model.getItems(false).size());
		CarriedItem verify = model.getItems(false).get(0);
		assertSame(item, verify);
		
		// Check some data of instantiated item
		assertEquals(template.getPrice()*choosenRating, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(template.getAvailability(), item.getAsObject(ItemAttribute.AVAILABILITY).getValue());
		assertEquals(1, item.getSlots().size());
		assertEquals(ItemHook.OPTICAL, item.getSlots().iterator().next().getSlot());
		assertEquals(choosenRating, item.getSlots().iterator().next().getCapacity());
		assertEquals(choosenRating, item.getSlots().iterator().next().getFreeCapacity());
		assertEquals(0, item.getSlots().iterator().next().getUsedCapacity());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleSelectionWithCharacterModifications() {
		ItemTemplate template = ShadowrunCore.getItem("adrenaline_pump");
		int choosenRating = 2;
		// This should fail, because of missing rating
		try {
			control.select(template);
			fail("Missing rating not detected");
		} catch (IllegalArgumentException e) {
		}
		// This should fail, because of rating too high
		try {
			control.select(template, new SelectionOption(SelectionOptionType.RATING, 9));
			fail("Invalid rating not detected");
		} catch (IllegalArgumentException e) {
		}
		CarriedItem item = control.select(template, new SelectionOption(SelectionOptionType.RATING, choosenRating));
		assertNotNull(item); 
		assertEquals(1, model.getItems(false).size());
		CarriedItem verify = model.getItems(false).get(0);
		assertSame(item, verify);
		
		// Check some data of instantiated item
		assertEquals(template.getPrice()*choosenRating, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		List<Modification> mods = item.getModifications();
		assertEquals(4, mods.size());
		assertTrue(mods.get(0) instanceof AttributeModification);
		assertEquals(choosenRating, ((AttributeModification)mods.get(0)).getValue() );
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponAccessory() {
		ItemTemplate template = ShadowrunCore.getItem("defiance_super_shock");
		CarriedItem item = control.select(template);
		assertNotNull(item); 
		assertEquals(1, model.getItems(false).size());
		CarriedItem verify = model.getItems(false).get(0);
		assertEquals(template.getPrice(), item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertSame(item, verify);
		assertEquals(0, item.getUserAddedAccessories().size());
		
		ItemTemplate accessTemp = ShadowrunCore.getItem("imaging_scope");
		CarriedItem access = control.embed(item, accessTemp);
		assertNotNull(access); 
		assertEquals(1, model.getItems(false).size());
		assertEquals(1, item.getUserAddedAccessories().size());
		assertEquals(ItemHook.TOP, access.getSlot());
		assertNotNull(item.getSlot(ItemHook.TOP));
		assertEquals(1, item.getSlot(ItemHook.TOP).getAllEmbeddedItems().size());
		
		assertEquals(template.getPrice()+accessTemp.getPrice(), item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
	}

	//-------------------------------------------------------------------
	/**
	 * An accessory with accessories
	 */
	@Test
	public void testWeaponAccessoryRecursive() {
		ItemTemplate template = ShadowrunCore.getItem("defiance_super_shock");
		CarriedItem item = control.select(template);
		assertNotNull(item);
		assertEquals(340, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		ItemTemplate accessTemp = ShadowrunCore.getItem("imaging_scope");
		CarriedItem access = control.embed(item, accessTemp);
		assertNotNull(access);
		assertEquals(ItemHook.TOP, access.getSlot());
		assertEquals(1, item.getUserAddedAccessories().size());
		assertEquals(1, item.getEffectiveAccessories().size());
		assertTrue(access.hasHook(ItemHook.OPTICAL));
		assertEquals(3, access.getSlot(ItemHook.OPTICAL).getCapacity());
		assertEquals(3, access.getSlot(ItemHook.OPTICAL).getFreeCapacity());
		assertEquals(0, access.getSlot(ItemHook.OPTICAL).getUsedCapacity());
		assertEquals(690, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		ItemTemplate optAccTemp = ShadowrunCore.getItem("low_light_vision");
		CarriedItem optAccess = control.embed(access, optAccTemp);
		assertNotNull(optAccess); 
		assertEquals(1, optAccess.getAsValue(ItemAttribute.CAPACITY).getModifiedValue());
		assertEquals(ItemHook.OPTICAL, optAccess.getSlot());

		assertEquals(1, model.getItems(false).size());
		assertEquals(1, access.getEffectiveAccessories().size());
		assertEquals(1, item.getUserAddedAccessories().size());
		assertEquals(ItemHook.OPTICAL, optAccess.getSlot());
		assertEquals(3, access.getSlot(ItemHook.OPTICAL).getCapacity());
		assertEquals(2, access.getSlot(ItemHook.OPTICAL).getFreeCapacity());
		assertEquals(1, access.getSlot(ItemHook.OPTICAL).getUsedCapacity());
		assertEquals(1190, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void testAvailableAccessories() {
		ItemTemplate ares = ShadowrunCore.getItem("ares_predator_vi");
		List<String> names = ShadowrunCore.getAccessoryItems(ItemHook.BARREL, ares).stream().map(temp -> temp.getId()).collect(Collectors.toList());
		assertTrue(names.contains("gas-vent_system"));
		assertTrue(names.contains("silencer"));
		assertFalse(names.contains("bipod")); // other mount
		assertFalse(names.contains("spare_clip")); // no mount / external
		assertFalse(names.contains("airbust_link")); // Requirement Launcher
		
		names = ShadowrunCore.getAccessoryItems(ItemHook.TOP, ares).stream().map(temp -> temp.getId()).collect(Collectors.toList());
		System.out.println("Allow "+names);
		assertTrue(names.contains("imaging_scope")); // Requirement pistols
		assertTrue(names.contains("laser_sight")); // Requirement pistols
		assertTrue(names.contains("periscope")); // Requirement pistols
		assertTrue(names.contains("smartgun_system")); // Requirement pistols
		
		names = ShadowrunCore.getAccessoryItems(ItemHook.FIREARMS_EXTERNAL, ares).stream().map(temp -> temp.getId()).collect(Collectors.toList());
		System.out.println("Allow "+names);
		assertTrue(names.contains("concealable_holster")); // Requirement pistols
		assertTrue(names.contains("hidden_arm_slide")); // Requirement pistols
		assertTrue(names.contains("quick_draw_holster")); // Requirement pistols
		assertTrue(names.contains("spare_clip")); 
		assertTrue(names.contains("speed_loader"));
		assertFalse(names.contains("shock_pad")); // Requirement Rifles
		
		CarriedItem instance = new CarriedItem(ares);
		// Check available slots
		names = instance.getSlots().stream().map(temp -> temp.getSlot().name()).collect(Collectors.toList());
		assertTrue(names.contains("TOP"));
		assertTrue(names.contains("BARREL"));
		assertFalse(names.contains("UNDER"));
		assertTrue(names.contains("INTERNAL"));
		assertTrue(names.contains("FIREARMS_EXTERNAL"));
		assertEquals(1,names.stream().filter(name -> name.equals("FIREARMS_EXTERNAL")).collect(Collectors.counting()).intValue());
		System.out.println("Slots "+names);
		
		names = control.getAvailableAccessoriesFor(instance).stream().map(temp -> temp.getId()).collect(Collectors.toList());
		System.out.println("all "+names);
		assertTrue(names.contains("gas-vent_system"));
		assertTrue(names.contains("silencer"));
		System.out.println("Allow "+names);
		assertTrue(names.contains("imaging_scope"));
		assertTrue(names.contains("laser_sight"));
		assertTrue(names.contains("periscope")); 
		assertTrue(names.contains("smartgun_system")); // Already embedded
		assertTrue(names.contains("concealable_holster"));
		assertTrue(names.contains("hidden_arm_slide")); 
		assertTrue(names.contains("quick_draw_holster"));
		assertTrue(names.contains("spare_clip")); 
		assertTrue(names.contains("speed_loader"));
	}
	
}

/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.chargen.FreePointsModification;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.chargen.PriorityAttributeGenerator;
import org.prelle.shadowrun6.chargen.PrioritySettings;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.AttributeModification.ModificationType;
import org.prelle.shadowrun6.proc.ApplyAttributeModifications;
import org.prelle.shadowrun6.proc.ResetModifications;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PrioAttribGenTest {

	private PriorityAttributeGenerator attrib;
	private ShadowrunCharacter model;
	private List<Modification> mods;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		model.setKarmaFree(50);
		model.setTemporaryChargenSettings(new PrioritySettings());
		GenerationEventDispatcher.clear();
		mods = new ArrayList<>();
		attrib = new PriorityAttributeGenerator(model);
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("mundane"));
	}
	
	//-------------------------------------------------------------------
	private void setMagicTo(int value) {
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("adept"));
		AttributeModification mod = new AttributeModification(Attribute.MAGIC, value);
		mod.setType(ModificationValueType.NATURAL);
		mod.setSource(ShadowrunCore.getMagicOrResonanceType("adept"));
		model.getAttribute(Attribute.MAGIC).addModification(mod);		
	}

	//-------------------------------------------------------------------
	@Test
	public void test1() {
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 2));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 12));
		attrib.process(model, mods);
		assertEquals(50, model.getKarmaFree());
		model.getAttribute(Attribute.BODY    ).setPoints(2); // 10 Karma
		model.getAttribute(Attribute.AGILITY ).setPoints(3); // 2
		model.getAttribute(Attribute.REACTION).setPoints(2); // 10 Karma
		model.getAttribute(Attribute.STRENGTH).setPoints(1); // 0
		model.getAttribute(Attribute.WILLPOWER).setPoints(5);// 4
		model.getAttribute(Attribute.LOGIC   ).setPoints(2); // 10 Karma
		model.getAttribute(Attribute.INTUITION).setPoints(3);// 2
		model.getAttribute(Attribute.CHARISMA).setPoints(5); // 4
		mods.clear();
		attrib.process(model, mods);
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(2, attrib.getPointsLeftSpecial());
		assertEquals(20, model.getKarmaFree());
		assertEquals("Warnings not correct", 1, attrib.getToDos().size());
		
		assertEquals(1, attrib.getPerAttribute(Attribute.BODY    ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.AGILITY ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.STRENGTH).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY    ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).adjust);
		assertEquals(10, attrib.getPerAttribute(Attribute.BODY    ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).karma);
		assertEquals(2, model.getAttribute(Attribute.BODY    ).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.AGILITY ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(0, model.getAttribute(Attribute.MAGIC   ).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void test2() {
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 2));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 10));
		model.getAttribute(Attribute.BODY    ).setPoints(2); // 10 Karma
		model.getAttribute(Attribute.AGILITY ).setPoints(3); 
		model.getAttribute(Attribute.REACTION).setPoints(2); // 10 Karma
		model.getAttribute(Attribute.STRENGTH).setPoints(1); // 0
		model.getAttribute(Attribute.WILLPOWER).setPoints(5);
		model.getAttribute(Attribute.LOGIC   ).setPoints(2); // 10 Karma
		model.getAttribute(Attribute.INTUITION).setPoints(3);// 25 Karma
		model.getAttribute(Attribute.CHARISMA).setPoints(5);
		attrib.process(model, mods);
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(2, attrib.getPointsLeftSpecial());
		assertEquals(-5, model.getKarmaFree());
		assertEquals(1, attrib.getToDos().size());

		assertEquals(1, attrib.getPerAttribute(Attribute.BODY    ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.AGILITY ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.STRENGTH).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY    ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).adjust);
		assertEquals(10, attrib.getPerAttribute(Attribute.BODY    ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY ).karma);
		assertEquals(25, attrib.getPerAttribute(Attribute.INTUITION ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).karma);
		assertEquals(2, model.getAttribute(Attribute.BODY    ).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.AGILITY ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(0, model.getAttribute(Attribute.MAGIC   ).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void test2MaxWithAugment() {
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 2));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 10));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.REACTION, 6));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 6));
		model.getAttribute(Attribute.BODY    ).setPoints(6);
		model.getAttribute(Attribute.AGILITY ).setPoints(1);
		model.getAttribute(Attribute.REACTION).setPoints(5);
		model.getAttribute(Attribute.STRENGTH).setPoints(1);
		model.getAttribute(Attribute.WILLPOWER).setPoints(1);
		model.getAttribute(Attribute.LOGIC   ).setPoints(1);
		model.getAttribute(Attribute.INTUITION).setPoints(1);
		model.getAttribute(Attribute.CHARISMA).setPoints(1);
		attrib.process(model, mods);
		assertEquals(1, attrib.getPointsLeft());
		assertEquals(50, model.getKarmaFree());
		assertEquals("ToDos are: "+attrib.getToDos(), 2, attrib.getToDos().size());
		assertEquals(5, model.getAttribute(Attribute.REACTION).getPoints());
		assertEquals(5, model.getAttribute(Attribute.REACTION).getModifiedValue());
		
		assertFalse(attrib.canBeIncreased(Attribute.REACTION));
		
		// Add synaptic booster
		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("synaptic_booster"), 1);
		model.addItem(item);
//		assertNotNull(charGen.getEquipmentController().select(ShadowrunCore.getItem("synaptic_booster")));
//		ShadowrunTools.loadEquipmentModificatdions(model);
		ShadowrunTools.recalculateCharacter(model);
		assertEquals(5, model.getAttribute(Attribute.REACTION).getPoints());
		assertEquals(6, model.getAttribute(Attribute.REACTION).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void testMaxAdjustmentPoints() {
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("magician"));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 2));
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 5));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 7));
		model.getAttribute(Attribute.BODY    ).setPoints(2);
		model.getAttribute(Attribute.EDGE    ).setPoints(3);
		model.getAttribute(Attribute.MAGIC   ).setPoints(5);
		attrib = new PriorityAttributeGenerator(model);
		attrib.process(model, mods);
		// 7 adjustment points go into EDGE (+2) and MAGIC (+5).
		// 1 attribute point goes into BODY
		assertEquals(1, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(25, model.getKarmaFree());
		assertEquals(1, attrib.getToDos().size());

		assertEquals(1, attrib.getPerAttribute(Attribute.BODY  ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.EDGE  ).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC ).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY  ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE  ).adjust);
		assertEquals(5, attrib.getPerAttribute(Attribute.MAGIC ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY  ).karma);
		assertEquals(25, attrib.getPerAttribute(Attribute.EDGE ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).karma);
		assertEquals(2, model.getAttribute(Attribute.BODY  ).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.EDGE  ).getModifiedValue());
		assertEquals(5, model.getAttribute(Attribute.MAGIC ).getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void testMaxAdjustmentPoints2() {
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("magician"));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 2));
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 6));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 7));
		model.getAttribute(Attribute.BODY    ).setPoints(2);
		model.getAttribute(Attribute.EDGE    ).setPoints(3);
		model.getAttribute(Attribute.MAGIC   ).setPoints(5);
		attrib = new PriorityAttributeGenerator(model);
		attrib.process(model, mods);
		// 7 adjustment points go into EDGE (+2) and MAGIC (+5).
		// 1 attribute point goes into BODY
		assertEquals(1, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(35, model.getKarmaFree());
		assertEquals(1, attrib.getToDos().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testMaxAdjustmentPoints3() {
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("magician"));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 24));
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 9));
		setMagicTo(3);
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 7));
		model.getAttribute(Attribute.BODY    ).setPoints(6); // 2 adjust, 3 regular
		model.getAttribute(Attribute.LOGIC   ).setPoints(4); // 3 regular
		model.getAttribute(Attribute.EDGE    ).setPoints(4); // 3 adjust 
		model.getAttribute(Attribute.MAGIC   ).setPoints(3); // 3 adjust
		attrib.process(model, mods);
		assertEquals(6, model.getAttribute(Attribute.BODY).getGenerationValue());
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getGenerationValue());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(19, attrib.getPointsLeft());
		assertEquals(50, model.getKarmaFree());
		assertEquals(1, attrib.getToDos().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testComplexTest() {
//		model  = new ShadowrunCharacter();
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 19));
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 11));
		setMagicTo(2);
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.EDGE, 7));
		model.getAttribute(Attribute.BODY     ).setPoints(6); // 5
		model.getAttribute(Attribute.AGILITY  ).setPoints(6); // 5
		model.getAttribute(Attribute.REACTION ).setPoints(5); // 4
		model.getAttribute(Attribute.WILLPOWER).setPoints(3); // 2
		model.getAttribute(Attribute.LOGIC    ).setPoints(2); // 1
		model.getAttribute(Attribute.INTUITION).setPoints(2); // 1
		model.getAttribute(Attribute.CHARISMA ).setPoints(3); // 2
		model.getAttribute(Attribute.EDGE     ).setPoints(7); // 6 special
		model.getAttribute(Attribute.MAGIC    ).setPoints(4); // 4 special
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		attrib.process(model, mods);
		assertEquals(4, model.getAttribute(Attribute.MAGIC).getPoints());
		assertEquals(2, model.getAttribute(Attribute.MAGIC).getGenerationModifier());
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getGenerationValue());
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		assertEquals(1, attrib.getPointsLeftSpecial());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(40, model.getKarmaFree());
	}

	//-------------------------------------------------------------------
	@Test
	public void testComplexTest2() {
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 19));
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 11));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.EDGE, 7));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 7));
		setMagicTo(2);
		model.getAttribute(Attribute.BODY     ).setPoints(6); // 4 regular, 1 special
		model.getAttribute(Attribute.AGILITY  ).setPoints(6); // 5
		model.getAttribute(Attribute.REACTION ).setPoints(5); // 4
		model.getAttribute(Attribute.WILLPOWER).setPoints(3); // 2
		model.getAttribute(Attribute.LOGIC    ).setPoints(2); // 1
		model.getAttribute(Attribute.INTUITION).setPoints(2); // 1
		model.getAttribute(Attribute.CHARISMA ).setPoints(3); // 2
		model.getAttribute(Attribute.EDGE     ).setPoints(7); // 6 special
		model.getAttribute(Attribute.MAGIC    ).setPoints(4); // 4 special
		attrib.process(model, mods);
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getGenerationValue());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(50, model.getKarmaFree());
	}

	//-------------------------------------------------------------------
	@Test
	public void testComplexTest3() {
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 17));
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 11));
		setMagicTo(2);
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.EDGE, 7));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 7));
		model.getAttribute(Attribute.BODY     ).setPoints(6); // 4 regular, 1 special
		model.getAttribute(Attribute.AGILITY  ).setPoints(6); // 5
		model.getAttribute(Attribute.REACTION ).setPoints(5); // 4
		model.getAttribute(Attribute.WILLPOWER).setPoints(3); // 2
		model.getAttribute(Attribute.LOGIC    ).setPoints(2); // 1 with 10 karma
		model.getAttribute(Attribute.INTUITION).setPoints(2); // 1 with 10 karma
		model.getAttribute(Attribute.CHARISMA ).setPoints(3); // 2
		model.getAttribute(Attribute.EDGE     ).setPoints(7); // 6 special
		model.getAttribute(Attribute.MAGIC    ).setPoints(4); // 4 special
		attrib.process(model, mods);
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getGenerationValue());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(30, model.getKarmaFree());
	}

	//-------------------------------------------------------------------
	@Test
	public void testComplexTest4() {
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 15));
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 13));
		setMagicTo(2);
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 7));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.STRENGTH, 8));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.WILLPOWER, 7));
		model.getAttribute(Attribute.BODY     ).setPoints(7); // 1 special, 5 regular
		model.getAttribute(Attribute.AGILITY  ).setPoints(3); // 2 regular
		model.getAttribute(Attribute.REACTION ).setPoints(3); // 2 regular
		model.getAttribute(Attribute.STRENGTH ).setPoints(6); // 5 special
		model.getAttribute(Attribute.WILLPOWER).setPoints(3); // 1 regular
		model.getAttribute(Attribute.LOGIC    ).setPoints(5); // 4 regular
		model.getAttribute(Attribute.INTUITION).setPoints(2); // 1 for 10 Karma
		model.getAttribute(Attribute.CHARISMA ).setPoints(1); // 
		model.getAttribute(Attribute.EDGE     ).setPoints(6); // 5 special
		model.getAttribute(Attribute.MAGIC    ).setPoints(2); // 2 special
		attrib = new PriorityAttributeGenerator(model);
		attrib.process(model, mods);
		assertEquals(4, model.getAttribute(Attribute.MAGIC).getGenerationValue());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(40, model.getKarmaFree());
	}

	//-------------------------------------------------------------------
	/**
	 * @see https://bitbucket.org/rpgframework/shadowrun-6/issues/33/attribute-optimizer-didnt-calculate-this
	 */
	@Test
	public void testGlennsBug() {
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 4));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 16));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.AGILITY, 7));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.CHARISMA, 8));
		attrib.process(model, mods);
		assertEquals(50, model.getKarmaFree());
		model.getAttribute(Attribute.BODY    ).setPoints(3); // 2 regular
		model.getAttribute(Attribute.AGILITY ).setPoints(6); // 2 adjust, 3 regular
		model.getAttribute(Attribute.REACTION).setPoints(5); // 1
		model.getAttribute(Attribute.STRENGTH).setPoints(1); // 0
		model.getAttribute(Attribute.WILLPOWER).setPoints(1);// 4
		model.getAttribute(Attribute.LOGIC   ).setPoints(1); // 1
		model.getAttribute(Attribute.INTUITION).setPoints(3);// 2
		model.getAttribute(Attribute.CHARISMA).setPoints(6); // 4
		model.getAttribute(Attribute.EDGE    ).setPoints(3); // 4
		attrib.process(model, mods);
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(50, model.getKarmaFree());
		
		// Now increase Logic to 2, paying 10 karma
		model.getAttribute(Attribute.LOGIC   ).setPoints(2); // 10 karma
		model.setKarmaFree(50);
		attrib.process(model, mods);
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(40, model.getKarmaFree());
		
		// Now for the bug: 
		model.getAttribute(Attribute.WILLPOWER).setPoints(2); // 10 karma
		model.getAttribute(Attribute.STRENGTH).setPoints(2); // 10 karma
		model.setKarmaFree(50);
		attrib.process(model, mods);
		assertEquals(10,attrib.getPerAttribute(Attribute.WILLPOWER).getKarmaInvest());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(20, model.getKarmaFree());

		assertEquals(1, attrib.getPerAttribute(Attribute.BODY  ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.STRENGTH).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.EDGE  ).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC ).base);
		
		assertEquals(2, attrib.getPerAttribute(Attribute.BODY   ).regular);
		assertEquals(3, attrib.getPerAttribute(Attribute.AGILITY).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE   ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC  ).regular);
		
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY   ).adjust);
		assertEquals(2, attrib.getPerAttribute(Attribute.AGILITY).adjust);
		assertEquals(3, attrib.getPerAttribute(Attribute.EDGE   ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC  ).adjust);
		
		assertEquals(0 , attrib.getPerAttribute(Attribute.BODY  ).karma);
		assertEquals(0 , attrib.getPerAttribute(Attribute.EDGE ).karma);
		assertEquals(0 , attrib.getPerAttribute(Attribute.MAGIC   ).karma);
		assertEquals(10, attrib.getPerAttribute(Attribute.STRENGTH).karma);
		
		assertEquals(3, model.getAttribute(Attribute.BODY  ).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.EDGE  ).getModifiedValue());
		assertEquals(0, model.getAttribute(Attribute.MAGIC ).getModifiedValue());
	}

	//-------------------------------------------------------------------
	/**
	 * @see
	 */
	@Test
	public void testMagicPoints() {
		mods.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, 4));
		mods.add(new FreePointsModification(Type.ATTRIBUTES, 16));
		MagicOrResonanceType source = ShadowrunCore.getMagicOrResonanceType("adept");
		assertNotNull(source);
		mods.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.MAGIC, 2, ModificationType.RELATIVE, source));
		mods.add(new AttributeModification(ModificationValueType.MAX, Attribute.CHARISMA, 8));
		mods = (new ApplyAttributeModifications()).process(model, mods);
		attrib.process(model, mods);
		assertEquals(50, model.getKarmaFree());
		
		assertEquals(1, model.getAttribute(Attribute.BODY  ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.EDGE  ).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.MAGIC ).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.MAGIC ).getGenerationValue());

		assertEquals(1, attrib.getPerAttribute(Attribute.BODY  ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.STRENGTH).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.EDGE  ).base);
		assertEquals(2, attrib.getPerAttribute(Attribute.MAGIC ).base);
		
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY   ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE   ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC  ).regular);
		
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY   ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE   ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC  ).adjust);
		
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY  ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH).karma);

		model.getAttribute(Attribute.BODY    ).setPoints(3); // 2 regular
		model.getAttribute(Attribute.AGILITY ).setPoints(6); // 2 adjust, 3 regular
		model.getAttribute(Attribute.REACTION).setPoints(5); // 1
		model.getAttribute(Attribute.STRENGTH).setPoints(1); // 0
		model.getAttribute(Attribute.WILLPOWER).setPoints(1);// 4
		model.getAttribute(Attribute.LOGIC   ).setPoints(1); // 1
		model.getAttribute(Attribute.INTUITION).setPoints(3);// 2
		model.getAttribute(Attribute.CHARISMA).setPoints(6); // 4
		model.getAttribute(Attribute.EDGE    ).setPoints(3); // 4
		model.getAttribute(Attribute.MAGIC   ).setPoints(4); // 2
		attrib.process(model, mods);
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(50, model.getKarmaFree());
		
		// Now increase Logic to 2, paying 10 karma
		model.getAttribute(Attribute.LOGIC   ).setPoints(2); // 10 karma
		model.setKarmaFree(50);
		attrib.process(model, mods);
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(40, model.getKarmaFree());
		
		// Now for the bug: 
		model.getAttribute(Attribute.WILLPOWER).setPoints(2); // 10 karma
		model.getAttribute(Attribute.STRENGTH).setPoints(2); // 10 karma
		model.setKarmaFree(50);
		attrib.process(model, mods);
		assertEquals(10,attrib.getPerAttribute(Attribute.WILLPOWER).getKarmaInvest());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0, attrib.getPointsLeftSpecial());
		assertEquals(20, model.getKarmaFree());
		
		assertEquals(3, model.getAttribute(Attribute.BODY   ).getModifiedValue());
		assertEquals(6, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.EDGE   ).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.MAGIC  ).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.MAGIC  ).getGenerationValue());

		assertEquals(1, attrib.getPerAttribute(Attribute.BODY    ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.AGILITY ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.STRENGTH).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.EDGE    ).base);
		assertEquals(2, attrib.getPerAttribute(Attribute.MAGIC   ).base);
		
		assertEquals(2, attrib.getPerAttribute(Attribute.BODY    ).regular);
		assertEquals(5, attrib.getPerAttribute(Attribute.AGILITY ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE    ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC   ).regular);
		
		assertEquals(0, attrib.getPerAttribute(Attribute.BODY   ).adjust);
		assertEquals(2, attrib.getPerAttribute(Attribute.AGILITY).adjust);
		assertEquals(3, attrib.getPerAttribute(Attribute.EDGE   ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC  ).adjust);
		
		assertEquals(0 , attrib.getPerAttribute(Attribute.BODY  ).karma);
		assertEquals(0 , attrib.getPerAttribute(Attribute.EDGE ).karma);
		assertEquals(0 , attrib.getPerAttribute(Attribute.MAGIC   ).karma);
		assertEquals(10, attrib.getPerAttribute(Attribute.STRENGTH).karma);
		
		assertEquals(3, model.getAttribute(Attribute.BODY  ).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.EDGE  ).getModifiedValue());
		assertEquals(0, model.getAttribute(Attribute.MAGIC ).getModifiedValue());
	}
	
}

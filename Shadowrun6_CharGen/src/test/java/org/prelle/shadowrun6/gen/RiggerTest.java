/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DronePool;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.proc.ItemRecalculation;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class RiggerTest {
	
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Locale.setDefault(Locale.ENGLISH);
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model = new ShadowrunCharacter();
		model.addSkill(new SkillValue(ShadowrunCore.getSkill("engineering"), 4));
		model.getAttribute(Attribute.LOGIC).setPoints(3);
	}

	//-------------------------------------------------------------------
	@Test
	public void testRigger() {
		// Prepare rig
		CarriedItem rig = new CarriedItem(ShadowrunCore.getItem("control_rig"), 2);
		// Add RCC
		CarriedItem rcc = new CarriedItem(ShadowrunCore.getItem("vulcan_liegelord"));
		ItemRecalculation.recalculate("", rcc);
		
		ItemTemplate weaponI = ShadowrunCore.getItem("ak_97");
		ItemTemplate accessI = ShadowrunCore.getItem("weapon_mount_standard");
		// Sensor: 4   Pilot: 4
		ItemTemplate droneI = ShadowrunCore.getItem("steel_lynx_combat_drone");
		assertNotNull(weaponI);
		assertNotNull(accessI);
		assertNotNull(droneI);
		CarriedItem drone = new CarriedItem(droneI);
		ItemRecalculation.recalculate("", drone);
		CarriedItem access = new CarriedItem(accessI);
		CarriedItem weapon = new CarriedItem(weaponI);
		drone.addAccessory(ItemHook.VEHICLE_BODY, access);
		access.addAccessory(ItemHook.VEHICLE_WEAPON, weapon);
		
		drone.refreshVirtual();
		
		List<CarriedItem> weapons = ShadowrunTools.getEmbeddedWeapons(drone);
		assertNotNull(weapons);
		assertFalse(weapons.isEmpty());
		weapon = weapons.get(0);
		DronePool pool = ShadowrunTools.getDroneWeaponPool(model, drone, weapon);
		assertNotNull(pool);
		assertEquals("Did not detect missing rig",0, pool.rigged);  // Logic 3 + Skill 4
		assertEquals("Did not detect missing rcc and autosoft",0, pool.viaRCC);  
		assertEquals("Did not detect missing autosoft",0, pool.autonomous); // No Autosoft at all - 0
		
		// Now add rigger command console (but no autosoft or rig yet)
		rcc.setSlot(ItemHook.ELECTRONIC_ACCESSORY);
		rcc.setUsedAsType(ItemType.ELECTRONICS);
		rcc.setUsedAsSubType(ItemSubType.RIGGER_CONSOLE);
		model.addItem(rcc);
		pool = ShadowrunTools.getDroneWeaponPool(model, drone, weapon);
		assertNotNull(pool);
		assertEquals("Did not detect missing rig",0, pool.rigged);  // Logic 3 + Skill 4
		assertEquals("Did not detect missing autosoft in RCC",0, pool.viaRCC); // Missing autosoft  
		assertEquals("Did not detect missing autosoft",0, pool.autonomous);  // Missing autosoft
		
		// Add wrong autosoft
		CarriedItem wrongAuto = new CarriedItem(ShadowrunCore.getItem("targeting"),3);
		wrongAuto.setChoiceReference("fn_har");
		rcc.addAccessory(wrongAuto);
		assertFalse("Added autosoft not found",rcc.getEffectiveAccessories().isEmpty());
		drone.addAccessory(wrongAuto);
		rcc.refreshVirtual();
		drone.refreshVirtual();
		pool = ShadowrunTools.getDroneWeaponPool(model, drone, weapon);
		assertNotNull(pool);
		assertEquals("Did not detect missing rig",0, pool.rigged);  // Logic 3 + Skill 4
		assertEquals("Did not detect wrong autosoft in RCC",3, pool.viaRCC); // Wrong autosoft  
		assertEquals("Did not detect wrong autosoft",3, pool.autonomous);  // Wrong autosoft
		
		// Add matching autosoft
		CarriedItem matchAuto = new CarriedItem(ShadowrunCore.getItem("targeting"), 4);
		matchAuto.setChoiceReference("ak_97");
		rcc.addAccessory(matchAuto);
		drone.addAccessory(matchAuto);
		System.out.println("Call getDroneWeaponPool");
		pool = ShadowrunTools.getDroneWeaponPool(model, drone, weapon);
		assertNotNull(pool);
		assertEquals("Did not detect missing rig",0, pool.rigged);  // Logic 3 + Skill 4
		assertEquals("Invalid for autosot in RCC",8, pool.viaRCC); // Sensor(4) + Autosoft(4)  
		assertEquals("Invalud for autosoft in drone",8, pool.autonomous);  // Sensor(4) + Autosoft(4)
		
		// Add rig
		model.addItem(rig);
		pool = ShadowrunTools.getDroneWeaponPool(model, drone, weapon);
		assertNotNull(pool);
		assertEquals("Invalid value with rig",9, pool.rigged);  // Logic 3 + Skill 4
		assertEquals("Invalid for autosot in RCC",8, pool.viaRCC); // Sensor(4) + Autosoft(4)  
		assertEquals("Invalud for autosoft in drone",8, pool.autonomous);  // Sensor(4) + Autosoft(4)
	}

}

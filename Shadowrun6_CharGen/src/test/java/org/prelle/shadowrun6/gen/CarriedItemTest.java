/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.BodytechQuality;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.proc.ItemRecalculation;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.ItemHookModification;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CarriedItemTest {

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Locale.setDefault(Locale.ENGLISH);
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleWeapon() {
		ItemTemplate template = ShadowrunCore.getItem("colt_america_l36");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("colt_america_l36"));
		ItemRecalculation.recalculate("", item);
		assertEquals(1, item.getCount());
		assertEquals(template.getAvailability(), item.getAvailability());
		assertEquals(template.getWeaponData().getDamage(), item.getAsValue(ItemAttribute.DAMAGE));
		assertTrue(Arrays.equals(template.getWeaponData().getAttackRating(), (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		assertEquals("SA", item.getAsObject(ItemAttribute.MODE).getValue());
		assertEquals("11(c)", item.getAsObject(ItemAttribute.AMMUNITION).getValue());
		assertEquals("2(L)", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		
		assertEquals(3, item.getSlots().size());
		assertNotNull(item.getSlot(ItemHook.TOP));
		assertEquals(0, item.getSlot(ItemHook.TOP).getCapacity());
		assertNotNull(item.getSlot(ItemHook.TOP).getAllEmbeddedItems());
		assertTrue(item.getSlot(ItemHook.TOP).getAllEmbeddedItems().isEmpty());
		assertNotNull(item.getSlot(ItemHook.BARREL));
		assertEquals(0, item.getSlot(ItemHook.BARREL).getCapacity());
		assertNull(item.getSlot(ItemHook.UNDER));
		
		assertEquals(0, item.getModifications().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponWithBuiltinAccessory() {
		ItemTemplate template = ShadowrunCore.getItem("ares_light_fire_75");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template);
		ItemRecalculation.recalculate("", item);
		assertEquals(1, item.getCount());
		assertEquals(template.getAvailability(), item.getAvailability());
		assertEquals(template.getWeaponData().getDamage(), item.getAsValue(ItemAttribute.DAMAGE));
		assertTrue(Arrays.equals(template.getWeaponData().getAttackRating(), (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		assertEquals("SA", item.getAsObject(ItemAttribute.MODE).getValue());
		assertEquals("16(c)", item.getAsObject(ItemAttribute.AMMUNITION).getValue());
		assertEquals("3(L)", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		
		assertEquals(4, item.getSlots().size());
		assertNotNull(item.getSlot(ItemHook.TOP));
		assertEquals(0, item.getSlot(ItemHook.TOP).getCapacity());
		assertNotNull(item.getSlot(ItemHook.TOP).getAllEmbeddedItems());
		assertEquals(1, item.getSlot(ItemHook.TOP).getAllEmbeddedItems().size());
		assertNotNull(item.getSlot(ItemHook.BARREL));
		assertNotNull(item.getSlot(ItemHook.BARREL).getAllEmbeddedItems());
		assertEquals(1, item.getSlot(ItemHook.BARREL).getAllEmbeddedItems().size());
		assertEquals("ares_lf_silencer", item.getSlot(ItemHook.BARREL).getAllEmbeddedItems().get(0).getItem().getId());
		assertNotNull(item.getSlot(ItemHook.INTERNAL));
		assertNull(item.getSlot(ItemHook.UNDER));
		assertTrue( Arrays.equals(new int[]{10,7,6,0,0}, (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		
		assertEquals(0, item.getModifications().size());
		// Expect included +1 modifier from laser-sight and +2 modifier from smartgun_system_internal 
		assertEquals(2, item.getAsObject(ItemAttribute.ATTACK_RATING).getModifications().size());
		
		System.out.println("--------------------------------");
		
		// Now remove the barrel, so there is no laser-sight
		ItemHookModification removeBarrelMod = new ItemHookModification(ItemHook.TOP, 0);
		removeBarrelMod.setRemove(true);
		ItemEnhancement removeBarrel = new ItemEnhancement();
		removeBarrel.addModification(removeBarrelMod);
		item.addEnhancement(new ItemEnhancementValue(removeBarrel));
		item.getAsObject(ItemAttribute.ATTACK_RATING);
		assertTrue("Result was "+Arrays.toString( (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()), 
				Arrays.equals(new int[] {9,6,5,0,0}, (int[])item.getAsObject(ItemAttribute.ATTACK_RATING).getValue()));
		assertEquals(0, item.getModifications().size());
//		// Expect -1 modifier from missing laser-sight and +2 modifier from smartgun_system_internal 
//		assertEquals(2, item.getAsObject(ItemAttribute.ATTACK_RATING).getModifications().size());
		// Expect  +2 modifier from smartgun_system_internal 
		assertEquals(1, item.getAsObject(ItemAttribute.ATTACK_RATING).getModifications().size());
	}


	//-------------------------------------------------------------------
	@Test
	public void testCyberlimbAttributeIncrease() {
		ItemTemplate template = ShadowrunCore.getItem("attribute_increase_agility");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template, 4);
		assertEquals(20000, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals("4", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		assertEquals( 4 , item.getAsValue(ItemAttribute.CAPACITY).getModifiedValue());
		assertEquals(1, item.getModifications().size());
		assertSame(AttributeModification.class, item.getModifications().get(0).getClass());
		assertEquals(Attribute.AGILITY, ((AttributeModification)item.getModifications().get(0)).getAttribute());
		assertEquals(4, ((AttributeModification)item.getModifications().get(0)).getValue());
		assertNotNull(((AttributeModification)item.getModifications().get(0)).getSource());
		
		assertEquals(0, item.getRequirements().size());
	}	

	//-------------------------------------------------------------------
	@Test
	public void testCyberlimb() {
		ItemTemplate template = ShadowrunCore.getItem("cyberleg_obvious");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("cyberleg_obvious"));
		ItemRecalculation.recalculate("", item);
		assertEquals("4", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		assertNotNull(item.getSlot(ItemHook.CYBERLIMB_IMPLANT));
		assertEquals( 20, item.getSlot(ItemHook.CYBERLIMB_IMPLANT).getCapacity());
		assertEquals(3, item.getModifications().size());
		assertSame(AttributeModification.class, item.getModifications().get(0).getClass());
		assertEquals(Attribute.AGILITY, ((AttributeModification)item.getModifications().get(0)).getAttribute());
		assertEquals(Attribute.STRENGTH, ((AttributeModification)item.getModifications().get(1)).getAttribute());
		assertEquals(Attribute.PHYSICAL_MONITOR, ((AttributeModification)item.getModifications().get(2)).getAttribute());
		assertEquals(2, ((AttributeModification)item.getModifications().get(1)).getValue());
		
		assertEquals(1000, item.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
		assertEquals(15000, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		item.setQuality(BodytechQuality.BETA);
		assertEquals(700, item.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
		assertEquals(22500, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
	}	

	//-------------------------------------------------------------------
	@Test
	public void testCyberlimbWithAttributeIncrease() {
		ItemTemplate template = ShadowrunCore.getItem("cyberleg_obvious");
		assertNotNull(template);
		CarriedItem leg = new CarriedItem(ShadowrunCore.getItem("cyberleg_obvious"));
		CarriedItem inc = new CarriedItem(ShadowrunCore.getItem("attribute_increase_agility"), 4);
		ItemRecalculation.recalculate("", inc);
		ItemRecalculation.recalculate("", leg);
		leg.addAccessory(ItemHook.CYBERLIMB_IMPLANT, inc);

		assertEquals(1000, leg.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue());
		assertEquals(35000, leg.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertTrue(leg.getToDos().isEmpty());
		leg.setQuality(BodytechQuality.BETA);
		ItemRecalculation.recalculate("", leg);
		assertEquals("Warning because of invalid accessory grade expected",1, leg.getToDos().size());
		
		// Correct accessory implant grade
		inc.setQuality(BodytechQuality.BETA);
		ItemRecalculation.recalculate("", leg);
		assertTrue(leg.getToDos().isEmpty());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleVehicle() {
		ItemTemplate template = ShadowrunCore.getItem("chrysler-nissan_jackrabbit");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("chrysler-nissan_jackrabbit"));
		ItemRecalculation.recalculate("", item);
		assertEquals(1, item.getCount());
		assertEquals("2", item.getAsObject(ItemAttribute.AVAILABILITY).getValue().toString());
		assertEquals(11000, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(template.getAvailability(), item.getAvailability());
		assertEquals(template.getVehicleData().getHandling(), item.getAsObject(ItemAttribute.HANDLING).getValue());
		assertEquals(template.getVehicleData().getAcceleration() , item.getAsValue(ItemAttribute.ACCELERATION).getModifiedValue());
		assertEquals(template.getVehicleData().getSpeedInterval(), item.getAsValue(ItemAttribute.SPEED_INTERVAL).getModifiedValue());
		assertEquals(template.getVehicleData().getTopSpeed()     , item.getAsValue(ItemAttribute.SPEED).getModifiedValue());
		assertEquals(template.getVehicleData().getBody()         , item.getAsValue(ItemAttribute.BODY).getModifiedValue());
		assertEquals(template.getVehicleData().getArmor()        , item.getAsValue(ItemAttribute.ARMOR).getModifiedValue());
		assertEquals(template.getVehicleData().getPilot()        , item.getAsValue(ItemAttribute.PILOT).getModifiedValue());
		assertEquals(template.getVehicleData().getSensor()       , item.getAsValue(ItemAttribute.SENSORS).getModifiedValue());
		assertEquals(template.getVehicleData().getSeats()        , item.getAsValue(ItemAttribute.SEATS).getModifiedValue());
		
//		assertEquals(2, item.getSlots().size());
//		assertNotNull(item.getSlot(ItemHook.TOP));
//		assertEquals(0, item.getSlot(ItemHook.TOP).getCapacity());
//		assertNotNull(item.getSlot(ItemHook.TOP).getAllEmbeddedItems());
//		assertTrue(item.getSlot(ItemHook.TOP).getAllEmbeddedItems().isEmpty());
//		assertNotNull(item.getSlot(ItemHook.BARREL));
//		assertEquals(0, item.getSlot(ItemHook.BARREL).getCapacity());
//		assertNull(item.getSlot(ItemHook.UNDER));
		
		assertEquals(0, item.getModifications().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void testDermalPlating() {
		ItemTemplate template = ShadowrunCore.getItem("dermal_plating");
		assertNotNull(template);
		CarriedItem item = new CarriedItem(template, 3);
		List<Modification> calc = ItemRecalculation.recalculate("", item);
		assertEquals(3, item.getRating());
		assertEquals(2, item.getModifications().size()); // Add item dermal_plating_armor, remove dermal deposits
	}

	//-------------------------------------------------------------------
	@Test
	public void testCyberlimbWithWeapon() {
		ItemTemplate weaponI = ShadowrunCore.getItem("fichetti_tiffani_needler");
		ItemTemplate accessI = ShadowrunCore.getItem("cyber_holdout");
		ItemTemplate limbI = ShadowrunCore.getItem("cyberarm_obvious");
		assertNotNull(weaponI);
		assertNotNull(accessI);
		assertNotNull(limbI);
		CarriedItem limb = new CarriedItem(limbI);
		assertEquals(15000, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		CarriedItem access = new CarriedItem(accessI);
//		access.setUsedAsType(ItemType.ACCESSORY);
//		access.setUsedAsSubType(ItemSubType.CYBER_LIMB_ACCESSORY);
//		access.setSlot(ItemHook.CYBERLIMB_IMPLANT);
		ItemRecalculation.recalculate("", access);
		ItemRecalculation.recalculate("", limb);
		assertTrue(limb.getWeaponDataRecursive().isEmpty());
		assertEquals(15000, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(2000, access.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		// Add holdout space into the arm
		limb.addAccessory(ItemHook.CYBERLIMB_IMPLANT, access);
		ItemRecalculation.recalculate("", limb);
		assertEquals(17000, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertTrue(limb.getWeaponDataRecursive().isEmpty());
		
		// Now add weapon to holdout space
		CarriedItem weapon = new CarriedItem(weaponI);
		assertEquals(435, weapon.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		access.addAccessory(ItemHook.MOUNTED_HOLDOUT, weapon);
		assertEquals(2435, access.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		assertEquals(17435, limb.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		ItemRecalculation.recalculate("", limb);
		assertFalse(limb.getWeaponDataRecursive().isEmpty());
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testAresAlpha() {
//		ItemTemplate template = ShadowrunCore.getItem("ares_alpha");
//		assertNotNull(template);
//		CarriedItem item = new CarriedItem(template);
//		System.out.println(item.dump());
//		Collection<CarriedItem> accessories = item.getEffectiveAccessories();	
//		System.out.println("WEAP = "+accessories.iterator().next());
//		assertEquals(ItemType.WEAPON, accessories.iterator().next().getUsedAsType());
//	}

}

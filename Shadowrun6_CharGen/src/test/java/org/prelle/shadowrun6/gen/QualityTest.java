/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Locale;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.chargen.PriorityAttributeGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemTemplate;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

/**
 * @author prelle
 *
 */
public class QualityTest {

	private NewPriorityCharacterGenerator charGen;
	private PriorityAttributeGenerator attrib;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Locale.setDefault(Locale.ENGLISH);
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		charGen = new NewPriorityCharacterGenerator();
		charGen.start(model);
		attrib = (PriorityAttributeGenerator) charGen.getAttributeController();
	}

	//-------------------------------------------------------------------
	@Test
	public void test1() {
		model.getAttribute(Attribute.BODY).setPoints(5);
		model.setMetatype(ShadowrunCore.getMetaType("troll"));
		ShadowrunTools.recalculateCharacter(model);
		assertNotNull(model.getQuality("dermal_deposits"));
		assertNotNull(model.getItem("dermal_deposits"));
		assertNull(model.getItem("dermal_plating"));
		assertEquals(6, model.getAttribute(Attribute.DEFENSE_RATING).getModifiedValue());
		
		// Now add cyberware "dermal plating" which whill remove dermal deposits
		ItemTemplate _dermal = ShadowrunCore.getItem("dermal_plating");
		CarriedItem dermal   = new CarriedItem(_dermal, 3);

		model.addItem(dermal);
		ShadowrunTools.recalculateCharacter(model);
		assertNotNull(model.getQuality("dermal_deposits"));
		assertNull(model.getItem("dermal_deposits"));
		assertNotNull(model.getItem("dermal_plating"));
		assertEquals(8, model.getAttribute(Attribute.DEFENSE_RATING).getModifiedValue());
	}
	
}

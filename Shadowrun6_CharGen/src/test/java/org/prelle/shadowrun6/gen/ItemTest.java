package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.CommonEquipmentController;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.levelling.EquipmentLeveller;

import de.rpgframework.ConfigOption;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

public class ItemTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAutoAccessory() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setMetatype(ShadowrunCore.getMetaType("human"));
		CommonEquipmentController equip = new CommonEquipmentController(new ProcessorRunner() {
			public void runProcessors() {
				ShadowrunTools.recalculateCharacter(model);
			}
		}, model, CharGenMode.LEVELING) {

			public boolean canIncreaseBoughtNuyen() {return false;}
			public boolean canDecreaseBoughtNuyen() {return false;}
			public boolean increaseBoughtNuyen() {return false;}
			public boolean decreaseBoughtNuyen() {return false;}
			public int getBoughtNuyen() {return 0;}
			public List<ConfigOption<?>> getConfigOptions() {return null;}
			};
		
		equip.select(ShadowrunCore.getItem("gm-nissan_dobermann"));
		
//		AvailableSlot slot = item.getSlot(ItemHook.VEHICLE_BODY);
//		assertNotNull(slot);
//		assertEquals(0, slot.getUsedCapacity());
//		assertEquals(slot.getFreeCapacity(), slot.getCapacity());
		try {
			String saved = new String(ShadowrunCore.save(model), "UTF-8");
			System.out.println(saved);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testAutoAccessoryWithAccessory() {
		ShadowrunCharacter model = new ShadowrunCharacter();
		model.setMetatype(ShadowrunCore.getMetaType("human"));
		CommonEquipmentController equip = new EquipmentLeveller(new ProcessorRunner() {
			public void runProcessors() {
				ShadowrunTools.recalculateCharacter(model);
			}
		}, model);
		
		CarriedItem drone = equip.select(ShadowrunCore.getItem("gm-nissan_dobermann"));

		assertEquals(1, model.getItems(false).size());
		assertEquals(2, model.getItemsRecursive(false).size());
		assertNotNull(drone.getUniqueId());
		
		AvailableSlot slot = drone.getSlot(ItemHook.VEHICLE_BODY);
		assertNotNull(slot);
		assertEquals(0, slot.getUsedCapacity());
		assertEquals(slot.getFreeCapacity(), slot.getCapacity());
		
		CarriedItem mount = slot.getAllEmbeddedItems().get(0);
		assertNotNull(mount.getUniqueId());
		assertNotNull("Auto-accessory not flagged as embedded",mount.getEmbeddedIn());
		assertTrue(mount.getEmbeddedIn().equals(drone.getUniqueId()));
		assertNotNull(drone.getEmbeddedItem(mount.getUniqueId()));
		
		// Add weapon
		CarriedItem weapon = equip.embed(mount, ShadowrunCore.getItem("hk_227"));
		assertEquals(1, model.getItems(false).size());
		assertEquals(6, model.getItemsRecursive(false).size());
		assertTrue(weapon.getEmbeddedIn().equals(mount.getUniqueId()));
		
		try {
			String saved = new String(ShadowrunCore.save(model), "UTF-8");
			System.out.println(saved);
			System.out.println(drone.dump());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void loadAutoAccessoryWithAccessory() throws IOException {
		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" + 
				"<sr6char magic=\"mundane\" meta=\"human\">\n" + 
				"   <name>Unnamed chummer</name>\n" + 
				"   <attributes>\n" + 
				"      <attribute id=\"BODY\" value=\"0\"/>\n" + 
				"      <attribute id=\"AGILITY\" value=\"0\"/>\n" + 
				"      <attribute id=\"REACTION\" value=\"0\"/>\n" + 
				"      <attribute id=\"STRENGTH\" value=\"0\"/>\n" + 
				"      <attribute id=\"WILLPOWER\" value=\"0\"/>\n" + 
				"      <attribute id=\"LOGIC\" value=\"0\"/>\n" + 
				"      <attribute id=\"INTUITION\" value=\"0\"/>\n" + 
				"      <attribute id=\"CHARISMA\" value=\"0\"/>\n" + 
				"      <attribute id=\"EDGE\" value=\"0\"/>\n" + 
				"      <attribute id=\"MAGIC\" value=\"0\"/>\n" + 
				"      <attribute id=\"RESONANCE\" value=\"0\"/>\n" + 
				"      <attribute id=\"HEAT\" value=\"0\"/>\n" + 
				"      <attribute id=\"REPUTATION\" value=\"0\"/>\n" + 
				"      <attribute id=\"POWER_POINTS\" value=\"0\"/>\n" + 
				"   </attributes>\n" + 
				"   <size>0</size>\n" + 
				"   <weight>0</weight>\n" + 
				"   <gender>MALE</gender>\n" + 
				"   <gear>\n" + 
				"      <item count=\"1\" ref=\"gm-nissan_dobermann\" subtype=\"MEDIUM_DRONES\" type=\"DRONES\" uniqueid=\"d2792f7b-64b7-4f61-9216-d0fe2a79efea\">\n" + 
				"         <generatedUUIDs>\n" + 
				"            <uuid>weapon_mount_standard|81206010-d673-493a-8c1c-79f78c780c49</uuid>\n" + 
				"         </generatedUUIDs>\n" + 
				"      </item>\n" + 
				"      <item count=\"1\" embedin=\"81206010-d673-493a-8c1c-79f78c780c49\" ref=\"hk_227\" slot=\"VEHICLE_WEAPON\" subtype=\"SUBMACHINE_GUNS\" type=\"ACCESSORY\" uniqueid=\"61ca1bec-6c2f-4a90-9ce8-d939da6bc4f4\">\n" + 
				"         <generatedUUIDs>\n" + 
				"            <uuid>smartgun_system|e7414d08-62e3-44ff-939b-22eb9cdd4b98</uuid>\n" + 
				"         </generatedUUIDs>\n" + 
				"      </item>\n" + 
				"   </gear>\n" + 
				"   <decisions/>\n" + 
				"</sr6char>";
		
		ShadowrunCharacter model = ShadowrunCore.load(xml.getBytes());
		assertEquals(1, model.getItems(false).size());
		assertEquals(6, model.getItemsRecursive(false).size()); // Drone, Mount, Weapon  + Weapon accessories

		CarriedItem drone = model.getItem("gm-nissan_dobermann");
		System.out.println(drone.dump());
		assertNotNull(drone);
		assertNotNull(drone.getUniqueId());
		assertEquals("d2792f7b-64b7-4f61-9216-d0fe2a79efea", drone.getUniqueId().toString());
		assertNotNull("Drone misses slot", drone.getSlot(ItemHook.VEHICLE_BODY));
		assertEquals("Drone misses weapon mount", 1, drone.getSlot(ItemHook.VEHICLE_BODY).getAllEmbeddedItems().size());
		assertEquals("Auto-accessory (mount) has wrong UUID", "81206010-d673-493a-8c1c-79f78c780c49", drone.getSlot(ItemHook.VEHICLE_BODY).getAllEmbeddedItems().get(0).getUniqueId().toString());
		assertNotNull(model.getItem(UUID.fromString("81206010-d673-493a-8c1c-79f78c780c49")));
		CarriedItem mount = model.getItem(UUID.fromString("81206010-d673-493a-8c1c-79f78c780c49"));
		
		AvailableSlot slot = mount.getSlot(ItemHook.VEHICLE_WEAPON);
		assertNotNull("Mount misses slot",slot);
		assertEquals(1, slot.getAllEmbeddedItems().size());
		
		System.out.println("---------------------------------");
		ShadowrunTools.recalculateCharacter(model);
		assertEquals(1, model.getItems(false).size());
		assertEquals(6, model.getItemsRecursive(false).size()); // Drone, Mount, Weapon + Weapon accessories
		mount = model.getItem(UUID.fromString("81206010-d673-493a-8c1c-79f78c780c49"));
		slot = mount.getSlot(ItemHook.VEHICLE_WEAPON);
		assertEquals("Slot content duplicated", 1, slot.getAllEmbeddedItems().size());
		
		slot = drone.getSlot(ItemHook.VEHICLE_BODY);
		assertNotNull(slot);
		assertEquals(0, slot.getUsedCapacity());
		assertEquals(slot.getFreeCapacity(), slot.getCapacity());
	}

	//-------------------------------------------------------------------
	@Test
	public void loadAndSave() throws IOException {
		ShadowrunCharacter model = ShadowrunCore.load(ClassLoader.getSystemResourceAsStream("Rigger.xml"));
		
		CarriedItem eye = model.getItem(UUID.fromString("41f0355e-42fc-453a-aeb6-d7f9d5434cad"));
		assertNotNull(eye);
		assertEquals(2,eye.getSlots().size());
		AvailableSlot implants = eye.getSlot(ItemHook.CYBEREYE_IMPLANT);
		assertNotNull(implants);
		assertEquals(6, implants.getAllEmbeddedItems().size());

		ShadowrunTools.recalculateCharacter(model);
		ShadowrunTools.recalculateCharacter(model);
		
		eye = model.getItem(UUID.fromString("41f0355e-42fc-453a-aeb6-d7f9d5434cad"));
		assertNotNull(eye);
		assertEquals(2,eye.getSlots().size());
		implants = eye.getSlot(ItemHook.CYBEREYE_IMPLANT);
		assertNotNull(implants);
		assertEquals(6, implants.getAllEmbeddedItems().size());
		
		try {
			String saved = new String(ShadowrunCore.save(model), "UTF-8");
			System.out.println(saved);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void loadAndSave2() throws IOException {
		ShadowrunCharacter model = ShadowrunCore.load(ClassLoader.getSystemResourceAsStream("Testadept.xml"));
		ShadowrunTools.recalculateCharacter(model);
		ShadowrunTools.recalculateCharacter(model);
		try {
			String saved = new String(ShadowrunCore.save(model), "UTF-8");
			System.out.println(saved);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void loadAndSave3() throws IOException {
		ShadowrunCharacter model = ShadowrunCore.load(ClassLoader.getSystemResourceAsStream("Vercybert.xml"));
		ShadowrunTools.recalculateCharacter(model);
		ShadowrunTools.recalculateCharacter(model);
		try {
			String saved = new String(ShadowrunCore.save(model), "UTF-8");
			System.out.println(saved);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.ShadowrunTools.PoolCalculation;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.AttributeModification.ModificationType;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;

import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.SkillModification;

/**
 * @author prelle
 *
 */
public class PoolCalculationTest {

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	//-------------------------------------------------------------------
	private void prepareAttribute(ShadowrunCharacter model) {
		AttributeValue aVal = model.getAttribute(Attribute.AGILITY);
		assertNotNull(aVal);
		aVal.setPoints(5);
		
		aVal.addModification(new AttributeModification(ModificationValueType.MAX, Attribute.AGILITY, 6)); // Should be ignored
		aVal.addModification(new AttributeModification(ModificationValueType.ALTERNATE, Attribute.AGILITY, 3)); // Should be ignored
		// WARNING: all items and powers are without sense here - they are merely used for type detection
		aVal.addModification( (new AttributeModification(ModificationValueType.NATURAL, Attribute.AGILITY, 2).setConditional(true))); // Should be ignored, because conditional
		assertEquals(0, aVal.getModifier());
		aVal.addModification(new AttributeModification(ModificationValueType.AUGMENTED, Attribute.AGILITY, 3, ModificationType.RELATIVE, ShadowrunCore.getAdeptPower("improved_reflexes"))); // Should be used
		assertEquals(3, aVal.getModifier());
		aVal.addModification(new AttributeModification(ModificationValueType.AUGMENTED, Attribute.AGILITY, 2, ModificationType.RELATIVE, ShadowrunCore.getItem("muscle_toner"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, aVal.getModifier());
		aVal.addModification(new AttributeModification(ModificationValueType.AUGMENTED, Attribute.AGILITY, 2, ModificationType.RELATIVE, ShadowrunCore.getAdeptPower("kinesics"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, aVal.getModifier());
	}

	//-------------------------------------------------------------------
	@Test
	public void testAttribute() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		
		List<PoolCalculation> list = ShadowrunTools.getAttributePoolCalculation(model, Attribute.AGILITY);
		assertEquals(9, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	private SkillValue prepareSkill(ShadowrunCharacter model) {
		Skill combat = ShadowrunCore.getSkill("close_combat");
		SkillValue sVal = new SkillValue(combat, 3);
		assertNotNull(sVal);
		model.addSkill(sVal);
		
		sVal.addModification(new SkillModification(ModificationValueType.MAX, combat, 6));
		assertEquals(0, sVal.getModifier());
		// WARNING: all items and powers are without sense here - they are merely used for type detection
		sVal.addModification( (new SkillModification(ModificationValueType.NATURAL, combat, 2).setConditional(true))); // Should be ignored, because conditional
		assertEquals(0, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 3, ShadowrunCore.getAdeptPower("improved_reflexes"))); // Should be used
		assertEquals(3, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 2, ShadowrunCore.getItem("muscle_toner"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 2, ShadowrunCore.getAdeptPower("kinesics"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, sVal.getModifier());
		
		return sVal;
	}

	//-------------------------------------------------------------------
	@Test
	public void testSkill() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		prepareSkill(model);
		Skill combat = ShadowrunCore.getSkill("close_combat");
		
		List<PoolCalculation> list = ShadowrunTools.getSkillPoolCalculation(model, combat, Attribute.AGILITY);
		System.out.println("testSkill: "+list);
		assertEquals(16, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSkillMissingSpecialization() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		SkillValue sVal = prepareSkill(model);
		Skill combat = sVal.getModifyable();
		
		List<PoolCalculation> list = ShadowrunTools.getSkillPoolCalculation(model, combat, Attribute.AGILITY, "unarmed","clubs");
		System.out.println("testSkillMissingSpecialization: "+list);
		assertEquals(16, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSkillSpecialization() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		SkillValue sVal = prepareSkill(model);
		Skill combat = sVal.getModifyable();
		sVal.addSpecialization(new SkillSpecializationValue(combat.getSpecialization("blades")));
		
		List<PoolCalculation> list = ShadowrunTools.getSkillPoolCalculation(model, combat, Attribute.AGILITY, "unarmed","blades");
		System.out.println("testSkillSpecialization: "+list);
		assertEquals(18, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponPoolWithoutSpec() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		prepareSkill(model);
		CarriedItem katana = new CarriedItem(ShadowrunCore.getItem("katana"));
		
		List<PoolCalculation> list = ShadowrunTools.getWeaponPoolCalculation(model, katana);
		System.out.println("testWeaponPoolWithoutSpec: "+list);
		assertEquals(16, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponPoolWithWrongSpec() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		SkillValue sVal = prepareSkill(model);
		Skill combat = sVal.getModifyable();
		sVal.addSpecialization(new SkillSpecializationValue(combat.getSpecialization("clubs")));
		CarriedItem katana = new CarriedItem(ShadowrunCore.getItem("katana"));
		
		List<PoolCalculation> list = ShadowrunTools.getWeaponPoolCalculation(model, katana);
		System.out.println("testWeaponPoolWithWrongSpec: "+list);
		assertEquals(16, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponPoolWithSpec() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		SkillValue sVal = prepareSkill(model);
		Skill combat = sVal.getModifyable();
		sVal.addSpecialization(new SkillSpecializationValue(combat.getSpecialization("blades")));
		CarriedItem katana = new CarriedItem(ShadowrunCore.getItem("katana"));
		
		List<PoolCalculation> list = ShadowrunTools.getWeaponPoolCalculation(model, katana);
		System.out.println("testWeaponPoolWithSpec: "+list);
		assertEquals(18, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponExoticWithoutSkill() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		SkillValue sVal = prepareSkill(model);
		CarriedItem katana = new CarriedItem(ShadowrunCore.getItem("monofilament_whip"));
		
		List<PoolCalculation> list = ShadowrunTools.getWeaponPoolCalculation(model, katana);
		System.out.println("testWeaponExoticWithoutSkill: "+list);
		assertEquals(0, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponExoticWithSkill() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		Skill combat = ShadowrunCore.getSkill("exotic_weapons");
		SkillValue sVal = new SkillValue(combat, 3);
		assertNotNull(sVal);
		model.addSkill(sVal);
		
		sVal.addModification(new SkillModification(ModificationValueType.MAX, combat, 6));
		assertEquals(0, sVal.getModifier());
		// WARNING: all items and powers are without sense here - they are merely used for type detection
		sVal.addModification( (new SkillModification(ModificationValueType.NATURAL, combat, 2).setConditional(true))); // Should be ignored, because conditional
		assertEquals(0, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 3, ShadowrunCore.getAdeptPower("improved_reflexes"))); // Should be used
		assertEquals(3, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 2, ShadowrunCore.getItem("muscle_toner"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 2, ShadowrunCore.getAdeptPower("kinesics"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, sVal.getModifier());

		CarriedItem katana = new CarriedItem(ShadowrunCore.getItem("monofilament_whip"));
		
		List<PoolCalculation> list = ShadowrunTools.getWeaponPoolCalculation(model, katana);
		System.out.println("testWeaponExoticWithSkill: "+list);
		assertEquals(0, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWeaponExoticWithSkillAndSpec() throws IOException {
		ShadowrunCharacter model = new ShadowrunCharacter();
		prepareAttribute(model);
		Skill combat = ShadowrunCore.getSkill("exotic_weapons");
		SkillValue sVal = new SkillValue(combat, 3);
		assertNotNull(sVal);
		model.addSkill(sVal);
		sVal.addSpecialization(new SkillSpecializationValue(combat.getSpecialization("monofilament_whip")));
		
		sVal.addModification(new SkillModification(ModificationValueType.MAX, combat, 6));
		assertEquals(0, sVal.getModifier());
		// WARNING: all items and powers are without sense here - they are merely used for type detection
		sVal.addModification( (new SkillModification(ModificationValueType.NATURAL, combat, 2).setConditional(true))); // Should be ignored, because conditional
		assertEquals(0, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 3, ShadowrunCore.getAdeptPower("improved_reflexes"))); // Should be used
		assertEquals(3, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 2, ShadowrunCore.getItem("muscle_toner"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, sVal.getModifier());
		sVal.addModification(new SkillModification(ModificationValueType.AUGMENTED, combat, 2, ShadowrunCore.getAdeptPower("kinesics"))); // Should be used with 1 point because of augmentation max
		assertEquals(4, sVal.getModifier());

		CarriedItem katana = new CarriedItem(ShadowrunCore.getItem("monofilament_whip"));
		
		List<PoolCalculation> list = ShadowrunTools.getWeaponPoolCalculation(model, katana);
		System.out.println("testWeaponExoticWithSkillAndSpec: "+list);
		assertEquals(18, list.stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum());
	}
	
}

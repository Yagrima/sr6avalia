/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.levelling.CharacterLeveller;
import org.prelle.shadowrun6.levelling.KarmaAttributeLeveller;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.AttributeModification.ModificationType;
import org.prelle.shadowrun6.modifications.ModificationValueType;

import de.rpgframework.ConfigChangeListener;
import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigNode;
import de.rpgframework.ConfigOption;
import de.rpgframework.ConfigOption.Type;
import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class AttributeLevellingTest {

	private CharacterLeveller charGen;
	private KarmaAttributeLeveller control; 
	private ShadowrunCharacter model;
	
	private List<Modification> unitTestMods;
	
	private static ConfigContainer cfgShadowrun;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
		
		cfgShadowrun = new ConfigContainerImpl(Preferences.userRoot().node("unittest"), "shadowrun");
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	private void applyUnitTestModifications(AttributeValue val) {
		unitTestMods.forEach(mod -> val.addModification(mod));
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		GenerationEventDispatcher.clear();
		model = new ShadowrunCharacter();
		charGen = new CharacterLeveller(model);
		charGen.attachConfigurationTree(cfgShadowrun);
		control = (KarmaAttributeLeveller) charGen.getAttributeController();
		
		unitTestMods = new ArrayList<>();
		unitTestMods.add(new AttributeModification(ModificationValueType.MAX, Attribute.BODY, 7));
		// There is an augmentation that granted one point
		unitTestMods.add(new AttributeModification(ModificationValueType.AUGMENTED, Attribute.BODY, 1, ModificationType.RELATIVE, new CarriedItem(ShadowrunCore.getItem("orthoskin"))));

		
		// Body set to 5 (started at 4), maximum is 7
		AttributeValue val = model.getAttribute(Attribute.BODY);
		val.setStart(4);
		val.setPoints(5);
		applyUnitTestModifications(val);
		// Track that character one increased attribute
		AttributeModification histMod = new AttributeModification(ModificationValueType.NATURAL, Attribute.BODY, 5, ModificationType.ABSOLUTE);
		histMod.setExpCost(25);
		model.addToHistory(histMod);
		
		assertEquals(1, model.getHistory().size());
		assertTrue(model.getHistory().get(0) instanceof AttributeModification);
		assertEquals(5, ((AttributeModification)model.getHistory().get(0)).getValue() );
		assertEquals("Invalid Karma cost in history", 25, ((AttributeModification)model.getHistory().get(0)).getExpCost() );
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() throws IOException {
		AttributeValue val = model.getAttribute(Attribute.BODY);
		assertEquals(4, val.getStart());
		assertEquals(5, val.getPoints());
		assertEquals(1, val.getAugmentedModifier());
		assertEquals(6, val.getAugmentedValue());
		assertEquals(6, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void normalIncrease() throws IOException {
		// Grant not enough karma to increase
		model.setKarmaFree(15);
		model.setKarmaInvested(1);
		
		assertFalse("Increase possible although not enough Karma", control.canBeIncreased(Attribute.BODY));
		assertFalse("Increase possible although not enough Karma", control.increase(Attribute.BODY));
		// Need 30 karma
		model.setKarmaFree(35);
		assertTrue("Increase not possible although enough Karma", control.canBeIncreased(Attribute.BODY));
		assertTrue("Increase not possible although enough Karma", control.increase(Attribute.BODY));
		assertEquals(5, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(2, model.getHistory().size());
		assertTrue(model.getHistory().get(1) instanceof AttributeModification);
		assertEquals(6, ((AttributeModification)model.getHistory().get(1)).getValue() );
		assertEquals("Invalid Karma cost in history", 30, ((AttributeModification)model.getHistory().get(1)).getExpCost() );
		assertEquals(ModificationType.ABSOLUTE, ((AttributeModification)model.getHistory().get(1)).getModificationType());
		
		AttributeValue val = model.getAttribute(Attribute.BODY);
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(6, val.getPoints());
		assertEquals(7, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void normalDecrease() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.canBeDecreased(Attribute.BODY));
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.decrease(Attribute.BODY));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(40, model.getKarmaFree());
		assertEquals(6, model.getKarmaInvested());
		assertEquals(0, model.getHistory().size());
		
		AttributeValue val = model.getAttribute(Attribute.BODY);
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(4, val.getPoints());
		assertEquals(5, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseWithoutRefund() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(true);
		control.setRefundBelowCreation(false);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.canBeDecreased(Attribute.BODY));
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.decrease(Attribute.BODY));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(15, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(1, model.getHistory().size());
		
		AttributeValue val = model.getAttribute(Attribute.BODY);
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(4, val.getPoints());
		assertEquals(5, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
		
		assertFalse("Decreasing below creation should not be allowed", control.canBeDecreased(Attribute.BODY));
		assertFalse("Decreasing below creation should not be allowed", control.decrease(Attribute.BODY));
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseBelowCreationWithoutRefundAtAll() throws IOException {
		control.setDecreaseBelowCreation(true);
		control.setDontRefund(true);
		control.setRefundBelowCreation(false);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		control.decrease(Attribute.BODY);
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.canBeDecreased(Attribute.BODY));
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.decrease(Attribute.BODY));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(15, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(1, model.getHistory().size());
		
		AttributeValue val = model.getAttribute(Attribute.BODY);
		applyUnitTestModifications(val);
		assertEquals(3, val.getPoints());
		assertEquals(4, val.getStart());
		assertEquals(4, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseBelowCreationWithoutRefundBelowCreation() throws IOException {
		control.setDecreaseBelowCreation(true);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		control.decrease(Attribute.BODY);
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.canBeDecreased(Attribute.BODY));
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.decrease(Attribute.BODY));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(15, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(1, model.getHistory().size());
		
		AttributeValue val = model.getAttribute(Attribute.BODY);
		applyUnitTestModifications(val);
		assertEquals(3, val.getPoints());
		assertEquals(4, val.getStart());
		assertEquals(4, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseBelowCreationWithRefund() throws IOException {
		control.setDecreaseBelowCreation(true);
		control.setDontRefund(false);
		control.setRefundBelowCreation(true);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		assertEquals(15, model.getKarmaFree());
		control.decrease(Attribute.BODY);
		assertEquals(0, model.getHistory().size());
		assertEquals(40, model.getKarmaFree());
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.canBeDecreased(Attribute.BODY));
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.decrease(Attribute.BODY));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(60, model.getKarmaFree());
		assertEquals(1, model.getHistory().size());
		assertEquals(-14, model.getKarmaInvested());
		assertEquals("Missing modification for attribute decrease below creation",1, model.getHistory().size());
		assertTrue(model.getHistory().get(0) instanceof AttributeModification);
		assertEquals(3, ((AttributeModification)model.getHistory().get(0)).getValue() );
		assertEquals("Invalid Karma cost in history", -20, ((AttributeModification)model.getHistory().get(0)).getExpCost() );
		assertEquals(ModificationType.ABSOLUTE, ((AttributeModification)model.getHistory().get(0)).getModificationType());
		
		AttributeValue val = model.getAttribute(Attribute.BODY);
		applyUnitTestModifications(val);
		assertEquals(3, val.getPoints());
		assertEquals(4, val.getStart());
		assertEquals(4, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void increaseStopsAtMax() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		model.setKarmaFree(55);
		model.setKarmaInvested(31);
		AttributeValue val = model.getAttribute(Attribute.BODY);
		val.setPoints(6);
		applyUnitTestModifications(val);
		assertTrue("Increase impossible although enough Karma", control.canBeIncreased(Attribute.BODY));
		val.setPoints(7);
		
		assertFalse("Increase possible above max", control.canBeIncreased(Attribute.BODY));
		assertFalse("Increase possible above max", control.increase(Attribute.BODY));
		assertEquals(4, val.getStart());
		assertEquals(7, val.getPoints());
		assertEquals(1, val.getAugmentedModifier());
		assertEquals(8, val.getModifiedValue());
		assertEquals(7, val.getMaximum());
	}
	
}

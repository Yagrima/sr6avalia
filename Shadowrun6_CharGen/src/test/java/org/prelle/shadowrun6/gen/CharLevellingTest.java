/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CharLevellingTest {

	private CharacterLeveller charGen;
	private ShadowrunCharacter model;
	
	private List<Modification> unitTestMods;
	
	private static Skill CON;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
		CON = ShadowrunCore.getSkill("con");
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	private void applyUnitTestModifications(SkillValue val) {
		unitTestMods.forEach(mod -> val.addModification(mod));
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		GenerationEventDispatcher.clear();
		unitTestMods = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() throws IOException {
		model = ShadowrunCore.load(ClassLoader.getSystemResourceAsStream("Nefertari.xml"));;
	
		System.out.println(ShadowrunTools.getInfluences(model));
		
		SpellValue val = model.getSpell("fireball");
		System.out.println(val.getInfluences());
		assertEquals(1, val.getInfluences().size());
		
		charGen = new CharacterLeveller(model);
		System.out.println(val.getInfluences());
		assertEquals(1, val.getInfluences().size());
	}
	
}

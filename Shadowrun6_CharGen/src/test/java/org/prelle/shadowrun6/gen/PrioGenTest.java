/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.LicenseValue;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.MagicOrResonanceOption;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.RewardImpl;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.SIN.Quality;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCharacter.Gender;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.ComplexFormController;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun6.charctrl.LifestyleController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.proc.ApplyAttributeModifications;
import org.prelle.shadowrun6.proc.GetModificationsFromEquipment;
import org.prelle.shadowrun6.proc.GetModificationsFromPowers;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PrioGenTest {

	private NewPriorityCharacterGenerator charGen;
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		model  = new ShadowrunCharacter();
		GenerationEventDispatcher.clear();
		charGen = new NewPriorityCharacterGenerator();
		charGen.attachConfigurationTree(new ConfigContainerImpl(Preferences.userNodeForPackage(PrioGenTest.class), "shadowrun"));
		charGen.start(model);
	}

	//-------------------------------------------------------------------
	/**
	 * Test calculation of augmentations
	 */
	@Test
	public void testAugmentation() {
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("adept"));
		model.getAttribute(Attribute.AGILITY).setPoints(2);
		
		CarriedItem leg1 = new CarriedItem(ShadowrunCore.getItem("cyberleg_obvious"));
		leg1.setName("Left Leg");
		CarriedItem agilityForLeg1 = new CarriedItem(ShadowrunCore.getItem("attribute_increase_agility"),3);
		leg1.addAccessory(ItemHook.CYBERLIMB_IMPLANT, agilityForLeg1);
		System.out.println("Leg1 = "+leg1.getCharacterModifications());
		int agil = 0;
		int agilCount = 0;
		for (Modification mod : leg1.getCharacterModifications()) {
			if (mod instanceof AttributeModification && ((AttributeModification)mod).getAttribute()==Attribute.AGILITY) {
				agil+=((AttributeModification)mod).getValue();
				agilCount++;
			}
		}
		assertEquals("AttributeModifications not combined", 1, agilCount);
		assertEquals("Resulting agility is wrong", 5,agil);
		
		CarriedItem leg2 = new CarriedItem(ShadowrunCore.getItem("cyberleg_obvious"));
		leg2.setName("Right Leg");
		CarriedItem agilityForLeg2 = new CarriedItem(ShadowrunCore.getItem("attribute_increase_agility"),2);
		leg2.addAccessory(ItemHook.CYBERLIMB_IMPLANT, agilityForLeg2);
		System.out.println("Leg2 = "+leg2.getCharacterModifications());
		
		CarriedItem bioware = new CarriedItem(ShadowrunCore.getItem("enhanced_articulation"));

		AdeptPowerValue adeptPower = new AdeptPowerValue(ShadowrunCore.getAdeptPower("improved_physical_attribute"));
		adeptPower.setChoice(Attribute.AGILITY);
		adeptPower.setChoiceReference(Attribute.AGILITY.name());
		
		model.addItem(leg1); // Agility 5
		model.addItem(leg2); // Agility 3
		assertEquals("Missing added item", 2, model.getItems(false).size());
		model.addItem(bioware);
		model.addAdeptPower(adeptPower);
		
		List<Modification> mod = new ArrayList<>();
		mod = (new GetModificationsFromEquipment()).process(model, mod);
		System.out.println("List after items: "+mod);
		mod = (new GetModificationsFromPowers()).process(model, mod);
		System.out.println("List after powers: "+mod);
		mod = (new ApplyAttributeModifications()).process(model, mod);
		System.out.println("List after apply: "+mod);
		
		AttributeValue agility = model.getAttribute(Attribute.AGILITY);
		assertEquals(2, agility.getPoints());
		assertEquals(4, agility.getAugmentedValue()); // 2 +1 from Muscle Toner +1 Improved Physical Attr
	}

	//-------------------------------------------------------------------
	/**
	 * Test calculation of augmentations
	 */
	@Test
	public void testImprovedPhysicalAttribute() {
		charGen.setPriority(PriorityType.MAGIC, Priority.A);
		charGen.setPriority(PriorityType.METATYPE, Priority.B);
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.C);
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("adept"));
		charGen.runProcessors();
		assertEquals(4, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.POWER_POINTS).getModifiedValue());
		
		assertEquals(50, model.getKarmaFree());
		assertEquals(12, charGen.getAttributeController().getPointsLeft());
		assertEquals(11, charGen.getAttributeController().getPointsLeftSpecial());
		model.getAttribute(Attribute.AGILITY).setPoints(6);
		charGen.runProcessors();
		AttributeValue agility = model.getAttribute(Attribute.AGILITY);
		assertEquals(6, agility.getPoints());
		assertEquals(6, agility.getModifiedValue());
		assertEquals(0, agility.getAugmentedModifier());
		assertEquals(6, agility.getAugmentedValue());
		assertEquals(50, model.getKarmaFree());
		assertEquals( 7, charGen.getAttributeController().getPointsLeft()); // From 1->6
		assertEquals(11, charGen.getAttributeController().getPointsLeftSpecial());
		
		assertEquals(4, model.getAttribute(Attribute.POWER_POINTS).getModifiedValue());
		assertEquals(4, charGen.getPowerController().getPowerPointsLeft(), 0f);
		// Add Improved Physical attribute
		assertNotNull(charGen.getPowerController().select(ShadowrunCore.getAdeptPower("improved_physical_attribute"), Attribute.AGILITY));
		assertEquals(4, model.getAttribute(Attribute.POWER_POINTS).getModifiedValue());
		assertEquals(3, charGen.getPowerController().getPowerPointsLeft(), 0f);
		
		assertEquals(50, model.getKarmaFree());
		assertEquals( 7, charGen.getAttributeController().getPointsLeft());
		assertEquals(11, charGen.getAttributeController().getPointsLeftSpecial());
		agility = model.getAttribute(Attribute.AGILITY);
		assertEquals(6, agility.getPoints());
		assertEquals(7, agility.getModifiedValue());
		assertEquals(1, agility.getAugmentedModifier());
		assertEquals(7, agility.getAugmentedValue());
	}

	//-------------------------------------------------------------------
	/**
	 * Test calculation of augmentations
	 */
	@Test
	public void testImprovedAbility() {
		charGen.setPriority(PriorityType.MAGIC, Priority.A);
		charGen.setPriority(PriorityType.METATYPE, Priority.B);
		charGen.setPriority(PriorityType.SKILLS, Priority.C);
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("adept"));
		charGen.runProcessors();
		
		assertEquals(50, model.getKarmaFree());
		assertEquals(20, charGen.getSkillController().getPointsLeftSkills());
		Skill combat = ShadowrunCore.getSkill("close_combat");
		SkillValue skill = charGen.getSkillController().select(combat);
		assertNotNull(skill);
		skill.setPoints(6);
		charGen.runProcessors();
		
		assertEquals(6, skill.getPoints());
		assertEquals(0, skill.getModifier());
		assertEquals(6, skill.getModifiedValue());
		assertEquals(50, model.getKarmaFree());
		assertEquals(14, charGen.getSkillController().getPointsLeftSkills()); // From 0->6
		
		assertEquals(4, model.getAttribute(Attribute.POWER_POINTS).getModifiedValue());
		assertEquals(4, charGen.getPowerController().getPowerPointsLeft(), 0f);
		// Add Improved Physical attribute
		assertNotNull(charGen.getPowerController().select(ShadowrunCore.getAdeptPower("improved_ability_combat"), combat));
		assertEquals(4, model.getAttribute(Attribute.POWER_POINTS).getModifiedValue());
		assertEquals(3, charGen.getPowerController().getPowerPointsLeft(), 0f);
		
		assertEquals(50, model.getKarmaFree());
		assertEquals(14, charGen.getSkillController().getPointsLeftSkills()); // From 0->6
		assertEquals(6, skill.getPoints());
		assertEquals(1, skill.getModifier());
		assertEquals(7, skill.getModifiedValue());
	}

	//-------------------------------------------------------------------
	@Test
	public void generateSloth() {
//		assertEquals(25, model.getKarmaFree());
//
		charGen.setPriority(PriorityType.RESOURCES, Priority.B);
		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.setPriority(PriorityType.MAGIC, Priority.E);
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.A);
		charGen.setPriority(PriorityType.SKILLS, Priority.C);
		assertEquals(50, model.getKarmaFree());
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("troll"));

		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(24, attrib.getPointsLeft());
		assertEquals(4 , attrib.getPointsLeftSpecial());
		assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(3 , attrib.getPointsLeftSpecial());
		
		// Body +4
		for (int i=0; i<7; i++)
			assertTrue(attrib.increase(Attribute.BODY));
		assertEquals(20, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(0 , attrib.getPointsLeftSpecial());
		// Agility 5
		for (int i=0; i<4; i++)
			assertTrue(attrib.increase(Attribute.AGILITY));
		assertEquals(16, attrib.getPointsLeft());
		// Reaction 5
		for (int i=0; i<4; i++)
			assertTrue(attrib.increase(Attribute.REACTION));
		assertEquals(12, attrib.getPointsLeft());
		// Strength 8
		for (int i=0; i<7; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.STRENGTH));
		assertEquals(5, attrib.getPointsLeft());
		// Willpower 3
		for (int i=0; i<2; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.WILLPOWER));
		assertEquals(3, attrib.getPointsLeft());
//		// Logic +2
//		for (int i=0; i<2; i++)
//			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.LOGIC));
		// Intuition +2
		for (int i=0; i<3; i++)
			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.INTUITION));
		// Charisma +2
//		for (int i=0; i<2; i++)
//			assertTrue("Failed inc to "+(i+1),attrib.increase(Attribute.CHARISMA));
		assertEquals(50, model.getKarmaFree());
		assertEquals(0, attrib.getPointsLeft());

		assertEquals(8, model.getAttribute(Attribute.BODY).getModifiedValue());
		assertEquals(5, model.getAttribute(Attribute.AGILITY).getModifiedValue());
		assertEquals(5, model.getAttribute(Attribute.REACTION).getModifiedValue());
		assertEquals(8, model.getAttribute(Attribute.STRENGTH).getModifiedValue());
		assertEquals(3, model.getAttribute(Attribute.WILLPOWER).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.LOGIC).getModifiedValue());
		assertEquals(4, model.getAttribute(Attribute.INTUITION).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.CHARISMA).getModifiedValue());

//		// S.71 Magic and resonance
//		charGen.setPriority(PriorityType.MAGIC, Priority.E);
//		MagicOrResonanceType mType = ShadowrunCore.getMagicOrResonanceType("mundane");
//		for (MagicOrResonanceOption opt : charGen.getMagicOrResonanceController().getAvailable()) {
//			if (opt.getType()==mType)
//				charGen.getMagicOrResonanceController().select(opt);
//		}
		// "mundane" should be autoselected by priority E
		assertNotNull(model.getMagicOrResonanceType());
		assertEquals("mundane", model.getMagicOrResonanceType().getId()); 

		// Skills
//		charGen.setPriority(PriorityType.SKILLS, Priority.C);
		SkillController skills = charGen.getSkillController();
		assertEquals(1, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(20, skills.getPointsLeftSkills());
//		SkillGroupValue sgVal = skills.select(ShadowrunCore.getSkillGroup("ATHLETICS"));
//		assertNotNull("Could not select skillgroup", sgVal);
//		assertTrue(skills.increase(sgVal));

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("athletics"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3

		sVal = skills.select(ShadowrunCore.getSkill("stealth"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3

		sVal = skills.select(ShadowrunCore.getSkill("close_combat"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		skills.select(sVal, ShadowrunCore.getSkill("close_combat").getSpecialization("blades"), false);

		sVal = skills.select(ShadowrunCore.getSkill("firearms"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertNotNull("Could not select skill", sVal);
		assertTrue(skills.increase(sVal)); // 2

		sVal = skills.select(ShadowrunCore.getSkill("astral"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("influence"));
		assertNotNull("Could not select skill", sVal);

		sVal = skills.select(ShadowrunCore.getSkill("piloting"));
		assertNotNull("Could not select skill", sVal);

		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(1, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("German");
		assertTrue(skills.canBeSelected(ShadowrunCore.getSkill("knowledge")));
		SkillValue knowledge = skills.select(ShadowrunCore.getSkill("knowledge"));
		assertNotNull(knowledge);
		knowledge.setName("Saeder-Krupp");
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());

		assertEquals(3, model.getSkillValue(ShadowrunCore.getSkill("athletics")).getModifiedValue());
		assertEquals(3, model.getSkillValue(ShadowrunCore.getSkill("stealth")).getModifiedValue());
		assertEquals(4, model.getSkillValue(ShadowrunCore.getSkill("close_combat")).getModifiedValue()); // 1 point spent for specialization

		/*
		 * Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		assertEquals(275000, model.getNuyen());
		assertEquals(45, model.getKarmaFree());
		for (int i=0; i<28; i++) {
			ctrl.increaseBoughtNuyen();
		}
//		assertEquals(70000, model.getNuyen());
		// SHopping
		ctrl.select(ShadowrunCore.getItem("fn_har"));
		SIN sin = charGen.getSINController().createNewSIN("Fake 1", Quality.SUPERFICIALLY_PLAUSIBLE);
//		assertEquals(58500, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("colt_cobra_tz120"));
		LicenseValue license = charGen.getSINController().createNewLicense(ShadowrunCore.getLicenseType("firearms"), sin, Quality.SUPERFICIALLY_PLAUSIBLE);
		license.setSIN(sin.getUniqueId());
//		assertEquals(57040, model.getNuyen());

		ctrl.select(ShadowrunCore.getItem("spurs_retractable_augment"));
		ctrl.select(ShadowrunCore.getItem("ares_predator_vi"));
		ctrl.select(ShadowrunCore.getItem("credstick_silver"));
//		assertEquals(56295, model.getNuyen());
		ctrl.select(ShadowrunCore.getItem("katana"));
		ctrl.select(ShadowrunCore.getItem("renraku_sensei"));
//		assertEquals(54295, model.getNuyen());
//		ctrl.select(ShadowrunCore.getItem("knucks"));
//		ctrl.select(ShadowrunCore.getItem("high_explosive"), new SelectionOption(SelectionOptionType.AMOUNT, 3));
//		assertEquals(53895, model.getNuyen());

		CarriedItem jacket = ctrl.select(ShadowrunCore.getItem("armor_jacket"));
		assertEquals(1000, jacket.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.embed(jacket, ShadowrunCore.getItem("electricity_resistance"), new SelectionOption(SelectionOptionType.RATING, 4));
		assertEquals(2000, jacket.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		ctrl.select(ShadowrunCore.getItem("explosive_rounds"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
//		assertEquals(51495, model.getNuyen());

		assertNotNull(ctrl.select(ShadowrunCore.getItem("synaptic_booster"), new SelectionOption(SelectionOptionType.RATING, 3)));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("reflex_recorder"), new SelectionOption(SelectionOptionType.SKILL, ShadowrunCore.getSkill("firearms"))));
//		assertEquals(25355, model.getNuyen());

		assertNotNull(ctrl.select(ShadowrunCore.getItem("ammo_holdout_light_machine"), 
				new SelectionOption(SelectionOptionType.AMOUNT, 30),
				new SelectionOption(SelectionOptionType.AMMOTYPE, ShadowrunCore.getAmmoType("regular"))));
		ctrl.select(ShadowrunCore.getItem("spare_clip"), new SelectionOption(SelectionOptionType.AMOUNT, 9));
//		assertEquals(24710, model.getNuyen());

//		ctrl.select(ShadowrunCore.getItem("flash-bang"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
//		assertEquals(24210, model.getNuyen());
		LifestyleValue life = new LifestyleValue(ShadowrunCore.getLifestyle("low"));
		life.setPrimary(true);
		life.setName("Appartment with chummer");
		life.setPaidMonths(3);
		charGen.getLifestyleController().addLifestyle(life);
		assertEquals(6000, charGen.getLifestyleController().getLifestyleCost(life));
//		assertEquals(12210, model.getNuyen());

		CarriedItem contacts = ctrl.select(ShadowrunCore.getItem("contacts"), new SelectionOption(SelectionOptionType.RATING, 3));
		assertNotNull(contacts);
		assertEquals(600, contacts.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		ctrl.embed(contacts, ShadowrunCore.getItem("flare_compensation"));
		ctrl.embed(contacts, ShadowrunCore.getItem("smartlink"));
		assertEquals(2850, contacts.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		ctrl.select(ShadowrunCore.getItem("kompositknochenKunststoff"));
//		assertEquals(2810, model.getNuyen());

//		skills = charGen.getSkillController();
//
//
//

		/*
		 * Qualities
		 */
		QualityController qualities = charGen.getQualityController();
		assertEquals(17, model.getKarmaFree());
		qualities.select(ShadowrunCore.getQuality("exceptional_attribute"), Attribute.BODY);
		assertEquals(5, model.getKarmaFree());
//		assertTrue(attrib.canBeIncreased(Attribute.BODY));
//		assertTrue(attrib.increase(Attribute.BODY));
//		assertTrue(attrib.increase(Attribute.WILLPOWER));
//		assertTrue(attrib.increase(Attribute.CHARISMA));
//		assertNull(qualities.select(ShadowrunCore.getQuality("exceptional_attribute"), Attribute.AGILITY));
////		// S.97 more Qualities
//		assertEquals(5, model.getKarmaFree());
//		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("spirit_bane"),"spirit_of_air")!=null);
//		assertEquals(17, model.getKarmaFree());
//		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("quick_healer"))!=null);
//		assertEquals(9, model.getKarmaFree());
//		assertEquals(18, model.getAttribute(Attribute.DAMAGE_OVERFLOW).getModifiedValue());
//		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("will_to_live"))!=null);
//		assertEquals(19, model.getAttribute(Attribute.DAMAGE_OVERFLOW).getModifiedValue());
//		assertEquals(1, model.getKarmaFree());
//		assertEquals(0, model.getKarmaFree());
//		assertTrue(charGen.getQualityController().select(ShadowrunCore.getQuality("bad_rep"))!=null);
//		assertEquals(7, model.getKarmaFree());
//		QualityValue qVal = charGen.getQualityController().select(ShadowrunCore.getQuality("gremlins"));
//		assertTrue(qVal!=null);
//		assertTrue("Failed increasing "+qVal,charGen.getQualityController().increase(qVal));
//		assertEquals(15, model.getKarmaFree());
//
//
////		assertEquals(16, model.getKarmaFree());
//
////		CarriedItem item = new CarriedItem(ShadowrunCore.getItem("combat_axe"));
////		model.addItem(item);
////		item = new CarriedItem(ShadowrunCore.getItem("ares_predator_v"));
////		model.addItem(item);
////
////
////		ItemTemplate tmpImaging = ShadowrunCore.getItem("imaging_scope");
////		ItemTemplate tmpSilencer= ShadowrunCore.getItem("ares_lf_silencer");
////		ItemTemplate tmpLowLight= ShadowrunCore.getItem("low_light_vision");
////
////		CarriedItem aresLF75 = ctrl.select(ShadowrunCore.getItem("ares_light_fire_75"));
////
////		System.out.println("Dump Ares Light Fire 75");
////		System.out.println("Weapon: "+aresLF75.getName());
////		for (AvailableSlot slot : aresLF75.getSlots()) {
////			for (CarriedItem accessory : slot.getEmbeddedItems()) {
////				System.out.println("- "+accessory.getName()+" \t("+slot.getSlot().getName()+")");
////				// WiFi advantages
////				for (String wifi : accessory.getWiFiAdvantageStrings()) {
////					System.out.println("    WIFI: "+wifi);
////				}
////			}
////		}
////
////		// What is already included
////		Collection<CarriedItem> access = aresLF75.getAccessories();
////		assertNotNull(access);
////		boolean found = false;
////		for (CarriedItem val : access) {
////			if (val.getItem()==tmpSilencer)
////				found=true;
////		}
////		assertTrue("Included silencer not found", found);
////		// INTERNAL and BARREL and UNDER are not available
////		assertEquals(ctrl.getEmbeddableIn(aresLF75, ItemHook.INTERNAL).toString(), 5, ctrl.getEmbeddableIn(aresLF75, ItemHook.INTERNAL).size());
//////		assertEquals(ctrl.getEmbeddableIn(aresLF75, ItemHook.BARREL).toString(), 7, ctrl.getEmbeddableIn(aresLF75, ItemHook.BARREL).size());
////		assertEquals(ctrl.getEmbeddableIn(aresLF75, ItemHook.UNDER).toString(), 0, ctrl.getEmbeddableIn(aresLF75, ItemHook.UNDER).size());
////		// There should be options for TOP slot
////		List<ItemTemplate> opt = ctrl.getEmbeddableIn(aresLF75, ItemHook.TOP);
////		assertFalse(opt.isEmpty());
////		assertTrue(opt.contains(tmpImaging));
////
////		// Add imaging
////		assertTrue(ctrl.canBeEmbedded(aresLF75, tmpImaging, ItemHook.TOP));
////		CarriedItem imaging = ctrl.embed(aresLF75, tmpImaging);
////		System.out.println("Embedding = "+imaging);
////		found = false;
////		for (CarriedItem val : aresLF75.getAccessories()) {
////			if (val.getItem()==tmpImaging)
////				found=true;
////		}
////		assertTrue("Embedded imaging scope not found", found);
//
//		// S.99 remaining karma
//		charGen.setKarmaOptimization(false);
//		assertEquals(15, model.getKarmaFree());
//		assertTrue(skills.increase(model.getSkillValue(ShadowrunCore.getSkill("perception")))); // Perception at 2 for 4 Karma
////		assertEquals(11, model.getKarmaFree());
//		assertTrue(skills.increase(model.getSkillValue(ShadowrunCore.getSkill("heavyweapons")))); // Heavy weapons at 2 for 4 Karma
////		assertEquals(7, model.getKarmaFree());
//		assertTrue(skills.increase(model.getSkillValue(ShadowrunCore.getSkill("first_aid")))); // First aid at 3 for 6 Karma
//		assertEquals(1, model.getKarmaFree());
//

		// S.99 Connections
		Connection schieber = charGen.getConnectionController().createConnection();
		schieber.setType("Schieber");
		schieber.setName("Fox");;
		assertEquals(1, schieber.getInfluence());
		assertEquals(1, schieber.getLoyalty());
//		assertTrue(charGen.getConnectionController().increaseInfluence(schieber));
//		assertTrue(charGen.getConnectionController().increaseLoyalty(schieber));
//		assertEquals(2, schieber.getInfluence());
//		assertEquals(2, schieber.getLoyalty());
		Connection doc = charGen.getConnectionController().createConnection();
		doc.setType("Straßendoc");
		doc.setName("Tupfer");
//		charGen.getConnectionController().increaseInfluence(doc);
//		charGen.getConnectionController().increaseLoyalty(doc);
//		assertEquals(1, charGen.getConnectionController().getToDos().size());

		model.setRealName("John Doe");
		model.setName("Sloth");
		model.setGender(Gender.MALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(250);
		model.setWeight(160);

		// Finalize
		charGen.stop();

		model.addReward(new RewardImpl("battleRoyale", 3, "Battle Royale", new Date(System.currentTimeMillis()), 2500));
		model.setKarmaFree(3);
		
		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateCovertOps() {
	}

	//-------------------------------------------------------------------
	@Test
	public void generateCombatMage() {
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.A);
		charGen.setPriority(PriorityType.METATYPE, Priority.B);
		charGen.setPriority(PriorityType.MAGIC, Priority.C);
		charGen.setPriority(PriorityType.SKILLS, Priority.D);
		charGen.setPriority(PriorityType.RESOURCES, Priority.E);
		assertEquals(50, model.getKarmaFree());
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("ork"));

		// Select Magician
		assertNotNull(model.getMagicOrResonanceType());
		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
			if (opt.getType().getId().equals("magician"))
				charGen.getMagicOrResonanceController().select(opt);
		}
		assertEquals(4, charGen.getSpellController().getSpellsLeft());

		/*
		 * Qualities
		 */
		assertEquals(50, model.getKarmaFree());
		QualityController qualities = charGen.getQualityController();
		qualities.select(ShadowrunCore.getQuality("aptitude"), ShadowrunCore.getSkill("sorcery"));
		assertEquals(38, model.getKarmaFree());
		QualityValue qVal = qualities.select(ShadowrunCore.getQuality("allergy_common_mild"), "Mild allergy against grass");
		assertEquals(49, model.getKarmaFree());
		qualities.select(ShadowrunCore.getQuality("ar_vertigo"));
		assertEquals(59, model.getKarmaFree());
		assertNotNull(qualities.select(ShadowrunCore.getQuality("astral_beacon")));
		assertEquals(69, model.getKarmaFree());
		qVal = qualities.select(ShadowrunCore.getQuality("focused_concentration"));
		qualities.increase(qVal);
		assertEquals(45, model.getKarmaFree());
		qualities.select(ShadowrunCore.getQuality("spirit_bane"), "spirit_of_water");
		assertEquals(57, model.getKarmaFree());
		
		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(24, attrib.getPointsLeft());
		assertEquals(11, attrib.getPointsLeftSpecial());
		assertTrue(attrib.increase(Attribute.EDGE));
		assertTrue(attrib.increase(Attribute.EDGE));
		assertTrue(attrib.increase(Attribute.EDGE));
		assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(7 , attrib.getPointsLeftSpecial());
		
		// Body +5 = 6
		for (int i=0; i<5; i++) assertTrue(attrib.increase(Attribute.BODY));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.AGILITY));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.REACTION));
		for (int i=0; i<6; i++) assertTrue(attrib.increase(Attribute.STRENGTH));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.WILLPOWER));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.LOGIC));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.INTUITION));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.CHARISMA));
		
		// Magic or resonance
		assertEquals("magician", model.getMagicOrResonanceType().getId()); 
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.MAGIC));
		assertEquals(57, model.getKarmaFree());
		assertEquals(0, attrib.getPointsLeft());
		assertEquals(0 , attrib.getPointsLeftSpecial());
		

		// Skills
		SkillController skills = charGen.getSkillController();
		assertEquals(3, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(16, skills.getPointsLeftSkills());
//		SkillGroupValue sgVal = skills.select(ShadowrunCore.getSkillGroup("ATHLETICS"));
//		assertNotNull("Could not select skillgroup", sgVal);
//		assertTrue(skills.increase(sgVal));

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("close_combat"));
		
		sVal = skills.select(ShadowrunCore.getSkill("conjuring"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		skills.select(sVal, ShadowrunCore.getSkill("conjuring").getSpecialization("summoning"), false);

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertTrue(skills.increase(sVal)); // 2

		sVal = skills.select(ShadowrunCore.getSkill("sorcery"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		assertTrue(skills.increase(sVal)); // 6
		assertTrue(skills.increase(sVal)); // 7

		assertNotNull(skills.select(ShadowrunCore.getSkill("stealth")));

		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(3, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("English");
		assertTrue(skills.canBeSelected(ShadowrunCore.getSkill("knowledge")));
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Magical History") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Wizzer Gangs") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Ork Culture") );
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());

		/*
		 * Spells
		 */
		assertEquals(52, model.getKarmaFree());
		SpellController spells = charGen.getSpellController();
		model.setTradition(ShadowrunCore.getTradition("hermetic"));
		spells.select(ShadowrunCore.getSpell("armor"), false);
		spells.select(ShadowrunCore.getSpell("confusion"), false);
		spells.select(ShadowrunCore.getSpell("detect_enemies"), false);
		spells.select(ShadowrunCore.getSpell("flamestrike"), false);
		spells.select(ShadowrunCore.getSpell("fireball"), false);
		spells.select(ShadowrunCore.getSpell("heal"), false);
		spells.select(ShadowrunCore.getSpell("ice_spear"), false);
		spells.select(ShadowrunCore.getSpell("ice_storm"), false);
		spells.select(ShadowrunCore.getSpell("levitate"), false);
		spells.select(ShadowrunCore.getSpell("physical_barrier"), false);

		/*
		 * Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		for (int i=0; i<11; i++)
			assertTrue("Failed increasing Nuyen", ctrl.increaseBoughtNuyen());
		ctrl.select(ShadowrunCore.getItem("armor_jacket"));
		CarriedItem item = ctrl.select(ShadowrunCore.getItem("renraku_sensei"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("trodes")));
		ctrl.select(ShadowrunCore.getItem("shock_gloves"));
		assertNotNull("Could not buy",ctrl.select(ShadowrunCore.getItem("chrysler-nissan_jackrabbit")));
		assertNotNull("Could not buy",ctrl.select(ShadowrunCore.getItem("reagents_per_dram"), new SelectionOption(SelectionOptionType.AMOUNT, 20)));
		
		// SINs
		SINController sins = charGen.getSINController();
//		sins.canCreateNewLicense(Quality.ANYONE, 3);
		SIN[] sins2 = sins.createNewSIN("NO Name", Quality.ANYONE, 3);
		sins2[0].setName("Ruth Rentpayer");
		
		// Lifestyles
		SingleLifestyleBuilder lsBuilder = new SingleLifestyleGenerator(model);
		lsBuilder.selectLifestyle(ShadowrunCore.getLifestyle("low"));
		lsBuilder.selectName("You don't wanna live there");
		lsBuilder.selectSIN(sins2[0]);
		
		LifestyleController lifestyles = charGen.getLifestyleController();
		lifestyles.addLifestyle(lsBuilder.getResult());
		
//		item = ctrl.select(ShadowrunCore.getItem("fake_sin"), 
//				new SelectionOption(SelectionOptionType.RATING, 1),
//				new SelectionOption(SelectionOptionType.AMOUNT, 3));
//		assertNotNull("Could not buy Fake Sin",item);
//		assertEquals(7500, item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		
		/*
		 * Contacts
		 */
		ConnectionsController contCtrl = charGen.getConnectionController();
		Connection con = contCtrl.createConnection();
		con.setType("Corporate War Mage");
		con.setName("Dr. No");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Crimson Crush Ganger");
		con.setName("Nonameicus");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Talismonger");
		con.setName("Nonameferatu");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		
		
		
		model.setRealName("Jane Doe");
		model.setName("Combat Mage");
		model.setGender(Gender.FEMALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(170);
		model.setWeight(180);

		// Finalize
		charGen.stop();

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateAdept() {
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.A);
		charGen.setPriority(PriorityType.METATYPE, Priority.C);
		charGen.setPriority(PriorityType.MAGIC, Priority.B);
		charGen.setPriority(PriorityType.SKILLS, Priority.D);
		charGen.setPriority(PriorityType.RESOURCES, Priority.E);
		assertEquals(50, model.getKarmaFree());
		assertTrue("Could not select Human",charGen.getMetatypeController().select(ShadowrunCore.getMetaType("human")));

		/*
		 * Qualities
		 */
		QualityController qualities = charGen.getQualityController();
		qualities.select(ShadowrunCore.getQuality("ar_vertigo"));
		qualities.select(ShadowrunCore.getQuality("guts"));
		qualities.select(ShadowrunCore.getQuality("honorbound"), "Codex Duello");
		qualities.select(ShadowrunCore.getQuality("quick_healer"));
		qualities.select(ShadowrunCore.getQuality("sinner"));
		qualities.select(ShadowrunCore.getQuality("toughness"));
		
		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(24, attrib.getPointsLeft());
		assertEquals(9, attrib.getPointsLeftSpecial());
		for (int i=0; i<6; i++)
			assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(3 , attrib.getPointsLeftSpecial());
		
		// Body +4
		for (int i=0; i<5; i++) assertTrue(attrib.increase(Attribute.BODY));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.AGILITY));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.REACTION));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.STRENGTH));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.WILLPOWER));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.LOGIC));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.INTUITION));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.CHARISMA));
		assertEquals(0, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(3 , attrib.getPointsLeftSpecial());
		
		// Magic or resonance
		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
			if (opt.getType().getId().equals("adept"))
				charGen.getMagicOrResonanceController().select(opt);
		}
		assertNotNull(model.getMagicOrResonanceType());
		assertEquals("adept", model.getMagicOrResonanceType().getId()); 
		assertEquals(3, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		AdeptPowerController powers2= charGen.getPowerController();
		assertEquals(3.0,powers2.getPowerPointsLeft(), 0.0);
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.MAGIC));
		assertEquals(6, model.getAttribute(Attribute.MAGIC).getModifiedValue());
		assertEquals(6.0,powers2.getPowerPointsLeft(), 0.0);
		

		// Skills
		SkillController skills = charGen.getSkillController();
		assertEquals(2, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(16, skills.getPointsLeftSkills());
//		SkillGroupValue sgVal = skills.select(ShadowrunCore.getSkillGroup("ATHLETICS"));
//		assertNotNull("Could not select skillgroup", sgVal);
//		assertTrue(skills.increase(sgVal));

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("athletics"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		skills.select(sVal, ShadowrunCore.getSkill("athletics").getSpecialization("throwing"), false);
		
		sVal = skills.select(ShadowrunCore.getSkill("biotech"));
		assertTrue(skills.increase(sVal)); // 2
		skills.select(sVal, ShadowrunCore.getSkill("biotech").getSpecialization("first_aid"), false);
		
		sVal = skills.select(ShadowrunCore.getSkill("close_combat"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		assertTrue(skills.increase(sVal)); // 6
		skills.select(sVal, ShadowrunCore.getSkill("close_combat").getSpecialization("unarmed"), false);

		sVal = skills.select(ShadowrunCore.getSkill("outdoors"));
		assertTrue(skills.increase(sVal)); // 2

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		skills.select(sVal, ShadowrunCore.getSkill("perception").getSpecialization("visual"), false);

		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(2, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("English");
		assertTrue(skills.canBeSelected(ShadowrunCore.getSkill("knowledge")));
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Fight Clubs") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Fort Lewis Geography") );
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());

		/*
		 * Powers
		 */
		AdeptPowerController powers = charGen.getPowerController();
		assertEquals(6.0,powers.getPowerPointsLeft(), 0.0);
		assertFalse(powers.canIncreasePowerPoints());
//		assertTrue(powers.increasePowerPoints()); // 4 Power points
//		assertTrue(powers.increasePowerPoints()); // 5 Power points
//		assertTrue(powers.increasePowerPoints()); // 6 Power points
		assertEquals(6.0,powers.getPowerPointsLeft(), 0.0);
		AdeptPowerValue power = powers.select(ShadowrunCore.getAdeptPower("combat_sense"));
		assertNotNull(power);
		assertTrue(powers.canBeIncreased(power));
		powers.increase(power);
		power = powers.select(ShadowrunCore.getAdeptPower("critical_strike"));
		assertTrue(powers.increase(power));
		power = powers.select(ShadowrunCore.getAdeptPower("improved_reflexes"));
		assertNotNull(power);
		assertTrue(powers.increase(power));
		assertNotNull(powers.select(ShadowrunCore.getAdeptPower("killing_hands")));

//		// Foci
//		FocusController foci = charGen.getFocusController();
//		foci.select(ShadowrunCore.getFocus("weapon_focus"), 2, ShadowrunCore.getItem("sword"));
		
		
		// SIN
		SINController sins = charGen.getSINController();
		sins.canCreateNewSIN(Quality.ANYONE, 3);
//		SIN[] sins2 = sins.createNewSIN("Fake SIN Rt.4", Quality.SUPERFICIALLY_PLAUSIBLE, 1);
		
		// Lifestyles
		SingleLifestyleBuilder lsBuilder = new SingleLifestyleGenerator(model);
		lsBuilder.selectLifestyle(ShadowrunCore.getLifestyle("low"));
		lsBuilder.selectName("Lousy appartment");
//		assertFalse(model.getSINs().isEmpty());
//		lsBuilder.selectSIN(model.getSINs().get(0));
		
		LifestyleController lifestyles = charGen.getLifestyleController();
		lifestyles.addLifestyle(lsBuilder.getResult());
		
		/*
		 * Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		for (int i=0; i<5; i++)
			ctrl.increaseBoughtNuyen();
		assertNotNull(ShadowrunCore.getItem("armor_vest"));
		ctrl.select(ShadowrunCore.getItem("armor_vest"));
		CarriedItem item = ctrl.select(ShadowrunCore.getItem("contacts"), new SelectionOption(SelectionOptionType.RATING, 3));
		ctrl.embed(item, ShadowrunCore.getItem("flare_compensation"));
		ctrl.embed(item, ShadowrunCore.getItem("image_link"));
		ctrl.embed(item, ShadowrunCore.getItem("low_light_vision"));
		item = ctrl.select(ShadowrunCore.getItem("sony_emporer"));
		assertNotNull(item);
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("trodes")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("subvocal_microphone")));
		ctrl.select(ShadowrunCore.getItem("yamaha_growler"));
		
		// Weapons
		ctrl.select(ShadowrunCore.getItem("bow5"));
//		ctrl.select(ShadowrunCore.getItem("arrow"), new SelectionOption(SelectionOptionType.RATING, 5));
		ctrl.select(ShadowrunCore.getItem("knife"));
		ctrl.select(ShadowrunCore.getItem("shock_gloves"));
		ctrl.select(ShadowrunCore.getItem("stun_baton"));
//		ctrl.select(ShadowrunCore.getItem("sword"));
		ctrl.select(ShadowrunCore.getItem("throwing_knives"));
		
		/*
		 * Contacts
		 */
		ConnectionsController contCtrl = charGen.getConnectionController();
		Connection con = contCtrl.createConnection();
		con.setType("First Nations Ganger");
		con.setName("Gungnam");
		contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Salish Government Secretary");
		con.setName("Salitos");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Sensei");
		con.setName("Sensible");
		contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Squatter");
		con.setName("Square");
		
		
		model.setRealName("John Doe");
		model.setName("Adept");
		model.setGender(Gender.FEMALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
//		model.setSize(230);
//		model.setWeight(160);

		// Finalize
		charGen.stop();

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateDecker() {
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.B);
		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.setPriority(PriorityType.MAGIC, Priority.E);
		charGen.setPriority(PriorityType.SKILLS, Priority.C);
		charGen.setPriority(PriorityType.RESOURCES, Priority.A);
		assertTrue("Could not select Dwarf",charGen.getMetatypeController().select(ShadowrunCore.getMetaType("dwarf")));

		/*
		 * Qualities
		 */
		QualityController qualities = charGen.getQualityController();
		qualities.select(ShadowrunCore.getQuality("allergy_uncommon_mild"), "Dairy");
		qualities.select(ShadowrunCore.getQuality("analytical_mind"));
		qualities.select(ShadowrunCore.getQuality("hardening"));
		qualities.select(ShadowrunCore.getQuality("gearhead"));
		qualities.select(ShadowrunCore.getQuality("aptitude"), ShadowrunCore.getSkill("cracking"));
		QualityValue qVal = qualities.select(ShadowrunCore.getQuality("dependents"), "Parent");
		qualities.increase(qVal);
		qualities.increase(qVal);
		
		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(16, attrib.getPointsLeft());
		assertEquals(4, attrib.getPointsLeftSpecial());
		for (int i=0; i<4; i++)
			assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(0 , attrib.getPointsLeftSpecial());
		
		// Body +4
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.BODY));
//		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.AGILITY));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.REACTION));
//		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.STRENGTH));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.WILLPOWER));
		for (int i=0; i<5; i++) assertTrue(attrib.increase(Attribute.LOGIC));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.INTUITION));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.CHARISMA));
//		assertEquals(0, attrib.getPointsLeft());  // 3 are paid from adjustment points
//		assertEquals(0 , attrib.getPointsLeftSpecial());
		
//		// Magic or resonance
//		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
//			if (opt.getType().getId().equals("adept"))
//				charGen.getMagicOrResonanceController().select(opt);
//		}
//		assertNotNull(model.getMagicOrResonanceType());
//		assertEquals("adept", model.getMagicOrResonanceType().getId()); 
//		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.MAGIC));
		

		// Skills
		SkillController skills = charGen.getSkillController();
		assertEquals(6, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(20, skills.getPointsLeftSkills());
		SkillValue sVal = skills.select(ShadowrunCore.getSkill("cracking"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		assertTrue(skills.increase(sVal)); // 6
		assertTrue(skills.increase(sVal)); // 7
		skills.select(sVal, ShadowrunCore.getSkill("electronics").getSpecialization("computer"), false);

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		
		sVal = skills.select(ShadowrunCore.getSkill("piloting"));
		assertTrue(skills.increase(sVal)); // 2
		
		sVal = skills.select(ShadowrunCore.getSkill("stealth"));
		
		sVal = skills.select(ShadowrunCore.getSkill("firearms"));
		assertTrue(skills.increase(sVal)); // 2
//		skills.select(sVal, ShadowrunCore.getSkill("perception").getSpecialization("visual"), false);
//
//		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(6, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("English");
		assertNotNull(skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "Japanese"));
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Data Havens") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Decker Bars") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Hackers") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Matrix Clubs") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Node Security Design") );
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());

//		/*
//		 * Powers
//		 */
//		AdeptPowerController powers = charGen.getPowerController();
//		assertEquals(3.0,powers.getPowerPointsLeft(), 0.0);
//		assertTrue(powers.canIncreasePowerPoints());
//		assertTrue(powers.increasePowerPoints()); // 4 Power points
//		assertTrue(powers.increasePowerPoints()); // 5 Power points
//		assertTrue(powers.increasePowerPoints()); // 6 Power points
//		assertEquals(6.0,powers.getPowerPointsLeft(), 0.0);
//		AdeptPowerValue power = powers.select(ShadowrunCore.getAdeptPower("combat_sense"));
//		assertNotNull(power);
//		assertTrue(powers.canBeIncreased(power));
//		powers.increase(power);
//		power = powers.select(ShadowrunCore.getAdeptPower("critical_strike"));
//		assertTrue(powers.increase(power));
//		power = powers.select(ShadowrunCore.getAdeptPower("improved_reflexes"));
//		assertNotNull(power);
//		assertTrue(powers.increase(power));
//		assertNotNull(powers.select(ShadowrunCore.getAdeptPower("killing_hands")));
		
		/*
		 * Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		for (int i=0; i<5; i++)
			assertTrue("Failed increasing Nuyen", ctrl.increaseBoughtNuyen());
		CarriedItem item = ctrl.select(ShadowrunCore.getItem("cyberears2"));
		CarriedItem enh = ctrl.embed(item, ShadowrunCore.getItem("audio_enhancement"));
		assertNotNull(enh);
		assertEquals(1, enh.getAsValue(ItemAttribute.CAPACITY).getModifiedValue());
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("damper")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("select_sound_filter"), new SelectionOption(SelectionOptionType.RATING, 2)));

		item = ctrl.select(ShadowrunCore.getItem("cybereye4"));
		ctrl.embed(item, ShadowrunCore.getItem("flare_compensation"));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("thermographic_vision")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("low_light_vision"), new SelectionOption(SelectionOptionType.RATING, 2)));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("vision_enhancement")));
		
		assertNotNull(ctrl.select(ShadowrunCore.getItem("cyberjack6")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("jammer_area"), new SelectionOption(SelectionOptionType.RATING, 6)));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("armor_jacket")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("data_tap")));
		
		item = ctrl.select(ShadowrunCore.getItem("transys_avalon"));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("biometric_reader")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("electronic_paper")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("printer")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("satellite_link")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("trid_projector")));
		item = ctrl.select(ShadowrunCore.getItem("shiawase_cyber6"));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("armor")));
//		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("browse")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("configurator")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("edit")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("exploit")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("overclock")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("signal_scrubber")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("stealth")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("toolbox")));
		assertNotNull(ctrl.embed(item, ShadowrunCore.getItem("trace")));

		item = ctrl.select(ShadowrunCore.getItem("ares_predator_vi"));
		CarriedItem scope = ctrl.embed(item,ShadowrunCore.getItem("imaging_scope"), ItemHook.TOP);
		assertNotNull(scope);
		assertNotNull(ctrl.embed(scope,ShadowrunCore.getItem("low_light_vision"), ItemHook.OPTICAL));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("ammo_holdout_light_machine"), 
				new SelectionOption(SelectionOptionType.AMOUNT, 10),
				new SelectionOption(SelectionOptionType.AMMOTYPE, ShadowrunCore.getAmmoType("regular"))));

		// Connections
		ConnectionsController conCtrl = charGen.getConnectionController();
		Connection contact = conCtrl.createConnection();
		conCtrl.increaseLoyalty(contact);
		contact.setType("Bartender");
		contact.setName("Bart");
		
		contact = conCtrl.createConnection();
		conCtrl.increaseInfluence(contact);
		conCtrl.increaseLoyalty(contact);
		contact.setType("Info Broker");
		contact.setName("Inigo");
		
		contact = conCtrl.createConnection();
		conCtrl.increaseLoyalty(contact);
		contact.setType("Matrix Ganger");
		contact.setName("Matt");
		
		contact = conCtrl.createConnection();
		contact.setType("Electronics Shop Owner");
		contact.setName("Eloy");
		
		
		
		// SINs
		SINController sins = charGen.getSINController();
//		sins.canCreateNewLicense(Quality.ANYONE, 3);
		SIN[] sins2 = sins.createNewSIN("Fake SIN Rt.4", Quality.SUPERFICIALLY_PLAUSIBLE, 1);
		LicenseValue lic = sins.createNewLicense(ShadowrunCore.getLicenseType("concealed_carry"), sins2[0], Quality.SUPERFICIALLY_PLAUSIBLE);
		lic.setSIN(sins2[0].getUniqueId());
		lic = sins.createNewLicense(ShadowrunCore.getLicenseType("cyberdeck"), sins2[0], Quality.SUPERFICIALLY_PLAUSIBLE);
		lic.setSIN(sins2[0].getUniqueId());
		lic = sins.createNewLicense(ShadowrunCore.getLicenseType("cyberware"), sins2[0], Quality.SUPERFICIALLY_PLAUSIBLE);
		lic.setSIN(sins2[0].getUniqueId());
		sins.createNewSIN("Fake SIN Rt.2", Quality.ROUGH_MATCH, 1);
		sins.createNewSIN("Fake SIN Rt.1", Quality.ANYONE, 1);
		
		// Lifestyles
		SingleLifestyleBuilder lsBuilder = new SingleLifestyleGenerator(model);
		lsBuilder.selectLifestyle(ShadowrunCore.getLifestyle("middle"));
		lsBuilder.selectName("Cosy appartment");
		lsBuilder.selectSIN(sins2[0]);
		
		LifestyleController lifestyles = charGen.getLifestyleController();
		lifestyles.addLifestyle(lsBuilder.getResult());
		
		model.setRealName("Jane Doe");
		model.setName("Decker");
		model.setGender(Gender.FEMALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(160);
		model.setWeight(80);
		InputStream in = null;
		try {
			 in = ClassLoader.getSystemResourceAsStream("Decker.png");
			model.setImage(in.readAllBytes());
		} catch (IOException e1) {
		} finally {
			try { in.close(); } catch (IOException e) {}
		}

		// Finalize
		charGen.stop();

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateStreetShaman() {
		charGen.setPriority(PriorityType.MAGIC, Priority.A);
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.B);
		charGen.setPriority(PriorityType.METATYPE, Priority.C);
		charGen.setPriority(PriorityType.SKILLS, Priority.D);
		charGen.setPriority(PriorityType.RESOURCES, Priority.E);
		assertEquals(50, model.getKarmaFree());
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("human"));

		/*
		 * Qualities
		 */
		QualityController qualities = charGen.getQualityController();
		QualityValue qVal = qualities.select(ShadowrunCore.getQuality("allergy_common_mild"), "Insect stings");
		qualities.select(ShadowrunCore.getQuality("distinctive_style"));
		qualities.select(ShadowrunCore.getQuality("mentor_spirit"), ShadowrunCore.getMentorSpirit("cat"));
		qualities.select(ShadowrunCore.getQuality("focused_concentration"));
		qualities.select(ShadowrunCore.getQuality("spirit_affinity"), ShadowrunCore.getSpirit("spirit_of_beasts"));
		qualities.select(ShadowrunCore.getQuality("spirit_bane"), ShadowrunCore.getSpirit("spirit_of_fire"));
		
		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(16, attrib.getPointsLeft());
		assertEquals(9, attrib.getPointsLeftSpecial());
		for (int i=0; i<6; i++) assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(3 , attrib.getPointsLeftSpecial());
		
		assertEquals(16, attrib.getPointsLeft());  // 3 are paid from adjustment points
		// Body +4
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.BODY));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.AGILITY));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.REACTION));
//		for (int i=0; i<0; i++) assertTrue(attrib.increase(Attribute.STRENGTH));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.WILLPOWER));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.CHARISMA));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.LOGIC));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.INTUITION));
		assertEquals(0, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(3 , attrib.getPointsLeftSpecial());
		
		// Magic or resonance
		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
			if (opt.getType().getId().equals("magician"))
				charGen.getMagicOrResonanceController().select(opt);
		}
		assertNotNull(model.getMagicOrResonanceType());
		assertEquals("magician", model.getMagicOrResonanceType().getId()); 
		assertEquals(3 , attrib.getPointsLeftSpecial());
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.MAGIC));
		assertEquals(1 , attrib.getPointsLeftSpecial());
		

		// Skills
		SkillController skills = charGen.getSkillController();
//		assertEquals(3, skills.getPointsLeftInKnowledgeAndLanguage());
//		assertEquals(16, skills.getPointsLeftSkills());

		skills.select(ShadowrunCore.getSkill("athletics"));

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("close_combat"));
		assertTrue(skills.increase(sVal)); // 2
		
		sVal = skills.select(ShadowrunCore.getSkill("conjuring"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		skills.select(sVal, ShadowrunCore.getSkill("conjuring").getSpecialization("summoning"), false);

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertTrue(skills.increase(sVal)); // 2

		sVal = skills.select(ShadowrunCore.getSkill("sorcery"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		assertTrue(skills.increase(sVal)); // 6
		skills.select(sVal, ShadowrunCore.getSkill("sorcery").getSpecialization("spellcasting"), false);

//		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(2, skills.getPointsLeftInKnowledgeAndLanguage());
//		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
//		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("English");
		assertTrue(skills.canBeSelected(ShadowrunCore.getSkill("knowledge")));
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Dive Bars") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Seattle Shamans") );
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());

		/*
		 * Spells
		 */
		SpellController spells = charGen.getSpellController();
		assertEquals(8, spells.getSpellsLeft());
		model.setTradition(ShadowrunCore.getTradition("shaman"));
		assertNotNull(spells.select(ShadowrunCore.getSpell("animate_plastic"), false));
		assertEquals(7, spells.getSpellsLeft());
		assertNotNull(spells.select(ShadowrunCore.getSpell("blast"), false));
		assertEquals(6, spells.getSpellsLeft());
		assertNotNull(spells.select(ShadowrunCore.getSpell("chaos"), false));
		assertNotNull(spells.select(ShadowrunCore.getSpell("clairaudience"), false));
		assertNotNull(spells.select(ShadowrunCore.getSpell("darkness"), false));
		assertNotNull(spells.select(ShadowrunCore.getSpell("fireball"), false));
		assertNotNull(spells.select(ShadowrunCore.getSpell("heal"), false));
		assertNotNull(spells.select(ShadowrunCore.getSpell("increase_reflexes"), false));
		assertEquals(0, spells.getSpellsLeft());
		assertNotNull(spells.select(ShadowrunCore.getSpell("levitate"), false));
		assertNotNull(spells.select(ShadowrunCore.getSpell("overclock"), false));
//		assertNotNull(spells.select(ShadowrunCore.getSpell("physical_barrier"), false));
//		assertNotNull(spells.select(ShadowrunCore.getSpell("stunbolt"), false));
//		assertNotNull(spells.select(ShadowrunCore.getSpell("trid_phantasm"), false));
		
		/*
		 * Contacts
		 */
		ConnectionsController contCtrl = charGen.getConnectionController();
		assertEquals(30, contCtrl.getPointsLeft());
		Connection con = contCtrl.createConnection();
		con.setType("Beat Cop");
		con.setName("Claus Cop");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Smuggler");
		con.setName("Steven Stealth");
		contCtrl.increaseInfluence(con); contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Street Kid");
		con.setName("Kiddo");
		contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Talismonger");
		con.setName("Tom Talis");
		contCtrl.increaseInfluence(con); contCtrl.increaseInfluence(con); contCtrl.increaseInfluence(con); contCtrl.increaseInfluence(con); 
		contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Yakuza Soldier");
		con.setName("Yak Yaksun");
		contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con);
		assertEquals(0, contCtrl.getPointsLeft());

		/*
		 * Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
		for (int i=0; i<4; i++)
			assertTrue("Failed increasing Nuyen", ctrl.increaseBoughtNuyen());
		ctrl.select(ShadowrunCore.getItem("armor_vest"));
		ctrl.select(ShadowrunCore.getItem("colt_government_2076"));
		ctrl.select(ShadowrunCore.getItem("combat_knife"));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("ammo_holdout_light_machine"), 
				new SelectionOption(SelectionOptionType.AMOUNT, 10),
				new SelectionOption(SelectionOptionType.AMMOTYPE, ShadowrunCore.getAmmoType("regular"))));
		CarriedItem item = ctrl.select(ShadowrunCore.getItem("meta_link"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("trodes")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("suzuki_mirage")));
		
		// Lifestyles
		SingleLifestyleBuilder lsBuilder = new SingleLifestyleGenerator(model);
		lsBuilder.selectLifestyle(ShadowrunCore.getLifestyle("low"));
		lsBuilder.selectName("You don't wanna live there");
		
		LifestyleController lifestyles = charGen.getLifestyleController();
		lifestyles.addLifestyle(lsBuilder.getResult());
		
		model.setRealName("Jane Doe");
		model.setName("Street Shaman");
		model.setGender(Gender.FEMALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(170);
		model.setWeight(68);
		model.setAge("2058");

		// Finalize
		charGen.stop();
		
		model.addRitual(new RitualValue(ShadowrunCore.getRitual("watcher")));
		model.addSpell(new SpellValue(ShadowrunCore.getSpell("heal"), true));

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateStreetSamurai() {
		charGen.setPriority(PriorityType.MAGIC, Priority.E);
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.B);
		charGen.setPriority(PriorityType.METATYPE, Priority.C);
		charGen.setPriority(PriorityType.SKILLS, Priority.D);
		charGen.setPriority(PriorityType.RESOURCES, Priority.A);
		assertEquals(50, model.getKarmaFree());
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("troll"));

		/*
		 * Qualities
		 */
		QualityController qualities = charGen.getQualityController();
		qualities.select(ShadowrunCore.getQuality("aptitude"), ShadowrunCore.getSkill("close_combat"));
		qualities.select(ShadowrunCore.getQuality("ambidextrous"));
		QualityValue qVal = qualities.select(ShadowrunCore.getQuality("dependents"), "Siblings");
		qualities.increase(qVal);
		qualities.select(ShadowrunCore.getQuality("high_pain_tolerance"));
		qVal = qualities.select(ShadowrunCore.getQuality("honorbound"), "Bushido");
		
		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(16, attrib.getPointsLeft());
		assertEquals(9, attrib.getPointsLeftSpecial());
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(5 , attrib.getPointsLeftSpecial());
		
		assertEquals(16, attrib.getPointsLeft());  // 3 are paid from adjustment points
		for (int i=0; i<6; i++) assertTrue(attrib.increase(Attribute.BODY));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.AGILITY));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.REACTION));
		for (int i=0; i<6; i++) assertTrue(attrib.increase(Attribute.STRENGTH));
//		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.WILLPOWER));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.CHARISMA));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.LOGIC));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.INTUITION));
//		assertEquals(0, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(0 , attrib.getPointsLeftSpecial());
		

		// Skills
		SkillController skills = charGen.getSkillController();
//		assertEquals(3, skills.getPointsLeftInKnowledgeAndLanguage());
//		assertEquals(16, skills.getPointsLeftSkills());

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("close_combat"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		assertTrue(skills.increase(sVal)); // 6
		assertTrue(skills.increase(sVal)); // 7
		skills.select(sVal, ShadowrunCore.getSkill("close_combat").getSpecialization("blades"), false);
		
		sVal = skills.select(ShadowrunCore.getSkill("firearms"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		skills.select(sVal, ShadowrunCore.getSkill("firearms").getSpecialization("pistols"), false);

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3

		sVal = skills.select(ShadowrunCore.getSkill("stealth"));
		assertTrue(skills.increase(sVal)); // 2

		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(2, skills.getPointsLeftInKnowledgeAndLanguage());
//		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
//		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("English");
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "Or'zet") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "Japanese") );
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());
		
		/*
		 * Contacts
		 */
		ConnectionsController contCtrl = charGen.getConnectionController();
		assertEquals(12, contCtrl.getPointsLeft());
		Connection con = contCtrl.createConnection();
		con.setType("Corporate Executive");
		con.setName("Caesar");
		contCtrl.increaseInfluence(con); 
		con = contCtrl.createConnection();
		con.setType("Martial Arts Instructor");
		con.setName("Martin");
		contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Squatter");
		con.setName("Squal");
		con = contCtrl.createConnection();
		con.setType("Urban Brawler");
		con.setName("Uruk");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		assertEquals(0, contCtrl.getPointsLeft());

		/*
		 * Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
//		for (int i=0; i<4; i++)
//			assertTrue("Failed increasing Nuyen", ctrl.increaseBoughtNuyen());
		ctrl.select(ShadowrunCore.getItem("bone_lacing_titanium"));
		CarriedItem item = ctrl.select(ShadowrunCore.getItem("cyberarm_obvious"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("attribute_increase_agility"), new SelectionOption(SelectionOptionType.RATING, 3)));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("attribute_increase_strength"), new SelectionOption(SelectionOptionType.RATING, 8)));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("cyber_slide")));
//		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("cyber_smuggling_compartment")));
		ctrl.select(ShadowrunCore.getItem("muscle_toner"), new SelectionOption(SelectionOptionType.RATING, 2));
		ctrl.select(ShadowrunCore.getItem("platelet_factories"));
		assertNull(ctrl.select(ShadowrunCore.getItem("reflex_recorder")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("reflex_recorder"), new SelectionOption(SelectionOptionType.SKILL, ShadowrunCore.getSkill("close_combat"))));
		assertEquals(1, model.getSkillValue(ShadowrunCore.getSkill("close_combat")).getModifier());
		
		assertEquals(4, model.getSkillValue(ShadowrunCore.getSkill("firearms")).getPoints());
		assertEquals(0, model.getSkillValue(ShadowrunCore.getSkill("firearms")).getModifier());
		assertEquals(4, model.getSkillValue(ShadowrunCore.getSkill("firearms")).getModifiedValue());
		assertNotNull(ctrl.select(ShadowrunCore.getItem("reflex_recorder"), new SelectionOption(SelectionOptionType.SKILL, ShadowrunCore.getSkill("firearms"))));
		assertEquals(4, model.getSkillValue(ShadowrunCore.getSkill("firearms")).getPoints());
//		System.out.println("firearms = "+model.getSkillValue(ShadowrunCore.getSkill("firearms")));
//		assertEquals(1, model.getSkillValue(ShadowrunCore.getSkill("firearms")).getModifier());
//		assertEquals(5, model.getSkillValue(ShadowrunCore.getSkill("firearms")).getModifiedValue());
		ctrl.select(ShadowrunCore.getItem("sleep_regulator"));
		ctrl.select(ShadowrunCore.getItem("wired_reflexes1"));
		
		
		assertNotNull(ctrl.select(ShadowrunCore.getItem("ares_roadmaster")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("armor_jacket")));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("contacts"), new SelectionOption(SelectionOptionType.RATING, 3)));
		ctrl.select(ShadowrunCore.getItem("ares_predator_vi"));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("ammo_holdout_light_machine"), 
				new SelectionOption(SelectionOptionType.AMOUNT, 10),
				new SelectionOption(SelectionOptionType.AMMOTYPE, ShadowrunCore.getAmmoType("regular"))));
		item = ctrl.select(ShadowrunCore.getItem("transys_avalon"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("trodes")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("subvocal_microphone")));
		
		// Lifestyles
		SingleLifestyleBuilder lsBuilder = new SingleLifestyleGenerator(model);
		lsBuilder.selectLifestyle(ShadowrunCore.getLifestyle("middle"));
		lsBuilder.selectName("Nice appartment");
		
		LifestyleController lifestyles = charGen.getLifestyleController();
		lifestyles.addLifestyle(lsBuilder.getResult());
		
		model.setRealName("John Doe");
		model.setName("Street Samurai");
		model.setGender(Gender.MALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(250);
		model.setWeight(188);
		model.setAge("born 2058");

		// Finalize
		charGen.stop();

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateTechnomancer() {
		charGen.setPriority(PriorityType.MAGIC, Priority.B);
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.A);
		charGen.setPriority(PriorityType.METATYPE, Priority.C);
		charGen.setPriority(PriorityType.SKILLS, Priority.D);
		charGen.setPriority(PriorityType.RESOURCES, Priority.E);
		assertEquals(50, model.getKarmaFree());
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("dwarf"));
		
		
		/*
		 * Qualities
		 */
		QualityController qualities = charGen.getQualityController();
		qualities.select(ShadowrunCore.getQuality("allergy_common_moderate"), "Grass");
		qualities.select(ShadowrunCore.getQuality("analytical_mind"));
		QualityValue qVal = qualities.select(ShadowrunCore.getQuality("focused_concentration"));
		qualities.increase(qVal);
		qualities.select(ShadowrunCore.getQuality("hardening"));
		qVal = qualities.select(ShadowrunCore.getQuality("social_stress"), "Social gatherings");
		qVal.setDescription("Social gatherings");
		
		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(24, attrib.getPointsLeft());
		assertEquals(9, attrib.getPointsLeftSpecial());
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(7 , attrib.getPointsLeftSpecial());
		
		assertEquals(24, attrib.getPointsLeft());  // 3 are paid from adjustment points
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.BODY));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.AGILITY));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.REACTION));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.STRENGTH));
		for (int i=0; i<6; i++) assertTrue(attrib.increase(Attribute.WILLPOWER));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.LOGIC));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.INTUITION));
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.CHARISMA));
		assertEquals(3, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(0 , attrib.getPointsLeftSpecial());
		
		// Magic or resonance
		for (MagicOrResonanceOption opt : new ArrayList<>(charGen.getMagicOrResonanceController().getAvailable())) {
			if (opt.getType().getId().equals("technomancer"))
				charGen.getMagicOrResonanceController().select(opt);
		}
		assertNotNull(model.getMagicOrResonanceType());
		assertEquals("technomancer", model.getMagicOrResonanceType().getId()); 
		assertEquals(3, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(0 , attrib.getPointsLeftSpecial());
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.MAGIC));
		assertEquals(3, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(0 , attrib.getPointsLeftSpecial());

		assertNotNull(charGen.getMetamagicOrEchoController().select(ShadowrunCore.getMetamagicOrEcho("overclocking")));
		

		// Skills
		SkillController skills = charGen.getSkillController();
		assertEquals(5, skills.getPointsLeftInKnowledgeAndLanguage());
		assertEquals(16, skills.getPointsLeftSkills());

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("con"));
		assertTrue(skills.increase(sVal)); // 2
//		skills.select(sVal, ShadowrunCore.getSkill("con").getSpecialization("acting"), false);
		
		sVal = skills.select(ShadowrunCore.getSkill("electronics"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		skills.select(sVal, ShadowrunCore.getSkill("electronics").getSpecialization("computer"), false);

		sVal = skills.select(ShadowrunCore.getSkill("firearms"));

		sVal = skills.select(ShadowrunCore.getSkill("piloting"));
		assertTrue(skills.increase(sVal)); // 2

//		sVal = skills.select(ShadowrunCore.getSkill("stealth"));
		
		sVal = skills.select(ShadowrunCore.getSkill("tasking"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		assertTrue(skills.increase(sVal)); // 6
		skills.select(sVal, ShadowrunCore.getSkill("tasking").getSpecialization("compiling"), false);
		skills.select(sVal, ShadowrunCore.getSkill("piloting").getSpecialization("ground_craft"), false);

		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(5, skills.getPointsLeftInKnowledgeAndLanguage());
//		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
//		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("English");
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Commlink Designs") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Host Designs") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Seattle Gangs") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Tacoma Geography") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Technomancer Hangouts") );
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());

		/*
		 * Complex forms
		 */
		ComplexFormController spells = charGen.getComplexFormController();
		assertEquals(6, spells.getComplexFormsLeft());
		assertNotNull(spells.select(ShadowrunCore.getComplexForm("diffusion"), Attribute.FIREWALL));
		assertEquals(5, spells.getComplexFormsLeft());
		assertNotNull(spells.select(ShadowrunCore.getComplexForm("infusion"), Attribute.ATTACK));
		assertNotNull(spells.select(ShadowrunCore.getComplexForm("infusion"), Attribute.SLEAZE));
//		assertNotNull(spells.select(ShadowrunCore.getComplexForm("living_network")));
		assertNotNull(spells.select(ShadowrunCore.getComplexForm("pulse_storm")));
		assertNotNull(spells.select(ShadowrunCore.getComplexForm("resonance_spike")));
		assertNotNull(spells.select(ShadowrunCore.getComplexForm("static_veil")));
		assertEquals(0, spells.getComplexFormsLeft());
		
		/*
		 * Contacts
		 */
		ConnectionsController contCtrl = charGen.getConnectionController();
		assertEquals(30, contCtrl.getPointsLeft());
		Connection con = contCtrl.createConnection();
		con.setType("Bartender");
		con.setName("Barty");
		contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); 
		con = contCtrl.createConnection();
		con.setType("Bounty Hunter");
		con.setName("Boba");
		contCtrl.increaseInfluence(con); 
		contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); 
		con = contCtrl.createConnection();
		con.setType("Cab Driver");
		con.setName("Cal");
		contCtrl.increaseInfluence(con); 
		con = contCtrl.createConnection();
		con.setType("Corporate Executive");
		con.setName("Ceo");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con);
		con = contCtrl.createConnection();
		con.setType("Decker");
		con.setName("Don");
		contCtrl.increaseInfluence(con); contCtrl.increaseLoyalty(con); contCtrl.increaseLoyalty(con); 
		con = contCtrl.createConnection();
		con.setType("Squatter");
		con.setName("Squish");
		contCtrl.increaseLoyalty(con); 
		assertEquals(0, contCtrl.getPointsLeft());

		/*
		 * Equipment
		 */
		EquipmentController ctrl = charGen.getEquipmentController();
//		for (int i=0; i<4; i++)
//			assertTrue("Failed increasing Nuyen", ctrl.increaseBoughtNuyen());
		ctrl.select(ShadowrunCore.getItem("jammer_area"), new SelectionOption(SelectionOptionType.RATING, 4));
		CarriedItem item = ctrl.select(ShadowrunCore.getItem("armor_jacket"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("electricity_resistance"), new SelectionOption(SelectionOptionType.RATING, 4)));
		ctrl.select(ShadowrunCore.getItem("stim_patch"), new SelectionOption(SelectionOptionType.RATING, 4), new SelectionOption(SelectionOptionType.AMOUNT, 3));
		
		
		ctrl.select(ShadowrunCore.getItem("walther_palm_pistol"));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("ammo_holdout_light_machine"), 
				new SelectionOption(SelectionOptionType.AMOUNT, 5),
				new SelectionOption(SelectionOptionType.AMMOTYPE, ShadowrunCore.getAmmoType("regular"))));
		item = ctrl.select(ShadowrunCore.getItem("meta_link"));
		
		// Lifestyles
		SingleLifestyleBuilder lsBuilder = new SingleLifestyleGenerator(model);
		lsBuilder.selectLifestyle(ShadowrunCore.getLifestyle("low"));
		lsBuilder.selectName("You call that an appartment?");
		
		LifestyleController lifestyles = charGen.getLifestyleController();
		lifestyles.addLifestyle(lsBuilder.getResult());
		
		model.setRealName("Ted");
		model.setName("Technomancer");
		model.setGender(Gender.MALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(140);
		model.setWeight(68);
		model.setAge("born 2058");

		// Finalize
		charGen.stop();

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void generateRigger() {
		charGen.setPriority(PriorityType.MAGIC, Priority.E);
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.C);
		charGen.setPriority(PriorityType.METATYPE, Priority.D);
		charGen.setPriority(PriorityType.SKILLS, Priority.B);
		charGen.setPriority(PriorityType.RESOURCES, Priority.A);
		assertEquals(50, model.getKarmaFree());
		charGen.getMetatypeController().select(ShadowrunCore.getMetaType("human"));

		/*
		 * Qualities
		 */
		QualityController qualities = charGen.getQualityController();
		qualities.select(ShadowrunCore.getQuality("aptitude"), ShadowrunCore.getSkill("piloting"));
		qualities.select(ShadowrunCore.getQuality("bad_rep"));
		QualityValue qVal = qualities.select(ShadowrunCore.getQuality("dependents"), "Estranged child");
		qualities.select(ShadowrunCore.getQuality("elf_poser"));
		qualities.select(ShadowrunCore.getQuality("gearhead"));
		qVal = qualities.select(ShadowrunCore.getQuality("in_debt"), "Mafia");
		qualities.select(ShadowrunCore.getQuality("juryrigger"));
		
		// Attributes
		AttributeController attrib = charGen.getAttributeController();
		assertEquals(12, attrib.getPointsLeft());
		assertEquals(4, attrib.getPointsLeftSpecial());
		for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.EDGE));
		assertEquals(0 , attrib.getPointsLeftSpecial());
		
		assertEquals(12, attrib.getPointsLeft());  // 3 are paid from adjustment points
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.BODY));
		for (int i=0; i<2; i++) assertTrue(attrib.increase(Attribute.AGILITY));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.REACTION));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.STRENGTH));
		for (int i=0; i<1; i++) assertTrue(attrib.increase(Attribute.WILLPOWER));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.LOGIC));
		for (int i=0; i<3; i++) assertTrue(attrib.increase(Attribute.INTUITION));
		//for (int i=0; i<4; i++) assertTrue(attrib.increase(Attribute.CHARISMA));
		assertEquals(0, attrib.getPointsLeft());  // 3 are paid from adjustment points
		assertEquals(0 , attrib.getPointsLeftSpecial());
		

		// Skills
		SkillController skills = charGen.getSkillController();
		assertEquals(24, skills.getPointsLeftSkills());
		assertEquals(4, skills.getPointsLeftInKnowledgeAndLanguage());

		SkillValue sVal = skills.select(ShadowrunCore.getSkill("biotech"));
		assertTrue(skills.increase(sVal)); // 2
		skills.select(sVal, ShadowrunCore.getSkill("biotech").getSpecialization("first_aid"), false);
		
		sVal = skills.select(ShadowrunCore.getSkill("engineering"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		skills.select(sVal, ShadowrunCore.getSkill("engineering").getSpecialization("aeronautics_mechanic"), false);

		sVal = skills.select(ShadowrunCore.getSkill("firearms"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		skills.select(sVal, ShadowrunCore.getSkill("firearms").getSpecialization("automatics"), false);

		sVal = skills.select(ShadowrunCore.getSkill("piloting"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		assertTrue(skills.increase(sVal)); // 4
		assertTrue(skills.increase(sVal)); // 5
		assertTrue(skills.increase(sVal)); // 6
		assertTrue(skills.increase(sVal)); // 7
		skills.select(sVal, ShadowrunCore.getSkill("piloting").getSpecialization("aircraft"), false);

		sVal = skills.select(ShadowrunCore.getSkill("perception"));
		assertTrue(skills.increase(sVal)); // 2
		assertTrue(skills.increase(sVal)); // 3
		skills.select(sVal, ShadowrunCore.getSkill("perception").getSpecialization("perception_urban"), false);
		
		sVal = skills.select(ShadowrunCore.getSkill("electronics"));
//		assertTrue(skills.increase(sVal)); // 2
		skills.select(sVal, ShadowrunCore.getSkill("electronics").getSpecialization("hardware"), false);

		assertEquals(0, skills.getPointsLeftSkills());
		assertEquals(4, skills.getPointsLeftInKnowledgeAndLanguage());
//		assertEquals(1, model.getSkillValues(SkillType.LANGUAGE).size());
//		assertEquals(SkillValue.LANGLEVEL_NATIVE, model.getSkillValues(SkillType.LANGUAGE).get(0).getPoints());
		model.getSkillValues(SkillType.LANGUAGE).get(0).setName("English");
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("language"), "German") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Australian Legends") );
//		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Desert Wars") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Drone Designers") );
		assertNotNull( skills.selectKnowledgeOrLanguage(ShadowrunCore.getSkill("knowledge"), "Rigger Clubs") );
		assertEquals(0, skills.getPointsLeftInKnowledgeAndLanguage());
		
		/*
		 * Contacts
		 */
		ConnectionsController contCtrl = charGen.getConnectionController();
		assertEquals(6, contCtrl.getPointsLeft());
		Connection con = contCtrl.createConnection();
		con.setType("Drone Racer");
		con.setName("Donna");
		con = contCtrl.createConnection();
		con.setType("Mechanic");
		con.setName("Mike");
		con = contCtrl.createConnection();
		con.setType("Squatter");
		con.setName("Squish");
		assertEquals(0, contCtrl.getPointsLeft());

		/*
		 * Now pay with nuyen
		 */
		assertEquals(450000, model.getNuyen());
		
		// Lifestyles
		SingleLifestyleBuilder lsBuilder = new SingleLifestyleGenerator(model);
		lsBuilder.selectLifestyle(ShadowrunCore.getLifestyle("middle"));
		lsBuilder.selectName("Small flat");
		
		LifestyleController lifestyles = charGen.getLifestyleController();
		lifestyles.addLifestyle(lsBuilder.getResult());

		// SINs
		SINController sins = charGen.getSINController();
		SIN sin1= sins.createNewSIN("Sinhead Sinner", Quality.SUPERFICIALLY_PLAUSIBLE);
		assertNotNull(sin1);
		LicenseValue lic = sins.createNewLicense(ShadowrunCore.getLicenseType("cyberware"), sin1, Quality.SUPERFICIALLY_PLAUSIBLE);
		lic.setSIN(sin1.getUniqueId());
		lic = sins.createNewLicense(ShadowrunCore.getLicenseType("rcc"), sin1, Quality.SUPERFICIALLY_PLAUSIBLE);
		lic.setSIN(sin1.getUniqueId());
		SIN sin2 = sins.createNewSIN("Sinhead Sinsible", Quality.ROUGH_MATCH);
		lic = sins.createNewLicense(ShadowrunCore.getLicenseType("cyberware"), sin2, Quality.ROUGH_MATCH);
		lic.setSIN(sin2.getUniqueId());
		lic = sins.createNewLicense(ShadowrunCore.getLicenseType("rcc"), sin2, Quality.ROUGH_MATCH);
		lic.setSIN(sin2.getUniqueId());

		/*
		 * Equipment
		 */		
		EquipmentController ctrl = charGen.getEquipmentController();
//		for (int i=0; i<3; i++)
//			assertTrue("Failed increasing Nuyen", ctrl.increaseBoughtNuyen());
		
		CarriedItem item = ctrl.select(ShadowrunCore.getItem("control_rig"), new SelectionOption(SelectionOptionType.RATING, 2));
		item = ctrl.select(ShadowrunCore.getItem("cybereye5"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("flare_compensation")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("low_light_vision")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("smartlink")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("thermographic_vision")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("vision_enhancement")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("vision_magnification")));

		item = ctrl.select(ShadowrunCore.getItem("cyberarm_obvious"));
		item.setName("Cyberarm, obvious, left");
		CarriedItem mount = ctrl.embed(item, ShadowrunCore.getItem("cyber_smg"));
		assertNotNull("Embedding failed", mount);
		CarriedItem weapon = ctrl.embed(mount, ShadowrunCore.getItem("uzi_iv"));
		assertNotNull("Embedding weapon failed", weapon);

//		ctrl.select(ShadowrunCore.getItem("aeronautical_repair_shop"));
		ctrl.select(ShadowrunCore.getItem("armor_jacket"));
		item = ctrl.select(ShadowrunCore.getItem("proteus_poseidon"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("biometric_reader")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("clearsight")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("electronic_warfare")));
		item = ctrl.select(ShadowrunCore.getItem("transys_avalon"));
		
		item = ctrl.select(ShadowrunCore.getItem("chrysler-nissan_pursuit_v"));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("evasion")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("maneuvering")));
		item = ctrl.select(ShadowrunCore.getItem("cyberspace_designs_dalmatian"), new SelectionOption(SelectionOptionType.AMOUNT, 2));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("evasion")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("maneuvering")));
		item = ctrl.select(ShadowrunCore.getItem("cyberspace_designs_quadrotor"), new SelectionOption(SelectionOptionType.AMOUNT, 4));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("evasion")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("maneuvering")));
		item = ctrl.select(ShadowrunCore.getItem("gmc_bulldog"));
		item = ctrl.select(ShadowrunCore.getItem("gmc_micromachine"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("maneuvering")));
		item = ctrl.select(ShadowrunCore.getItem("gm-nissan_dobermann"), new SelectionOption(SelectionOptionType.AMOUNT, 2));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("evasion")));
		assertNotNull("Embedding failed",ctrl.embed(item, ShadowrunCore.getItem("maneuvering")));
		mount = ctrl.embed(item, ShadowrunCore.getItem("weapon_mount_standard"));
		assertNotNull("Embedding failed",mount);
		weapon = ctrl.embed(mount, ShadowrunCore.getItem("ak_97"));
		assertNotNull("Embedding weapon failed",mount);
		item = ctrl.select(ShadowrunCore.getItem("mct_hornet"), new SelectionOption(SelectionOptionType.AMOUNT, 2));
		item = ctrl.select(ShadowrunCore.getItem("mct-nissan_roto_drone"), new SelectionOption(SelectionOptionType.AMOUNT, 2));
		item = ctrl.select(ShadowrunCore.getItem("mct-sikorsky-bell_microskimmer_xxs"), new SelectionOption(SelectionOptionType.AMOUNT, 5));
		item = ctrl.select(ShadowrunCore.getItem("suzuki_mirage"));

				
		
		ctrl.select(ShadowrunCore.getItem("walther_palm_pistol"));
		assertNotNull(ctrl.select(ShadowrunCore.getItem("ammo_holdout_light_machine"), 
				new SelectionOption(SelectionOptionType.AMOUNT, 5),
				new SelectionOption(SelectionOptionType.AMMOTYPE, ShadowrunCore.getAmmoType("regular"))));
		item = ctrl.select(ShadowrunCore.getItem("meta_link"));
		
		model.setRealName("Reginald");
		model.setName("Rigger");
		model.setGender(Gender.MALE);
//		model.setConcept(ShadowrunCore.getCharacterConcept("streetsamurai"));
		model.setSize(180);
		model.setWeight(88);
		model.setAge("born 2058");

		// Finalize
		charGen.stop();

		try {
			System.out.println("Save "+model.getName());
			byte[] data = ShadowrunCore.save(model);
			System.out.println(new String(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * 
	 */
	@Test
	public void testMagicBug() {
		charGen.setPriority(PriorityType.METATYPE, Priority.A);
		charGen.setPriority(PriorityType.ATTRIBUTE, Priority.B);
		charGen.setPriority(PriorityType.MAGIC, Priority.C);
		charGen.setPriority(PriorityType.SKILLS, Priority.D);
		charGen.setPriority(PriorityType.RESOURCES, Priority.E);
		model.setMagicOrResonanceType(ShadowrunCore.getMagicOrResonanceType("adept"));
		charGen.runProcessors();
		
		assertEquals(50, model.getKarmaFree());
		assertEquals(1, model.getAttribute(Attribute.BODY     ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.AGILITY  ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.REACTION ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.STRENGTH ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.WILLPOWER).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.LOGIC    ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.INTUITION).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.CHARISMA ).getModifiedValue());
		assertEquals(1, model.getAttribute(Attribute.EDGE     ).getModifiedValue());
		assertEquals(2, model.getAttribute(Attribute.MAGIC    ).getModifiedValue());
		assertEquals(0, model.getAttribute(Attribute.RESONANCE).getModifiedValue());

		AttributeController attrib = charGen.getAttributeController();
		assertEquals(1, attrib.getPerAttribute(Attribute.BODY     ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.AGILITY  ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.REACTION ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.STRENGTH ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.WILLPOWER).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.LOGIC    ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.INTUITION).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.CHARISMA ).base);
		assertEquals(1, attrib.getPerAttribute(Attribute.EDGE     ).base);
		assertEquals(2, attrib.getPerAttribute(Attribute.MAGIC    ).base);
		assertEquals(0, attrib.getPerAttribute(Attribute.RESONANCE).base);

		assertEquals(0, attrib.getPerAttribute(Attribute.BODY     ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY  ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.REACTION ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.WILLPOWER).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.LOGIC    ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.INTUITION).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.CHARISMA ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE     ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC    ).regular);
		assertEquals(0, attrib.getPerAttribute(Attribute.RESONANCE).regular);

		assertEquals(0, attrib.getPerAttribute(Attribute.BODY     ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY  ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.REACTION ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.WILLPOWER).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.LOGIC    ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.INTUITION).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.CHARISMA ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE     ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC    ).adjust);
		assertEquals(0, attrib.getPerAttribute(Attribute.RESONANCE).adjust);

		assertEquals(0, attrib.getPerAttribute(Attribute.BODY     ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.AGILITY  ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.REACTION ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.STRENGTH ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.WILLPOWER).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.LOGIC    ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.INTUITION).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.CHARISMA ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.EDGE     ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.MAGIC    ).karma);
		assertEquals(0, attrib.getPerAttribute(Attribute.RESONANCE).karma);

	}

}

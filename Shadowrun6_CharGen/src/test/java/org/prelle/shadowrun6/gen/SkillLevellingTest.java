/**
 * 
 */
package org.prelle.shadowrun6.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.DummyRulePlugin;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.levelling.CharacterLeveller;
import org.prelle.shadowrun6.levelling.KarmaSkillLeveller;
import org.prelle.shadowrun6.modifications.SkillModification;
import org.prelle.shadowrun6.modifications.SkillSpecializationModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SkillLevellingTest {

	private CharacterLeveller charGen;
	private KarmaSkillLeveller control; 
	private ShadowrunCharacter model;
	
	private List<Modification> unitTestMods;
	
	private static Skill CON;

	//-------------------------------------------------------------------
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {}
		});
		ShadowrunCore.initialize(new DummyRulePlugin<ShadowrunCharacter>());
		CON = ShadowrunCore.getSkill("con");
		Preferences pref = Preferences.userRoot().node("junit");
		SR6ConfigOptions.attachConfigurationTree(new ConfigContainerImpl(pref, "shadowrun6"));
//		System.exit(0);
	}

	//-------------------------------------------------------------------
	private void applyUnitTestModifications(SkillValue val) {
		unitTestMods.forEach(mod -> val.addModification(mod));
	}

	//-------------------------------------------------------------------
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		GenerationEventDispatcher.clear();
		model = new ShadowrunCharacter();
		charGen = new CharacterLeveller(model);
		control = (KarmaSkillLeveller) charGen.getSkillController();
		
		unitTestMods = new ArrayList<>();
		unitTestMods.add(new SkillModification(ModificationValueType.MAX, CON, 1));
		// There is an augmentation that granted one point
		unitTestMods.add(new SkillModification(CON, 1, new CarriedItem(ShadowrunCore.getItem("orthoskin"))));

		
		// Body set to 5 (started at 4), maximum is 7
		SkillValue val = new SkillValue(CON, 5);
		model.addSkill(val);
		val.setStart(4);
		val.setPoints(5);
		applyUnitTestModifications(val);
		// Track that character one increased attribute
		SkillModification histMod = new SkillModification(CON, 5);
		histMod.setExpCost(25);
		model.addToHistory(histMod);
		
		assertEquals(1, model.getHistory().size());
		assertTrue(model.getHistory().get(0) instanceof SkillModification);
		assertEquals(5, ((SkillModification)model.getHistory().get(0)).getValue() );
		assertEquals("Invalid Karma cost in history", 25, ((SkillModification)model.getHistory().get(0)).getExpCost() );
	}

	//-------------------------------------------------------------------
	@Test
	public void testIdle() throws IOException {
		SkillValue val = model.getSkillValue(CON);
		assertEquals(4, val.getStart());
		assertEquals(5, val.getPoints());
		assertEquals(6, val.getModifiedValue());
		assertEquals(1, val.getMaximumModifier());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void normalIncrease() throws IOException {
		// Grant not enough karma to increase
		model.setKarmaFree(15);
		model.setKarmaInvested(1);
		
		SkillValue val = model.getSkillValue(CON);
		assertNotNull("Skill not found", val);
		assertNotNull(control);
		assertFalse("Increase possible although not enough Karma", control.canBeIncreased(val));
		assertFalse("Increase possible although not enough Karma", control.increase(val));
		// Need 30 karma
		model.setKarmaFree(35);
		assertEquals(30, control.getIncreaseCost(CON));
		assertTrue("Increase not possible although enough Karma", control.canBeIncreased(val));
		assertTrue("Increase not possible although enough Karma", control.increase(val));
		assertEquals(5, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(2, model.getHistory().size());
		assertTrue(model.getHistory().get(1) instanceof SkillModification);
		assertEquals(6, ((SkillModification)model.getHistory().get(1)).getValue() );
		assertEquals("Invalid Karma cost in history", 30, ((SkillModification)model.getHistory().get(1)).getExpCost() );
		
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(6, val.getPoints());
		assertEquals(7, val.getModifiedValue());
		assertEquals(1, val.getMaximumModifier());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void normalDecrease() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		SkillValue val = model.getSkillValue(CON);
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.canBeDecreased(val));
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.decrease(val));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(40, model.getKarmaFree());
		assertEquals(6, model.getKarmaInvested());
		assertEquals(0, model.getHistory().size());
		
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(4, val.getPoints());
		assertEquals(5, val.getModifiedValue());
		assertEquals(1, val.getMaximumModifier());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseWithoutRefund() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(true);
		control.setRefundBelowCreation(false);
		SR6ConfigOptions.REFUND_FROM_CAREER.set(false);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		SkillValue val = model.getSkillValue(CON);
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.canBeDecreased(val));
		assertTrue("Decreasing should be allowed for values changes made in career mode", control.decrease(val));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(15, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(1, model.getHistory().size());
		
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(4, val.getPoints());
		assertEquals(5, val.getModifiedValue());
		assertEquals(1, val.getMaximumModifier());
		
		assertFalse("Decreasing below creation should not be allowed", control.canBeDecreased(val));
		assertFalse("Decreasing below creation should not be allowed", control.decrease(val));
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseBelowCreationWithoutRefundAtAll() throws IOException {
		control.setDecreaseBelowCreation(true);
		control.setDontRefund(true);
		control.setRefundBelowCreation(false);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		SkillValue val = model.getSkillValue(CON);
		control.decrease(val);
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.canBeDecreased(val));
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.decrease(val));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(15, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(1, model.getHistory().size());
		
		applyUnitTestModifications(val);
		assertEquals(3, val.getPoints());
		assertEquals(4, val.getStart());
		assertEquals(4, val.getModifiedValue());
		assertEquals(1, val.getMaximumModifier());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseBelowCreationWithoutRefundBelowCreation() throws IOException {
		control.setDecreaseBelowCreation(true);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		SkillValue val = model.getSkillValue(CON);
		control.decrease(val);
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.canBeDecreased(val));
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.decrease(val));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(15, model.getKarmaFree());
		assertEquals(31, model.getKarmaInvested());
		assertEquals(1, model.getHistory().size());
		
		applyUnitTestModifications(val);
		assertEquals(3, val.getPoints());
		assertEquals(4, val.getStart());
		assertEquals(4, val.getModifiedValue());
		assertEquals(1, val.getMaximumModifier());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void decreaseBelowCreationWithRefund() throws IOException {
		control.setDecreaseBelowCreation(true);
		control.setDontRefund(false);
		control.setRefundBelowCreation(true);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		SkillValue val = model.getSkillValue(CON);
		control.decrease(val); // Decrease to 4 
		assertEquals(0, model.getHistory().size());
		assertEquals(40, model.getKarmaFree());
		assertEquals(6, model.getKarmaInvested());
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.canBeDecreased(val));
		assertTrue("Decreasing should be allowed for values changes made in creation mode", control.decrease(val));
		// Check effects of decreasing from 5 to 4 (25 karma)
		assertEquals(60, model.getKarmaFree());
		assertEquals(-14, model.getKarmaInvested());
		assertEquals("Missing modification for attribute decrease below creation",1, model.getHistory().size());
		assertTrue(model.getHistory().get(0) instanceof SkillModification);
		assertEquals(4, ((SkillModification)model.getHistory().get(0)).getValue() );
		assertEquals("Invalid Karma cost in history", -20, ((SkillModification)model.getHistory().get(0)).getExpCost() );
		
		applyUnitTestModifications(val);
		assertEquals(3, val.getPoints());
		assertEquals(4, val.getStart());
		assertEquals(4, val.getModifiedValue());
		assertEquals(1, val.getMaximumModifier());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void increaseStopsAtMax() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		unitTestMods.clear();
		// There is an augmentation that granted one point
		unitTestMods.add(new SkillModification(CON, 1, new CarriedItem(ShadowrunCore.getItem("orthoskin"))));

		model.setKarmaFree(55);
		model.setKarmaInvested(31);
		SkillValue val = model.getSkillValue(CON);
		val.setPoints(8);
		assertTrue("Increase impossible although enough Karma", control.canBeIncreased(val));
		val.setPoints(9);
		// Try to increase above max
		model.setKarmaFree(60);
		charGen.runProcessors();
		assertFalse("Increase possible above max", control.canBeIncreased(val));
		assertFalse("Increase possible above max", control.increase(val));
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(9, val.getPoints());
		assertEquals(1, val.getModifier());
		assertEquals(10, val.getModifiedValue());
		assertEquals(0, val.getMaximumModifier());
		// Raise maximum and try again
		unitTestMods.clear();
		unitTestMods.add(new SkillModification(ModificationValueType.MAX, CON, 1));
		unitTestMods.add(new SkillModification(CON, 1, new CarriedItem(ShadowrunCore.getItem("orthoskin"))));
		charGen.runProcessors();
		applyUnitTestModifications(val);
		assertTrue("Increase impossible although enough Karma", control.canBeIncreased(val));
		assertTrue("Increase possible above max", control.increase(val));
		applyUnitTestModifications(val);
		assertEquals(4, val.getStart());
		assertEquals(10, val.getPoints());
		assertEquals(1, val.getMaximumModifier());
		assertEquals(1, val.getModifier());
		assertEquals(11, val.getModifiedValue());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void normalSpecialize() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		
		SkillValue val = model.getSkillValue(CON);
		val.setPoints(0);
		assertTrue("Skill has specializations - it should be able", control.canSpecializeIn(val));
		assertFalse("Should not be able to specialize in skill with 0 rating", control.canBeSelected(val, CON.getSpecialization("disguise"), false));
		assertFalse("Should not be able to expertise in skill with 0 rating", control.canBeSelected(val, CON.getSpecialization("disguise"), true));
		val.setPoints(1);
		assertTrue("Should be able to specialize in skill with 1 rating", control.canBeSelected(val, CON.getSpecialization("disguise"), false));
		assertFalse("Should not be able to expertise in skill with 1 rating", control.canBeSelected(val, CON.getSpecialization("disguise"), true));
		
		assertEquals(1, model.getHistory().size());
		SkillSpecializationValue spec = control.select(val, CON.getSpecialization("disguise"), false);
		assertEquals(2, model.getHistory().size());
		assertNotNull("Should be able to specialize in skill with 1 rating", spec);
		assertEquals(CON.getSpecialization("disguise"), spec.getSpecial());
		assertFalse(spec.isExpertise());
		assertEquals(10, model.getKarmaFree());
		assertEquals(36, model.getKarmaInvested());
		assertEquals(2, model.getHistory().size());
		assertTrue(model.getHistory().get(1) instanceof SkillSpecializationModification);
		assertEquals(1, ((SkillSpecializationModification)model.getHistory().get(1)).getValue() );
		assertFalse(((SkillSpecializationModification)model.getHistory().get(1)).isExpertise() );
		assertEquals("Invalid Karma cost in history", 5, ((SkillSpecializationModification)model.getHistory().get(1)).getExpCost() );
		
		// Should not be able to get an expertise unless there are 5 points
		assertFalse("Should not be able to specialize in skill with 1 rating", control.canBeSelected(val, CON.getSpecialization("disguise"), true));
		val.setPoints(5);
		assertTrue("Should be able to specialize in skill with 5 rating", control.canBeSelected(val, CON.getSpecialization("disguise"), true));
		spec = control.select(val, CON.getSpecialization("disguise"), true);
		assertNotNull(spec);
		assertEquals(CON.getSpecialization("disguise"), spec.getSpecial());
		assertTrue(spec.isExpertise());
		assertEquals(5, model.getKarmaFree());
		assertEquals(41, model.getKarmaInvested());
		assertEquals(3, model.getHistory().size());
		assertTrue(model.getHistory().get(2) instanceof SkillSpecializationModification);
		assertEquals(2, ((SkillSpecializationModification)model.getHistory().get(2)).getValue() );
		assertTrue(((SkillSpecializationModification)model.getHistory().get(2)).isExpertise() );
		assertEquals("Invalid Karma cost in history", 5, ((SkillSpecializationModification)model.getHistory().get(2)).getExpCost() );
	}
	
	//-------------------------------------------------------------------
	@Test
	public void normalUnspecialize() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		SkillSpecialization spec = CON.getSpecialization("disguise");
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		SkillSpecializationModification specMod = new SkillSpecializationModification(CON, spec);
		specMod.setExpCost(5);
		model.addToHistory(specMod);
		
		SkillValue val = model.getSkillValue(CON);
		val.setPoints(1);
		val.addSpecialization(new SkillSpecializationValue(spec));
		
		// Test starts
		assertTrue(control.canBeDeselected(val, spec, false));
		assertFalse("Should not be able to deselect something that isn't there",control.canBeDeselected(val, spec, true));
		// Really deselect
		assertTrue(control.deselect(val, spec, false));
		assertEquals("Not removed from history", 1, model.getHistory().size());
		assertEquals("Karma not refunded", 20, model.getKarmaFree());
		assertEquals("Refunded karma not removed", 26, model.getKarmaInvested());
	}
	
	//-------------------------------------------------------------------
	@Test
	public void normalUnexpertize() throws IOException {
		control.setDecreaseBelowCreation(false);
		control.setDontRefund(false);
		control.setRefundBelowCreation(false);
		
		SkillSpecialization spec = CON.getSpecialization("disguise");
		
		model.setKarmaFree(15);
		model.setKarmaInvested(31);
		SkillSpecializationModification specMod = new SkillSpecializationModification(CON, spec);
		specMod.setExpertise(true);
		specMod.setExpCost(5);
		model.addToHistory(specMod);
		
		SkillValue val = model.getSkillValue(CON);
		val.setPoints(5);
		SkillSpecializationValue specVal = new SkillSpecializationValue(spec);
		specVal.setExpertise(true);
		val.addSpecialization(specVal);
		
		// Test starts
		assertFalse(control.canBeDeselected(val, spec, false));
		assertTrue("Should not be able to deselect something that isn't there",control.canBeDeselected(val, spec, true));
		// Really deselect
		assertTrue(control.deselect(val, spec, true));
		assertEquals("Karma not refunded", 20, model.getKarmaFree());
		assertEquals("Refunded karma not removed", 26, model.getKarmaInvested());
		assertEquals(1, model.getHistory().size());
		
		specVal = val.getSpecialization(spec);
		assertNotNull(specVal);
		assertFalse(specVal.isExpertise());
	}
	
}

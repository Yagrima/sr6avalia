/**
 * 
 */
package org.prelle.shadowrun6.gen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Lifestyle;
import org.prelle.shadowrun6.LifestyleOption;
import org.prelle.shadowrun6.LifestyleOptionValue;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SingleLifestyleGenerator implements SingleLifestyleBuilder {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	protected ShadowrunCharacter model;

	private LifestyleValue data;
	
	//-------------------------------------------------------------------
	public SingleLifestyleGenerator(ShadowrunCharacter model) {
		if (model==null)
			throw new NullPointerException("Model is null");
		this.model = model;
		
		data = new LifestyleValue();
		data.setLifestyle(ShadowrunCore.getLifestyles().get(0));
		data.setPaidMonths(1);
	}
	
	//-------------------------------------------------------------------
	public SingleLifestyleGenerator(ShadowrunCharacter model, LifestyleValue life) {
		if (model==null)
			throw new NullPointerException("Model is null");
		this.model = model;
		
		data = life;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<ToDoElement>();
		if (model.getLifestyle().isEmpty())
			ret.add(new ToDoElement(Severity.INFO, RES.getString("lifestylegen.todo.missing_primary_lifestyle")));
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.LifestyleController#getAvailableLifestyles()
	 */
	@Override
	public List<Lifestyle> getAvailableLifestyles() {
		return ShadowrunCore.getLifestyles();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#canSelectLifestyle(org.prelle.shadowrun6.Lifestyle)
	 */
	@Override
	public boolean canSelectLifestyle(Lifestyle lifestyle) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#selectLifestyle(org.prelle.shadowrun6.Lifestyle)
	 */
	@Override
	public void selectLifestyle(Lifestyle lifestyle) {
		data.setLifestyle(lifestyle);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.LifestyleController#getAvailableOptions(org.prelle.shadowrun6.Lifestyle)
	 */
	@Override
	public List<LifestyleOption> getAvailableOptions() {
		return ShadowrunCore.getLifestyleOptions();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#canSelectOption(org.prelle.shadowrun6.LifestyleOption)
	 */
	@Override
	public boolean canSelectOption(LifestyleOption option) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#selectOption(org.prelle.shadowrun6.LifestyleOption)
	 */
	@Override
	public LifestyleOptionValue selectOption(LifestyleOption option) {
		LifestyleOptionValue val = new LifestyleOptionValue(option);
		data.addOption(val);
		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#deselectOption(org.prelle.shadowrun6.LifestyleOptionValue)
	 */
	@Override
	public void deselectOption(LifestyleOptionValue option) {
		data.removeOption(option);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#selectSIN(org.prelle.shadowrun6.SIN)
	 */
	@Override
	public void selectSIN(SIN sin) {
		data.setSIN(sin.getUniqueId());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#selectName(java.lang.String)
	 */
	@Override
	public void selectName(String name) {
		data.setName(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#selectDescription(java.lang.String)
	 */
	@Override
	public void selectDescription(String text) {
		data.setDescription(text);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.LifestyleController#getLifestyleCost(org.prelle.shadowrun6.Lifestyle, java.util.List)
	 */
	@Override
	public int getLifestyleCost() {
		List<LifestyleOption> options = new ArrayList<>();
		for (LifestyleOptionValue opt : data.getOptions())
			options.add(opt.getOption());
		
		return ShadowrunTools.getLifestyleCost(model, data.getLifestyle(), options, new ArrayList<>());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SingleLifestyleBuilder#getResult()
	 */
	@Override
	public LifestyleValue getResult() {
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

}

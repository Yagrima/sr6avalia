
/**
 * 
 */
package org.prelle.shadowrun6.gen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;

import de.rpgframework.ConfigContainer;

/**
 * @author prelle
 *
 */
public class CharacterGeneratorRegistry {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen");
	
	private static List<CharacterGenerator> generators;
	private static CharacterGenerator selected;
	private static ConfigContainer addBelow;
	
	//-------------------------------------------------------------------
	static {
		generators = new ArrayList<CharacterGenerator>();
	}
	
	//-------------------------------------------------------------------
	public static void attachConfigurationTree(ConfigContainer value) {
		addBelow = value;
		for (CharacterGenerator gen : generators) {
			if (addBelow!=null)
				gen.attachConfigurationTree(addBelow);
		}
	}

	//-------------------------------------------------------------------
	public static void register(CharacterGenerator charGen) {
		for (CharacterGenerator tmp : generators) {
			if (tmp==charGen || tmp.getClass()==charGen.getClass())
				return;
		}
		
//		if (!generators.contains(charGen)) {
			generators.add(charGen);
			if (addBelow!=null)
				charGen.attachConfigurationTree(addBelow);
//		}
	}

	//-------------------------------------------------------------------
	public static List<CharacterGenerator> getGenerators() {
		return new ArrayList<>(generators);
	}

	//-------------------------------------------------------------------
	public static void select(CharacterGenerator charGen, ShadowrunCharacter model) {
		logger.info("Switch used generator from "+selected+" to "+charGen);
		if (charGen==selected) {
			logger.info("Keep same generator - model is generation mode = "+model.isGenerationMode());
			if (!model.isGenerationMode())
				charGen.continueCreation(model);
			else
				charGen.start(model);
			return;
		}
		
		CharacterGeneratorRegistry.selected = charGen;
		if (charGen!=null) {
			logger.info(charGen.getClass()+" as generator selected");
			logger.debug("Call "+charGen.getClass()+".start");
			if (!model.isGenerationMode())
				charGen.continueCreation(model);
			else
				charGen.start(model);
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(
							GenerationEventType.CONSTRUCTIONKIT_CHANGED, 
							charGen));
		}
	}

	//-------------------------------------------------------------------
	public static void selectAndContinue(CharacterGenerator charGen, ShadowrunCharacter model) {
		if (charGen==null)
			throw new NullPointerException("Generator may not be null");
		selected = charGen;
		if (charGen!=null) {
			logger.info("Continue "+model.getName()+" with "+charGen.getClass()+" as generator "+charGen);
			logger.info("Settings are: "+model.getChargenSettings());
			logger.debug("Call "+charGen.getClass()+".continueCreation");
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(
							GenerationEventType.CONSTRUCTIONKIT_CHANGED, 
							charGen));
			charGen.continueCreation(model);
		}
	}

	//--------------------------------------------------------------------
	public static CharacterGenerator getSelected() {
		return selected;
	}

	//--------------------------------------------------------------------
	public static CharacterController get(String chargenUsed) {
		for (CharacterGenerator gen : generators) {
			if (gen.getID().equalsIgnoreCase(chargenUsed))
				return gen;
		}
		return null;
	}

}

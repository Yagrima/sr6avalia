package org.prelle.shadowrun6.gen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.CharacterConcept;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.ComplexFormController;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.charctrl.DecisionToMake;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.charctrl.LifestyleController;
import org.prelle.shadowrun6.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun6.charctrl.MetamagicOrEchoController;
import org.prelle.shadowrun6.charctrl.MetatypeController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.charctrl.SummonableController;
import org.prelle.shadowrun6.chargen.CommonFinalizeNuyenAndKarmaGenerator;
import org.prelle.shadowrun6.chargen.CommonMetaMagicOrEchoController;
import org.prelle.shadowrun6.common.FocusGenerator;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigContainer;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterHandle.Format;
import de.rpgframework.character.CharacterHandle.Type;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public abstract class CommonSR6CharacterGenerator extends BasePluginData implements CharacterGenerator {

	protected final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private static final Logger logger = LogManager.getLogger("shadowrun6.gen");


	protected ShadowrunCharacter model;
	protected CharGenMode mode;

	protected CharacterConcept concept;
	protected MetatypeController meta;
	protected MagicOrResonanceController magOrRes;
	protected AttributeController attrib;
	protected SkillController    skill;
	protected SpellController    spell;
	protected SpellController    alchemy;
	protected RitualController   ritual;
	protected QualityController  quality;
	protected AdeptPowerController power;
	protected ComplexFormController cforms;
	protected EquipmentController   equip;
	protected ConnectionsController connections;
	protected SINController       sins;
	protected LifestyleController lifestyles;
	protected SummonableController summonables;
	protected CommonFinalizeNuyenAndKarmaGenerator nuyen;
	protected CommonMetaMagicOrEchoController metaEchoes;
	protected FocusGenerator foci;

	protected List<CharacterProcessor> processChain;
	protected Map<Modification, DecisionToMake> decisions;

	protected List<Modification> unitTestModifications;

	//-------------------------------------------------------------------
	protected CommonSR6CharacterGenerator() {
		mode = CharGenMode.CREATING;
		processChain = new ArrayList<>();
		decisions    = new HashMap<>();
		unitTestModifications = new ArrayList<>();
		
		nuyen = new CommonFinalizeNuyenAndKarmaGenerator();
	}

	//-------------------------------------------------------------------
	public void addUnitTestModification(Modification mod) {
		unitTestModifications.add(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void removeUnitTestModification(Modification mod) {
		unitTestModifications.remove(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		// To override in child classes
	}

	//-------------------------------------------------------------------
	public void runProcessors() {
		logger.info("\n\nSTART: runProcessors: "+processChain.size()+"-------------------------------------------------------");
		List<Modification> unprocessed = new ArrayList<>(unitTestModifications);
		for (CharacterProcessor processor : processChain) {
			unprocessed = processor.process(model, unprocessed);
			logger.debug("------karma="+model.getKarmaFree()+" nuyen="+model.getNuyen()+" after "+processor.getClass().getSimpleName()+"     "+unprocessed);
		}
		logger.info("Remaining mods  = "+unprocessed);
		logger.info("Remaining karma = "+model.getKarmaFree()+" for "+model.getName());
		logger.info("ToDos = "+getToDos());
		logger.info("STOP : runProcessors: "+processChain.size()+"-------------------------------------------------------");
		logger.info("pp = "+model.getAttribute(Attribute.POWER_POINTS));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getMode()
	 */
	@Override
	public CharGenMode getMode() {
		return mode;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getHelpText()
	 */
	@Override
	public String getHelpText() {
		if (i18nHelp==null)
			return null;
		String key = getHelpI18NKey();

		try {
			return i18nHelp.getString(key);
		} catch (MissingResourceException mre) {
			System.err.println("Missing property '"+key+"' in "+i18nHelp.getBaseBundleName());
			logger.error("Missing property '"+key+"' in "+i18nHelp.getBaseBundleName());
			return "Missing property '"+key+"' in "+i18nHelp.getBaseBundleName();
		}
		//		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getCharacter()
	 */
	@Override
	public ShadowrunCharacter getCharacter() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getMagicOrResonanceController()
	 */
	@Override
	public MagicOrResonanceController getMagicOrResonanceController() {
		return magOrRes;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getMetatypeController()
	 */
	@Override
	public MetatypeController getMetatypeController() {
		return meta;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attrib;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getSpellController()
	 */
	@Override
	public SpellController getSpellController() {
		return spell;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getAlchemyController()
	 */
	@Override
	public SpellController getAlchemyController() {
		return spell;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getRitualController()
	 */
	@Override
	public RitualController getRitualController() {
		return ritual;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getQualityController()
	 */
	@Override
	public QualityController getQualityController() {
		return quality;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getPowerController()
	 */
	@Override
	public AdeptPowerController getPowerController() {
		return power;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getComplexFormController()
	 */
	@Override
	public ComplexFormController getComplexFormController() {
		return cforms;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getEquipmentController()
	 */
	@Override
	public EquipmentController getEquipmentController() {
		return equip;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getConnectionController()
	 */
	@Override
	public ConnectionsController getConnectionController() {
		return connections;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSINController()
	 */
	@Override
	public SINController getSINController() {
		return sins;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getLifestyleController()
	 */
	@Override
	public LifestyleController getLifestyleController() {
		return lifestyles;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSummonableController()
	 */
	@Override
	public SummonableController getSummonableController() {
		return summonables;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getFocusController()
	 */
	@Override
	public FocusController getFocusController() {
		return foci;
	}

	//--------------------------------------------------------------------
	public CharacterConcept getConcept() {
		return concept;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#setConcept(org.prelle.shadowrun6.CharacterConcept)
	 */
	@Override
	public void setConcept(CharacterConcept concept) {
		if (this.concept==concept)
			// Nothing changed
			return;

		if (this.concept!=null) {
			// Remove old concept
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.CHARACTERCONCEPT_REMOVED, this.concept));
		}

		this.concept = concept;
		if (concept!=null) {
			logger.info("Select concept "+concept);
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.CHARACTERCONCEPT_ADDED, concept));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getMetamagicOrEchoController()
	 */
	@Override
	public MetamagicOrEchoController getMetamagicOrEchoController() {
		return metaEchoes;
	}
	
	//-------------------------------------------------------------------
	public void saveCreation() throws IOException {
		model.setGenerationMode(true);
		/*
		 * 1. Convert character into Byte Buffer - or fail
		 */
		byte[] encoded = null;
		try { encoded = ShadowrunCore.save(model); } catch (IOException e) {
			logger.error("Cannot save character, since encoding failed: "+e);
			throw e;
		}

		/*
		 * 2. Use character service to save character
		 */
		try {
//			if (handle==null) {
//				logger.debug("CharacterHandle does not exist yet - prepare it");
				CharacterHandle handle = CharacterProviderLoader.getCharacterProvider().createCharacter(model.getName(), RoleplayingSystem.SHADOWRUN6);
//			}
			logger.info("Save character "+model.getName());
			CharacterProviderLoader.getCharacterProvider().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, null, encoded);
			logger.info("Saved character "+model.getName()+" successfully");
		} catch (IOException e) {
			logger.error("Failed saving character",e);
			throw e;
		}
	}
}

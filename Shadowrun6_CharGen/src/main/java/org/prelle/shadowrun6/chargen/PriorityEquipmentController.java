/**
 *
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.common.CommonEquipmentController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemTemplate.Legality;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.modifications.AddNuyenModification;
import org.prelle.shadowrun6.modifications.SpecialRuleModification;
import org.prelle.shadowrun6.modifications.SpecialRuleModification.Rule;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class PriorityEquipmentController extends CommonEquipmentController implements CharacterProcessor {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR6CharacterGenerator parent;

	//--------------------------------------------------------------------
	public PriorityEquipmentController(CommonSR6CharacterGenerator parent) {
		super(parent, parent.getCharacter(), CharGenMode.CREATING);
		this.parent = parent;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//--------------------------------------------------------------------
	public boolean canIncreaseBoughtNuyen() {
		return model.getKarmaFree()>0;
	}

	//--------------------------------------------------------------------
	public boolean canDecreaseBoughtNuyen() {
		return model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma>0;
	}

	//--------------------------------------------------------------------
	public boolean increaseBoughtNuyen() {
		logger.info("Increase bought nuyen");
		if (!canIncreaseBoughtNuyen()) {
			logger.warn("Tried to increase nuyen with Karma, but that wasn't valid (free karma="+model.getKarmaFree()+")");
			return false;
		}
		
		model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma++;
		logger.info("this="+this);
		logger.info("Generate 2000 nuyen for 1 karma  (now "+model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma+")");
		parent.runProcessors();
		logger.info("Generated 2000 nuyen for 1 karma  (afterwards "+model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma+")");
		return true;
	}

	//--------------------------------------------------------------------
	public boolean decreaseBoughtNuyen() {
		if (!canDecreaseBoughtNuyen())
			return false;
		
		model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma--;
		logger.info("Return 2000 nuyen for 1 karma  (now "+model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma+")");
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	public int getBoughtNuyen() {
		return model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canBeSelected(org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public boolean canBeSelected(ItemTemplate item, UseAs usage, SelectionOption...options) {
		boolean ret = super.canBeSelected(item, usage, options);
		if (!ret)
			return false;
		
		Availability avail = getAvailability(item, item.getNonAccessoryUsage(), options);
		
		if (avail.getLegality()==Legality.FORBIDDEN && avail.getValue()>6)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#select(org.prelle.shadowrun6.items.ItemTemplate, int)
	 */
	@Override
	public CarriedItem select(ItemTemplate item, SelectionOption...options) {
		CarriedItem ret = super.select(item, options);
		if (ret!=null)
			parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#embed(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, ItemHook hook, SelectionOption...options) {
		CarriedItem ret = super.embed(container, item, hook, options);
		if (ret!=null)
			parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#embed(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, SelectionOption...options) {
		CarriedItem ret = super.embed(container, item, options);
		if (ret!=null)
			parent.runProcessors();
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#deselect(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean deselect(CarriedItem ref) {
		logger.debug("Deselect/Undo "+ref);
		boolean removed = super.deselect(ref);
		logger.debug("Deselect/Undo returned "+removed);
		
		if (removed)
			parent.runProcessors();
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#sell(org.prelle.shadowrun6.items.CarriedItem, float)
	 */
	@Override
	public boolean sell(CarriedItem ref, float factor) {
		logger.debug("Sell "+ref+" for "+factor);
		boolean removed = super.sell(ref, factor);
		logger.debug("Sell "+removed);
		if (removed) 
			parent.runProcessors();
		
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#increase(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean increase(CarriedItem data) {
		if (!super.increase(data))
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#decrease(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean decrease(CarriedItem data) {
		if (!super.decrease(data))
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process "+this);
		try {
			// Reset
			todos.clear();
			int nuyenForKarma = 2000;
			boolean inDebt = false;
			
			// Wait for Nuyen from priority
			for (Modification tmp: previous) {
				if (tmp instanceof AddNuyenModification) {
					AddNuyenModification mod = (AddNuyenModification)tmp;
					model.setNuyen(model.getNuyen()+mod.getValue());
					logger.info("Add "+mod.getValue()+" nuyen");
				} else if (tmp instanceof SpecialRuleModification) {
					SpecialRuleModification mod = (SpecialRuleModification)tmp;
					if (mod.getRule()==Rule.IN_DEBT) {
						nuyenForKarma = 5000;
						inDebt = true;
					} else
						unprocessed.add(tmp);
				} else
					unprocessed.add(tmp);
			}
			logger.info("this="+this);
			logger.info("mods prev="+previous);
			logger.info("mods unpr="+unprocessed);
			logger.info("usedKarma="+model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma);
			
			// Convert karma to nuyen
			int moreNuyen = model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma * nuyenForKarma;
			logger.info("  "+moreNuyen+" additional nuyen from Karma ("+nuyenForKarma+" each)");
			model.setNuyen(model.getNuyen()+moreNuyen);
			model.setKarmaFree(model.getKarmaFree() - model.getTemporaryChargenSettings(PrioritySettings.class).usedKarma);
//			costs.setKarmaInvestLimit(maxConvertibleKarma);
//			logger.debug("max convertible karma is "+maxConvertibleKarma);

			// Pay nuyen for gear
			int boughtTotal = 0;
			for (CarriedItem item : model.getItems(false)) {
				int cost = item.getAsValue(ItemAttribute.PRICE).getModifiedValue();
				
				/* Now apply cost multiplier for dwarfs and trolls */
				MetaType meta = model.getMetatype();
				if (meta.getVariantOf()!=null)
					meta = meta.getVariantOf();
				switch (meta.getId()) {
				case "dwarf":
					if (item.isType(ItemType.ARMOR) || item.isType(ItemType.ARMOR_ADDITION)) {
						cost *= 1.1f;
					}
					break;
				case "troll":
					cost *= 1.1f;
					break;
				}

				boughtTotal += cost;
				logger.info("  Pay "+cost+" nuyen for "+item.getNameWithRating());
				model.setNuyen( model.getNuyen() - cost);
			}
			logger.info("  Pay "+boughtTotal+" nuyen for resources - "+model.getNuyen()+" left");
			
			// Pay nuyen for selected foci
			for (FocusValue focus : model.getFoci()) {
				int nuyen = focus.getCostNuyen();
				logger.info("Pay "+nuyen+" nuyen for force "+focus.getLevel()+" focus "+focus);
				model.setNuyen(model.getNuyen() - nuyen);
			}
			
			// If in debt, remember it
			model.setDebt(inDebt?moreNuyen:0);
			if (inDebt) {
				logger.info("Debt: "+model.getDebt());
			}
			
			/*
			 * Inform user when Nuyens are lost
			 */
			logger.info("Nuyen: "+model.getNuyen());
			if (model.getNuyen()>5000) {
				int amount = model.getNuyen()-5000;
				todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "priogen.todo.nuyen", amount)));
			}
			if (model.getNuyen()<0) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.get(RES, "priogen.todo.nuyen.negative")));
			}

		} finally {
			logger.trace("STOP : process()");
		}
		
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(SR6ConfigOptions.HIDE_AUTOGEAR);
	}

}

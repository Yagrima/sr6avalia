/**
 *
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ComplexForm;
import org.prelle.shadowrun6.ComplexFormValue;
import org.prelle.shadowrun6.MentorSpirit;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Spirit;
import org.prelle.shadowrun6.Sprite;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.charctrl.ComplexFormController;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.items.ItemTemplate;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityComplexFormController implements ComplexFormController {
	
	private static final Logger logger = LogManager.getLogger("shadowrun6.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR6CharacterGenerator parent;
	private ShadowrunCharacter model;

	private List<ComplexForm> available;
	private int points;
	private List<ToDoElement> todos;

	//-------------------------------------------------------------------
	public PriorityComplexFormController(CommonSR6CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		todos = new ArrayList<>();
		available = new ArrayList<ComplexForm>();
		updateAvailableComplexForms();
	}

	//--------------------------------------------------------------------
	private void updateAvailableComplexForms() {
		available.clear();
		available.addAll(ShadowrunCore.getComplexForms());
		// Remove those the character already has
		for (ComplexFormValue val : model.getComplexForms()) {
			ComplexForm form = val.getModifyable();
			if (!form.isMultipleSelectable()) {
				available.remove(form);
			}
		}

		Collections.sort(available);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------	
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#getComplexFormsLeft()
	 */
	@Override
	public int getComplexFormsLeft() {
		return points;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#getAvailableComplexForms()
	 */
	@Override
	public List<ComplexForm> getAvailableComplexForms() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#canBeSelected(org.prelle.shadowrun6.ComplexForm)
	 */
	@Override
	public boolean canBeSelected(ComplexForm data) {
		if (data==null)
			throw new NullPointerException();

		// Is it already selected?
		for (ComplexFormValue selected : model.getComplexForms()) {
			if (selected.getModifyable()==data && !selected.getModifyable().isMultipleSelectable())
				return false;
		}

		// Are there points left
		return points>0 || model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#canBeDeselected(org.prelle.shadowrun6.ComplexFormValue)
	 */
	@Override
	public boolean canBeDeselected(ComplexFormValue data) {
		return model.getComplexForms().contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#select(org.prelle.shadowrun6.ComplexForm)
	 */
	@Override
	public ComplexFormValue select(ComplexForm data) {
		if (data.needsChoice())
			throw new IllegalArgumentException("ComplexForm "+data+" needs a choice");
		if (!canBeSelected(data)) {
			logger.warn("Trying to select a complex form that cannot be selected");
			return null;
		}
		logger.info("select "+data.getId());

		ComplexFormValue ref = new ComplexFormValue(data);
		model.addComplexForm(ref);
		parent.runProcessors();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#select(org.prelle.shadowrun6.ComplexForm)
	 */
	@Override
	public ComplexFormValue select(ComplexForm data, Object choice) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select a complex form that cannot be selected");
			return null;
		}
		logger.info("select "+data.getId());

		ComplexFormValue ref = new ComplexFormValue(data);
		model.addComplexForm(ref);
		ref.setChoice(choice);
		switch (data.getChoice()) {
		case ATTRIBUTE:
		case PHYSICAL_ATTRIBUTE:
			ref.setChoiceReference(  ((Attribute)choice).name() );
			break;
		case COMBAT_SKILL:
		case SKILL:
			ref.setChoiceReference(  ((Skill)choice).getId() );
			break;
		case MATRIX_ACTION:
			ref.setChoiceReference(  ((ShadowrunAction)choice).getId() );
			break;
		case MATRIX_ATTRIBUTE:
			ref.setChoiceReference(  ((Attribute)choice).name() );
			break;
		case MENTOR_SPIRIT:
			ref.setChoiceReference(  ((MentorSpirit)choice).getId() );
			ref.setChoice( (MentorSpirit)choice );
			break;
		case PROGRAM:
			ref.setChoiceReference(  ((ItemTemplate)choice).getId() );
			ref.setChoice( (ItemTemplate)choice );
			break;
		case SPIRIT:
			if (choice instanceof Spirit) {
				ref.setChoiceReference(  ((Spirit)choice).getId() );
			} else if (choice instanceof Sprite) {
				ref.setChoiceReference(  ((Sprite)choice).getId() );
			} else if (choice instanceof String) {
				Spirit spirit = ShadowrunCore.getSpirit(choice.toString());
				if (spirit==null) {
					Sprite sprite = ShadowrunCore.getSprite(choice.toString());
					if (sprite==null) {
						logger.error("Can't find a spirit or sprite with ID '"+choice+"'");
						throw new IllegalArgumentException("Can't find a spirit or sprite with ID '"+choice+"'");
					} else
						ref.setChoice(sprite);
				} else {
					ref.setChoice(spirit);
				}
				ref.setChoiceReference(choice.toString());
			}
			break;
		default:
			logger.error("Don't know how to set reference for choice type "+data.getChoice()+" and choice '"+choice+"'");
			throw new IllegalArgumentException("Don't know how to set reference for choice type "+data.getChoice()+" and choice '"+choice+"'");
		}

		parent.runProcessors();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#deselect(org.prelle.shadowrun6.ComplexFormValue)
	 */
	@Override
	public void deselect(ComplexFormValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return;
		}
		logger.debug("select "+data);

		model.removeComplexForm(data);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		try {
			todos.clear();
			points = 0;
			
			// Process Modifications
			for (Modification tmp : previous) {
				if (tmp instanceof FreePointsModification) {
					FreePointsModification mod = (FreePointsModification)tmp;
					if (mod.getType()==Type.COMPLEX_FORMS) {
						points += mod.getCount();
					} else
						unprocessed.add(tmp);
				} else
					unprocessed.add(tmp);
			}
			
			logger.debug("  Points to spend on complex forms: "+points);
			// Pay spells
			for (ComplexFormValue val : model.getComplexForms()) {
				if (points>0) {
					logger.debug("  Get complex form "+val.getModifyable()+" for free");
					points--;
				} else {
					logger.info("  Invest 5 karma for complex form "+val.getModifyable());
					model.setKarmaFree(model.getKarmaFree()-5);
					model.setKarmaInvested(model.getKarmaInvested()+5);
				}
			}
			logger.debug("Free complex forms left: "+points);
			
			updateAvailableComplexForms();
			
			// ToDos
			if (points>0) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.format(RES, "priogen.todo.complexforms", points)));
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
		
	}

}

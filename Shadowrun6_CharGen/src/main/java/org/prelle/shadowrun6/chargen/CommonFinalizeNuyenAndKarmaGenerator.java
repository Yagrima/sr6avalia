/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.Controller;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CommonFinalizeNuyenAndKarmaGenerator implements CharacterProcessor, Controller {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen");

	private final static PropertyResourceBundle RES = (PropertyResourceBundle) ShadowrunCharGenConstants.RES;
	
	private List<ToDoElement> todos;
	
	//-------------------------------------------------------------------
	public CommonFinalizeNuyenAndKarmaGenerator() {
		todos     = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		try {
			todos.clear();
			/*
			 * Remaining karma
			 */
			if (model.getKarmaFree()>2) {
				todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "priogen.todo.karma", model.getKarmaFree())));
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

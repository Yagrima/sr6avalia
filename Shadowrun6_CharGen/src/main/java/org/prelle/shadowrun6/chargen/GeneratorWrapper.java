package org.prelle.shadowrun6.chargen;

import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.CharacterConcept;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.ComplexFormController;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.charctrl.LifestyleController;
import org.prelle.shadowrun6.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun6.charctrl.MetamagicOrEchoController;
import org.prelle.shadowrun6.charctrl.MetatypeController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.charctrl.SummonableController;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.WizardPageType;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.gen.event.GenerationEventType;

import de.rpgframework.ConfigContainer;
import de.rpgframework.character.RulePlugin;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan Prelle
 *
 */
public class GeneratorWrapper implements CharacterGenerator, GenerationEventListener {
	
	private CharacterGenerator generator;

	//-------------------------------------------------------------------
	public GeneratorWrapper(CharacterGenerator generator) {
		if (generator==null)
			throw new NullPointerException("Generator may not be null");
		this.generator = generator;
		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.CONSTRUCTIONKIT_CHANGED) {
			generator = (CharacterGenerator) event.getKey();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getMode()
	 */
	@Override
	public CharGenMode getMode() {
		return generator.getMode();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getName()
	 */
	@Override
	public String getName() {
		return generator.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#runProcessors()
	 */
	@Override
	public void runProcessors() {
		generator.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getCharacter()
	 */
	@Override
	public ShadowrunCharacter getCharacter() {
		return generator.getCharacter();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return generator.getAttributeController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return generator.getSkillController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSpellController()
	 */
	@Override
	public SpellController getSpellController() {
		return generator.getSpellController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getAlchemyController()
	 */
	@Override
	public SpellController getAlchemyController() {
		return generator.getAlchemyController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getRitualController()
	 */
	@Override
	public RitualController getRitualController() {
		return generator.getRitualController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getQualityController()
	 */
	@Override
	public QualityController getQualityController() {
		return generator.getQualityController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getPowerController()
	 */
	@Override
	public AdeptPowerController getPowerController() {
		return generator.getPowerController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getComplexFormController()
	 */
	@Override
	public ComplexFormController getComplexFormController() {
		return generator.getComplexFormController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getEquipmentController()
	 */
	@Override
	public EquipmentController getEquipmentController() {
		return generator.getEquipmentController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getConnectionController()
	 */
	@Override
	public ConnectionsController getConnectionController() {
		return generator.getConnectionController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSINController()
	 */
	@Override
	public SINController getSINController() {
		return generator.getSINController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getLifestyleController()
	 */
	@Override
	public LifestyleController getLifestyleController() {
		return generator.getLifestyleController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSummonableController()
	 */
	@Override
	public SummonableController getSummonableController() {
		return generator.getSummonableController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getProductName()
	 */
	@Override
	public String getProductName() {
		return generator.getProductName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getProductNameShort()
	 */
	@Override
	public String getProductNameShort() {
		return generator.getProductNameShort();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getPage()
	 */
	@Override
	public int getPage() {
		return generator.getPage();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.HardcopyPluginData#getHelpText()
	 */
	@Override
	public String getHelpText() {
		return generator.getHelpText();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getId()
	 */
	@Override
	public String getId() {
		return getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#setPlugin(de.rpgframework.RulePlugin)
	 */
	@Override
	public void setPlugin(RulePlugin<?> plugin) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getPlugin()
	 */
	@Override
	public RulePlugin<?> getPlugin() {
		return generator.getPlugin();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getResourceBundle()
	 */
	@Override
	public ResourceBundle getResourceBundle() {
		return generator.getResourceBundle();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.PluginData#getHelpResourceBundle()
	 */
	@Override
	public ResourceBundle getHelpResourceBundle() {
		return generator.getHelpResourceBundle();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
		generator.attachConfigurationTree(addBelow);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#start(org.prelle.shadowrun6.ShadowrunCharacter)
	 */
	@Override
	public void start(ShadowrunCharacter model) {
		generator.start(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#stop()
	 */
	@Override
	public void stop() {
		generator.stop();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		return generator.getWizardPages();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#hasEnoughData()
	 */
	@Override
	public boolean hasEnoughData() {
		return generator.hasEnoughData();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#hasEnoughDataIgnoreMoney()
	 */
	@Override
	public boolean hasEnoughDataIgnoreMoney() {
		return generator.hasEnoughDataIgnoreMoney();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return generator.getToDos();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#setConcept(org.prelle.shadowrun6.CharacterConcept)
	 */
	@Override
	public void setConcept(CharacterConcept concept) {
		generator.setConcept(concept);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getConcept()
	 */
	@Override
	public CharacterConcept getConcept() {
		return generator.getConcept();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getMetatypeController()
	 */
	@Override
	public MetatypeController getMetatypeController() {
		return generator.getMetatypeController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getMagicOrResonanceController()
	 */
	@Override
	public MagicOrResonanceController getMagicOrResonanceController() {
		return generator.getMagicOrResonanceController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getMetamagicOrEchoController()
	 */
	@Override
	public MetamagicOrEchoController getMetamagicOrEchoController() {
		return generator.getMetamagicOrEchoController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getID()
	 */
	@Override
	public String getID() {
		return "none";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getFocusController()
	 */
	@Override
	public FocusController getFocusController() {
		return generator.getFocusController();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#continueCreation(org.prelle.shadowrun6.ShadowrunCharacter)
	 */
	@Override
	public void continueCreation(ShadowrunCharacter model) {
		generator.continueCreation(model);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#saveCreation()
	 */
	@Override
	public void saveCreation() throws IOException {
		generator.saveCreation();
	}

}

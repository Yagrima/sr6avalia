package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.MetamagicOrEcho.Type;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.MetamagicOrEchoController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.MetamagicOrEchoModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan Prelle
 *
 */
public class CommonMetaMagicOrEchoController implements MetamagicOrEchoController, CharacterProcessor {

	protected final static Logger logger = LogManager.getLogger("shadowrun6.gen");

	protected ProcessorRunner parent;
	protected ShadowrunCharacter model;
	protected List<MetamagicOrEcho> available;
	protected List<ToDoElement> todos;
	protected CharGenMode mode;

	//-------------------------------------------------------------------
	public CommonMetaMagicOrEchoController(CharacterController parent, CharGenMode mode) {
		this.parent = parent;
		this.mode   = mode;
		model = parent.getCharacter();
		available = new ArrayList<>();
		todos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(SR6ConfigOptions.METAECHOES_DONT_REFUND, SR6ConfigOptions.METAECHOES_IGNORE_REFUND_ORDER);
	}

	//-------------------------------------------------------------------
	protected void updateAvailable() {
		// Update available
		available.clear();
		if (model==null)
			return;
		MagicOrResonanceType mor = model.getMagicOrResonanceType();
		if (mor==null)
			return;

		for (MetamagicOrEcho tmp : ShadowrunCore.getMetamagicOrEchoes()) {
			if (model.hasMetamagicOrEcho(tmp.getId()))
				continue;
			switch (tmp.getType()) {
			case ECHO:
				if (mor.usesResonance())
					available.add(tmp);
				break;
			case METAMAGIC_ADEPT:
				if (mor.usesPowers())
					available.add(tmp);
				break;
			case METAMAGIC:
				if (mor.usesMagic())
					available.add(tmp);
				break;
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		//		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			todos.clear();

			// Apply modifications from 
			
			updateAvailable();
			logger.debug("Available metamagic/echoes = "+available);
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#getAvailableMetamagics()
	 */
	@Override
	public List<MetamagicOrEcho> getAvailableMetamagics() {
		return available;
	}

	//-------------------------------------------------------------------
	private int getKarmaRequirement() {
		return 10+(model.getInitiateSubmersionLevel()+1);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#canBeSelected(org.prelle.shadowrun6.MetamagicOrEcho)
	 */
	@Override
	public boolean canBeSelected(MetamagicOrEcho data) {
		// Not more than current Magic or Resonance rating
		Attribute attrib = (data.getType()==Type.ECHO)?Attribute.RESONANCE:Attribute.MAGIC;
		int maxAmount = model.getAttribute(attrib).getModifiedValue();
		int current = 0;
		for (MetamagicOrEchoValue tmp :  model.getMetamagicOrEchoes()) {
			if (tmp.getModifyable().hasLevels())
					current+=tmp.getLevel();
			else
				current++;
		}
		if (current>=maxAmount)
			return false;
		
		// Required karma
		int karma = 10+(model.getInitiateSubmersionLevel()+1);
		if (model.getKarmaFree()<karma)
			return false;
		// Generally available
		return available.contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#select(org.prelle.shadowrun6.MetamagicOrEcho)
	 */
	@Override
	public MetamagicOrEchoValue select(MetamagicOrEcho data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select MetamagicOrEcho '"+data+"' which cannot be selected");
			return null;
		}
		
		MetamagicOrEchoValue ref = new MetamagicOrEchoValue(data);
		if (data.hasLevels())
			ref.setLevel(1);
		
		int karma = getKarmaRequirement();
		model.setKarmaFree( model.getKarmaFree() - karma);
		model.setKarmaInvested(model.getKarmaInvested() + karma);
		logger.info("Bought "+data+" for "+karma+" Karma");
		model.addMetamagicOrEcho(ref);
		
		// Add to history
		if (!(this instanceof CommonMetamagicOrEchoGenerator)) {
			MetamagicOrEchoModification mod = new MetamagicOrEchoModification(data);
			mod.setExpCost(karma);
			mod.setSource(this);
			mod.setDate(new Date(System.currentTimeMillis()));
			model.addToHistory(mod);
		}

		parent.runProcessors();
		
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#canBeDeselected(org.prelle.shadowrun6.MetamagicOrEchoValue)
	 */
	@Override
	public boolean canBeDeselected(MetamagicOrEchoValue ref) {
		// Character must have selected option, to be able to deselect it
		if ( !model.getMetamagicOrEchoes().contains(ref) ) {
			return false;
		}
		// For a option with levels, levels must be decreased first
		if (ref.getLevel()>1) {
			return false;
		}
		// If choice to remove was the latest addition, allow it 
		MetamagicOrEchoModification latest = getHighestModification();
		if (latest!=null && ref.getModifyable()==latest.getMetamagicOrEcho())
			return true;
		
		boolean ignoreRefundOrder = (Boolean)SR6ConfigOptions.METAECHOES_IGNORE_REFUND_ORDER.getValue();
		// Otherwise, only if configured by settings
		return ignoreRefundOrder || mode==CharGenMode.CREATING;
	}

	//-------------------------------------------------------------------
	private MetamagicOrEchoModification getModification(MetamagicOrEchoValue key) {
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof MetamagicOrEchoModification))
				continue;
			MetamagicOrEchoModification amod = (MetamagicOrEchoModification)mod;
			if (amod.getMetamagicOrEcho()==key.getModifyable())
				return amod;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * Get the last modification made to Metamagic or Echoes
	 */
	private MetamagicOrEchoModification getHighestModification() {
		int lastVal = 0;
		MetamagicOrEchoModification last = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof MetamagicOrEchoModification))
				continue;
			MetamagicOrEchoModification amod = (MetamagicOrEchoModification)mod;
			if (amod.getExpCost()>lastVal) {
				lastVal = amod.getExpCost();
				last = amod;
			}
		}
		return last;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#deselect(org.prelle.shadowrun6.MetamagicOrEchoValue)
	 */
	@Override
	public boolean deselect(MetamagicOrEchoValue ref) {
		if (!canBeDeselected(ref)) {
			logger.warn("Trying to deselect "+ref+" which cannot be deselected");
			return false;			
		}
		
		model.removeMetamagicOrEcho(ref);
		
		MetamagicOrEchoModification oldMod = getModification(ref);
		model.removeFromHistory(oldMod);

		if (!(this instanceof CommonMetamagicOrEchoGenerator)) {
			// Levelling
			boolean dontRefund = (Boolean)SR6ConfigOptions.METAECHOES_DONT_REFUND.getValue();
			boolean ignoreRefundOrder = (Boolean)SR6ConfigOptions.METAECHOES_IGNORE_REFUND_ORDER.getValue();
			if (!dontRefund) {
				/*
				 * Refund invested Karma
				 * If order is ignored, refund the highest possible Karma,
				 * otherwise the Karma that once has been invested.
				 */
				int karma = 0;
				if (ignoreRefundOrder || oldMod==null) {
					karma = getKarmaRequirement();
				} else {
					karma = oldMod.getExpCost();
				}
				model.setKarmaFree( model.getKarmaFree() + karma);
				model.setKarmaInvested(model.getKarmaInvested() - karma);
				logger.info("Refund "+ref.getModifyable().getId()+" for "+karma+" Karma");
			}
		}
		
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#canBeIncreased(org.prelle.shadowrun6.MetamagicOrEchoValue)
	 */
	@Override
	public boolean canBeIncreased(MetamagicOrEchoValue data) {
		if (!model.getMetamagicOrEchoes().contains(data)) 
			return false;

		// Required karma
		int karma = 10+(model.getInitiateSubmersionLevel()+1);
		if (model.getKarmaFree()<karma) 
			return false;
		
		if (data.getModifyable().getType()==Type.ECHO) {
			// Submersion level may not be higher than resonance rating
			if (model.getInitiateSubmersionLevel()>=model.getAttribute(Attribute.RESONANCE).getModifiedValue())
				return false;
		} else {
			// Initiate level may not be higher than magic rating
			if (model.getInitiateSubmersionLevel()>=model.getAttribute(Attribute.MAGIC).getModifiedValue())
				return false;
		}

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#increase(org.prelle.shadowrun6.MetamagicOrEchoValue)
	 */
	@Override
	public boolean increase(MetamagicOrEchoValue data) {
		if (!canBeIncreased(data)) {
			logger.warn("Trying to increase MetamagicOrEcho '"+data+"' which cannot be increased");
			return false;
		}

		int karma = getKarmaRequirement();
		model.setKarmaFree( model.getKarmaFree() - karma);
		model.setKarmaInvested(model.getKarmaInvested() + karma);
		logger.info("Increased "+data+" for "+karma+" Karma");
		data.setLevel(data.getLevel()+1);
		
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#canBeDecreased(org.prelle.shadowrun6.MetamagicOrEchoValue)
	 */
	@Override
	public boolean canBeDecreased(MetamagicOrEchoValue data) {
		return model.getMetamagicOrEchoes().contains(data) && data.getModifyable().hasLevels();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetamagicOrEchoController#decrease(org.prelle.shadowrun6.MetamagicOrEchoValue)
	 */
	@Override
	public boolean decrease(MetamagicOrEchoValue data) {
		if (!canBeDecreased(data)) {
			logger.warn("Trying to decrease MetamagicOrEcho '"+data+"' which cannot be decreased");
			return false;
		}

		int karma = 10 + model.getInitiateSubmersionLevel();
		model.setKarmaFree( model.getKarmaFree() + karma);
		model.setKarmaInvested(model.getKarmaInvested() - karma);
		logger.info("Decreased "+data+" for "+karma+" Karma");
		data.setLevel(data.getLevel()-1);
		
		if (data.getLevel()==0)
			model.removeMetamagicOrEcho(data);
		
		parent.runProcessors();
		
		return true;
	}

}

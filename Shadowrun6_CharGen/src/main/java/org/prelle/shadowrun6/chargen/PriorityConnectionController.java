/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityConnectionController implements ConnectionsController, CharacterProcessor {
	
	private static final Logger logger = LogManager.getLogger("shadowrun6.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	protected CommonSR6CharacterGenerator parent;
	protected ShadowrunCharacter model;

	private List<ToDoElement> todos;
	private int pointsLeft;
	
	//-------------------------------------------------------------------
	/**
	 */
	public PriorityConnectionController(CommonSR6CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		todos = new ArrayList<>();
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return pointsLeft;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canCreateConnection()
	 */
	@Override
	public boolean canCreateConnection() {
		if (getPointsLeft()<2)
			return false;
				
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#createConnection()
	 */
	@Override
	public Connection createConnection() {
		if (!canCreateConnection()) {
			logger.warn("Trying to create new connection which is not allowed");
			return null;
		}
		
		Connection ref =  new Connection("?", "?", 1, 1);
		// Add connection
		model.addConnection(ref);		
		
		// Recalc
		parent.runProcessors();
		
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#removeConnection(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public void removeConnection(Connection con) {
		model.removeConnection(con);
		parent.runProcessors();
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canIncreaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canIncreaseInfluence(Connection con) {
		// Augmented max or invested max ?
		int max = model.getAttribute(Attribute.CHARISMA).getGenerationValue();
		
		if (con.getInfluence()>=max)
			return false;
		
		return getPointsLeft()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#increaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean increaseInfluence(Connection con) {
		if (!canIncreaseInfluence(con))
			return false;
		
		con.setInfluence(con.getInfluence()+1);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canDecreaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canDecreaseInfluence(Connection con) {
		return con.getInfluence()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#decreaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean decreaseInfluence(Connection con) {
		if (!canDecreaseInfluence(con))
			return false;
		
		if (con.getInfluence()==1) {
			// Add connection
			model.removeConnection(con);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));			
		} else {
			con.setInfluence(con.getInfluence()-1);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		}
		
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canIncreaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canIncreaseLoyalty(Connection con) {
		// Augmented max or invested max ?
		int max = model.getAttribute(Attribute.CHARISMA).getGenerationValue();
		
		if (con.getLoyalty()>=max)
			return false;
		
		return getPointsLeft()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#increaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean increaseLoyalty(Connection con) {
		if (!canIncreaseLoyalty(con))
			return false;
		
		logger.info("Increase loyalty of "+con);
		con.setLoyalty(con.getLoyalty()+1);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canDecreaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canDecreaseLoyalty(Connection con) {
		return con.getLoyalty()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#decreaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean decreaseLoyalty(Connection con) {
		if (!canDecreaseLoyalty(con))
			return false;
		
		if (con.getLoyalty()==1) {
			// Add connection
			model.removeConnection(con);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_REMOVED, con));			
		} else {
			con.setLoyalty(con.getLoyalty()-1);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CONNECTION_CHANGED, con));
		}
		
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
//		List<String> ret = new ArrayList<String>();
//		if (getPointsLeft()>0) {
//			ret.add(String.format(RES.getString("priogen.todo.connection"), getPointsLeft()));
//		}
//		
//		// Check connections are in range
//		for (Connection con : model.getConnections()) {
//			if (con.getInfluence()>6 && !allowHighFriends())
//				ret.add(String.format(RES.getString("priogen.todo.connection.highinfluence"), con.getName()));
//			else if ( (con.getLoyalty()+con.getInfluence())>7 && !allowHighFriends())
//				ret.add(String.format(RES.getString("priogen.todo.connection.hightotal"), con.getName()));
//		}
//		return ret;
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		List<Modification> unprocessed = new ArrayList<>();

		try {
			todos.clear();
			pointsLeft = model.getAttribute(Attribute.CHARISMA).getModifiedValue()*6;
			
			for (Connection con : model.getConnections()) {
				int cost = con.getInfluence() + con.getLoyalty();
				logger.info("Pay "+cost+" connections points on "+con);
				pointsLeft -= cost;
				// ToDos
				if ("?".equals(con.getName())) {
					todos.add(new ToDoElement(Severity.INFO, Resource.get(RES, "priogen.todo.contacts.noname")));
				}
			}

			if (pointsLeft>0) {
				todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "priogen.todo.contacts.tospend", pointsLeft)));
			} else if (pointsLeft<0) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.format(RES, "priogen.todo.contacts.toomuch", pointsLeft)));
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return previous;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

}

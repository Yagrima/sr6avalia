/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.Quality.QualityType;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.common.CommonQualityController;
import org.prelle.shadowrun6.modifications.QualityModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;
import org.prelle.shadowrun6.requirements.Requirement;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.DecisionToMake;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CommonQualityGenerator extends CommonQualityController implements QualityController, CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen");

	protected List<Quality> available;
	private int netBonusKarma;
	private boolean ignoreKarma;
	
	protected List<DecisionToMake> decisions;
	
	//-------------------------------------------------------------------
	public CommonQualityGenerator(CharacterController parent, CharGenMode mode) {
		super(parent, mode);
		available = new ArrayList<>();
		decisions = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#getNetBonusKarma()
	 */
	@Override
	public int getNetBonusKarma() {
		return netBonusKarma;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#getAvailableQualities()
	 */
	@Override
	public List<Quality> getAvailableQualities() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#canBeSelected(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public boolean canBeSelected(Quality data) {
		if (data==null)
			throw new NullPointerException("Quality not set");
		if (!data.isFreeSelectable())
			throw new NullPointerException("Quality only selectable by metatype");
		if (data.isMetagenetic())
			throw new NullPointerException("Quality only selectable for changelings");
		// Not already selected
		if (model.getQualities().stream().anyMatch(qval -> (qval.getModifyable()==data && !data.isMultipleSelectable()))) {
			return false;
		}
		
		// Are there any requirements that must be fulfilled
		for (Requirement req : data.getPrerequisites()) {
			boolean isMet = ShadowrunTools.isRequirementMet(req, model);
			if (!isMet) {
				// Requirement not met - cannot be selected
				return false;
			}
		}
		
		
		// Points left
		if (ignoreKarma)
			return true;
		
		if (data.getType()==QualityType.POSITIVE)
			return model.getKarmaFree()>=(getKarmaCost(data));
		else
			return true;
	}

	//-------------------------------------------------------------------
	protected int getKarmaCost(Quality data) {
		return data.getCost();
	}

	//-------------------------------------------------------------------
	protected int getKarmaCost(QualityValue data) {
		return (data.getPoints()*data.getModifyable().getCost());
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#select(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public QualityValue select(Quality data) {
		QualityValue sVal = super.select(data);
		parent.runProcessors();
		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#select(org.prelle.shadowrun6.Quality, java.lang.Object)
	 */
	@Override
	public QualityValue select(Quality data, Object choice) {
		QualityValue sVal = super.select(data, choice);
		parent.runProcessors();
		
		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#deselect(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean deselect(QualityValue ref) {
		boolean success = super.deselect(ref);
		if (!success)
			return false;
		
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#increase(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean increase(QualityValue ref) {
		boolean success = super.increase(ref);
		if (!success)
			return false;
		
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#decrease(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean decrease(QualityValue ref) {
		boolean success = super.decrease(ref);
		if (!success)
			return false;
		
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#isRecommended(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public boolean isRecommended(Quality val) {
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			available.clear();
			todos.clear();
			decisions.clear();
			model.clearRacialQualities();
			netBonusKarma = 0;
			int totalQualities = 0;
			
			// Qualities selected by metatype or magic/resonance choice
			for (Modification tmp : previous) {
				if (tmp instanceof QualityModification) {
					Quality data = ((QualityModification)tmp).getModifiedItem();
					QualityValue ref = new QualityValue(data, 1);
					if (((QualityModification)tmp).getValue()>1)
						ref.setPoints(((QualityModification)tmp).getValue());
					if (data.needsChoice()) {
						logger.warn("   TODO: implement choices: "+data.getSelect()+" for quality "+data);
//						switch (data.getSelect()) {
//						
//						}
//						DecisionToMake decide = new DecisionToMake(data.get)
					}
					logger.info("Add quality "+data.getId()+" "+((data.getMax()>1)?ref.getPoints():"")+" from "+tmp.getSource());

					ref.recalculateModifications();
					
					
					model.addRacialQuality(ref);
				} else
					unprocessed.add(tmp);
			}
			
			// Add user selected qualities
			for (QualityValue val : model.getUserSelectedQualities()) {
				totalQualities++;
				int toPay = val.getPoints()*val.getModifyable().getCost();
				if (toPay>0 && val.getModifyable().getType()==QualityType.NEGATIVE) {
					netBonusKarma += toPay;
					toPay*=-1;
				} else
					netBonusKarma -= toPay;
				
				// Ignore racial qualities
				if (model.hasRacialQuality(val.getModifyable().getId())) {
					// Already selected
					if (val.getModifyable().isMultipleSelectable()) {
						model.addQuality(val);
						logger.info("Pay "+val.getPoints()+" karma for "+val);
						model.setKarmaFree(model.getKarmaFree()-val.getPoints());
					} else {
						// Ignore selection, since it is already auto selected
						model.removeQuality(val);
						logger.warn("Remove user-selected quality that is also auto-selected: "+val.getModifyable());
					}
				} else {
					// Not a racial quality
					if (toPay>=0)
						logger.info("  Pay "+toPay+" karma for "+val);
					else
						logger.info("  Gain "+(-1*toPay)+" karma for "+val);
					model.setKarmaFree(model.getKarmaFree()-toPay);
				}
			}
			logger.info("  Netto bonus karma = "+netBonusKarma+"    total no. of qualities: "+totalQualities);
			
			/*
			 * Determine available qualities
			 */
			for (Quality qual : ShadowrunCore.getQualities()) {
				// qualities that are singular selectable, must be checked
				if (!qual.isMultipleSelectable()) {
					// Check if user already has it - if so, skip it
					if (model.getQuality(qual.getId())!=null)
						continue;
				}
				// Can it be selected
				if (qual.isFreeSelectable() && !qual.isMetagenetic() && canBeSelected(qual))
					available.add(qual);
			}
			
			/*
			 * See if all qualities that need a choice, have one
			 */
			for (QualityValue qual : model.getQualities()) {
				Quality q = qual.getModifyable();
				if (qual.getModifyable().needsChoice()) {
					if (q.getSelect()==ChoiceType.NAME && qual.getDescription()==null) {
						todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "todo.qualities.needsChoice", qual.getModifyable().getName())));
						logger.warn("Choice missing for "+qual.getName());
					} else
					if (q.getSelect()!=ChoiceType.NAME && qual.getChoiceReference()==null) {
						todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "todo.qualities.needsChoice", qual.getModifyable().getName())));
						logger.warn("Choice missing for "+qual.getName());
					}
					// Special handling for mentor spirits and mystical adepts
					if (q.getSelect()==ChoiceType.MENTOR_SPIRIT && model.getMagicOrResonanceType().paysPowers()) {
						if (qual.getMysticAdeptUses()==null) {
							todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "todo.qualities.needsChoiceMystical", qual.getModifyable().getName())));
							logger.warn("Mystical adepts must choose for mentor spirit");
						}
					}
				}
			}
			
			/*
			 * Ensure only 6 selected qualities and limit of 20 bonus karma
			 */
			if (model.getUserSelectedQualities().size()>6) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.get(RES, "priogen.todo.qualities.6")));
			}
			if (netBonusKarma>20) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.format(RES, "priogen.todo.qualities.bonus", netBonusKarma)));
			}
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

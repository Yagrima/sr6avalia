/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.LicenseType;
import org.prelle.shadowrun6.LicenseValue;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.SIN.Quality;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.SINController;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CommonSINController implements SINController {

	private final static int PRICE_SIN_LEVEL = 2500;
	private final static int PRICE_LICENSE_LEVEL = 200;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen");
	protected ShadowrunCharacter model;
	private ProcessorRunner parent;

	//-------------------------------------------------------------------
	public CommonSINController(CharacterController parent) {
		this.parent = parent;
		this.model = parent.getCharacter();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public boolean canCreateNewSIN(Quality quality) {
		if (quality==Quality.REAL_SIN)
			return false;
		
		int cost = quality.getValue()*PRICE_SIN_LEVEL;
		if (model.getNuyen()<cost) 
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun6.SIN.Quality, int)
	 */
	@Override
	public boolean canCreateNewSIN(Quality quality, int count) {
		if (quality==Quality.REAL_SIN)
			return false;
		
		int cost = quality.getValue()*PRICE_SIN_LEVEL * count;
		if (model.getNuyen()<cost)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canDeleteSIN(org.prelle.shadowrun6.SIN)
	 */
	@Override
	public boolean canDeleteSIN(SIN data) {
		if (data.getQuality()==Quality.REAL_SIN)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#createNewSIN(java.lang.String, org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public SIN createNewSIN(String name, Quality quality) {
		if (!canCreateNewSIN(quality)) {
			logger.warn("Cannot create SIN with quality "+quality);
			return null;
		}
		
		SIN sin = new SIN(quality);
		sin.setName(name);
		model.addSIN(sin);
		
		parent.runProcessors();
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#createNewSIN(java.lang.String, org.prelle.shadowrun6.SIN.Quality, int)
	 */
	@Override
	public SIN[] createNewSIN(String name, Quality quality, int count) {
		if (!canCreateNewSIN(quality, count))
			return null;
		
		SIN[] ret = new SIN[count];
		for (int i=1; i<=count; i++) {
			SIN sin = new SIN(quality);
			sin.setName(name+" "+i);
			model.addSIN(sin);
			ret[i-1]=sin;
		}
		parent.runProcessors();
		
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#deleteSIN(org.prelle.shadowrun6.SIN)
	 */
	@Override
	public boolean deleteSIN(SIN data) {
		if (!canDeleteSIN(data)) {
			logger.warn("Cannot delete SIN: "+data);
			return false;
		}
		
		model.removeSIN(data);
		// Remove all licenses
		for (LicenseValue lic : model.getLicenses()) {
			if (lic.getSIN().equals(data.getUniqueId())) {
				logger.info("Delete license '"+lic.getName()+"' because SIN for "+data.getName()+"/"+data.getUniqueId()+" has been deleted");
				model.removeLicense(lic);
			}
		}
		// Remove all lifestyles
		for (LifestyleValue life : model.getLifestyle()) {
			if (data.getUniqueId().equals(life.getSIN())) {
				logger.info("Delete lifestyle '"+life.getName()+"' because SIN for "+data.getName()+"/"+data.getUniqueId()+" has been deleted");
				model.removeLifestyle(life);
			}
		}

		parent.runProcessors();
		
		return true;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public boolean canCreateNewLicense(LicenseType type, SIN sin, Quality quality) {
		if (sin==null)
			throw new NullPointerException("SIN is null");
		int cost = quality.getValue()*PRICE_LICENSE_LEVEL;
		if (model.getNuyen()<cost) {
			logger.warn("Cannot create license: Not enough nuyen ("+model.getNuyen()+" < "+cost+")");
			return false;
		}
		
		if (sin.getQuality().getValue()<quality.getValue()) {
			logger.warn("Cannot create license: Selected rating higher than SIN ("+quality.getValue()+" < "+sin.getQualityValue()+")");
			return false;
		}
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#createNewLicense(java.lang.String, org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public LicenseValue createNewLicense(LicenseType type, SIN sin, Quality quality) {
		if (!canCreateNewLicense(type, sin, quality))
			return null;
		
		LicenseValue ret = new LicenseValue();
		ret.setRating(quality);
		ret.setType(type);
		ret.setSIN(sin.getUniqueId());
		model.addLicense(ret);
		logger.info("Added license "+sin);
		
		parent.runProcessors();
		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		logger.trace("START: process");
		
		List<Modification> unprocessed = previous;
		try {
//			/*
//			 * Detect an existing real SIN
//			 */
//			SIN existingRealSIN = null;
//			for (SIN sin : model.getSINs()) {
//				logger.debug("Found existing SIN "+sin);
//				if (sin.getQuality()==Quality.REAL_SIN) {
//					existingRealSIN = sin;
//					break;
//				}
//			}
//			
//			
//			boolean needsRealSIN = false;
//			List<UUID> memorizedSINs = new ArrayList<>();
//			for (Modification tmp : previous) {
//				if (tmp instanceof MemorizeUUIDModification) {
//					memorizedSINs.add( ((MemorizeUUIDModification)tmp).getUUID() );
//				} else if (tmp instanceof SINModification) {
//					SINModification mod = (SINModification)tmp;
//					logger.debug("Needs real SIN (from "+mod.getSource()+")");
//					needsRealSIN = true;
//					if (existingRealSIN!=null) {
//						logger.debug("use existing real SIN "+existingRealSIN.getUniqueId());
//					unprocessed.add(new LifestyleCostModification(10, 0, existingRealSIN.getUniqueId(), mod.getSource())); 
//					} else {
//						// Create real sin
//						SIN sin = new SIN(Quality.REAL_SIN);
//						model.addSIN(sin);
//						if (memorizedSINs.isEmpty())
//							logger.debug("created new SIN "+sin.getUniqueId());
//						else {
//							sin.setUniqueId(memorizedSINs.remove(0));
//							logger.debug("created new SIN "+sin.getUniqueId()+" with reapplied UUID");
//						}
//						unprocessed.add(new LifestyleCostModification(10, 0, sin.getUniqueId(), mod.getSource())); 
//					}					
//				} else
//					unprocessed.add(tmp);
//			}
//			
//			// If char does not need a real SIN and there is - delete it
//			if (existingRealSIN!=null && !needsRealSIN)
//				model.removeSIN(existingRealSIN);
			
			
			// Walk through all SINs - ignore real SINs / criminals
			for (SIN sin : model.getSINs()) {
				if (sin.getQuality()==Quality.REAL_SIN) {
					continue;
				}
				logger.info("Pay "+sin.getQualityValue()*PRICE_SIN_LEVEL +" for "+sin);
				model.setNuyen( model.getNuyen() - sin.getQualityValue()*PRICE_SIN_LEVEL);
			}

			// Walk through all Licenses 
			for (LicenseValue lic : model.getLicenses()) {
				logger.info("Pay "+lic.getRating().getValue()*PRICE_LICENSE_LEVEL +" for "+lic);
				model.setNuyen( model.getNuyen() - lic.getRating().getValue()*PRICE_LICENSE_LEVEL);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

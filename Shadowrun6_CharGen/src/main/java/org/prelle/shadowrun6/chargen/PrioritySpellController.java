/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.Spell.Category;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.Tradition;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PrioritySpellController implements SpellController, RitualController, CharacterProcessor {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen");

	private CommonSR6CharacterGenerator parent;
	private ShadowrunCharacter model;
	
//	private List<Spell> recommended;
	
	private List<Spell> availableSpells;
	private List<Spell> availableAlchemy;
	private List<Ritual> availableRituals;
	protected List<ToDoElement> todos;
	private int free;

	//-------------------------------------------------------------------
	public PrioritySpellController(CommonSR6CharacterGenerator parent) {
		this.parent = parent;
		model = parent.getCharacter();
		availableSpells   = new ArrayList<Spell>();
		availableAlchemy  = new ArrayList<Spell>();
		availableRituals  = new ArrayList<Ritual>();
		todos      = new ArrayList<>();
//		recommended = new ArrayList<Spell>();
		updateAvailableSpells();
		updateAvailableRituals();
	}

	//--------------------------------------------------------------------
	private void updateAvailableSpells() {
		availableSpells.clear();
		availableSpells.addAll(ShadowrunCore.getSpells());
		availableAlchemy.clear();
		availableAlchemy.addAll(ShadowrunCore.getSpells());
		// Remove those the character already has
		for (SpellValue val : model.getSpells()) {
			logger.trace("Not available anymore "+val);
			if (val.isAlchemistic())
				availableAlchemy.remove(val.getModifyable());
			else
				availableSpells.remove(val.getModifyable());
		}
		
		Collections.sort(availableSpells);
		Collections.sort(availableAlchemy);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	private int getTotalSpellsAndRituals() {
		return model.getRituals().size()+model.getSpells().size();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getSpellsLeft()
	 */
	@Override
	public int getSpellsLeft() {
		return free;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getAvailableSpells()
	 */
	@Override
	public List<Spell> getAvailableSpells(boolean alchemistic) {
		return alchemistic?availableAlchemy:availableSpells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getAvailableSpells(org.prelle.shadowrun6.Spell.Category)
	 */
	@Override
	public List<Spell> getAvailableSpells(Category category, boolean alchemistic) {
		List<Spell> ret = new ArrayList<>();
		for (Spell tmp : alchemistic?availableAlchemy:availableSpells)
			if (tmp.getCategory()==category)
				ret.add(tmp);
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#canBeSelected(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public boolean canBeSelected(Spell data, boolean alchemistic) {
		if (free==0 && model.getKarmaFree()<5)
			return false;
		return getAvailableSpells(alchemistic).contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#canBeDeselected(org.prelle.shadowrun6.SpellValue)
	 */
	@Override
	public boolean canBeDeselected(SpellValue data) {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#select(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public SpellValue select(Spell data, boolean alchemistic) {
		if (!canBeSelected(data, alchemistic)) {
			logger.warn("Trying to select a spell that cannot be selected");
			return null;
		}
		logger.debug("select "+data);
		
		SpellValue ref = new SpellValue(data);
		ref.setAlchemistic(alchemistic);
		model.addSpell(ref);
		logger.debug("send event "+data);
		parent.runProcessors();
		
		updateAvailableSpells();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#deselect(org.prelle.shadowrun6.SpellValue)
	 */
	@Override
	public void deselect(SpellValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to deselect a spell that cannot be deselected");
			return;
		}
		logger.debug("deselect "+data);
		
		model.removeSpell(data);
		parent.runProcessors();
		
		updateAvailableSpells();
	}

	//--------------------------------------------------------------------
	private void updateAvailableRituals() {
		availableRituals.clear();
		availableRituals.addAll(ShadowrunCore.getRituals());
		// Remove those the character already has
		for (RitualValue val : model.getRituals()) {
			logger.trace("Not available anymore "+val);
			availableRituals.remove(val.getModifyable());
		}
		
		Collections.sort(availableRituals);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#getRitualsLeft()
	 */
	@Override
	public int getRitualsLeft() {
		return getSpellsLeft();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#getAvailableRituals()
	 */
	@Override
	public List<Ritual> getAvailableRituals() {
		return availableRituals;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#canBeSelected(org.prelle.shadowrun6.Ritual)
	 */
	@Override
	public boolean canBeSelected(Ritual data) {
		if (getTotalSpellsAndRituals()>=model.getAttribute(Attribute.MAGIC).getModifiedValue()*2)
			return false;
		return getAvailableRituals().contains(data);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#select(org.prelle.shadowrun6.Ritual)
	 */
	@Override
	public RitualValue select(Ritual data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select a ritual that cannot be selected");
			return null;
		}
		logger.debug("select "+data);
		
		RitualValue ref = new RitualValue(data);
		model.addRitual(ref);
		parent.runProcessors();
		
		updateAvailableRituals();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#canBeDeselected(org.prelle.shadowrun6.RitualValue)
	 */
	@Override
	public boolean canBeDeselected(RitualValue data) {
		return model.hasRitual(data.getModifyable().getId());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#deselect(org.prelle.shadowrun6.RitualValue)
	 */
	@Override
	public void deselect(RitualValue data) {
		if (!canBeDeselected(data)) {
			logger.warn("Trying to deselect a ritual that cannot be deselected");
			return;
		}
		logger.debug("deselect "+data);
		
		model.removeRitual(data);
		parent.runProcessors();
		
		updateAvailableRituals();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#changeMagicTradition(org.prelle.shadowrun6.Tradition)
	 */
	@Override
	public void changeMagicTradition(Tradition data) {
		if (model.getTradition()==data)
			return;
		logger.info("Change magic tradition to "+data);
		model.setTradition(data);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			todos.clear();
			free = 0;
			
			// Process Modifications
			for (Modification tmp : previous) {
				if (tmp instanceof FreePointsModification) {
					FreePointsModification mod = (FreePointsModification)tmp;
					if (mod.getType()==Type.SPELLS_RITUALS) {
						free += mod.getCount();
					} else
						unprocessed.add(tmp);
				} else
					unprocessed.add(tmp);
			}
			
			logger.debug("  Points to spend on spells and rituals: "+free);
			// Pay spells
			for (SpellValue val : model.getSpells()) {
				if (free>0) {
					logger.debug("  Get spell "+val.getModifyable()+" for free");
					free--;
				} else {
					logger.info("  Invest 5 karma for spell "+val.getModifyable());
					model.setKarmaFree(model.getKarmaFree()-5);
					model.setKarmaInvested(model.getKarmaInvested()+5);
				}
			}
			
			// Pay rituals
			for (RitualValue val : model.getRituals()) {
				if (free>0) {
					logger.debug("  Get ritual "+val.getModifyable()+" for free");
					free--;
				} else {
					logger.info("  Invest 5 karma for ritual "+val.getModifyable());
					model.setKarmaFree(model.getKarmaFree()-5);
					model.setKarmaInvested(model.getKarmaInvested()+5);
				}
			}
			logger.debug("Free spells/rituals left: "+free);

			/*
			 * Determine available spells
			 */
			updateAvailableSpells();
			updateAvailableSpells();
			
			
			// ToDos
			if (free>0) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.format(RES, "priogen.todo.spells", free)));
			}
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

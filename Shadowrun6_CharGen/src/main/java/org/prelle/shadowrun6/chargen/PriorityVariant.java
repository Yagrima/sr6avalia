package org.prelle.shadowrun6.chargen;

import java.util.ResourceBundle;

import org.prelle.shadowrun6.Resource;

public enum PriorityVariant {
	STANDARD,
	PRIME_RUNNER,
	STREET_LEVEL
	;
	public String getName() { 
		return Resource.get(ResourceBundle.getBundle(NewPriorityCharacterGenerator.class.getName()), "variant."+this.name().toLowerCase());
	}
}
package org.prelle.shadowrun6.chargen;

import java.util.HashMap;
import java.util.Map;

import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.charctrl.AttributeController.PerAttributePoints;

/**
 * @author Stefan Prelle
 *
 */
public class PrioritySettings {

	public PriorityVariant variant;
	
//	Map<Priority,PriorityType> priorities = new HashMap<Priority, PriorityType>();
	Map<PriorityType, Priority> priorities = new HashMap<PriorityType, Priority>();
	int mysticAdeptMaxPoints;
	int mysticAdeptPowerPoints;
	/**
	 * Karma points converted to Nuyen
	 */
	int usedKarma;
	public Map<Attribute, PerAttributePoints> perAttrib = new HashMap<>();
	
}

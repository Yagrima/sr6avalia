/**
 *
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.common.CommonSkillController;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.modifications.AllowModification;
import org.prelle.shadowrun6.modifications.ForbidModification;
import org.prelle.shadowrun6.modifications.SkillModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class PrioritySkillController extends CommonSkillController implements SkillController, CharacterProcessor {

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private int pointsSkills;
	private int pointsLangAndKnow;
	private List<ToDoElement> normalToDos;
	private List<ToDoElement> knowledgeToDos;
	/** Skills that are not restricted anymore */

	//--------------------------------------------------------------------
	/**
	 */
	public PrioritySkillController(CommonSR6CharacterGenerator parent) {
		super(parent);
		model = parent.getCharacter();
		avSkills       = new ArrayList<Skill>();
		recSkills      = new ArrayList<Skill>();
		normalToDos    = new ArrayList<>();
		knowledgeToDos = new ArrayList<>();
		
		// Ensure native language skill is present
		boolean found = false;
		for (SkillValue val : parent.getCharacter().getSkillValues(SkillType.LANGUAGE)) {
			if (val.getPoints()==SkillValue.LANGLEVEL_NATIVE)
				found=true;
		}
		if (!found)
			parent.getCharacter().addSkill(new SkillValue(ShadowrunCore.getSkill("language"), SkillValue.LANGLEVEL_NATIVE));
	}

//	//--------------------------------------------------------------------
//	public int getPointsInKnowledgeAndLanguage() {
//		int sum = 0;
//		for (SkillValue sVal : model.getSkillValues(false)) {
//			Skill skill = sVal.getModifyable();
//			if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE) {
//				if (skill.getId().startsWith("native_language"))
//					continue;
//				sum += sVal.getPoints();
//				sum += sVal.getSkillSpecializations().size();
//			}
//		}
//		return sum;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getPointsLeftInKnowledgeAndLanguage()
	 */
	@Override
	public int getPointsLeftInKnowledgeAndLanguage() {
		return pointsLangAndKnow;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getPointsLeftSkills()
	 */
	@Override
	public int getPointsLeftSkills() {
		return pointsSkills;
	}

	//-------------------------------------------------------------------
	private int getIncreaseCost(ShadowrunCharacter model, Skill key) {
		SkillValue sVal = model.getSkillValue(key);

		if (key.getType()==SkillType.KNOWLEDGE || key.getType()==SkillType.LANGUAGE) 
			return 3;
		
		int newVal = (sVal==null)?1:(sVal.getModifiedValue()+1);
		return newVal*5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeSelected(org.prelle.shadowrun6.Skill)
	 */
	@Override
	public boolean canBeSelected(Skill skill) {
		if (skill.getType()==SkillType.LANGUAGE || skill.getType()==SkillType.KNOWLEDGE)
			return canBeSelectedKnowledgeOrLanguage(skill);
		
		// Points left
		return getPointsLeftSkills()>0 || getIncreaseCost(model,skill)<=model.getKarmaFree();
	}

	//--------------------------------------------------------------------
	public boolean canBeSelectedKnowledgeOrLanguage(Skill skill) {
		// Only check knowledge and language skills here
		if (skill.getType()!=SkillType.KNOWLEDGE && skill.getType()!=SkillType.LANGUAGE) {
			return false;
		}

		// Points left
		return (getPointsLeftInKnowledgeAndLanguage()>0 || getPointsLeftSkills()>0 || model.getKarmaFree()>=3);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#selectKnowledgeOrLanguage(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public SkillValue selectKnowledgeOrLanguage(Skill skill, String customName) {
		if (!canBeSelectedKnowledgeOrLanguage(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}

		// Change model
		SkillValue sVal = new SkillValue(skill, 1);
		sVal.setName(customName);
		logger.info("Selected skill "+sVal);
		model.addSkill(sVal);

		parent.runProcessors();

		return sVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#select(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		SkillSpecializationValue ret = super.select(ref, spec, expertise);
		
		// Inform listeners
		parent.runProcessors();
	
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean deselect(SkillValue ref) {
		logger.info("deselect "+ref);
		boolean deselected = super.deselect(ref);
		if (!deselected)
			return deselected;
		
		// Inform listeners
		parent.runProcessors();	
		return true;
	}
		

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#select(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public boolean deselect(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		boolean ret = super.deselect(ref, spec, expertise);
		
		// Inform listeners
		if (ret)
			parent.runProcessors();
	
		return ret;
	}

	//-------------------------------------------------------------------
	private List<SkillValue> getMaximizedSkills() {
		List<SkillValue> maxed = new ArrayList<SkillValue>();
		for (SkillValue val : model.getSkillValues()) {
			if (val.getPoints() >= (6+val.getMaximumModifier()))
				maxed.add(val);
		}
		return maxed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeIncreased(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean canBeIncreased(SkillValue ref) {
		SkillType type = ref.getModifyable().getType();
		// Cannot increase knowledge skills
		if (type==SkillType.KNOWLEDGE)
			return false;
		// Is automatically added
		if (model.isAutoSkill(ref))
			return false;
		
		// Maximum not reached yet
		int maximum = 6  + ref.getMaximumModifier();
		if (type==SkillType.LANGUAGE) {
			// Language skills have different maximums
			maximum = SkillValue.LANGLEVEL_EXPERT;
		}
		if (ref.getPoints()>=maximum) {
			if (logger.isTraceEnabled())
				logger.trace("Cannot increase skill "+ref+" because current value "+ref.getModifiedValue()+" is already at maximum "+maximum);
			return false;
		}
		
		Collection<SkillValue> alreadyMaxed = getMaximizedSkills();

		// Only allow to max an attribute, if there isn't one already
		if ((ref.getPoints()+1)>=(6 + ref.getMaximumModifier())) {
			if (logger.isTraceEnabled())
				logger.trace("Increasing "+ref.getModifyable()+" would reach maximum of "+(6+ref.getMaximumModifier())+".  Is already one maxed = "+alreadyMaxed);
			return alreadyMaxed.isEmpty();
		}
		
//		boolean enoughKarma = model.getKarmaFree() >= ((ref.getModifiedValue()+1)*5); 
//		if (!enoughKarma)
//			return false;

		// Enough points
		int karmaCost = getIncreaseCost(model,ref.getModifyable());
		if (type!=SkillType.KNOWLEDGE && type!=SkillType.LANGUAGE) {
			if (getPointsLeftSkills()<=0 && karmaCost>model.getKarmaFree()) {
				logger.debug("Cannot increase because no points ("+getPointsLeftSkills()+") left and not enough karma ("+model.getKarmaFree()+") to raise to "+(ref.getPoints()+1));
				return false;
			}
		} else {
			// Is language or knowledge
			if (!(getPointsLeftInKnowledgeAndLanguage()>0 || getIncreaseCost(model,ref.getModifyable())<=model.getKarmaFree())) {
				logger.debug("Cannot increase because no points ("+getPointsLeftInKnowledgeAndLanguage()+") left and not enough karma ("+model.getKarmaFree()+") to raise");				
			}

			return getPointsLeftInKnowledgeAndLanguage()>0 || karmaCost<=model.getKarmaFree();
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.common.CommonSkillController#increase(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean increase(SkillValue ref) {
		logger.debug("try increase "+ref);
		if (!super.increase(ref))
			return false;
		
		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeDecreased(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean canBeDecreased(SkillValue ref) {
		// Minimum not reached yet
		if (ref.getPoints()<=0)
			return false;
		
		if (model.isAutoSkill(ref))
			return false;

		return true;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.common.CommonSkillController#decrease(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean decrease(SkillValue ref) {
		logger.debug("try decrease "+ref);
		if (!super.decrease(ref))
			return false;
		
		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeSelected(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public boolean canBeSelected(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		// Not already selected
		if (ref.hasSpecialization(spec))
			return false;
		// Cannot select expertise at generation
		if (expertise)
			return false;
		// Ref belongs to character
		if (!model.getSkillValues().contains(ref))
			return false;
		// Value should be at least 1 
		if (ref.getPoints()<1)
			return false;		
		// At generation only one specialization per skill
		if (ref.getSkillSpecializations().size()>0)
			return false;
		// Not possible for knowledge skills
		if (ref.getModifyable().getType()==SkillType.KNOWLEDGE)
			return false;
		
		// Points left
		return getPointsLeftSkills()>0 || model.getKarmaFree()>4;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref) {
		// Ref belongs to character
		if (!model.getSkillValues().contains(ref))
			return false;
		
		if (model.isAutoSkill(ref))
			return false;
		
		Skill skill = ref.getModifyable();
		if (skill.getType()==SkillType.LANGUAGE && ref.getPoints()==SkillValue.LANGLEVEL_NATIVE)
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		// Not already selected
		if (!ref.hasSpecialization(spec)) {
			logger.debug("Skill does not have specialization(exp="+expertise+") to deselect: "+spec+" not in "+ref.getSkillSpecializations());
			return false;
		}
		// Ref belongs to character
		if (!model.getSkillValues().contains(ref)) {
			return false;
		}
		
		if (model.isAutoSkill(ref))
			return false;

		return ref.getSpecialization(spec).isExpertise() == expertise;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canSpecializeIn(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean canSpecializeIn(SkillValue val) {
		return !val.getModifyable().getSpecializations().isEmpty() && (pointsSkills>0 || model.getKarmaFree()>=5);
	}

//	//--------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun6.charctrl.SkillController#increase(org.prelle.shadowrun6.SkillValue)
//	 */
//	@Override
//	public boolean increase(SkillValue ref) {
//		boolean ret = super.increase(ref);
//		if (ret)
//			parent.runProcessors();
//		return ret;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun6.common.CommonSkillController#decrease(org.prelle.shadowrun6.SkillValue)
//	 */
//	@Override
//	public boolean decrease(SkillValue ref) {
//		boolean ret = super.decrease(ref);
//		if (ret)
//			parent.runProcessors();
//		return ret;
//	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun6.common.CommonSkillController#select(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
//	 */
//	@Override
//	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec, boolean expertise) {
//		SkillSpecializationValue ret = super.select(ref, spec, expertise);
//		if (ret!=null)
//			parent.runProcessors();
//		return ret;
//	}

	//-------------------------------------------------------------------
	private void payKnowledgeWithPoints(Map<SkillValue,Integer> alreadyPayed, List<SkillValue> values) {
		for (SkillValue val : values) {
			if (val.getModifyable().getType()!=SkillType.KNOWLEDGE && val.getModifyable().getType()!=SkillType.LANGUAGE)
				continue;
			if (val.getPoints()==SkillValue.LANGLEVEL_NATIVE) {
				alreadyPayed.put(val, val.getPoints());
				continue;
			}
			
			if (val.getPoints()<=pointsLangAndKnow) {
				logger.debug("  invest "+val.getPoints()+" free knowledge/language skill points for "+val.getModifyable());
				pointsLangAndKnow-= val.getPoints();
				alreadyPayed.put(val, val.getPoints());
			} else if (pointsLangAndKnow>0 ){
				logger.debug("  invest "+pointsLangAndKnow+" remaining knowledge/language skill points for "+val.getModifyable());
				alreadyPayed.put(val, pointsLangAndKnow);
				pointsLangAndKnow = 0;
			} else {
				logger.debug("  No more free knowledge/language skill points for "+val.getModifyable());
				alreadyPayed.put(val, pointsLangAndKnow);
			}
		}
		logger.debug("Free language/knowledge skill points left: "+pointsLangAndKnow);

	}
	
	//-------------------------------------------------------------------
	private void payNormalSkillsWithPoints(Map<SkillValue,Integer> alreadyPayed, List<SkillValue> values) {
		for (SkillValue val : values) {
			Skill attr = val.getModifyable();
			Integer _payed = alreadyPayed.get(val);
			int payed = (_payed!=null)?_payed:0;
			
			int toPay = val.getPoints() - payed;
//			logger.debug("  "+attr+" is payed up to "+payed+" and distributed are "+val.getPoints()+"   need to pay="+toPay+"   left="+pointsSkills);
			if (toPay<=pointsSkills) {
				logger.debug("  invest "+toPay+" for "+attr);
				pointsSkills-= toPay;
				alreadyPayed.put(val, val.getPoints());
			} else if (pointsSkills>0) {
				logger.debug("  invest "+pointsSkills+" points for "+attr+" (from "+toPay+" requested)");
				alreadyPayed.put(val, pointsSkills);
				pointsSkills = 0;
			} else {
				logger.debug("  cannot invest points for "+attr);
			}
		}
		logger.debug("Free skill points left: "+pointsSkills);

	}
	
	//-------------------------------------------------------------------
	private void payNormalSkillsWithKarma(Map<SkillValue,Integer> alreadyPayed, List<SkillValue> values) {
		for (SkillValue val : values) {
			int ppCost = (val.getModifyable().getType()==SkillType.KNOWLEDGE || val.getModifyable().getType()==SkillType.LANGUAGE)?3:5;
			
			Skill attr = val.getModifyable();
			Integer _payed = alreadyPayed.get(val);
			int payed = (_payed!=null)?_payed:0;
			
			for (int next=payed+1; next<=val.getPoints(); next++) {
				int cost = next*ppCost;
				logger.debug("  pay "+cost+" karma for "+attr+" = "+next+"    already payed = "+alreadyPayed.get(val));
				model.setKarmaFree(model.getKarmaFree()-cost);
			}
		}
		logger.debug("After paying skills free karma is: "+model.getKarmaFree());
	}
	
	//-------------------------------------------------------------------
	private void paySpecializationsWithPoints(Map<SkillValue,Integer> alreadyPayed, List<SkillValue> values) {
		for (SkillValue val : values) {
			Skill attr = val.getModifyable();
			// Pay specializations
			if (val.getSkillSpecializations().size()>0) {
				// May only have one specialization
				while (val.getSkillSpecializations().size()>1) {
					logger.warn("  More than 1 specialization for skill "+val.getModifyable()+" - remove "+val.getSkillSpecializations().get(0));
					val.removeSpecialization(val.getSkillSpecializations().get(0));
				}
				if (pointsSkills>0) {
					logger.debug("  invest 1 for "+attr+"'s specialization");
					pointsSkills--;
				} else {
					// No points for specialization
					logger.debug("  pay 5 karma for "+attr+"'s specialization");
					model.setKarmaFree(model.getKarmaFree()-5);
//					logger.warn("  No points for specialization for skill "+val.getModifyable()+" - remove "+val.getSkillSpecializations().get(0));
//					val.removeSpecialization(val.getSkillSpecializations().get(0));
				}
			}
		}
		logger.debug("Free skill points left: "+pointsSkills);

	}
	
	//-------------------------------------------------------------------
	private void paySpecializationsWithKarma(Map<SkillValue,Integer> alreadyPayed, List<SkillValue> values) {
		for (SkillValue val : values) {
			Skill attr = val.getModifyable();
			// Pay specializations
			if (val.getSkillSpecializations().size()>0) {
				// May only have one specialization
				while (val.getSkillSpecializations().size()>1) {
					logger.warn("  More than 1 specialization for skill "+val.getModifyable()+" - remove "+val.getSkillSpecializations().get(0));
					val.removeSpecialization(val.getSkillSpecializations().get(0));
				}
				if (pointsSkills>0) {
					logger.debug("  invest 1 for "+attr+"'s specialization");
					pointsSkills--;
				} else {
					// No points for specialization
					logger.warn("  No points for specialization for skill "+val.getModifyable()+" - remove "+val.getSkillSpecializations().get(0));
					val.removeSpecialization(val.getSkillSpecializations().get(0));
				}
			}
		}
		logger.debug("Free skill points left: "+pointsSkills);

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		this.model = model;

		logger.trace("START: process");
		try {
			todos.clear();
			normalToDos.clear();
			knowledgeToDos.clear();
			todos.clear();
			avSkills.clear();
			allowedSkills.clear();
			unrestricted.clear();
			restricted.clear();
			pointsSkills = 0;
			pointsLangAndKnow = model.getAttribute(Attribute.LOGIC).getPoints();
			
			logger.debug("  get "+pointsLangAndKnow+" knowledge points");
			
			
			// Process Modifications
			for (Modification tmp : previous) {
				if (tmp instanceof FreePointsModification) {
					FreePointsModification mod = (FreePointsModification)tmp;
					if (mod.getType()==Type.SKILLS) {
						logger.debug("  add "+mod.getCount()+" skill points");
						pointsSkills += mod.getCount();
					} else
						unprocessed.add(tmp);
				} else if (tmp instanceof AllowModification) {
					AllowModification mod = (AllowModification)tmp;
					logger.debug("  Allow skill "+mod.getSkill()+" because of "+mod.getSource());
					unrestricted.add(mod.getSkill());
				} else if (tmp instanceof ForbidModification) {
					ForbidModification mod = (ForbidModification)tmp;
					logger.debug("  Forbid skill "+mod.getSkill()+" because of "+mod.getSource());
					restricted.add(mod.getSkill());
				} else if (tmp instanceof SkillModification) {
					SkillModification mod = (SkillModification)tmp;
					SkillValue sval = model.getSkillValue(mod.getSkill());
					logger.info("  Apply "+mod+" to skill value "+sval);
					if (sval==null) {
						logger.debug("Skill "+mod.getSkill()+" does not exist######################################");
						sval = new SkillValue(mod.getSkill(), 0);
						model.addSkill(sval);
					} else
						logger.debug("Skill "+mod.getSkill()+" exist###################################### mods="+sval.getModifications());
					sval.addModification(mod);
					logger.debug("  skill value now: "+sval);
				} else
					unprocessed.add(tmp);
			}
			
			/*
			 * Remove points invested in forbidden skills
			 */
			for (Skill forbidden : restricted) {
				SkillValue sval = model.getSkillValue(forbidden);
				if (sval!=null && sval.getPoints()!=0) {
					sval.setPoints(0);
					if (sval.getModifiedValue()==0) {
						logger.warn("Skill "+forbidden.getId()+" is forbidden - remove it");
						model.removeSkill(sval);
					} else {
						logger.warn("Skill "+forbidden.getId()+" is forbidden - set value to 0");
					}
				}
			}

			// Ensure maximum and minimum values
			boolean alreadyAnSkillMaxed = false;
			for (SkillValue val : model.getSkillValues()) {
				int max = 6 + val.getMaximumModifier();
				if (val.getPoints()>=max ) {
					if (alreadyAnSkillMaxed) {
						int maxAllow = max-1;
						logger.warn("Reduce points for "+val.getModifyable()+" since another attribute was already maxed");
						val.setPoints(maxAllow);
					} else {
						alreadyAnSkillMaxed = true;
						while (val.getPoints()>max) {
							val.setPoints(val.getPoints()-1);
							logger.warn("Reduce points for "+val.getModifyable()+" since attribute was above maximum");
						}
					}
				}
			}
			
			
			/*
			 * Count points spend
			 * Start to invest points on high values - others might be better off payed with karma
			 */
			List<SkillValue> values = model.getSkillValues();
			Collections.sort(values, new Comparator<SkillValue>() {
				public int compare(SkillValue o1, SkillValue o2) {
					return ((Integer)o1.getPoints()).compareTo(o2.getPoints());
				}
			});
			Collections.reverse(values);

			Map<SkillValue,Integer> alreadyPayed = new HashMap<>();
			// Pay free language/knowledge skills first
			payKnowledgeWithPoints(alreadyPayed, values);
			// Pay normal skills
			payNormalSkillsWithPoints(alreadyPayed, values);

			/*
			 * Pay rest with karma
			 */
			payNormalSkillsWithKarma(alreadyPayed, values);
			paySpecializationsWithPoints(alreadyPayed, values);
			
			/*
			 * Determine available skills
			 */
			for (Skill skill : ShadowrunCore.getSkills()) {
				if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE) {
					avSkills.add(skill);
					continue;
				}
				// Is it allowed
				if (!skill.isRestricted() || (unrestricted.contains(skill)) && !restricted.contains(skill))
					allowedSkills.add(skill);
				SkillValue val = model.getSkillValue(skill);
				if (val==null || skill.isToSpecify()) {
					// Might add skill if it is not restricted
					if (ignoreRequirements || ( (!skill.isRestricted() || (unrestricted.contains(skill))) && !restricted.contains(skill)))
						avSkills.add(skill);
				}
			}
			
			
			// ToDos
			if (pointsSkills>0) {
				todos.add(new ToDoElement(Severity.STOPPER, String.format(Resource.get(RES, "priogen.todo.skills"), pointsSkills)));
				normalToDos.add(new ToDoElement(Severity.STOPPER, String.format(Resource.get(RES, "priogen.todo.skills"), pointsSkills)));
			}
			if (pointsLangAndKnow>0) {
				todos.add(new ToDoElement(Severity.STOPPER, String.format(Resource.get(RES, "priogen.todo.knowledge"), pointsLangAndKnow)));
				knowledgeToDos.add(new ToDoElement(Severity.STOPPER, String.format(Resource.get(RES, "priogen.todo.knowledge"), pointsLangAndKnow)));
			}
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getNormalToDos()
	 */
	@Override
	public List<ToDoElement> getNormalToDos() {
		return normalToDos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getKnowledgeToDos()
	 */
	@Override
	public List<ToDoElement> getKnowledgeToDos() {
		return knowledgeToDos;
	}

		
}

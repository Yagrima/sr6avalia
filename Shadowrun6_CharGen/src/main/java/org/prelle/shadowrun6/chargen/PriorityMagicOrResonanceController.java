/**
 *
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.MagicOrResonanceOption;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Tradition;
import org.prelle.shadowrun6.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun6.charctrl.modifications.AvailableMagicOrResonanceModification;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.modifications.AllowModification;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.AttributeModification.ModificationType;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityMagicOrResonanceController implements MagicOrResonanceController, CharacterProcessor {
	
	private static final Logger logger = LogManager.getLogger("shadowrun6.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	private CommonSR6CharacterGenerator parent;
	private ShadowrunCharacter model;

	private List<MagicOrResonanceOption> available;
	private List<ToDoElement> todos;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityMagicOrResonanceController(CommonSR6CharacterGenerator parent) {
		this.parent = parent;
		available = new ArrayList<>();
		model = parent.getCharacter();
		todos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#getAvailable()
	 */
	@Override
	public List<MagicOrResonanceOption> getAvailable() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#canBeSelected(org.prelle.shadowrun6.MagicOrResonanceOption)
	 */
	@Override
	public boolean canBeSelected(MagicOrResonanceOption type) {
		return available.contains(type);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#select(org.prelle.shadowrun6.MagicOrResonanceOption)
	 */
	@Override
	public void select(MagicOrResonanceOption value) {
		logger.info("Select magic/resonance "+value);
		if (value==null)
			model.setMagicOrResonanceType(null);
		else
			model.setMagicOrResonanceType(value.getType());
		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			todos.clear();

			/*
			 * Build list of available options
			 */
			available.clear();
			List<MagicOrResonanceType> avTypes = new ArrayList<>();
			for (Modification _mod : previous) {
				if (_mod instanceof AvailableMagicOrResonanceModification) {
					AvailableMagicOrResonanceModification mod = (AvailableMagicOrResonanceModification)_mod;
					available.add(mod.getOption());
					avTypes.add(mod.getOption().getType());
				} else
					unprocessed.add(_mod);
			}
			logger.debug("Available: "+avTypes);
			
			/*
			 * If current selection is not available, remove it
			 */
			if ((model.getMagicOrResonanceType()!=null) && !avTypes.contains(model.getMagicOrResonanceType())) {
				logger.warn("Previous selection "+model.getMagicOrResonanceType()+" is not available anymore");
				model.setMagicOrResonanceType(null);
			}
			
			/*
			 * If there is no selection yet and there is only one option, select it
			 */
			if (model.getMagicOrResonanceType()==null || avTypes.size()==1) {
				logger.debug("Autoselect "+avTypes.get(0));
				model.setMagicOrResonanceType(avTypes.get(0));
			}
			
			/*
			 * If a magic or resonance type is selected, apply its points
			 */
			PrioritySettings settings = model.getTemporaryChargenSettings(PrioritySettings.class);
			for (MagicOrResonanceOption tmp : available) {
				if (tmp.getType()==model.getMagicOrResonanceType()) {
					logger.info("Apply selections from "+model.getMagicOrResonanceType().getId());
					logger.info("   "+tmp.getType().getModifications());
					unprocessed.addAll(tmp.getType().getModifications());
					switch (tmp.getType().getId()) {
					case "mundane": 
						break;
					case "technomancer":
						unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.RESONANCE, tmp.getValue(), ModificationType.RELATIVE, tmp.getType()));
						if (SR6ConfigOptions.HOUSERULE_ADJUSTED_MAGIC_FOR_SPELLS_PP.getValue()) {
							int total = model.getAttribute(Attribute.MAGIC).getPoints() + tmp.getValue();
							unprocessed.add(new FreePointsModification(Type.COMPLEX_FORMS, total, tmp.getType()));
						} else {
							unprocessed.add(new FreePointsModification(Type.COMPLEX_FORMS, tmp.getValue()*2, tmp.getType()));
						}
						break;
					case "aspectedmagician":
						if (model.getAspectSkill()!=null) {
							logger.info("  Add aspected skill "+model.getAspectSkill());
							AllowModification allow = new AllowModification(model.getAspectSkill());
							allow.setSource(tmp);
							unprocessed.add(allow);
							if (model.getAspectSkill().getId().equals("sorcery") || model.getAspectSkill().getId().equals("enchanting")) {
								if (SR6ConfigOptions.HOUSERULE_ADJUSTED_MAGIC_FOR_SPELLS_PP.getValue()) {
									int total = model.getAttribute(Attribute.MAGIC).getPoints() + tmp.getValue();
									unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, total*2, tmp.getType()));
								} else {
									unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, tmp.getValue()*2, tmp.getType()));
								}
							}
						} else {
							logger.debug("  No aspect skill selected yet");
						}
						unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.MAGIC, tmp.getValue(), ModificationType.RELATIVE, tmp.getType()));
						break;
					case "magician":
						if (SR6ConfigOptions.HOUSERULE_ADJUSTED_MAGIC_FOR_SPELLS_PP.getValue()) {
							int total = model.getAttribute(Attribute.MAGIC).getPoints() + tmp.getValue();
							unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, total*2, tmp.getType()));
						} else {
							unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, tmp.getValue()*2, tmp.getType()));
						}
						unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.MAGIC, tmp.getValue(), ModificationType.RELATIVE, tmp.getType()));
						break;
					case "adept":
						unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.MAGIC, tmp.getValue(), ModificationType.RELATIVE, tmp.getType()));
						// Free points for adepts
						if (SR6ConfigOptions.HOUSERULE_ADJUSTED_MAGIC_FOR_SPELLS_PP.getValue()) {
							int total = model.getAttribute(Attribute.MAGIC).getPoints() + tmp.getValue();
							unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.POWER_POINTS, total, ModificationType.RELATIVE, tmp.getType()));
						} else {
							unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.POWER_POINTS, tmp.getValue(), ModificationType.RELATIVE, tmp.getType()));
						}
						break;
					case "mysticadept":
						unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.MAGIC, tmp.getValue(), ModificationType.RELATIVE, tmp.getType()));
						if (SR6ConfigOptions.HOUSERULE_ADJUSTED_MAGIC_FOR_SPELLS_PP.getValue()) {
							settings.mysticAdeptMaxPoints = tmp.getValue() + model.getAttribute(Attribute.MAGIC).getPoints();
						} else {
							settings.mysticAdeptMaxPoints = tmp.getValue();
						}
						// Power points may not be larger than magic max
						settings.mysticAdeptPowerPoints = Math.min(tmp.getValue(), settings.mysticAdeptPowerPoints);
						unprocessed.add(new FreePointsModification(Type.SPELLS_RITUALS, mysticAdeptGetSpellPoints()*2, tmp.getType()));
						unprocessed.add(new AttributeModification(ModificationValueType.NATURAL, Attribute.POWER_POINTS, settings.mysticAdeptPowerPoints, ModificationType.RELATIVE, tmp.getType()));
						
						break;
					default:
						logger.error("Don't know how to handle "+tmp.getType().getId());
					}
					break;
				}
			}
			logger.debug("Unprocessed = "+unprocessed);
			logger.info("mysticAdeptMax="+settings.mysticAdeptMaxPoints+"   mysticAdeptPower="+settings.mysticAdeptPowerPoints);
			
			/*
			 * ToDo
			 */
			if (model.getMagicOrResonanceType()==null) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.get(RES, "priogen.todo.magic")));
			}
			if (model.getTradition()==null && model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesSpells()) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.get(RES, "priogen.todo.tradition")));
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#mysticAdeptGetSpellPoints()
	 */
	@Override
	public int mysticAdeptGetSpellPoints() {
		PrioritySettings settings = model.getTemporaryChargenSettings(PrioritySettings.class);
		return settings.mysticAdeptMaxPoints-settings.mysticAdeptPowerPoints;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#mysticAdeptGetPowerPoints()
	 */
	@Override
	public int mysticAdeptGetPowerPoints() {
		return model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptPowerPoints;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#mysticAdeptCanIncreasePowerPoint()
	 */
	@Override
	public boolean mysticAdeptCanIncreasePowerPoint() {
		return model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptPowerPoints<model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptMaxPoints;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#mysticAdeptCanDecreasePowerPoint()
	 */
	@Override
	public boolean mysticAdeptCanDecreasePowerPoint() {
		return model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptPowerPoints>0;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#mysticAdeptIncreasePowerPoint()
	 */
	@Override
	public boolean mysticAdeptIncreasePowerPoint() {
		if (!mysticAdeptCanIncreasePowerPoint())
			return false;
		
		model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptPowerPoints++;
		logger.info("Increase mystic adepts power points to "+model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptPowerPoints);
		parent.runProcessors();
		return true;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#mysticAdeptDecreasePowerPoint()
	 */
	@Override
	public boolean mysticAdeptDecreasePowerPoint() {
		if (!mysticAdeptCanDecreasePowerPoint())
			return false;
		
		model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptPowerPoints--;
		logger.info("Decrease mystic adepts power points to "+model.getTemporaryChargenSettings(PrioritySettings.class).mysticAdeptPowerPoints);
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#setAspectSkill(org.prelle.shadowrun6.Skill)
	 */
	@Override
	public void setAspectSkill(Skill skill) {
		logger.info("Set aspect skill: "+skill);
		model.setAspectSkill(skill);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MagicOrResonanceController#selectTradition(org.prelle.shadowrun6.Tradition)
	 */
	@Override
	public void selectTradition(Tradition value) {
		logger.info("Set tradition: "+value);
		model.setTradition(value);
		parent.runProcessors();
	}

}

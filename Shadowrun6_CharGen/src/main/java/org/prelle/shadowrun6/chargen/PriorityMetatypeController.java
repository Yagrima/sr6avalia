/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.MetaTypeOption;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.MetatypeController;
import org.prelle.shadowrun6.charctrl.modifications.AvailableMetaTypeModification;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityMetatypeController implements MetatypeController, CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen");

	protected final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	private CommonSR6CharacterGenerator parent;
	
	private Map<MetaType, MetaTypeOption> availableOptions;
	private List<ToDoElement> todos;

	//-------------------------------------------------------------------
	public PriorityMetatypeController(CommonSR6CharacterGenerator parent, ShadowrunCharacter model) {
		this.parent= parent;
		availableOptions  = new HashMap<MetaType,MetaTypeOption>();
		todos = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetatypeController#getAvailable()
	 */
	@Override
	public List<MetaTypeOption> getAvailable() {
		List<MetaTypeOption> avail = new ArrayList<MetaTypeOption>();
		avail.addAll(availableOptions.values());
		Collections.sort(avail);
		return avail;
	}

	//--------------------------------------------------------------------
	private MetaTypeOption getOptionByMetaType(MetaType type) {
		return availableOptions.get(type);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetatypeController#getKarmaCost(org.prelle.shadowrun6.MetaType)
	 */
	@Override
	public int getKarmaCost(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option!=null)
			return option.getAdditionalKarmaKost();
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetatypeController#canBeSelected(org.prelle.shadowrun6.MetaType)
	 */
	@Override
	public boolean canBeSelected(MetaType type) {
		MetaTypeOption option = getOptionByMetaType(type);
		if (option==null)
			return false;
		
		logger.debug("canBeSelected("+type+") = "+parent.getCharacter().getKarmaFree()+" >= "+option.getAdditionalKarmaKost());
		return parent.getCharacter().getKarmaFree()>=option.getAdditionalKarmaKost();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.MetatypeController#select(org.prelle.shadowrun6.MetaType)
	 */
	@Override
	public boolean select(MetaType value) {
		logger.info("Select metatype "+value);
		if (!canBeSelected(value)) {
			logger.warn("Cannot select "+value);
			return false;
		}

		parent.getCharacter().setMetatype(value);
		
		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();
		
		try {
			availableOptions.clear();
			todos.clear();
			for (Modification _mod : previous) {
				if (_mod instanceof AvailableMetaTypeModification) {
					AvailableMetaTypeModification mod = (AvailableMetaTypeModification)_mod;
					availableOptions.put(mod.getMetaType().getType(), mod.getMetaType());
				} else
					unprocessed.add(_mod);
			}
			
			logger.debug("Available metatype options: "+availableOptions);
			MetaType selected = parent.getCharacter().getMetatype();
			logger.debug("  selected: "+selected);

			/*
			 * If a metatype is selected, apply it
			 */
			if (selected==null) {
				todos.add(new ToDoElement(Severity.STOPPER, RES.getString("priogen.todo.metatype")));
			} else {
				MetaTypeOption option = availableOptions.get(selected);
				if (option==null) {
					// Previous selection not possible anymore
					logger.warn("Deselected metatype since it is not available anymore");
					parent.getCharacter().setMetatype(null);
					todos.add(new ToDoElement(Severity.STOPPER, RES.getString("priogen.todo.metatype")));
				} else {
					int karma = option.getAdditionalKarmaKost();
					if (karma>0) {
						logger.info("Pay "+karma+" for metatype "+option.getType().getId());
						model.setKarmaFree(model.getKarmaFree()-karma);
					}
					// Applying modification is a special CharacterProcessor from Core
				}
			}
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

}

package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.gen.CommonSR6CharacterGenerator;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan Prelle
 *
 */
public class CommonMetamagicOrEchoGenerator extends CommonMetaMagicOrEchoController {

	public CommonMetamagicOrEchoGenerator(CommonSR6CharacterGenerator parent) {
		super(parent, CharGenMode.CREATING);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			todos.clear();
			
			// Pay metamagics / echoes with karma
			int desired = 1;
			for (MetamagicOrEchoValue tmp : model.getMetamagicOrEchoes()) {
				int karma = 10 + desired;
				model.setKarmaFree( model.getKarmaFree() - karma);
				logger.info("Pay "+karma+" for metamagic/echo "+tmp);
				// Modifications are done in GetModificationsFromQualities
//				unprocessed.addAll(tmp.getModifications());
				desired++;
			}
			
			
			updateAvailable();
			logger.debug("Available metamagic/echoes = "+available);
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

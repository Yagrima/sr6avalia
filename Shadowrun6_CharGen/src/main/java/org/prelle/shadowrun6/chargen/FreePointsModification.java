/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.Date;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class FreePointsModification implements Modification {
	
	public enum Type {
		ADJUSTMENT_POINTS,
		ATTRIBUTES,
		SKILLS,
		SPELLS_RITUALS,
		COMPLEX_FORMS,
		CONNECTIONS,
		HIGH_CONNECTIONS, 
	}

	private Type type;
	private int count;
	private Object source;
	
	//-------------------------------------------------------------------
	/**
	 */
	public FreePointsModification(Type type, int count) {
		this.type = type;
		this.count= count;
	}
	
	//-------------------------------------------------------------------
	/**
	 */
	public FreePointsModification(Type type, int count, Object src) {
		this.type = type;
		this.count= count;
		this.source = src;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return type+"="+count;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date arg0) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int arg0) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object arg0) {
		// TODO Auto-generated method stub

	}

	public Type getType() {
		return type;
	}

	public int getCount() {
		return count;
	}

}

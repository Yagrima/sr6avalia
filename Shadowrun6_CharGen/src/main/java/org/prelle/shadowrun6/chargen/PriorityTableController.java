/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MagicOrResonanceOption;
import org.prelle.shadowrun6.MetaTypeOption;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityOption;
import org.prelle.shadowrun6.PriorityTableEntry;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.modifications.AvailableMagicOrResonanceModification;
import org.prelle.shadowrun6.charctrl.modifications.AvailableMetaTypeModification;
import org.prelle.shadowrun6.chargen.FreePointsModification.Type;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.modifications.AddNuyenModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class PriorityTableController implements CharacterProcessor {

	protected final static Logger logger = LogManager.getLogger("shadowrun6.gen");

	protected PrioritySettings settings;
	protected CharacterGenerator parent;

	//--------------------------------------------------------------------
	public PriorityTableController(CharacterGenerator parent) {
		this.parent = parent;
		
		settings = parent.getCharacter().getTemporaryChargenSettings(PrioritySettings.class);
		logger.info("Priority table initialized with "+settings.priorities);
	}

	//-------------------------------------------------------------------
	/**
	 * Get settings that need saving, when character is saved during generation mode
	 */
	public Map<PriorityType, Priority> getToSave() { 
		return settings.priorities;
	}

	//-------------------------------------------------------------------
	/**
	 * Load settings that had been saved when character was saved during generation mode
	 */
	public void setToSave(Map<PriorityType, Priority> data) { 
		settings.priorities = data;
	}

	//-------------------------------------------------------------------
	public Priority getPriority(PriorityType option) {
		return settings.priorities.get(option);
	}
	
	//-------------------------------------------------------------------
	public PriorityType getTypeForPrio(Priority prio) {
		for (PriorityType type : PriorityType.values()) {
			if (settings.priorities.get(type)==prio) {
				return type;
			}
		}
		
		return null;
	}

	//-------------------------------------------------------------------
	public Priority getPriorityWithLevelOfPlay(PriorityType option) {
		Priority ret = getPriority(option);
		
		if (settings.variant==PriorityVariant.STREET_LEVEL && ret!=null && ret!=Priority.E)
			ret = Priority.values()[ret.ordinal()+1];
		
		return ret;
	}

	//--------------------------------------------------------------------
	public void setPriority(PriorityType option, Priority prio) {
		logger.debug("........"+settings.priorities);
		Priority oldPriority = getPriority(option);
		PriorityType oldOption = getTypeForPrio(prio);
		
		if (oldOption==option && oldPriority==prio) {
//			parent.runProcessors();
			return;
		}

		settings.priorities.put(option, prio);
		settings.priorities.put(oldOption, oldPriority);
		logger.info("Set "+option+" to "+prio+" and change "+oldOption+" to "+oldPriority);
		
		parent.runProcessors();
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		try {
			// Column 1: Metatype
			PriorityTableEntry entry = ShadowrunCore.getPriorityTableEntry(PriorityType.METATYPE, getPriorityWithLevelOfPlay(PriorityType.METATYPE));
			logger.info("Metatype is "+getPriorityWithLevelOfPlay(PriorityType.METATYPE));
			unprocessed.add(new FreePointsModification(Type.ADJUSTMENT_POINTS, entry.getAdjustmentPoints()));
			for (PriorityOption _opt : entry) {
				MetaTypeOption opt = (MetaTypeOption)_opt;
				unprocessed.add(new AvailableMetaTypeModification(opt));
			}
			// Column 2: Attributes
			Priority prio = getPriorityWithLevelOfPlay(PriorityType.ATTRIBUTE);
			logger.info("Attribute is "+prio);
			switch (prio) {
			case A: unprocessed.add(new FreePointsModification(Type.ATTRIBUTES, 24)); break;
			case B: unprocessed.add(new FreePointsModification(Type.ATTRIBUTES, 16)); break;
			case C: unprocessed.add(new FreePointsModification(Type.ATTRIBUTES, 12)); break;
			case D: unprocessed.add(new FreePointsModification(Type.ATTRIBUTES,  8)); break;
			case E: unprocessed.add(new FreePointsModification(Type.ATTRIBUTES,  2)); break;
			}
			// Column 3: Magic or resonance
			logger.info("Magic/Resonance is "+getPriorityWithLevelOfPlay(PriorityType.MAGIC));
			entry = ShadowrunCore.getPriorityTableEntry(PriorityType.MAGIC, getPriorityWithLevelOfPlay(PriorityType.MAGIC));
			for (PriorityOption _opt : entry) {
				MagicOrResonanceOption opt = (MagicOrResonanceOption)_opt;
				unprocessed.add(new AvailableMagicOrResonanceModification(opt));
			}
			// Column 4: Skills
			prio = getPriorityWithLevelOfPlay(PriorityType.SKILLS);
			logger.info("Skills is "+prio);
			switch (prio) {
			case A: unprocessed.add(new FreePointsModification(Type.SKILLS, 32)); break;
			case B: unprocessed.add(new FreePointsModification(Type.SKILLS, 24)); break;
			case C: unprocessed.add(new FreePointsModification(Type.SKILLS, 20)); break;
			case D: unprocessed.add(new FreePointsModification(Type.SKILLS, 16)); break;
			case E: unprocessed.add(new FreePointsModification(Type.SKILLS, 10)); break;
			}
			// Column 5: Resources
			prio = getPriorityWithLevelOfPlay(PriorityType.RESOURCES);
			logger.info("Resources is "+prio);
			switch (prio) {
			case A: unprocessed.add(new AddNuyenModification(450000)); break;
			case B: unprocessed.add(new AddNuyenModification(275000)); break;
			case C: unprocessed.add(new AddNuyenModification(150000)); break;
			case D: unprocessed.add(new AddNuyenModification( 50000)); break;
			case E: unprocessed.add(new AddNuyenModification(  8000)); break;
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

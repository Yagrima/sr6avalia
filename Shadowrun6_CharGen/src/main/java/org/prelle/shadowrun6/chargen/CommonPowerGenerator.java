/**
 * 
 */
package org.prelle.shadowrun6.chargen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Sense;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.modifications.AdeptPowerModification;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.proc.ApplyAdeptPowerModifications;
import org.prelle.shadowrun6.proc.CharacterProcessor;
import org.prelle.shadowrun6.requirements.Requirement;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CommonPowerGenerator implements AdeptPowerController, CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen.power");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;

	protected ProcessorRunner parent;
	protected ShadowrunCharacter model;
	protected List<AdeptPower> available;
	protected List<ToDoElement> todos;
	protected float powerPointsInvested;
	protected float powerPointsLeft;
	protected Map<AdeptPowerValue, Integer> payedLevel;
	
	//--------------------------------------------------------------------
	public CommonPowerGenerator(CharacterController parent) {
		this.parent = parent;
		model = parent.getCharacter();
		todos = new ArrayList<>();
		available = new ArrayList<>();
		payedLevel = new HashMap<AdeptPowerValue, Integer>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#getPowerPointsInvested()
	 */
	@Override
	public float getPowerPointsInvested() {
		return powerPointsInvested;
	}

	//--------------------------------------------------------------------
	private void updateAvailablePowers() {
		available.clear();
		if (model.getMagicOrResonanceType()==null)
			return;
		if (!model.getMagicOrResonanceType().usesPowers())
			return;
		available = new ArrayList<AdeptPower>();
		for (AdeptPower power : ShadowrunCore.getAdeptPowers()) {			
			if (canBeSelected(power))
				available.add(power);
		}
		
		Collections.sort(available);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#getAvailablePowers()
	 */
	@Override
	public List<AdeptPower> getAvailablePowers() {
		return available;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#getPowerPointsLeft()
	 */
	@Override
	public float getPowerPointsLeft() {
		return powerPointsLeft;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canIncreasePowerPoints()
	 */
	@Override
	public boolean canIncreasePowerPoints() {
		if (!SR6ConfigOptions.HOUSERULE_MYSTADEPT_NEED_TO_BUY_PP.getValue())
			return false;
		
		MagicOrResonanceType magic = model.getMagicOrResonanceType();
		if (magic==null)
			return false;
		if (!magic.usesPowers())
			return false;
		
		// Normal adepts don't increase power points
		if (!magic.paysPowers())
			return false;
		
		// Cannot increase higher than magic attribute
		AttributeValue val = model.getAttribute(Attribute.POWER_POINTS);
		if (val.getModifiedValue() >= model.getAttribute(Attribute.MAGIC).getModifiedValue())
			return false;
		
		// Needs 5 Karma per power point
		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canDecreasePowerPoints()
	 */
	@Override
	public boolean canDecreasePowerPoints() {
		if (!SR6ConfigOptions.HOUSERULE_MYSTADEPT_NEED_TO_BUY_PP.getValue())
			return false;
		
		AttributeValue val = model.getAttribute(Attribute.POWER_POINTS);
		return val.getPoints()>0 && getPowerPointsLeft()>=1.0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#increasePowerPoints()
	 */
	@Override
	public boolean increasePowerPoints() {
		if (!canIncreasePowerPoints())
			return false;
		
		AttributeValue val = model.getAttribute(Attribute.POWER_POINTS);
		val.setPoints(val.getPoints()+1);
		logger.info("Increase power points to "+model.getPowerPoints());
		
		// Inform listeners
		parent.runProcessors();

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#decreasePowerPoint()
	 */
	@Override
	public boolean decreasePowerPoint() {
		if (!canDecreasePowerPoints())
			return false;
		
		AttributeValue val = model.getAttribute(Attribute.POWER_POINTS);
		val.setPoints(val.getPoints()-1);
		logger.info("Decrease power points to "+model.getPowerPoints());
		
		// Inform listeners
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EXPERIENCE_CHANGED, null, new int[] {model.getKarmaFree(), model.getKarmaInvested()}));
		parent.runProcessors();

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeSelected(org.prelle.shadowrun6.AdeptPower)
	 */
	@Override
	public boolean canBeSelected(AdeptPower data) {
		MagicOrResonanceType magic = model.getMagicOrResonanceType();
		if (magic==null)
			return false;
		if (!magic.usesPowers())
			return false;
		
		// Are requirements met?
		for (Requirement req : data.getRequirements()) {
			if (!ShadowrunTools.isRequirementMet(req, model)) {
				logger.debug("Cannot select "+data+" because of unmet requirement: "+req);
				return false;
			}
		}
		
		// If already selected, only
		if (model.hasAdeptPower(data.getId())) {
			// Character already has power. Can it be selected
			// multiple times?
			if (data.getSelectFrom()==null) {
				// No, than this can not be selected, since it already is selected
//				logger.debug("Char already has power "+data.getId()+" that cannot be selected again with different choice");
				return false;
			}
		}
		// Can the character afford the cost
		if (getPowerPointsLeft()<data.getCostForLevel(1)) {
			logger.trace("Char cannot afford power "+data.getId());
			return false;
		}
		
		return true;
	}

	//--------------------------------------------------------------------
	private boolean canBeSelected(AdeptPower data, Object choice) {
		if (!canBeSelected(data))
			return false;
		
		// Are requirements met?
		for (Requirement req : data.getRequirements()) {
			Requirement realReq = ShadowrunTools.instantiateRequirement(req, choice, 1);
			if (!ShadowrunTools.isRequirementMet(realReq, model)) {
				logger.debug("Cannot select "+data+" because of unmet requirement: "+realReq);
				return false;
			}
		}
		
		// If already selected, only
		if (model.hasAdeptPower(data.getId())) {
			// Ensure no given power has same choice
			for (AdeptPowerValue tmp : model.getAdeptPowers()) {
				if (tmp.getModifyable()!=data)
					continue;
				if (tmp.getChoice()==choice) {
					return false;
				}
			}
		}
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#select(org.prelle.shadowrun6.AdeptPower)
	 */
	@Override
	public AdeptPowerValue select(AdeptPower data) {
		logger.debug("try select "+data+"   needsChoice="+data.needsChoice());
		if (data.needsChoice())
			throw new IllegalArgumentException(data.getId()+" needs to be accompanied with a selection");
		if (!canBeSelected(data))
			return null;
		
		AdeptPowerValue ref = new AdeptPowerValue(data);
		ref.setLevel(1);
		model.addAdeptPower(ref);
		logger.info("Selected adept power "+ref);
		
		// Inform listeners
		parent.runProcessors();
		
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#select(org.prelle.shadowrun6.AdeptPower, java.lang.Object)
	 */
	@Override
	public AdeptPowerValue select(AdeptPower data, Object selection) {
//		logger.debug("try select "+data+" with "+selection);
		if (!canBeSelected(data, selection)) {
			logger.error("Trying to select "+data+" which cannot be selected");
			return null;
		}
		
		AdeptPowerValue ref = new AdeptPowerValue(data);
		ref.setLevel(1);
		switch (data.getSelectFrom()) {
		case PHYSICAL_ATTRIBUTE:
			if (!(selection instanceof Attribute) || !((Attribute)selection).isPhysical()) {
				logger.warn("Selection "+selection+" is not a physical attribute");
				return null;
			}
			ref.setChoice((Attribute)selection);
			ref.setChoiceReference(((Attribute)selection).name());
			break;
		case COMBAT_SKILL:
			if (!(selection instanceof Skill))
				return null;
			Skill skilSel = (Skill)selection;
			if (skilSel.getType()!=SkillType.COMBAT) {
				logger.warn("Can only select combat skills");
				return null;
			}
			ref.setChoice((Skill)selection);
			ref.setChoiceReference(((Skill)selection).getId());
//			ref.setSelectedSkill(skilSel);
			break;
		case SKILL:
			if (!(selection instanceof Skill))
				return null;
			skilSel = (Skill)selection;
				ref.setChoice(skilSel);
				ref.setChoiceReference(skilSel.getId());
			break;
		case NONCOMBAT_SKILL:
			if (!(selection instanceof Skill))
				return null;
			skilSel = (Skill)selection;
			if (skilSel.getType()==SkillType.COMBAT) {
				logger.warn("Can only select for non-combat skills");
				return null;
			}
			ref.setChoice((Skill)selection);
			ref.setChoiceReference(((Skill)selection).getId());
//			ref.setSelectedSkill(skilSel);
			break;
		case SENSE:
			if (!(selection instanceof Sense)) {
				logger.warn("Selection "+selection+" is not a sense");
				return null;
			}
			ref.setChoice((Sense)selection);
			ref.setChoiceReference(((Sense)selection).name());
			break;
		default:
			throw new RuntimeException("Not supported yet: "+data.getSelectFrom());
		}
		model.addAdeptPower(ref);
		logger.info("Selected adept power "+ref);
		
		// Inform listeners
		parent.runProcessors();
		
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeDeselected(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean canBeDeselected(AdeptPowerValue ref) {
		if (!model.getAdeptPowers().contains(ref)) {
			return false;
		}
		// Cannot manually deselect automatic given powers
		if (model.getAutoAdeptPowers().contains(ref))
			return false;

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#deselect(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean deselect(AdeptPowerValue ref) {
		logger.debug("try deselect "+ref);
		if (!canBeDeselected(ref))
			return false;
		
		model.removeAdeptPower(ref);
		logger.info("Deselected adept power "+ref);
		
		// Inform listeners
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWER_REMOVED, ref));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_POWERS, null, getPowerPointsLeft()));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POWERS_AVAILABLE_CHANGED, getAvailablePowers()));		
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeIncreased(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean canBeIncreased(AdeptPowerValue ref) {
		// Is the power one that can be increased
		if (!ref.getModifyable().hasLevels())
			return false;
		// Is the level below magic attribute
//		if (ref.getLevel()>=model.getAttribute(Attribute.MAGIC).getModifiedValue())
//			return false;
		
		// Is there enough karma
		float costDiff = ref.getModifyable().getCostForLevel(ref.getLevel()+1) - ref.getModifyable().getCostForLevel(ref.getLevel());
		if (getPowerPointsLeft()<costDiff)
			return false;
		
		/* Hardcoded limitations */
		if (ref.getModifyable().getId().startsWith("improved_ability")) {
			SkillValue sVal = model.getSkillValue((Skill)ref.getChoice());
			if (sVal==null)
				return false;
			int allowed = Math.round(sVal.getPoints()*1.5f);
			if (ref.getLevel()>=allowed)
				return false;
		}
		
		/* Rating may not be higher than magic rating */
		if (ref.getLevel()>=model.getAttribute(Attribute.MAGIC).getModifiedValue())
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeDecreased(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean canBeDecreased(AdeptPowerValue ref) {
		if (!model.getAdeptPowers().contains(ref)) {
			return false;
		}
		return (ref.getLevel()>0);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#increase(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean increase(AdeptPowerValue ref) {
		if (!canBeIncreased(ref))
			return false;
		
		logger.info("Increase power "+ref.getModifyable().getId()+" from "+ref.getLevel()+" to "+(ref.getLevel()+1));
		ref.setLevel(ref.getLevel()+1);
		
		// Inform listeners
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#decrease(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean decrease(AdeptPowerValue ref) {
		if (!canBeDecreased(ref))
			return false;
		
		logger.info("Decrease power "+ref.getModifyable().getId()+" from "+ref.getLevel()+" to "+(ref.getLevel()+1));
		ref.setLevel(ref.getLevel()-1);
		if (ref.getLevel()==0) {
			logger.info("Remove power "+ref.getModifyable().getId());
			model.removeAdeptPower(ref);
		}
			
		
		// Inform listeners
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#isRecommended(org.prelle.shadowrun6.AdeptPower)
	 */
	@Override
	public boolean isRecommended(AdeptPower val) {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

//	//-------------------------------------------------------------------
//	private void mergePowerModification(AdeptPowerModification mod) {
//		for (AdeptPowerValue tmp : model.getUserSelectedAdeptPowers()) {
//			if (tmp.getModifyable()==mod.getPower()) {
//				// Does eventually required choice match?
//				if (mod.getPower().needsChoice() && !mod.getChoice().equals(tmp.getChoiceReference())) {
//					// No
//					continue;
//				}
//				
//				logger.debug("  merge existing "+tmp+" with auto "+mod);
//				if (tmp.getModifyable().hasLevels()) {
//					// Power with levels - increase it by adding it to value
//					tmp.addModification(mod);
//				} else {
//					// Power without level is user selected and auto added at the same time!!
//				}
//				return;
//			}
//		}
//		// Not found yet - auto-add it
//		AdeptPowerValue toAdd = new AdeptPowerValue(mod.getPower());
//		if (mod.getPower().hasLevels()) {
//			toAdd.setLevel(mod.getValue());
//		}
//		if (mod.getPower().needsChoice()) {
//			toAdd.setChoiceReference(mod.getChoice());
//			toAdd.setChoice(ShadowrunTools.resolveChoiceType(mod.getPower().getSelectFrom(), mod.getChoice()));
//			if (toAdd.getChoice()==null)
//				throw new IllegalArgumentException("Could not resolve "+mod.getPower().getSelectFrom()+" "+mod.getChoice());
//		}
//		
//		model.addAutoAdeptPower(toAdd);
//		logger.debug("  auto-add power "+toAdd);
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			todos.clear();
			payedLevel.clear();
			powerPointsInvested = 0;
			powerPointsLeft    = 0;
			
			AttributeValue val = model.getAttribute(Attribute.POWER_POINTS);
//			val.clearModifications();

			// Process Modifications
			for (Modification tmp : previous) {
				if (tmp instanceof AdeptPowerModification) {
					ApplyAdeptPowerModifications.mergePowerModification(model, (AdeptPowerModification) tmp);
//					mergePowerModification((AdeptPowerModification) tmp);
				} else if (tmp instanceof AttributeModification) {
					AttributeModification mod = (AttributeModification) tmp;
					if (mod.getAttribute()==Attribute.POWER_POINTS) {
						model.getAttribute(Attribute.POWER_POINTS).addModification(mod);
					} else
						unprocessed.add(tmp);
				} else
					unprocessed.add(tmp);
			}
			
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesPowers()) {
//				// Only for adepts: power points are equal to their magic attribute
//				if (!model.getMagicOrResonanceType().paysPowers()) {
//					AttributeModification aMod = new AttributeModification(ModificationValueType.NATURAL, Attribute.POWER_POINTS, model.getAttribute(Attribute.MAGIC).getModifiedValue());
//					aMod.setSource(model.getMagicOrResonanceType());
//					val.addModification(aMod);
//					logger.debug("Add base power mod "+aMod);
//					logger.debug("After = "+val);
//				} else {
//					logger.debug("No base power mod ");
//				}

				powerPointsLeft    = val.getModifiedValue();
				logger.debug("  Start Power Points: "+powerPointsLeft+" from "+val);
				for (Modification mod : val.getModifications()) {
					logger.debug("  mod = "+mod);
				}

				// Apply modifications by Adept Powers
				for (AdeptPowerValue ref :model.getAdeptPowers()) {
					logger.info("POWER "+ref);
					if (ref.getModifications()!=null && !ref.getModifications().isEmpty()) {
						logger.debug(" - "+ref.getModifyable().getId()+" has modifications: "+ref.getModifications());
						for (Modification mod : ref.getModifications()) {
							mod.setSource(ref.getModifyable());
						}
						unprocessed.addAll(ref.getModifications());
					}
				}

				/*
				 * Pay bought power points
				 */
				if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesPowers()) {
					model.setKarmaFree(model.getKarmaFree()-(5*val.getPoints()));
					logger.debug("  buy "+val.getPoints()+" power points for "+(5*val.getPoints()+" karma"));
				}
				logger.debug("  Power Points: "+powerPointsLeft);
				// Pay spells
				/*
				 * Walk through powers
				 */
				for (AdeptPowerValue tmp : model.getUserSelectedAdeptPowers()) {
					if (model.getAutoAdeptPowers().contains(tmp))
						continue;
					AdeptPower power = tmp.getModifyable();
					float cost = (float)power.getCost();
					if (power.hasLevels()) {
						cost = power.getCostForLevel(tmp.getLevel());
					} else {
						// If already payed, set it to no cost
					}

					logger.info(String.format("Pay %.3f power points for %s", cost, tmp.getName()));
					powerPointsLeft -= cost;
					powerPointsInvested += cost;
				}
				logger.debug("Power Points left: "+powerPointsLeft);

				/*
				 * Determine available powers
				 */
				updateAvailablePowers();


				// ToDos
				if (powerPointsLeft!=0 && model.getMagicOrResonanceType().usesPowers()) {
					todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES,"priogen.todo.power", powerPointsLeft)));
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.chargen.cost;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeNumericalValue;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class PriorityEquipmentCostCalculator implements CharacterProcessor {
	
	private static final Logger logger = LogManager.getLogger("shadowrun.gen.cost");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	private Priority priority;
	private int investedKarma;
	private int maxInvestedKarma;

	//-------------------------------------------------------------------
	/**
	 */
	public PriorityEquipmentCostCalculator() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public void setPriority(Priority priority) {
		this.priority = priority;
		logger.info("setPriority("+priority+")");
	}

	//-------------------------------------------------------------------
	public void setInvestedKarma(int val) {
		this.investedKarma = val;
	}

	//-------------------------------------------------------------------
	public void setKarmaInvestLimit(int maxConvertibleKarma) {
		this.maxInvestedKarma = maxConvertibleKarma;
	}

	//--------------------------------------------------------------------
	public List<String> getToDos() {
		List<String> ret = new ArrayList<>();
		if (investedKarma>maxInvestedKarma) {
			ret.add(String.format(RES.getString("equip.todo.too_much_karma"), investedKarma, maxInvestedKarma));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.debug("START: process");
		try {
			/*
			 * Calculate Nuyen depending on priority
			 */
			int baseNuyen = 0;
			if (priority!=null) {
				switch (priority) {
				case A: baseNuyen=450000; break;
				case B: baseNuyen=275000; break;
				case C: baseNuyen=140000; break;
				case D: baseNuyen= 50000; break;
				case E: baseNuyen=  6000; break;
				}
			}

			/* Add 2000 per invested karma */
			baseNuyen += investedKarma*2000;
			if (investedKarma>0) {
				logger.debug("Pay "+investedKarma+" karma for "+(investedKarma*2000)+" nuyen");
				model.setKarmaFree( model.getKarmaFree()-investedKarma);
			}			
			logger.debug("Spend "+baseNuyen+" Nuyen");

			/*
			 * CarriedItems
			 */
			for (CarriedItem item : model.getItems(false)) {
				if (item.isCreatedByModification())
					continue;
				ItemAttributeNumericalValue itemVal = item.getAsValue(ItemAttribute.PRICE);
//				logger.debug("* "+item+" = "+itemVal);
				baseNuyen -= itemVal.getModifiedValue();
			}
			
			// Store result in character
			model.setNuyen(baseNuyen);
		} finally {
			logger.debug("STOP : process()");
		}
		
		return unprocessed;
	}

}

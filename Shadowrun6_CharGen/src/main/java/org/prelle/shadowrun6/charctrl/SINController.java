/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import org.prelle.shadowrun6.LicenseType;
import org.prelle.shadowrun6.LicenseValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.proc.CharacterProcessor;

/**
 * @author prelle
 *
 */
public interface SINController extends Controller, CharacterProcessor {

	//-------------------------------------------------------------------
	public boolean canCreateNewSIN(SIN.Quality quality);

	//-------------------------------------------------------------------
	public boolean canCreateNewSIN(SIN.Quality quality, int count);

	//-------------------------------------------------------------------
	public boolean canDeleteSIN(SIN data);

	//-------------------------------------------------------------------
	public SIN createNewSIN(String name, SIN.Quality quality);

	//-------------------------------------------------------------------
	public SIN[] createNewSIN(String name, SIN.Quality quality, int count);

	//-------------------------------------------------------------------
	public boolean deleteSIN(SIN data);
	

	//-------------------------------------------------------------------
	public boolean canCreateNewLicense(LicenseType type, SIN sin, SIN.Quality quality);

	//-------------------------------------------------------------------
	public LicenseValue createNewLicense(LicenseType type, SIN sin, SIN.Quality quality);

}

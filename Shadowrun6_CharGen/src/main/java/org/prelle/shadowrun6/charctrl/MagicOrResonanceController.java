/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.MagicOrResonanceOption;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Tradition;

/**
 * @author Stefan
 *
 */
public interface MagicOrResonanceController extends Controller {

	//--------------------------------------------------------------------
	public List<MagicOrResonanceOption> getAvailable();
	
	//--------------------------------------------------------------------
	public boolean canBeSelected(MagicOrResonanceOption type);
	
	//--------------------------------------------------------------------
	public void select(MagicOrResonanceOption value);
	
	//--------------------------------------------------------------------
	public int mysticAdeptGetSpellPoints();
	
	//--------------------------------------------------------------------
	public int mysticAdeptGetPowerPoints();
	
	//--------------------------------------------------------------------
	public boolean mysticAdeptCanIncreasePowerPoint();
	
	//--------------------------------------------------------------------
	public boolean mysticAdeptCanDecreasePowerPoint();
	
	//--------------------------------------------------------------------
	public boolean mysticAdeptIncreasePowerPoint();
	
	//--------------------------------------------------------------------
	public boolean mysticAdeptDecreasePowerPoint();
	
	//--------------------------------------------------------------------
	public void setAspectSkill(Skill skill);

	//--------------------------------------------------------------------
	public void selectTradition(Tradition value);
	
}

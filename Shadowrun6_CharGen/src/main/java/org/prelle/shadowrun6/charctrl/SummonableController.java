/**
 *
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.Summonable;
import org.prelle.shadowrun6.SummonableValue;
import org.prelle.shadowrun6.proc.CharacterProcessor;

/**
 * @author Stefan
 *
 */
public interface SummonableController extends Controller  {

	public List<Summonable> getAvailable();

	public boolean canBeSelected(Summonable value);

	public boolean canBeDeselected(SummonableValue value);

	public SummonableValue select(Summonable value, int services);

	public void deselect(SummonableValue value);

}

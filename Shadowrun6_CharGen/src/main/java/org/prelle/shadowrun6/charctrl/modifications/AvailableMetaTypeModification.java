/**
 * 
 */
package org.prelle.shadowrun6.charctrl.modifications;

import java.util.Date;

import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.MetaTypeOption;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class AvailableMetaTypeModification implements Modification {
	
	private MetaTypeOption metatype;

	//--------------------------------------------------------------------
	public AvailableMetaTypeModification(MetaTypeOption value) {
		this.metatype = value;
	}

	//--------------------------------------------------------------------
	public String toString() { return metatype.getType().getId(); }

	//--------------------------------------------------------------------
	/**
	 * @return the metatype
	 */
	public MetaTypeOption getMetaType() {
		return metatype;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(Date date) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Modification clone() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getExpCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setExpCost(int expCost) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSource(Object src) {
		// TODO Auto-generated method stub
		
	}

}

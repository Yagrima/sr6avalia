/**
 *
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ComplexForm;
import org.prelle.shadowrun6.ComplexFormValue;
import org.prelle.shadowrun6.proc.CharacterProcessor;

/**
 * @author prelle
 *
 */
public interface ComplexFormController extends Controller, CharacterProcessor {

	//--------------------------------------------------------------------
	/**
	 * Returns the number of complex forms left to select
	 */
	public int getComplexFormsLeft();

	//-------------------------------------------------------------------
	public List<ComplexForm> getAvailableComplexForms();

	//-------------------------------------------------------------------
	public boolean canBeSelected(ComplexForm data);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(ComplexFormValue data);

	//-------------------------------------------------------------------
	public ComplexFormValue select(ComplexForm data);

	//-------------------------------------------------------------------
	public ComplexFormValue select(ComplexForm data, Object choice);

	//-------------------------------------------------------------------
	public void deselect(ComplexFormValue data);

}

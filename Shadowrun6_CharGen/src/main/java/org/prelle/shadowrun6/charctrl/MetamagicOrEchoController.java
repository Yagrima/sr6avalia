package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.MetamagicOrEchoValue;

/**
 * @author Stefan Prelle
 *
 */
public interface MetamagicOrEchoController extends Controller {

	//-------------------------------------------------------------------
	public List<MetamagicOrEcho> getAvailableMetamagics();

	//-------------------------------------------------------------------
	public boolean canBeSelected(MetamagicOrEcho data);

	//-------------------------------------------------------------------
	public MetamagicOrEchoValue select(MetamagicOrEcho data);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(MetamagicOrEchoValue ref);

	//-------------------------------------------------------------------
	public boolean deselect(MetamagicOrEchoValue ref);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(MetamagicOrEchoValue data);

	//-------------------------------------------------------------------
	public boolean increase(MetamagicOrEchoValue data);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(MetamagicOrEchoValue data);

	//-------------------------------------------------------------------
	public boolean decrease(MetamagicOrEchoValue data);

}

/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemTemplate;

/**
 * @author prelle
 *
 */
public interface MagicEquipmentController extends Controller {

	public int getBindingCost(ItemTemplate focus, int power);

	public boolean canBindFocus(CarriedItem focus);

	public boolean canUndoBindFocus(CarriedItem focus);

	public boolean bindFocus(CarriedItem focus);

	public boolean undoBindFocus(CarriedItem focus);
	
}

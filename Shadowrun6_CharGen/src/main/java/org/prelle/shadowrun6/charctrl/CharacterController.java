/**
 *
 */
package org.prelle.shadowrun6.charctrl;

import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.gen.WizardPageType;

import de.rpgframework.ConfigContainer;

/**
 * @author prelle
 *
 */
public interface CharacterController extends ProcessorRunner {

	//-------------------------------------------------------------------
	public void attachConfigurationTree(ConfigContainer addBelow);
	
	//-------------------------------------------------------------------
	public WizardPageType[] getWizardPages();

	//-------------------------------------------------------------------
	public CharGenMode getMode();

	//-------------------------------------------------------------------
	public String getName();

	//-------------------------------------------------------------------
	public ShadowrunCharacter getCharacter();

	//-------------------------------------------------------------------
	public AttributeController getAttributeController();

	//-------------------------------------------------------------------
	public SkillController getSkillController();

	//-------------------------------------------------------------------
	public SpellController getSpellController();

	//-------------------------------------------------------------------
	public SpellController getAlchemyController();

	//-------------------------------------------------------------------
	public RitualController getRitualController();

	//-------------------------------------------------------------------
	public QualityController getQualityController();

	//-------------------------------------------------------------------
	public AdeptPowerController getPowerController();

	//-------------------------------------------------------------------
	public ComplexFormController getComplexFormController();

	//-------------------------------------------------------------------
	public EquipmentController getEquipmentController();

	//-------------------------------------------------------------------
	public ConnectionsController getConnectionController();

	//-------------------------------------------------------------------
	public SINController getSINController();

	//-------------------------------------------------------------------
	public LifestyleController getLifestyleController();

	//-------------------------------------------------------------------
	public SummonableController getSummonableController();

	//-------------------------------------------------------------------
	public MetamagicOrEchoController getMetamagicOrEchoController();

	//-------------------------------------------------------------------
	public FocusController getFocusController();

}

/**
 * 
 */
package org.prelle.shadowrun6.charctrl;

import java.util.List;

import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.Tradition;

/**
 * @author prelle
 *
 */
public interface SpellController extends Controller {

	//--------------------------------------------------------------------
	/**
	 * Returns the number of spells left to select
	 */
	public int getSpellsLeft();

	//-------------------------------------------------------------------
	public List<Spell> getAvailableSpells(boolean alchemistic);

	//-------------------------------------------------------------------
	public List<Spell> getAvailableSpells(Spell.Category category, boolean alchemistic);


	//-------------------------------------------------------------------
	public boolean canBeSelected(Spell data, boolean alchemistic);

	//-------------------------------------------------------------------
	public boolean canBeDeselected(SpellValue data);

	//-------------------------------------------------------------------
	public SpellValue select(Spell data, boolean alchemistic);

	//-------------------------------------------------------------------
	public void deselect(SpellValue data);

	//--------------------------------------------------------------------
	public void changeMagicTradition(Tradition data);

}

package org.prelle.shadowrun6.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class FocusGenerator implements CharacterProcessor, FocusController {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.genlvl.focus");
	protected ShadowrunCharacter model;
	protected ProcessorRunner parent;
	
	private List<ToDoElement> todos;
	private int forcePool;

	//--------------------------------------------------------------------
	/**
	 */
	public FocusGenerator(ProcessorRunner parent) {
		this.parent = parent;
		todos      = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#getFocusPointsLeft()
	 */
	@Override
	public int getFocusPointsLeft() {
		return forcePool;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#canBeSelected(org.prelle.shadowrun6.Focus, int)
	 */
	@Override
	public boolean canBeSelected(Focus data, int rating) {
		/*
		 * You can’t bond more foci than your Magic attribute,
		 * and the maximum Force of all your bonded
		 * foci can’t exceed your Magic x 5.
		 */		
		if (model.getFoci().size()>=model.getAttribute(Attribute.MAGIC).getModifiedValue()) {
			// More than magic attribute
			return false;
		}

		// maximum force
		if (rating>forcePool)
			return false;
		
		return !data.needsChoice();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#canBeSelected(org.prelle.shadowrun6.Focus, int, java.lang.Object)
	 */
	@Override
	public boolean canBeSelected(Focus data, int rating, Object choice) {
		/*
		 * You can’t bond more foci than your Magic attribute,
		 * and the maximum Force of all your bonded
		 * foci can’t exceed your Magic x 5.
		 */		
		if (model.getFoci().size()>=model.getAttribute(Attribute.MAGIC).getModifiedValue()) {
			// More than magic attribute
			return false;
		}
		// maximum force
		if (rating>forcePool)
			return false;
		
		if (!data.needsChoice())
			return false;
		
		try {
			if (ShadowrunTools.getChoiceReference(data.getChoice(), choice)==null) {
				logger.warn("Given choice for focus does not match type "+data.getChoice()+": "+choice);
				return false;
			}
		} catch (Exception e) {
			logger.warn("Error verifying choice type "+data.getChoice()+": "+choice);
			return false;
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#canBeDeselected(org.prelle.shadowrun6.FocusValue)
	 */
	@Override
	public boolean canBeDeselected(FocusValue data) {
		return model.getFoci().contains(data);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#select(org.prelle.shadowrun6.Focus, int)
	 */
	@Override
	public FocusValue select(Focus data, int rating) {
		if (!canBeSelected(data, rating)) 
			return null;
		
		FocusValue val = new FocusValue(data, rating);
		model.addFocus(val);
		logger.info("Selected focus "+val);
		
		parent.runProcessors();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#select(org.prelle.shadowrun6.Focus, int, java.lang.Object)
	 */
	@Override
	public FocusValue select(Focus data, int rating, Object choice) {
		if (!canBeSelected(data, rating, choice)) 
			return null;
		
		FocusValue val = new FocusValue(data, rating);
		val.setChoice(choice);
		val.setChoiceReference(ShadowrunTools.getChoiceReference(data.getChoice(), choice));
		model.addFocus(val);
		logger.info("Selected focus "+val);
		
		parent.runProcessors();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#deselect(org.prelle.shadowrun6.FocusValue)
	 */
	@Override
	public boolean deselect(FocusValue data) {
		if (!canBeDeselected(data))
			return false;

		model.removeFocus(data);
		logger.info("Deselected focus "+data);

		parent.runProcessors();
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		this.model = model;
		try {
			forcePool = model.getAttribute(Attribute.MAGIC).getModifiedValue()*5;
			
			// Pay karma for selected foci - Nuyen are paid in EquipmentController
			for (FocusValue focus : model.getFoci()) {
				int karma = focus.getCostKarma();
				logger.info("Pay "+karma+" Karma for force "+focus.getLevel()+" focus "+focus);
				model.setKarmaFree(model.getKarmaFree()-karma);
				
				forcePool -= focus.getLevel();
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");			
		}
		return unprocessed;
	}

}

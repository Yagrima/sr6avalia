/**
 *
 */
package org.prelle.shadowrun6.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.items.AccessoryData;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.BodytechQuality;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemTemplate.Multiply;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.items.proc.ItemRecalculation;
import org.prelle.shadowrun6.requirements.AnyRequirement;
import org.prelle.shadowrun6.requirements.ItemHookRequirement;
import org.prelle.shadowrun6.requirements.ItemRequirement;
import org.prelle.shadowrun6.requirements.ItemSubTypeRequirement;
import org.prelle.shadowrun6.requirements.ItemTypeRequirement;
import org.prelle.shadowrun6.requirements.Requirement;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public abstract class CommonEquipmentController implements EquipmentController {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen.items");
	
	protected ProcessorRunner parent;
	protected ShadowrunCharacter model;
	private CharGenMode mode;
	
	private Map<Modification, CarriedItem> itemsByMods;
	protected List<ToDoElement> todos;

	//--------------------------------------------------------------------
	public CommonEquipmentController(ProcessorRunner parent, ShadowrunCharacter model, CharGenMode mode) {
		this.parent= parent;
		this.model = model;
		this.mode  = mode;
		todos = new ArrayList<>();
		itemsByMods = new HashMap<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getCost(org.prelle.shadowrun6.items.ItemTemplate, int, org.prelle.shadowrun6.items.BodytechQuality)
	 */
	@Override
	public int getCost(ItemTemplate item, UseAs usage, SelectionOption...options) {
		float cost = item.getPrice();
		if (usage.getExtraCost()>0)
			cost += usage.getExtraCost();
		
		for (SelectionOption option : options) {
			switch (option.getType()) {
			case AMOUNT:
				cost *= option.getAsAmount();
				break;
			case BODYTECH_QUALITY:
				if (Arrays.asList(ItemType.bodytechTypes()).contains(usage.getType())) {
					switch (option.getAsBodytechQuality()) {
					case ALPHA: cost *= 1.2; break;
					case BETA : cost *= 1.5; break;
					case DELTA: cost *= 2.5; break;
					case USED : cost *= 0.5; break;
					default:
					}
				}
				break;
			case RATING:
				if (item.hasRating()) {
					if (Arrays.asList(item.getMultiplyWithRate()).contains(Multiply.PRICE)) {
						if (item.getPriceTable()!=null) {
							cost = item.getPriceTable()[option.getAsRating()-1]*option.getAsRating();
						} else
							cost *= option.getAsRating();
					} else if (Arrays.asList(item.getMultiplyWithRate()).contains(Multiply.PRICE2)) {
						if (item.getPriceTable()!=null) {
							cost = item.getPriceTable()[option.getAsRating()-1]*(option.getAsRating()*option.getAsRating());
						} else
							cost *= (option.getAsRating()*option.getAsRating());
					}
				}
				break;
			case SKILL:
			case PHYSICAL_SKILL:
				break;
			case AMMOTYPE:
				cost *= option.getAsAmmoType().getCostMultiplier();
				break;
			default:
				logger.warn("Don't know what to do for "+option);
				System.err.println("Don't know what to do for "+option);
			}
		}
		
		/*
		 * Now apply cost multiplier for dwarfs and trolls
		 * CRB 247 / GRW 248
		 */
		MetaType meta = model.getMetatype();
		if (meta.getVariantOf()!=null)
			meta = meta.getVariantOf();
		switch (meta.getId()) {
		case "dwarf":
			if (item.isType(ItemType.ARMOR) || item.isType(ItemType.ARMOR_ADDITION)) {
				cost *= 1.1f;
			}
			break;
		case "troll":
			cost *= 1.1f;
			break;
		}
		
		return Math.round(cost);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getAvailability(org.prelle.shadowrun6.items.ItemTemplate, org.prelle.shadowrun6.items.UseAs, org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption[])
	 */
	@Override
	public Availability getAvailability(ItemTemplate item, UseAs usage, SelectionOption...options) {
		Availability avail = item.getAvailability();
		if (usage.getAlternateAvailability()!=null)
			avail = usage.getAlternateAvailability();
		
		
		Availability baseAvail = avail;
		for (SelectionOption option : options) {
			switch (option.getType()) {
			case BODYTECH_QUALITY:
				if (Arrays.asList(ItemType.bodytechTypes()).contains(usage.getType())) {
					switch (option.getAsBodytechQuality()) {
					case ALPHA: avail=new Availability(baseAvail.getValue()+1, baseAvail.getLegality(), baseAvail.isAddToAvailability()); break;
					case  BETA: avail=new Availability(baseAvail.getValue()+2, baseAvail.getLegality(), baseAvail.isAddToAvailability()); break;
					case DELTA: avail=new Availability(baseAvail.getValue()+3, baseAvail.getLegality(), baseAvail.isAddToAvailability()); break;
					case USED : avail=new Availability(baseAvail.getValue()-1, baseAvail.getLegality(), baseAvail.isAddToAvailability()); break;
					default:
					}
				}
				break;
			case RATING:
				if (item.hasRating()) {
					if (Arrays.asList(item.getMultiplyWithRate()).contains(Multiply.AVAIL))  {
						logger.warn("Change availability from "+avail.getValue()+" to *"+option.getAsRating());
						avail=new Availability(avail.getValue()*option.getAsRating(), baseAvail.getLegality(), baseAvail.isAddToAvailability());
					} else if (Arrays.asList(item.getMultiplyWithRate()).contains(Multiply.AVAIL3))  {
						logger.warn("Calculate availability from "+option.getAsRating());
						avail=new Availability(Math.round((float)option.getAsRating()/3.0f), baseAvail.getLegality(), baseAvail.isAddToAvailability());
					}
				}
				break;
			default:
				logger.debug("Don't know how to handle option "+option);
			}
		}
		
		return avail;
	}

	//--------------------------------------------------------------------
	private SelectionOption getOptionFor(ChoiceType choice, SelectionOption...options) {
		for (SelectionOption opt : options) {
			if (choice==ChoiceType.SKILL && opt.getType()==SelectionOptionType.SKILL)
				return opt;
			if (choice==ChoiceType.PHYSICAL_SKILL && opt.getType()==SelectionOptionType.PHYSICAL_SKILL)
				return opt;
			if (choice==ChoiceType.AMMUNITION_TYPE && opt.getType()==SelectionOptionType.AMMOTYPE)
				return opt;
		}
		logger.warn("Cannot find selection option for "+choice+" in options "+Arrays.toString(options));
		return null;
	}
	
	
	//-------------------------------------------------------------------
	/*
	 * Now apply cost multiplier for dwarfs and trolls
	 * CRB 247 / GRW 248
	 */
	protected int getCostWithMetatypeModifier(ItemTemplate item, float cost) {
		return ShadowrunTools.getCostWithMetatypeModifier(model, item, cost);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canBePayed(org.prelle.shadowrun6.items.ItemTemplate, boolean, org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption[])
	 */
	@Override
	public boolean canBePayed(ItemTemplate item, boolean asAccessory, SelectionOption...options) {
		if (item==null)
			throw new NullPointerException();
		// Is paying gear required at all?
		Boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear==null)
			payGear=true;
		if (mode==CharGenMode.LEVELING && !payGear)
			return true;
		
		UseAs usage = item.getDefaultUsage();
		if (asAccessory) {
			for (UseAs tmp : item.getUseAs()) {
				if (tmp.getType()==ItemType.ACCESSORY) {
					usage = tmp;
					break;
				}
			}
		}
		if (usage==null)
			throw new NullPointerException("No usage found for "+item+" and accessory="+asAccessory);
			
		
		// Calculate price and check it
		int price = getCost(item, usage, options);
		price =  Math.round(getCostWithMetatypeModifier(item, price));
		
		if (price>model.getNuyen())
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canBeSelected(org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public boolean canBeSelected(ItemTemplate item, UseAs usage, SelectionOption...options) {
		if (item==null)
			throw new NullPointerException();
		
		if (usage==null)
			usage = getSuitableNonAccessoryUsage(item, options);
		logger.debug("canBeSelected as "+usage+" with options "+Arrays.asList(options));
		if (usage==null) {
			logger.warn("No suitable non-accessory usage found for "+item.getId());
			return false;
		}
		
		// Check if gear has requirement
		for(Requirement tmp : item.getRequirements()) {
			if (!ShadowrunTools.isRequirementMet(tmp, model)) {
				logger.warn("Cannot select "+item+" because requirement not met: "+tmp);
				return false;
			}
		}
		
		
		// Check if eventually required rating is present
		if (item.hasRating()) {
			int rating = -99;
			int maxRat = (usage.getMaxRating()>0)?usage.getMaxRating():item.getMaximumRating();
			logger.debug("maximum rating is "+maxRat);
			for (SelectionOption opt : options) {
				if (opt.getType()==SelectionOptionType.RATING) {
					rating = opt.getAsRating();
				}
			}
			if (rating==-99)
				throw new IllegalArgumentException("Item "+item.getId()+" needs a rating SelctionOption");
			if (rating>maxRat )
				throw new IllegalArgumentException("Rating too high (max="+maxRat+")");
			if (rating<item.getMinimumRating())
				throw new IllegalArgumentException("Rating too low (min="+item.getMinimumRating()+")");
		}
		
		
		// Calculate price and check it
		int price = getCost(item, usage, options);
		
		// Is paying gear required at all?
		boolean payGear = (SR6ConfigOptions.PAY_GEAR!=null)?((Boolean)SR6ConfigOptions.PAY_GEAR.getValue()):false;
		if (payGear && price>model.getNuyen())
			return false;
		
//		if (item.getChoice()!=null) {
//			SelectionOption option = getOptionFor(item.getChoice(), options);
//			if (option==null) {
//				logger.warn("Cannot select "+item+" - missing selection option for choice type "+item.getChoice());
//				return false;
//			}
//		}
		
		return true;
	}

	//-------------------------------------------------------------------
	private CarriedItem buildItem(ItemTemplate item, UseAs usage, SelectionOption...options) {
		if (usage==null)
			throw new NullPointerException("UseAs");
		int rating = 0;
		for (SelectionOption opt : options) {
			if (opt.getType()==SelectionOptionType.RATING)
				rating = opt.getAsRating();
		}
		
		CarriedItem ref = (item.hasRating())?(new CarriedItem(item,usage,rating)):(new CarriedItem(item,usage));
		BodytechQuality quality = null;
		if (usage.getType()==ItemType.CYBERWARE || usage.getType()==ItemType.BIOWARE || usage.getType()==ItemType.NANOWARE)
			quality = BodytechQuality.STANDARD;
		
		// Modify according to options
		for (SelectionOption option : options) {
			switch (option.getType()) {
			case AMOUNT:
				ref.setCount(option.getAsAmount());
				break;
			case BODYTECH_QUALITY:
				quality = option.getAsBodytechQuality();
				break;
			case RATING:
				break;
			case SKILL:
			case PHYSICAL_SKILL:
				ref.setChoice(option.getAsSkill());
				ref.setChoiceReference(  option.getAsSkill().getId() );
				break;
			case AMMOTYPE:
				ref.setChoice(option.getAsAmmoType());
				ref.setChoiceReference(  option.getAsAmmoType().getId() );
				break;
			case NAME:
				ref.setChoice(option.getAsName());
				ref.setChoiceReference(  option.getAsName() );
				break;
			default:
				logger.error("Unprocessed SelectionOptionType "+option.getType());
				System.err.println("Unprocessed SelectionOptionType "+option.getType());
			}
		}

		if (quality!=null)
			ref.setQuality(quality);
		
		// Should be obsolete by new item (sub)type management
//		// Sanity check
//		if (item.getChoice()!=null) {
//			switch (item.getChoice()) {
//			case WEAPON:
//				if (ref.getChoice()==null) {
//					logger.warn("Item "+item.getId()+" requires an SelectionOption.ACCESSORY");
//					return null;
//				}
//				ItemTemplate access = (ItemTemplate)ref.getChoice();
//				if (item.getChoiceSubType()!=null && access.getSubtype()!=item.getChoiceSubType()) {
//					logger.warn("Item "+item.getId()+" requires an SelectionOption.ACCESSORY of subtype "+item.getChoiceSubType()+" but found "+access.getSubtype());
//					return null;				
//				}
//			}
//		}
		
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#select(org.prelle.shadowrun6.items.ItemTemplate, int)
	 */
	@Override
	public CarriedItem select(ItemTemplate item, SelectionOption...options) {
		logger.debug("START-------select "+item.getId()+"-------------"+Arrays.toString(options));
		if (!canBeSelected(item, item.getDefaultUsage(), options)) {
			logger.warn("Cannot select "+item);
			return null;
		}
		if (item.getChoice()!=null) {
			SelectionOption option = getOptionFor(item.getChoice(), options);
			if (option==null) {
				logger.warn("Cannot select "+item+" - missing selection option for choice type "+item.getChoice());
				return null;
			}
		}
		logger.debug("Options: "+Arrays.toString(options));

		UseAs usage = getSuitableNonAccessoryUsage(item, options);
		CarriedItem ref = buildItem(item, usage, options);
		ref.setUsedAsType(usage.getType());
		ref.setUsedAsSubType(usage.getSubtype());
		logger.info("Select "+ref);
		model.addItem(ref);
		
		logger.debug("STOP -------select "+item.getId()+"-------------");
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#deselect(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean deselect(CarriedItem ref) {
		boolean removed = model.removeItem(ref);
		logger.info("Remove "+ref.getName()+" = "+removed);
		// If it was an accessory, also remove it there
		if (ref.getEmbeddedIn()!=null) {
			CarriedItem container = model.getItem(ref.getEmbeddedIn());
			if (container!=null) {
				boolean r2 = container.removeAccessory(ref);
				if (r2) {
					logger.info("Also removed "+ref.getName()+" from  parent "+container.getName());
					removed = true;
				} else {
					logger.debug("Item not found\n"+container.dump());
				}
			} else {
				logger.warn("Container to remove item from is unknown - possibly a deprecated saving format");
			} 
		}
		
		// If not found directly, check accessory slots
		if (!removed) {
			logger.debug("Item to remove not found directly - check accessories");
			for (CarriedItem tmp : model.getItems(false)) {
				for (CarriedItem access : tmp.getUserAddedAccessories()) {
					if (access==ref) {
						logger.debug("Remove item "+ref.getName()+"  EMBEDDED IN "+tmp.getName()+" in "+access.getSlot());
						removed = tmp.removeAccessory(ref);
						if (removed)
							tmp.getSlot(access.getSlot()).removeEmbeddedItem(access);
						ItemRecalculation.recalculate("", tmp);
						break;
					}
				}
			}
		}
		if (!removed) 
			logger.warn("Item to remove not found");
		
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#sell(org.prelle.shadowrun6.items.CarriedItem, float)
	 */
	@Override
	public boolean sell(CarriedItem ref, float factor) {
		boolean removed = model.removeItem(ref);
		// If not found directly, check accessory slots
		if (!removed) {
			logger.debug("Item to remove not found directly - check accessories");
			for (CarriedItem tmp : model.getItems(false)) {
				for (CarriedItem access : tmp.getUserAddedAccessories()) {
					if (access==ref) {
						logger.debug("Remove item "+ref.getName()+"  EMBEDDED IN "+tmp.getName());
						removed = tmp.removeAccessory(ref);
						logger.debug(tmp.dump());
						ItemRecalculation.recalculate("", tmp);
						break;
					}
				}
			}
		}
		if (!removed) {
			logger.warn("Item to sell not found");
		}
		return removed;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getEmbeddingSlotState(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public Map<ItemHook, EmbedOption> getEmbeddingSlotState(CarriedItem container, ItemTemplate n) {
		logger.debug("getEmbeddingSlotState("+n+")");
		Map<ItemHook, EmbedOption> ret = new HashMap<>();
		if (n==null)
			return ret;
		logger.debug("  slots: "+container.getSlots());
		for (AvailableSlot slot : container.getSlots()) {
			ret.put(slot.getSlot(), EmbedOption.NOT_POSSIBLE);
			if (slot.getSlot()==ItemHook.INTERNAL)
				continue;
			boolean canHaveMultiple = slot.getSlot().hasCapacity();
			if (canBeEmbedded(container, n, slot.getSlot())) {
				if (slot.getAllEmbeddedItems().isEmpty() || canHaveMultiple) {
					ret.put(slot.getSlot(), EmbedOption.POSSIBLE);
				} else {
					ret.put(slot.getSlot(), EmbedOption.AFTER_REMOVING);
				}
			}
			logger.debug("  slot "+slot.getSlot()+" = "+ret.get(slot.getSlot()));
		}
		
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getEmbeddableIn(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref) {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
		for (ItemTemplate item : ShadowrunCore.getItems(ItemType.ACCESSORY)) {
			if (!ref.hasEmbedded(item))
				ret.add(item);
		}
		return ret;
	}

//	//-------------------------------------------------------------------
//	protected List<ItemHook> getPossibleEmbedHooks(ItemTemplate item) {
//		List<ItemHook> ret = new ArrayList<>();
//		
//		// Ensure that all slot requirements are met
//		for (Requirement req : item.getRequirements()) {
//			if (req instanceof ItemHookRequirement) {
//				ItemHookRequirement hookReq = (ItemHookRequirement)req;
//				ret.add(hookReq.getSlot());
//			}
//		}
////		// All items without embed hook, require EXTERNAL
////		if (ret.isEmpty() || item.getType()==ItemType.WEAPON)
////			ret.add(ItemHook.FIREARMS_EXTERNAL);
//		
//		return ret;
//	}
	
	//-------------------------------------------------------------------
	/**
	 * Check all usages of an accessory that apply to the container, 
	 * ignoring the capacity requirements.
	 * 
	 * @param container Item to add to
	 * @param accessory The accessory to add
	 * @return
	 */
	private List<UseAs> getEmbeddingOptions(CarriedItem container, ItemTemplate accessory) {
		List<UseAs> matching = new ArrayList<>();
		for (UseAs tmp : accessory.getUseAs()) {
			if (tmp.getType()==ItemType.ACCESSORY && container.hasHook(tmp.getSlot())) {
				// Item requires a slot that exists in container
				matching.add(tmp);
			}
		}
		return matching;
	}
	
	//-------------------------------------------------------------------
	/**
	 * When trying to add the accessory to a specific slot, what options
	 * (with respect to capacity) would be valid
	 * 
	 * @param container Item to add to
	 * @param accessory The accessory to add
	 * @return List of usages with different ratings (if rating applies)
	 */
	private List<UseAs> getValidEmbeddingOptions(CarriedItem container, ItemTemplate accessory, ItemHook hook) {
		List<UseAs> matching = new ArrayList<>();
		UseAs usage = accessory.getUsageFor(hook);
		if (usage==null) {
//			logger.warn("No valid usage option for accessory "+accessory.getId()+" in slot "+hook);
			return matching;
		}
		if (accessory.hasRating()) {
			// Accessory has ratings. Do they affect the capacity
			Availability avail = accessory.getAvailability();
			if (usage.getAlternateAvailability()!=null)
				avail = usage.getAlternateAvailability();
			int maxRat = Math.max(accessory.getMaximumRating(), usage.getMaxRating());
			// Some items have no maximum rating
			if (maxRat==0)
				maxRat = 24;
			int minRat = Math.max(1, accessory.getMinimumRating());
//			logger.info("    ratings of "+accessory+" from "+minRat+" to "+maxRat+"   acc="+accessory.getMaximumRating()+"  usage="+usage.getMaxRating());
			for (int rat=minRat; rat<=maxRat; rat++) {
//				logger.info("    rating "+rat);
				UseAs toAdd = new UseAs(usage);
				toAdd.setRating(rat);
				for (Multiply multi : accessory.getMultiplyWithRate()) {
					switch (multi) {
					case AVAIL:
						toAdd.setAlternateAvailability(new Availability(avail.getValue()*rat, avail.getLegality(), avail.isAddToAvailability()));
						break;
					case PRICE:
						toAdd.setExtraCost( (accessory.getPrice() + usage.getExtraCost()) * rat );
						break;
					case PRICE2:
						toAdd.setExtraCost( (accessory.getPrice() + usage.getExtraCost()) * rat * rat);
						break;
					case CAPACITY:
						toAdd.setCapacity(usage.getCapacity() * rat);
						break;
					case ESSENCE:
						toAdd.setEssence(usage.getEssence() * rat);
						break;
					case MODIFIER:
						// Not relevant here
						break;
					default:
						logger.warn("Don't know how to deal with multiplier "+multi+" for "+accessory+"   ..... "+accessory.getUseAs());
						System.err.println("Don't know how to deal with multiplier "+multi+" for "+accessory+"   ..... "+accessory.getUseAs());
					}
				}
				matching.add(toAdd);
			}
			
		} else {
			// No rating
			matching.add(usage);
		}
		
		return matching;
	}
	
	//-------------------------------------------------------------------
	/**
	 * When trying to add the accessory to a specific slot, what options
	 * (with respect to capacity) would be valid
	 * 
	 * @param container Item to add to
	 * @param accessory The accessory to add
	 * @return List of usages with different ratings (if rating applies)
	 */
	private List<UseAs> getValidNonEmbeddingOptions(ItemTemplate accessory) {
		List<UseAs> matching = new ArrayList<>();
		UseAs usage = accessory.getDefaultUsage();
		if (usage==null) {
			logger.error("No default usage for accessory: "+accessory);
		}
		if (accessory.hasRating()) {
			// Accessory has ratings. Do they affect the capacity
			Availability avail = accessory.getAvailability();
			if (usage.getAlternateAvailability()!=null)
				avail = usage.getAlternateAvailability();
			int min = Math.max(1, accessory.getMinimumRating());
			int max = Math.max(usage.getMaxRating(), accessory.getMinimumRating());
			if (max==0)
				max=12;
			for (int rat=min; rat<=max; rat++) {
				UseAs toAdd = new UseAs(usage);
				toAdd.setRating(rat);
				for (Multiply multi : accessory.getMultiplyWithRate()) {
					switch (multi) {
					case AVAIL:
						toAdd.setAlternateAvailability(new Availability(avail.getValue()*rat, avail.getLegality(), avail.isAddToAvailability()));
						break;
					case PRICE:
						toAdd.setExtraCost( (accessory.getPrice() + usage.getExtraCost()) * rat );
						break;
					case PRICE2:
						toAdd.setExtraCost( (accessory.getPrice() + usage.getExtraCost()) * rat * rat);
						break;
					case CAPACITY:
						toAdd.setCapacity(usage.getCapacity() * rat);
						break;
					case ESSENCE:
						toAdd.setEssence(usage.getEssence() * rat);
						break;
					default:
						logger.warn("Don't know how to deal with multiplier "+multi);
						System.err.println("Don't know how to deal with multiplier "+multi+" for "+accessory+"   ..... "+accessory.getUseAs());
					}
				}
				matching.add(toAdd);
			}
			
		} else {
			// No rating
			matching.add(usage);
		}
		
		return matching;
	}

	//-------------------------------------------------------------------
	@Override
	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed) {
		if (toEmbed==null)
			throw new NullPointerException("Item is null");
		
		// Ensure that all slot requirements are met
		for (Requirement req : toEmbed.getItem().getRequirements()) {
			if (req instanceof ItemHookRequirement) {
				ItemHookRequirement hookReq = (ItemHookRequirement)req;
				AvailableSlot avail = container.getSlot(hookReq.getSlot());
				if (avail==null) {
					// Required slot not available
					logger.warn("Cannot embed, because slot "+hookReq.getSlot()+" not existent in "+container);
					return false;
				} else {
					if (avail.getSlot().hasCapacity()) {
						// Is there enough capacity left
						if (hookReq.getCapacity()>avail.getFreeCapacity()) {
							logger.warn("Cannot embed, because slot is out of capacity: required="+hookReq.getCapacity()+"  free="+avail.getFreeCapacity());
							return false;
						}
					} else {
						switch (avail.getSlot()) {
						case BARREL:
						case TOP:
						case STOCK:
						case UNDER:
							// All weapon hooks must be empty
							if (!avail.getAllEmbeddedItems().isEmpty())
								return false;
							break;
						default:
						}
					}
				}
			} else
				logger.warn("Did not check requirement "+req);
		}
		return true;
	}

	//-------------------------------------------------------------------
	private UseAs getSuitableNonAccessoryUsage(ItemTemplate item, SelectionOption... options) {
		// Is the given hook, a valid one
		List<UseAs> validUsages = getValidNonEmbeddingOptions(item);
//		validUsages.add(item.getDefaultUsage());
		logger.debug("Valid; "+validUsages);
		if (validUsages.isEmpty()) {
			// No combination is possible
			return null;
		}
		
		int amount = 1;
		BodytechQuality quality;
		UseAs selected = null;
		for (SelectionOption opt : options) {
			switch (opt.getType()) {
			case AMOUNT: 
				amount = opt.getAsAmount(); 
				break;
			case BODYTECH_QUALITY: 
				quality = opt.getAsBodytechQuality();
				break;
			case RATING:
				// Is there a valid rating matching the selected rating?
				// Only perform for items with a rating
				if (item.hasRating()) {
					logger.debug("Compare rating "+opt.getAsRating()+" with valid: "+validUsages);					
					if (opt.getAsRating()>=item.getMinimumRating() && opt.getAsRating()<=item.getMaximumRating()) {
						selected = item.getDefaultUsage();
					}
					if (selected==null) {
					for (UseAs usage : item.getUseAs()) {
						if (usage.getMaxRating()>0) {
							if (opt.getAsRating()>=item.getMinimumRating() && opt.getAsRating()<=usage.getMaxRating()) {
								selected = item.getDefaultUsage();
								logger.debug("Rating is valid for usage: "+usage);
								break;
							}
						}
					}
					}
					
					if (selected==null) {
						// No matching rating found
						throw new IllegalArgumentException("Valid ratings are: "+validUsages.stream().map( u -> u.getRating() ).collect(Collectors.toList()));
					}
				} else {
					logger.warn("Selected rating option, but item "+item.getId()+" does not have ratings");
				}
				break;
			case SKILL:
			case PHYSICAL_SKILL:
				break;
			default:
				break;
			}
		}
		
		// If a selection hasn't been made yet, select the first from the valid ones
		if (selected==null) {
			selected = validUsages.get(0);
		}
		
		return selected;
	}

	//-------------------------------------------------------------------
	private UseAs getSuitableAccessoryUsage(CarriedItem container, ItemTemplate toEmbed, ItemHook slot, SelectionOption... options) {
		// Is the given hook, a valid one
		List<UseAs> validUsages = getValidEmbeddingOptions(container, toEmbed, slot);
		if (validUsages.isEmpty()) {
			// No combination is possible
			return null;
		}
		
		int amount = 1;
		BodytechQuality quality;
		UseAs selected = null;
		for (SelectionOption opt : options) {
			switch (opt.getType()) {
			case AMOUNT: 
				amount = opt.getAsAmount(); 
				break;
			case BODYTECH_QUALITY: 
				quality = opt.getAsBodytechQuality();
				break;
			case RATING:
				// Is there a valid rating matching the selected rating?
				// Only perform for items with a rating
				if (toEmbed.hasRating()) {
					for (UseAs tmp : validUsages) {
						if (tmp.getRating() == opt.getAsRating()) {
							selected = tmp;
							break;
						}
					}
					if (selected==null) {
						// No matching rating found
						return null;
					}
				} else {
					logger.warn("Selected rating option, but item "+toEmbed.getId()+" does not have ratings");
				}
				break;
			case SKILL:
			case PHYSICAL_SKILL:
				break;
			default:
				break;
			}
		}
		
		// If a selection hasn't been made yet, select the first from the valid ones
		if (selected==null) {
			selected = validUsages.get(0);
		}
		
		return selected;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canBeEmbedded(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate, org.prelle.shadowrun6.items.AccessorySlot)
	 */
	@Override
	public boolean canBeEmbedded(CarriedItem container, ItemTemplate toEmbed, ItemHook slot, SelectionOption... options) {
		AvailableSlot avail = container.getSlot(slot);
//		logger.info("canBeEmbedded(..."+toEmbed.getId()+","+slot+")  avail="+avail);
		if (avail==null) {
			// Required slot not available
			if (logger.isDebugEnabled())
				logger.debug("Cannot embed "+toEmbed+" because slot "+slot+" not available in "+container);
			return false;
		}
		
		UseAs selected = getSuitableAccessoryUsage(container, toEmbed, slot, options);
		if (selected==null) {
			if (logger.isDebugEnabled())
				logger.debug("Cannot embed "+toEmbed+" because no suitable UseAs found in "+container);
			return false;
		}
		
		// For slots with capacity
		if (slot.hasCapacity()) {
			int neededCap = selected.getCapacity();
			if (slot.hasCapacity() && avail.getFreeCapacity()<neededCap) {
				if (logger.isDebugEnabled()) {
					logger.debug("Cannot embed - no free capacity  ("+avail.getFreeCapacity()+" < "+neededCap+") in slot "+slot+" with capacity "+avail.getCapacity());
					for (CarriedItem item : avail.getAllEmbeddedItems()) {
						logger.debug(item.getAsValue(ItemAttribute.CAPACITY).getModifiedValue()+" - "+item.getItem().getId());
					}
				}
				return false;
			}
		} else {
			if (avail.getAllEmbeddedItems().size()>0) {
				switch (avail.getSlot()) {
				case BARREL:
				case TOP:
				case UNDER:
				case STOCK:
					logger.warn("Slot not empty");
					return false;
				default:
				}
			}
		}
		
		// TODO: do we wan't to check for availability and sufficient Nuyen here?
		
		return true;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canBeEmbedded(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate, org.prelle.shadowrun6.items.AccessorySlot)
//	 */
//	@Override
//	public boolean canBeEmbedded(CarriedItem container, CarriedItem toEmbed, ItemHook slot) {
//		AvailableSlot avail = container.getSlot(slot);
//		if (avail==null) {
//			// Required slot not available
//			return false;
//		}
//		
//		// Is the given hook, a valid one
//		List<ItemHook> validSlots = getPossibleEmbedHooks(toEmbed.getItem());
//		if (!validSlots.contains(slot))
//			return false;
//		
//
//		int neededCap = toEmbed.getCapacity(slot);
//		logger.debug("Needed capacity for "+toEmbed.getName()+" is "+neededCap);
//		if (slot.hasCapacity() && avail.getFreeCapacity()<neededCap) {
//			logger.debug("Cannot embed - no free capacity  ("+avail.getFreeCapacity()+" < "+neededCap+")");
//			return false;
//		}
////			cap = avail.getCapacity();
////		if (embeddedItems.size()>cap)
////			throw new IllegalStateException("Cannot add any more items. Already have "+embeddedItems.size());
//
//		return true;
//	}

	//--------------------------------------------------------------------
	private boolean isRequirementMet(CarriedItem ref, ItemHook slot, AvailableSlot hook, Requirement req, AccessoryData access) {
		if (access==null) {
			logger.warn("Missing AccessoryData when checking requirement "+req+" in "+ref);
			return false;
		}
		if (req instanceof ItemHookRequirement) {
			ItemHookRequirement iReq = (ItemHookRequirement)req;
//			logger.debug("  - slot="+slot+"   iReq.getSlot="+iReq.getSlot());
			if (iReq.getSlot()!=slot) {
				return false;
			}
			if (slot.hasCapacity()) {
				int free = hook.getFreeCapacity();
				if (free<access.getCapacitySize()) {
					// Too big
					return false;
				}
			}
		} else if (req instanceof ItemTypeRequirement) {
			ItemTypeRequirement tReq = (ItemTypeRequirement)req;
			if (ref.getUsedAsType()!=tReq.getType()) {
				// Wrong item type
				logger.debug("  wrong item type - exp. "+tReq.getType());
				return false;
			}
		} else if (req instanceof ItemSubTypeRequirement) {
			ItemSubTypeRequirement tReq = (ItemSubTypeRequirement)req;
			boolean found = false;
			for (ItemSubType tmp : tReq.getType()) {
				if (ref.getUsedAsSubType()==tmp) {
					found = true;
					break;
				}
			}
			if (!found) {
				// Wrong item subtype
//				logger.debug("  wrong item subtype - exp. "+tReq.getType()+" but item is "+ref.getItem().getSubtype());
				return false;
			}
		} else if (req instanceof ItemRequirement) {
			ItemRequirement iReq = (ItemRequirement)req;
			if (!iReq.getItemID().equals(ref.getItem().getId())) {
//				logger.debug("  wrong item - only usable with "+iReq.getItemID());
				return false;
			}
		} else {
			logger.warn("Unprocessed requirement type: "+req);
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getEmbeddableIn(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public List<ItemTemplate> getEmbeddableIn(CarriedItem ref, ItemHook slot) {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();

		AvailableSlot hook = ref.getSlot(slot);
		// Ensure item has that slot available
		if (hook==null) {
			logger.warn("Embeddable items for non-existing hook "+slot+" have been requested for "+ref);
			return ret;
		}

		itemloop:
			for (ItemTemplate item : ShadowrunCore.getItems(ItemType.ACCESSORY)) {
				logger.trace("Check for embedding "+item);
//				Accessory access = (Accessory)item;
//				if (item.getId().contains("silencer")) {
//					logger.debug("Foo");
//				}
				AccessoryData access = item.getAccessoryData();
				boolean slotMatches = false;
				for (Requirement req : item.getRequirements()) {
					if (req instanceof ItemHookRequirement) {
						ItemHookRequirement iReq = (ItemHookRequirement)req;
						if (iReq.getSlot()==slot) {
							slotMatches = true;
							if (iReq.getCapacity()>hook.getFreeCapacity()) {
								// Not enough capacity left
								continue itemloop;
							}
//						} else if (slot.hasCapacity()) {
//							int free = hook.getFreeCapacity();
//							logger.debug("for item "+item+" the accessory is "+access);
//							if (access==null) {
//								// Not an item with capacity
//								continue itemloop;
//							}
//							if (free<access.getCapacitySize()) {
//								// Too big
//								continue itemloop;
//							}
						}
					} else if (req instanceof AnyRequirement) {
						AnyRequirement aReq = (AnyRequirement)req;
						logger.info("check AnyRequirement for "+access);
//						boolean anyMisMatches = false;
//						for (Requirement tmpReq : aReq.getOptionList()) {
//							if (!isRequirementMet(ref, slot, hook, tmpReq, access))
//								anyMisMatches = true;
//						}
//						if (anyMisMatches)
//							continue itemloop;
						boolean anyMatch = false;
						for (Requirement tmpReq : aReq.getOptionList()) {
							logger.debug("* check "+ref+" and slot "+slot+" for hook "+hook+" and "+tmpReq+" = "+isRequirementMet(ref, slot, hook, tmpReq, access));
							if (isRequirementMet(ref, slot, hook, tmpReq, access))
								anyMatch = true;
						}
						if (!anyMatch)
							continue itemloop;
						slotMatches = true;
					} else {
						if (!isRequirementMet(ref, slot, hook, req, access))
							continue itemloop;
//						logger.debug("  requirement "+req+" is met");
					}
				}

				if (!slotMatches) {
//					logger.debug("  Request was for slot "+slot+", but item does not provide that");
					// Wrong slot
					continue itemloop;
				}
				logger.debug("--- Add "+item+" ---");
				ret.add(item);
			}

		// Remove all those already inserted in that slot
		for (CarriedItem already : hook.getAllEmbeddedItems()) {
			if (ret.contains(already.getItem())) {
				ret.remove(already.getItem());
//				logger.debug("  Remove option "+already.getItem()+" since it already exists in slot");
			}
		}


		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getEmbeddedIn(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public List<CarriedItem> getEmbeddedIn(CarriedItem ref) {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * Find the best possible slot
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#embed(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, SelectionOption...options) {
		if (item==null)
			throw new NullPointerException("Item is null");
		if (container==null)
			throw new NullPointerException("Container to embed in is null");
		logger.debug("START-------embed "+item.getId()+" into "+container+"-------------");
		
		/*
		 * Make a list of valid hooks
		 */
		List<ItemHook> validHooks = new ArrayList<>();
		for (UseAs usage : item.getUseAs()) {
			if (usage==null) {
				logger.error("Missing UseAs for item "+item.getId());
				continue;
			}
			if (usage.getType()==ItemType.ACCESSORY && container.hasHook(usage.getSlot())) {
				validHooks.add(usage.getSlot());
			}
		}
		if (validHooks.size()==0) {
			logger.warn("Cannot embed - no valid hook found");
			return null;
		}
		if (validHooks.size()>1) {
			logger.warn("Cannot embed - multiple valid hooks found - use embed() with explicit hook");
			return null;
		}
		
		return embed(container, item, validHooks.get(0), options);
		
//		/*
//		 * If anything is embedded into cyber- or bioware, the accessory
//		 * must have the same quality as the container
//		 */
//		if (container.getUsedAsType()==ItemType.CYBERWARE || container.getUsedAsType()==ItemType.BIOWARE) {
//			// Check of option is present and eventually modify it
//			boolean addQuality = true;
//			for (SelectionOption opt : options) {
//				if (opt.getType()==SelectionOptionType.BODYTECH_QUALITY) {
//					addQuality = false;
//					if (opt.getAsBodytechQuality()!=container.getQuality()) {
//						logger.warn("Change quality of embedded "+item.getId()+" to container: "+container.getQuality());
//						opt.setValue(container.getQuality());
//					}
//				}
//			}
//			if (addQuality) {
//				SelectionOption[] opt2 = new SelectionOption[options.length+1];
//				System.arraycopy(options, 0, opt2, 0, options.length);
//				opt2[opt2.length-1] = new SelectionOption(SelectionOptionType.BODYTECH_QUALITY, container.getQuality());
//				logger.warn("Add quality of embedded "+item.getId()+" to container: "+container.getQuality());
//				options = opt2;
//			}
//		}
//		
//		if (!canBeEmbedded(container, item, options)) {
//			logger.warn("Cannot embed "+item+" to "+container);
//			return null;
//		}
//
//		CarriedItem accessory = buildItem(item, options);
//		if (accessory==null) {
//			logger.warn("Could not build accessory");
//			return null;
//		}
//		logger.debug("START-------embed "+item.getId()+" into "+container+"-------------");
//		logger.debug("  Worth before: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//
//		/*
//		 * Build a list of possible slots
//		 */
//		List<UseAs> matching = new ArrayList<>();
//		for (UseAs tmp : item.getUseAs()) {
//			if (tmp.getType()==ItemType.ACCESSORY && container.hasHook(tmp.getSlot())) {
//				// Item requires a slot that exists in container
//				matching.add(tmp);
//			}
//		}
//		
//		/*
//		 * Verify that
//		 */
//		
//		List<AvailableSlot> embedIn = new ArrayList<>();
//		// Ensure that all slot requirements are met
//		for (Requirement req : item.getRequirements()) {
//			if (req instanceof ItemHookRequirement) {
//				ItemHookRequirement hookReq = (ItemHookRequirement)req;
//				AvailableSlot avail = container.getSlot(hookReq.getSlot());
//				if (avail==null) {
//					logger.error("  Container "+container+" hasn't the necessary slot "+hookReq.getSlot());
//					return null;
//				}
//				embedIn.add(avail);
//			}
//		}
//
//		logger.debug("  Accessory is worth "+accessory.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//
//		if (embedIn.isEmpty()) {
//			logger.debug("No need for a specific slot for accessory "+item.getId()+" in "+container);
//			accessory.setSlot(ItemHook.FIREARMS_EXTERNAL);
////			logger.warn("No slot to embed "+item.getId()+" into "+container+" found");
////			return null;
//			container.addAccessory(ItemHook.FIREARMS_EXTERNAL, accessory);
//		} else {
//			for (AvailableSlot slot : embedIn) {
//				logger.info("Embed "+accessory+" in "+slot.getSlot());
//				container.addAccessory(slot.getSlot(), accessory);
//			}
//		}
//
//		logger.debug("  Worth afterwards: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());
//		logger.debug("STOP -------embed "+item.getId()+" into "+container+"-------------");
//		return accessory;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#embed(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, ItemHook hook, SelectionOption...options) {
		if (item==null)
			throw new NullPointerException("Item is null");
		logger.debug("START-------embed "+item.getId()+" into "+container+"-------------");
		/*
		 * If anything is embedded into cyber- or bioware, the accessory
		 * must have the same quality as the container
		 */
		if (container.isType(ItemType.CYBERWARE) || container.isType(ItemType.BIOWARE)) {
			// Check of option is present and eventually modify it
			boolean addQuality = true;
			for (SelectionOption opt : options) {
				if (opt.getType()==SelectionOptionType.BODYTECH_QUALITY) {
					addQuality = false;
					if (opt.getAsBodytechQuality()!=container.getQuality()) {
						logger.warn("Change quality of embedded "+item.getId()+" to container: "+container.getQuality());
						opt.setValue(container.getQuality());
					}
				}
			}
			if (addQuality) {
				SelectionOption[] opt2 = new SelectionOption[options.length+1];
				System.arraycopy(options, 0, opt2, 0, options.length);
				opt2[opt2.length-1] = new SelectionOption(SelectionOptionType.BODYTECH_QUALITY, container.getQuality());
				logger.warn("Add quality of embedded "+item.getId()+" to container: "+container.getQuality());
				options = opt2;
			}
		}

		if (!canBeEmbedded(container, item, hook, options)) {
			logger.warn("Cannot embed "+item+" to "+container);
			return null;
		}
		
		/*
		 * Embedding is allowed
		 */
		logger.debug("Embedding allowed");
		UseAs usage = getSuitableAccessoryUsage(container, item, hook, options);
		if (usage==null) {
			throw new NullPointerException("No accessory usage for item "+item.getId()+" for hook "+hook+" in container "+container.getNameWithRating());
		}
		CarriedItem accessory = buildItem(item, usage, options);
		accessory.setUsedAsType(usage.getType());
		accessory.setUsedAsSubType(usage.getSubtype());
		accessory.setSlot(hook);


		logger.debug("  Worth before: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());

		logger.info("Add accessory "+accessory+"  in "+container.getName()+" in slot "+hook);
		container.addAccessory(hook, accessory);
		model.addItem(accessory);
		
//		/*
//		 * If item is to be embedded in an auto-item, 
//		 */
//		if (model.isAutoItem(container)) {
//			logger.debug("Embed into auto item");
//			accessory.setEmbeddedIn(container.getUniqueId());
//			model.addItem(accessory);
//		}

		logger.debug("  Worth afterwards: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		logger.debug("STOP -------embed "+item.getId()+" into "+container+"-------------");
		return accessory;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#remove(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean remove(CarriedItem container, CarriedItem accessory) {
		if (accessory==null)
			throw new NullPointerException("Item is null");
		logger.debug("START-------remove "+accessory.getName()+" from "+container+"-------------");
		logger.debug("  Worth before: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());

		logger.info("Remove accessory "+accessory+"  in "+container.getName());
		container.removeAccessory(accessory);
		model.removeItem(accessory);
		
		// If the container was an auto-added item that accessory must be removed from character too
		if (model.isAutoItem(container)) {
			model.removeAutoItemAccessories(accessory);
		}

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, container));
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, container));
		logger.debug("  Worth afterwards: "+container.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		logger.debug("STOP -------embed "+accessory.getItem().getName()+" into "+container+"-------------");
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getAvailableAccessoriesFor(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public List<ItemTemplate> getAvailableAccessoriesFor(CarriedItem ref) {
		logger.warn("START: getAvailableAccessoriesFor: "+ref);
		List<ItemTemplate> ret = new ArrayList<>();
		for (AvailableSlot slot : ref.getSlots()) {
			for (ItemTemplate access : ShadowrunCore.getAccessoryItems(slot.getSlot(), ref.getItem())) {
				// Already as an accessory - in any slot
				boolean notAddedYet = true;
				for (AvailableSlot slot2 : ref.getSlots()) {
					if (!slot2.getAllEmbeddedItems().stream().filter(inst -> inst.getItem()==access).collect(Collectors.toList()).isEmpty())
						notAddedYet = false;
				}
				if (!ret.contains(access) && notAddedYet) {
					// Could be added. Make a capacity check (for an considered empty slot)
//					UseAs usage = access.getUsageFor(slot.getSlot());
					UseAs usage = getSuitableAccessoryUsage(ref, access, slot.getSlot());
					if (usage==null) {
						logger.warn("No usage for item "+access.getId()+" in slot "+slot);
						continue;
					}
					// If slot is INTERNAL this means auto-added - ignore it
					if (usage.getSlot()==ItemHook.INTERNAL) {
						logger.debug("Ignore accessory item "+access.getId()+" because slot is INTERNAL");
						continue;
					}

					if (slot.getCapacity()<usage.getCapacity()) {
						if (logger.isTraceEnabled())
							logger.trace("Cannot embed "+access+" into too smal slot "+usage);
						continue;
					}

					// Don't show accessories that are already added and should not be added multiple times
					boolean alreadyAdded = slot.getAllEmbeddedItems().stream().anyMatch( carried -> carried.getItem()==access);
					if (alreadyAdded && !access.isMultipleUsably()) {
						logger.warn("Usually it should not be possible to add this item more than once");
						System.err.println("Usually it should not be possible to add this item more than once");
//						if (logger.isTraceEnabled())
//							logger.trace("Cannot embed "+access+", because it already added");
//						continue;
					}
					
					ret.add(access);
				}
			}
		}
		Collections.sort(ret);
		
		logger.warn("STOP : getAvailableAccessoriesFor returns "+ret.size()+" elements");
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getAvailableEnhancementsFor(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public List<ItemEnhancement> getAvailableEnhancementsFor(CarriedItem ref) {
		List<ItemEnhancement> ret = new ArrayList<>();
		for (ItemEnhancement enh : ShadowrunCore.getItemEnhancements(ref.getUsedAsType(), ref.getUsedAsSubType()))
			ret.add(enh);
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getEnhancementsIn(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public List<ItemEnhancement> getEnhancementsIn(CarriedItem ref) {
		// TODO Auto-generated method stub
		logger.warn("TODO: getAvailableEnhancementsFor");
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#modify(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemEnhancement)
	 */
	@Override
	public boolean modify(CarriedItem container, ItemEnhancement mod) {
		// TODO Auto-generated method stub
		logger.warn("TODO: modify");
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#remove(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemEnhancement)
	 */
	@Override
	public boolean remove(CarriedItem container, ItemEnhancement mod) {
		// TODO Auto-generated method stub
		logger.warn("TODO: remove");
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#increase(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean increase(CarriedItem data) {
		if (!canChangeCount(data, data.getCount()+1))
			return false;
		
		data.setCount(data.getCount()+1);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, data));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#decrease(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean decrease(CarriedItem data) {
		if (!canChangeCount(data, data.getCount()-1))
			return false;
		
		if (data.getCount()<=1)
			return false;
		data.setCount(data.getCount()-1);

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, data));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getCountChangeCost(org.prelle.shadowrun6.items.CarriedItem, int)
	 */
	@Override
	public int getCountChangeCost(CarriedItem data, int newCount) {
		int totalCost  = data.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		int singleCost = totalCost / data.getCount();
		int newTotal   = singleCost * newCount;
		return newTotal - totalCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canChangeCount(org.prelle.shadowrun6.items.CarriedItem, int)
	 */
	@Override
	public boolean canChangeCount(CarriedItem data, int newCount) {
		if (newCount<1) return false;
		if (newCount>100) return false;
		
		Boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear!=null && !payGear)
			return true;
		
		int newCost = getCountChangeCost(data, newCount);
		return (newCost<0 || (newCost<=model.getNuyen()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#markUpdated(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public void markUpdated(CarriedItem data) {
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, data));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY, data));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canBuyLevel(org.prelle.shadowrun6.items.CarriedItem, int)
	 */
	@Override
	public boolean canBuyLevel(CarriedItem item, int newLevel) {
		CarriedItem testItem = new CarriedItem(item.getItem(), newLevel);
		int newCost = testItem.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		Availability newAvail= testItem.getAvailability();
		
		if (newAvail.getValue()>12)
			return false;
		
		int diff = newCost - testItem.getAsValue(ItemAttribute.PRICE).getModifiedValue();
		if (diff<=0)
			return true;
		
		return diff <= model.getNuyen();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canBuyQuality(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.BodytechQuality)
	 */
	@Override
	public boolean canBuyQuality(CarriedItem item, BodytechQuality newQuality) {
		// TODO Auto-generated method stub
		logger.error("TODO: canBuyQuality("+item+", "+newQuality);
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#changeQuality(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.BodytechQuality)
	 */
	@Override
	public boolean changeQuality(CarriedItem item, BodytechQuality newQuality) {
		if (!canBuyQuality(item, newQuality))
			return false;
		logger.debug("Change bodytech quality of "+item+" to "+newQuality);
		
		item.setQuality(newQuality);
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getOptions(org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public List<SelectionOptionType> getOptions(ItemTemplate item, UseAs usage) {
		List<SelectionOptionType> list = new ArrayList<>();
		
		ItemType type = usage.getType();
		
		if (item.hasRating())
			list.add(SelectionOptionType.RATING);
		
		if (type==ItemType.CYBERWARE || type==ItemType.BIOWARE)
			list.add(SelectionOptionType.BODYTECH_QUALITY);
		
		if (item.getChoice()!=null) {
			switch (item.getChoice()) {
			case SKILL:
				list.add(SelectionOptionType.SKILL);
				break;
			case PHYSICAL_SKILL:
				list.add(SelectionOptionType.PHYSICAL_SKILL);
				break;
			case AMMUNITION_TYPE:
				list.add(SelectionOptionType.AMMOTYPE);
				break;
			case NAME:
				list.add(SelectionOptionType.NAME);
				break;
			default:
				logger.warn("TODO: Implement choice for "+item.getChoice());
			}
		}
		
		return list;
	}

}

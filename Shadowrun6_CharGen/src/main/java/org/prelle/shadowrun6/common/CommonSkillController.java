package org.prelle.shadowrun6.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.SkillController;

import de.rpgframework.genericrpg.ToDoElement;

public abstract class CommonSkillController implements SkillController {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen.skill");
	protected ShadowrunCharacter model;
	protected ProcessorRunner parent;
	protected List<Skill> recSkills;
	protected List<Skill> avSkills;
	protected List<Skill> allowedSkills;
	protected List<ToDoElement> todos;
	protected boolean ignoreRequirements;
	protected List<Skill> unrestricted;
	protected List<Skill> restricted;

	//--------------------------------------------------------------------
	public CommonSkillController(ProcessorRunner parent) {
		this.parent = parent;
		allowedSkills  = new ArrayList<>();
		avSkills   = new ArrayList<>();
		recSkills  = new ArrayList<>();
		todos      = new ArrayList<>();
		unrestricted   = new ArrayList<>();
		restricted   = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getAllowedSkills()
	 */
	@Override
	public List<Skill> getAllowedSkills() {
		return allowedSkills;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getAvailableSkills()
	 */
	@Override
	public List<Skill> getAvailableSkills() {
		return avSkills;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getAvailableSkills()
	 */
	@Override
	public List<Skill> getAvailableSkills(SkillType... types) {
		List<SkillType> valid = Arrays.asList(types);
		return avSkills.stream().filter(p -> valid.contains(p.getType()) ).collect(Collectors.toList());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#select(org.prelle.shadowrun6.Skill)
	 */
	@Override
	public SkillValue select(Skill skill) {
		logger.debug("select "+skill);
		if (skill==null)
			throw new NullPointerException("Parameter is null");
		
		if (!canBeSelected(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}
		
		// Change model
		logger.info("Selected skill "+skill);
		SkillValue sVal = model.getSkillValue(skill);
		if (sVal==null || skill.getId().equals("knowledge") || skill.getId().equals("language")) {
			sVal = new SkillValue(skill, 1);
			model.addSkill(sVal);
		} else
			sVal.setPoints(1);
	
		// Fire event
		parent.runProcessors();
		
		return sVal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#increase(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean increase(SkillValue ref) {
		logger.debug("increase "+ref);
		if (!canBeIncreased(ref)) {
			logger.warn("Trying to increase a skill which cannot be increased: "+ref);
			return false;
		}
	
		// Change model
		ref.setPoints(ref.getPoints()+1);
		logger.info("Increase skill "+ref.getModifyable().getId()+" to "+ref.getPoints());
		
//		// Inform listeners
//		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#decrease(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean decrease(SkillValue ref) {
		logger.debug("decrease "+ref);
		if (!canBeDecreased(ref))
			return false;
	
		// Change model
		ref.setPoints(ref.getPoints()-1);
		if (ref.getModifiedValue()==0)
			model.removeSkill(ref);
		
//		// Inform listeners
//		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	protected boolean[] hasSpecialAndExpertise(SkillValue ref) {
		boolean hasSpecialization =false;
		boolean hasExpertise = false;
		for (SkillSpecializationValue sVal : ref.getSkillSpecializations()) {
			if (sVal.isExpertise())
				hasExpertise=true;
			else
				hasSpecialization=true;
		}
		return new boolean[] {hasSpecialization, hasExpertise};
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#select(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		logger.debug("select "+ref+"/"+spec);
		if (!canBeSelected(ref, spec, expertise))
			return null;
		
		SkillSpecializationValue ret = null;
		if (!expertise) {
			logger.info("Select specialization "+spec+" of "+ref);
			ret = new SkillSpecializationValue(spec);
			ref.getSkillSpecializations().add(ret);
		} else {
			logger.info("Select expertise "+spec+" of "+ref);
			ret = ref.getSpecialization(spec);
			ret.setExpertise(true);
		}
	
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean deselect(SkillValue ref) {
		logger.info("deselect "+ref);
		if (!canBeDeselected(ref))
			return false;
	
		// Change model
		ref.setPoints(0);
		model.removeSkill(ref);
		
		// Inform listeners
//		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public boolean deselect(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		if (!canBeDeselected(ref, spec, expertise)) {
			logger.warn("Specialization "+spec+" cannot be deselected");
			return false;
		}

		SkillSpecializationValue val = ref.getSpecialization(spec);
		if (val==null) {
			logger.error("Could not find specialization "+spec+" in "+ref.getSkillSpecializations());
			return false;
		}

		if (expertise) {
			logger.info("Deselect expertise "+spec+" from "+ref);
			val.setExpertise(false);
		} else {
			logger.info("Deselect specialization "+spec+" from "+ref);
			ref.removeSpecialization(val);
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#isRecommended(org.prelle.shadowrun6.Skill)
	 */
	@Override
	public boolean isRecommended(Skill val) {
		return recSkills.contains(val);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#setIgnoreRequirements(boolean)
	 */
	@Override
	public void setIgnoreRequirements(boolean value) {
		logger.info("set 'ignoreRequirements' to "+value);
		ignoreRequirements = value;
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#isIgnoreRequirements()
	 */
	@Override
	public boolean isIgnoreRequirements() {
		return ignoreRequirements;
	}

}
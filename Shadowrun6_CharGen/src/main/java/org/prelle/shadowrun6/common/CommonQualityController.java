/**
 * 
 */
package org.prelle.shadowrun6.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ElementType;
import org.prelle.shadowrun6.MentorSpirit;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.Quality.QualityType;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Spirit;
import org.prelle.shadowrun6.Sprite;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.proc.CharacterProcessor;
import org.prelle.shadowrun6.requirements.Requirement;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.DecisionToMake;

/**
 * @author prelle
 *
 */
public abstract class CommonQualityController implements QualityController, CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.gen");

	protected final static PropertyResourceBundle RES = (PropertyResourceBundle) ShadowrunCharGenConstants.RES;

	protected CharGenMode mode;
	protected ProcessorRunner parent;
	protected ShadowrunCharacter model;

	protected List<Quality> available;
	protected List<ToDoElement> todos;
	protected List<DecisionToMake> decisions;
	
	//-------------------------------------------------------------------
	public CommonQualityController(CharacterController parent, CharGenMode mode) {
		this.parent= parent;
		this.model = parent.getCharacter();
		this.mode  = mode;
		available = new ArrayList<>();
		todos     = new ArrayList<>();
		decisions = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#getAvailableQualities()
	 */
	@Override
	public List<Quality> getAvailableQualities() {
		return available;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#canBeSelected(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public boolean canBeSelected(Quality data) {
		if (data==null)
			throw new NullPointerException("Quality not set");
		if (!data.isFreeSelectable())
			throw new NullPointerException("Quality only selectable by metatype");
		if (data.isMetagenetic())
			throw new NullPointerException("Quality only selectable for changelings");
		// Not already selected
		if (model.getQualities().stream().anyMatch(qval -> (qval.getModifyable()==data && !data.isMultipleSelectable()))) {
			return false;
		}
		
		// Check requirements
		for (Requirement req : data.getPrerequisites()) {
			if (!ShadowrunTools.isRequirementMet(req, model)) {
				return false;
			}
		}
		
		if (data.getType()==QualityType.POSITIVE)
			return model.getKarmaFree()>=(getKarmaCost(data));
		else
			return true;
	}

	//-------------------------------------------------------------------
	protected abstract int getKarmaCost(Quality data);

	//-------------------------------------------------------------------
	protected abstract int getKarmaCost(QualityValue data);
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#select(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public QualityValue select(Quality data) {
		logger.debug("select "+data);
		if (data.needsChoice())
			throw new IllegalArgumentException("Quality "+data+" needs a choice");
		
		if (!canBeSelected(data)) {
			logger.warn("Trying to select quality "+data+" which cannot be selected");
			return null;
		}
		
		// Change model
		logger.info("Selected quality "+data+" for model "+model.getName()+" and parent "+parent);
		QualityValue sVal = new QualityValue(data, 1);
		sVal.recalculateModifications();
		model.addQuality(sVal);

		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#select(org.prelle.shadowrun6.Quality, java.lang.Object)
	 */
	@Override
	public QualityValue select(Quality data, Object choice) {
		logger.debug("select "+data+" with choice "+choice+" for "+model.getName());
		if (choice==null)
			throw new NullPointerException("Choice is null");
		if (!data.needsChoice())
			throw new IllegalArgumentException("Quality "+data+" does not need a choice");
		
		if (!canBeSelected(data)) {
			logger.warn("Trying to select quality "+data+" which cannot be selected");
			return null;
		}

		// Change model
		QualityValue sVal = new QualityValue(data, 1);
		model.addQuality(sVal);
		sVal.setChoice(choice);
		switch (data.getSelect()) {
		case ATTRIBUTE:
		case PHYSICAL_ATTRIBUTE:
			sVal.setChoiceReference(  ((Attribute)choice).name() );
			break;
		case COMBAT_SKILL:
		case SKILL:
			sVal.setChoiceReference(  ((Skill)choice).getId() );
			break;
		case MATRIX_ACTION:
			sVal.setChoiceReference(  ((ShadowrunAction)choice).getId() );
			break;
		case MENTOR_SPIRIT:
			sVal.setChoiceReference(  ((MentorSpirit)choice).getId() );
			sVal.setChoice( (MentorSpirit)choice );
			break;
		case SPIRIT:
			if (choice instanceof Spirit) {
				sVal.setChoiceReference(  ((Spirit)choice).getId() );
			} else if (choice instanceof String) {
				Spirit spirit = ShadowrunCore.getSpirit(choice.toString());
				if (spirit==null) {
					logger.error("Can't find a spirit or sprite with ID '"+choice+"'");
					throw new IllegalArgumentException("Can't find a spirit with ID '"+choice+"'");
				} else {
					sVal.setChoice(spirit);
				}
				sVal.setChoiceReference(choice.toString());
			}
			break;
		case SPRITE:
			if (choice instanceof Sprite) {
				sVal.setChoiceReference(  ((Sprite)choice).getId() );
			} else if (choice instanceof String) {
				Sprite sprite = ShadowrunCore.getSprite(choice.toString());
				if (sprite==null) {
					logger.error("Can't find a spirit or sprite with ID '"+choice+"'");
					throw new IllegalArgumentException("Can't find a spirit or sprite with ID '"+choice+"'");
				} else
					sVal.setChoice(sprite);
			}
			sVal.setChoiceReference(choice.toString());
			break;
		case ELEMENTAL:
			if (choice instanceof ElementType) {
				sVal.setChoiceReference( ((ElementType)choice).name() );
			} else if (choice instanceof String) {
				ElementType element = ElementType.valueOf((String)choice);
				if (element!=null) {
					logger.error("Can't find an element with ID '"+choice+"'");
					throw new IllegalArgumentException("Can't find an element with ID '"+choice+"'");
				} else {
					sVal.setChoice(element);
				}
				sVal.setChoiceReference(choice.toString());
			}
			break;	
		case NAME:
			sVal.setChoiceReference((String)choice);
			sVal.setDescription((String)choice);
			break;
		default:
			logger.error("Don't know how to set reference for choice type "+data.getSelect()+" and choice '"+choice+"'");
			throw new IllegalArgumentException("Don't know how to set reference for choice type "+data.getSelect()+" and choice '"+choice+"'");
		}
		
		/*
		 * When an quality needs a choice to be made, all modifications attached 
		 * to the quality needs to be scanned to be modified accordingly
		 */
		sVal.recalculateModifications();
		logger.info("Selected quality "+data+" with modifications "+sVal.getModifications()+" for "+model.getName());
		
		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#canBeDeselected(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean canBeDeselected(QualityValue ref) {
		return model.getUserSelectedQualities().contains(ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#deselect(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean deselect(QualityValue ref) {
		logger.info("deselect "+ref);
		if (!canBeDeselected(ref))
			return false;

		// Change model
		model.removeQuality(ref);
		
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#canBeIncreased(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean canBeIncreased(QualityValue ref) {
		// Quality must exist in character
		if (!model.hasQuality(ref.getModifyable().getId()))
			return false;
		Quality data = ref.getModifyable();
		if (data.getMax()<2) {
			return false;
		}
		// Must not be at limit
		if (ref.getPoints()>=ref.getModifyable().getMax())
			return false;

		// Special limit check for "Impaired"
		if (data.getId().equals("impaired")) {
			Attribute attr = (Attribute) ref.getChoice();
			if (attr!=null && model.getAttribute(attr).getMaximum()==2) {
				return false;
			}
		}
		
		
		if (data.getType()==QualityType.POSITIVE) {
			// Enough points
			if (model.getKarmaFree()<getKarmaCost(data)) {
				return false;
			}
//		} else {
//			if (mode==CharGenMode.CREATING) {
//				// May not exceed 25 points from negative qualities
//				if ( (getPointsInNegativeQualities()+getKarmaCost(data))>25)
//					return false;
//			}
		}
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#canBeDecreased(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean canBeDecreased(QualityValue ref) {
		// Quality must exist in character
		if (!model.hasQuality(ref.getModifyable().getId()))
			return false;
		// Must have levels
		if (ref.getModifyable().getMax()==0)
			return false;
		// Must not be 0
		if (ref.getPoints()<=0)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#increase(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean increase(QualityValue ref) {
		logger.debug("increase "+ref);
		if (!canBeIncreased(ref))
			return false;

		// Change model
		ref.setPoints(ref.getPoints()+1);
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#decrease(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean decrease(QualityValue ref) {
		logger.debug("decrease "+ref);
		if (!canBeDecreased(ref))
			return false;

		// Change model
		ref.setPoints(ref.getPoints()-1);
		if (ref.getPoints()==0)
			model.removeQuality(ref);
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#isRecommended(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public boolean isRecommended(Quality val) {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#getChoicesFor(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public List<Object> getChoicesFor(Quality val) {
		List<Object> ret = new ArrayList<>();
		if (val.needsChoice()) {
			switch (val.getSelect()) {
			default:
				logger.warn("TODO: don't know how to select "+val.getSelect());
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(SR6ConfigOptions.QUALITIES_DONT_REFUND, SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#getSelectionCost(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public int getSelectionCost(Quality val) {
		return (mode==CharGenMode.CREATING)?val.getCost():(val.getCost()*2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#getDeselectionCost(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public int getDeselectionCost(QualityValue val) {
		Quality qua = val.getModifyable();
		return -1* ((mode==CharGenMode.CREATING)?qua.getCost():(qua.getCost()*2));
	}

	//-------------------------------------------------------------------
	public void addToDo(ToDoElement toDoElement) {
		todos.add(toDoElement);
	}

}

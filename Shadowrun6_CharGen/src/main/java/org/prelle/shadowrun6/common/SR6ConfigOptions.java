package org.prelle.shadowrun6.common;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.ConfigOption.Type;

/**
 * @author Stefan Prelle
 *
 */
public class SR6ConfigOptions {
	
	public final static PropertyResourceBundle CONFIG_RES = (PropertyResourceBundle) ResourceBundle.getBundle(SR6ConfigOptions.class.getName());

	private final static String PROP_SKILLS_IGNORE_REQUIREMENTS      = "skills.ignore_requirements";
	private final static String PROP_METAECHOES_DONT_REFUND          = "metaechoes.dont_refund";
	private final static String PROP_METAECHOES_IGNORE_REFUND_ORDER  = "metaechoes.ignore_refund_order";
	private final static String PROP_QUALITIES_DONT_REFUND           = "qualities.dont_refund";
	private final static String PROP_QUALITIES_REFUND_FROM_CREATION  = "qualities.refund_from_creation";
	private final static String PROP_ALLOW_UNDO_FROM_CAREER   = "allow_undo_from_career";
	private final static String PROP_ALLOW_UNDO_FROM_CREATION = "allow_undo_from_creation";
	private final static String PROP_REFUND_FROM_CAREER       = "refund_from_career";
	private final static String PROP_REFUND_FROM_CREATION     = "refund_from_creation";
	private final static String PROP_PAY_GEAR                 = "pay_gear";
	private final static String PROP_HIDE_AUTOGEAR            = "hide_autogear";
	private final static String PROP_HOUSE_ADJUSTED_MAGIC_FOR_SPELLS_PP = "houserule.adjusted_magic_for_spells_pp";
	private final static String PROP_HOUSE_MYSTADEPT_NEED_TO_BUY_PP  = "houserule.mystic_adept_need_to_buy_pp";

	public static ConfigOption<Boolean> SKILLS_IGNORE_REQUIREMENTS;
	public static ConfigOption<Boolean> METAECHOES_DONT_REFUND;
	public static ConfigOption<Boolean> METAECHOES_IGNORE_REFUND_ORDER;
	public static ConfigOption<Boolean> QUALITIES_DONT_REFUND;
	public static ConfigOption<Boolean> QUALITIES_REFUND_FROM_CREATION;
	public static ConfigOption<Boolean> ALLOW_UNDO_FROM_CAREER;
	public static ConfigOption<Boolean> ALLOW_UNDO_FROM_CREATION;
	public static ConfigOption<Boolean> REFUND_FROM_CAREER;
	public static ConfigOption<Boolean> REFUND_FROM_CREATION;
	public static ConfigOption<Boolean> PAY_GEAR;
	public static ConfigOption<Boolean> HIDE_AUTOGEAR;
	public static ConfigOption<Boolean> HOUSERULE_ADJUSTED_MAGIC_FOR_SPELLS_PP;
	public static ConfigOption<Boolean> HOUSERULE_MYSTADEPT_NEED_TO_BUY_PP;
	
	public static void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		SKILLS_IGNORE_REQUIREMENTS     = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_SKILLS_IGNORE_REQUIREMENTS, Type.BOOLEAN, false);
		METAECHOES_DONT_REFUND         = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_METAECHOES_DONT_REFUND, Type.BOOLEAN, false);
		METAECHOES_IGNORE_REFUND_ORDER = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_METAECHOES_IGNORE_REFUND_ORDER, Type.BOOLEAN, false);
		QUALITIES_DONT_REFUND          = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_QUALITIES_DONT_REFUND, Type.BOOLEAN, true);
		QUALITIES_REFUND_FROM_CREATION = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_QUALITIES_REFUND_FROM_CREATION, Type.BOOLEAN, true);
		
		ALLOW_UNDO_FROM_CAREER   = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_ALLOW_UNDO_FROM_CAREER  , Type.BOOLEAN, true);
		ALLOW_UNDO_FROM_CREATION = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_ALLOW_UNDO_FROM_CREATION  , Type.BOOLEAN, false);
		REFUND_FROM_CAREER       = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_REFUND_FROM_CAREER  , Type.BOOLEAN, true);
		REFUND_FROM_CREATION     = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_REFUND_FROM_CREATION, Type.BOOLEAN, false);

		PAY_GEAR				 = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_PAY_GEAR, Type.BOOLEAN, true);
		HIDE_AUTOGEAR			 = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_HIDE_AUTOGEAR, Type.BOOLEAN, true);
		
		// Houserules
		HOUSERULE_ADJUSTED_MAGIC_FOR_SPELLS_PP = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_HOUSE_ADJUSTED_MAGIC_FOR_SPELLS_PP, Type.BOOLEAN, false);
		HOUSERULE_MYSTADEPT_NEED_TO_BUY_PP  = (ConfigOption<Boolean>) cfgShadowrun.createOption(PROP_HOUSE_MYSTADEPT_NEED_TO_BUY_PP, Type.BOOLEAN, false);
	}

}

/**
 *
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ComplexForm;
import org.prelle.shadowrun6.ComplexFormValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.ComplexFormController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.ComplexFormModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaComplexFormController implements ComplexFormController, CharacterProcessor {

	private static Logger logger = LogManager.getLogger("shadowrun6.lvl");

	protected ProcessorRunner parent;
	protected List<ToDoElement> todos;
	protected ShadowrunCharacter model;

	private List<ComplexForm> availableComplexForms;

	//-------------------------------------------------------------------
	/**
	 */
	public KarmaComplexFormController(ProcessorRunner parent) {
		this.parent = parent;
		todos      = new ArrayList<>();
		availableComplexForms   = new ArrayList<ComplexForm>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#getComplexFormsLeft()
	 */
	@Override
	public int getComplexFormsLeft() {
		return 0;
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		availableComplexForms.clear();
		availableComplexForms.addAll(ShadowrunCore.getComplexForms());
		// Remove those the character already has
		for (ComplexFormValue val : model.getComplexForms()) {
			if (val.getModifyable().isMultipleSelectable())
				continue;
			logger.trace("Not available anymore "+val.getName());
			availableComplexForms.remove(val.getModifyable());
		}

		Collections.sort(availableComplexForms);
		logger.info("Update available = "+availableComplexForms);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#getAvailableComplexForms()
	 */
	@Override
	public List<ComplexForm> getAvailableComplexForms() {
		return availableComplexForms;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#canBeSelected(org.prelle.shadowrun6.ComplexForm)
	 */
	@Override
	public boolean canBeSelected(ComplexForm data) {
		if (data==null)
			throw new NullPointerException();
		// Is it already selected
		for (ComplexFormValue already : model.getComplexForms()) {
			if (already.getModifyable()==data && !data.isMultipleSelectable()) {
				return false;
			}
		}

//		// Logic restricts amount of forms
//		if (model.getComplexForms().size()>=model.getAttribute(Attribute.LOGIC).getModifiedValue())
//			return false;

		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#canBeDeselected(org.prelle.shadowrun6.ComplexFormValue)
	 */
	@Override
	public boolean canBeDeselected(ComplexFormValue cform) {
		// Minimum requirement is that the complex form is known
		if (!model.getComplexForms().contains(cform)) {
			return false;
		}
		
		boolean allowUndoCareer   = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER.getValue();
		boolean allowUndoCreation = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.getValue();
		ComplexFormModification mod = getModification(cform);
				
		if (mod==null) {
			return allowUndoCreation;
		} else if (mod.getSource()==this) {
			// This career session - always allowed
			return true;
		} else {
			return allowUndoCareer;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#select(org.prelle.shadowrun6.ComplexForm)
	 */
	@Override
	public ComplexFormValue select(ComplexForm data) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select complex form '"+data.getId()+"' which is not allowed");
			return null;
		}
		// Don't operate when a choice is required
		if (data.needsChoice()) {
			logger.error("Selection of complex form "+data.getId()+" requires a choice");
			return null;
		}
		
		
		int cost = 5;
		logger.debug("select "+data+" for "+cost+" karma");

		// Change model
		logger.info("Selected data "+data);
		ComplexFormValue ref = new ComplexFormValue(data);
		model.addComplexForm(ref);

		// Make undoable
		ComplexFormModification mod = new ComplexFormModification(data, this, null);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		parent.runProcessors();
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#select(org.prelle.shadowrun6.ComplexForm, java.lang.Object)
	 */
	@Override
	public ComplexFormValue select(ComplexForm data, Object choice) {
		if (!canBeSelected(data)) {
			logger.warn("Trying to select complex form '"+data.getId()+"' which is not allowed");
			return null;
		}
		if (choice==null) {
			logger.error("Choice is a null pointer");
			return null;
		}
		
		
		int cost = 5;
		logger.debug("select "+data+" with choice '"+choice+"/"+choice.getClass()+"' for "+cost+" karma");

		// Change model
		logger.info("Selected data "+data);
		ComplexFormValue ref = new ComplexFormValue(data);
		ref.setChoice(choice);
		ref.setChoiceReference(ShadowrunTools.getChoiceReference(data.getChoice(), choice));
		model.addComplexForm(ref);

		// Make undoable
		ComplexFormModification mod = new ComplexFormModification(data, this, null);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setChoice(ref.getChoiceReference());
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		parent.runProcessors();
		return ref;
	}

	//-------------------------------------------------------------------
	private ComplexFormModification getModification(ComplexFormValue key) {
		ComplexFormModification ret = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof ComplexFormModification))
				continue;
			ComplexFormModification amod = (ComplexFormModification)mod;
			if (amod.getComplexForm()!=key.getModifyable())
				continue;
			if (amod.getComplexForm().needsChoice()) {
				if (!amod.getChoice().equals(key.getChoiceReference()))
					continue;
				
			}
//			if (ret==null || mod.getExpCost()>ret.getExpCost())
//				ret = amod;
			return amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ComplexFormController#deselect(org.prelle.shadowrun6.ComplexFormValue)
	 */
	@Override
	public void deselect(ComplexFormValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return;

		model.removeComplexForm(ref);

		boolean refundCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		boolean refundCareer   = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		ComplexFormModification mod = getModification(ref);
		
		int cost = 0;
		if (mod==null) {
			// Spell was chosen during creation
			cost = refundCreation?5:0;
		} else if (mod.getExpCost()==0) {
			// Spell was granted for free
		} else if (mod.getSource()==this) {
			// Spell was selected this session
			cost = mod.getExpCost();
		} else {
			// Spell was selected during a previous career session
			cost = refundCareer?5:0;
		}
		logger.debug("Refund "+cost+" karma    - refundCreation="+refundCreation+", refundCareer="+refundCareer);
		
		if (mod!=null) {
			model.removeFromHistory(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(
				SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER, 
				SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION, 
				SR6ConfigOptions.REFUND_FROM_CAREER, 
				SR6ConfigOptions.REFUND_FROM_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.debug("run");
		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			updateAvailable();
		} finally {
			
		}
		return unprocessed;
	}

}

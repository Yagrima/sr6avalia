/**
 * 
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Sense;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.ShadowrunCharGenConstants;
import org.prelle.shadowrun6.modifications.AdeptPowerModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class AdeptPowerLeveller implements AdeptPowerController, CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.gen");

	private final static ResourceBundle RES = ShadowrunCharGenConstants.RES;
	
	private ProcessorRunner parent;
	private ShadowrunCharacter model;
	private List<ToDoElement> todos;
	private List<AdeptPower> available;

	//--------------------------------------------------------------------
	public AdeptPowerLeveller(CharacterController parent) {
		this.parent = parent;
		todos      = new ArrayList<>();
		available  = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#getPowerPointsInvested()
	 */
	@Override
	public float getPowerPointsInvested() {
		float invest = 0;
		for (AdeptPowerValue tmp : model.getAdeptPowers()) {
			if (model.getAutoAdeptPowers().contains(tmp))
				continue;
//			AdeptPower power = tmp.getModifyable();
//			if (tmp.getLevel()>0) {
//				invest += power.getCostForLevel(tmp.getLevel());
//			} else {
				invest += tmp.getCost();
//			}
		}
		return invest;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#getAvailablePowers()
	 */
	@Override
	public List<AdeptPower> getAvailablePowers() {
		return available;
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		// Build a list 
		available.clear();
		for (AdeptPower power : ShadowrunCore.getAdeptPowers()) {			
			if (canBeSelected(power))
				available.add(power);
		}
		
		Collections.sort(available);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#getPowerPointsLeft()
	 */
	@Override
	public float getPowerPointsLeft() {
		int points = 0;
		AttributeValue aVal = model.getAttribute(Attribute.POWER_POINTS);
		if (aVal!=null)
			points = aVal.getModifiedValue();
		return ((float)points) - getPowerPointsInvested();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canIncreasePowerPoints()
	 */
	@Override
	public boolean canIncreasePowerPoints() {
		return false;
//		MagicOrResonanceType magic = model.getMagicOrResonanceType();
//		if (!magic.usesPowers())
//			return false;
//		
//		// Normal adepts don't increase power points
//		if (!magic.paysPowers())
//			return false;
//		
//		// Cannot increase higher than magic attribute
//		if (model.getBoughtPowerPoints() >= model.getAttribute(Attribute.MAGIC).getModifiedValue())
//			return false;
//		
//		// Needs 5 Karma per power point
//		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canDecreasePowerPoints()
	 */
	@Override
	public boolean canDecreasePowerPoints() {
		return false;
//		return model.getBoughtPowerPoints()>0 && getPowerPointsLeft()>=1.0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#increasePowerPoints()
	 */
	@Override
	public boolean increasePowerPoints() {
		if (!canIncreasePowerPoints())
			return false;
		
		AttributeValue val = model.getAttribute(Attribute.POWER_POINTS);
		val.setPoints(val.getPoints()+1);
		model.setKarmaFree(model.getKarmaFree()-5);
		model.setKarmaInvested(model.getKarmaInvested()+5);
		logger.info("Increase power points to "+model.getPowerPoints());
		
		// Inform listeners
		parent.runProcessors();

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#decreasePowerPoint()
	 */
	@Override
	public boolean decreasePowerPoint() {
		if (!canDecreasePowerPoints())
			return false;
		
		AttributeValue val = model.getAttribute(Attribute.POWER_POINTS);
		val.setPoints(val.getPoints()-1);
		model.setKarmaFree(model.getKarmaFree()+5);
		model.setKarmaInvested(model.getKarmaInvested()-5);
		logger.info("Decrease power points to "+model.getPowerPoints());
		
		// Inform listeners
		parent.runProcessors();

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeSelected(org.prelle.shadowrun6.AdeptPower)
	 */
	@Override
	public boolean canBeSelected(AdeptPower data) {
		MagicOrResonanceType magic = model.getMagicOrResonanceType();
		if (!magic.usesPowers())
			return false;
		
		// If already selected, only
		if (model.hasAdeptPower(data.getId())) {
			// Character already has power. Can it be selected
			// multiple times?
			if (data.getSelectFrom()==null) {
				// No, than this can not be selected
				return false;
			}
		}
		// Can the character afford the cost
		if (getPowerPointsLeft()<data.getCostForLevel(1))
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	private boolean canBeSelected(AdeptPower data, Object choice) {
		if (!canBeSelected(data))
			return false;
		
		// If already selected, only
		if (model.hasAdeptPower(data.getId())) {
			// Ensure no given power has same choice
			for (AdeptPowerValue tmp : model.getAdeptPowers()) {
				if (tmp.getModifyable()!=data)
					continue;
				if (tmp.getChoice()==choice) {
					return false;
				}
			}
		}
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#select(org.prelle.shadowrun6.AdeptPower)
	 */
	@Override
	public AdeptPowerValue select(AdeptPower data) {
		logger.debug("try select "+data);
		if (!canBeSelected(data))
			return null;
		if (data.needsChoice())
			throw new IllegalArgumentException(data.getId()+" needs to be accompanied with a selection");

		// Determine cost
		int cost = 0;
		
		AdeptPowerValue ref = new AdeptPowerValue(data);
		ref.setLevel(1);
		model.addAdeptPower(ref);
		logger.info("Selected adept power "+ref);

		// Make undoable
		AdeptPowerModification mod = new AdeptPowerModification(data, 1);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);
		
		// Inform listeners
		parent.runProcessors();
		
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#select(org.prelle.shadowrun6.AdeptPower, java.lang.Object)
	 */
	@Override
	public AdeptPowerValue select(AdeptPower data, Object selection) {
		logger.debug("try select "+data+" with "+selection);
		if (!canBeSelected(data, selection))
			return null;
		
		AdeptPowerValue ref = new AdeptPowerValue(data);
		ref.setLevel(1);
		switch (data.getSelectFrom()) {
		case PHYSICAL_ATTRIBUTE:
			if (!(selection instanceof Attribute) || !((Attribute)selection).isPhysical()) {
				logger.warn("Selection "+selection+" is not a physical attribute");
				return null;
			}
			ref.setChoice((Attribute)selection);
			ref.setChoiceReference(((Attribute)selection).name());
			break;
		case COMBAT_SKILL:
			if (!(selection instanceof Skill))
				return null;
			Skill skilSel = (Skill)selection;
			if (skilSel.getType()!=SkillType.COMBAT) {
				logger.warn("Can only select combat skills");
				return null;
			}
			ref.setChoice((Skill)selection);
			ref.setChoiceReference(((Skill)selection).getId());
//			ref.setSelectedSkill(skilSel);
			break;
		case SKILL:
			if (!(selection instanceof Skill))
				return null;
			skilSel = (Skill)selection;
			switch (skilSel.getType()) {
			case COMBAT:
			case PHYSICAL:
			case SOCIAL:
			case TECHNICAL:
			case VEHICLE:
//				ref.setSelectedSkill(skilSel);
				ref.setChoice(skilSel);
				ref.setChoiceReference(skilSel.getId());
				break;
			default:
				logger.warn("Can not select "+skilSel.getType()+" for "+data);
				return null;
			}
			break;
		case SENSE:
			if (!(selection instanceof Sense)) {
				logger.warn("Selection "+selection+" is not a sense");
				return null;
			}
			ref.setChoice((Sense)selection);
			ref.setChoiceReference(((Sense)selection).name());
			break;
		default:
			throw new RuntimeException("Not supported yet: "+data.getSelectFrom());
		}
		model.addAdeptPower(ref);
		logger.info("Selected adept power "+ref);

		// Make undoable
		AdeptPowerModification mod = new AdeptPowerModification(data, 1, ref.getChoiceReference());
		mod.setExpCost(0);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);
		
		// Inform listeners
		parent.runProcessors();
		
		return ref;
	}

	//-------------------------------------------------------------------
	private AdeptPowerModification getModification(AdeptPowerValue sVal) {
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof AdeptPowerModification))
				continue;
			AdeptPowerModification amod = (AdeptPowerModification)mod;
			if (amod.getPower()!=sVal.getModifyable())
				continue;
			if (amod.getValue()!=sVal.getLevel())
				continue;
			return amod;
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeDeselected(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean canBeDeselected(AdeptPowerValue ref) {
		if (!model.getAdeptPowers().contains(ref)) {
			return false;
		}
		if (model.getAutoAdeptPowers().contains(ref)) {
			return false;
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#deselect(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean deselect(AdeptPowerValue ref) {
		logger.debug("try deselect "+ref);
		if (!canBeDeselected(ref))
			return false;
		
		model.removeAdeptPower(ref);
		logger.info("Deselected adept power "+ref);
		
		// Inform listeners
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeIncreased(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean canBeIncreased(AdeptPowerValue ref) {
		// Is the power one that can be increased
		if (!ref.getModifyable().hasLevels())
			return false;
		// Is the level below magic attribute
		if (ref.getLevel()>=model.getAttribute(Attribute.MAGIC).getModifiedValue())
			return false;
		
		// Is there enough karma
		float costDiff = ref.getModifyable().getCostForLevel(ref.getLevel()+1) - ref.getModifyable().getCostForLevel(ref.getLevel());
		if (getPowerPointsLeft()<costDiff)
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#canBeDecreased(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean canBeDecreased(AdeptPowerValue ref) {
		// Is the power one that can be increased
		if (!ref.getModifyable().hasLevels())
			return false;
		
		// Is the level >0
		if (ref.getLevel()<=0)
			return false;
		
		if (ref.getLevel()==1)
			return canBeDeselected(ref);
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#increase(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean increase(AdeptPowerValue ref) {
		if (!canBeIncreased(ref))
			return false;
		
		logger.info("Increase power "+ref.getModifyable().getId()+" from "+ref.getLevel()+" to "+(ref.getLevel()+1)+" (free="+ref.getAutoLevel()+")");
		ref.setLevel(ref.getLevel()+1);
		// If power was only auto selected before, add it to user selection now
		if (model.getAutoAdeptPowers().contains(ref)) {
			model.removeAdeptPower(ref);
			model.addAdeptPower(ref);
			logger.debug("  Move power from auto selection to user selection");
		}
		

		// Make undoable
		AdeptPowerModification mod = new AdeptPowerModification(ref.getModifyable(), ref.getLevel(), ref.getChoiceReference());
		mod.setExpCost(0);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);
		
		// Inform listeners
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#decrease(org.prelle.shadowrun6.AdeptPowerValue)
	 */
	@Override
	public boolean decrease(AdeptPowerValue ref) {
		if (!canBeDecreased(ref))
			return false;
		
		logger.info("Decrease power "+ref.getModifyable().getId()+" from "+ref.getLevel()+" to "+(ref.getLevel()-1));
		ref.setLevel(ref.getLevel()-1);
		if (ref.getLevel()==0) {
			logger.info("Removed power "+ref.getModifyable().getId());
			model.removeAdeptPower(ref);
		}
		
		// Undo
		AdeptPowerModification mod = getModification(ref);
		if (mod!=null)
			model.removeFromHistory(mod);
		
		// Inform listeners
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AdeptPowerController#isRecommended(org.prelle.shadowrun6.AdeptPower)
	 */
	@Override
	public boolean isRecommended(AdeptPower val) {
		// TODO Auto-generated method stub
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.debug("run");
		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			updateAvailable();
			
			float left = getPowerPointsLeft(); 
			if (left>0 && model.getMagicOrResonanceType().usesPowers()) {
				todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "priogen.todo.power", left)));
			} else if (left<0 && model.getMagicOrResonanceType().usesPowers()) {
				todos.add(new ToDoElement(Severity.STOPPER, Resource.format(RES, "priogen.todo.toomanypower", Math.abs(left))));
			}
		} finally {
			
		}
		return unprocessed;
	}

}

package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.common.CommonEquipmentController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;

public class EquipmentLeveller extends CommonEquipmentController {

	//-------------------------------------------------------------------
	public EquipmentLeveller(ProcessorRunner parent, ShadowrunCharacter model) {
		super(parent, model, CharGenMode.LEVELING);
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canChangeCount(org.prelle.shadowrun6.items.CarriedItem, int)
	 */
	@Override
	public boolean canChangeCount(CarriedItem data, int newCount) {
		if (newCount<1) return false;
		if (newCount>100) return false;
		
		Boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (newCount>data.getCount() && payGear) {
			// On increase
			int instanceCost = data.getAsValue(ItemAttribute.PRICE).getModifiedValue() / data.getCount();
			instanceCost = Math.round( getCostWithMetatypeModifier(data.getItem(), instanceCost) );
			return instanceCost<=model.getNuyen();
		}
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#increase(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean increase(CarriedItem data) {
		boolean allowed = super.increase(data);
		if (!allowed)
			return false;

		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			int instanceCost = data.getAsValue(ItemAttribute.PRICE).getModifiedValue() / data.getCount();
			instanceCost = Math.round( getCostWithMetatypeModifier(data.getItem(), instanceCost) );
			if (instanceCost > model.getNuyen())
				return false;
			model.setNuyen(model.getNuyen() - instanceCost);

			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, data));
		}		

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#decrease(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean decrease(CarriedItem data) {
		boolean allowed = super.decrease(data);
		if (!allowed)
			return false;
		
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			int instanceCost = data.getAsValue(ItemAttribute.PRICE).getModifiedValue() / data.getCount();
			instanceCost = Math.round( getCostWithMetatypeModifier(data.getItem(), instanceCost) );
			model.setNuyen(model.getNuyen() + instanceCost);

			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_CHANGED, data));
		}
		return true;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#select(org.prelle.shadowrun6.items.ItemTemplate, int)
	 */
	@Override
	public CarriedItem select(ItemTemplate item, SelectionOption...options) {
		logger.debug("START-------select "+item.getId()+"-------------");
		CarriedItem added = super.select(item, options);
		if (added==null)
			return null;
		
		/*
		 * In case of augmentation, decrease evtl. existing essence hole
		 */
		if (Arrays.asList(ItemType.bodytechTypes()).contains(added.getUsedAsType()) && model.getEssenceHole()>0) {
			int essence = added.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue();
			int reduce  = Math.min(essence, model.getEssenceHole());
			logger.info("Adding "+added+" reduces essence hole by "+(reduce/1000f));
			model.setEssenceHole(model.getEssenceHole()-reduce);
			if (model.getEssenceHole()<0)
				model.setEssenceHole(0);
		}

		/*
		 * Pay with nuyen
		 */
		logger.debug("selected - eventually pay");
		boolean payGear = (SR6ConfigOptions.PAY_GEAR!=null)?((Boolean)SR6ConfigOptions.PAY_GEAR.getValue()):false;
		if (payGear) {
			int price = added.getAsValue(ItemAttribute.PRICE).getModifiedValue();
			price = Math.round(getCostWithMetatypeModifier(item, price));
			model.setNuyen(model.getNuyen()-price);
			logger.info("Paying '"+added.getNameWithRating()+"' costs "+price+" and reduces the nuyen to "+model.getNuyen());
		} else
			logger.info("PAY_GEAR is false - get '"+added.getNameWithRating()+"' for free");
		
		
		parent.runProcessors();
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_ADDED, added));
		return added;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#deselect(org.prelle.shadowrun6.items.CarriedItem)
	 */
	@Override
	public boolean deselect(CarriedItem ref) {
		logger.info("Deselect "+ref+"  (it exists "+ref.getCount()+" times)");
		boolean removed = super.deselect(ref);
		logger.debug("Removing "+ref.getItem()+" returned "+removed);
		if (!removed)
			return false;
		
		// Get money
		model.setNuyen(model.getNuyen() + ref.getAsValue(ItemAttribute.PRICE).getModifiedValue());
		
		
		/*
		 * In case of augmentation, refund essence
		 */
		if (Arrays.asList(ItemType.bodytechTypes()).contains(ref.getUsedAsType())) {
			int essence = ref.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue();
			logger.info("Undoing "+ref+" refunds "+(essence/1000f)+" essence.");
			model.setEssence(model.getEssence() + (essence/1000f));
		}

		if (removed) {
			parent.runProcessors();
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_REMOVED, removed));
		}
		return removed;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#sell(org.prelle.shadowrun6.items.CarriedItem, float)
	 */
	@Override
	public boolean sell(CarriedItem ref, float factor) {
		logger.debug("Sell "+ref+" with factor "+factor+"  (it exists "+ref.getCount()+" times)");
		float grant = ref.getAsValue(ItemAttribute.PRICE).getModifiedValue() * factor;
		grant = getCostWithMetatypeModifier(ref.getItem(), grant);

		/*
		 * Don't sell gear when you don't need to pay
		 */
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();

		
		if (ref.getCount()>1) {
			// Multiple instances - sell only one instance
			grant = grant / ref.getCount();
			if (payGear) {
				model.setNuyen(model.getNuyen() + Math.round(grant));
				logger.info("Sell "+ref.getNameWithRating()+" and get "+Math.round(grant)+" nuyen");
			} else {
				logger.info("Sell "+ref.getNameWithRating()+" but don't get nuyen because PAY_GEAR is off");
			}
			ref.setCount(ref.getCount()-1);
			return true;
		}
		
		// Only one instance
		boolean removed = super.sell(ref, factor);
		if (!removed) {
			logger.warn("Removing failed");
			return removed;
		}
		
		
		if (payGear) {
			model.setNuyen(model.getNuyen() + Math.round(grant));
			logger.info("Sell "+ref.getNameWithRating()+" and get "+Math.round(grant)+" nuyen");
		} else {
			logger.info("Sell "+ref.getNameWithRating()+" but don't get nuyen because PAY_GEAR is off");
		}
		
		/*
		 * In case of augmentation, enlarge essence hole
		 */
		if (Arrays.asList(ItemType.bodytechTypes()).contains(ref.getUsedAsType())) {
			int essence = ref.getAsValue(ItemAttribute.ESSENCECOST).getModifiedValue();
			logger.info("Selling/Trashing "+ref+" increases the essence hole by "+(essence/1000f));
			model.setEssenceHole(model.getEssenceHole()+essence);
		}

		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#embed(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, SelectionOption...options) {
		logger.debug("START-------embed "+item.getId()+"-------------");
		// Read config
		boolean payGear = (SR6ConfigOptions.PAY_GEAR!=null)?((Boolean)SR6ConfigOptions.PAY_GEAR.getValue()):false;
		// Ensure that gear can be payed (if need be)
		if (payGear && !canBePayed(item, true, options)) {
			logger.warn("Trying to embed an item that cannot be payed - and PAY_GEAR is true");
			return null;
		}

		CarriedItem embedded = super.embed(container, item, options);
		if (embedded==null)
			return null;

		/*
		 * Pay with nuyen
		 */
		if (payGear) {
			int price = embedded.getAsValue(ItemAttribute.PRICE).getModifiedValue();
			price = Math.round(getCostWithMetatypeModifier(item, price));
			model.setNuyen(model.getNuyen()-price);
			logger.info("Paying embedded '"+embedded.getNameWithRating()+"' costs "+price+" and reduces the nuyen to "+model.getNuyen());
		} else
			logger.info("PAY_GEAR is false - get '"+embedded.getNameWithRating()+"' for free");

		parent.runProcessors();
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, container));
		return embedded;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#embed(org.prelle.shadowrun6.items.CarriedItem, org.prelle.shadowrun6.items.ItemTemplate)
	 */
	@Override
	public CarriedItem embed(CarriedItem container, ItemTemplate item, ItemHook hook, SelectionOption...options) {
		logger.debug("START-------embed "+item.getId()+"-------------");
		// Read config
		boolean payGear = (SR6ConfigOptions.PAY_GEAR!=null)?((Boolean)SR6ConfigOptions.PAY_GEAR.getValue()):false;
		// Ensure that gear can be payed (if need be)
		if (payGear && !canBePayed(item, true, options)) {
			logger.warn("Trying to embed an item that cannot be payed - and PAY_GEAR is true");
			return null;
		}
		// Can it be embedded at all?
		if (!super.canBeEmbedded(container, item, hook, options)) {
			logger.warn("Trying to embed an item that cannot be embedded");
			return null;
		}
		
		
		CarriedItem embedded = super.embed(container, item, hook, options);
		if (embedded==null)
			return null;

		/*
		 * Pay with nuyen
		 */
		if (payGear) {
			int price = embedded.getAsValue(ItemAttribute.PRICE).getModifiedValue();
			price = Math.round(getCostWithMetatypeModifier(item, price));
			model.setNuyen(model.getNuyen()-price);
			logger.info("Paying embedded '"+embedded.getNameWithRating()+"' costs "+price+" and reduces the nuyen to "+model.getNuyen());
		} else
			logger.info("PAY_GEAR is false - get '"+embedded.getNameWithRating()+"' for free");

		parent.runProcessors();
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.EQUIPMENT_CHANGED, container));
		return embedded;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canIncreaseBoughtNuyen()
	 */
	@Override
	public boolean canIncreaseBoughtNuyen() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#canDecreaseBoughtNuyen()
	 */
	@Override
	public boolean canDecreaseBoughtNuyen() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#increaseBoughtNuyen()
	 */
	@Override
	public boolean increaseBoughtNuyen() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#decreaseBoughtNuyen()
	 */
	@Override
	public boolean decreaseBoughtNuyen() {
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.EquipmentController#getBoughtNuyen()
	 */
	@Override
	public int getBoughtNuyen() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(SR6ConfigOptions.PAY_GEAR, SR6ConfigOptions.HIDE_AUTOGEAR);
	}

}

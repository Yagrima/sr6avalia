/**
 * 
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.Quality.QualityType;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.common.CommonQualityController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.QualityModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author spr
 *
 */
public class QualityLeveller extends CommonQualityController implements QualityController {

	//-------------------------------------------------------------------
	public QualityLeveller(CharacterController parent) {
		super(parent, CharGenMode.LEVELING);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.common.CommonQualityController#getKarmaCost(org.prelle.shadowrun6.Quality)
	 */
	@Override
	protected int getKarmaCost(Quality data) {
		return data.getCost() * 2;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.common.CommonQualityController#getKarmaCost(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	protected int getKarmaCost(QualityValue data) {
		return (data.getPoints()*data.getModifyable().getCost()) * 2;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#getNetBonusKarma()
	 */
	@Override
	public int getNetBonusKarma() {
		return 0;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#select(org.prelle.shadowrun6.Quality)
	 */
	@Override
	public QualityValue select(Quality data) {
		QualityValue sVal = super.select(data);
		
		int karma = 0;
		if (data.getType()==QualityType.POSITIVE) {
			// pay twice the Karma cost of the quality
			karma = getKarmaCost(data);
			model.setKarmaFree(model.getKarmaFree()-karma);
			model.setKarmaInvested(model.getKarmaInvested()+karma);
			logger.debug("Pay "+karma+" Karma");
		}
		// Log in history
		QualityModification mod = new QualityModification(data, 1);
		mod.setRemove(false);
		mod.setExpCost(karma);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));

		model.addToHistory(mod);
		
		parent.runProcessors();
		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#select(org.prelle.shadowrun6.Quality, java.lang.Object)
	 */
	@Override
	public QualityValue select(Quality data, Object choice) {
		QualityValue sVal = super.select(data, choice);
		
		if (mode==CharGenMode.LEVELING && data.getType()==QualityType.POSITIVE) {
			// pay twice the Karma cost of the quality
			int karma = getKarmaCost(data);
			model.setKarmaFree(model.getKarmaFree()-karma);
			model.setKarmaInvested(model.getKarmaInvested()+karma);
			// Log in history
			QualityModification mod = new QualityModification(data, 1);
			mod.setRemove(false);
			mod.setExpCost(karma);
			mod.setSource(this);
			mod.setDate(new Date(System.currentTimeMillis()));
			mod.setChoice(sVal.getChoiceReference());
			model.addToHistory(mod);
		}

		parent.runProcessors();
		return sVal;
	}

	//-------------------------------------------------------------------
	private QualityModification getLatestModification(QualityValue key) {
		QualityModification ret = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof QualityModification))
				continue;
			QualityModification qmod = (QualityModification)mod;
			if (mod.getSource()!=this && mod.getSource()!=null)
				continue;
			if (qmod.getModifiedItem()!=key.getModifyable())
				continue;
			if (qmod.getModifiedItem().needsChoice() && !key.getChoiceReference().equals(qmod.getChoice()))
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = qmod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	private void deselectInLevelling(QualityValue ref) {
		Quality data = ref.getModifyable();
		int karma = 0;
		boolean refund = !(Boolean)SR6ConfigOptions.QUALITIES_DONT_REFUND.getValue();
		boolean refundFromCreation = !(Boolean)SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getValue();
		logger.info("OPTION "+SR6ConfigOptions.QUALITIES_DONT_REFUND.getLocalId()+" = "+SR6ConfigOptions.QUALITIES_DONT_REFUND.getValue());
		logger.info("OPTION "+SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getLocalId()+" = "+SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getValue());
		// Is this simply an undo?
		QualityModification undo = getLatestModification(ref);
		if (undo!=null) {
			// This is an undo of a quality selected during character advancement
			
			if (undo.getSource()==this) {
				// Undo something from this session
				if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
					// Undo a negative quality added during this session 
					// --> No need to repay karma
					model.removeFromHistory(undo);
					logger.info("Negative quality '"+ref.getModifyable().getId()+"'had been bought during this session - no refund");
				} else {
					// Undo a positive quality added during this session
					// --> refund karma
					karma = undo.getExpCost();
					model.setKarmaFree(model.getKarmaFree()+karma);
					model.setKarmaInvested(model.getKarmaInvested()-karma);
					model.removeFromHistory(undo);
					logger.info("Positive quality '"+ref.getModifyable().getId()+"'had been bought during this session - refund "+karma+" karma");
				}
			} else {
				// Undo something from a previous session
				karma = undo.getExpCost();
				if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
					// This negative quality has been added some sessions ago - without gaining karma
					// Usually this requires Cost*2 karma to remove
					if (refund) {
						// --> No need to pay for removal
						model.removeFromHistory(undo);
						logger.info("Negative quality '"+ref.getModifyable().getId()+"'had been bought during a past session - but due to 'DoRefund' no karma needs to be payed for removal");
					} else {
						// Pay regular cost to remove it
						karma = getKarmaCost(ref);
						// Log in history
						QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
						mod.setRemove(true);
						mod.setExpCost(karma);
						mod.setSource(this);
						if (data.needsChoice())
							mod.setChoice(ref.getChoiceReference());
						model.addToHistory(mod);
						logger.info("Negative quality '"+ref.getModifyable().getId()+"' had been bought during a past session - pay twice Karma cost for removal");
					}						
				} else {
					// This positive quality has been added some sessions ago - by paying karma
					// Usually this would not give any karma refund, when losing it
					if (refund) {
						// Refund karma spent for this positive quality
						logger.info("Positive quality '"+ref.getModifyable().getId()+"' had been bought during a past session - but due to 'DoRefund' Karma gets refunded");
						karma = undo.getExpCost();
						model.setKarmaFree(model.getKarmaFree()+karma);
						model.setKarmaInvested(model.getKarmaInvested()-karma);
						model.removeFromHistory(undo);
					} else {
						// Simply loose quality without Karma refund
						QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
						mod.setRemove(true);
						mod.setExpCost(0);
						mod.setSource(this);
						if (data.needsChoice())
							mod.setChoice(ref.getChoiceReference());
						model.addToHistory(mod);
						logger.info("Positive quality '"+ref.getModifyable().getId()+"' had been bought during a past session - gain nothing from removal");
					}
				}
			}
		} else {
			// This is an undo of a quality selected during character generation
			if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
				// This negative quality has been added on generation - with gaining karma
				// Usually this requires normal karma to remove
				if (refundFromCreation) {
					// Regain karma
					karma = ref.getModifyable().getCost() * ref.getPoints();
					model.setKarmaFree(model.getKarmaFree()+karma);
					model.setKarmaInvested(model.getKarmaInvested()-karma);
					logger.info("Negative quality '"+ref.getModifyable().getId()+"' from creation - 'Refund from creation' is active, so get karma");
				} else {
					karma = getKarmaCost(ref);
					// Log in history
					QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
					mod.setRemove(true);
					mod.setExpCost(karma);
					mod.setSource(this);
					if (data.needsChoice())
						mod.setChoice(ref.getChoiceReference());
					model.addToHistory(mod);
					logger.info("Negative quality '"+ref.getModifyable().getId()+"' from creation - pay twice Karma cost for removal");
				}						
			} else {
				// This positive quality has been picked on creation - by paying karma
				// Usually this would not give any karma refund, when losing it
				if (refundFromCreation) {
					// Refund karma spent for this positive quality
					logger.info("Positive quality '"+ref.getModifyable().getId()+"' from creation - but due to 'Refund from creation' Karma gets refunded");
					karma = ref.getModifyable().getCost() * ref.getPoints();
					model.setKarmaFree(model.getKarmaFree()+karma);
					model.setKarmaInvested(model.getKarmaInvested()-karma);
					// Log it
					QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
					mod.setRemove(true);
					mod.setExpCost(-karma);
					mod.setSource(this);
					if (data.needsChoice())
						mod.setChoice(ref.getChoiceReference());
					model.addToHistory(mod);
				} else {
					// Simply loose quality without Karma refund
					QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
					mod.setRemove(true);
					mod.setExpCost(0);
					mod.setSource(this);
					if (data.needsChoice())
						mod.setChoice(ref.getChoiceReference());
					model.addToHistory(mod);
					logger.info("Positive quality '"+ref.getModifyable().getId()+"' from creation - gain nothing from removal");
				}
			}
		}
		
		
		if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
			// pay twice the Karma cost of the quality
			karma = getKarmaCost(ref);
			model.setKarmaFree(model.getKarmaFree()-karma);
			model.setKarmaInvested(model.getKarmaInvested()+karma);
			logger.info("Pay "+karma+" to remove negative quality: "+ref);
		} else if (getLatestModification(ref)!=null) {
		} else if (refund) {
			// TODO: refund for removing positive quality
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#canBeDeselected(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean canBeDeselected(QualityValue ref) {
		if (!super.canBeDeselected(ref))
			return false;
		
		// Must have enough Karma to remove negative qualities
		if (ref.getModifyable().getType()==QualityType.NEGATIVE)
			return model.getKarmaFree() >= getKarmaCost(ref);
			
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#deselect(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean deselect(QualityValue ref) {
		boolean success = super.deselect(ref);
		if (!success)
			return false;

		deselectInLevelling(ref);
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.QualityController#increase(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean increase(QualityValue ref) {
		boolean success = super.increase(ref);
		if (!success)
			return false;

		Quality data = ref.getModifyable();
		int karma = 0;
		boolean refund = !(Boolean)SR6ConfigOptions.QUALITIES_DONT_REFUND.getValue();
		boolean refundFromCreation = !(Boolean)SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getValue();
		logger.info("OPTION "+SR6ConfigOptions.QUALITIES_DONT_REFUND.getLocalId()+" = "+SR6ConfigOptions.QUALITIES_DONT_REFUND.getValue());
		logger.info("OPTION "+SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getLocalId()+" = "+SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getValue());

		// Is this simply an undo?
		QualityModification undo = getLatestModification(ref);
		logger.info("undo = "+undo);
		if (undo!=null) {
			// This is an undo of a quality decreased during character advancement
			
			if (undo.getSource()==this) {
				// Undo something from this session
				if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
					// Undo a negative quality decreased during this session 
					karma = undo.getExpCost(); // May have been 0, if nothing had been paid
					logger.info("Negative quality '"+ref.getModifyable().getId()+"'had been decreased this session  - refund "+karma+" karma");
				} else {
					// Positive quality
					karma = undo.getExpCost(); // May have been 0, if nothing had been paid
					logger.info("Positive quality '"+ref.getModifyable().getId()+"'had been decreased this session granting "+karma+" Karma - pay that again");
				}
				model.removeFromHistory(undo);
			} else {
				// Undo something from a previous session
				karma = undo.getExpCost();
				if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
					// This negative quality has been decreased some sessions ago - with paying Cost*2 karma
					// Increasing it again grants that Karma again, if "refund" is active
					if (refund) {
						// --> Get Karma granted when re-increasing it again
						model.removeFromHistory(undo);
						logger.info("Negative quality '"+ref.getModifyable().getId()+"'had been decreased during a past session - but due to 'DoRefund' invested karma for decreasing is refunded");
					} else {
						// Ignore previous decreasing - just increase it for 0 karma
						karma = 0;
						// Log in history
						QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
						mod.setRemove(true);
						mod.setExpCost(karma);
						mod.setSource(this);
						if (data.needsChoice())
							mod.setChoice(ref.getChoiceReference());
						model.addToHistory(mod);
						logger.info("Negative quality '"+ref.getModifyable().getId()+"' had been decreased during a past session - pay twice Karma cost for removal");
					}						
				} else {
					// Simply pay Cost*2 to get an increase in an positive quality
					karma = undo.getExpCost();
					logger.info("Positive quality '"+ref.getModifyable().getId()+"' gets increased for "+karma+" karma");
					QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
					mod.setRemove(true);
					mod.setExpCost(0);
					mod.setSource(this);
					if (data.needsChoice())
						mod.setChoice(ref.getChoiceReference());
					model.addToHistory(mod);
				}
			}
		} else {
			// This is an increase of a quality selected during character generation
			if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
				// This negative quality has been added on generation - don't get any benefit from increasing it
				karma = 0;
				// Log in history
				QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
				mod.setRemove(true);
				mod.setExpCost(karma);
				mod.setSource(this);
				model.addToHistory(mod);
				if (data.needsChoice())
					mod.setChoice(ref.getChoiceReference());
				logger.info("Negative quality '"+ref.getModifyable().getId()+"' from creation increased without getting karma ");
			} else {
				// Simply pay Cost*2 to get an increase in an positive quality
				karma = getKarmaCost(ref.getModifyable());
				logger.info("Positive quality '"+ref.getModifyable().getId()+"' gets increased for "+karma+" karma");
				QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
				mod.setRemove(true);
				mod.setExpCost(0);
				mod.setSource(this);
				if (data.needsChoice())
					mod.setChoice(ref.getChoiceReference());
				model.addToHistory(mod);
			}
		}
			
		if (data.getType()==QualityType.POSITIVE) {
			model.setKarmaFree(model.getKarmaFree()-karma);
			model.setKarmaInvested(model.getKarmaInvested()+karma);
		} else {
			model.setKarmaFree(model.getKarmaFree()+karma);
			model.setKarmaInvested(model.getKarmaInvested()-karma);
		}
		
		parent.runProcessors();
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.common.CommonQualityController#decrease(org.prelle.shadowrun6.QualityValue)
	 */
	@Override
	public boolean decrease(QualityValue ref) {
		boolean success = super.decrease(ref);
		if (!success)
			return false;

		Quality data = ref.getModifyable();
		int karma = 0;
		boolean refund = !(Boolean)SR6ConfigOptions.QUALITIES_DONT_REFUND.getValue();
		boolean refundFromCreation = !(Boolean)SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getValue();
		logger.info("OPTION "+SR6ConfigOptions.QUALITIES_DONT_REFUND.getLocalId()+" = "+SR6ConfigOptions.QUALITIES_DONT_REFUND.getValue());
		logger.info("OPTION "+SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getLocalId()+" = "+SR6ConfigOptions.QUALITIES_REFUND_FROM_CREATION.getValue());

		// Is this simply an undo?
		QualityModification undo = getLatestModification(ref);
		if (undo!=null) {
			// This is an undo of a quality selected during character advancement
			
			if (undo.getSource()==this) {
				// Undo something from this session
				if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
					// Undo a negative quality added during this session 
					// --> No need to repay karma
					model.removeFromHistory(undo);
					logger.info("Negative quality '"+ref.getModifyable().getId()+"'had been increased during this session - no refund");
				} else {
					// Undo a positive quality added during this session
					// --> refund karma
					karma = undo.getExpCost();
					model.setKarmaFree(model.getKarmaFree()+karma);
					model.setKarmaInvested(model.getKarmaInvested()-karma);
					model.removeFromHistory(undo);
					logger.info("Positive quality '"+ref.getModifyable().getId()+"'had been bought increased this session - refund "+karma+" karma");
				}
			} else {
				// Undo something from a previous session
				karma = undo.getExpCost();
				if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
					// This negative quality has been added some sessions ago - without gaining karma
					// Usually this requires Cost*2 karma to remove
					if (refund) {
						// --> No need to pay for removal
						model.removeFromHistory(undo);
						logger.info("Negative quality '"+ref.getModifyable().getId()+"'had been bought during a past session - but due to 'DoRefund' no karma needs to be payed for removal");
					} else {
						// Pay regular cost to remove it
						karma = getKarmaCost(ref);
						// Log in history
						QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
						mod.setRemove(true);
						mod.setExpCost(karma);
						mod.setSource(this);
						if (data.needsChoice())
							mod.setChoice(ref.getChoiceReference());
						model.addToHistory(mod);
						logger.info("Negative quality '"+ref.getModifyable().getId()+"' had been bought during a past session - pay twice Karma cost for removal");
					}						
				} else {
					// This positive quality has been added some sessions ago - by paying karma
					// Usually this would not give any karma refund, when losing it
					if (refund) {
						// Refund karma spent for this positive quality
						logger.info("Positive quality '"+ref.getModifyable().getId()+"' had been bought during a past session - but due to 'DoRefund' Karma gets refunded");
						karma = undo.getExpCost();
						model.setKarmaFree(model.getKarmaFree()+karma);
						model.setKarmaInvested(model.getKarmaInvested()-karma);
						model.removeFromHistory(undo);
					} else {
						// Simply loose quality without Karma refund
						QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
						mod.setRemove(true);
						mod.setExpCost(0);
						mod.setSource(this);
						if (data.needsChoice())
							mod.setChoice(ref.getChoiceReference());
						model.addToHistory(mod);
						logger.info("Positive quality '"+ref.getModifyable().getId()+"' had been bought during a past session - gain nothing from removal");
					}
				}
			}
		} else {
			// This is an undo of a quality selected during character generation
			if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
				// This negative quality has been added on generation - with gaining karma
				// Usually this requires normal karma to remove
				if (refundFromCreation) {
					// Regain karma
					karma = ref.getModifyable().getCost() * ref.getPoints();
					model.setKarmaFree(model.getKarmaFree()+karma);
					model.setKarmaInvested(model.getKarmaInvested()-karma);
					logger.info("Negative quality '"+ref.getModifyable().getId()+"' from creation - 'Refund from creation' is active, so get karma");
				} else {
					karma = getKarmaCost(ref);
					// Log in history
					QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
					mod.setRemove(true);
					mod.setExpCost(karma);
					mod.setSource(this);
					if (data.needsChoice())
						mod.setChoice(ref.getChoiceReference());
					model.addToHistory(mod);
					logger.info("Negative quality '"+ref.getModifyable().getId()+"' from creation - pay twice Karma cost for removal");
				}						
			} else {
				// This positive quality has been picked on creation - by paying karma
				// Usually this would not give any karma refund, when losing it
				if (refundFromCreation) {
					// Refund karma spent for this positive quality
					logger.info("Positive quality '"+ref.getModifyable().getId()+"' from creation - but due to 'Refund from creation' Karma gets refunded");
					karma = ref.getModifyable().getCost() * ref.getPoints();
					model.setKarmaFree(model.getKarmaFree()+karma);
					model.setKarmaInvested(model.getKarmaInvested()-karma);
					// Log it
					QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
					mod.setRemove(true);
					mod.setExpCost(-karma);
					mod.setSource(this);
					if (data.needsChoice())
						mod.setChoice(ref.getChoiceReference());
					model.addToHistory(mod);
				} else {
					// Simply loose quality without Karma refund
					QualityModification mod = new QualityModification(ref.getModifyable(), ref.getPoints());
					mod.setRemove(true);
					mod.setExpCost(0);
					mod.setSource(this);
					if (data.needsChoice())
						mod.setChoice(ref.getChoiceReference());
					model.addToHistory(mod);
					logger.info("Positive quality '"+ref.getModifyable().getId()+"' from creation - gain nothing from removal");
				}
			}
		}

		logger.debug("Karma = "+karma);
		if (ref.getModifyable().getType()==QualityType.NEGATIVE) {
			model.setKarmaFree(model.getKarmaFree()-karma);
			model.setKarmaInvested(model.getKarmaInvested()+karma);
			logger.info("Pay "+karma+" to decrease negative quality: "+ref);
		} else if (getLatestModification(ref)!=null) {
		} else if (refund) {
			// TODO: refund for removing positive quality
			model.setKarmaFree(model.getKarmaFree()+karma);
			model.setKarmaInvested(model.getKarmaInvested()-karma);
			logger.info("Getting "+karma+" to decrease positive quality: "+ref);
		}
		
		parent.runProcessors();
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			available.clear();
			todos.clear();
			decisions.clear();
			
			// Qualities selected by metatype or magic/resonance choice
			for (Modification tmp : previous) {
				if (tmp instanceof QualityModification) {
					Quality data = ((QualityModification)tmp).getModifiedItem();
					QualityValue ref = new QualityValue(data, 1);
					if (((QualityModification)tmp).getValue()>1)
						ref.setPoints(((QualityModification)tmp).getValue());
					if (data.needsChoice()) {
						logger.warn("   TODO: implement choices: "+data.getSelect()+" for quality "+data);
//						switch (data.getSelect()) {
//						
//						}
//						DecisionToMake decide = new DecisionToMake(data.get)
					}
					logger.info("Add quality "+data.getId()+" "+((data.getMax()>1)?ref.getPoints():"")+" from "+tmp.getSource());

					ref.recalculateModifications();
					
					
					model.addRacialQuality(ref);
				} else
					unprocessed.add(tmp);
			}
			
			/*
			 * Determine available qualities
			 */
			for (Quality qual : ShadowrunCore.getQualities()) {
				// qualities that are singular selectable, must be checked
				if (!qual.isMultipleSelectable()) {
					// Check if user already has it - if so, skip it
					if (model.getQuality(qual.getId())!=null)
						continue;
				}
				// Can it be selected
				if (qual.isFreeSelectable() && !qual.isMetagenetic() && canBeSelected(qual))
					available.add(qual);
			}
			
			/*
			 * See if all qualities that need a choice, have one
			 */
			for (QualityValue qual : model.getQualities()) {
				Quality q = qual.getModifyable();
				if (qual.getModifyable().needsChoice()) {
					if (q.getSelect()==ChoiceType.NAME && qual.getDescription()==null) {
						todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "todo.qualities.needsChoice", qual.getModifyable().getName())));
						logger.warn("Choice missing for "+qual.getName());
					} else
					if (q.getSelect()!=ChoiceType.NAME && qual.getChoiceReference()==null) {
						todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "todo.qualities.needsChoice", qual.getModifyable().getName())));
						logger.warn("Choice missing for "+qual.getName());
					}
					// Special handling for mentor spirits and mystical adepts
					if (q.getSelect()==ChoiceType.MENTOR_SPIRIT && model.getMagicOrResonanceType().paysPowers()) {
						if (qual.getMysticAdeptUses()==null) {
							todos.add(new ToDoElement(Severity.WARNING, Resource.format(RES, "todo.qualities.needsChoiceMystical", qual.getModifyable().getName())));
							logger.warn("Mystical adepts must choose for mentor spirit");
						}
					}
				}
			}
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

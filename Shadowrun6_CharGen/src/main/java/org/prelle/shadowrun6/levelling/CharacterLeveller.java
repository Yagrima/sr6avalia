/**
 *
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ComplexFormController;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.charctrl.LifestyleController;
import org.prelle.shadowrun6.charctrl.MetamagicOrEchoController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.charctrl.SummonableController;
import org.prelle.shadowrun6.chargen.CommonMetaMagicOrEchoController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.WizardPageType;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigContainer;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CharacterLeveller implements CharacterController {

	private final static Logger logger = LogManager.getLogger("shadowrun6.lvl");

	private ShadowrunCharacter model;
	private List<Modification> undoList;

	private AttributeController attrib;
	private QualityController qualities;
	private SkillController skills;
	private SpellController spells;
	private SpellController alchemy;
	private RitualController rituals;
	private AdeptPowerController powers;
	private ComplexFormController cforms;
	private EquipmentController equip;
	private ConnectionsController connections;
	private SINController sins;
	private LifestyleController lifestyles;
	private SummonableController summonables;
	private CommonMetaMagicOrEchoController metaEchoes;
	private FocusController foci;

	protected List<CharacterProcessor> processChain;

	protected List<Modification> unitTestModifications;
	
	private boolean ignoreRun;

	//-------------------------------------------------------------------
	public CharacterLeveller(ShadowrunCharacter model) {
		this.model = model;
		undoList  = new ArrayList<Modification>();
		processChain = new ArrayList<>();
		unitTestModifications = new ArrayList<>();

		attrib = new KarmaAttributeLeveller(this);
		qualities = new QualityLeveller(this);
		skills = new KarmaSkillLeveller(this);
		spells = new KarmaSpellController(this, false);
		alchemy = new KarmaSpellController(this, true);
		rituals= new KarmaRitualController(this);
		powers = new AdeptPowerLeveller(this);
		cforms = new KarmaComplexFormController(this);
		equip  = new EquipmentLeveller(this, model);
		connections = new ConnectionLeveller(this);
		sins   = new SINLeveller(this);
		lifestyles = new LifestyleLeveller(this);
		summonables = new NoCostSummonableController(model);
		metaEchoes  = new CommonMetaMagicOrEchoController(this, CharGenMode.LEVELING);
		foci   = new FocusLeveller(this);

		processChain.clear();
//		processChain.add( new ResetModifications());
//		processChain.add( new ResolveChoicesInReferences() );
//		processChain.add( new GetModificationsFromMetaType() );  // Add modifications from metatype
////		processChain.add( new GetModificationsFromMagicOrResonance() ); 
//		processChain.add( new GetModificationsFromQualities() );  // Add modifications from qualities
//		processChain.add( new GetModificationsFromPowers() );  
//		processChain.add( new GetModificationsFromEquipment());
//		processChain.add( new GetModificationsFromMetamagicOrEchoes() );
//		processChain.add( new ApplyCarriedItemModifications());
		processChain.addAll(ShadowrunTools.RECALCULATE_STEPS);
		processChain.add( (CharacterProcessor)qualities ); 
		processChain.add( (CharacterProcessor) attrib );
		processChain.add( (CharacterProcessor) skills );
		processChain.add( (CharacterProcessor) spells );
		processChain.add( (CharacterProcessor) alchemy );
		processChain.add( (CharacterProcessor) rituals );
		processChain.add( (CharacterProcessor) powers );
		processChain.add( (CharacterProcessor) cforms );
		processChain.add( (CharacterProcessor) sins );
//		processChain.add( (CharacterProcessor) equip);
		processChain.add( metaEchoes);
		processChain.add( foci);
		processChain.add( (CharacterProcessor) connections );
//		processChain.add( new CalculateDerivedAttributes());
//		processChain.add( new CalculateGenerationEssence());
//		processChain.add( new ApplyRelevanceAndEdgeMods());
////		processChain.add( nuyen);
		
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void addUnitTestModification(Modification mod) {
		unitTestModifications.add(mod);
		runProcessors();
	}

	//-------------------------------------------------------------------
	public void removeUnitTestModification(Modification mod) {
		unitTestModifications.remove(mod);
		runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#getWizardPages()
	 */
	@Override
	public WizardPageType[] getWizardPages() {
		return new WizardPageType[]{
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.CharacterGenerator#attachConfigurationTree(de.rpgframework.ConfigContainer)
	 */
	@Override
	public void attachConfigurationTree(ConfigContainer cfgShadowrun) {
		SR6ConfigOptions.attachConfigurationTree(cfgShadowrun);
		
		cfgShadowrun.addListener( (source,options) -> {
			logger.debug("Config changed: "+options);
			runProcessors();
			});
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getName()
	 */
	@Override
	public String getName() {
		return model.getName();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getCharacter()
	 */
	@Override
	public ShadowrunCharacter getCharacter() {
		return model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getMode()
	 */
	@Override
	public CharGenMode getMode() {
		return CharGenMode.LEVELING;
	}

	//--------------------------------------------------------------------
	public void updateHistory() {
		logger.info("Update character history");
		Date now = new Date(System.currentTimeMillis());
		for (Modification changed : undoList) {
			changed.setDate(now);
			model.addToHistory(changed);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attrib;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skills;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getPowerController()
	 */
	@Override
	public AdeptPowerController getPowerController() {
		return powers;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getEquipmentController()
	 */
	@Override
	public EquipmentController getEquipmentController() {
		return equip;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getComplexFormController()
	 */
	@Override
	public ComplexFormController getComplexFormController() {
		return cforms;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getConnectionController()
	 */
	@Override
	public ConnectionsController getConnectionController() {
		return connections;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSINController()
	 */
	@Override
	public SINController getSINController() {
		return sins;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getLifestyleController()
	 */
	@Override
	public LifestyleController getLifestyleController() {
		return lifestyles;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSpellController()
	 */
	@Override
	public SpellController getSpellController() {
		return spells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getAlchemyController()
	 */
	@Override
	public SpellController getAlchemyController() {
		return alchemy;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getRitualController()
	 */
	@Override
	public RitualController getRitualController() {
		return rituals;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getQualityController()
	 */
	@Override
	public QualityController getQualityController() {
		return qualities;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getSummonableController()
	 */
	@Override
	public SummonableController getSummonableController() {
		return summonables;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getMetamagicOrEchoController()
	 */
	@Override
	public MetamagicOrEchoController getMetamagicOrEchoController() {
		return metaEchoes;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.CharacterController#getFocusController()
	 */
	@Override
	public FocusController getFocusController() {
		return foci;
	}

	//-------------------------------------------------------------------
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<ToDoElement>();
		if (model==null)
			return ret;
		
		ret.addAll(attrib.getToDos());
		ret.addAll(skills.getToDos());
		ret.addAll(qualities.getToDos());
//		ret.addAll(equip.getToDos());
		ret.addAll(connections.getToDos());
		ret.addAll(lifestyles.getToDos());
		ret.addAll(metaEchoes.getToDos());
		ret.addAll(powers.getToDos());
		ret.addAll(spells.getToDos());
		ret.addAll(sins.getToDos());
		ret.addAll(metaEchoes.getToDos());
		ret.addAll(foci.getToDos());
//		if (model.getMagicOrResonanceType()!=null) {
//			if (model.getMagicOrResonanceType().usesSpells()) {
//				ret.addAll(spells.getToDos());
//			}
//			if (model.getMagicOrResonanceType().usesPowers()) {
//				ret.addAll(powers.getToDos());
//			}
//			if (model.getMagicOrResonanceType().usesResonance()) {
//				ret.addAll(cforms.getToDos());
//			}
//		}
//		ret.addAll(nuyen.getToDos());
		ret.addAll(metaEchoes.getToDos());

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ProcessorRunner#runProcessors()
	 */
	@Override
	public void runProcessors() {
		if (ignoreRun)
			return;
		
		try {
			ignoreRun = true;
			logger.info("\n\nSTART: runProcessors: "+processChain.size()+"-------------------------------------------------------");
			List<Modification> unprocessed = new ArrayList<>(unitTestModifications);
			for (CharacterProcessor processor : processChain) {
				unprocessed = processor.process(model, unprocessed);
				logger.info("------karma is "+model.getKarmaFree()+"/\u00A5 "+model.getNuyen()+" after "+processor.getClass().getSimpleName()+"     "+unprocessed);
//				logger.info("------HEAT is "+model.getAttribute(Attribute.HEAT));
			}
			logger.info("Remaining mods  = "+unprocessed);
			logger.info("Remaining karma = "+model.getKarmaFree());
			logger.info("ToDos = "+getToDos());
			logger.info("STOP : runProcessors: "+processChain.size()+"-------------------------------------------------------");
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null ));
		} finally {
			ignoreRun = false;
		}
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.levelling;

import java.util.List;

import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.common.CommonLifestyleController;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author spr
 *
 */
public class LifestyleLeveller extends CommonLifestyleController {

	//-------------------------------------------------------------------
	/**
	 * @param parent
	 */
	public LifestyleLeveller(CharacterController parent) {
		super(parent);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#addLifestyle(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void addLifestyle(LifestyleValue ref) {
		int cost = ShadowrunTools.getLifestyleCost(model, ref);
		
		logger.info("Add lifestyle for "+cost+" Nuyen: "+ref);
		model.addLifestyle(ref);
		model.setNuyen(model.getNuyen() - cost);
		
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.charctrl.LifestyleController#removeLifestyle(org.prelle.shadowrun.LifestyleValue)
	 */
	@Override
	public void removeLifestyle(LifestyleValue lifestyle) {
		model.removeLifestyle(lifestyle);
		int cost = lifestyle.getCost();
		logger.info("Removed lifestyle for "+cost+" Nuyen: "+lifestyle);
		model.setNuyen(model.getNuyen() + cost);
		
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
//		for (Lifest)
		
		return unprocessed;
	}

}

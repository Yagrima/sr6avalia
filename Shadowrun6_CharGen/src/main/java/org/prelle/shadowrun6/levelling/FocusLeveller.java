package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.FocusModification;
import org.prelle.shadowrun6.modifications.SpellModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class FocusLeveller implements CharacterProcessor, FocusController {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.genlvl");
	protected ShadowrunCharacter model;
	protected CharacterController parent;
	
	private List<ToDoElement> todos;
	private int forcePool;

	//--------------------------------------------------------------------
	/**
	 */
	public FocusLeveller(CharacterController parent) {
		this.parent = parent;
		this.model  = parent.getCharacter();
		todos      = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		// TODO Auto-generated method stub
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#getFocusPointsLeft()
	 */
	@Override
	public int getFocusPointsLeft() {
		return forcePool;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#canBeSelected(org.prelle.shadowrun6.Focus, int)
	 */
	@Override
	public boolean canBeSelected(Focus data, int rating) {
		/*
		 * You can’t bond more foci than your Magic attribute,
		 * and the maximum Force of all your bonded
		 * foci can’t exceed your Magic x 5.
		 */		
		if (model.getFoci().size()>=model.getAttribute(Attribute.MAGIC).getModifiedValue()) {
			// More than magic attribute
			System.err.println("FocusLeveller: Cannot select "+data.getId()+" because force is higher than magic "+model.getAttribute(Attribute.MAGIC).getModifiedValue());
			return false;
		}

		// maximum force
		if (rating>forcePool) {
			System.err.println("FocusLeveller: Cannot select "+data.getId()+" because force is higher than force pool "+forcePool);
			return false;
		}
		
		return !data.needsChoice();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#canBeSelected(org.prelle.shadowrun6.Focus, int, java.lang.Object)
	 */
	@Override
	public boolean canBeSelected(Focus data, int rating, Object choice) {
		/*
		 * You can’t bond more foci than your Magic attribute,
		 * and the maximum Force of all your bonded
		 * foci can’t exceed your Magic x 5.
		 */		
		if (model.getFoci().size()>=model.getAttribute(Attribute.MAGIC).getModifiedValue()) {
			// More than magic attribute
			return false;
		}
		// maximum force
		if (rating>forcePool)
			return false;
		
		if (!data.needsChoice())
			return false;
		
		try {
			if (ShadowrunTools.getChoiceReference(data.getChoice(), choice)==null) {
				logger.warn("Given choice for focus does not match type "+data.getChoice()+": "+choice);
				return false;
			}
		} catch (Exception e) {
			logger.warn("Error verifying choice type "+data.getChoice()+": "+choice);
			return false;
		}
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#canBeDeselected(org.prelle.shadowrun6.FocusValue)
	 */
	@Override
	public boolean canBeDeselected(FocusValue data) {
		return model.getFoci().contains(data);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#select(org.prelle.shadowrun6.Focus, int)
	 */
	@Override
	public FocusValue select(Focus data, int rating) {
		if (!canBeSelected(data, rating)) 
			return null;
		
		FocusValue val = new FocusValue(data, rating);
		model.addFocus(val);
		logger.info("Selected focus "+val);
		
		payFocus(val);
		
		parent.runProcessors();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#select(org.prelle.shadowrun6.Focus, int, java.lang.Object)
	 */
	@Override
	public FocusValue select(Focus data, int rating, Object choice) {
		if (!canBeSelected(data, rating, choice)) 
			return null;
		
		FocusValue val = new FocusValue(data, rating);
		val.setChoice(choice);
		val.setChoiceReference(ShadowrunTools.getChoiceReference(data.getChoice(), choice));
		model.addFocus(val);
		logger.info("Selected focus "+val);
		
		payFocus(val);
		
		parent.runProcessors();
		return val;
	}

	//--------------------------------------------------------------------
	private void payFocus(FocusValue val) {
		int karma = val.getCostKarma();
		model.setKarmaFree(model.getKarmaFree() - val.getCostKarma());
		model.setKarmaInvested(model.getKarmaInvested() + val.getCostKarma());
		boolean payGear = (Boolean)SR6ConfigOptions.PAY_GEAR.getValue();
		if (payGear) {
			model.setNuyen(model.getNuyen() - val.getCostNuyen());
			logger.info("Pay "+karma+" karma and "+val.getCostNuyen()+" nuyen for focus "+val.getModifyable());
		} else {
			logger.info("Pay "+karma+" karma and no nuyen (per user config) for focus "+val.getModifyable());
		}

		// Make undoable
		FocusModification mod = new FocusModification(val.getModifyable(), 1);
		mod.setChoice(val.getChoiceReference());
		mod.setChoicesChoice(val.getChoicesChoiceReference());
		mod.setExpCost(val.getCostKarma());
		if (val.getUniqueId()!=null)
			mod.setFocusUUID(val.getUniqueId().toString());
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);
	}

	//-------------------------------------------------------------------
	private FocusModification getModification(FocusValue sVal) {
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof FocusModification))
				continue;
			FocusModification amod = (FocusModification)mod;
			// Both UUIDs match
			if (sVal.getUniqueId()!=null && amod.getFocusUUID()!=null && amod.getFocusUUID().equals(sVal.getUniqueId().toString()))
				return amod;
			// Skip if not identical type of focus
			if (amod.getModifiedItem()!=sVal.getModifyable())
				continue;
			// Must compare choice?
			if (sVal.getModifyable().needsChoice()) {
				if (!sVal.getChoiceReference().equals(amod.getChoice()))
					continue;
				// What about the 2nd level chocie?
			}
			return amod;
		}
		return null;
	}


	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.FocusController#deselect(org.prelle.shadowrun6.FocusValue)
	 */
	@Override
	public boolean deselect(FocusValue data) {
		if (!canBeDeselected(data))
			return false;

		logger.info("Deselected focus "+data);
		model.removeFocus(data);

		boolean refundCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		boolean refundCareer   = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		FocusModification mod = getModification(data);
		
		int cost = 0;
		int nuyen= 0;
		if (mod==null) {
			// Focus was chosen during creation
			cost = refundCreation?data.getCostKarma():0;
		} else if (mod.getExpCost()==0) {
			// Focus was granted for free
		} else if (mod.getSource()==this) {
			// Focus was selected this session
			cost = mod.getExpCost();
			nuyen = data.getCostNuyen();
		} else {
			// Spell was selected during a previous career session
			cost = refundCareer?data.getCostKarma():0;
			nuyen = data.getCostNuyen();
		}
		logger.debug("Refund "+cost+" karma and "+nuyen+" karma   - refundCreation="+refundCreation+", refundCareer="+refundCareer);
		
		if (mod!=null) {
			model.removeFromHistory(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);
		// Refund nuyen
		model.setNuyen(model.getNuyen()+nuyen);

		parent.runProcessors();
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		this.model = model;
		try {
			forcePool = model.getAttribute(Attribute.MAGIC).getModifiedValue()*5;
			
			// Pay from force pool
			for (FocusValue focus : model.getFoci()) {

				forcePool -= focus.getLevel();
			}
			logger.info("Force pool for foci is "+forcePool);
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");			
		}
		return unprocessed;
	}

}

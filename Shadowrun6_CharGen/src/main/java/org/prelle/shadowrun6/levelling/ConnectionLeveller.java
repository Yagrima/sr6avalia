/**
 * 
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ConnectionLeveller implements ConnectionsController, CharacterProcessor {

	protected static final Logger logger = LogManager.getLogger("shadowrun6.lvl");
	
	private CharacterController parent; 
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	/**
	 */
	public ConnectionLeveller(CharacterController parent) {
		this.parent = parent;
		this.model = parent.getCharacter();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canCreateConnection()
	 */
	@Override
	public boolean canCreateConnection() {
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#createConnection()
	 */
	@Override
	public Connection createConnection() {
		logger.info("Add new connection");
		Connection data = new Connection(null, null, 1, 1);
		model.addConnection(data);
		
		parent.runProcessors();
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#removeConnection(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public void removeConnection(Connection con) {
		logger.info("Remove connection");
		model.removeConnection(con);
		
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canIncreaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canIncreaseInfluence(Connection con) {
		return con.getInfluence()<12;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#increaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean increaseInfluence(Connection con) {
		if (!canIncreaseInfluence(con))
			return false;
		
		con.setInfluence(con.getInfluence()+1);
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canDecreaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canDecreaseInfluence(Connection con) {
		return con.getInfluence()>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#decreaseInfluence(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean decreaseInfluence(Connection con) {
		if (!canDecreaseInfluence(con))
			return false;
		
		con.setInfluence(con.getInfluence()-1);
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canIncreaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canIncreaseLoyalty(Connection con) {
		return con.getLoyalty()<6;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#increaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean increaseLoyalty(Connection con) {
		if (!canIncreaseLoyalty(con))
			return false;
		
		con.setLoyalty(con.getLoyalty()+1);
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#canDecreaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean canDecreaseLoyalty(Connection con) {
		return con.getLoyalty()>1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#decreaseLoyalty(org.prelle.shadowrun6.Connection)
	 */
	@Override
	public boolean decreaseLoyalty(Connection con) {
		if (!canDecreaseLoyalty(con))
			return false;
		
		con.setLoyalty(con.getLoyalty()-1);
		parent.runProcessors();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		List<ToDoElement> ret = new ArrayList<ToDoElement>();
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.ConnectionsController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		// TODO Auto-generated method stub
		return unprocessed;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.LicenseType;
import org.prelle.shadowrun6.LicenseValue;
import org.prelle.shadowrun6.LifestyleValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.SIN.Quality;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.SINController;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SINLeveller implements SINController {

	private final static int PRICE_SIN_LEVEL = 2500;
	private final static int PRICE_LICENSE_LEVEL = 200;
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.lvl");
	protected ShadowrunCharacter model;
	private ProcessorRunner parent;

	//-------------------------------------------------------------------
	public SINLeveller(CharacterController parent) {
		this.parent = parent;
		this.model = parent.getCharacter();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public boolean canCreateNewSIN(Quality quality) {
		if (quality==Quality.REAL_SIN)
			return false;
		
		int cost = quality.getValue()*PRICE_SIN_LEVEL;
		if (model.getNuyen()<cost)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canCreateNewSIN(org.prelle.shadowrun6.SIN.Quality, int)
	 */
	@Override
	public boolean canCreateNewSIN(Quality quality, int count) {
		if (quality==Quality.REAL_SIN)
			return false;
		
		int cost = quality.getValue()*PRICE_SIN_LEVEL * count;
		if (model.getNuyen()<cost)
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canDeleteSIN(org.prelle.shadowrun6.SIN)
	 */
	@Override
	public boolean canDeleteSIN(SIN data) {
		if (data.getQuality()==Quality.REAL_SIN)
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#createNewSIN(java.lang.String, org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public SIN createNewSIN(String name, Quality quality) {
		if (!canCreateNewSIN(quality))
			return null;
		
		SIN sin = new SIN(quality);
		sin.setName(name);
		model.addSIN(sin);
		logger.info("Added new SIN: "+name+"   Quality="+quality);
		
		parent.runProcessors();
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		
		return sin;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#createNewSIN(java.lang.String, org.prelle.shadowrun6.SIN.Quality, int)
	 */
	@Override
	public SIN[] createNewSIN(String name, Quality quality, int count) {
		if (!canCreateNewSIN(quality, count))
			return null;
		
		SIN[] ret = new SIN[count];
		for (int i=1; i<=count; i++) {
			SIN sin = new SIN(quality);
			sin.setName(name+" "+i);
			model.addSIN(sin);
			ret[i-1]=sin;
		}
		parent.runProcessors();
		
//		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.NUYEN_RECALC_NECESSARY,null));
		
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#deleteSIN(org.prelle.shadowrun6.SIN)
	 */
	@Override
	public boolean deleteSIN(SIN data) {
		if (!canDeleteSIN(data)) {
			logger.warn("Cannot delete SIN: "+data);
			return false;
		}
		
		model.removeSIN(data);
		// Remove all licenses
		for (LicenseValue lic : model.getLicenses()) {
			if (lic.getSIN().equals(data.getUniqueId())) {
				logger.info("Delete license '"+lic.getName()+"' because SIN for "+data.getName()+"/"+data.getUniqueId()+" has been deleted");
				model.removeLicense(lic);
			}
		}
		// Remove all lifestyles
		for (LifestyleValue life : model.getLifestyle()) {
			if (data.getUniqueId().equals(life.getSIN())) {
				logger.info("Delete lifestyle '"+life.getName()+"' because SIN for "+data.getName()+"/"+data.getUniqueId()+" has been deleted");
				model.removeLifestyle(life);
			}
		}
		
		parent.runProcessors();
		
		return true;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#canCreateNewLicense(org.prelle.shadowrun6.LicenseType, org.prelle.shadowrun6.SIN, org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public boolean canCreateNewLicense(LicenseType type, SIN sin, Quality quality) {
		if (type==null) return false;
		if (sin==null) return false;
		if (quality==null) return false;
		int cost = quality.getValue()*PRICE_LICENSE_LEVEL;
		if (model.getNuyen()<cost)
			return false;
		
		if (sin.getQuality().ordinal()<quality.ordinal())
			return false;
		
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SINController#createNewLicense(java.lang.String, org.prelle.shadowrun6.SIN.Quality)
	 */
	@Override
	public LicenseValue createNewLicense(LicenseType type, SIN sin, Quality quality) {
		if (!canCreateNewLicense(type, sin, quality))
			return null;
		
		int cost = quality.getValue()*PRICE_LICENSE_LEVEL;
		
		LicenseValue lic = new LicenseValue();
		lic.setRating(quality);
		lic.setType(type);
		lic.setSIN(sin.getUniqueId());
		model.addLicense(lic);
		logger.info("Added license "+sin+" for "+cost+" nuyen");
		
		// Buy
		model.setNuyen(model.getNuyen() - cost);
		
		parent.runProcessors();
		
		return lic;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> unprocessed) {
		logger.trace("START: process");
		try {

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

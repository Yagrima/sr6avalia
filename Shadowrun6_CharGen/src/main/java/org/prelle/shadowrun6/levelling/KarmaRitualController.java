/**
 * 
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.RitualValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.RitualModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaRitualController implements RitualController, CharacterProcessor {
	
	private static Logger logger = LogManager.getLogger("shadowrun6.lvl");

	private ShadowrunCharacter model;
	protected ProcessorRunner parent;

	protected List<Ritual> recRituals;
	protected List<Ritual> avRituals;
	protected List<ToDoElement> todos;
	
	//-------------------------------------------------------------------
	/**
	 */
	public KarmaRitualController(ProcessorRunner parent) {
		this.parent = parent;
		todos      = new ArrayList<>();
		avRituals = new ArrayList<>();
		recRituals = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	protected void updateAvailable() {
		logger.debug("START updateAvailable");
		
		avRituals.clear();
		avRituals.addAll(ShadowrunCore.getRituals());
		// Remove those the character already has
		for (RitualValue val : model.getRituals()) {
			logger.trace("Not available anymore "+val);
			avRituals.remove(val.getModifyable());
		}
		
		Collections.sort(avRituals);

		logger.debug("STOP updateAvailable");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#getRitualsLeft()
	 */
	@Override
	public int getRitualsLeft() {
		return -1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#getAvailableRituals()
	 */
	@Override
	public List<Ritual> getAvailableRituals() {
		return avRituals;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#canBeSelected(org.prelle.shadowrun6.Ritual)
	 */
	@Override
	public boolean canBeSelected(Ritual spell) {
		// Not already selected
		for (RitualValue tmp : model.getRituals()) {
			if (tmp.getModifyable()==spell)
				return false;
		}
		
		// Enough karma?
		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#canBeSelected(org.prelle.shadowrun6.Ritual)
	 */
	@Override
	public boolean canBeDeselected(RitualValue spell) {
		// Minimum requirement is that the spell is known
		if (!model.getRituals().contains(spell))
			return false;
		
		boolean allowUndoCareer   = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER.getValue();
		boolean allowUndoCreation = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.getValue();
		RitualModification mod = getModification(spell);
				
		if (mod==null) {
			return allowUndoCreation;
		} else if (mod.getSource()==this) {
			// This career session - always allowed
			return true;
		} else
			return allowUndoCareer;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.RitualController#select(org.prelle.shadowrun6.Ritual)
	 */
	@Override
	public RitualValue select(Ritual ritual) {
		if (!canBeSelected(ritual)) {
			logger.warn("Trying to select ritual "+ritual+" which cannot be selected");
			return null;
		}

		int cost = 5;
		logger.debug("select "+ritual+" for "+cost+" karma");

		// Change model
		logger.info("Selected ritual "+ritual);
		RitualValue sVal = new RitualValue(ritual);
		model.addRitual(sVal);

		// Make undoable
		RitualModification mod = new RitualModification(ritual);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		parent.runProcessors();
		return sVal;
	}

	//-------------------------------------------------------------------
	private RitualModification getModification(RitualValue sVal) {
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof RitualModification))
				continue;
			RitualModification amod = (RitualModification)mod;
			if (amod.getRitual()!=sVal.getModifyable())
				continue;
			return amod;
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public void deselect(RitualValue ref) {
		logger.debug("deselect "+ref);
		if (!canBeDeselected(ref))
			return;

		boolean refundCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		boolean refundCareer   = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		RitualModification mod = getModification(ref);
		
		int cost = 0;
		if (mod==null) {
			// Spell was chosen during creation
			cost = refundCreation?5:0;
		} else if (mod.getExpCost()==0) {
			// Spell was granted for free
		} else if (mod.getSource()==this) {
			// Spell was selected this session
			cost = mod.getExpCost();
		} else {
			// Spell was selected during a previous career session
			cost = refundCareer?5:0;
		}
		logger.debug("Refund "+cost+" karma    - refundCreation="+refundCreation+", refundCareer="+refundCareer);
		
		if (mod!=null) {
			model.removeFromHistory(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(
				SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER, 
				SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION, 
				SR6ConfigOptions.REFUND_FROM_CAREER, 
				SR6ConfigOptions.REFUND_FROM_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			updateAvailable();
		} finally {
			
		}
		return unprocessed;
	}

}

package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.common.CommonSkillController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.modifications.AllowModification;
import org.prelle.shadowrun6.modifications.SkillModification;
import org.prelle.shadowrun6.modifications.SkillSpecializationModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaSkillLeveller extends CommonSkillController implements SkillController, CharacterProcessor {

	private static Logger logger = LogManager.getLogger("shadowrun6.lvl.skill");

	//-------------------------------------------------------------------
	public KarmaSkillLeveller(CharacterController parent) {
		super(parent);
		this.model  = parent.getCharacter();
	}

	//-------------------------------------------------------------------
	public void setDontRefund(boolean value) { logger.info("OPTION 'Don't refund'="+value); SR6ConfigOptions.REFUND_FROM_CAREER.set(!value);  }
	public void setDecreaseBelowCreation(boolean value) { logger.info("OPTION 'Decrease below creation'="+value); SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.set(value); }
	public void setRefundBelowCreation(boolean value) { logger.info("OPTION 'Refund below creation'="+value); SR6ConfigOptions.REFUND_FROM_CREATION.set(value) ; }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getPointsLeftSkills()
	 */
	@Override
	public int getPointsLeftSkills() {
		return 0;
	}

	//-------------------------------------------------------------------
	private SkillModification getHighestModification(Skill key) {
		SkillModification ret = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof SkillModification))
				continue;
			SkillModification amod = (SkillModification)mod;
			if (amod.getSkill()!=key)
				continue;
			
			if (mod.getSource()!=this && mod.getSource()!=null)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeDecreased(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean canBeDecreased(SkillValue val) {
		// Knowledge skills have no level
		if (val.getModifyable().getType()==SkillType.KNOWLEDGE) 
			return false;
		// Language skills cannot be decreased below 1
		if (val.getModifyable().getType()==SkillType.KNOWLEDGE) 
			return false;
		// Is automatically added
		if (model.isAutoSkill(val))
			return false;

		SkillModification toUndo = getHighestModification(val.getModifyable());
		if (toUndo!=null && val.getStart()<val.getPoints()) {
			if (toUndo.getSource()!=null)
				// Has been increased this session
				return true;
			// Has been increased in career mode
			return (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER.getValue();
		} else {
			// Has been increased in generation mode
			boolean decreaseBelowCreation = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.getValue();
			return (val.getPoints()>0 && decreaseBelowCreation);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#canBeIncreased(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canBeIncreased(SkillValue val) {
		// Knowledge skills have no level
		if (val.getModifyable().getType()==SkillType.KNOWLEDGE) 
			return false;
		// Enough karma?
		if (model.getKarmaFree()<getIncreaseCost(val.getModifyable()))
			return false;

		int max = 9+val.getMaximumModifier();
		if (val.getModifyable().getType()==SkillType.LANGUAGE) 
			max=3;
		if (val.getPoints()>=max)
			return false;

		// Skills for which groups character is incompetent, cannot be increased
		for (QualityValue qual : model.getQualities()) {
			if (qual.getModifyable().getId().equals("incompetent") && val.getModifyable()==qual.getChoice()) {
				return false;
			}
		}
		// Is automatically added
		if (model.isAutoSkill(val))
			return false;
			
		return true;
	}

	//-------------------------------------------------------------------
	public int getIncreaseCost(Skill key) {
		SkillValue sVal = model.getSkillValue(key);
		int newVal = (sVal==null)?1:(sVal.getPoints()+1);

		// Raising language skills is like a new specialization
		if (key.getType()==SkillType.KNOWLEDGE) 
			return 3;
		if (key.getType()==SkillType.LANGUAGE) {
			logger.error("Calling getIncreaseCost(Skill) for language skill does not work");
			return newVal*3;
		}

		int cost = newVal*5;
		
		return cost;
	}

	//-------------------------------------------------------------------
	private int getIncreaseCost(SkillValue sVal) {
		Skill key = sVal.getModifyable();
		int newVal = (sVal==null)?1:(sVal.getPoints()+1);

		// Raising language skills is like a new specialization
		if (key.getType()==SkillType.KNOWLEDGE) 
			return 3;
		if (key.getType()==SkillType.LANGUAGE) {
			return newVal*3;
		}

		int cost = newVal*5;
		
		return cost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#select(org.prelle.shadowrun6.Skill)
	 */
	@Override
	public SkillValue select(Skill skill) {
		logger.debug("Select "+skill);
		if (!canBeSelected(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}

		int cost = getIncreaseCost(skill);
		logger.debug("select "+skill+" for "+cost+" karma");

		// Change
		SkillValue val = super.select(skill);
		if (val==null)
			return null;

		// Make undoable
		SkillModification mod = new SkillModification(skill, val.getPoints());
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);
		parent.runProcessors();

		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canSpecializeIn(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean canSpecializeIn(SkillValue val) {
		return !val.getModifyable().getSpecializations().isEmpty() && model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean deselect(SkillValue ref) {
		logger.info("deselect "+ref);
		boolean deselected = super.deselect(ref);
		if (!deselected)
			return deselected;
		
		boolean refundFromCareer = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		boolean refundFromCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		Skill key = ref.getModifyable();
		SkillModification toUndo = getHighestModification(ref.getModifyable());
		int karma = (key.getType()==SkillType.KNOWLEDGE || key.getType()==SkillType.LANGUAGE)?3:5;
		if (toUndo!=null) {
			// Decrease from career
			model.removeFromHistory(toUndo);
			karma = toUndo.getExpCost();
			
			if (refundFromCareer) {
				logger.info("Deselecting"+key+" from prev. career session grants "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
			} else {
				logger.info("Removing "+key+" from prev. career session without granting "+karma);
				SkillModification mod = new SkillModification(key, ref.getPoints());
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		} else {
			// Deselect from creation value
			logger.debug("Remove a skill that has chosen at creation - invested karma was "+karma);
			if (refundFromCreation) {
				logger.info("Removing "+key+" from creation session to "+ref.getPoints()+" granting "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
				// If it granted Karma, log it
				SkillModification mod = new SkillModification(key, model.getSkillValue(key).getPoints()+1);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			} else {
				SkillModification mod = new SkillModification(key, -1);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		}

		// Inform listener
		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public boolean deselect(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		logger.info("TRY: Deselect expertise="+expertise+" "+spec+" from "+ref);
		if (!canBeDeselected(ref, spec, expertise)) {
			logger.warn("Trying to deselect "+spec+" with cannot be done");
			return false;
		}
		
		logger.info("Deselect specialization "+spec+" from "+ref);
		super.deselect(ref, spec, expertise);
		SkillSpecializationModification toUndo = getHighestSpecializationModification(ref.getModifyable(), spec);
		logger.debug("remove from history: "+toUndo);
		model.removeFromHistory(toUndo);

		// Pay karma
		int cost = toUndo.getExpCost(); // getIncreaseCost(ref.getModifyable());
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		// Inform listener
		parent.runProcessors();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#select(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public SkillSpecializationValue select(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		if (!canBeSelected(ref, spec, expertise)) {
			logger.warn("Trying to select specialization "+spec+" which cannot be selected");
			return null;
		}

		int cost = 5;

		try {
			throw new RuntimeException("Trace");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("  select specialization "+spec+" in skill "+ref);
		SkillSpecializationValue val = super.select(ref, spec, expertise);

		// Make undoable
		SkillSpecializationModification mod = new SkillSpecializationModification(ref.getModifyable(), spec);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setExpertise(expertise);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);
		SkillSpecializationModification toUndo = getHighestSpecializationModification(ref.getModifyable(), spec);
		if (mod!=toUndo) {
			logger.error("Should be same modifications: "+mod+"   vs. "+toUndo);
		}

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		parent.runProcessors();
		
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#increase(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean increase(SkillValue ref) {
		if (!canBeIncreased(ref)) {
			logger.warn("Trying to increase skill "+ref+" which cannot be increased");
			return false;
		}

		int cost = getIncreaseCost(ref);
		logger.debug("try increase "+ref+" for "+cost+" karma");
		if (!super.increase(ref))
			return false;

		// Make undoable
		SkillModification mod = new SkillModification(ref.getModifyable(), ref.getPoints());
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		parent.runProcessors();

		return true;
	}

	//-------------------------------------------------------------------
	private SkillSpecializationModification getHighestSpecializationModification(Skill key, SkillSpecialization spec) {
		SkillSpecializationModification ret = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof SkillSpecializationModification))
				continue;
			SkillSpecializationModification amod = (SkillSpecializationModification)mod;
			if (amod.getSkillSpecialization()==spec)
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#decrease(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean decrease(SkillValue ref) {
		logger.debug("try decrease "+ref);
		if (!super.decrease(ref))
			return false;

		boolean refundFromCareer = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		boolean refundFromCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		Skill key = ref.getModifyable();
		int karma = getIncreaseCost(key);
		SkillModification toUndo = getHighestModification(key);
		if (toUndo!=null) {
			// Decrease from career
			model.removeFromHistory(toUndo);
			karma = toUndo.getExpCost();
			
			if (refundFromCareer) {
				logger.info("Decreased "+key+" from prev. career session to "+ref.getPoints()+" granting "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
			} else {
				logger.info("Decreased "+key+" from prev. career session to "+ref.getPoints()+" without granting "+karma);
				SkillModification mod = new SkillModification(key, -1);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		} else {
			// Decreasing below the creation value
			logger.debug("Decrease a value that has chosen at creation - invested karma was "+karma);
			if (refundFromCreation) {
				logger.info("Decreased "+key+" from creation session to "+ref.getPoints()+" granting "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
				// If it granted Karma, log it
				SkillModification mod = new SkillModification(key, model.getSkillValue(key).getPoints()+1);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			} else {
				SkillModification mod = new SkillModification(key, -1);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		}

		// Inform listener
		parent.runProcessors();

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return new ArrayList<ToDoElement>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeSelected(org.prelle.shadowrun6.Skill)
	 */
	@Override
	public boolean canBeSelected(Skill skill) {
		if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE)
			return model.getKarmaFree()>=3;
		
		// Not already selected
		boolean found = model.getSkillValues().stream().anyMatch(val -> val.getModifyable()==skill);
		if (found)
			return false;
		// Enough karma?
		return model.getKarmaFree()>=getIncreaseCost(skill);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeSelected(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public boolean canBeSelected(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		logger.debug("canBeSelected("+ref+"), spec="+spec+"   expert="+expertise);
		// Skill should exist in Character
		if (model.getSkillValue(ref.getModifyable())==null)
			return false;
		// Characters SkillValue should match given skill value
		if (model.getSkillValue(ref.getModifyable())!=ref) {
//			logger.debug("Can't because char does not have skill");
			return false;
		}
		// Not possible for knowledge skills
		if (ref.getModifyable().getType()==SkillType.KNOWLEDGE)
			return false;
		
		// Value should be at least 1 - or 5 for expertises
		if (ref.getPoints()< (expertise?5:1)) {
//			logger.debug("Can't because minimal value not met");
			return false;
		}
		
		/*
		 * Exotic weapons are exempt from normal rules
		 */
		if (ref.getModifyable().getId().equals("exotic_weapons")) {
			if (expertise)
				return false;
			// Not have the same twice
			if (model.getSkillValue(ShadowrunCore.getSkill("exotic_weapons")).hasSpecialization(spec)) {
				logger.warn("Already have this specialization");
				return false;
			}
		} else {

			/*
			 * You may only have 1 special. and 1 expertise
			 * */
			boolean[] hasSE = hasSpecialAndExpertise(ref);
			boolean hasSpecialization =hasSE[0];
			boolean hasExpertise = hasSE[1];
			if (hasSpecialization && hasExpertise) {
//			logger.debug("Can't because already has special and expertise");
				return false;
			}
			/*
			 * Before another specialization can be used, a previous one 
			 * has to be converted to a expertise
			 */
			if (hasSpecialization && !expertise) {
//			logger.debug("Can't because must convert existing specialization to expertise first");
				return false;
			}

			// Specialization should not exist yet
			if (expertise) {
				SkillSpecializationValue sVal = ref.getSpecialization(spec);			
				if (sVal==null) return false; // Requires existing specialization
				if (sVal.isExpertise()) return false; // Already has expertise
			} else {
				if (ref.hasSpecialization(spec))
					return false;
			}
		}

		// Requires 5 karma
		return model.getKarmaFree()>=5;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#selectKnowledgeOrLanguage(org.prelle.shadowrun6.Skill, java.lang.String)
	 */
	@Override
	public SkillValue selectKnowledgeOrLanguage(Skill skill, String customName) {
		logger.debug("try select "+skill);
		if (!canBeSelected(skill)) {
			logger.warn("Trying to select skill "+skill+" which cannot be selected");
			return null;
		}
		if (skill.getType()!=SkillType.KNOWLEDGE && skill.getType()!=SkillType.LANGUAGE) {
			logger.warn("Trying to select skill "+skill+" which is not language or knowledge");
			return null;
		}

		int cost = 3;
		logger.debug("select "+skill+" for "+cost+" karma");

		// Change model
		logger.info("Selected skill "+skill);
		SkillValue sVal = new SkillValue(skill, 1);
		sVal.setName(customName);
		model.addSkill(sVal);
	
		// Make undoable
		SkillModification mod = new SkillModification(skill, 1);
		mod.setExpCost(cost);
		mod.setSource(this);
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, sVal));

		return sVal;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public boolean canBeDeselected(SkillValue val) {
		/*
		 * Only allow deselections for language or knowledge skills
		 */
		Skill skill = val.getModifyable();
		if (skill.getType()!=SkillType.LANGUAGE && skill.getType()!=SkillType.KNOWLEDGE)
			return false;
		// Don't allow it on languages with higher specializations
		if (skill.getType()!=SkillType.KNOWLEDGE && val.getPoints()>1)
			return false;
		// Is automatically added
		if (model.isAutoSkill(val))
			return false;
		
		SkillModification toUndo = getHighestModification(val.getModifyable());
		if (toUndo!=null) {
			if (toUndo.getSource()!=null)
				// Has been increased this session
				return true;
			// Has been increased in career mode
			return (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER.getValue();
		} else {
			// Has been increased in generation mode
			boolean undoFromCreation = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.getValue();
			return undoFromCreation;
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#canBeDeselected(org.prelle.shadowrun6.SkillValue, org.prelle.shadowrun6.SkillSpecialization)
	 */
	@Override
	public boolean canBeDeselected(SkillValue ref, SkillSpecialization spec, boolean expertise) {
		SkillSpecializationModification toUndo = getHighestSpecializationModification(ref.getModifyable(), spec);
//		logger.debug("canBeDeselected = "+toUndo+"    ex="+expertise);
		if (toUndo==null) {
//			logger.debug("canBeDeselected for expertise="+expertise+":  no matching mod");
			return false;
		}
		// Is automatically added
		if (model.isAutoSkill(ref))
			return false;
		
		if (expertise) {
			if (!toUndo.isExpertise())
				return false;
			
			boolean hasSpecialization = hasSpecialAndExpertise(ref)[0];
//			logger.debug("canBeDeselected for expertise: hasSpec="+hasSpecialization);
			return !hasSpecialization;
		} else {
			// Trying to deselect specialization - only if highest modification isn't expertise
//			logger.debug("canBeDeselected for special: expertise="+toUndo.isExpertise());
			return !toUndo.isExpertise();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#getPointsLeftInKnowledgeAndLanguage()
	 */
	@Override
	public int getPointsLeftInKnowledgeAndLanguage() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(
				SR6ConfigOptions.SKILLS_IGNORE_REQUIREMENTS,
				SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER, 
				SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION, 
				SR6ConfigOptions.REFUND_FROM_CAREER, 
				SR6ConfigOptions.REFUND_FROM_CREATION);
	}

	@Override
	public List<ToDoElement> getNormalToDos() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public List<ToDoElement> getKnowledgeToDos() {
		// TODO Auto-generated method stub
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		try {
			this.ignoreRequirements = (SR6ConfigOptions.SKILLS_IGNORE_REQUIREMENTS!=null)?((Boolean) SR6ConfigOptions.SKILLS_IGNORE_REQUIREMENTS.getValue()):false;
			// Clear all attribute modifications in character
			todos.clear();
			avSkills.clear();
			unrestricted.clear();
//			for (Attribute key : Attribute.values()) {
//				AttributeValue val = model.getAttribute(key);
//				val.clearModifications();
//			}

			// Apply attribute modifications
			for (Modification tmp : previous) {
				if (tmp instanceof AllowModification) {
					AllowModification mod = (AllowModification)tmp;
					logger.debug("  Allow skill "+mod.getSkill()+" because of "+mod.getSource());
					unrestricted.add(mod.getSkill());
				} else if (tmp instanceof SkillModification) {
					SkillModification mod = (SkillModification)tmp;
					SkillValue sval = model.getSkillValue(mod.getSkill());
					logger.info("  Apply "+mod+" to skill value "+sval);
					if (sval==null) {
						logger.debug("Skill "+mod.getSkill()+" does not exist######################################");
						sval = new SkillValue(mod.getSkill(), 0);
						model.addSkill(sval);
					} else
						logger.debug("Skill "+mod.getSkill()+" exist###################################### mods="+sval.getModifications());
					sval.addModification(mod);
					logger.debug("  skill value now: "+sval);
				} else
					unprocessed.add(tmp);
			}
			
			
			/*
			 * Determine available skills
			 */
			for (Skill skill : ShadowrunCore.getSkills()) {
				if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE) {
					avSkills.add(skill);
					continue;
				}
				SkillValue val = model.getSkillValue(skill);
				if (val==null || skill.isToSpecify()) {
					// Might add skill if it is not restricted
					if (ignoreRequirements || !skill.isRestricted() || (unrestricted.contains(skill)))
						avSkills.add(skill);
				}
			}
			logger.debug("available skills = "+avSkills);
			
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

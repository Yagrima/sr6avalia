/**
 *
 */
package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.Tradition;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.SpellModification;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaSpellController implements SpellController, CharacterProcessor {

	private static Logger logger = LogManager.getLogger("shadowrun6.lvl");

	protected ProcessorRunner parent;
	private boolean alchemistic;
	protected List<Spell> recSpells;
	protected List<Spell> avSpells;
	protected List<ToDoElement> todos;

	private ShadowrunCharacter model;
	
	//-------------------------------------------------------------------
	/**
	 */
	public KarmaSpellController(ProcessorRunner parent, boolean alchemistic) {
		this.parent = parent;
		this.alchemistic = alchemistic;
		todos      = new ArrayList<>();
		avSpells = new ArrayList<>();
		recSpells = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	protected void updateAvailable() {
		avSpells.clear();
		avSpells.addAll(ShadowrunCore.getSpells());
		// Remove those the character already has
		for (SpellValue val : model.getSpells(alchemistic)) {
			logger.trace("Not available anymore "+val);
			avSpells.remove(val.getModifyable());
		}

		Collections.sort(avSpells);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getSpellsLeft()
	 */
	@Override
	public int getSpellsLeft() {
		return -1;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getAvailableSpells()
	 */
	@Override
	public List<Spell> getAvailableSpells(boolean alchemistic) {
		return avSpells;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#getAvailableSpells(org.prelle.shadowrun6.Spell.Category)
	 */
	@Override
	public List<Spell> getAvailableSpells(Spell.Category category, boolean alchemistic) {
		return getAvailableSpells(alchemistic).stream().filter(test -> test.getCategory()==category).collect(Collectors.toList());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#canBeSelected(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public boolean canBeSelected(Spell spell, boolean alchemistic) {
		// Not already selected
		for (SpellValue tmp : model.getSpells(alchemistic)) {
			if (tmp.getModifyable()==spell)
				return false;
		}

		// Enough karma?
		return model.getKarmaFree()>=5;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#canBeSelected(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public boolean canBeDeselected(SpellValue spell) {
		// Minimum requirement is that the spell is known
		if (!model.getSpells().contains(spell))
			return false;
		
		boolean allowUndoCareer   = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER.getValue();
		boolean allowUndoCreation = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.getValue();
		SpellModification mod = getModification(spell);
				
		if (mod==null) {
			return allowUndoCreation;
		} else if (mod.getSource()==this) {
			// This career session - always allowed
			return true;
		} else
			return allowUndoCareer;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#select(org.prelle.shadowrun6.Spell)
	 */
	@Override
	public SpellValue select(Spell spell, boolean alchemistic) {
		if (!canBeSelected(spell,alchemistic)) {
			logger.warn("Trying to select spell "+spell+" which cannot be selected");
			return null;
		}

		int cost = 5;
		logger.debug("select "+spell+" for "+cost+" karma");

		// Change model
		logger.info("Selected spell "+spell);
		SpellValue sVal = new SpellValue(spell);
		sVal.setAlchemistic(alchemistic);
		model.addSpell(sVal);

		// Make undoable
		SpellModification mod = new SpellModification(spell);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		mod.setAlchemistic(alchemistic);
		model.addToHistory(mod);

		// Pay karma
		model.setKarmaFree(model.getKarmaFree()-cost);
		model.setKarmaInvested(model.getKarmaInvested()+cost);

		parent.runProcessors();
		return sVal;
	}

	//-------------------------------------------------------------------
	private SpellModification getModification(SpellValue sVal) {
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof SpellModification))
				continue;
			SpellModification amod = (SpellModification)mod;
			if (amod.getSpell()!=sVal.getModifyable())
				continue;
			if (amod.isAlchemistic()!=sVal.isAlchemistic())
				continue;
			return amod;
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SkillController#deselect(org.prelle.shadowrun6.SkillValue)
	 */
	@Override
	public void deselect(SpellValue ref) {
		logger.debug("deselect "+ref.getModifyable());
		if (!canBeDeselected(ref))
			return;

		model.removeSpell(ref);

		boolean refundCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		boolean refundCareer   = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		SpellModification mod = getModification(ref);
		
		int cost = 0;
		if (mod==null) {
			// Spell was chosen during creation
			cost = refundCreation?5:0;
		} else if (mod.getExpCost()==0) {
			// Spell was granted for free
		} else if (mod.getSource()==this) {
			// Spell was selected this session
			cost = mod.getExpCost();
		} else {
			// Spell was selected during a previous career session
			cost = refundCareer?5:0;
		}
		logger.debug("Refund "+cost+" karma    - refundCreation="+refundCreation+", refundCareer="+refundCareer);
		
		if (mod!=null) {
			model.removeFromHistory(mod);
		}

		// Grant karma
		model.setKarmaFree(model.getKarmaFree()+cost);
		model.setKarmaInvested(model.getKarmaInvested()-cost);

		parent.runProcessors();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.SpellController#changeMagicTradition(org.prelle.shadowrun6.Tradition)
	 */
	@Override
	public void changeMagicTradition(Tradition data) {
		if (model.getTradition()==data)
			return;
		logger.info("Change magic tradition to "+data);
		model.setTradition(data);
		parent.runProcessors();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(
				SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER, 
				SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION, 
				SR6ConfigOptions.REFUND_FROM_CAREER, 
				SR6ConfigOptions.REFUND_FROM_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.debug("run");
		this.model = model;
		try {			
			// Clear all attribute modifications in character
			todos.clear();
			updateAvailable();
		} finally {
			
		}
		return unprocessed;
	}

}

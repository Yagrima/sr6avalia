package org.prelle.shadowrun6.levelling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.AttributeController;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.AttributeModification.ModificationType;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.proc.CharacterProcessor;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class KarmaAttributeLeveller implements AttributeController, CharacterProcessor {

	private static Logger logger = LogManager.getLogger("shadowrun6.lvl");

	private CharacterController parent;
	private ShadowrunCharacter model;

	private List<ToDoElement> todos;
	private List<Attribute> metatypeAttribute;

	//-------------------------------------------------------------------
	public KarmaAttributeLeveller(CharacterController parent) {
		this.parent = parent;
		this.model  = parent.getCharacter();
		todos = new ArrayList<>();
		metatypeAttribute = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getConfigOptions()
	 */
	@Override
	public List<ConfigOption<?>> getConfigOptions() {
		return Arrays.asList(
				SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER, 
				SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION, 
				SR6ConfigOptions.REFUND_FROM_CAREER, 
				SR6ConfigOptions.REFUND_FROM_CREATION);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getPointsLeftSpecial()
	 */
	@Override
	public int getPointsLeftSpecial() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getValueState(org.prelle.shadowrun6.Attribute, int)
	 */
	@Override
	public ValueState getValueState(Attribute key, int value) {
		if (value<1) return ValueState.BELOW_MINIMUM;
		AttributeValue val = model.getAttribute(key);
		if (value<=val.getModifier()) return ValueState.WITHIN_MODIFIER;

		if (value>val.getMaximum()) return ValueState.ABOVE_MAX;

		if (value<=val.getModifiedValue()) return ValueState.SELECTED;

		// Not selected yet.
		return ValueState.UNSELECTED;
	}

	//-------------------------------------------------------------------
	private AttributeModification getHighestModification(Attribute key) {
		AttributeModification ret = null;
		for (Modification mod : model.getHistory()) {
			if (!(mod instanceof AttributeModification))
				continue;
			AttributeModification amod = (AttributeModification)mod;
			if (mod.getSource()!=this && mod.getSource()!=null)
				continue;
			if (amod.getAttribute()!=key)
				continue;
			if (mod.getExpCost()<0)
				continue;
			if (ret==null || mod.getExpCost()>ret.getExpCost())
				ret = amod;
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#canBeIncreased(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canBeIncreased(Attribute key) {
		// Enough karma?
		if (model.getKarmaFree()<getIncreaseCost(key))
			return false;

		AttributeValue val = model.getAttribute(key);
		// If maximum already reached, deny increasing
		if (val.getPoints()>=val.getMaximum())
			return false;

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getIncreaseCost(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public int getIncreaseCost(Attribute key) {
		switch (key) {
		case MAGIC:
		case RESONANCE:
			return (model.getAttribute(key).getModifiedValue()+1)*5;
		default:
			return (model.getAttribute(key).getPoints()+1)*5;
		}		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#increase(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean increase(Attribute key) {
		logger.debug("increase "+key);
		if (!canBeIncreased(key))
			return false;

		int cost = getIncreaseCost(key);

		// Change
		model.getAttribute(key).setPoints( model.getAttribute(key).getPoints() +1);
		logger.info("Increased "+key+" to "+model.getAttribute(key).getModifiedValue()+" paying "+cost+" karma");

		// Grant power point for adept
		if (key==Attribute.MAGIC && model.getMagicOrResonanceType().usesPowers()) {
			// There may be a house rule that mystic adepts need to buy PP (instead of getting them free)			
			if (model.getMagicOrResonanceType().paysPowers() && SR6ConfigOptions.HOUSERULE_MYSTADEPT_NEED_TO_BUY_PP.getValue()) {
				logger.warn("House rule prevents adding an free power point for increased magic for mystic adepts");
			} else {
				AttributeValue pp = model.getAttribute(Attribute.POWER_POINTS);
				pp.setPoints(pp.getPoints()+1);
				logger.info("Increase power points because MAGIC is increased");
			}
		}
		
		// Make undoable
		AttributeModification mod = new AttributeModification(key, model.getAttribute(key).getPoints());
		mod.setModificationType(ModificationType.ABSOLUTE);
		mod.setExpCost(cost);
		mod.setSource(this);
		mod.setDate(new Date(System.currentTimeMillis()));
		model.addToHistory(mod);

		// Pay
		model.setKarmaInvested(model.getKarmaInvested()+cost);
		model.setKarmaFree(model.getKarmaFree()-cost);
		parent.runProcessors();

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#canBeDecreased(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean canBeDecreased(Attribute key) {
		AttributeValue aVal = model.getAttribute(key);
		AttributeModification toUndo = getHighestModification(key);
		if (toUndo!=null && aVal.getStart()<aVal.getPoints()) {
			if (toUndo.getSource()!=null)
				// Has been increased this session
				return true;
			// Has been increased in career mode
			return (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CAREER.getValue();
		} else {
			// Has been increased in generation mode
			boolean decreaseBelowCreation = (Boolean)SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.getValue();
			return (aVal.getPoints()>1 && decreaseBelowCreation);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#decrease(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean decrease(Attribute key) {
		logger.debug("decrease "+key);
		if (!canBeDecreased(key))
			return false;

		// Change model
		AttributeValue val = model.getAttribute(key);
		model.getAttribute(key).setPoints( val.getPoints() -1);

		// Undo power point for adept
		if (key==Attribute.MAGIC && model.getMagicOrResonanceType().usesPowers()) {
			// There may be a house rule that mystic adepts need to buy PP (instead of getting them free)			
			if (model.getMagicOrResonanceType().paysPowers() && SR6ConfigOptions.HOUSERULE_MYSTADEPT_NEED_TO_BUY_PP.getValue()) {
				logger.warn("House rule prevents removing a free power point for increased magic for mystic adepts");
			} else {
				AttributeValue pp = model.getAttribute(Attribute.POWER_POINTS);
				if (pp.getPoints()>0)
					pp.setPoints(pp.getPoints()-1);
				logger.info("Decrease power points because MAGIC is increased");
			}
		}

		// Log
		boolean refundFromCareer = (Boolean)SR6ConfigOptions.REFUND_FROM_CAREER.getValue();
		boolean refundFromCreation = (Boolean)SR6ConfigOptions.REFUND_FROM_CREATION.getValue();
		int karma = 0;
		AttributeModification toUndo = getHighestModification(key);
		if (toUndo!=null) {
			// Decrease from career
			model.removeFromHistory(toUndo);
			karma = toUndo.getExpCost();
			
			if (refundFromCareer) {
				logger.info("Decreased "+key+" from prev. career session to "+val.getModifiedValue()+" granting "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);
			} else {
				logger.info("Decreased "+key+" from prev. career session to "+val.getModifiedValue()+" without granting "+karma);
				AttributeModification mod = new AttributeModification(key, val.getPoints());
				mod.setModificationType(ModificationType.RELATIVE);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
			
		} else {
			// Decreasing below the creation value
			if (refundFromCreation) {
				karma = getIncreaseCost(key);
				logger.info("Decreased "+key+" from creation session to "+val.getModifiedValue()+" granting "+karma+" karma  ");
				model.setKarmaInvested(model.getKarmaInvested()-karma);
				model.setKarmaFree(model.getKarmaFree()+karma);

				// If it granted Karma, log it
				AttributeModification mod = new AttributeModification(key, model.getAttribute(key).getPoints());
				mod.setModificationType(ModificationType.ABSOLUTE);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			} else {
				logger.info("Decreased "+key+" from creation to "+val.getModifiedValue()+" without granting "+karma);
				AttributeModification mod = new AttributeModification(key, -1);
				mod.setModificationType(ModificationType.RELATIVE);
				mod.setExpCost(-karma);
				mod.setSource(this);
				mod.setDate(new Date(System.currentTimeMillis()));
				model.addToHistory(mod);
			}
		}

		parent.runProcessors();

		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.Controller#getToDos()
	 */
	@Override
	public List<ToDoElement> getToDos() {
		return todos;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue value) {
		return canBeIncreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue value) {
		return canBeDecreased(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean increase(AttributeValue value) {
		return increase(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#decrease(de.rpgframework.genericrpg.SelectedValue)
	 */
	@Override
	public boolean decrease(AttributeValue value) {
		return decrease(value.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#getPerAttribute(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public PerAttributePoints getPerAttribute(Attribute key) {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.charctrl.AttributeController#isRacialAttribute(org.prelle.shadowrun6.Attribute)
	 */
	@Override
	public boolean isRacialAttribute(Attribute key) {
		return metatypeAttribute.contains(key);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun6.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		try {
//			metatypeAttribute.clear();
//			perAttrib.clear();
			
			// Clear all attribute modifications in character
			todos.clear();
//			for (Attribute key : Attribute.values()) {
//				AttributeValue val = model.getAttribute(key);
//				val.clearModifications();
//			}

			// Apply attribute modifications
			for (Modification _mod : previous) {
				if (_mod instanceof AttributeModification) {
					AttributeModification mod = (AttributeModification)_mod;
					AttributeValue val = model.getAttribute(mod.getAttribute());
					if (mod.getType()==ModificationValueType.NATURAL) {
						logger.info("  Add "+mod);
						logger.info("    before "+val);
					}
					val.addModification(mod);
					if (mod.getType()==ModificationValueType.MAX && mod.getValue()>6)
						metatypeAttribute.add(mod.getAttribute());
				} else
					unprocessed.add(_mod);
			}
			logger.debug("  Metatype attributes are: "+metatypeAttribute);
			
			/*
			 * Warn about attributes that are below creation value
			 */
			
			
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

	//-------------------------------------------------------------------
	public void setDontRefund(boolean value) {
		SR6ConfigOptions.REFUND_FROM_CAREER.set(!value);
	}

	public void setDecreaseBelowCreation(boolean value) {
		SR6ConfigOptions.ALLOW_UNDO_FROM_CREATION.set(value);
	}

	public void setRefundBelowCreation(boolean value) {
		SR6ConfigOptions.REFUND_FROM_CREATION.set(value);
	}

}

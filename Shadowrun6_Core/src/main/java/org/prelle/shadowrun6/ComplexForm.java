/**
 *
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.MissingResourceException;

import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class ComplexForm extends BasePluginData implements Comparable<ComplexForm> {

	public enum Duration {
		IMMEDIATE,
		SUSTAINED,
		PERMANENT,
		;
		public String getName() { return Resource.get(ShadowrunCore.getI18nResources(),"cplxform.duration."+name().toLowerCase()); }
		public String getShortName() { return Resource.get(ShadowrunCore.getI18nResources(),"cplxform.duration."+name().toLowerCase()+".short"); }
	}

	@Attribute(required=true)
	private String id;
	@Attribute(name="fad",required=true)
	private int fading;
	@Attribute(name="dur",required=true)
	private Duration duration;
	@Attribute(name="mul")
	private boolean multipleSelectable;
	@Attribute(name="choice")
	private ChoiceType choice;

	//--------------------------------------------------------------------
	/**
	 */
	public ComplexForm() {
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("complexform."+id);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return id;
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "complexform."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "complexform."+id+".desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ComplexForm o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @return the fading
	 */
	public int getFading() {
		return fading;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public ChoiceType getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(ChoiceType choice) {
		this.choice = choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the multipleSelectable
	 */
	public boolean isMultipleSelectable() {
		return multipleSelectable;
	}

	//-------------------------------------------------------------------
	/**
	 * @param multipleSelectable the multipleSelectable to set
	 */
	public void setMultipleSelectable(boolean multipleSelectable) {
		this.multipleSelectable = multipleSelectable;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the duration
	 */
	public Duration getDuration() {
		return duration;
	}

	//-------------------------------------------------------------------
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Duration duration) {
		this.duration = duration;
	}

	//-------------------------------------------------------------------
	public boolean needsChoice() {
		return choice!=null;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6;

/**
 * @author prelle
 *
 */
public enum ChoiceType {
	
	ATTRIBUTE,
	AMMUNITION_TYPE,
	MATRIX_ACTION,
	SKILL,
	COMBAT_SKILL,
	NONCOMBAT_SKILL,
	PHYSICAL_SKILL,
	PHYSICAL_ATTRIBUTE,
	MATRIX_ATTRIBUTE,
	SENSE,
	MENTOR_SPIRIT,
	PROGRAM,
	ELEMENTAL,
	SPIRIT,
	SPRITE,
	NAME,
	SUBSTANCE_SEVERITY,
	SPELL_CATEGORY,
	MELEE_WEAPON,
	WEAPON,
	ADEPT_POWER,

}

package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.AttributeModification.ModificationType;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.SelfExplainingNumericalValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name="attr")
public class AttributeValue extends ModifyableImpl implements Comparable<AttributeValue>, ModifyableValue<Attribute>, SelfExplainingNumericalValue<Attribute> {

	@org.prelle.simplepersist.Attribute(name="id",required=true)
	private Attribute id;

	/**
	 * The recent value of the attribute, including modifications by
	 * race on generation, the distributed points on generation
	 * and the points bought.
	 * Bought = unmodifiedValue - start;
	 */
	@org.prelle.simplepersist.Attribute(name="value",required=true)
	private int distributed;

	/**
	 * The final value of the attribute after generation, before
	 * exp have been spent.
	 * During priority generation this contains the value until
	 * which the attribute is paid by attribute points
	 */
	@org.prelle.simplepersist.Attribute(name="start",required=false)
	private int start;

	//-------------------------------------------------------------------
	public AttributeValue() {
	}

	//-------------------------------------------------------------------
	public AttributeValue(Attribute attr, int val) {
		this.id = attr;
		this.distributed = val;
		this.start = val;
	}

	//-------------------------------------------------------------------
	public AttributeValue(AttributeValue copy) {
		this.id = copy.getModifyable();
		this.start = copy.getStart();
		this.distributed = copy.getPoints();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%24s  START=%d  NORM=%d MOD=%d  VAL=%d", id, start, distributed, getModifier(), getModifiedValue());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public Attribute getAttribute() {
		return id;
	}

	//-------------------------------------------------------------------
	public int getNaturalModifier() {
		int count = 0;
		
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()!=id) {
					// This should not happen
					continue;
				}
				if (aMod.getType()==ModificationValueType.NATURAL)
					count += aMod.getValue();
				}
		}
		return count;
	}

	//-------------------------------------------------------------------
	public int getNaturalValue() {
		return distributed + getNaturalModifier();
	}

	//-------------------------------------------------------------------
	public int getAugmentedModifier() {
		float count = 0;
		
		List<BasePluginData> sources = new ArrayList<>();
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()!=id) {
					// This should not happen
					continue;
				}
				
				if (aMod.getType()==ModificationValueType.AUGMENTED && !aMod.isConditional()) {
					if (aMod.getValue()>=100)
						count += ((float)aMod.getValue())/1000f;
					else
						count += aMod.getValue();
					// Special rule, part 1
					if (aMod.getSource()!=null) {
						if (aMod.getSource() instanceof BasePluginData) {
							sources.add((BasePluginData) aMod.getSource());
						} else if (aMod.getSource() instanceof CarriedItem) {
							sources.add(  ((CarriedItem)aMod.getSource()).getItem() );
						} else if (aMod.getSource() instanceof Attribute) {
						} else {
							LogManager.getLogger("shadowrun6").warn("Don't know how to obtain source from "+aMod.getSource().getClass());
						}
					}
				}
			}
		}

		/*
		 * Special rule, part 2: Reaction enhancers and wired reflexes are compatibel when on WiFi
		 */
		if (id==Attribute.REACTION) {
			boolean hasEnhancers = sources.contains(ShadowrunCore.getItem("reaction_enhancers"));
			boolean hasWiredReflexes = sources.stream().anyMatch(item -> item.getId().startsWith("wired_reflexes"));
			if (count>4 && hasEnhancers && hasWiredReflexes)
				return (int)count;
		}
		
		// Modifier cannot be more then 4 (Core Page 37)
		return Math.min((int)count, 4);
	}

	//-------------------------------------------------------------------
	public int getAugmentedValue() {
		return getNaturalValue() + getAugmentedModifier();
	}

	//-------------------------------------------------------------------
	public int getGenerationModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.NATURAL && 
						((aMod.getSource() instanceof MetaType) || (aMod.getSource() instanceof MagicOrResonanceType) || (aMod.getSource() instanceof Attribute)))
					count += aMod.getValue();
			}
		}
		return count;
	}

	//-------------------------------------------------------------------
	public int getGenerationValue() {
		return distributed + getGenerationModifier();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return getAugmentedValue();
	}

	//-------------------------------------------------------------------
	public int getModifier() {
		return getNaturalModifier()+getAugmentedModifier();
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Attribute getModifyable() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AttributeValue other) {
		return id.compareTo(other.getModifyable());
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int distributed) {
		this.distributed = distributed;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}

	//-------------------------------------------------------------------
	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}

	//-------------------------------------------------------------------
	/**
	 * Points gained by investing EXP
	 * @return the v1Value
	 */
	public int getBought() {
		return distributed - start;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return distributed;
	}

	//--------------------------------------------------------------------
	public int getBoughtOnlyPoints() {
		return distributed;
	}

	//--------------------------------------------------------------------
	public int getMaximum() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.MAX)
					count += aMod.getValue();
			}
		}
		return Math.max(count, getNaturalModifier());
	}

	//--------------------------------------------------------------------
	public int getVolatileModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.AUGMENTED && !aMod.isConditional()) {
					if (aMod.getSource() instanceof MetaType)
						continue;	
					if (aMod.getSource() instanceof MagicOrResonanceType)
						continue;
					count += aMod.getValue();
				}
			}
		}
		
		// Modifier cannot be more then 4 (Core Page 37)
		return Math.min(count, 4);
	}

	//--------------------------------------------------------------------
	public List<String> getModifierExplanation() {
		List<String> data = new ArrayList<>();
//		System.out.println("***ModifierExplanation for "+id);
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
//				System.out.println(" **"+aMod+"  from "+aMod.getSource());
//				System.out.println(" **"+aMod+"  from "+aMod.getSource());
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.AUGMENTED && !aMod.isConditional()) {
//					System.out.println("  *"+ShadowrunTools.getModificationSourceString(aMod.getSource(), aMod.getValue()));
					if (aMod.getSource() instanceof MetaType)
						continue;	
					if (aMod.getSource() instanceof MagicOrResonanceType)
						continue;
					data.add(ShadowrunTools.getModificationSourceString(aMod.getSource(), aMod.getValue()));
				}
			}
		}
		return data;
	}

	//-------------------------------------------------------------------
	public int getAlternateValue() {
		int count = 0;
		int half  = 0;
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification aMod = (AttributeModification)mod;
				if (aMod.getAttribute()==id && aMod.getType()==ModificationValueType.AUGMENTED) {
					if (aMod.getValue()<100) {
						count += aMod.getValue();
					} else {
						half++;
					}
				}
			}
		}
		if (half>0.5)
			count+= (int)(half/2);
		
		// Augmented can be max. normal value +4
		return Math.min(getPoints()+4, count);
	}

	//--------------------------------------------------------------------
	public String getDisplayString() {
		if (getAugmentedModifier()>0)
			return getModifiedValue()+"("+getAugmentedValue()+")";
		return String.valueOf(getModifiedValue());
	}

	//--------------------------------------------------------------------
	public void addNaturalModifier(int value, Object source ) {
		addModification(new AttributeModification(ModificationValueType.NATURAL, id, value, ModificationType.RELATIVE, source));
	}
	
}

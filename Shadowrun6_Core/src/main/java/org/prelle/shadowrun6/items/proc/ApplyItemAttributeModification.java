package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemTemplate.Multiply;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyItemAttributeModification implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	public ApplyItemAttributeModification() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(java.lang.String, org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		String prefix = indent+model.getItem().getId()+": ";
		List<Modification> unprocessed = new ArrayList<>();

		try {
			ItemTemplate ref = model.getItem();
			// For easier access, prepare a list of things to multiply with eventually existing rating
			List<Multiply> multi = new ArrayList<ItemTemplate.Multiply>();
			if (ref.hasRating() && ref.getMultiplyWithRate()!=null)
				multi = Arrays.asList(ref.getMultiplyWithRate());

			for (Modification tmp : previous) {
				if (tmp instanceof ItemAttributeModification) {
					ItemAttributeModification mod = (ItemAttributeModification)tmp;
					// Special handling for attributes regarding slots
					if (mod.getSlot()!=null) {
						AvailableSlot slot = model.getSlot(mod.getSlot());
						switch (mod.getAttribute()) {
						case CAPACITY:
							logger.info("increase capacity of slot "+slot+" by "+mod.getValue());
							slot.addBonusCapacity(mod.getValue());
							break;
						default:
							logger.warn("Don't know how to set "+mod.getAttribute()+" on slot");
						}
						continue;
					}
					
					if (model.hasAttribute(mod.getAttribute())) {
						if (!mod.isIncluded()) {
							logger.info(prefix+"Apply to attribute: "+mod+"  - before: "+model.getAttribute(mod.getAttribute()));
						} else {
							logger.trace(prefix+"Ignore already included: "+mod);
						}
						model.getAttribute(mod.getAttribute()).addModification(mod);
						logger.info(prefix+"Apply to attribute: "+mod+"  - afterwards: "+model.getAttribute(mod.getAttribute()));
					} else if (mod.getAttribute()==ItemAttribute.CONCEALABILITY){
						// Drop this attribute
					} else {
						logger.debug(prefix+"Modification for non-existing attribute "+mod.getAttribute()+" - pass it upwards");
						unprocessed.add(tmp);
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			
		}
		return unprocessed;
	}

}

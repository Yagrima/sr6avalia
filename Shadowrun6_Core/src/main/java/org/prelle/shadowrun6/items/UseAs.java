package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.persist.AvailabilityConverter;
import org.prelle.shadowrun6.persist.ItemHookConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * Defines how the item can be used
 * @author Stefan
 *
 */
public class UseAs {

	@Attribute
	protected ItemType type;
	@Attribute
	protected ItemSubType subtype;
	@Attribute(name="ess")
	private float essence;
	@Attribute(name="cap")
	private int capacity;
	@Attribute(name="slot")
	@AttribConvert(ItemHookConverter.class)
	private ItemHook slot;
	@Attribute(name="cost")
	private int extraCost;
	@Attribute(name="avail")
	@AttribConvert(AvailabilityConverter.class)
	private Availability alternateAvailability;
	@Attribute(name="maxrat")
	private int maxRating;

	/* Selected by EquipmentController to temporarily store chosen rating when UseAs is used to list valid option */
	private transient int rating;
	
	//--------------------------------------------------------------------
	public UseAs() {
	}

	//--------------------------------------------------------------------
	public UseAs(ItemType type, ItemSubType subtype) {
		this.type = type;
		this.subtype = subtype;
	}

	//--------------------------------------------------------------------
	public UseAs(ItemType type, ItemSubType subtype, ItemHook slot) {
		this.type = type;
		this.subtype = subtype;
		this.slot    = slot;
	}

	//--------------------------------------------------------------------
	public UseAs(ItemType type, ItemSubType subtype, ItemHook slot, int capacity) {
		this.type = type;
		this.subtype = subtype;
		this.slot    = slot;
		this.capacity= capacity;
	}

	//--------------------------------------------------------------------
	public UseAs(UseAs other) {
		this.type    = other.getType();
		this.subtype = other.subtype;
		this.essence = other.essence;
		this.capacity= other.capacity;
		this.slot    = other.slot;
		this.alternateAvailability = other.alternateAvailability;
		this.maxRating = other.maxRating;
		this.extraCost = other.extraCost;
	}
	
	public String toString() {
		return "UseAs("+type+","+subtype+",slot="+slot+",cap="+capacity+",ess="+essence+",maxRat="+maxRating+" - rating="+rating+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ItemType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the subtype
	 */
	public ItemSubType getSubtype() {
		return subtype;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the essence
	 */
	public float getEssence() {
		return essence;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return slot;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the extraCost
	 */
	public int getExtraCost() {
		return extraCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the alternateAvailability
	 */
	public Availability getAlternateAvailability() {
		return alternateAvailability;
	}

	//-------------------------------------------------------------------
	/**
	 * @param alternateAvailability the alternateAvailability to set
	 */
	public void setAlternateAvailability(Availability alternateAvailability) {
		this.alternateAvailability = alternateAvailability;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the maxRating
	 */
	public int getMaxRating() {
		return maxRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param maxRating the maxRating to set
	 */
	public void setMaxRating(int maxRating) {
		this.maxRating = maxRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ItemType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param subtype the subtype to set
	 */
	public void setSubtype(ItemSubType subtype) {
		this.subtype = subtype;
	}

	//-------------------------------------------------------------------
	/**
	 * @param essence the essence to set
	 */
	public void setEssence(float essence) {
		this.essence = essence;
	}

	//-------------------------------------------------------------------
	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	//-------------------------------------------------------------------
	/**
	 * @param slot the slot to set
	 */
	public void setSlot(ItemHook slot) {
		this.slot = slot;
	}

	//-------------------------------------------------------------------
	/**
	 * @param extraCost the extraCost to set
	 */
	public void setExtraCost(int extraCost) {
		this.extraCost = extraCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

}

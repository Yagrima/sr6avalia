/**
 *
 */
package org.prelle.shadowrun6.items;

import java.text.Collator;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

/**
 * @author prelle
 *
 */
public class AmmunitionType extends BasePluginData implements Comparable<AmmunitionType> {

	@Attribute(required=true)
	private String id;
	@Attribute(name="cmult")
	private float costMultiplier;
	@Element(name="modifications")
	private ModificationList modifications;

	//-------------------------------------------------------------------
	public AmmunitionType() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		try {
			return i18n.getString("ammotype."+id);
		} catch (MissingResourceException mre) {
			if (!reportedKeys.contains(mre.getKey())) {
				reportedKeys.add(mre.getKey());
				logger.error("Missing property '"+mre.getKey()+"' in "+i18n.getBaseBundleName());
				if (MISSING!=null)
					MISSING.println(mre.getKey()+"=");
			}
			return id;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "ammotype."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "ammotype."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the costMultiplier
	 */
	public float getCostMultiplier() {
		return costMultiplier;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public ModificationList getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AmmunitionType o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

}

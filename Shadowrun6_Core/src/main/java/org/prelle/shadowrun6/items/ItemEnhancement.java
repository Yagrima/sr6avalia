/**
 *
 */
package org.prelle.shadowrun6.items;

import java.text.Collator;
import java.util.List;
import java.util.MissingResourceException;

import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.modifications.ItemHookModification;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ItemEnhancement extends BasePluginData implements Comparable<ItemEnhancement>  {

	@Attribute(required=true)
	private String id;
	@Attribute
	protected ItemType type;
	@Attribute
	protected ItemSubType subtype;
	@Element
	private ModificationList modifications;
	@Element
	private RequirementList requires;

	//--------------------------------------------------------------------
	public ItemEnhancement() {
		modifications = new ModificationList();
		requires = new RequirementList();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null)
			return id;
		try {
			return i18n.getString("itemmod."+id);
		} catch (MissingResourceException e) {
			logger.error(e.getMessage()+" in "+i18n.getBaseBundleName());
			return "itemmod."+id;
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "itemmod."+id+".page";
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	public void addModification(ItemHookModification mod) {
		modifications.add(mod);
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ItemEnhancement other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//-------------------------------------------------------------------
	public ItemType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the subtype
	 */
	public ItemSubType getSubtype() {
		return subtype;
	}

}

/**
 *
 */
package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.persist.WeaponDamageConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class AmmunitionData {

	@Attribute(name="gz")
	@AttribConvert(WeaponDamageConverter.class)
	private Damage damageGroundZero;
	@Attribute(name="close")
	@AttribConvert(WeaponDamageConverter.class)
	private Damage damageClose;
	@Attribute(name="near")
	@AttribConvert(WeaponDamageConverter.class)
	private Damage damageNear;
	@Attribute(name="blast")
	private int blast;
	@Attribute
	private ChoiceType choice;
	
	
	//-------------------------------------------------------------------
	public AmmunitionData() {
	}

	//-------------------------------------------------------------------
	/**
	 * @return the blast
	 */
	public int getBlast() {
		return blast;
	}

	//-------------------------------------------------------------------
	/**
	 * @param blast the blast to set
	 */
	public void setBlast(int blast) {
		this.blast = blast;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the damageGroundZero
	 */
	public Damage getDamageGroundZero() {
		return damageGroundZero;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damageGroundZero the damageGroundZero to set
	 */
	public void setDamageGroundZero(Damage damageGroundZero) {
		this.damageGroundZero = damageGroundZero;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the damageClose
	 */
	public Damage getDamageClose() {
		return damageClose;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damageClose the damageClose to set
	 */
	public void setDamageClose(Damage damageClose) {
		this.damageClose = damageClose;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the damageNear
	 */
	public Damage getDamageNear() {
		return damageNear;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damageNear the damageNear to set
	 */
	public void setDamageNear(Damage damageNear) {
		this.damageNear = damageNear;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public ChoiceType getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(ChoiceType choice) {
		this.choice = choice;
	}

}

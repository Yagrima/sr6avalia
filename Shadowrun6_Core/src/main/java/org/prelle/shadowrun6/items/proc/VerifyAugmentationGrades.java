/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class VerifyAugmentationGrades implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 */
	public VerifyAugmentationGrades() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(java.lang.String, org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed) {
		String prefix = indent+model.getItem().getId()+": ";
		
		ItemTemplate ref = model.getItem();
		if (ref.isType(ItemType.CYBERWARE)) {
			/*
			 * Walk through all accessories
			 */
			for (CarriedItem tmp : model.getUserAddedAccessories()) {
				if (tmp.getQuality()!=model.getQuality()) {
					logger.debug(prefix+Resource.format(ShadowrunCore.getI18nResources(), "todos.carrieditem.cyberware.qualitymismatch", tmp.getName(), model.getQuality().getName()));
					model.addToDo(new ToDoElement(Severity.WARNING, Resource.format(ShadowrunCore.getI18nResources(), "todos.carrieditem.cyberware.qualitymismatch", tmp.getName(), model.getQuality().getName())));
				}
			}
		}

		return unprocessed;
	}

}

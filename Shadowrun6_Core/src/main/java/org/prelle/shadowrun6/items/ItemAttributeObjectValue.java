package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.List;

import de.rpgframework.genericrpg.modification.Modification;

public class ItemAttributeObjectValue extends ItemAttributeValue  {

	protected Object value;

	//--------------------------------------------------------------------
	public ItemAttributeObjectValue(ItemAttribute attr, Object val, List<Modification> mods) {
		super(attr, mods);
		value = val;
	}

	//--------------------------------------------------------------------
	public ItemAttributeObjectValue(ItemAttribute attr, Object val) {
		super(attr, new ArrayList<>());
		value = val;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return value+" ("+modifications+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return
	 */
	public Object getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	public void setValue(Object value) {
		this.value = value;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemTemplate.Multiply;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ItemHookModification;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyItemHookModifications implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");
	
	private String prefix;
	private String indent;

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		prefix = indent+model.getItem().getId()+": ";
		this.indent = indent;
		List<Modification> unprocessed = new ArrayList<>();

		try {
			ItemTemplate ref = model.getItem();
			// For easier access, prepare a list of things to multiply with eventually existing rating
			List<Multiply> multi = new ArrayList<ItemTemplate.Multiply>();
			if (ref.hasRating() && ref.getMultiplyWithRate()!=null)
				multi = Arrays.asList(ref.getMultiplyWithRate());

			for (Modification tmp : previous) {
				if (tmp instanceof ItemHookModification) {
					ItemHookModification mod = (ItemHookModification)tmp;
					if (mod.isRemove() && model.hasHook(mod.getHook())) {
						// Undo stock-accessories
						logger.debug("Slot to remove: "+model.getSlot(mod.getHook()));
						for (CarriedItem item : model.getSlot(mod.getHook()).getAllEmbeddedItems()) {
							if (logger.isDebugEnabled())
								logger.debug(prefix+"In removed "+mod.getHook()+" there is a "+item+"  - created by mod = "+item.isCreatedByModification()+" which has mods="+item.getModifications());
							if (item.isCreatedByModification()) {
								// Is a stock accessory - it's modifications are usually included
								// in the item stats and must now be removed
								model.addToDo(new ToDoElement(Severity.INFO, 
										Resource.format(ShadowrunCore.getI18nResources(), "todos.carrieditem.slot_removed_by_enhancement", 
												mod.getHook().getName(),
												String.valueOf(tmp.getSource()),
												item.getName())));
								for (Modification mod2 : item.getModifications()) {
									undoFromVirtual(model, mod2);
								}
							}
						}
						logger.info(prefix+"remove slot "+mod.getHook());
						model.removeHook(mod.getHook());
					} else if (model.hasHook(mod.getHook())) {
						// Do nothing if slot already exists
					} else {
						// Create a new slot
						// If present, use a cached one
						AvailableSlot toAdd = model._intern_getCachedSlot(mod.getHook());
						if (toAdd==null)
							toAdd = new AvailableSlot(mod.getHook(), (multi.contains(Multiply.CAPACITY))?(mod.getCapacity()*model.getRating()):mod.getCapacity());
						toAdd.clearBonusCapacity();
						model.addHook(toAdd);
						logger.info(prefix+"   added slot "+toAdd.getSlot()+" to "+model.getNameWithRating());
						// May be wrong - but slots with capacity may as well set the attribute CAPACITY
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			
		}
		return unprocessed;
	}
	
	//--------------------------------------------------------------------
	private void undoFromVirtual(CarriedItem model, Modification tmp) {
		if (tmp instanceof ItemAttributeModification) {
			ItemAttributeModification mod = (ItemAttributeModification)tmp;
			logger.info(prefix+"Undo "+mod.getAttribute()+" = "+mod.getValue());
			switch (mod.getAttribute()) {
			case ATTACK_RATING:
				int[] moddedAR = (int[]) model.getAsObject(ItemAttribute.ATTACK_RATING).getValue();
				for (int i=0; i<moddedAR.length; i++)
					moddedAR[i] = Math.max(0, moddedAR[i]-mod.getValue());
				// No need to set again - copied by reference
				logger.info(prefix+"Changed AR: "+Arrays.toString(moddedAR));
				break;
			default:
				logger.error(prefix+"Don't know how to undo modifications for "+mod.getAttribute());
			}
		} else {
			logger.error(prefix+"Don't know how to undo modification "+tmp.getClass()+" = "+tmp);
		}
	}

}

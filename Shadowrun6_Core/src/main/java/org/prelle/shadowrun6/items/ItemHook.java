/**
 *
 */
package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;

/**
 * @author Stefan
 *
 */
public enum ItemHook {

	FIREARMS_EXTERNAL,
	BARREL,
	INTERNAL,
	SMARTGUN,
	TOP,
	UNDER,
	STOCK,
	ELECTRONIC_ACCESSORY,
	OPTICAL(true),
	AUDIO(true),
	SENSOR_HOUSING(true),
	ARMOR(true),
	ARMOR_ADDITION(true),
	HELMET_ACCESSORY(true),
	HEADWARE_IMPLANT(true),
	CYBEREYE_IMPLANT(true),
	CYBEREAR_IMPLANT(true),
	CYBERLIMB_IMPLANT(true),
	MOUNTED_HOLDOUT,
	MOUNTED_PISTOL_LIGHT,
	MOUNTED_PISTOL_MACHINE,
	MOUNTED_PISTOL_HEAVY,
	MOUNTED_SMG, // Submachine guns
	MOUNTED_SHOTGUN,
	MOUNTED_RIFLES,
	MOUNTED_LAUNCHER,
	VEHICLE_DRIVE(true),
	VEHICLE_PROTECTION(true),
	VEHICLE_WEAPON(true),
	VEHICLE_BODY(true),
	VEHICLE_ELECTRONICS(true),
	VEHICLE_COSMETICS(true),
	SOFTWARE(true),
	SKILLJACK(true),
	SOFTWARE_DRONE(true),
	;

	boolean hasCapacity;
	
	//-------------------------------------------------------------------
	private ItemHook() {
		hasCapacity = false;
	}
	
	//-------------------------------------------------------------------
	private ItemHook(boolean cap) {
		hasCapacity = cap;
	}
	
	//-------------------------------------------------------------------
	public String getName() {
		return Resource.get(ShadowrunCore.getI18nResources(),"itemhook."+this.name().toLowerCase());
	}
	
	//-------------------------------------------------------------------
	public boolean hasCapacity() {
		return hasCapacity;
	}
}

/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.modifications.AccessoryModification;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyAccessoryModification implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		String prefix = indent+model.getItem().getId()+": ";
		List<Modification> unprocessed = new ArrayList<>();

		try {
			for (Modification tmp : previous) {
				if (tmp instanceof AccessoryModification) {
					AccessoryModification mod = (AccessoryModification)tmp;
					CarriedItem accessory = model.getMemorizedAccessory(mod);
					if (accessory==null) {
							accessory = new CarriedItem(mod.getItem(), mod.getRating());
							accessory.setCreatedByModification(true);
							accessory.setSlot(mod.getHook());
							accessory.setEmbeddedIn(model.getUniqueId());
							ItemRecalculation.recalculate(indent+"   ", accessory);
							model.memorizeAccessory(mod, accessory);
							/*
							 * Also save the UUID of the accessory, if the accessory can
							 * contain accessories itself
							 */
							if (accessory.getSlots().size()>0) {
								if (!model.getGeneratedUUIDs().isEmpty()) {
									// Walk through all stored generated UUIDs until you find the matching one
									boolean notFound = true;
									for (String keyVal : model.getGeneratedUUIDs()) {
										String[] idAndUUID = keyVal.split("\\|");
										if (idAndUUID[0].equals(mod.getItem().getId())) {
											logger.info("Replace generated UUID "+accessory.getUniqueId()+" with memorized "+idAndUUID[1]);
											accessory.setUniqueId(UUID.fromString(idAndUUID[1]));
											notFound=false;
											break;
										}
									}
									if (notFound) {
										logger.warn("Did not find previously generated UUID for item "+mod.getItem().getId());
									}
								} else {
									// Not UUIDs yet - add them
									model.addGeneratedUUIDs(mod.getItem().getId()+"|"+accessory.getUniqueId());
									logger.warn("Fix missing generatedUUID with "+accessory.getUniqueId()+" - if you had items embedded in your "+mod.getItem().getName()+", they are lost");
								}
							}
					}
					// If item attribute modifications are already included, mark them
					if (mod.isIncluded()) {
						accessory.setIgnoredForCalculations(true);
						// Mark modifications as already included
						for (Modification tmp2 : accessory.getModifications()) {
							if (tmp2 instanceof ItemAttributeModification) {
								((ItemAttributeModification)tmp2).setIncluded(true);
							}
						}
					}
					if (model.hasHook(mod.getHook())) {
						// Accessory added to existing slot
						model.getSlot(mod.getHook()).addEmbeddedItem(accessory);
						if (logger.isInfoEnabled())
							logger.info(prefix+"embedded item "+mod.getItem()+" in existing slot "+accessory.getSlot()+"  - ignore it's modifications = "+mod.isIncluded());
					} else {
						// Accessory add to not existing slot
						// To be more robust against errors in data, create the slot (without capacity)
						AvailableSlot toAdd = new AvailableSlot(mod.getHook());
						toAdd.addEmbeddedItem(accessory);
						model.addHook(toAdd);
						if (logger.isInfoEnabled())
							logger.info(prefix+"embedded item "+mod.getItem()+" in previously not existing slot "+accessory.getSlot()+"  - ignore it's modifications = "+mod.isIncluded());
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			
		}
		return unprocessed;
	}

}

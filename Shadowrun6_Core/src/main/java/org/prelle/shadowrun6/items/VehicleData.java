/**
 * 
 */
package org.prelle.shadowrun6.items;

import org.prelle.shadowrun6.persist.OnRoadOffRoadConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="vehicle")
public class VehicleData {
	
	public enum VehicleType {
		GROUND("ground_craft"),
		WATER("watercraft"),
		AIR("aircraft")
		;
		String specializationId;
		private VehicleType(String specialID) {
			specializationId = specialID;
		}
		public String getSpecializationID() { return specializationId; }
	}

	@Attribute(name="han")
	@AttribConvert(OnRoadOffRoadConverter.class)
	private OnRoadOffRoadValue handling;
	@Attribute(name="acc")
	private int acceleration;
	@Attribute(name="spdi")
	private int speedInterval;
	@Attribute(name="tspd")
	private int topSpeed;
	@Attribute(name="bod")
	private int body;
	@Attribute(name="arm")
	private int armor;
	@Attribute(name="pil")
	private int pilot;
	@Attribute(name="sen")
	private int sensor;
	@Attribute(name="sea")
	private int seats;
	@Attribute(name="type")
	private VehicleType type;
	
	//-------------------------------------------------------------------
	public VehicleData() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the handling
	 */
	public OnRoadOffRoadValue getHandling() {
		return handling;
	}

	//-------------------------------------------------------------------
	/**
	 * @param handling the handling to set
	 */
	public void setHandling(OnRoadOffRoadValue handling) {
		this.handling = handling;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the acceleration
	 */
	public int getAcceleration() {
		return acceleration;
	}

	//-------------------------------------------------------------------
	/**
	 * @param acceleration the acceleration to set
	 */
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the body
	 */
	public int getBody() {
		return body;
	}

	//-------------------------------------------------------------------
	/**
	 * @param body the body to set
	 */
	public void setBody(int body) {
		this.body = body;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the armor
	 */
	public int getArmor() {
		return armor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param armor the armor to set
	 */
	public void setArmor(int armor) {
		this.armor = armor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the pilot
	 */
	public int getPilot() {
		return pilot;
	}

	//-------------------------------------------------------------------
	/**
	 * @param pilot the pilot to set
	 */
	public void setPilot(int pilot) {
		this.pilot = pilot;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the sensor
	 */
	public int getSensor() {
		return sensor;
	}

	//-------------------------------------------------------------------
	/**
	 * @param sensor the sensor to set
	 */
	public void setSensor(int sensor) {
		this.sensor = sensor;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the seats
	 */
	public int getSeats() {
		return seats;
	}

	//-------------------------------------------------------------------
	/**
	 * @param seats the seats to set
	 */
	public void setSeats(int seats) {
		this.seats = seats;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the speedInterval
	 */
	public int getSpeedInterval() {
		return speedInterval;
	}

	//-------------------------------------------------------------------
	/**
	 * @param speedInterval the speedInterval to set
	 */
	public void setSpeedInterval(int speedInterval) {
		this.speedInterval = speedInterval;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the topSpeed
	 */
	public int getTopSpeed() {
		return topSpeed;
	}

	//-------------------------------------------------------------------
	/**
	 * @param topSpeed the topSpeed to set
	 */
	public void setTopSpeed(int topSpeed) {
		this.topSpeed = topSpeed;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public VehicleType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(VehicleType type) {
		this.type = type;
	}

}

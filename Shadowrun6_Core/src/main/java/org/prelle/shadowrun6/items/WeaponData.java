/**
 *
 */
package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.persist.AmmunitionConverter;
import org.prelle.shadowrun6.persist.AttackRatingConverter;
import org.prelle.shadowrun6.persist.FireModesConverter;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.shadowrun6.persist.SkillSpecializationConverter;
import org.prelle.shadowrun6.persist.WeaponDamageConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class WeaponData extends ModifyableImpl {

	private transient ItemTemplate parent;
	@Attribute
	@AttribConvert(value=SkillConverter.class)
	private Skill skill;
	@Attribute
	@AttribConvert(value=SkillSpecializationConverter.class)
	private SkillSpecialization spec;
	@Attribute(name="attack")
	@AttribConvert(value=AttackRatingConverter.class)
	private int[] attackRating;

	@Attribute(name="dmg")
	@AttribConvert(WeaponDamageConverter.class)
	protected Damage damage;
	@Attribute(name="mode")
	@AttribConvert(FireModesConverter.class)
	private List<FireMode> mode;
	@Attribute(name="ammo")
	@AttribConvert(AmmunitionConverter.class)
	private List<AmmunitionSlot> ammo;

	//-------------------------------------------------------------------
	/**
	 */
	public WeaponData() {
		ammo = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (parent!=null)
			return parent.getName();
		return "?";
	}

	//--------------------------------------------------------------------
	/**
	 * Convenience for getSkillSpecialization().getSkill()
	 * @return the skill
	 */
	public Skill getSkill() {
		if (skill!=null)
			return skill;
		return spec.getSkill();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the damage
	 */
	public Damage getDamage() {
		return damage;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the spec
	 */
	public SkillSpecialization getSpecialization() {
		return spec;
	}

	//-------------------------------------------------------------------
	/**
	 * @param spec the spec to set
	 */
	public void setSpecialization(SkillSpecialization spec) {
		if (spec!=null)
			this.skill = spec.getSkill();
		this.spec = spec;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damage the damage to set
	 */
	public void setDamage(Damage damage) {
		this.damage = damage;
	}

//	//--------------------------------------------------------------------
//	public ItemAttributeValue getRecoilCompensation(List<Modification> mods) {
//		return new ItemAttributeValue(ItemAttribute.RECOIL_COMPENSATION, recoilCompensation, mods);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the mode
	 */
	public List<FireMode> getFireModes() {
		return mode;
	}

	//-------------------------------------------------------------------
	public List<String> getFireModeNames() {
		List<String> ret = new ArrayList<String>();
		if (mode!=null) {
			for (FireMode tmp : mode)
				ret.add(tmp.getName());
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public List<AmmunitionSlot> getAmmunition() {
		return ammo;
	}

	//-------------------------------------------------------------------
	public List<String> getAmmunitionNames() {
		List<String> ret = new ArrayList<String>();
		if (ammo!=null) {
			for (AmmunitionSlot tmp : ammo)
				ret.add(tmp.toString());
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public boolean isMeleeWeapon() {
		return skill!=null && (
				skill.getId().equals("unarmed")
				|| skill.getId().equals("blades")
				|| skill.getId().equals("clubs")
				|| skill.getId().equals("exoticClose")
				);
	}

	//--------------------------------------------------------------------
	public boolean isRangedWeapon() {
		return skill!=null && !isMeleeWeapon();
	}

	//-------------------------------------------------------------------
	/**
	 * @param mode the mode to set
	 */
	public void setFireModes(List<FireMode> mode) {
		this.mode = mode;
	}

	//-------------------------------------------------------------------
	/**
	 * @param ammo the ammo to set
	 */
	public void setAmmunition(AmmunitionSlot ammo) {
		this.ammo = Collections.singletonList(ammo);
	}

	//-------------------------------------------------------------------
	/**
	 * @param ammo the ammo to set
	 */
	public void setAmmunition(List<AmmunitionSlot> ammo) {
		this.ammo = ammo;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the parent
	 */
	public ItemTemplate getParent() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @param parent the parent to set
	 */
	public void setParent(ItemTemplate parent) {
		this.parent = parent;
	}

	//-------------------------------------------------------------------
	public int[] getAttackRating() {
		return attackRating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param attackRating the attackRating to set
	 */
	public void setAttackRating(int[] attackRating) {
		this.attackRating = attackRating;
	}

}

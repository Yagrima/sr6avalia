package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;

/**
 * @author prelle
 *
 */
public class AvailableSlot {

	@Attribute
	private ItemHook ref;
	@Attribute
	private int capacity;
	
	private transient List<CarriedItem> embedded;
	private transient int bonusCap;
	private transient List<CarriedItem> resetProofEmbedded;
	
	//-------------------------------------------------------------------
	public AvailableSlot() {
		embedded = new ArrayList<CarriedItem>();
		resetProofEmbedded = new ArrayList<CarriedItem>();
	}
	
	//-------------------------------------------------------------------
	public AvailableSlot(ItemHook hook) {
		this();
		this.ref = hook;
		if (hook.hasCapacity)
			throw new IllegalArgumentException("Hook has capacity - use other constructor");
	}
	
	//-------------------------------------------------------------------
	public AvailableSlot(ItemHook hook, int capacity) {
		this();
		this.ref = hook;
		this.capacity = capacity;
	}

	//-------------------------------------------------------------------
	public List<CarriedItem> getAllEmbeddedItems() {
		return new ArrayList<>(embedded);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return ref+"(cap="+capacity+", items="+embedded+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @param accessory the accessory to set
	 */
	public void addEmbeddedItem(CarriedItem accessory) {
		if (embedded.contains(accessory))
			return;
		int cap = 1;
		if (ref.hasCapacity)
			cap = capacity;
		if (capacity>0 && embedded.size()>cap)
			throw new IllegalStateException("Cannot add any more items. Already have "+embedded.size()+" : "+embedded);
		
		embedded.add(accessory);
		if (!resetProofEmbedded.contains(accessory) && !accessory.isCreatedByModification())
			resetProofEmbedded.add(accessory);
	}

	//-------------------------------------------------------------------
	public boolean removeEmbeddedItem(CarriedItem accessory) {
		resetProofEmbedded.remove(accessory);
		return embedded.remove(accessory);		
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity + bonusCap;
	}

	//-------------------------------------------------------------------
	public void clearBonusCapacity() {
		bonusCap = 0;
	}
	
	//-------------------------------------------------------------------
	public void addBonusCapacity(int cap) {
		this.bonusCap+=cap;
	}

	//-------------------------------------------------------------------
	public int getUsedCapacity() {
		int sum = 0;
		for (CarriedItem item : embedded) {
			if (item.isIgnoredForCalculations())
				continue;
			int val = item.getAsValue(ItemAttribute.CAPACITY).getModifiedValue();
			sum+= val;
		}
		return sum;
	}

	//-------------------------------------------------------------------
	public int getFreeCapacity() {
		return getCapacity() - getUsedCapacity();
	}

	//-------------------------------------------------------------------
	public void clear() {
		embedded.clear();
		embedded.addAll(resetProofEmbedded);
	}

}

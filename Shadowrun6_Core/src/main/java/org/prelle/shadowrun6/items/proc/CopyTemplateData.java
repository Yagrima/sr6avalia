/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.ArmorData;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.CyberdeckData;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemTemplate.Multiply;
import org.prelle.shadowrun6.items.RiggerConsoleData;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.items.VehicleData;
import org.prelle.shadowrun6.items.WeaponData;
import org.prelle.shadowrun6.requirements.Requirement;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class CopyTemplateData implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	private String prefix;
	
	//--------------------------------------------------------------------
	public CopyTemplateData() {
	}

	//--------------------------------------------------------------------
	private void copyBasicData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		int rating = model.getRating();
		// Availability
		if ( multi.contains(Multiply.AVAIL) ) {
			model.setAttribute(ItemAttribute.AVAILABILITY, new Availability(rating*ref.getAvailability().getValue(), ref.getAvailability().getLegality(), false) );
		} else {
			model.setAttribute(ItemAttribute.AVAILABILITY, ref.getAvailability());
		}
		// Cost
		int price = ref.getPrice();
		if (multi.contains(Multiply.PRICE)) {
			if (ref.getPriceTable()!=null)
				price = ref.getPriceTable()[rating-1]*rating;
			else
				price *= rating;
		} else if (multi.contains(Multiply.PRICE2)) {
			if (ref.getPriceTable()!=null)
				price = ref.getPriceTable()[rating-1]*rating * rating;
			else
				price *= rating * rating;
		}
		if (ref.getDefaultUsage()!=null && model.getSlot()==null)
			price += ref.getDefaultUsage().getExtraCost();
		if (model.getSlot()!=null && ref.getUsageFor(model.getSlot())!=null)
			price += ref.getUsageFor(model.getSlot()).getExtraCost();
		model.setAttribute(ItemAttribute.PRICE, price );
		// Device Rating
		if (ref.getDeviceRating()>0)
			model.setAttribute(ItemAttribute.DEVICE_RATING, ref.getDeviceRating() );
	}

	//--------------------------------------------------------------------
	private void copyWeaponData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		WeaponData data = ref.getWeaponData();
		if (data==null)
			return;
		
//		setAttribute(ItemAttribute.DAMAGE, data.getDamage() );
		model.setAttribute( data.getDamage() );
		data.getDamage().clearModifications();
		model.setAttribute(ItemAttribute.MODE  , data.getFireModes() );
		model.setAttribute(ItemAttribute.ATTACK_RATING, data.getAttackRating() );
		if (multi.contains(Multiply.ATTACK)) {
			int[] ret = new int[5];
			for (int i=0; i<5; i++) {
				ret[i] = Math.round(ref.getWeaponData().getAttackRating()[i] * model.getRating());
			}
			model.setAttribute(ItemAttribute.ATTACK_RATING, ret );
		}
		model.setAttribute(ItemAttribute.AMMUNITION, data.getAmmunition() );
		model.setAttribute(ItemAttribute.SKILL , data.getSkill() );
		model.setAttribute(ItemAttribute.SKILL_SPECIALIZATION, data.getSpecialization() );
	}

	//--------------------------------------------------------------------
	private void copyUsageData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		UseAs usage = model.getItem().getDefaultUsage();
		if (model.getSlot()!=null) {
			usage = model.getItem().getUsageFor(model.getSlot());
		}
		if (usage==null && !ref.getUseAs().isEmpty()) {
			usage = ref.getUseAs().get(0);
			logger.warn(prefix+"ItemHook/Slot not set in CarriedItem - assume first UseAs from ItemTemplate: "+usage);
		}

		logger.info(prefix+"Usage for slot "+model.getSlot()+": "+usage);
		if (usage==null)
			return;
		model.setUsedAsType(usage.getType());
		model.setUsedAsSubType(usage.getSubtype());
		
		int rating = model.getRating();
		
//		if (usage.getType()==ItemType.ACCESSORY) {
			// Item is dealt with as an accessory
		float qMod = 1.0f;
		if (model.getQuality()!=null) {
			switch (model.getQuality()) {
			case STANDARD: break;
			case ALPHA: qMod = 0.8f; break;
			case BETA : qMod = 0.7f; break;
			case DELTA: qMod = 0.5f; break;
			case USED : qMod = 1.1f; break;
			}
		}

		int   essence = Math.round(usage.getEssence()*1000*qMod);
		int   cap     = usage.getCapacity();
		int   cost    = usage.getExtraCost() + ref.getPrice();
		logger.debug(prefix+"Essence/Capacity/Cost = "+essence+"/"+cap+"/"+cost);
		// Cost
		if (multi.contains(Multiply.PRICE)) {
			model.getAsValue(ItemAttribute.PRICE).setPoints( rating*cost);
		} else if (multi.contains(Multiply.PRICE2)) {
			model.getAsValue(ItemAttribute.PRICE).setPoints( rating*rating*cost);
		} else {
			model.getAsValue(ItemAttribute.PRICE).setPoints(cost);
		}
		// Capacity
		if (model.hasAttribute(ItemAttribute.CAPACITY))
			model.getAsValue(ItemAttribute.CAPACITY).setPoints( multi.contains(Multiply.CAPACITY) ? rating*cap : cap );
		else
			model.setAttribute(ItemAttribute.CAPACITY, multi.contains(Multiply.CAPACITY) ? rating*cap : cap );
		// Essence
		model.setAttribute(ItemAttribute.ESSENCECOST, multi.contains(Multiply.ESSENCE) ? rating*essence: essence );
//		}
	}

	//--------------------------------------------------------------------
	private void copyArmorData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		ArmorData data = ref.getArmorData();
		if (data==null)
			return;
		
		if (model.getRating()>0) {
			model.setAttribute(ItemAttribute.ARMOR, model.getRating());
		} else {
			model.setAttribute(ItemAttribute.ARMOR, data.getRating());
		}
	}

	//--------------------------------------------------------------------
	private void copyVehicleData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		VehicleData data = ref.getVehicleData();
		if (data==null)
			return;
		
		model.setAttribute(ItemAttribute.HANDLING    , data.getHandling() );
		model.setAttribute(ItemAttribute.ACCELERATION, data.getAcceleration() );
		model.setAttribute(ItemAttribute.SPEED_INTERVAL, data.getSpeedInterval() );
		model.setAttribute(ItemAttribute.SPEED         , data.getTopSpeed() );
		model.setAttribute(ItemAttribute.BODY          , data.getBody() );
		model.setAttribute(ItemAttribute.ARMOR         , data.getArmor() );
		model.setAttribute(ItemAttribute.PILOT         , data.getPilot() );
		model.setAttribute(ItemAttribute.SENSORS       , data.getSensor() );
		model.setAttribute(ItemAttribute.SEATS         , data.getSeats() );
	}

	//--------------------------------------------------------------------
	private void copyRiggerConsoleData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		RiggerConsoleData data = ref.getRiggerConsoleData();
		if (data==null)
			return;
		
		model.setAttribute(ItemAttribute.DATA_PROCESSING, data.getDataProcessing() );
		model.setAttribute(ItemAttribute.FIREWALL       , data.getFirewall() );
		model.setAttribute(ItemAttribute.CONCURRENT_PROGRAMS, data.getDataProcessing() );
	}

	//--------------------------------------------------------------------
	private void copyCyberdeckData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		CyberdeckData data = ref.getCyberdeckData();
		if (data==null)
			return;
		
		model.setAttribute(ItemAttribute.DATA_PROCESSING, data.getDataProcessing() );
		model.setAttribute(ItemAttribute.FIREWALL       , data.getFirewall() );
		model.setAttribute(ItemAttribute.ATTACK         , data.getAttack() );
		model.setAttribute(ItemAttribute.SLEAZE         , data.getSleaze() );
		model.setAttribute(ItemAttribute.CONCURRENT_PROGRAMS, data.getPrograms() );
	}

	//--------------------------------------------------------------------
	private void copyAmmunitionData(CarriedItem model, ItemTemplate ref, List<Multiply> multi) {
		/*
		 * Not in the mood to convert all AmmunitionData attributes into 
		 * ItemAttributes, if there is no way to influence them.
		 * This may change in the future.
		 */
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed) {
		prefix = indent+model.getItem().getId()+": ";
		ItemTemplate ref = model.getItem();
		try {
			
			// For easier access, prepare a list of things to multiply with eventually existing rating
			List<Multiply> multi = new ArrayList<ItemTemplate.Multiply>();
			if (ref.hasRating() && ref.getMultiplyWithRate()!=null)
				multi = Arrays.asList(ref.getMultiplyWithRate());
			
			/*
			 * 1. Copy data from ItemTemplates into attribute map
			 */
			copyBasicData(model, ref, multi);
			copyWeaponData(model, ref, multi);
			copyUsageData(model, ref, multi);
			copyVehicleData(model, ref, multi);
			copyArmorData(model, ref, multi);
			copyRiggerConsoleData(model, ref, multi);
			copyCyberdeckData(model, ref, multi);
			copyAmmunitionData(model, ref, multi);
			
			if (model.getChoice()==null && model.getChoiceReference()!=null) {
				model.setChoice(ShadowrunTools.resolveChoiceType(model.getItem().getChoice(), model.getChoiceReference()));
			}
			/*
			 * Copy modifications
			 * Where necessary provide an rating
			 */
			for (Modification mod : ref.getModifications()) {
				Modification realMod = ShadowrunTools.instantiateModification(mod, model.getChoice(), ref.hasRating()?model.getRating():1);
				logger.debug(prefix+"modification from template (with rating): "+realMod);
				unprocessed.add(realMod);
			}
			for (Modification mod : model.getAutoModifications()) {
				logger.debug(prefix+"auto-added modification  "+mod);
				unprocessed.add(mod);				
			}
			
			/*
			 * Copy requirements
			 */
			for (Requirement req : ref.getRequirements()) {
				Requirement realReq = ShadowrunTools.instantiateRequirement(req, null, ref.hasRating()?model.getRating():1);
				logger.debug(prefix+"requirement from template (with rating): "+realReq);
				model.addRequirement(realReq);
			}
		} finally {
			
		}
		return unprocessed;
	}

}

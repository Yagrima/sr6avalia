/**
 * 
 */
package org.prelle.shadowrun6.items.proc;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemHook;

import de.rpgframework.genericrpg.ToDoElement;
import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class ApplyUserAddedAccessories implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> previous) {
		String prefix = indent+model.getItem().getId()+": ";

		try {
			for (CarriedItem accessory : model.getUserAddedAccessories()) {
				ItemHook hook = accessory.getSlot();
				if (hook==null) {
					model.addToDo(new ToDoElement(Severity.WARNING, Resource.format(
							ShadowrunCore.getI18nResources(), 
							"todos.carrieditem.accessory_without_slot", 
							accessory.getName())));
				} else {
					AvailableSlot slot = model.getSlot(hook);
					if (slot==null) {
						if (hook==ItemHook.FIREARMS_EXTERNAL) {
							
						} else {
							// Assigned to non-existing slot
							logger.debug(prefix+accessory.getItem().getId()+" is assigned to non-existing slot "+accessory.getSlot());
							model.addToDo(new ToDoElement(Severity.WARNING, Resource.format(
									ShadowrunCore.getI18nResources(), 
									"todos.carrieditem.accessory_in_invalid_slot", 
									accessory.getName(), hook.getName())));
						}
					} else {
						// Slot exists
						logger.info("Assign user-accessory "+accessory.getItem().getId()+" to slot "+hook);
						accessory.refreshVirtual();
						slot.addEmbeddedItem(accessory);
					}						
				}
			}
		} finally {
			
		}
		return previous;
	}

}

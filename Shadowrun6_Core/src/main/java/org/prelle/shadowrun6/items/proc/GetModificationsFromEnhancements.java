package org.prelle.shadowrun6.items.proc;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemEnhancementValue;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public class GetModificationsFromEnhancements implements CarriedItemProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.items.proc");

	//--------------------------------------------------------------------
	/**
	 */
	public GetModificationsFromEnhancements() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.items.proc.CarriedItemProcessor#process(java.lang.String, org.prelle.shadowrun6.items.CarriedItem, java.util.List)
	 */
	@Override
	public List<Modification> process(String indent, CarriedItem model, List<Modification> unprocessed) {
		String prefix = indent+model.getItem().getId()+": ";
		
		for (ItemEnhancementValue enhancement : model.getEnhancements()) {
			List<Modification> mods = enhancement.getModifications();
			if (!mods.isEmpty()) {
				logger.debug(prefix+"Modifications from "+enhancement.getModifyable().getId()+" = "+mods);
				unprocessed.addAll(mods);
			}
		}
		return unprocessed;
	}

}

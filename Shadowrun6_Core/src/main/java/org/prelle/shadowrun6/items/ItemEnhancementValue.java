package org.prelle.shadowrun6.items;

import java.util.ArrayList;
import java.util.List;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

public class ItemEnhancementValue extends ModifyableImpl {

	private ItemEnhancement ref;

	//--------------------------------------------------------------------
	public ItemEnhancementValue(ItemEnhancement ref) {
		this.ref = ref;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return ref.toString();
	}

	//--------------------------------------------------------------------
	public ItemEnhancement getModifyable() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ModifyableImpl#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		return ref.getModifications();
	}

}

/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.requirements.Requirement;
import org.prelle.shadowrun6.requirements.RequirementList;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class Quality extends BasePluginData implements Comparable<Quality> {

	private static Logger logger = LogManager.getLogger("shadowrun");
	
	public enum QualityType {
		POSITIVE,
		NEGATIVE,
		;
		public String getName() { return Resource.get(ShadowrunCore.getI18nResources(),"qualitytype."+name().toLowerCase()); }
	}
	
	@Attribute
	private String id;
	@Attribute
	private int karma;
	@Attribute
	private QualityType type;
	@Attribute
	private int max;
	@Attribute(name="multi")
	private boolean multi;
	@Attribute
	private ChoiceType select;
	@Element
	private ModificationList modifications;
	@Element
	private RequirementList requires;
	@Attribute
	private boolean generationOnly;
	@Attribute
	private int generationMax;
	@Attribute(name="meta")
	private boolean metagenetic;
	@Attribute(name="selectable")
	private boolean freeSelectable;

	//-------------------------------------------------------------------
	/**
	 */
	public Quality() {
		modifications = new ModificationList();
		requires      = new RequirementList();
		freeSelectable= true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Quality o) {
		if (type.ordinal()!=o.getType().ordinal())
			return ((Integer)type.ordinal()).compareTo(o.getType().ordinal());
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
		if (i18n==null) 
			return id;
		try {
			return i18n.getString("quality."+id);
		} catch (MissingResourceException e) {
			if (!reportedKeys.contains(e.getKey())) {
				reportedKeys.add(e.getKey());
				logger.warn(String.format("key missing:    %s   %s", i18n.getBaseBundleName(), e.getKey()));
				if (MISSING!=null) {
					MISSING.println(e.getKey()+"=");
				}
			}
			return e.getKey();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Return a shortend version of the name if present, otherwise the normal 
	 * name
	 */
	public String getShortName() {
		if (i18n==null) 
			return id;
		String key = "quality."+id+".short";
		if (i18n.containsKey(key))
			return i18n.getString(key);
		else
			return getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "quality."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "quality."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cost
	 */
	public int getCost() {
		return karma;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public QualityType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		return modifications;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	//-------------------------------------------------------------------
	public boolean needsChoice() {
//		for (Modification mod : modifications) {
//			if (mod instanceof OptionModification)
//				return true;
//		}
//		return false;
		return select!=null;
	}

//	//-------------------------------------------------------------------
//	public OptionModification.Type getOptionType() {
//		for (Modification mod : modifications) {
//			if (mod instanceof OptionModification)
//				return ((OptionModification)mod).getType();
//		}
//		throw new NoSuchElementException();
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the multipleSelectable
	 */
	public boolean isMultipleSelectable() {
		return multi;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the select
	 */
	public ChoiceType getSelect() {
		return select;
	}

	public static void setLogger(Logger logger) {
		Quality.logger = logger;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setKarma(int karma) {
		this.karma = karma;
	}

	public void setType(QualityType type) {
		this.type = type;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public void setMultipleSelectable(boolean multipleSelectable) {
		this.multi = multipleSelectable;
	}

	public void setSelect(ChoiceType select) {
		this.select = select;
	}

	public void addRequirements(List<Requirement> value) {
		requires.addAll(value);
	}

	public void addRequirement(Requirement value) {
		requires.add(value);
	}

	public List<Requirement> getPrerequisites() {
		return new ArrayList<Requirement>(requires);
	}

	public boolean isGenerationOnly() {
		return generationOnly;
	}

	public void setGenerationOnly(boolean generationOnly) {
		this.generationOnly = generationOnly;
	}

	public int getGenerationMax() {
		return generationMax;
	}

	public void setGenerationMax(int generationMax) {
		this.generationMax = generationMax;
	}

	public boolean isMetagenetic() {
		return metagenetic;
	}

	public void setMetagenetic(boolean metagenetic) {
		this.metagenetic = metagenetic;
	}

	//-------------------------------------------------------------------
	public boolean isFreeSelectable() {
		return freeSelectable;
	}

	//-------------------------------------------------------------------
	public void setFreeSelectable(boolean freeSelectable) {
		this.freeSelectable = freeSelectable;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.modifications;

import java.util.Date;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public abstract class ModificationBase<T> implements ValueModification<T> {
	
	@org.prelle.simplepersist.Attribute
	protected int expCost;
	@org.prelle.simplepersist.Attribute
	protected Date date;
	protected transient Object source;
	
	protected T modifiedItem;

	//--------------------------------------------------------------------
	public ModificationBase() {
	}

	//-------------------------------------------------------------------
    @SuppressWarnings("unchecked")
	public ValueModification<T> clone() {
    	try {
    		return (ValueModification<T>) super.clone();
    	} catch ( CloneNotSupportedException e ) {
    		throw new InternalError();
    	}
    }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification other) {
		return date.compareTo(other.getDate());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {		
		return expCost;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.genericrpg.modifications.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
		this.expCost = expCost;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public T getModifiedItem() {
		return modifiedItem;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="specrulemod")
public class SpecialRuleModification implements Modification {
	
	public enum Rule {
		/**
		 * high_pain_tolerance / Hohe Schmerztoleranz
		 * Pool reduction by damage is 1 point later
		 */
		PAIN_TOLERANCE_HIGH,
		/**
		 * low_pain_tolerance / Niedrige Schmerztoleranz
		 * Pool reduction for every 2 points of damage
		 */
		PAIN_TOLERANCE_LOW,
		/**
		 * will_to_live / Überlebenswille
		 * Fields for Additional damage below 0
		 */
		DAMAGE_OVERFLOW,
		/** Gain 5000 Nuyen for 1 Karma */
		IN_DEBT,
	}
	
	@Attribute
	private Rule ref;
	@Attribute(name="lvl")
	private int level;
	
	private transient Object source;

	//-------------------------------------------------------------------
	public SpecialRuleModification() {
	}

	//-------------------------------------------------------------------
	public SpecialRuleModification(Rule type) {
		this.ref = type;
	}

	//-------------------------------------------------------------------
	public SpecialRuleModification(Rule type, int val) {
		this.ref = type;
		this.level = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
			return "Rule "+ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification o) {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		return new SpecialRuleModification(ref);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	public Rule getRule() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//-------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

}

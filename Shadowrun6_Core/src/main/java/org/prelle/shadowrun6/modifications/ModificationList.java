/**
 *
 */
package org.prelle.shadowrun6.modifications;

import java.util.ArrayList;
import java.util.Collection;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="modifications")
@ElementListUnion({
    @ElementList(entry="accessorymod", type=AccessoryModification.class),
    @ElementList(entry="addlstylemod", type=AddLifestyleModification.class),
    @ElementList(entry="allowmod", type=AllowModification.class),
    @ElementList(entry="attrmod", type=AttributeModification.class),
    @ElementList(entry="cformmod", type=ComplexFormModification.class),
    @ElementList(entry="dmgtypemod", type=DamageTypeModification.class),
    @ElementList(entry="edgemod", type=EdgeModification.class),
    @ElementList(entry="focusmod", type=FocusModification.class),
    @ElementList(entry="forbidmod", type=ForbidModification.class),
    @ElementList(entry="itemmod", type=CarriedItemModification.class),
    @ElementList(entry="itemattrmod", type=ItemAttributeModification.class),
    @ElementList(entry="itemhookmod", type=ItemHookModification.class),
    @ElementList(entry="lifecostmod", type=LifestyleCostModification.class),
    @ElementList(entry="lifestylemod", type=LifestyleModification.class),
    @ElementList(entry="metaechomod", type=MetamagicOrEchoModification.class),
    @ElementList(entry="nuyenmod", type=AddNuyenModification.class),
    @ElementList(entry="optionmod", type=OptionModification.class),
    @ElementList(entry="powermod", type=AdeptPowerModification.class),
    @ElementList(entry="qualitymod", type=QualityModification.class),
    @ElementList(entry="relevancemod", type=RelevanceModification.class),
    @ElementList(entry="ritualmod", type=RitualModification.class),
    @ElementList(entry="selmod", type=ModificationChoice.class),
    @ElementList(entry="sinmod", type=SINModification.class),
    @ElementList(entry="skillmod", type=SkillModification.class),
    @ElementList(entry="skillspecmod", type=SkillSpecializationModification.class),
    @ElementList(entry="specrulemod", type=SpecialRuleModification.class),
    @ElementList(entry="spellmod", type=SpellModification.class),
 })
public class ModificationList extends ArrayList<Modification> {

	//-------------------------------------------------------------------
	public ModificationList() {
	}

	//-------------------------------------------------------------------
	/**
	 * @param c
	 */
	public ModificationList(Collection<? extends Modification> c) {
		super(c);
		}


}

/**
 * 
 */
package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.Lifestyle;
import org.prelle.shadowrun6.persist.LifestyleConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class AddLifestyleModification implements Modification {

	@Attribute
	@AttribConvert(LifestyleConverter.class)
	private Lifestyle type;
	@org.prelle.simplepersist.Attribute
	protected Date date;
	
	protected transient Object source;
	
	//-------------------------------------------------------------------
	public AddLifestyleModification() {
	}
	
	//-------------------------------------------------------------------
	public AddLifestyleModification(Lifestyle type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#getDate()
	 */
	@Override
	public Date getDate() {
		return date;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.Datable#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification other) {
		return date.compareTo(other.getDate());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#clone()
	 */
	@Override
	public Modification clone() {
		AddLifestyleModification cloned = new AddLifestyleModification(type);
		cloned.setDate(date);
		return cloned;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Lifestyle getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Lifestyle type) {
		this.type = type;
	}

}

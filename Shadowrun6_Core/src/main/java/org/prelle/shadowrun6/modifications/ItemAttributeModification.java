package org.prelle.shadowrun6.modifications;

import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ItemAttributeModification extends ModificationBase<ItemAttribute> {

	@Attribute
    private ItemAttribute attr;
	@Attribute
    private int val;
	@Attribute(name="cond",required=false)
    private Boolean conditional = null;
	/**
	 * Item type to limit this modification
	 */
	@Attribute(name="type",required=false)
    private ItemType type;
	@Attribute(name="subtype",required=false)
    private ItemSubType subtype;
	@Attribute
    private String limitToItem;
	/**
	 * Skill an item must use when this skill shall apply
	 */
	@Attribute(name="skill",required=false)
	@AttribConvert(SkillConverter.class)
    private Skill itemSkill;
	/**
	 * Item hook to limit this modification
	 */
	@Attribute(name="slot",required=false)
    private ItemHook slot;

	private transient Boolean included = null;
	   
    //-----------------------------------------------------------------------
    public ItemAttributeModification() {
    }

    //-----------------------------------------------------------------------
    public ItemAttributeModification(ItemAttribute attr, int val) {
        this.attr = attr;
        this.val  = val;
    }

//    //-------------------------------------------------------------------
//    /**
//     * @see org.prelle.shadowrun6.modifications.ModificationBase#clone()
//     */
//    @Override
//	public ValueModification<ItemAttribute> clone() {
//   		return new ItemAttributeModification(attr, val);
//     }

	//-------------------------------------------------------------------
 	public ItemAttributeModification clone() {
//    	try {
    		return (ItemAttributeModification) super.clone();
//    	} catch ( CloneNotSupportedException e ) {
//    		throw new InternalError();
//    	}
    }

    //-----------------------------------------------------------------------
    public String toString() {
    	String effect = attr+"/"+((val<0)?(" "+val):(" +"+val));
    	if (conditional!=null && conditional)
    		effect+=" (cond.)";
    	if (limitToItem!=null)
    		effect +=" limit to "+limitToItem;
    	if (type!=null)
    		effect += " type="+type;
    	if (subtype!=null)
    		effect += " subtype="+subtype;
    	if (included!=null && included)
    		effect += " already included";
   		effect += " from "+source;
    	
    	return effect;
    }

    //-----------------------------------------------------------------------
    public ItemAttribute getAttribute() {
        return attr;
    }

    //-----------------------------------------------------------------------
    public void setAttribute(ItemAttribute attr) {
        this.attr = attr;
    }

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public ItemAttribute getModifiedItem() {
		return attr;
	}

    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }

    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }

//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof ItemAttributeModification) {
            ItemAttributeModification amod = (ItemAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return (amod.getValue()==val);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof ItemAttributeModification) {
            ItemAttributeModification amod = (ItemAttributeModification)o;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof ItemAttributeModification))
            return toString().compareTo(obj.toString());
        ItemAttributeModification other = (ItemAttributeModification)obj;
        if (attr!=other.getAttribute())
            return (Integer.valueOf(attr.ordinal())).compareTo(Integer.valueOf(other.getAttribute().ordinal()));
        return 0;
    }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		if (conditional==null) return false;
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the included
	 */
	public boolean isIncluded() {
		return (included!=null)?included:false;
	}

	//-------------------------------------------------------------------
	/**
	 * @param included the included to set
	 */
	public void setIncluded(Boolean included) {
		this.included = included;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ItemType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(ItemType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the subtype
	 */
	public ItemSubType getSubtype() {
		return subtype;
	}

	//-------------------------------------------------------------------
	/**
	 * @param subtype the subtype to set
	 */
	public void setSubtype(ItemSubType subtype) {
		this.subtype = subtype;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return slot;
	}

}// ItemAttributeModification

package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.persist.ItemConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="accessorymod")
public class AccessoryModification implements Modification {

	@Attribute
    private ItemHook hook;
	@Attribute
	@AttribConvert(ItemConverter.class)
    private ItemTemplate item;
	private transient Object source;
	@Attribute
    private int rating;
	@Attribute
    private Boolean included;

    //-----------------------------------------------------------------------
    public AccessoryModification() {
    }

    //-----------------------------------------------------------------------
    public AccessoryModification(ItemHook hook, ItemTemplate item, int rating, Object source) {
        this.hook = hook;
        this.item = item;
        this.rating = rating;
        this.source = source;
    }

    //-----------------------------------------------------------------------
    public String toString() {
           return "Deploy "+hook+"="+item;
    }

    //-----------------------------------------------------------------------
    public ItemHook getHook() {
        return hook;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof AccessoryModification) {
            AccessoryModification amod = (AccessoryModification)o;
            if (amod.getHook()!=hook) return false;
            return (amod.getItem()==item);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof AccessoryModification) {
            AccessoryModification amod = (AccessoryModification)o;
            if (amod.getHook()!=hook) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof AccessoryModification))
            return toString().compareTo(obj.toString());
        AccessoryModification other = (AccessoryModification)obj;
        if (hook!=other.getHook())
            return (Integer.valueOf(hook.ordinal())).compareTo(Integer.valueOf(other.getHook().ordinal()));
        return 0;
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#clone()
	 */
	@Override
	public Modification clone() {
		return new AccessoryModification(hook, item, rating, source);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public ItemTemplate getItem() {
		return item;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rating
	 */
	public int getRating() {
		return rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the included
	 */
	public boolean isIncluded() {
		return (included!=null)?included:false;
	}

	//-------------------------------------------------------------------
	/**
	 * @param included the included to set
	 */
	public void setIncluded(Boolean included) {
		this.included = included;
	}

}

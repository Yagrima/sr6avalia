package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.items.Damage;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="dmgtypemod")
public class DamageTypeModification implements Modification {

	@Attribute
    private Damage.Type type;

	private transient Object source;

    //-----------------------------------------------------------------------
    public DamageTypeModification() {
    }

    //-----------------------------------------------------------------------
    public DamageTypeModification(Damage.Type type) {
    	this.type = type;
    }

    //-----------------------------------------------------------------------
    public String toString() {
           return "Damage="+type;
    }
    
    //-----------------------------------------------------------------------
    public Damage.Type getType() {
    	return type;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof DamageTypeModification) {
            DamageTypeModification amod = (DamageTypeModification)o;
            return amod.getType()==type; 
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof DamageTypeModification))
            return toString().compareTo(obj.toString());
        DamageTypeModification other = (DamageTypeModification)obj;
        return type.compareTo(other.getType());
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#clone()
	 */
	@Override
	public Modification clone() {
		return new DamageTypeModification(type);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

}

package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class RelevanceModification implements Modification {

	public enum RelevanceType {
		COMBAT,
		SOCIAL,
		MAGICAL,
		MATRIX
	}

	@Attribute
	private RelevanceType topic;
	protected transient Object source;

	//-----------------------------------------------------------------------
	public RelevanceModification() {
	}

	//-----------------------------------------------------------------------
	/**
	 */
	public boolean equals(Object o) {
		if (o instanceof RelevanceModification) {
			RelevanceModification amod = (RelevanceModification)o;
			if (amod.getType()     !=topic) return false;
			if (amod.getSource()   !=source) return false;
			return true;
		} else
			return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Relevance("+topic+") (from "+source+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public RelevanceType getType() {
		return topic;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(RelevanceType type) {
		this.topic = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.Modification#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	@Override
	public Date getDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDate(Date date) {
		// TODO Auto-generated method stub

	}

	@Override
	public int compareTo(Modification o) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Modification clone() {
    	try {
    		return (RelevanceModification) super.clone();
    	} catch ( CloneNotSupportedException e ) {
    		throw new InternalError();
    	}
	}

	@Override
	public int getExpCost() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setExpCost(int expCost) {
		// TODO Auto-generated method stub

	}

}// EdgeModification

package org.prelle.shadowrun6.modifications;

import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.persist.FocusConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class FocusModification extends ModificationBase<Focus> {

	@Attribute
	@AttribConvert(FocusConverter.class)
	private Focus ref;
	@Attribute
	private int value;
	@Attribute
	private String choice;
	@Attribute(name="choice2")
	private String choicesChoice;
	@Attribute(name="uuid")
	private String focusUUID;
    
    //-----------------------------------------------------------------------
    public FocusModification() {
    }
    
    //-----------------------------------------------------------------------
    public FocusModification(Focus ref, int val) {
    	if (ref==null) throw new NullPointerException("Focus may not be null");
    	this.ref = ref;
        this.value = val;
    }
    
    //-----------------------------------------------------------------------
    public FocusModification(Focus ref, int val, String choice) {
    	if (ref==null) throw new NullPointerException("Focus may not be null");
    	this.ref = ref;
        this.value = val;
        this.choice = choice;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
          if (ref!=null) {
        	  if (ref.needsChoice())
        		  return ref.getId()+"("+choice+")";
        	 return ref.getId();
          }
         return "Any focus";
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof FocusModification) {
        	FocusModification amod = (FocusModification)o;
            if (amod.getModifiedItem()!=ref)
            	return false;
            if (ref.needsChoice() && !amod.getChoice().equals(choice))
            	return false;
             return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
            return toString().compareTo(obj.toString());
     }

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getValue()
	 */
	@Override
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#setValue(int)
	 */
	@Override
	public void setValue(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.modification.ValueModification#getModifiedItem()
	 */
	@Override
	public Focus getModifiedItem() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public String getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(String choice) {
		this.choice = choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choicesChoice
	 */
	public String getChoicesChoice() {
		return choicesChoice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choicesChoice the choicesChoice to set
	 */
	public void setChoicesChoice(String choicesChoice) {
		this.choicesChoice = choicesChoice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the focusUUID
	 */
	public String getFocusUUID() {
		return focusUUID;
	}

	//-------------------------------------------------------------------
	/**
	 * @param focusUUID the focusUUID to set
	 */
	public void setFocusUUID(String focusUUID) {
		this.focusUUID = focusUUID;
	}
    
}// AttributeModification

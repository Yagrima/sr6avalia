package org.prelle.shadowrun6.modifications;

import java.util.Date;

import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
@Root(name="itemhookmod")
public class ItemHookModification implements Modification {

	@Attribute(required=true)
    private ItemHook hook;
	@Attribute
    private int capacity;
	@Attribute
    private boolean remove;
	private transient Object source;

    //-----------------------------------------------------------------------
    public ItemHookModification() {
    }

    //-----------------------------------------------------------------------
    public ItemHookModification(ItemHook hook, int val) {
        this.hook = hook;
        this.capacity  = val;
    }

    //-----------------------------------------------------------------------
    public String toString() {
    	String actionName = remove?"RemoveHook":"AddHook";
    	if (capacity<=0)
    		return actionName+" "+hook;
    	else
       		return actionName+" "+hook+" (Cap="+capacity+")";
    }

    //-----------------------------------------------------------------------
    public ItemHook getHook() {
        return hook;
    }

    //-----------------------------------------------------------------------
    public int getCapacity() {
        return capacity;
    }

    //-----------------------------------------------------------------------
    public void setCapacity(int val) {
        this.capacity = val;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof ItemHookModification) {
            ItemHookModification amod = (ItemHookModification)o;
            if (amod.getHook()!=hook) return false;
            return (amod.getCapacity()==capacity);
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof ItemHookModification) {
            ItemHookModification amod = (ItemHookModification)o;
            if (amod.getHook()!=hook) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof ItemHookModification))
            return toString().compareTo(obj.toString());
        ItemHookModification other = (ItemHookModification)obj;
        if (hook!=other.getHook())
            return (Integer.valueOf(hook.ordinal())).compareTo(Integer.valueOf(other.getHook().ordinal()));
        return 0;
    }

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getDate()
	 */
	@Override
	public Date getDate() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setDate(java.util.Date)
	 */
	@Override
	public void setDate(Date date) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#clone()
	 */
	@Override
	public Modification clone() {
		return new ItemHookModification(hook, capacity);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getExpCost()
	 */
	@Override
	public int getExpCost() {
		return 0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setExpCost(int)
	 */
	@Override
	public void setExpCost(int expCost) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#getSource()
	 */
	@Override
	public Object getSource() {
		return source;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.modifications.ModificationBase#setSource(java.lang.Object)
	 */
	@Override
	public void setSource(Object src) {
		this.source = src;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the remove
	 */
	public boolean isRemove() {
		return remove;
	}

	//-------------------------------------------------------------------
	/**
	 * @param remove the remove to set
	 */
	public void setRemove(boolean remove) {
		this.remove = remove;
	}


}//

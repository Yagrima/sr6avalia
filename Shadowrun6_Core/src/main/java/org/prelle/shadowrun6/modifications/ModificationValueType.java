/**
 * 
 */
package org.prelle.shadowrun6.modifications;

/**
 * @author prelle
 *
 */
public enum ModificationValueType {

	/** Current Natural */
	NATURAL,
	/** Natural max */
	MAX,
	AUGMENTED,
	ALTERNATE,
	
}

/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.proc.ItemRecalculation;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromEquipment implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc.items");
	
	//-------------------------------------------------------------------
	public GetModificationsFromEquipment() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Apply modifications by qualities
//			logger.debug("2. Apply modifications from qualities");
			for (CarriedItem ref :model.getItems(false)) {
				// Ensure item usage type is present
				if (ref.getUsedAsType()==null && ref.getSlot()==null) {
					ref.setUsedAsType(ref.getItem().getNonAccessoryType());
					ref.setUsedAsSubType(ref.getItem().getSubtype(ref.getUsedAsType()));
					logger.warn("Missing usage for '"+ref.getNameWithRating()+"' - assume "+ref.getUsedAsType()+"/"+ref.getUsedAsSubType());
					ItemRecalculation.recalculate("", ref);
				}
				
				logger.debug("ITEM "+ref+" / "+ref.getCharacterModifications());
				if (ref.getModifications()!=null && !ref.getCharacterModifications().isEmpty()) {
					logger.info(" - "+ref.getItem().getId()+" has modifications: "+ref.getModifications());
					unprocessed.addAll(ref.getModifications());
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

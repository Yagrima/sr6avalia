/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.RelevanceModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyRelevanceAndEdgeMods implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6");
	
	//-------------------------------------------------------------------
	public ApplyRelevanceAndEdgeMods() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process QualityModifications
			for (Modification tmp : previous) {
				if (tmp instanceof EdgeModification) {
					EdgeModification mod = (EdgeModification)tmp;
					if (mod.getSkill()!=null) {
						SkillValue value = model.getSkillValue(mod.getSkill());
						if (value!=null) {
							logger.info("Apply EdgeModifcation from "+mod.getSource()+" to skill "+mod.getSkill()+": "+mod);
							value.addModification(mod);
						} else {
							logger.debug("Cannot apply EdgeMod to skill "+mod.getSkill().getId()+" - character does not have this skill");
						}
					} else if (mod.getSkillType()!=null) {
						for (SkillValue value : ShadowrunTools.getAllSkillValues(model, mod.getSkillType())) {
							logger.info("Apply EdgeModifcation from "+mod.getSource()+" to skill "+value.getModifyable());
							value.addModification(mod);
						}
					} else if (mod.getSpellCategory()!=null) {
						for (SpellValue spell : model.getSpells()) {
							if (spell.getModifyable().getCategory()==mod.getSpellCategory()) {
								logger.info("Apply EdgeModifcation from "+mod.getSource()+" to spell '"+spell.getModifyable()+"': "+mod);
								spell.addModification(mod); 
							}
						}
					} else {
						System.err.println("ApplyRelevanceAndEdgeMods: NOT IMPLEMENTED: process EdgeMod "+mod);
						logger.warn("NOT IMPLEMENTED: process EdgeMod "+mod);
					}
				} else if (tmp instanceof RelevanceModification) {
					RelevanceModification mod = (RelevanceModification)tmp;
					model.addRelevanceModification(mod);
					logger.info("Apply RelevanceModification "+mod);
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

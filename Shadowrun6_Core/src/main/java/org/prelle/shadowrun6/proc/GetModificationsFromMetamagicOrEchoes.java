/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromMetamagicOrEchoes implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	public GetModificationsFromMetamagicOrEchoes() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Calculate effective modifications from available powers
			for (MetamagicOrEchoValue ref :model.getMetamagicOrEchoes()) {
				logger.debug("  add from metamagic/echo "+ref.getModifyable().getId());
				// Calculate modifications
				ref.clearModifications();
				int multiplier = (ref.getModifyable().hasLevels())?ref.getLevel():1;
				for (Modification mod : ref.getModifyable().getModifications()) {
					Modification realMod = ShadowrunTools.instantiateModification(mod, null, multiplier);
					ref.addModification(realMod);
				}
				
				
				
				if (ref.getModifications()!=null && !ref.getModifications().isEmpty()) {
					logger.info(" - "+ref.getModifyable().getId()+" has modifications: "+ref.getModifications());
					for (Modification mod : ref.getModifications()) {
						mod.setSource(ref.getModifyable());
					}
//					logger.debug(" - add modifications: "+ref.getModifications());
					unprocessed.addAll(ref.getModifications());
				}
			}
			
			// Raise resonance or magic
			if (model.getInitiateSubmersionLevel()>0) {
				if (model.getMagicOrResonanceType().usesMagic()) {
					unprocessed.add(new AttributeModification(ModificationValueType.MAX, Attribute.MAGIC, model.getMetamagicOrEchoes().size()));
				} else if (model.getMagicOrResonanceType().usesResonance()) {
					unprocessed.add(new AttributeModification(ModificationValueType.MAX, Attribute.RESONANCE, model.getMetamagicOrEchoes().size()));
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

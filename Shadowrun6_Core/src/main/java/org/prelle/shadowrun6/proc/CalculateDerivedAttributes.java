/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.Damage;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeNumericalValue;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.AttributeModification.ModificationType;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CalculateDerivedAttributes implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.proc");

	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Apply attribute modifications
			for (Modification _mod : previous) {
				if (_mod instanceof AttributeModification) {
					AttributeModification mod = (AttributeModification)_mod;
					if (mod.getType()==ModificationValueType.MAX) {
						unprocessed.add(mod);
					} else {
						AttributeValue val = model.getAttribute(mod.getAttribute());
						logger.debug("  Add "+mod+" from "+mod.getSource());
						val.addModification(mod);
					}
				} else if (_mod instanceof ItemAttributeModification) {
					ItemAttributeModification mod = (ItemAttributeModification)_mod;
					for (CarriedItem item : model.getItems(true)) {
						logger.debug("check "+mod+" on "+item);
						logger.debug("item = "+item.getUsedAsType()+"   "+item.getItem().getNonAccessoryType()+"/"+item.getItem().getSubtype(null)+"   slot="+item.getSlot());
						if (mod.getType()!=null && mod.getType()!=item.getUsedAsType())
							continue;
						if (mod.getSubtype()!=null && mod.getSubtype()!=item.getUsedAsSubType())
							continue;
						logger.info("Apply "+mod+" to "+item);
						if (item.getModifications().size()>30) {
							logger.fatal("STOP HERE - too many modifications");
							System.exit(1);
						}
						item.addAutoModification(mod);
					}
				} else
					unprocessed.add(_mod);
			}

			// Set character back to zero
			//			logger.debug("1. Calculate derived attributes");
			AttributeValue val = null;

			/*
			 * Physical condition monitor
			 */
			int phy = Math.round(model.getAttribute(Attribute.BODY).getModifiedValue()/2.0f) + 8;
			val = model.getAttribute(Attribute.PHYSICAL_MONITOR);
			val.setPoints(phy);
			logger.debug(" Monitor Physical = "+val.getModifiedValue()+"    modifier="+val.getModifier());

			/*
			 * Stun condition monitor
			 */
			int stun = Math.round(model.getAttribute(Attribute.WILLPOWER).getModifiedValue()/2.0f) + 8;
			val = model.getAttribute(Attribute.STUN_MONITOR);
			val.setPoints(stun);
			logger.debug(" Monitor Stun     = "+val.getModifiedValue());

			/*
			 * Physical initiative
			 */
			val = model.getAttribute(Attribute.INITIATIVE_PHYSICAL);
			val.setPoints(0);
			val.addNaturalModifier(model.getAttribute(Attribute.REACTION).getAugmentedValue(), Attribute.REACTION);
			val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			logger.debug(" INI Physical = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getAugmentedValue()+" d6");
			// Minor actions (Physical)
			val = model.getAttribute(Attribute.MINOR_ACTION);
			val.setPoints(1);
			val.addNaturalModifier(model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getAugmentedValue(), Attribute.INITIATIVE_DICE_PHYSICAL);
			logger.debug("              = "+val.getDisplayString()+"   "+val.getModifications());

			/*
			 * astral initiative
			 */
			val = model.getAttribute(Attribute.INITIATIVE_ASTRAL);
			val.setPoints(0);
			val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
			val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			logger.debug(" Base INI Astral = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getAugmentedValue()+" d6");
			// Minor actions (Astral)
			val = model.getAttribute(Attribute.MINOR_ACTION_ASTRAL);
			val.setPoints(1);
			val.addNaturalModifier(model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getAugmentedValue(), Attribute.INITIATIVE_DICE_ASTRAL);
			logger.debug("                 = "+val.getDisplayString()+"   "+val.getModifications());

			/*
			 * matrix initiative (AR)
			 */
			val = model.getAttribute(Attribute.INITIATIVE_MATRIX);
			val.setPoints(0);
			CarriedItem bestDF = ShadowrunTools.getBestMatrixDF(model);
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesResonance()) {
				// Technomancers
				val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
				val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			} else if (bestDF!=null) {
				// With commlink
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX, model.getAttribute(Attribute.REACTION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.REACTION));
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			} 
			logger.debug(" Base INI Matrix = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getAugmentedValue()+" d6");
			// Minor actions (Matrix)
			val = model.getAttribute(Attribute.MINOR_ACTION_MATRIX);
			val.setPoints(1 + model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getAugmentedValue());
			logger.debug("                 = "+val.getDisplayString()+"   "+val.getModifications());

			/*
			 * matrix initiative (VR, cold sim)
			 */
			val = model.getAttribute(Attribute.INITIATIVE_MATRIX_VR_COLD);
			val.setPoints(0);
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesResonance()) {
				// Technomancers
				val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
				val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			} else if (bestDF!=null) {
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_COLD, bestDF.getAsValue(ItemAttribute.DATA_PROCESSING).getModifiedValue(), ModificationType.RELATIVE, ItemAttribute.DATA_PROCESSING));
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_COLD, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			} 
			logger.debug(" Base INI Matrix VR = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_COLD).getAugmentedValue()+" d6");

			/*
			 * matrix initiative (VR, cold sim)
			 */
			val = model.getAttribute(Attribute.INITIATIVE_MATRIX_VR_HOT);
			val.setPoints(0);
			if (model.getMagicOrResonanceType()!=null && model.getMagicOrResonanceType().usesResonance()) {
				// Technomancers
				val.addNaturalModifier(model.getAttribute(Attribute.LOGIC).getAugmentedValue(), Attribute.LOGIC);
				val.addNaturalModifier(model.getAttribute(Attribute.INTUITION).getAugmentedValue(), Attribute.INTUITION);
			} else if (bestDF!=null) {
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_HOT, bestDF.getAsValue(ItemAttribute.DATA_PROCESSING).getModifiedValue(), ModificationType.RELATIVE, ItemAttribute.DATA_PROCESSING));
				val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.INITIATIVE_MATRIX_VR_HOT, model.getAttribute(Attribute.INTUITION).getAugmentedValue(), ModificationType.RELATIVE, Attribute.INTUITION));
			} 
			logger.debug(" Base INI Matrix VR Hot = "+val.getDisplayString()+" + "+model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_HOT).getAugmentedValue()+" d6");

			/*
			 * Defensive
			 */
			int sum = model.getAttribute(Attribute.REACTION).getAugmentedValue() + model.getAttribute(Attribute.INTUITION).getAugmentedValue();
			val = model.getAttribute(Attribute.DEFENSIVE_POOL);
			val.setPoints(sum);
			logger.debug(" Defensive pool = "+val.getAugmentedValue());

			/*
			 * Dodge
			 */
			if (model.getSkillValue(ShadowrunCore.getSkill("athletics"))!=null) {
				sum += model.getSkillValue(ShadowrunCore.getSkill("athletics")).getModifiedValue();
			}
			val = model.getAttribute(Attribute.DODGE);
			val.setPoints(sum);
			logger.debug(" Dodge = "+val.getAugmentedValue());

			/*
			 * Composure
			 */
			sum = model.getAttribute(Attribute.CHARISMA).getAugmentedValue() + model.getAttribute(Attribute.WILLPOWER).getAugmentedValue();
			val = model.getAttribute(Attribute.COMPOSURE);
			val.setPoints(sum);
			logger.debug(" Composure = "+val.getAugmentedValue());

			/*
			 * Judge intentions
			 */
			sum = model.getAttribute(Attribute.WILLPOWER).getAugmentedValue() + model.getAttribute(Attribute.INTUITION).getAugmentedValue();
			val = model.getAttribute(Attribute.JUDGE_INTENTIONS);
			val.setPoints(sum);
			logger.debug(" Judge Intentions = "+val.getAugmentedValue());

			/*
			 * lifting/carrying
			 */
			sum = model.getAttribute(Attribute.BODY).getAugmentedValue() + model.getAttribute(Attribute.WILLPOWER).getAugmentedValue();
			val = model.getAttribute(Attribute.LIFT_CARRY);
			val.setPoints(sum);
			logger.debug(" Lift/Carry = "+val.getAugmentedValue());

			/*
			 * memory
			 */
			sum = model.getAttribute(Attribute.LOGIC).getAugmentedValue() + model.getAttribute(Attribute.INTUITION).getAugmentedValue();
			val = model.getAttribute(Attribute.MEMORY);
			val.setPoints(sum);
			logger.debug(" Memory = "+val.getAugmentedValue());

			/*
			 * Damage Resistance
			 */
			val = model.getAttribute(Attribute.DAMAGE_RESISTANCE);
			val.addModification(new AttributeModification(ModificationValueType.NATURAL, Attribute.DAMAGE_RESISTANCE, model.getAttribute(Attribute.BODY).getAugmentedValue(), ModificationType.RELATIVE, Attribute.BODY));
			logger.debug("Damage Resistance = "+val);
			
			/*
			 * Damage overflow
			 */
			val = model.getAttribute(Attribute.DAMAGE_OVERFLOW);
			val.setPoints(0);
			val.addNaturalModifier(model.getAttribute(Attribute.BODY).getAugmentedValue(), Attribute.BODY);
			val.addNaturalModifier(model.getAttribute(Attribute.BODY).getAugmentedValue(), Attribute.BODY);
			logger.debug(" Damage overflow = "+val.getAugmentedValue());

			/*
			 * Alternate cyberlimb attributes
			 */
			val = model.getAttribute(Attribute.AGILITY);
			logger.debug(" Agility alternate = "+val.getAlternateValue());
			val = model.getAttribute(Attribute.STRENGTH);
			logger.debug(" Strength alternate = "+val.getAlternateValue());

			/*
			 * Defense rating
			 * First of all find the highest normal armor
			 */
			CarriedItem bestArmor = null;
			for (CarriedItem item : model.getItems(false)) {
				if (!item.hasAttribute(ItemAttribute.ARMOR) || item.isType(ItemType.VEHICLES) || item.isType(ItemType.DRONES))
					continue;
				item.setIgnoredForCalculations(true);
				// If no previous selection or armor is better, use it
				if (bestArmor==null || item.getAsValue(ItemAttribute.ARMOR).getModifiedValue()> bestArmor.getAsValue(ItemAttribute.ARMOR).getModifiedValue() )
					bestArmor = item;
				// Gear pieces that add armor are also allowed
				if (item.getItem().getArmorData()!=null && item.getItem().getArmorData().addsToMain())
					item.setIgnoredForCalculations(false);
			}
			if (bestArmor!=null)
				bestArmor.setIgnoredForCalculations(false);


			int defRating = model.getAttribute(Attribute.BODY).getAugmentedValue();
			logger.debug("  Base Defensive rating = "+defRating);

			for (CarriedItem item : model.getItems(true)) {
				if (!item.hasAttribute(ItemAttribute.ARMOR) || item.isIgnoredForCalculations() || item.isType(ItemType.VEHICLES) || item.isType(ItemType.DRONES))
					continue;
				ItemAttributeNumericalValue armorAtt = item.getAsValue(ItemAttribute.ARMOR);
				defRating += armorAtt.getModifiedValue();
				logger.debug("  Add Defensive rating = "+armorAtt.getModifiedValue()+" from "+item.getNameWithRating());
			}
			val = model.getAttribute(Attribute.DEFENSE_RATING);
			logger.info("Defensive rating = "+defRating+" = "+val);
			if (logger.isTraceEnabled()) {
				for (Modification mod : val.getModifications()) {
					logger.trace("#### "+mod+"  from "+mod.getSource());
				}
			}
			val.setPoints(defRating);

			/*
			 * Unarmed attacks
			 */
			int attRat = model.getAttribute(Attribute.REACTION).getAugmentedValue()+ model.getAttribute(Attribute.STRENGTH).getAugmentedValue();
			for (CarriedItem item : model.getItems(true)) {
				if (item.getItem().getId().startsWith("unarmed")) {
					item.setAttributeOverride(ItemAttribute.ATTACK_RATING, new int[] {attRat,0,0,0,0});
					// Damage is calculated later on the fly by method ShadowrunTools.getWeaponDamage
					Damage dmg = (Damage) item.getAsValue(ItemAttribute.DAMAGE);
					// Apply MELEE_DAMAGE
					for (Modification mod : model.getAttribute(Attribute.MELEE_DAMAGE).getModifications()) {
						ItemAttributeModification itemMod = new ItemAttributeModification(ItemAttribute.DAMAGE, ((AttributeModification)mod).getValue());
						itemMod.setSource(mod.getSource());
						logger.debug("  add to unarmed damage: "+itemMod);
						dmg.addModification(itemMod);
					}
					
					logger.debug("Set Unarmed attack with attack rating "+attRat+" and current damage "+dmg);
//					dmg.setPoints( Math.round( ((float)model.getAttribute(Attribute.STRENGTH).getAugmentedValue())/2.0f));
//					logger.debug("Set base damage to "+dmg.getPoints());
				}
			}
			//			CarriedItem unarmedRef = new CarriedItem(ShadowrunCore.getItem("unarmed"));
			//			unarmedRef.setAttributeOverride(ItemAttribute.ATTACK_RATING, new int[] {attRat,0,0,0,0});
			//			model.addAutoItem(unarmedRef);

		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

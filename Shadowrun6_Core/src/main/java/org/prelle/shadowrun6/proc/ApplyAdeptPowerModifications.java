/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.modifications.AdeptPowerModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyAdeptPowerModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	public ApplyAdeptPowerModifications() {
	}
	//-------------------------------------------------------------------
	public static void mergePowerModification(ShadowrunCharacter model, AdeptPowerModification mod) {
		for (AdeptPowerValue tmp : model.getUserSelectedAdeptPowers()) {
			if (tmp.getModifyable()==mod.getPower()) {
				// Does eventually required choice match?
				logger.info("Power ... "+tmp.getModifyable()+"    ref="+tmp.getChoiceReference()+"   mod.getPower="+mod.getPower());
				if (mod.getPower().needsChoice() && mod.getChoice()!=null && !mod.getChoice().equals(tmp.getChoiceReference())) {
					// No
					continue;
				}
				
				if (tmp.getModifyable().hasLevels()) {
					logger.debug("  merge existing "+tmp+" with auto "+mod+" of level "+mod.getValue());
					tmp.addModification(mod);
					return;
				} else if (tmp.getModifyable().needsChoice()) {
					// Power can be selected multiple times
					logger.debug("  merge existing "+tmp+" with auto "+mod+" leads to new instance of power");
				} else {
					// Power without level is user selected and auto added at the same time!!
					logger.warn("Power "+tmp.getModifyable()+" is user selected and also auto-selected from "+mod.getSource());
					return;
				}
			}
		}
		// Not found yet - auto-add it
		AdeptPowerValue toAdd = new AdeptPowerValue(mod.getPower(), mod);
		if (mod.getPower().needsChoice()) {
			toAdd.setChoiceReference(mod.getChoice());
			toAdd.setChoice(ShadowrunTools.resolveChoiceType(mod.getPower().getSelectFrom(), mod.getChoice()));
			logger.warn("TODO: add possibility to define auto-selected power choice");
//			if (toAdd.getChoice()==null)
//				throw new IllegalArgumentException("Could not resolve "+mod.getPower().getSelectFrom()+" "+mod.getChoice());
		}
		
		// If power has levels, add it as user selected power, so it can be increased later
		if (mod.getPower().hasLevels()) {
			model.addAdeptPower(toAdd);
		} else {
			model.addAutoAdeptPower(toAdd);
		}
		logger.debug("  auto-add power "+toAdd);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process AttributeModifications
			for (Modification tmp : previous) {
				if (tmp instanceof AdeptPowerModification) {
					mergePowerModification(model, (AdeptPowerModification) tmp);
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

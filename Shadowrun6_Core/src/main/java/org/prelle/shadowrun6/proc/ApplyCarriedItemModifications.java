/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.modifications.CarriedItemModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplyCarriedItemModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6");
	
	//-------------------------------------------------------------------
	public ApplyCarriedItemModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process CarriedItemModification
			for (Modification tmp : previous) {
				if (tmp instanceof CarriedItemModification) {
					logger.debug("Apply "+tmp);
					CarriedItemModification mod = (CarriedItemModification)tmp;
					if (mod.isRemove()) {
						for (CarriedItem item : model.getItems(true)) {
							if (item.getItem()==mod.getItem()) {
								logger.info("  Remove "+item+"   (because of "+tmp.getSource()+")");
								model.removeItem(item);
								model.removeAutoItem(item);
							}
						}
					} else {
						// Add
						logger.info("  Add "+mod.getItem()+"   (because of "+tmp.getSource().getClass().getSimpleName()+":"+tmp.getSource()+")");
						CarriedItem toAdd = new CarriedItem(mod.getItem(), mod.getRating());
						toAdd.setCreatedByModification(true);
						
						if (tmp.getSource() instanceof CarriedItem) {
							// Get UUIDs from item that was the reason for this creation
							CarriedItem sourceOfCreation = (CarriedItem) tmp.getSource();
							if (sourceOfCreation!=null) {
								if (!sourceOfCreation.getGeneratedUUIDs().isEmpty()) {
									// Walk through all stored generated UUIDs until you find the matching one
									boolean notFound = true;
									for (String keyVal : sourceOfCreation.getGeneratedUUIDs()) {
										String[] idAndUUID = keyVal.split("\\|");
										if (idAndUUID[0].equals(mod.getItem().getId())) {
											toAdd.setUniqueId(UUID.fromString(idAndUUID[1]));
											notFound=false;
											logger.debug("Remembered UUID for "+toAdd);
											break;
										}
									}
									if (notFound) {
										logger.warn("Did not find previously generated UUID for item "+mod.getItem().getId());
									}
								} else {
									// Not UUIDs yet - add them
									sourceOfCreation.addGeneratedUUIDs(mod.getItem().getId()+"|"+toAdd.getUniqueId());
									logger.warn("Fix missing generatedUUID with "+toAdd.getUniqueId()+" - if you had items embedded in your "+mod.getItem().getName()+", they are lost");
								}
							}
						}
						
						model.addAutoItem(toAdd);
						
						/* Add eventually existing accessories */
//						for (CarriedItem accessory : model.getAutoItemAccessoriesFor(toAdd.getUniqueId())) {
//							logger.debug("Should add accessory "+accessory+" to auto-item "+toAdd.getName());
//							toAdd.addAccessory(accessory);
//						}
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

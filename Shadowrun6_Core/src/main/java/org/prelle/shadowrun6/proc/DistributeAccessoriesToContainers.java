/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.items.CarriedItem;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * Walk through all used added items and copy those that are embedded anywhere
 * into their respective container items.
 * 
 * @author prelle
 *
 */
public class DistributeAccessoriesToContainers implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc.items");
	
	//-------------------------------------------------------------------
	public DistributeAccessoriesToContainers() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			for (CarriedItem ref :model.getItemsAddedByUser()) {
				if (ref.getEmbeddedIn()!=null) {
					CarriedItem container = model.getItem(ref.getEmbeddedIn());
					if (container!=null) {
						logger.debug("Distribute "+ref.getName()+" to "+container.getName());
						if (container.getEmbeddedItem(ref.getUniqueId())==null) {
							container.addAccessory(ref);
						} else {
							// Already embedded
						}
					} else {
						// Did not find container to embed to
						logger.warn("Cannot find container "+ref.getEmbeddedIn()+" in character");
					}
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

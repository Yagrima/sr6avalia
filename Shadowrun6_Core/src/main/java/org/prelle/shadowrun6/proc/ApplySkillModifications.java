/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ApplySkillModifications implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	public ApplySkillModifications() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process AttributeModifications
			for (Modification tmp : previous) {
				if (tmp instanceof SkillModification) {
					SkillModification mod = (SkillModification)tmp;
					Skill skill = mod.getSkill();
					if (skill==null) {
						logger.error("SkillModification without a skill from "+mod.getSource()+" = "+tmp);
						System.err.println("SkillModification without a skill from "+mod.getSource()+" = "+tmp);
						continue;
					}
					SkillValue sVal = model.getSkillValue( skill );
					if (sVal!=null) {
						logger.info("Apply "+tmp+" to skill "+sVal);
						sVal.addModification(tmp);
					} else if (skill.getType()==SkillType.KNOWLEDGE || skill.getType()==SkillType.LANGUAGE) {
						sVal = new SkillValue(skill, mod.getValue());
						sVal.setName(mod.getName());
						logger.info("Add automatic skill: "+sVal);
						model.addAutoSkill(sVal);
					} else {
						sVal = new SkillValue(skill, 0);
						sVal.addModification(tmp);
						logger.info("Add automatic skill "+sVal+" from "+tmp.getSource()+" with value "+mod);
						System.err.println("TODO Add automatic skill "+sVal+" from "+tmp.getSource()+" with value "+mod);
						model.addSkill(sVal);
					}
				} else
					unprocessed.add(tmp);
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.modifications.QualityModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class AddSystemSelectedQualities implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6");
	
	//-------------------------------------------------------------------
	public AddSystemSelectedQualities() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process QualityModifications
			model.clearRacialQualities();
			for (Modification tmp : previous) {
				logger.info("Remains: "+tmp);
				if (tmp instanceof QualityModification) {
					Quality data = ((QualityModification)tmp).getModifiedItem();
					QualityValue ref = new QualityValue(data, 1);
					if (data.needsChoice()) {
						logger.warn("   TODO: implement choices");
					}
					logger.info("Add quality "+data.getId()+" from "+tmp.getSource());
					model.addRacialQuality(ref);
				} else
					unprocessed.add(tmp);
			}
			logger.fatal("STOP here");
			System.exit(1);
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MentorSpirit;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.QualityValue.MentorSpiritMods;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.modifications.AllowModification;
import org.prelle.shadowrun6.modifications.ForbidModification;
import org.prelle.shadowrun6.modifications.ModificationChoice;
import org.prelle.shadowrun6.modifications.QualityModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromQualities implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	public GetModificationsFromQualities() {
	}

	//-------------------------------------------------------------------
	private QualityValue processSingle(ShadowrunCharacter model, QualityModification mod) {
		Quality data = mod.getModifiedItem();
		QualityValue ref = new QualityValue(data, 1);
		if (mod.getValue()>1)
			ref.setPoints(mod.getValue());
		if (data.needsChoice()) {
			logger.warn("   TODO: implement choices");
			switch (data.getSelect()) {
			case NAME:
				ref.setDescription(mod.getChoice());
				break;
			}
		}
		logger.info("Add automatic quality '"+data.getId()+"' "+((data.getMax()>1)?ref.getPoints():"")+" from "+mod.getSource());
		ref.recalculateModifications();
		model.addRacialQuality(ref);
		return ref;
	}

	//-------------------------------------------------------------------
	private List<Modification> processSingeWithoutChoice(QualityValue ref) {
		ref.recalculateModifications();
		if (ref.getModifications()!=null && !ref.getModifications().isEmpty()) {
			logger.debug(" - "+ref.getModifyable().getId()+" has modifications: "+ref.getModifications());
			for (Modification mod : ref.getModifications()) {
				mod.setSource(ref.getModifyable());
				if (ref.getChoice()!=null) {
					switch (ref.getModifyable().getSelect()) {
					case SKILL:
						if (mod instanceof AllowModification)
							((AllowModification)mod).setSkill((Skill) ref.getChoice());
						else if (mod instanceof ForbidModification)
							((ForbidModification)mod).setSkill((Skill) ref.getChoice());
						break;
					}
					logger.debug("added "+mod);
				}
			}
//			logger.debug(" - add modifications: "+ref.getModifications());
			return ref.getModifications();
		}
		return new ArrayList<>();
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>();

		logger.trace("START: process");
		try {
			// Process QualityModifications
			for (Modification tmp : previous) {
				if (tmp instanceof QualityModification) {
					processSingle(model, (QualityModification) tmp);
				} else
					unprocessed.add(tmp);
			}
			
			
			// Apply modifications by qualities
			logger.debug("2. Apply modifications from qualities");
			for (QualityValue ref : model.getQualities()) {
				logger.info("QUAL "+ref);
				unprocessed.addAll(processSingeWithoutChoice(ref));

				/*
				 * If the resolved choice carries modifications itself, use them too
				 */
				if (ref.getChoice()!=null) {
					switch (ref.getModifyable().getSelect()) {
					case MENTOR_SPIRIT:
						List<Modification> toClone = new ArrayList<Modification>( ((MentorSpirit)ref.getChoice()).getModifications() );
						logger.debug("----"+model.getMagicOrResonanceType().getId());
						logger.debug("----Common mentor spirit modifications: "+toClone);
						if (model.getMagicOrResonanceType().getId().equals("adept")) {
							logger.debug("Add for adept: "+((MentorSpirit)ref.getChoice()).getAdeptModifications());
							toClone.addAll(((MentorSpirit)ref.getChoice()).getAdeptModifications());
						} else if (model.getMagicOrResonanceType().getId().equals("magician") || model.getMagicOrResonanceType().getId().equals("aspectedmagician")) {
							logger.info("Add for magician: "+((MentorSpirit)ref.getChoice()).getMagicianModifications());
							toClone.addAll(((MentorSpirit)ref.getChoice()).getMagicianModifications());
						} else if (model.getMagicOrResonanceType().getId().equals("mysticadept")) {
							QualityValue.MentorSpiritMods useAs = ref.getMysticAdeptUses();
							if (useAs==null) {
								logger.warn("decide where to use modifications for adepts or magicians");								
							} else if (useAs==MentorSpiritMods.MAGICIAN) {
								logger.info("Add for magician: "+((MentorSpirit)ref.getChoice()).getMagicianModifications());
								toClone.addAll(((MentorSpirit)ref.getChoice()).getMagicianModifications());								
							} else if (useAs==MentorSpiritMods.ADEPT) {
								logger.debug("Add for adept: "+((MentorSpirit)ref.getChoice()).getAdeptModifications());
								toClone.addAll(((MentorSpirit)ref.getChoice()).getAdeptModifications());								
							}
						}						
						
						for (Modification mod : toClone) {
							Modification realMod;
							if (mod instanceof ModificationChoice) {
								ModificationChoice choice = (ModificationChoice)mod;
								if (model.getDecision(choice)!=null) {
									realMod = model.getDecision(choice);
									logger.info("Character has a decision for "+realMod+" from "+choice);
								} else {
									logger.warn("Still to decide: "+choice);
									realMod = mod;
								}
								
							} else {
								realMod = ShadowrunTools.instantiateModification(mod, ref.getChoice(), 1);
								realMod.setSource(ref.getChoice());
							}
							// If modification itself is a Quality, apply it
							if (realMod instanceof QualityModification) {
								logger.debug(realMod+" is a quality - recurse");
									QualityValue added = processSingle(model, (QualityModification) realMod);
								List<Modification> apply = processSingeWithoutChoice(added);
								unprocessed.addAll(apply);
							} else {							
								unprocessed.add(realMod);
							}
						}

						break;
					case SKILL:
						// Already done by PrioritySkillController
//						logger.error("Not implemented: get modifications from choice type SKILL  = quality was "+ref);
						break;
					case SPIRIT:
						// Nothing to do here
						break;
//					case ATTRIBUTE:
//						// Already done by attribute modifications - no need for more processing
//						break;
					case NAME:
					case ELEMENTAL:
					case ATTRIBUTE:
						break;
					default:
						logger.error("Not implemented: get modifications from "+ref.getChoice().getClass()+" "+ref.getModifyable().getSelect());
						System.err.println("GetModificationsFromQualities: Not implemented: get modifications from "+ref.getChoice().getClass()+" "+ref.getModifyable().getSelect());
					}
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
		return unprocessed;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.modifications.AdeptPowerModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class GetModificationsFromFoci implements CharacterProcessor {
	
	protected static final Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	public GetModificationsFromFoci() {
	}
	
//	//-------------------------------------------------------------------
//	private Object getChoiceFor(FocusValue ref) {
//		Focus focus = ref.getModifyable();
//		if (ref.getChoice()!=null)
//			return ref.getChoice();
//		switch (focus.getChoice()) {
//		case WEAPON:
//			ItemTemplate weapon = (ItemTemplate)ShadowrunTools.resolveChoiceType(focus.getChoice(), ref.getChoiceReference());
//			ref.setChoice(weapon);
//			break;
//		
//		default:
//			logger.error("Not implemented for "+focus.getChoice());
//		}
//		return ref.getChoice();
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.proc.CharacterProcessor#process(org.prelle.shadowrun5.ShadowrunCharacter, java.util.List)
	 */
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {
		List<Modification> unprocessed = new ArrayList<>(previous);

		logger.trace("START: process");
		try {
			// Apply modifications by qualities
//			logger.debug("2. Apply modifications from qualities");
			for (FocusValue ref :model.getFoci()) {
				logger.debug("FOCUS "+ref+" / "+ref.getModifications());
				Focus focus = ref.getModifyable();
				logger.debug("  direct modifications: "+focus.getModifications());
				if (focus.needsChoice()) {
					Object choice = ShadowrunTools.resolveChoiceType(focus.getChoice(), ref.getChoiceReference());
					if (choice==null) {
						logger.warn("No choice for "+focus);
					} else {
						ref.setChoice(choice);
						switch (focus.getChoice()) {
						case MELEE_WEAPON:
							CarriedItem item = new CarriedItem((ItemTemplate) choice);
							item.setUsedFocus(ref);
							model.addAutoItem(item);
							item.setName(item.getName()+" ("+focus.getName()+")");
							logger.info("Added auto-gear: "+item.getName());
							break;
						case SPELL_CATEGORY:
							focus.getModifications().forEach(mod -> {
								Modification realMod = ShadowrunTools.instantiateModification(mod, choice, ref.getLevel());
								realMod.setSource(ref);
								logger.info("Add "+realMod+" from "+ref);
								unprocessed.add(realMod);
							});
							break;
						case ADEPT_POWER:
							focus.getModifications().forEach(mod -> {
								Modification realMod = null;
								if (mod instanceof AdeptPowerModification) {
									AdeptPower power = (AdeptPower)ref.getChoice();
									realMod = new AdeptPowerModification(power,0);
									if (power.needsChoice())
										((AdeptPowerModification)realMod).setChoice(ref.getChoicesChoiceReference());
									if (power.hasLevels())
										((AdeptPowerModification)realMod).setValue( ref.getLevel() );
								} else { 
									realMod = ShadowrunTools.instantiateModification(mod, choice, ref.getLevel());
								}
								realMod.setSource(ref);
								logger.info("Add "+realMod+" from "+ref);
								unprocessed.add(realMod);
							});
							break;
						case SPIRIT:	
							unprocessed.addAll(focus.getModifications());
							break;
						default:
							logger.warn("\n\nTODO: Implement effects of focus type: "+focus.getId()+" and choice "+focus.getChoice()+"\n");
//							System.exit(1);
							unprocessed.addAll(focus.getModifications());
						}
					}
				} else {
					// No choices necessary
					for (Modification mod : focus.getModifications()) {
						Modification realMod = ShadowrunTools.instantiateModification(mod, null, 1);
						realMod.setSource(ref);
						logger.info("Add "+realMod+" from "+ref);
						unprocessed.add(realMod);
					}
				}
			}
		} finally {
			logger.trace("STOP : process() ends with "+unprocessed.size()+" modifications still to process");
		}
//		logger.fatal("STOP HERE: "+unprocessed);
//		System.exit(1);
		return unprocessed;
	}

}

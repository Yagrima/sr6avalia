/**
 * 
 */
package org.prelle.shadowrun6.proc;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.modifications.AttributeModification;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class CalculateEssence implements CharacterProcessor {

	private final static Logger logger = LogManager.getLogger("shadowrun6.proc");
	
	//-------------------------------------------------------------------
	@Override
	public List<Modification> process(ShadowrunCharacter model, List<Modification> previous) {

		logger.trace("START: process");
		try {

			float sum = 0.0f;
			for (CarriedItem item : model.getItems(false)) {
				if (Arrays.asList(ItemType.bodytechTypes()).contains(item.getUsedAsType())) {
					float essence = ShadowrunTools.getItemAttribute(model, item, ItemAttribute.ESSENCECOST).getModifiedValue()/1000.0f;
//					float essence = (Float)item.getAsObject(ItemAttribute.ESSENCECOST);
					logger.info("* "+item.getName()+" = "+essence);
					sum += essence;
				}
			}
			sum += (float)(model.getEssenceHole())/1000.0f;

			float normalLow = (6000 - (int)(sum*1000)) / 1000.0f;
			if (model.getEssence()==0 || normalLow<model.getEssence()) {
				logger.info("Unused essence decreased to "+normalLow);
				model.setEssence(normalLow);
			}
			logger.info("sum="+sum+"  normalLow="+normalLow+"  unused="+model.getEssence());

			float min = Math.min(model.getEssence(), 6.0f-sum);
			if (min!=model.getEssence()) {
				logger.warn("Fix essence to "+min);
				model.setEssence(min);
			}
			int magicMalus = 6 - (int)model.getEssence();
			logger.info("Magic malus is "+magicMalus);
			model.getAttribute(Attribute.MAGIC).addModification(new AttributeModification(Attribute.MAGIC, -magicMalus, Attribute.ESSENCE));
			model.getAttribute(Attribute.RESONANCE).addModification(new AttributeModification(Attribute.RESONANCE, -magicMalus, Attribute.ESSENCE));
			
		} finally {
			logger.trace("STOP : process() ends with "+previous.size()+" modifications still to process");
		}
		return previous;
	}

}

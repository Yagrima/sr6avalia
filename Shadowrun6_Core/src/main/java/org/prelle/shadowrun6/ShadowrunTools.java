/**
 *
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MetamagicOrEcho.Type;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.actions.ShadowrunAction.Category;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.Damage;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemAttributeNumericalValue;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.LivingPersona;
import org.prelle.shadowrun6.items.VehicleData.VehicleType;
import org.prelle.shadowrun6.modifications.AccessoryModification;
import org.prelle.shadowrun6.modifications.AddNuyenModification;
import org.prelle.shadowrun6.modifications.AdeptPowerModification;
import org.prelle.shadowrun6.modifications.AllowModification;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.CarriedItemModification;
import org.prelle.shadowrun6.modifications.ComplexFormModification;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.ForbidModification;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;
import org.prelle.shadowrun6.modifications.ItemHookModification;
import org.prelle.shadowrun6.modifications.LifestyleCostModification;
import org.prelle.shadowrun6.modifications.MetamagicOrEchoModification;
import org.prelle.shadowrun6.modifications.ModificationBase;
import org.prelle.shadowrun6.modifications.ModificationChoice;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.QualityModification;
import org.prelle.shadowrun6.modifications.RelevanceModification;
import org.prelle.shadowrun6.modifications.RitualModification;
import org.prelle.shadowrun6.modifications.SINModification;
import org.prelle.shadowrun6.modifications.SkillModification;
import org.prelle.shadowrun6.modifications.SkillSpecializationModification;
import org.prelle.shadowrun6.modifications.SpecialRuleModification;
import org.prelle.shadowrun6.modifications.SpellModification;
import org.prelle.shadowrun6.proc.ApplyAdeptPowerModifications;
import org.prelle.shadowrun6.proc.ApplyAttributeModifications;
import org.prelle.shadowrun6.proc.ApplyCarriedItemModifications;
import org.prelle.shadowrun6.proc.ApplyMemorizedUUIDModifications;
import org.prelle.shadowrun6.proc.ApplyRelevanceAndEdgeMods;
import org.prelle.shadowrun6.proc.ApplySkillModifications;
import org.prelle.shadowrun6.proc.CalculateDerivedAttributes;
import org.prelle.shadowrun6.proc.CalculateEssence;
import org.prelle.shadowrun6.proc.CalculatePersona;
import org.prelle.shadowrun6.proc.CharacterProcessor;
import org.prelle.shadowrun6.proc.DistributeAccessoriesToContainers;
import org.prelle.shadowrun6.proc.FixDeprecatedRecursiveAccessories;
import org.prelle.shadowrun6.proc.GetModificationsFromEquipment;
import org.prelle.shadowrun6.proc.GetModificationsFromFoci;
import org.prelle.shadowrun6.proc.GetModificationsFromMagicOrResonance;
import org.prelle.shadowrun6.proc.GetModificationsFromMetaType;
import org.prelle.shadowrun6.proc.GetModificationsFromMetamagicOrEchoes;
import org.prelle.shadowrun6.proc.GetModificationsFromPowers;
import org.prelle.shadowrun6.proc.GetModificationsFromQualities;
import org.prelle.shadowrun6.proc.RecalculateEquipment;
import org.prelle.shadowrun6.proc.ResetModifications;
import org.prelle.shadowrun6.proc.ResolveChoicesInReferences;
import org.prelle.shadowrun6.requirements.AnyRequirement;
import org.prelle.shadowrun6.requirements.AttributeRequirement;
import org.prelle.shadowrun6.requirements.ItemHookRequirement;
import org.prelle.shadowrun6.requirements.ItemRequirement;
import org.prelle.shadowrun6.requirements.ItemSubTypeRequirement;
import org.prelle.shadowrun6.requirements.ItemTypeRequirement;
import org.prelle.shadowrun6.requirements.MagicOrResonanceRequirement;
import org.prelle.shadowrun6.requirements.MetatypeRequirement;
import org.prelle.shadowrun6.requirements.Requirement;
import org.prelle.shadowrun6.requirements.SkillRequirement;

import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.Datable;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductService;
import de.rpgframework.products.ProductServiceLoader;

/**
 * @author prelle
 *
 */
public class ShadowrunTools {

	private final static ResourceBundle CORE = ShadowrunCore.getI18nResources();

	private static Logger logger = LogManager.getLogger("shadowrun6");
	
	public static class PoolCalculation {
		public int value;
		public String source;
		public boolean hitAugmentLimit;
		public PoolCalculation(int val, String src) {
			this.value = val;
			this.source= src;
		}
		public String toString() { return value+":"+source+(hitAugmentLimit?"*":""); }
	}

	public final static List<CharacterProcessor> RECALCULATE_STEPS = Arrays.asList(new CharacterProcessor[]{
		new ResetModifications(),
		new ResolveChoicesInReferences(),
		new GetModificationsFromMetaType(),
		new GetModificationsFromMagicOrResonance(),
		new GetModificationsFromQualities(),
		new ApplyAdeptPowerModifications(),
		new GetModificationsFromPowers(),
		new RecalculateEquipment(),
		new FixDeprecatedRecursiveAccessories(),
		new DistributeAccessoriesToContainers(),
		new GetModificationsFromEquipment(),
		new GetModificationsFromMetamagicOrEchoes(),
		new GetModificationsFromFoci(),
		new ApplyAdeptPowerModifications(),
		new ApplyCarriedItemModifications(),
		new ApplyAttributeModifications(),
		new ApplySkillModifications(),
		new ApplyMemorizedUUIDModifications(),
//		new ApplySINModifications(),
		new ApplyRelevanceAndEdgeMods(),
		new CalculateDerivedAttributes(),
		new CalculateEssence(),
		new CalculatePersona(),
	});
	
	//--------------------------------------------------------------------
	public static void calculateEssenceCost(ShadowrunCharacter model) {
		logger.trace("calculateEssenceCost()");
		float sum = 0.0f;
		for (CarriedItem item : model.getItems(false)) {
			if (Arrays.asList(ItemType.bodytechTypes()).contains(item.getUsedAsType())) {
				float essence = getItemAttribute(model, item, ItemAttribute.ESSENCECOST).getModifiedValue()/1000.0f;
//				float essence = (Float)item.getAsObject(ItemAttribute.ESSENCECOST);
				logger.debug("* "+item.getName()+" = "+essence);
				sum += essence;
			}
		}

		float normalLow = 6.0f - sum;
		if (model.getEssence()==0 || normalLow<model.getEssence()) {
			logger.info("Unused essence decreased to "+normalLow);
			model.setEssence(normalLow);
		}
		logger.trace("sum="+sum+"  normalLow="+normalLow+"  unused="+model.getEssence());

		float min = Math.min(model.getEssence(), 6.0f-sum);
		if (min!=model.getEssence()) {
			logger.warn("Fix essence to "+min);
			model.setEssence(min);
		}
	}

//	//-------------------------------------------------------------------
//	private static void applyStandardItemMdodifications(ShadowrunCharacter model, CarriedItem ref) {
//		logger.info("  apply modifications from item templates (add slots, accessories ...) for "+ref);
//		ref.updateModifications();
//
//		ItemTemplate item = ref.getItem();
//
//		for (Modification mod : item.getModifications()) {
//			if (mod instanceof ItemHookModification) {
//				ItemHookModification aMod = (ItemHookModification)mod;
//				if (ref.getSlot(aMod.getHook())!=null)
//					continue;
//				AvailableSlot hook = (aMod.getCapacity()>1)?(new AvailableSlot(aMod.getHook(), aMod.getCapacity())):(new AvailableSlot(aMod.getHook()));
//				ref.addSlot(hook);
//				logger.debug("    add slot "+aMod.getHook()+" to "+ref);
//			} else if (mod instanceof AccessoryModification) {
//				AccessoryModification aMod = (AccessoryModification)mod;
//				AvailableSlot hook = ref.getSlot(aMod.getHook());
//				if (hook==null) {
//					hook = new AvailableSlot(aMod.getHook());
//					ref.addSlot(hook);
//				}
//				logger.warn("TODO: instantiate accessory in load "+aMod.getItem());
////				CarriedItem accessory = instantiate(aMod.getItem());
////				accessory.setPrice(0);
////				hook.addEmbeddedItem(accessory);
//			} else if (mod instanceof AttributeModification) {
//				mod.setSource(ref);
////				AttributeModification aMod = (AttributeModification)mod;
////				model.getAttribute(aMod.getAttribute()).addModification(aMod);
//			} else {
//				logger.warn("TODO: "+mod);
//			}
//		}
//	}

	//-------------------------------------------------------------------
	public static void applyModification(ShadowrunCharacter model, Modification mod) {
		if (mod.getSource()==null)
			throw new NullPointerException("No source in modification "+mod);

		if (mod instanceof AttributeModification)  {
			AttributeModification newMod = (AttributeModification) mod;
			model.getAttribute(newMod.getAttribute()).addModification(newMod);
		} else if (mod instanceof LifestyleCostModification) {
			// Ignore
		} else if (mod instanceof QualityModification) {
			QualityModification qMod = (QualityModification)mod;
			QualityValue toApply = new QualityValue(qMod.getModifiedItem(), qMod.getValue());
			model.addRacialQuality(toApply);
		} else if (mod instanceof CarriedItemModification) {
			CarriedItemModification itemMod = (CarriedItemModification)mod;
			CarriedItem item = (itemMod.getItem().hasRating())?(new CarriedItem(itemMod.getItem(), itemMod.getRating())):(new CarriedItem(itemMod.getItem()));
			item.setCreatedByModification(true);
			model.addItem(item);
		} else if (mod instanceof SkillModification) {
			SkillModification sMod = (SkillModification)mod;
			SkillValue sVal = model.getSkillValue(sMod.getSkill());
			if (sVal!=null)
				sVal.addModification(mod);
			else
				logger.warn("Have modification "+sMod+" from "+sMod.getSource()+" , but character does not have that skill");
		} else {
			logger.info("Don't know how to apply "+mod.getClass());
			System.exit(0);
		}

	}

	//-------------------------------------------------------------------
	public static Object resolveChoiceType(ChoiceType type, String reference) {
		if (reference==null)
			return null;
		switch (type) {
		case ADEPT_POWER:
			return ShadowrunCore.getAdeptPower(reference);
		case AMMUNITION_TYPE:
			return ShadowrunCore.getAmmoType(reference);
		case ATTRIBUTE:
			return Attribute.valueOf(reference);
		case ELEMENTAL:
			return ElementType.valueOf(reference);
		case PHYSICAL_ATTRIBUTE:
			for (Attribute attr : Attribute.physicalValues()) {
				if (attr.name().equals(reference))
					return attr;
			}
			throw new IllegalArgumentException("Not a physical attribute: "+reference);
		case SKILL:
		case COMBAT_SKILL:
		case NONCOMBAT_SKILL:
		case PHYSICAL_SKILL:
			return ShadowrunCore.getSkill(reference);
		case NAME:
			return reference;
		case MATRIX_ACTION:
			return ShadowrunCore.getAction(reference);
		case MATRIX_ATTRIBUTE:
			for (Attribute attr : Attribute.matrixValues()) {
				if (attr.name().equals(reference))
					return attr;
			}
			throw new IllegalArgumentException("Not a matrix attribute: "+reference);
		case MENTOR_SPIRIT:
			return ShadowrunCore.getMentorSpirit(reference);
		case PROGRAM:
			return ShadowrunCore.getItem(reference);
		case SPELL_CATEGORY:
			return Spell.Category.valueOf(reference);
		case SPIRIT:
			return ShadowrunCore.getSpirit(reference);
		case MELEE_WEAPON:
		case WEAPON:
			return ShadowrunCore.getItem(reference);
		case SENSE:
			return Sense.valueOf(reference);
		default:
			throw new IllegalArgumentException("ChoiceType "+type+" not supported yet ("+reference+")");
		}

	}

	//-------------------------------------------------------------------
	public static String getChoiceReference(ChoiceType selectFrom, Object choice) {
		logger.debug("getChoiceReference "+selectFrom);
		
		switch (selectFrom) {
		case ADEPT_POWER:
			return ((AdeptPower)choice).getId();
		case ATTRIBUTE:
		case MATRIX_ATTRIBUTE:
		case PHYSICAL_ATTRIBUTE:
			return ((Attribute)choice).name();
		case SKILL:
		case COMBAT_SKILL:
		case NONCOMBAT_SKILL:
		case PHYSICAL_SKILL:
			return ((Skill)choice).getId();
		case ELEMENTAL:
			return ((ElementType)choice).name();
		case MATRIX_ACTION:
			return ((ShadowrunAction)choice).getId();
		case MENTOR_SPIRIT:
			return ((MentorSpirit)choice).getId();
		case NAME:
			return (String)choice;
		case SPELL_CATEGORY:
			return ((Spell.Category)choice).name();
		case SPIRIT:
			return ((Spirit)choice).getId();
		case SPRITE:
			return ((Sprite)choice).getId();
		case SENSE:
			return ((Sense)choice).name();
		case WEAPON:
		case MELEE_WEAPON:
			ItemTemplate weapon = (ItemTemplate)choice;
			if (!weapon.isType(ItemType.WEAPON)) {
				logger.error("Choice is not a weapon: "+choice);
				return null;
			}
			return weapon.getId();
		default:
			logger.error("Choosing "+selectFrom+" not implemented");
		}
		return null;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Object> getChoices(ChoiceType selectFrom) {
		logger.debug("getChoices "+selectFrom);
		
		switch (selectFrom) {
		case ADEPT_POWER:
			return new ArrayList(ShadowrunCore.getAdeptPowers());
		case ATTRIBUTE:
			return Arrays.asList(Attribute.primaryValues());
		case MATRIX_ATTRIBUTE:
			return Arrays.asList(ItemAttribute.DATA_PROCESSING, ItemAttribute.FIREWALL, ItemAttribute.SLEAZE, ItemAttribute.ATTACK);
		case PHYSICAL_ATTRIBUTE:
			return Arrays.asList(Attribute.physicalValues());
		case SKILL:
			return new ArrayList(ShadowrunCore.getSkills());
		case COMBAT_SKILL:
			return new ArrayList(ShadowrunCore.getSkills().stream().filter(skill -> skill.getType()==SkillType.COMBAT).collect(Collectors.toList()));
		case NONCOMBAT_SKILL:
			return new ArrayList(ShadowrunCore.getSkills().stream().filter(skill -> skill.getType()!=SkillType.COMBAT).collect(Collectors.toList()));
		case PHYSICAL_SKILL:
			return new ArrayList(ShadowrunCore.getSkills().stream().filter(skill -> skill.getAttribute1().isPhysical()).collect(Collectors.toList()));
		case ELEMENTAL:
			return Arrays.asList(ElementType.values());
		case MATRIX_ACTION:
			return new ArrayList(ShadowrunCore.getActions(Category.MATRIX));
		case MENTOR_SPIRIT:
			return new ArrayList(ShadowrunCore.getMentorSpirits());
		case NAME:
			return new ArrayList<>();
		case SPELL_CATEGORY:
			return Arrays.asList(Spell.Category.values());
		case SPIRIT:
			return new ArrayList(ShadowrunCore.getSpirits());
		case SPRITE:
			return new ArrayList(ShadowrunCore.getSprites());
		case SENSE:
			return Arrays.asList(Sense.values());
		default:
			logger.error("Choosing "+selectFrom+" not implemented");
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<SkillValue> getAllSkillValues(ShadowrunCharacter model, SkillType... types) {
		List<SkillType> filter = Arrays.asList(types);
		if (filter.isEmpty())
			filter = Arrays.asList(SkillType.regularValues());

		List<SkillValue> ret = new ArrayList<>();
		for (Skill skill : ShadowrunCore.getSkills()) {
			if (!filter.contains(skill.getType()))
				continue;
			switch (skill.getType()) {
			case COMBAT:
			case MAGIC:
			case PHYSICAL:
			case RESONANCE:
			case SOCIAL:
			case TECHNICAL:
			case VEHICLE:
				SkillValue val = model.getSkillValue(skill);
				if (val==null) {
					if (skill.isUseUntrained()) {
						val = new SkillValue(skill, 0);
					} else
						val = new SkillValue(skill, -1);
				}
				ret.add(val);
				break;
			case LANGUAGE:
			case KNOWLEDGE:
				break;
			case ACTION:
			case NOT_SET:
				break;
			}
		}

		for (SkillValue val : model.getSkillValues()) {
			SkillType tmpType = val.getModifyable().getType();
			if (!filter.contains(tmpType))
				continue;
			if (Arrays.asList(SkillType.individualValues()).contains(tmpType)) {
				ret.add(val);
			}
		}

		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Convert modifications to those that can be applied to a character.
	 */
	public static Modification instantiateModification(Modification mod, Object choice, int multiplier) {
		if (mod instanceof AttributeModification)  {
			AttributeModification newMod = (AttributeModification)  ((AttributeModification)mod).clone();
			if (newMod.getAttribute()==null) {
				if (choice==null)
					throw new NullPointerException("Choice must be an Attribute");
				if (choice instanceof Attribute)
					newMod.setAttribute((Attribute)choice);
				else
					throw new IllegalArgumentException();
			}
			if (multiplier>1)
				newMod.setValue(newMod.getValue()*multiplier);
			return newMod;
		} else if (mod instanceof SkillModification)  {
			SkillModification newMod = (SkillModification)  ((SkillModification)mod).clone();
			if (choice!=null && choice instanceof Skill)
				newMod.setSkill((Skill) choice);
			if (choice!=null && choice instanceof String)
				newMod.setName((String)choice);
			if (multiplier>1)
				newMod.setValue(newMod.getValue()*multiplier);
			return newMod;
		} else if (mod instanceof LifestyleCostModification)  {
			LifestyleCostModification newMod = (LifestyleCostModification)  ((LifestyleCostModification)mod).clone();
			return newMod;
		} else if (mod instanceof SINModification)  {
			SINModification newMod = (SINModification)  ((SINModification)mod).clone();
			return newMod;
		} else if (mod instanceof QualityModification)  {
			QualityModification newMod = (QualityModification)  ((QualityModification)mod).clone();
			return newMod;
		} else if (mod instanceof CarriedItemModification)  {
			CarriedItemModification newMod = (CarriedItemModification)  ((CarriedItemModification)mod).clone();
			if (multiplier>1)
				newMod.setRating(multiplier);
			return newMod;
		} else if (mod instanceof AllowModification)  {
			AllowModification newMod = (AllowModification)  ((AllowModification)mod).clone();
			return newMod;
		} else if (mod instanceof EdgeModification)  {
			return  ((EdgeModification)mod).clone();
		} else if (mod instanceof ItemAttributeModification)  {
			ItemAttributeModification clone = ((ItemAttributeModification)mod).clone();
			clone.setValue(clone.getValue()*multiplier);
			return clone;
		} else if (mod instanceof ForbidModification)  {
			return  ((ForbidModification)mod).clone();
		} else if (mod instanceof RelevanceModification)  {
			return  ((RelevanceModification)mod).clone();
		} else if (mod instanceof SpecialRuleModification)  {
			SpecialRuleModification newMod =  (SpecialRuleModification) ((SpecialRuleModification)mod).clone();
			if (multiplier>1)
				newMod.setLevel(newMod.getLevel()*multiplier);
			return newMod;
		} else if (mod instanceof AdeptPowerModification)  {
			return  ((AdeptPowerModification)mod).clone();
		} else if (mod instanceof ItemHookModification)  {
			return mod;
		} else if (mod instanceof AccessoryModification)  {
			return mod;
		} else if (mod instanceof ModificationChoice)  {
			ModificationChoice cloned = ((ModificationChoice)mod).clone();
			cloned.clear();
			for (Modification tmp : ((ModificationChoice)mod)) {
				cloned.add(instantiateModification(tmp, choice, multiplier));
			}
			return cloned;
		} else {
			System.err.println("No special handling for "+mod.getClass()+" and choice="+choice+"  and multiplier="+multiplier);
			logger.warn("No special handling for "+mod.getClass()+" and choice="+choice+"  and multiplier="+multiplier);
			throw new IllegalArgumentException("Don't support "+mod.getClass());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Convert modifications to those that can be applied to a character.
	 */
	public static Requirement instantiateRequirement(Requirement mod, Object choice, int multiplier) {
		if (multiplier==0)
			multiplier=1;
		if (mod instanceof AttributeRequirement)  {
			AttributeRequirement newMod = (AttributeRequirement)  ((AttributeRequirement)mod).clone();
			if (newMod.getAttribute()==null) {
				if (choice==null)
					throw new NullPointerException("Choice must be an Attribute");
				if (choice instanceof Attribute)
					newMod.setAttribute((Attribute)choice);
				else
					throw new IllegalArgumentException();
			}
			return newMod;
		} else if (mod instanceof SkillRequirement)  {
			SkillRequirement newMod = (SkillRequirement)  ((SkillRequirement)mod).clone();
			if (choice!=null && choice instanceof Skill)
				newMod.setSkill((Skill) choice);
			return newMod;
		} else if (mod instanceof ItemHookRequirement)  {
			ItemHookRequirement req = (ItemHookRequirement)mod;
			ItemHookRequirement newReq = new ItemHookRequirement(req.getSlot(), req.getCapacity()*multiplier);
			return newReq;
		} else if (mod instanceof AnyRequirement)  {
			AnyRequirement req = (AnyRequirement)mod;
			AnyRequirement newReq = new AnyRequirement();
			newReq.setNegated(req.isNegated());
			for (Requirement tmp2 : req.getOptionList()) {
				Requirement real = instantiateRequirement(tmp2, choice, multiplier);
				real.setNegated(tmp2.isNegated());
			}
			return newReq;
		} else if (mod instanceof ItemSubTypeRequirement)  {
			return mod;
		} else if (mod instanceof ItemTypeRequirement)  {
			return mod;
		} else if (mod instanceof ItemRequirement)  {
			return mod;
		} else {
			logger.warn("No special handling for "+mod.getClass()+" and choice="+choice);
			throw new IllegalArgumentException("Don't support "+mod.getClass());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Determine all modifications that must be applied to a character or
	 * a character leveller
	 */
	public static List<Modification> getCharacterModifications(ShadowrunCharacter data) {
		logger.debug(" START: getCharacterModifications");
		List<Modification> ret = new ArrayList<>();
		/*
		 * Modifications depending on metatype
		 */
		logger.debug("  1. Check metatype");
		for (Modification mod : data.getMetatype().getModifications()) {
			Modification newMod = instantiateModification(mod, null, 1);
			newMod.setSource(data.getMetatype());
			ret.add(newMod);
		}

		/*
		 * Modifications depending on adept powers
		 */
		logger.debug("  2. Check adept powers");
		for (AdeptPowerValue power : data.getAdeptPowers()) {
			Object choice = (power.getModifyable().needsChoice()) ? resolveChoiceType(power.getModifyable().getSelectFrom(), power.getChoiceReference()): null;
			for (Modification mod : power.getModifyable().getModifications()) {
				Modification newMod = instantiateModification(mod, choice, 1);
				newMod.setSource(power);
				ret.add(newMod);
			}
		}

		/*
		 * Modifications depending on qualities
		 */
		logger.debug("  3. Check qualities");
		for (QualityValue qual : data.getQualities()) {
			logger.info("Check "+qual);
			Object choice = (qual.getModifyable().needsChoice()) ? resolveChoiceType(qual.getModifyable().getSelect(), qual.getChoiceReference()): null;
			qual.setChoice(choice);
			for (Modification mod : resolveModifications(qual.getModifyable(), choice)) {
				Modification newMod = instantiateModification(mod, choice, 1);
				newMod.setSource(qual);
				ret.add(newMod);
			}
		}

		logger.debug(" STOP : getCharacterModifications");
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Convert modifications to those that can be applied to a character.
	 */
	public static List<Modification> resolveModifications(Quality quality, Object choice) {
		List<Modification> ret = new ArrayList<>();
		if (!quality.needsChoice())
			return ret;

		for (Modification mod : quality.getModifications()) {
			try {
				Modification newMod = instantiateModification(mod, choice, 1);
				ret.add(newMod);
			} catch (Exception e) {
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Error in character: "+e.toString());
			}
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * Apply those modifications not saved in the character - e.g. the
	 * resistance boni depending on level
	 */
	public static void recalculateCharacter(ShadowrunCharacter data) {
		logger.info("----------------recalculateCharacter");
//		logger.info("before recalc: \n"+data.dumpAttributes());

		List<Modification> unprocessed = new ArrayList<>();
		for (CharacterProcessor step : RECALCULATE_STEPS) {
			logger.debug("  run "+step.getClass().getSimpleName());
			unprocessed = step.process(data, unprocessed);
			logger.debug("  after "+step.getClass().getSimpleName()+" = "+unprocessed);
		}
		logger.info("  unprocessed = "+unprocessed);
//		logger.info("after recalc: \n"+data.dumpAttributes());
	}

	//--------------------------------------------------------------------
	public static List<PoolCalculation> getAttributePoolCalculation(ShadowrunCharacter model, org.prelle.shadowrun6.Attribute attrib) {
		List<PoolCalculation> ret = new ArrayList<>();
		// Add the unmodified attribute
		AttributeValue aVal = model.getAttribute(attrib);
		if (aVal.getPoints()>0)
			ret.add(new PoolCalculation(aVal.getPoints(), aVal.getAttribute().getName()));
		// Now add modifiers from the attribute
		int augAllowed = 4;
		for (Modification mod : aVal.getModifications()) {
			if (mod instanceof AttributeModification) {
				AttributeModification sMod = (AttributeModification)mod;
				if (!sMod.isConditional() && sMod.getType()!=ModificationValueType.MAX && sMod.getType()!=ModificationValueType.ALTERNATE) {
					int val = Math.min(augAllowed, sMod.getValue());
					if (sMod.getType()==ModificationValueType.NATURAL)
						val = sMod.getValue();
					// Mark modifiers being capped with augmentation limit
					PoolCalculation calc = new PoolCalculation(val, ShadowrunTools.getModificationSourceString(sMod.getSource()));
					// Augmentation limit is only valid if not NATURAL
					if (sMod.getType()!=ModificationValueType.NATURAL)
						calc.hitAugmentLimit = val<sMod.getValue();
					ret.add(calc);
					augAllowed -= val;
				}
			}
		}
		
		return ret;
	}

	//--------------------------------------------------------------------
	public static String getAttributePoolExplanation(ShadowrunCharacter model, org.prelle.shadowrun6.Attribute attrib) {
		return String.join("\n",getAttributePoolCalculation(model, attrib).stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
	}

	//--------------------------------------------------------------------
	/**
	 * @param skill
	 * @param useAttrib  Attribute to use 
	 * @param special IDs of specializations to use (only use highest)
	 * @return
	 */
	public static List<PoolCalculation> getSkillPoolCalculation(ShadowrunCharacter model, Skill skill, org.prelle.shadowrun6.Attribute useAttrib, String...special) {
		List<PoolCalculation> ret = new ArrayList<>();
		
		// Add the unmodified skill
		SkillValue     sVal = model.getSkillValue(skill);
		if (sVal==null) {
			// Skill not present
			if (!skill.isUseUntrained()) {
				Resource.format(CORE, "explain.skill_not_untrained", skill.getName());
				ret.add(new PoolCalculation(0, Resource.format(CORE, "explain.skill_not_untrained", skill.getName())));
				return ret;
			} else {
				ret.add(new PoolCalculation(-1, Resource.format(CORE, "explain.untrained_skill", skill.getName())));
			}
		} else {
			ret.add(new PoolCalculation(sVal.getPoints(), Resource.format(ShadowrunCore.getI18nResources(), "explain.skillpoints", skill.getName())));
			// Now add modifiers from the skill
			int augAllowed = 4;
			for (Modification mod : sVal.getModifications()) {
				if (mod instanceof SkillModification) {
					SkillModification sMod = (SkillModification)mod;
					if (sMod.getSkill()==skill && !sMod.isConditional() && sMod.getModificationType()!=ModificationValueType.MAX) {
						int val = Math.min(augAllowed, sMod.getValue());
						// Mark modifiers being capped with augmentation limit
						PoolCalculation calc = new PoolCalculation(val, ShadowrunTools.getModificationSourceString(sMod.getSource()));
						calc.hitAugmentLimit = val<sMod.getValue();
						ret.add(calc);
						augAllowed -= val;
					}
				}
			}
			
			// Now specializations
			SkillSpecializationValue bestSpec = null;
			for (SkillSpecializationValue spec : sVal.getSkillSpecializations()) {
				// Test if specializ. matches requested specs
				if (!Arrays.asList(special).contains(spec.getSpecial().getId()))
					continue;
				if (bestSpec==null || spec.isExpertise())
					bestSpec = spec;
			}
			if (bestSpec!=null) {
				if (bestSpec.isExpertise()) {
					ret.add(new PoolCalculation(3, Resource.format(CORE, "explain.expertise", bestSpec.getSpecial().getName())));
				} else {
					ret.add(new PoolCalculation(2, Resource.format(CORE, "explain.specialization", bestSpec.getSpecial().getName())));				
				}
			}
		}
		
		// Add the attribute
		ret.addAll(getAttributePoolCalculation(model, useAttrib));
		
		
		return ret;
	}

	//--------------------------------------------------------------------
	public static int getSkillPool(ShadowrunCharacter model, Skill skill, String... special) {
		return (int)getSkillPoolCalculation(model, skill, skill.getAttribute1(), special).stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum();
	}

	//--------------------------------------------------------------------
	public static int getSkillPool(ShadowrunCharacter model, Skill skill, org.prelle.shadowrun6.Attribute useAttrib, String... special) {
		return (int)getSkillPoolCalculation(model, skill, useAttrib, special).stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum();
	}

	//--------------------------------------------------------------------
	public static String getSkillPoolExplanation(ShadowrunCharacter model, Skill skill, String... special) {
		return String.join("\n",getSkillPoolCalculation(model, skill, skill.getAttribute1(), special).stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
	}

	//-------------------------------------------------------------------
	public static List<PoolCalculation> getWeaponPoolCalculation(ShadowrunCharacter model, CarriedItem item) {
		if (item.getItem().getWeaponData()==null) {
			throw new IllegalArgumentException(item.getName()+" is not a weapon but a "+item.getItem().getTypes()+" and of type "+item.getItem().getClass());
		}
		Skill skill = item.getItem().getWeaponData().getSkill();
		SkillValue sVal = model.getSkillValue(skill);
		
		List<PoolCalculation> ret = new ArrayList<>();
		String special = null;
		
		// Find the correct specialization
		if (skill.getId().equals("exotic_weapons")) {
			// Without skill, you cannot use the weapon
			if (sVal==null) {
				ret.add(new PoolCalculation(0, Resource.format(CORE, "explain.missing_exotic_skill", item.getItem().getName())));
				return ret;
			}
			// Find matching specialization
			SkillSpecializationValue spec = null;
			for (SkillSpecializationValue tmp : sVal.getSkillSpecializations()) {
				if (tmp.getSpecial().getExoticItem()==item.getItem()) {
					spec = tmp;
					break;
				}
			}
			// Without specialization, the weapon cannot be used
			if (spec==null) {
				ret.add(new PoolCalculation(0, Resource.format(CORE, "explain.missing_exotic_specialization", item.getItem().getName())));				
				return ret;
			}
			// Specialization found
			special = spec.getSpecial().getId();
		} else {		
			SkillSpecialization required = item.getItem().getWeaponData().getSpecialization();			
			special = required.getId();
		}

		
		ret.addAll( getSkillPoolCalculation(model, skill, skill.getAttribute1(), special) );		
		
		/*
		 * Add eventually existing focus
		 */
		if (item.getUsedFocus()!=null) {
			FocusValue focus = item.getUsedFocus();
			if (focus.getModifyable().getChoice()==ChoiceType.MELEE_WEAPON) {
				ret.add( new PoolCalculation(focus.getLevel(), focus.getName()));
			}
		}
		
		return ret;
	}

	//--------------------------------------------------------------------
	public static int getWeaponPool(ShadowrunCharacter model, CarriedItem item) {
		return (int)getWeaponPoolCalculation(model, item).stream().collect(Collectors.summarizingInt(pc -> pc.value)).getSum();
	}

	//--------------------------------------------------------------------
	public static String getWeaponPoolExplanation(ShadowrunCharacter model, CarriedItem item) {
		return String.join("\n",getWeaponPoolCalculation(model, item).stream().map(pool -> pool.value+" "+pool.source+(pool.hitAugmentLimit?"*":" ") ).collect(Collectors.toList()));
	}

//	//-------------------------------------------------------------------
//	public static int getWeaponPool(ShadowrunCharacter model, CarriedItem item) {
//		if (item.getItem().getWeaponData()==null) {
//			throw new IllegalArgumentException(item.getName()+" is not a weapon but a "+item.getItem().getTypes()+" and of type "+item.getItem().getClass());
//		}
//
//		Skill skill = item.getItem().getWeaponData().getSkill();
//		SkillValue sVal = model.getSkillValue(skill);
//		int pool = model.getSkillPool(skill);
//		System.err.println("getWeaponPool: "+item+"  regular pool = "+pool);
//		
//		/*
//		 * Treat exotic weapons different
//		 */
//		if (skill.getId().equals("exotic_weapons")) {
//			// Without skill, you cannot use the weapon
//			if (sVal==null)
//				return 0;
//			// Find matching specialization
//			SkillSpecializationValue spec = null;
//			for (SkillSpecializationValue tmp : sVal.getSkillSpecializations()) {
//				if (tmp.getSpecial().getExoticItem()==item.getItem()) {
//					spec = tmp;
//					break;
//				}
//			}
//			// Without specialization, the weapon cannot be used
//			if (spec==null)
//				return 0;
//			// Specialization found
//			return pool+2;   // TODO: Clarify if +2 from special. applies here
//		} else {		
//			SkillSpecialization required = item.getItem().getWeaponData().getSpecialization();			
//			if (required!=null) {
//				if (sVal!=null) {
//					SkillSpecializationValue spec = sVal.getSpecialization(required);
//					if (spec!=null) {
//						// Has specialization or expertise
//						if (spec.isExpertise())
//							return pool+3;
//						else
//							return pool+2;
//					}
//				}
//			}
//		}
//		
//		/*
//		 * Add eventually existing focus
//		 */
//		if (item.getUsedFocus()!=null) {
//			FocusValue focus = item.getUsedFocus();
//			if (focus.getModifyable().getChoice()==ChoiceType.MELEE_WEAPON) {
//				pool += focus.getLevel();
//			}
//		}
//		
//		return pool;
//	}
//
//	//-------------------------------------------------------------------
//	public static String getWeaponPoolExplanation(ShadowrunCharacter model, CarriedItem item) {
//		if (item.getItem().getWeaponData()==null) {
//			throw new IllegalArgumentException(item.getName()+" is not a weapon but a "+item.getItem().getTypes()+" and of type "+item.getItem().getClass());
//		}
//
//		List<String> data = new ArrayList<>();
//		Skill skill = item.getItem().getWeaponData().getSkill();
//		data.addAll( model.getSkillPoolExplanation(skill) );
//		
//		// Specialization
//		SkillValue sVal = model.getSkillValue(skill);
//		SkillSpecialization specRequired = item.getItem().getWeaponData().getSpecialization();
//		if (sVal!=null && sVal.hasSpecialization(specRequired)) {
//			SkillSpecializationValue spec = sVal.getSpecialization(specRequired); 
//			if (spec.isExpertise()) {
//				data.add(Resource.format(CORE, "explain.expertise", specRequired.getName()));
//			} else {
//				data.add(Resource.format(CORE, "explain.specialization", specRequired.getName()));				
//			}
//		}
//		
//		/*
//		 * Add eventually existing focus
//		 */
//		if (item.getUsedFocus()!=null) {
//			FocusValue focus = item.getUsedFocus();
//			if (focus.getModifyable().getChoice()==ChoiceType.MELEE_WEAPON) {
//				data.add("+"+focus.getLevel()+" "+focus.getName());
//			}
//		}
//		return String.join("\n", data);
//	}

	//-------------------------------------------------------------------
	/*
	 * Called from Shadowrun6_Print
	 */
	public static Damage getWeaponDamage(ShadowrunCharacter model, CarriedItem item) {
		if (item.getItem().getWeaponData()==null) {
			throw new IllegalArgumentException(item.getName()+" is not a weapon but a "+item.getItem().getTypes()+" and of type "+item.getItem().getClass());
		}

//		return ((Damage)model.getItem("unarmed").getAsValue(ItemAttribute.DAMAGE));
		
		Damage damage = (Damage)item.getAsValue(ItemAttribute.DAMAGE);
		if (damage.isAddStrength()) {
			AttributeValue val = model.getAttribute(Attribute.STRENGTH);
			int strHalf = Math.round( val.getModifiedValue() / 2.0f);
			Damage damage2 = new Damage();
			damage2.setValue(damage.getValue() + strHalf);
			damage2.setType(damage.getType());
			damage2.setModifications(damage.getModifications());
			return damage2;
		}
		return damage;
	}

	//-------------------------------------------------------------------
	public static String getRequirementString(Requirement check) {
		if (check instanceof ItemRequirement) {
			ItemRequirement req = (ItemRequirement)check;
			ItemTemplate item = ShadowrunCore.getItem(req.getItemID());
			if (req.isNegated())
				return Resource.format(CORE, "requirement.item.not_present", item.getName());
			else
				return Resource.format(CORE, "requirement.item.present", item.getName());
		} else if (check instanceof AttributeRequirement) {
			AttributeRequirement req = (AttributeRequirement)check;
			if (req.isNegated())
				return req.getAttribute().getName()+" < "+req.getValue();
			else
				return req.getAttribute().getName()+" >= "+req.getValue();
		} else {
			System.err.println("No getRequirementString for "+check.getClass());
			logger.error("No getRequirementString for "+check.getClass());
			return String.valueOf(check);
		}
	}

	//-------------------------------------------------------------------
	public static String getModificationString(Modification mod) {
		if (mod instanceof AddNuyenModification) {
			AddNuyenModification aMod = (AddNuyenModification)mod;
			if (aMod.getValue()>0)
				return "+ \u00A5"+aMod.getValue();
			else
				return "- \u00A5"+Math.abs(aMod.getValue());
		} else if (mod instanceof AdeptPowerModification) {
			AdeptPowerModification aMod = (AdeptPowerModification)mod;
			StringBuffer buf = new StringBuffer(aMod.getModifiedItem().getName());
			if (aMod.getModifiedItem().needsChoice()) {
				if (aMod.getChoice()==null) {
					buf.append(" (ANY)");
				} else {
					Object resolvedO = resolveChoiceType(aMod.getModifiedItem().getSelectFrom(), aMod.getChoice());
					if (resolvedO==null) {
						buf.append(" (Unknown)");
					} else if (resolvedO instanceof Sense) {
						buf.append(" ("+((Sense)resolvedO).getName()+")");
					} else if (resolvedO instanceof BasePluginData) {
						BasePluginData resolved = (BasePluginData)resolvedO;
						buf.append(" ("+resolved.getName()+")");
					} else {
						logger.error("Don't know how to deal with "+resolvedO.getClass());
					}
				}
			}						
			if (aMod.getValue()>0)
				buf.append(" "+aMod.getValue());
			return buf.toString();
		} else if (mod instanceof AttributeModification) {
			AttributeModification aMod = (AttributeModification)mod;
			if (aMod.getValue()>=100)
				return aMod.getAttribute().getName()+" +"+((float)aMod.getValue())/1000f;
			if (aMod.getValue()>0)
				return aMod.getAttribute().getName()+" +"+aMod.getValue();
			else
				return aMod.getAttribute().getName()+" -"+Math.abs(aMod.getValue());

		} else if (mod instanceof SkillModification) {
			SkillModification sMod = (SkillModification)mod;
			try {
				if (sMod.getValue()>0)
					return sMod.getSkill().getName()+" +"+sMod.getValue();
				else
					return sMod.getSkill().getName()+" -"+Math.abs(sMod.getValue());
			} catch (Exception e) {
				logger.error("Failed",e);
				return String.valueOf(sMod);
			}

		} else if (mod instanceof QualityModification) {
			QualityModification qMod = (QualityModification)mod;
			if (qMod.isRemove()) {
				return Resource.get(CORE,"label.remove")+" "+qMod.getModifiedItem().getName();
			} else
				return qMod.getModifiedItem().getName();
		} else if (mod instanceof SpellModification) {
			SpellModification qMod = (SpellModification)mod;
			return Resource.get(CORE,"label.spell")+" "+qMod.getSpell().getName();
		} else if (mod instanceof RitualModification) {
			RitualModification qMod = (RitualModification)mod;
			return Resource.get(CORE,"label.ritual")+" "+qMod.getRitual().getName();
		} else if (mod instanceof ItemHookModification) {
			ItemHookModification qMod = (ItemHookModification)mod;
			return Resource.format(CORE,"label.addhook", qMod.getHook().getName(), qMod.getCapacity());
		} else if (mod instanceof ItemAttributeModification) {
			ItemAttributeModification aMod = (ItemAttributeModification)mod;
			if (aMod.getValue()>0)
				return " +"+aMod.getValue()+" ("+getModificationSourceString(aMod.getSource())+")";
			else
				return " -"+Math.abs(aMod.getValue())+" ("+getModificationSourceString(aMod.getSource())+")";

		} else if (mod instanceof SkillSpecializationModification) {
			SkillSpecializationModification sMod = (SkillSpecializationModification)mod;
			if (sMod.isExpertise())
				return Resource.format(CORE,"label.skill.expertise", sMod.getSkill().getName(), sMod.getSkillSpecialization().getName());
			else
				return Resource.format(CORE,"label.skill.specialization", sMod.getSkill().getName(), sMod.getSkillSpecialization().getName());
		} else if (mod instanceof LifestyleCostModification) {
			LifestyleCostModification sMod = (LifestyleCostModification)mod;
			try {
				return Resource.format(CORE,"mod.lifestylecost", sMod.getPercent());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return sMod.toString();
			}
		} else if (mod instanceof MetamagicOrEchoModification) {
			MetamagicOrEchoModification mMod = (MetamagicOrEchoModification)mod;
			if (mMod.getMetamagicOrEcho().getType()==Type.ECHO) {
				return Resource.get(CORE,"label.echo")+" "+mMod.getMetamagicOrEcho().getName();
			} else {
				return Resource.get(CORE,"label.metamagic")+" "+mMod.getMetamagicOrEcho().getName();
			}
		} else if (mod instanceof CarriedItemModification) {
			CarriedItemModification cMod = (CarriedItemModification)mod;
			if (cMod.isRemove())
				return Resource.get(CORE,"mod.carrieditem.remove")+" "+cMod.getItem().getName();
			return Resource.get(CORE,"mod.carrieditem.add")+" "+cMod.getItem().getName();
		} else {
			System.err.println("No getModificationString for "+mod.getClass());
			logger.error("No getModificationString for "+mod.getClass());
			return String.valueOf(mod);
		}
	}

	//-------------------------------------------------------------------
	public static String getModificationSourceString(Object source) {
		if (source==null)
			return "Unknown source";
		if (source instanceof Attribute) 
			return ((Attribute)source).getName();
		if (source instanceof ItemAttribute) 
			return ((ItemAttribute)source).getName();
		if (source instanceof Skill) 
			return ((Skill)source).getName();
		if (source instanceof CarriedItem) {
			return ((CarriedItem)source).getName();
		} else if (source instanceof AdeptPower) {
			return Resource.get(CORE, "label.adeptpower")+" "+((AdeptPower)source).getName();
		} else if (source instanceof ItemTemplate) {
			return ((ItemTemplate)source).getName();
		} else if (source instanceof String) {
			return (String)source;
		} else if (source instanceof BasePluginData) {
			return ((BasePluginData)source).getName();
		}
		logger.warn("Missing treatment for modification source: "+source.getClass());
		return source.getClass().getSimpleName();
	}

	//-------------------------------------------------------------------
	public static String getModificationSourceString(Object source, int val) {
		if (val>0)
			return "+"+val+" "+getModificationSourceString(source);
		if (val<0)
			return val+" "+getModificationSourceString(source);
		return "\u00B10 "+getModificationSourceString(source);
	}

	//-------------------------------------------------------------------
	private static HistoryElementImpl makeFromReward(ProductService sessServ, ShadowrunCharacter charac, Reward reward) {
		HistoryElementImpl current = new HistoryElementImpl();
		current.setName(reward.getTitle());
		Adventure adv = null;
		if (reward.getId()!=null) {
			adv = sessServ.getAdventure(RoleplayingSystem.SHADOWRUN6, reward.getId());
			if (adv==null) {
				logger.warn("Rewards of character '"+charac.getName()+"' reference an unknown adventure: "+reward.getId());
			}
		}
		current.addGained(reward);
		return current;
	}

	//-------------------------------------------------------------------
	/**
	 * @param aggregate Aggregate history elements with same adventure
	 */
	public static List<HistoryElement> convertToHistoryElementList(ShadowrunCharacter charac, boolean aggregate) {
		List<HistoryElement> ret = new ArrayList<HistoryElement>();

		if (charac.getRewards().isEmpty())
			return ret;
		
		// Initial reward
		logger.debug("Sort "+charac.getRewards().size()+" rewards  and "+charac.getHistory().size()+" mods");
		
		Reward firstReward = charac.getRewards().get(0);
		
		/*
		 * Build a merged list of rewards and modifications and sort it by time
		 */
		List<Datable> rewardsAndMods = new ArrayList<Datable>();
		rewardsAndMods.addAll(charac.getRewards());
		rewardsAndMods.remove(firstReward);
		for (Modification mod : charac.getHistory()) {
			rewardsAndMods.add(mod.clone());
		}
//		rewardsAndMods.addAll(charac.getHistory());
		logger.trace("Unsorted = "+rewardsAndMods);
		sort(rewardsAndMods);
		logger.trace("Sorted = "+rewardsAndMods);

		
		ProductService sessServ = ProductServiceLoader.getInstance();
		/*
		 * Now build a list of HistoryElements. Start a new H
		 */
		HistoryElementImpl current = makeFromReward(sessServ, charac, firstReward);
		ret.add(current);

		for (Datable item : rewardsAndMods) {
			if (item instanceof RewardImpl) {
				Reward reward = (RewardImpl)item;
				Adventure adv = null;
				if (reward.getId()!=null) {
					adv = sessServ.getAdventure(RoleplayingSystem.SHADOWRUN6, reward.getId());
					if (adv==null) {
						logger.warn("Rewards of character '"+charac.getName()+"' reference an unknown adventure: "+reward.getId());
					}
				}
				// If is same adventure as current, keep same history element
				if (!aggregate || !(adv!=null && current!=null && adv.getId().equals(current.getAdventureID())) ) {
					current = new HistoryElementImpl();
					current.setName(reward.getTitle());
					if (adv!=null) {
						current.setName(adv.getTitle());
						current.setAdventure(adv);
					}
					ret.add(current);
				}
				current.addGained(reward);
			} else if (item instanceof ModificationBase) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
				} else {
					Modification lastMod = (current.getSpent().isEmpty())?null:current.getSpent().get(current.getSpent().size()-1);
					if (lastMod!=null && lastMod.getClass()==item.getClass()) {
						if (item instanceof SkillModification) {
//							logger.debug("Combine "+lastMod+" with "+item);
							// Aggregate same skill
							SkillModification lastSMod = (SkillModification)lastMod;
							SkillModification newtMod = (SkillModification)item;
							if (lastSMod.getSkill()==newtMod.getSkill()) {
								// Same skill
								lastSMod.setValue(newtMod.getValue());
								lastSMod.setExpCost(lastSMod.getExpCost() + newtMod.getExpCost());
							} else {
								// Different skill
								current.addSpent((ModificationBase<?>) item);
							}
						} else {
							current.addSpent((ModificationBase<?>) item);
						}
					} else {
						current.addSpent((ModificationBase<?>) item);
					}
				}
			} else if (item instanceof AddNuyenModification) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
					current = new HistoryElementImpl();
					current.setName("???");
					current.addSpent((Modification) item);
					ret.add(current);
				} else {
				}
			} else if (item instanceof SpellModification || item instanceof ComplexFormModification) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
					current = new HistoryElementImpl();
					current.setName("???");
					current.addSpent((Modification) item);
					ret.add(current);
				} else {
					current.addSpent((Modification) item);
				}
			} else if (item instanceof MetamagicOrEchoModification) {
				logger.info("....mod "+((Modification)item).getExpCost());
//				if (current==null) {
//					logger.error("Failed preparing history: Exp spent on modification without previous reward");
//					current = new HistoryElementImpl();
//					current.setName("???");
//					current.addSpent((Modification) item);
//					ret.add(current);
//				} else {
					current.addSpent((Modification) item);
//				}
			} else {
				logger.error("Don't know how to "+item);
			}
		}


		logger.trace("  return "+ret.size()+" elements");
		return ret;
	}

	//-------------------------------------------------------------------
	private static void sort(List<Datable> rewardsAndMods) {
		Collections.sort(rewardsAndMods, new Comparator<Datable>() {
			public int compare(Datable o1, Datable o2) {
				Long time1 = 0L;
				Long time2 = 0L;
				if (o1.getDate()!=null)	time1 = o1.getDate().getTime();
				if (o2.getDate()!=null)	time2 = o2.getDate().getTime();

				int cmp = time1.compareTo(time2);
				if (cmp==0) {
					if (o1 instanceof RewardImpl && o2 instanceof ModificationBase) return -1;
					if (o1 instanceof ModificationBase && o2 instanceof RewardImpl) return  1;
				}
				return cmp;
			}
		});
	}


	//-------------------------------------------------------------------
	public static boolean isRequirementMet(Requirement req, ItemTemplate item) {

		if (req instanceof ItemHookRequirement) {
			ItemHookRequirement hookReq = (ItemHookRequirement)req;
			if (hookReq.getSlot()!=null) {
				for (ItemHookModification hookMod : item.getSlots()) {
					if (hookMod.getHook()==hookReq.getSlot()) {
						logger.debug("Required slot "+hookReq.getSlot()+" found in "+item);
						return true;
					}
				}
				return false;
			}
		} else if (req instanceof ItemRequirement) {
			ItemRequirement itemReq = (ItemRequirement)req;
			if (itemReq.isNegated())
				return !itemReq.getItemID().equals(item.getId());
			else
				return itemReq.getItemID().equals(item.getId());
		} else if (req instanceof ItemTypeRequirement) {
			ItemTypeRequirement itemReq = (ItemTypeRequirement)req;
			if (itemReq.isNegated())
				return !item.isType(itemReq.getType());
			else
				return item.isType(itemReq.getType());
		} else if (req instanceof ItemSubTypeRequirement) {
			ItemSubTypeRequirement itemReq = (ItemSubTypeRequirement)req;
			if (itemReq.isNegated())
				return !item.hasSubtype(itemReq.getType());
			else
				return item.hasSubtype(itemReq.getType());
		} else if (req instanceof AttributeRequirement) {
			AttributeRequirement attrReq = (AttributeRequirement)req;
			if (item.getId().startsWith("weapon_mount") && attrReq.getAttribute()==Attribute.STRENGTH)
				return true;
		} else if (req instanceof AnyRequirement) {
			AnyRequirement anyReq = (AnyRequirement)req;
			for (Requirement tmp : anyReq.getOptionList()) {
				if (isRequirementMet(tmp, item))
					return true;
			}
			return false;
		}
		logger.warn("TODO: check requirement "+req.getClass()+" for "+req+" on item "+item);
		System.err.println("TODO: check requirement "+req.getClass()+" for "+req+" on item "+item);

		return false;
	}

	//-------------------------------------------------------------------
	public static ItemAttributeNumericalValue getItemAttribute(ShadowrunCharacter model, CarriedItem item, ItemAttribute attr) {
		ItemAttributeNumericalValue val = item.getAsValue(attr);

		// Apply modifications from accessory slots
		for (CarriedItem accessory : item.getUserAddedAccessories()) {
			for (Modification mod : accessory.getModifications()) {
				if ((mod instanceof ItemAttributeModification) && ((ItemAttributeModification)mod).getAttribute()==attr) {
					val.addModification(mod);
				}
			}
		}

		switch (attr) {
		case ESSENCECOST:
			if (model.hasQuality("sensitive_system")) {
				val.setPoints(val.getPoints()*2);
			}
			break;
		default:
		}

		// TODO: check requirements
		return val;
	}

	//-------------------------------------------------------------------
	public static String getItemAttributeString(ShadowrunCharacter model, CarriedItem item, ItemAttribute attr) {
		switch (attr) {
		case MODE:
			return String.valueOf(item.getAsObject(attr));
		case SKILL:
			return ((Skill)item.getAsObject(attr).getValue()).getName();
		case PRICE:
			return String.valueOf(item.getAsObject(attr));
		case AMMUNITION:
			return String.valueOf(item.getAsObject(attr).getValue());
		default:
			ItemAttributeNumericalValue val = getItemAttribute(model, item, attr);
			if (val.getModifier()==0)
				return String.valueOf(val.getPoints());
			else
				return val.getPoints()+" ("+val.getModifiedValue()+")";
		}
	}

	//--------------------------------------------------------------------
	public static Map<ItemAttribute, String> getItemAttributeStrings(CarriedItem item) {
		Map<ItemAttribute, String> ret = new HashMap<ItemAttribute, String>();

		ret.put(ItemAttribute.AVAILABILITY  , String.valueOf(item.getAvailability()));
		if (item.isType(ItemType.WEAPON)) {
			ret.put(ItemAttribute.AMMUNITION, item.getAsValue(ItemAttribute.AMMUNITION).toString());
			ret.put(ItemAttribute.DAMAGE    , item.getAsValue(ItemAttribute.DAMAGE).toString());
		}

		return ret;
	}

	//-------------------------------------------------------------------
	public static String getDrainString(Spell spell) {
		if (spell.getDrain()<0)
			return CORE.getString("label.drain.short")+" "+spell.getDrain();
		if (spell.getDrain()>0)
			return CORE.getString("label.drain.short")+" +"+spell.getDrain();
		return CORE.getString("label.drain.short");
	}

	//--------------------------------------------------------------------
	public static int getLifestyleCost(ShadowrunCharacter model, LifestyleValue val) {
		List<LifestyleOption> options = new ArrayList<>();
		for (LifestyleOptionValue opt : val.getOptions())
			options.add(opt.getOption());
		
		int costPerMonth =  getLifestyleCost(model, val.getLifestyle(), options, val.getModifications());
		return costPerMonth * val.getPaidMonths();
	}

	//--------------------------------------------------------------------
	public static int getLifestyleCost(ShadowrunCharacter model, Lifestyle base, List<LifestyleOption> options, List<Modification> staticMods) {
		if (model==null)
			throw new NullPointerException("model is null");
		if (base==null)
			throw new NullPointerException("Lifestyle is null");
		if (options==null)
			throw new NullPointerException("options is null");
		float sum = base.getCost();
		for (LifestyleOption opt : options) {
			if (opt==null)
				continue;
			for (Modification mod : opt.getModifications()) {
				if (mod instanceof LifestyleCostModification) {
					LifestyleCostModification lcMod = (LifestyleCostModification)mod;
					if (lcMod.getPercent()!=0) {
						sum += base.getCost() * lcMod.getPercent() / 100.0;
					} else {
						sum += lcMod.getFixed();
					}
				}
			}
		}

		for (Modification mod : staticMods) {
			if (mod instanceof LifestyleCostModification) {
				LifestyleCostModification lcMod = (LifestyleCostModification)mod;
				if (lcMod.getPercent()!=0) {
					sum += base.getCost() * lcMod.getPercent() / 100.0;
				} else {
					sum += lcMod.getFixed();
				}
			}
		}
//		// Metatype
//		if (model.getMetatype()!=null) {
//			if (model.getMetatype().getId().equals("dwarf") || (model.getMetatype().getVariantOf()!=null && model.getMetatype().getVariantOf().getId().equals("dwarf"))) {
//				sum *= 1.2;
//			} else if (model.getMetatype().getId().equals("troll") || (model.getMetatype().getVariantOf()!=null && model.getMetatype().getVariantOf().getId().equals("troll"))) {
//				sum *= 2;
//			}
//		}
		return Math.round(sum);
	}


	//-------------------------------------------------------------------
	public static void reward(ShadowrunCharacter charac, RewardImpl reward) {
		logger.info("Add reward "+reward+" to "+charac);
		charac.addReward(reward);

		charac.setKarmaFree(charac.getKarmaFree() + reward.getExperiencePoints());
		for (Modification mod : reward.getModifications()) {
			if (mod instanceof AddNuyenModification) {
				AddNuyenModification nMod = (AddNuyenModification)mod;
				logger.info("Add nuyen: "+nMod.getValue());
				charac.setNuyen(charac.getNuyen() + nMod.getValue());
				// Add to history
				charac.addToHistory(mod);

//			if (mod instanceof ResourceModification) {
//				Resource res = ((ResourceModification)mod).getResource();
//				int      val = ((ResourceModification)mod).getValue();
//				String title = ((ResourceModification)mod).getResourceName();
//
//				if (res.isBaseResource()) {
//					for (ResourceReference ref : charac.getResources()) {
//						if (ref.getResource()==res) {
//							ref.setValue(ref.getValue() + val);
//							logger.info("Reward existing resource to "+ref);
//							ref.setDescription(title);
//							break;
//						}
//					}
//				} else {
//					ResourceReference ref = new ResourceReference(res, val);
//					ref.setDescription(title);
//					logger.info("Reward new resource "+ref);
//					charac.addResource(ref);
//				}
//				// Add to history
//				charac.addToHistory(mod);
			} else {
				logger.error("Unsupported modification: "+mod.getClass());
			}
		}
	}

//	//-------------------------------------------------------------------
//	public static void equip(ShadowrunCharacter model, CarriedItem item) {
//		logger.info("Equip "+item.getName());
//
//		List<Modification> mods = item.getCharacterModifications();
//		for (Modification tmp : mods) {
//			tmp.setSource(item);
//			if (tmp instanceof AttributeModification) {
//				AttributeModification mod = (AttributeModification)tmp;
//				model.getAttribute(mod.getAttribute()).addModification(mod);
//				logger.debug(" add mod "+mod);
//			} else if (tmp instanceof CarriedItemModification) {
//				CarriedItemModification mod = (CarriedItemModification)tmp;
//				if (mod.isRemove()) {
//					try {
//						CarriedItem toRemove = model.getItem(mod.getItem().getId());
//						if (toRemove!=null) {
//							logger.info("  remove "+toRemove);
//							model.removeAutoItem(toRemove);
//						}
//					} catch (NoSuchElementException e) {
//						// Character does not have this item
//					}
//				} else {
//					logger.info("  add "+mod.getItem());
//				}
//			} else if (tmp instanceof SkillModification) {
//				SkillModification mod = (SkillModification)tmp;
//				model.getSkillValue(mod.getSkill()).addModification(mod);
//				logger.info(" added mod "+mod+" to "+model.getSkillValue(mod.getSkill()));
//			} else
//				logger.warn("Don't know how to deal with "+tmp.getClass());
//		}
//	}
//
//	//-------------------------------------------------------------------
//	public static void unequip(ShadowrunCharacter model, CarriedItem item) {
//		logger.info("Unequip "+item.getName());
//
//		List<Modification> mods = item.getCharacterModifications();
//		for (Modification tmp : mods) {
//			if (tmp instanceof AttributeModification) {
//				AttributeModification mod = (AttributeModification)tmp;
//				model.getAttribute(mod.getAttribute()).removeModification(mod);
//			} else
//				logger.warn("Don't know how to deal with "+tmp.getClass());
//		}
//	}
//
//	//-------------------------------------------------------------------
//	public static void loadEquipmentModificatdions(ShadowrunCharacter model) {
//		logger.info("----------------loadEquipmentModifications");
//		logger.debug("Update modifications from equipment");
//		for (CarriedItem item : model.getItems(true))
//			equip(model, item);
//	}

	//-------------------------------------------------------------------
	public static SkillSpecialization getSpecializationForVehicle(ItemTemplate item) {
		Skill pilot = ShadowrunCore.getSkill("piloting");
		if (pilot==null)
			return null;
		
		ItemType typeI = item.getTypes().get(0);
		switch (typeI) {
		case VEHICLES:
			switch (item.getSubtype(typeI)) {
			case BIKES:
			case CARS:
			case TRUCKS:
				return pilot.getSpecialization("ground_craft") ;
			case BOATS:
			case SUBMARINES:
				return pilot.getSpecialization("watercraft") ;
			case FIXED_WING:
			case ROTORCRAFT:
			case VTOL:
				return pilot.getSpecialization("aircraft") ;
			default:
			}
			break;
		case DRONES:
			VehicleType type = item.getVehicleData().getType();
			if (type==null) {
				logger.error("Cannot detect skill for drone without type");
				return null;
			}
			switch (type) {
			case GROUND:
				return pilot.getSpecialization("ground_craft") ;
			case AIR:
				return pilot.getSpecialization("aircraft") ;
			case WATER:
				return pilot.getSpecialization("watercraft") ;
			}
//			switch (item.getSubtype()) {
//			case BIKES:
//			case CARS:
//			case TRUCKS:
//				return ShadowrunCore.getSkill("pilot_ground_craft");
//			case BOATS:
//			case SUBMARINES:
//				return ShadowrunCore.getSkill("pilot_watercraft");
//			case FIXED_WING:
//			case ROTORCRAFT:
//			case VTOL:
//				return ShadowrunCore.getSkill("pilot_aircraft");
//			default:
//			}

		default:
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static List<CarriedItem> getMatrixItems(ShadowrunCharacter model) {
		List<CarriedItem> ret = model.getItems(true, ItemType.ELECTRONICS).stream()
				.filter(item -> item.isSubType(ItemSubType.COMMLINK) || item.isSubType(ItemSubType.CYBERDECK) || item.isSubType(ItemSubType.RIGGER_CONSOLE))
				.collect(Collectors.toList());

		if (model!=null && model.getMagicOrResonanceType()!=null) {
			if (model.getMagicOrResonanceType().usesResonance()) {
				LivingPersona persona = new LivingPersona(model);
				ret.add(0, persona);
			}
		}

		// Cyberdecks in cyberware
		for (CarriedItem item3 : model.getItems(true, ItemType.CYBERWARE)) {
			List<CarriedItem> accs = item3.getUserAddedAccessories().stream()
					.filter(item -> item.isSubType(ItemSubType.COMMLINK) || item.isSubType(ItemSubType.CYBERDECK) || item.isSubType(ItemSubType.RIGGER_CONSOLE))
					.collect(Collectors.toList());
			logger.debug("Embedded matrix items in "+item3.getItem().getId()+" = "+accs);
			ret.addAll(accs);
		}

		Collections.sort(ret, new Comparator<CarriedItem>() {
			public int compare(CarriedItem o1, CarriedItem o2) {
				return o1.getSubType().compareTo(o2.getSubType());
			}
		});
		return ret;
	}

	//-------------------------------------------------------------------
	public static int[] getMonitorArray(ShadowrunCharacter model, Attribute attr) {
		int add = 0;
//		int add = model.getAttribute(attr).getModifiedValue();
//		add = Math.round( (float)add / 2.0f);
		if (attr==Attribute.BODY && model.getAttribute(Attribute.PHYSICAL_MONITOR)!=null)
			add+=model.getAttribute(Attribute.PHYSICAL_MONITOR).getModifiedValue();
		if (attr==Attribute.WILLPOWER && model.getAttribute(Attribute.STUN_MONITOR)!=null)
			add+=model.getAttribute(Attribute.STUN_MONITOR).getModifiedValue();
		int[] ret = new int[add];

		int start = 0;
		int every = 3;
		if (model.hasAdeptPower("pain_resistance")) {
			start+=model.getAdeptPower("pain_resistance").getLevel();
		}

		for (int i=start; i<ret.length; i++) {
			ret[i] = - ((i+1-start)/every);
			if (attr==Attribute.BODY && model.hasQuality("high_pain_tolerance")) {
				if (ret[i]<0)
					ret[i]++;
			}
			if (attr==Attribute.BODY && model.hasQuality("low_pain_tolerance")) {
				ret[i]*=2;;
			}
		}
		logger.debug("array for "+attr+": "+Arrays.toString(ret));

		return ret;
	}

	//-------------------------------------------------------------------
	public static int[] getDroneMonitorArray(CarriedItem item) {
		ItemAttributeNumericalValue val = item.getAsValue(ItemAttribute.BODY);
		if (val==null)
			throw new IllegalArgumentException("Item does not have a BODY attribute");
		int add = 8 + Math.round(val.getModifiedValue()/2.0f);
		int[] ret = new int[add];

		int start = 0;
		int every = 3;

		for (int i=start; i<ret.length; i++) {
			ret[i] = - ((i+1-start)/every);
		}
		return ret;
	}
	
	//-------------------------------------------------------------------
	public static String makeFeatureString(Spell spell) {
		StringBuffer buf = new StringBuffer();
		Iterator<SpellFeatureReference> it = spell.getFeatures().iterator();
		while (it.hasNext()) {
			SpellFeatureReference ref = it.next();
			buf.append(ref.getFeature().getName());
			if (it.hasNext())
				buf.append(", ");
		}
		return buf.toString();
	}
	
	//-------------------------------------------------------------------
	public static String makeFeatureString(Ritual ritual) {
		StringBuffer buf = new StringBuffer();
		Iterator<RitualFeatureReference> it = ritual.getFeatures().iterator();
		while (it.hasNext()) {
			RitualFeatureReference ref = it.next();
			buf.append(ref.getFeature().getName());
			if (it.hasNext())
				buf.append(", ");
		}
		return buf.toString();
	}
	
	//-------------------------------------------------------------------
	public static List<BasePluginData> getInfluences(ShadowrunCharacter model) {
		List<BasePluginData> ret = new ArrayList<>();
		for (SkillValue val : model.getSkillValues()) {
			for (Modification tmp : val.getModifications()) {
				if (tmp instanceof EdgeModification) {
					if (!ret.contains(tmp.getSource())) {
						ret.add((BasePluginData) tmp.getSource());
					}
				}
			}
		}
		return ret;
	}
	
	//-------------------------------------------------------------------
	public static String getAttackRatingString(int[] attackRating) {
		String[] ratings = new String[attackRating.length];
		for (int i=0; i<ratings.length; i++) {
			int val = attackRating[i];
			if (val>0)
				ratings[i] = String.valueOf(val);
			else
				ratings[i] = "-";
		}
		return String.join("/", ratings);
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("incomplete-switch")
	public static String getInitiativeString(ShadowrunCharacter model, Attribute iniAttribute) {
		String base = model.getAttribute(iniAttribute).getDisplayString();
		switch (iniAttribute) {
		case INITIATIVE_PHYSICAL:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getModifiedValue());
		case INITIATIVE_MATRIX:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX).getModifiedValue());
		case INITIATIVE_MATRIX_VR_COLD:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_COLD).getModifiedValue());
		case INITIATIVE_MATRIX_VR_HOT:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_MATRIX_VR_HOT).getModifiedValue());
		case INITIATIVE_ASTRAL:
			return Resource.format(CORE, "label.ini", base, model.getAttribute(Attribute.INITIATIVE_DICE_ASTRAL).getModifiedValue());
		}
		return ""+base;
	}

	//-------------------------------------------------------------------
	public static boolean isRequirementMet(Requirement requirement, ShadowrunCharacter model) {

		if (requirement instanceof AttributeRequirement) {
			AttributeRequirement req = (AttributeRequirement)requirement;
			if (req.getAttribute()==null)
				return true;
			AttributeValue val = model.getAttribute(req.getAttribute());
			return val.getModifiedValue()>=req.getValue();
		}
		if (requirement instanceof SkillRequirement) {
			SkillRequirement req = (SkillRequirement)requirement;
			if (req.getSkill()==null)
				return true;
			SkillValue val = model.getSkillValue(req.getSkill());
			if (val==null && req.getValue()!=0)
				return false;
			return val.getPoints()>=req.getValue();
		}
		if (requirement instanceof AnyRequirement) {
			AnyRequirement req = (AnyRequirement)requirement;
			for (Requirement tmp : req.getOptionList()) {
				if (isRequirementMet(tmp, model))
					return true;
			}
			return false;
		}
		if (requirement instanceof MetatypeRequirement) {
			MetatypeRequirement req = (MetatypeRequirement)requirement;
			MetaType meta = req.getMetaType();
			if (meta==null) {
				logger.error("Requirement with unknown metatype "+req.getMetaTypeId());
				return false;
			}
			return meta==model.getMetatype() || (model.getMetatype()!=null && meta==model.getMetatype().getVariantOf());
		}
		if (requirement instanceof MagicOrResonanceRequirement) {
			MagicOrResonanceRequirement req = (MagicOrResonanceRequirement)requirement;
			MagicOrResonanceType type = req.getMagicOrResonanceType();
			if (type==null) {
				logger.error("Requirement with unknown magic or resonance ");
				return false;
			}
			return type==model.getMagicOrResonanceType();
		}
		if (requirement instanceof ItemRequirement) {
			ItemRequirement req = (ItemRequirement)requirement;
			CarriedItem item = model.getItem(req.getItemID());
			if (item==null && req.isNegated()) return true;
			if (item!=null && !req.isNegated()) return true;
			return false;
		}
		logger.warn("TODO: check requirement "+requirement.getClass());
		System.err.println("TODO: check requirement "+requirement.getClass());

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * Prepare a single section from a multicolumn table with sections
	 * @param <T> Data type
	 * @param <C> Column type. 
	 * @param <S> Section type. Should implement comparable
	 * @param data Data to sort
	 * @param section Section to return
	 * @param detectColumn Method to detect column
	 * @param detectSection Method to detect section
	 * @return
	 */
	public static <T,C,S> Map<C, List<T>> sortToColumns(List<T> data, S section, Function<T,C> detectColumn, Function<T,S> detectSection) {
		List<T> allSorted = new ArrayList<>(data);
		// Sort by sections
		Collections.sort(allSorted, new Comparator<T>() {
			@SuppressWarnings("unchecked")
			public int compare(T o1, T o2) {
				S section1 = detectSection.apply(o1);
				S section2 = detectSection.apply(o2);
				if (section1==null && section2==null) return 0;
				if (section1==null && section2!=null) return +1;
				if (section1!=null && section2==null) return -1;
				if (section instanceof Comparable) {
					return ((Comparable<S>)section1).compareTo(section2);
				} else
					return String.valueOf(section1).compareTo(String.valueOf(section2));
			}
		});
		
		Map<C, List<T>> ret = new HashMap<>();
		for (T item : data) {
			// Ignore data from unwanted section
			if (section!=detectSection.apply(item))
				continue;
			// Sort to matching column
			C column = detectColumn.apply(item);
			List<T> list = ret.get(column);
			if (list==null) {
				list = new ArrayList<>();
				ret.put(column, list);
			}
			list.add(item);
		}
		
		return ret;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @param <T>
	 * @param <C>
	 * @param data
	 * @param columns
	 * @param minRows
	 * @param detectCategory
	 * @param categoryCompare
	 * @return
	 */
	public static <T,C> List<Object>[] getAsBalancedCategoryTable(List<T> data, int columns, int minRows, Function<T, C> detectCategory, Comparator<C> categoryCompare) {
		Map<C, List<Object>> listsByCategory = new HashMap<>();
		// Sort all data into categorized lists
		for (T item : data) {
			C category = detectCategory.apply(item);
			List<Object> list = listsByCategory.get(category);
			if (list==null) {
				list = new ArrayList<>();
				list.add(category); // Add header to list
				listsByCategory.put(category, list);
			}
			list.add(item);
		}
		
		// Make a first guess for required rows
		int totalItems = data.size()+listsByCategory.size();
		int rowsFirstAssumption = totalItems/columns;
		if ((totalItems%columns)>0)
			rowsFirstAssumption++;
		int guessedRows = Math.max(minRows, rowsFirstAssumption);
		
		List<C> categories = new ArrayList<>(listsByCategory.keySet());
		Collections.sort(categories, categoryCompare);
		@SuppressWarnings("unchecked")
		Class<C> categoryClass = (Class<C>) categories.get(0).getClass();

		// Prepare result
		@SuppressWarnings("unchecked")
		List<Object>[] resultLists = new ArrayList[columns];
		for (int i=0; i<columns; i++)
			resultLists[i] = new ArrayList<>();

		/*
		 * Try several iterations
		 */
		int rows = guessedRows;
		outer:
		while ( (rows-guessedRows)<4 ) {
			int maxItems = rows*columns;
			// Build a combined list of all categories
			List<Object> all = new ArrayList<Object>();
			categories.forEach(cat -> all.addAll(listsByCategory.get(cat)));
			
			/*
			 * Ensure that there is no category at a column end.
			 * If so, fill an empty line there
			 */
			for (int col=0; col<columns; col++) {
				int colEnd = ((col+1)*rows)-1;
				Object item = (colEnd<all.size())?all.get(colEnd):null;
				if (item!=null && categoryClass.isInstance(item)) {
					// Last element in column was a category header
					// Inject an empty line here
					all.add(colEnd, null);
				}
			}
			// If after all eventually injects the number of items does
			// not exceed maximum, we are okay
			if (all.size()<=maxItems) {
				// Fill into result lists
				for (int i=0; i<columns; i++) {
					int to = Math.min( ((i+1)*rows), all.size());
					if (i*rows <= to)
						resultLists[i].addAll(all.subList(i*rows, to));
				}
				break outer;
			}
			// Otherwise try with a row more
			rows++;
		}
		
		return resultLists;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @param <T>
	 * @param <C>
	 * @param data
	 * @param columns
	 * @param minRows
	 * @param detectCategory
	 * @param categoryCompare
	 * @return
	 */
	public static <T,C> List<Object> getAsBalancedCategoryList(List<T> data, int columns, int minRows, Function<T, C> detectCategory, Comparator<C> categoryCompare) {
		List<Object>[] raw = getAsBalancedCategoryTable(data, columns, minRows, detectCategory, categoryCompare);
		List<Object> ret = new ArrayList<>();
		while (true) {
			for (int i=0; i<columns; i++) {
				if (raw[i].isEmpty()) {
					if (i==0)
						return ret;
					else
						ret.add(null);
				} else {
					ret.add(raw[i].remove(0));
				}
			}
		}
	}
	
	//--------------------------------------------------------------------
	/*
	 * Find the best commlink or cyber jack
	 */
	public static CarriedItem getBestMatrixDF(ShadowrunCharacter model) {
		CarriedItem bestDF = null;
		int bestSum = 0;
		for (CarriedItem item : model.getItems(true)) {
			if (!item.hasAttribute(ItemAttribute.DATA_PROCESSING))
				continue;
			logger.info("  consider for DF: "+item);
			int d = item.getAsValue(ItemAttribute.DATA_PROCESSING).getModifiedValue();
			int f = item.getAsValue(ItemAttribute.FIREWALL).getModifiedValue();
			int sum = d+f;
			if (sum>bestSum) {
				bestDF = item;
				bestSum= sum;
			}
		}
		return bestDF;
	}
	
	//--------------------------------------------------------------------
	/**
	 * Determine the most powerful RCC available
	 */
	public static CarriedItem getBestRCC(ShadowrunCharacter model) {
		CarriedItem ret = null;
		for (CarriedItem item : model.getItems(true, ItemType.ELECTRONICS)) {
			// Only evaluate RIGGER_CONSOLEs
			if (!item.getItem().isSubtype(ItemSubType.RIGGER_CONSOLE, ItemType.ELECTRONICS))
				continue;
			if (ret==null || ret.getAsValue(ItemAttribute.DEVICE_RATING).getModifiedValue()<item.getAsValue(ItemAttribute.DEVICE_RATING).getModifiedValue())
				ret = item;
		}
		return ret;
	}
	
	//--------------------------------------------------------------------
	/**
	 * Determine the dice pool in different modes when using the drone.
	 * May be 0, if not possible
	 * @param model  Character
	 * @param drone  Drone/Vehicle
	 * @param action Action to perform
	 * @return Pool object or null, if not a drone
	 */
	public static DronePool getDronePool(ShadowrunCharacter model, CarriedItem drone, DroneAction action) {
		// Ensure it is a drone or vehicle
		if (!drone.getItem().isType(ItemType.DRONES) && !drone.getItem().isType(ItemType.VEHICLES))
			return null;
		
		// Collect some necessary data
		CarriedItem simrig = model.getItem("control_rig");
		int simRigRating = (simrig==null)?0:simrig.getRating();
		CarriedItem rcc = ShadowrunTools.getBestRCC(model);
		
		DronePool pool = new DronePool();
		Skill pilotSkill = ShadowrunCore.getSkill("piloting");
		switch (action) {
		case EVADE:
			// Assumption: Use Skill Pilot to evade when jumped in
			SkillSpecialization spec = ShadowrunTools.getSpecializationForVehicle(drone.getItem());
			if (simrig!=null) {
				// Use INTUITION instead REACTION (jumped in attribte)
				pool.rigged = getSkillPool(model, pilotSkill, Attribute.INTUITION, (spec!=null)?spec.getId():null) + simRigRating;
			} else {
				// Jumped in impossible without rig
				pool.rigged = 0;
			}
			// Via RCC: Requires evasion autosoft on rcc
			if (rcc==null) {
				pool.viaRCC = 0;
			} else {
				CarriedItem autosoft = rcc.getEmbeddedItem("evasion");
				if (autosoft==null)
					pool.viaRCC = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue()-1;
				else
					pool.viaRCC = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue() + autosoft.getRating();
			}
			// Autonomous
			CarriedItem autosoft = drone.getEmbeddedItem("evasion");
			if (autosoft==null)
				pool.autonomous = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue()-1;
			else
				pool.autonomous = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue() + autosoft.getRating();
			break;
		case PERCEPTION:
			// Jumped in (dont make an assumption about specializations)
			if (simrig!=null) {
				// Use INTUITION instead REACTION (jumped in attribte)
				pool.rigged = getSkillPool(model, ShadowrunCore.getSkill("perception"), (String)null);
			} else {
				// Jumped in impossible without rig
				pool.rigged = 0;
			}
			// Via RCC: Requires evasion autosoft on rcc
			if (rcc==null) {
				pool.viaRCC = 0;
			} else {
				autosoft = rcc.getEmbeddedItem("clearsight");
				if (autosoft==null)
					pool.viaRCC = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue()-1;
				else
					pool.viaRCC = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue() + autosoft.getRating();
			}
			// Autonomous
			autosoft = drone.getEmbeddedItem("clearsight");
			if (autosoft==null)
				pool.autonomous = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue()-1;
			else
				pool.autonomous = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue() + autosoft.getRating();
			break;
		case CRACKING:
			// Jumped in
			if (simrig!=null) {
				pool.rigged = getSkillPool(model, ShadowrunCore.getSkill("cracking"), "electronic_warfare") + simRigRating;
			} else {
				// Jumped in impossible without rig
				pool.rigged = 0;
			}
			// Via RCC: Requires electronic warfare autosoft on rcc
			if (rcc==null) {
				pool.viaRCC = 0;
			} else {
				autosoft = rcc.getEmbeddedItem("electronic_warfare");
				if (autosoft==null)
					pool.viaRCC = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue()-1;
				else
					pool.viaRCC = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue() + autosoft.getRating();
			}
			// Autonomous
			autosoft = drone.getEmbeddedItem("electronic_warfare");
			if (autosoft==null)
				pool.autonomous = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue()-1;
			else
				pool.autonomous = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue() + autosoft.getRating();
			break;
		case STEALTH:
			// Jumped in
			if (simrig!=null) {
				pool.rigged = getSkillPool(model, ShadowrunCore.getSkill("stealth"), getSpecializationForVehicle(drone.getItem()).getId()) + simRigRating;
			} else {
				// Jumped in impossible without rig
				pool.rigged = 0;
			}
			// Via RCC: Requires stealth autosoft on rcc
			if (rcc==null) {
				pool.viaRCC = 0;
			} else {
				autosoft = rcc.getEmbeddedItem("stealth");
				if (autosoft==null)
					pool.viaRCC = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue()-1;
				else
					pool.viaRCC = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue() + autosoft.getRating();
			}
			// Autonomous
			autosoft = drone.getEmbeddedItem("stealth");
			if (autosoft==null)
				pool.autonomous = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue()-1;
			else
				pool.autonomous = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue() + autosoft.getRating();
			break;
		case PILOT:
			// Jumped in
			if (simrig!=null) {
				pool.rigged = getSkillPool(model, ShadowrunCore.getSkill("piloting"), getSpecializationForVehicle(drone.getItem()).getId()) + simRigRating;
			} else {
				// Jumped in impossible without rig
				pool.rigged = 0;
			}
			// Via RCC: Requires pilot maneuver on rcc
			if (rcc==null) {
				pool.viaRCC = 0;
			} else {
				autosoft = rcc.getEmbeddedItem("maneuvering");
				if (autosoft==null)
					pool.viaRCC = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue()-1;
				else
					pool.viaRCC = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue() + autosoft.getRating();
			}
			// Autonomous
			autosoft = drone.getEmbeddedItem("maneuvering");
			if (autosoft==null)
				pool.autonomous = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue()-1;
			else
				pool.autonomous = drone.getAsValue(ItemAttribute.PILOT).getModifiedValue() + autosoft.getRating();
			break;
		default:
			logger.warn("DroneAction "+action+" not implemented yet");
		}
		
		return pool;
	}
	
	//--------------------------------------------------------------------
	public static List<CarriedItem> getEmbeddedWeapons(CarriedItem container) {
		List<CarriedItem> ret = new ArrayList<>();
		container.getEffectiveAccessories().forEach(item -> {
			if (item.getItem().isType(ItemType.WEAPON)) {
				ret.add(item);
			}
			else if (item.getItem().getChoice()==ChoiceType.WEAPON && item.getChoice()!=null) {
				ret.add(new CarriedItem( (ItemTemplate)item.getChoice()) );
			}
			// Recurse
			ret.addAll(ShadowrunTools.getEmbeddedWeapons(item));
		});
		return ret;
	}
	
	//--------------------------------------------------------------------
	/**
	 * Get the dice pools for using the weapon in a drone
	 * Rigged = Riggers skill with replaced physical attribute plus implanted
	 *          simrig rating
	 * RCC    = 
	 */
	public static DronePool getDroneWeaponPool(ShadowrunCharacter model, CarriedItem drone,  CarriedItem weapon) {
		return getDroneWeaponPool(model, drone, getBestRCC(model), weapon);
	}
	
	//--------------------------------------------------------------------
	/**
	 * Get the dice pools for using the weapon in a drone
	 * Rigged = Riggers skill with replaced physical attribute plus implanted
	 *          simrig rating
	 * RCC    = 
	 */
	public static DronePool getDroneWeaponPool(ShadowrunCharacter model, CarriedItem drone, CarriedItem rcc, CarriedItem weapon) {
		// Collect some necessary data
		CarriedItem rig = model.getItem("control_rig");
		int rigRating = (rig==null)?0:rig.getRating();
		
		DronePool pool = new DronePool();
		/*
		 *  Rigged
		 */
		Skill skill = ShadowrunCore.getSkill("engineering");
		if (rig==null) {
			pool.rigged = 0;
		} else {
			pool.rigged = getSkillPool(model, skill, "gunnery") + rigRating;
		}
		
		/*
		 * RCC
		 * Requires targeting autosoft on RCC
		 */
		if (rcc==null) {
			pool.viaRCC = 0;
		} else {
			// Search matching autosoft
			boolean anyTargeting = false;
			CarriedItem autosoft = null;
			for (CarriedItem item : rcc.getEffectiveAccessories()) {
				if (!item.getItem().getId().equals("targeting"))
					continue;
				anyTargeting = true;
				if (item.getChoiceReference()==null) {
					logger.warn("Weapon model of targeting autosoft not defined in drone "+drone);
				} else if (item.getChoiceReference().equals(weapon.getItem().getId()))
					autosoft = item;
			}
			if (autosoft!=null) {
				// Matching autosoft
				pool.viaRCC = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue() + autosoft.getRating();
			} else if (anyTargeting) {
				// At least a targeting autosoft for a different weapon
				pool.viaRCC = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue()-1;
			} else {
				// No targeting autosoft at all
				pool.viaRCC = 0;
			}
		}
		// Autonomous
		// Search matching autosoft
		boolean anyTargeting = false;
		CarriedItem autosoft = null;
		for (CarriedItem item : drone.getEffectiveAccessories()) {
			if (!item.getItem().getId().equals("targeting"))
				continue;
			anyTargeting = true;
			if (item.getChoiceReference()==null) {
				logger.warn("Weapon model of targeting autosoft not defined in drone "+drone);
			} else if (item.getChoiceReference().equals(weapon.getItem().getId()))
				autosoft = item;
		}
		if (autosoft!=null) {
			// Matching autosoft
			pool.autonomous = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue() + autosoft.getRating();
		} else if (anyTargeting) {
			// At least a targeting autosoft for a different weapon
			pool.autonomous = drone.getAsValue(ItemAttribute.SENSORS).getModifiedValue()-1;
		} else {
			// No targeting autosoft at all
			pool.autonomous = 0;
		}

		return pool;
	}
	
	//-------------------------------------------------------------------
	/*
	 * Now apply cost multiplier for dwarfs and trolls
	 * CRB 247 / GRW 248
	 */
	public static int getCostWithMetatypeModifier(ShadowrunCharacter model, ItemTemplate item, float cost) {
		MetaType meta = model.getMetatype();
		if (meta.getVariantOf()!=null)
			meta = meta.getVariantOf();
		switch (meta.getId()) {
		case "dwarf":
			if (item.isType(ItemType.ARMOR) || item.isType(ItemType.ARMOR_ADDITION)) {
				cost *= 1.1f;
			}
			break;
		case "troll":
			cost *= 1.1f;
			break;
		}
		return Math.round(cost);
	}
	
}

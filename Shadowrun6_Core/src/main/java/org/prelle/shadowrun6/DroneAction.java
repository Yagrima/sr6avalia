package org.prelle.shadowrun6;

/**
 * @author Stefan
 *
 */
public enum DroneAction {

	EVADE,
	PERCEPTION,
	CRACKING,
	PILOT,
	STEALTH,
	WEAPON
	;
	public String getName() {
		return Resource.get(ShadowrunCore.getI18nResources(), "droneaction."+name().toLowerCase());
	}
	
}

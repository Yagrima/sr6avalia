/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.persist.FocusConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class FocusValue extends ModifyableImpl implements Modifyable, Comparable<FocusValue> {

	@Attribute(name="focus")
	@AttribConvert(FocusConverter.class)
	private Focus focus;
	@Attribute
	private int level;
	@Attribute(name="name")
	private String name;
	@Attribute(name="choice")
	private String choiceReference;
	@Attribute(name="choice2")
	private String choicesChoiceReference;
	@Attribute(name="uniqueid")
	private UUID uniqueId;

	private transient Object choice;

	//-------------------------------------------------------------------
	public FocusValue() {
	}

	//-------------------------------------------------------------------
	public FocusValue(Focus focus, int rating) {
		this.focus = focus;
		level=rating;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Focus("+focus+",lvl="+level+",ch="+choice+",mods="+modifications+")";
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (name!=null)
			return name;
		return focus.getName();
	}
	
	//-------------------------------------------------------------------
	public String getType() {
		StringBuffer ret = new StringBuffer(focus.getName());
		if (choice!=null) {
			if (choice instanceof org.prelle.shadowrun6.Attribute)
				ret.append(" ("+((org.prelle.shadowrun6.Attribute)choice).getName()+")");
			else if (choice instanceof Skill)
				ret.append(" ("+((Skill)choice).getName()+")");
			else if (choice instanceof ShadowrunAction)
				ret.append(" ("+((ShadowrunAction)choice).getName()+")");
			else if (choice instanceof Sense)
				ret.append(" ("+((Sense)choice).getName()+")");
			else 
				ret.append(" ("+choice+")");
		}
		ret.append(" "+getLevel());
		return ret.toString();
	}

	//-------------------------------------------------------------------
	public String getId() {
		return focus.getId();
	}

	//-------------------------------------------------------------------
	public String getTypeId() {
		return focus.getTypeId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(FocusValue other) {
		return focus.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public Focus getModifyable() {
		return focus;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//--------------------------------------------------------------------
	public int getCostNuyen() {
		return level*focus.getNuyenCost();
	}

	//--------------------------------------------------------------------
	public int getCostKarma() {
		return level*focus.getBondingMultiplier();
	}

	//--------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	public String getChoiceText() {
		if (focus.getChoice()==null)
			return "";
		switch (focus.getChoice()) {
		case MELEE_WEAPON:
		case WEAPON:
			return ((ItemTemplate)choice).getName();
		case SPELL_CATEGORY:
			return ((Spell.Category)choice).getName();
		case SPIRIT:
			return ((Spirit)choice).getName();
		case ADEPT_POWER:
			return ((AdeptPower)choice).getName();
		}
		return "?"+focus.getChoice()+"?";
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * Return all modifications without FocusModification - if existing
	 * @see de.rpgframework.genericrpg.modification.Modifyable#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		List<Modification> ret = new ArrayList<Modification>();
//		for (Modification mod : modifications) {
//			if (!(mod instanceof FocusModification))
//				ret.add(mod);
//		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choicesChoiceReference
	 */
	public String getChoicesChoiceReference() {
		return choicesChoiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choicesChoiceReference the choicesChoiceReference to set
	 */
	public void setChoicesChoiceReference(String choicesChoiceReference) {
		this.choicesChoiceReference = choicesChoiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uniqueId
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uniqueId the uniqueId to set
	 */
	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.persist.SpellConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class SpellValue extends ModifyableImpl implements Modifyable, Comparable<SpellValue> {

	@Attribute(name="spell")
	@AttribConvert(SpellConverter.class)
	private Spell spell;
	@Attribute
	private boolean alchemistic;

	//-------------------------------------------------------------------
	public SpellValue() {
	}

	//-------------------------------------------------------------------
	public SpellValue(Spell spell) {
		this.spell = spell;
	}

	//-------------------------------------------------------------------
	public SpellValue(Spell spell, boolean alchemistic) {
		this.spell = spell;
		this.alchemistic = alchemistic;
	}

	//-------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o!=null && o instanceof SpellValue) {
			SpellValue other = (SpellValue)o;
			if (spell!=other.getModifyable()) return false;
			return alchemistic==other.isAlchemistic();
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SpellValue other) {
		return spell.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public Spell getModifyable() {
		return spell;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the alchemistic
	 */
	public boolean isAlchemistic() {
		return alchemistic;
	}

	//-------------------------------------------------------------------
	/**
	 * @param alchemistic the alchemistic to set
	 */
	public void setAlchemistic(boolean alchemistic) {
		this.alchemistic = alchemistic;
	}
	
	//-------------------------------------------------------------------
	public List<BasePluginData> getInfluences() {
		List<BasePluginData> ret = new ArrayList<>();
		for (Modification mod : modifications) {
//			if (mod instanceof SkillModification) {
//				SkillModification sMod = (SkillModification)mod;
//				if (sMod.getSkill()==skill && sMod.isConditional() && sMod.getModificationType()==ModificationValueType.CURRENT) {
//					if (sMod.getSource()==null) {
//						System.err.println("SkillValue.getInfluences: No source for SkillModification "+sMod);
//					} else if (! (sMod.getSource() instanceof BasePluginData)) {
//						System.err.println("SkillValue.getInfluences: Source of SkillModification "+sMod+" is of type "+sMod.getSource().getClass());
//					} else {
//						ret.add( (BasePluginData)sMod.getSource());
//					}
//				}
//			} else 
			if (mod instanceof EdgeModification) {
				EdgeModification eMod = (EdgeModification)mod;
				if (eMod.getSource()==null) {
					System.err.println("SkillValue.getInfluences: No source for EdgeModification "+eMod);
				} else if (! (eMod.getSource() instanceof BasePluginData)) {
					System.err.println("SkillValue.getInfluences: Source of EdgeModification "+eMod+" is of type "+eMod.getSource().getClass());
				} else {
					ret.add( (BasePluginData)eMod.getSource());
				}
				
			} else {
				LogManager.getLogger("shadowrun6.jfx").warn("Don't know how to deal with modification "+mod.getClass());
			}
		}
		return ret;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6;

/**
 * @author prelle
 *
 */
public enum DamageType {
	PHYSICAL,
	STUN,
	PHYSICAL_SPECIAL,
	STUN_SPECIAL,
	FIRE,
	COLD,
	ELECTRICITY,
	CHEMICAL
	;
	public String getName() { return Resource.get(ShadowrunCore.getI18nResources(),"damagetype."+name().toLowerCase()); }
	public String getShortName() { return Resource.get(ShadowrunCore.getI18nResources(),"damagetype."+name().toLowerCase()+".short"); }
}

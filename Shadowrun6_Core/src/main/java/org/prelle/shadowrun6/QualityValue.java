/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.modifications.ModificationList;
import org.prelle.shadowrun6.persist.QualityConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "qualityval")
public class QualityValue extends ModifyableImpl implements ModifyableValue<Quality>, Comparable<QualityValue> {

	/**
	 * For mystic adepts using a mentor spirit:
	 * Use modifications from Adept or Magician?
	 */
	public enum MentorSpiritMods {
		ADEPT,
		MAGICIAN
	}
	
	
	@Attribute(name="quality")
	@AttribConvert(QualityConverter.class)
	private Quality quality;
	/**
	 * The current value on the skill without the attributes
	 */
	@Attribute(name="val")
	private int value;
	@Element
	private String description;
	@Attribute(name="choice")
	private String choiceReference;
	@Attribute(name="mystic")
	private MentorSpiritMods mysticAdeptUses;
	@Element
	private ModificationList chosenMods;
	
	private transient Object choice;

	//-------------------------------------------------------------------
	public QualityValue() {
		modifications = new ModificationList();
	}

	//-------------------------------------------------------------------
	public QualityValue(Quality data, int val) {
		this();
		this.quality = data;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (modifications.isEmpty())
			return String.format("%s",getName()+"//"+description);
		return String.format("%s = %d (mod=%s)",
				getName(),
				value,
				String.valueOf(modifications)
				);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Quality getModifyable() {
		return quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return value + getModifier();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(QualityValue other) {
		return quality.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getNormalModifier()
	 */
	@Override
	public int getModifier() {
		int count = 0;
//		for (Modification mod : modifications) {
//			if (mod instanceof SkillModification) {
//				SkillModification sMod = (SkillModification)mod;
//				if (sMod.getSkill()==skill)
//					count += sMod.getValue();
//			}
//		}
		return count;
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (quality==null) 
			return "null";
		
		String ratingSuffix="";
		if (quality.getMax()>0)
			ratingSuffix=" "+value;

		return quality.getName()+ratingSuffix;
	}

	//-------------------------------------------------------------------
	public String getShortName() {
		if (quality==null) 
			return "null";
		
		String ratingSuffix="";
		if (quality.getMax()>0)
			ratingSuffix=" "+value;

		return quality.getShortName()+ratingSuffix;
	}

	//-------------------------------------------------------------------
	public String getNameSecondLine() {
		if (quality==null) 
			return null;
		
		if (description!=null)
			return description;
		
		if (choice!=null) {
			switch (quality.getSelect()) {
			case ATTRIBUTE:
			case PHYSICAL_ATTRIBUTE:
				return ((org.prelle.shadowrun6.Attribute)choice).getName();
			case COMBAT_SKILL:
			case SKILL:
				return ((Skill)choice).getName();
			case MATRIX_ACTION: return ((ShadowrunAction)choice).getName();
			case MENTOR_SPIRIT: return ((MentorSpirit)choice).getName();
			case SPIRIT: return ((Spirit)choice).getName();
			case SPRITE: return ((Sprite)choice).getName();
			case NAME  : return ((String)choice);
			case ELEMENTAL: return ((ElementType)choice).getName();
//			case SUBSTANCE_SEVERITY:
//			case SPIRIT:
			}
			System.out.println("QualityValue.getName(): Don't know how to work with choice of "+quality.getSelect());
			return quality.getName()+" ("+choice+")";
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<Modification> getModifications() {
		List<Modification> ret = new ArrayList<>();
//		if (!quality.needsChoice())
//			ret.addAll(quality.getModifications());
//		else
		ret.addAll(modifications);
		return ret;
	}

//	//-------------------------------------------------------------------
//	public void addModification(Modification mod) {
//		modifications.add(mod);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
		
		if (choice==null)
			return;
		
//		if (choiceReference==null)
//			System.err.println("Set ref by "+choice+"    // "+choice.getClass());
		if (choice instanceof BasePluginData) {
			choiceReference = ((BasePluginData)choice).getId();
		} else
			choiceReference = String.valueOf(choice);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	public void recalculateModifications() {
		clearModifications();
		for (Modification tmp : quality.getModifications()) {
			modifications.add( ShadowrunTools.instantiateModification(tmp, choice, value) );
			if (quality.needsChoice()) {
				if (choice!=null && choice instanceof Modifyable) {
					modifications.addAll(((Modifyable)choice).getModifications());
				}				
			}

		}
	}

	//--------------------------------------------------------------------
	/**
	 * @return the mysticAdeptUses
	 */
	public MentorSpiritMods getMysticAdeptUses() {
		return mysticAdeptUses;
	}

	//--------------------------------------------------------------------
	/**
	 * @param mysticAdeptUses the mysticAdeptUses to set
	 */
	public void setMysticAdeptUses(MentorSpiritMods mysticAdeptUses) {
		this.mysticAdeptUses = mysticAdeptUses;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the chosenMods
	 */
	public ModificationList getChosenMods() {
		return chosenMods;
	}

	//--------------------------------------------------------------------
	/**
	 * @param chosenMods the chosenMods to set
	 */
	public void setChosenMods(ModificationList chosenMods) {
		this.chosenMods = chosenMods;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.modifications.AdeptPowerModification;
import org.prelle.shadowrun6.persist.AdeptPowerConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
public class AdeptPowerValue extends ModifyableImpl implements Modifyable, Comparable<AdeptPowerValue> {

	@Attribute(name="power")
	@AttribConvert(AdeptPowerConverter.class)
	private AdeptPower power;
	@Attribute
	private int level;
//	@Attribute(name="selSkill")
//	@AttribConvert(SkillConverter.class)
//	private Skill selectedSkill;
//	@Attribute(name="selAttr")
//	private org.prelle.shadowrun.Attribute selectedAttribute;
	@Attribute(name="choice")
	private String choiceReference;
	@Attribute(name="uniqueid")
	private UUID uniqueId;
	private transient Object choice;
	private transient List<AdeptPowerModification> valueMods;

	//-------------------------------------------------------------------
	public AdeptPowerValue() {
		uniqueId = UUID.randomUUID();
		valueMods = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue(AdeptPower power) {
		this();
		this.power = power;
		if (power.hasLevels())
			level=1;
	}

	//-------------------------------------------------------------------
	public AdeptPowerValue(AdeptPower power, AdeptPowerModification autoMod) {
		this();
		this.power = power;
		valueMods.add(autoMod);
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Power("+power+",lvl="+level+",ch="+choice+",mods="+modifications+",autolvl="+getAutoLevel()+")";
	}

	//-------------------------------------------------------------------
	public String getName() {
		StringBuffer ret = new StringBuffer(power.getName());
//		if (selectedSkill!=null) {
//			ret.append(" ("+selectedSkill.getName()+")");
//		} else 
			if (choice!=null) {
				if (choice instanceof org.prelle.shadowrun6.Attribute)
					ret.append(" ("+((org.prelle.shadowrun6.Attribute)choice).getName()+")");
				else if (choice instanceof Skill)
					ret.append(" ("+((Skill)choice).getName()+")");
				else if (choice instanceof ShadowrunAction)
					ret.append(" ("+((ShadowrunAction)choice).getName()+")");
				else if (choice instanceof Sense)
					ret.append(" ("+((Sense)choice).getName()+")");
				else 
					ret.append(" ("+choice+")");
		}
		if (power.hasLevels())
			ret.append(" "+getModifiedLevel());
		return ret.toString();
	}

	//-------------------------------------------------------------------
	public String getId() {
		return power.getId();
	}

	//-------------------------------------------------------------------
	public String getTypeId() {
		return power.getTypeId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AdeptPowerValue other) {
		return power.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	public AdeptPower getModifyable() {
		return power;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	//--------------------------------------------------------------------
	public int getModifiedLevel() {
		return level + getAutoLevel();
	}

	//--------------------------------------------------------------------
	/**
	 * Determine the starting level for user selection
	 * @return the level
	 */
	public int getAutoLevel() {
		int payed = 0;
		for (Modification tmp : valueMods) {
			if (tmp instanceof AdeptPowerModification) {
				payed += ((AdeptPowerModification)tmp).getValue();
			}
		}
		return payed;
	}

	//--------------------------------------------------------------------
	public float getCost() {
		if (power.hasLevels()) {
			return power.getCostForLevel(level);
		}
		return (float)power.getCost();
	}

	//--------------------------------------------------------------------
	/**
	 * @param level the level to set
	 */
	public void setLevel(int level) {
		this.level = level;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choice
	 */
	public Object getChoice() {
		return choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choice the choice to set
	 */
	public void setChoice(Object choice) {
		this.choice = choice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the choiceReference
	 */
	public String getChoiceReference() {
		return choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @param choiceReference the choiceReference to set
	 */
	public void setChoiceReference(String choiceReference) {
		this.choiceReference = choiceReference;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uniqueId
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uniqueId the uniqueId to set
	 */
	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * Return all modifications without AdeptPowerModification - if existing
	 * @see de.rpgframework.genericrpg.modification.Modifyable#getModifications()
	 */
	@Override
	public List<Modification> getModifications() {
		List<Modification> ret = new ArrayList<Modification>();
		for (Modification mod : modifications) {
			if (!(mod instanceof AdeptPowerModification))
				ret.add(mod);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public void addModification(AdeptPowerModification mod) {
		valueMods.add(mod);
	}

	//-------------------------------------------------------------------
	public void clearValueModifications() {
		valueMods.clear();;
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class RitualConverter implements StringValueConverter<Ritual> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Ritual value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Ritual read(String idref) throws Exception {
		Ritual spell = ShadowrunCore.getRitual(idref);
		if (spell==null)
			throw new ReferenceException(ReferenceType.RITUAL, idref);

		return spell;
	}

}

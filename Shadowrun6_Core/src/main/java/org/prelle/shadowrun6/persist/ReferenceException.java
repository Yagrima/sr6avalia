/**
 *
 */
package org.prelle.shadowrun6.persist;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class ReferenceException extends RuntimeException {

	public enum ReferenceType {
		ACTION,
		ADEPTPOWER,
		COMPLEXFORM,
		FOCUS,
		ITEM,
		ITEM_ENHANCEMENT,
		MAGIC, // Magic or resonance
		METAMAGIC_ECHO,
		METATYPE,
		QUALITY,
		LICENSE_TYPE,
		LIFESTYLE,
		LIFESTYLE_OPTION,
		PROGRAM,
		RITUAL,
		RITUALFEATURE,
		SKILL,
		SKILLSPECIALIZATION,
		SPELL,
		SPELLFEATURE,
		SUMMONABLE,
		TRADITION,
	}

	private ReferenceType type;
	private String reference;

	//--------------------------------------------------------------------
	public ReferenceException(ReferenceType type, String ref) {
		super("Invalid reference to "+type+" '"+ref+"'");
		this.type = type;
		this.reference = ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ReferenceType getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

}

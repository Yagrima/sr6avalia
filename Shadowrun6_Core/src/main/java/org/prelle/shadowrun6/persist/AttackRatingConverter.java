package org.prelle.shadowrun6.persist;

import org.prelle.simplepersist.StringValueConverter;

public class AttackRatingConverter implements StringValueConverter<int[]> {
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public int[] read(String v) throws Exception {
		v = v.trim();
		
		int[] ret = new int[5];
		
		String[] buf = v.trim().split(",");
		for (int i=0; i<buf.length; i++) {
			if (buf[i].isEmpty())
				ret[i]=0;
			else
				ret[i] = Integer.parseInt(buf[i]);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(int[] v) throws Exception {
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<5; i++) {
			if (i>0)
				buf.append(",");
			if (v[i]!=0)
				buf.append(v);
		}
		return buf.toString();
	}
	
}
/**
 * 
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class SpellConverter implements StringValueConverter<Spell> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Spell value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Spell read(String idref) throws Exception {
		Spell spell = ShadowrunCore.getSpell(idref);
		if (spell==null)
			throw new ReferenceException(ReferenceType.SPELL, idref);

		return spell;
	}

}

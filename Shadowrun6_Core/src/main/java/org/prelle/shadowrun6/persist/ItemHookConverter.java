/**
 *
 */
package org.prelle.shadowrun6.persist;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

/**
 * @author Stefan
 *
 */
public class ItemHookConverter implements XMLElementConverter<ItemHook>, StringValueConverter<ItemHook> {

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.XMLElementConverter#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, ItemHook value) throws Exception {
		// TODO Auto-generated method stub

	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.XMLElementConverter#read(org.prelle.simplepersist.unmarshal.XMLTreeItem, javax.xml.stream.events.StartElement, javax.xml.stream.XMLEventReader)
	 */
	@Override
	public ItemHook read(XMLTreeItem node, StartElement ev,
			XMLEventReader evRd) throws Exception {
		XMLEvent nextEv = evRd.nextEvent();
		if (!nextEv.isCharacters()) {
			throw new IllegalArgumentException("Expect CHARACTERS after "+ev+", but got "+nextEv.getEventType());
		}

		return ItemHook.valueOf(nextEv.asCharacters().getData());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public ItemHook read(String v) throws Exception {
		v = v.trim();
		
		return ItemHook.valueOf(v);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(ItemHook v) throws Exception {
		return v.name();
	}

}

/**
 * 
 */
package org.prelle.shadowrun6.persist;

import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.persist.ReferenceException.ReferenceType;
import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class AdeptPowerConverter implements StringValueConverter<AdeptPower> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(AdeptPower value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public AdeptPower read(String idref) throws Exception {
		AdeptPower data = ShadowrunCore.getAdeptPower(idref);
		if (data==null)
			throw new ReferenceException(ReferenceType.ADEPTPOWER, idref);

		return data;
	}

}

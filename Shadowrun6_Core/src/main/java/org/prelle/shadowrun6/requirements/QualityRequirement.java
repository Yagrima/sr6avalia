package org.prelle.shadowrun6.requirements;

import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.persist.QualityConverter;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name = "qualreq")
public class QualityRequirement extends Requirement {

	@Attribute(name="ref")
	private String id;
    private transient Quality resolved;
   
    //-----------------------------------------------------------------------
    public QualityRequirement() {
    }
    
    //-----------------------------------------------------------------------
    public QualityRequirement(Quality attr) {
        this.resolved = attr;
        this.id  = attr.getId();
     }
    
    //-----------------------------------------------------------------------
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
   
    //-----------------------------------------------------------------------
    public String toString() {
        return id;
    }
 
    //-----------------------------------------------------------------------
    public Quality getQuality() {
    	if (resolved!=null)
    		return resolved;

    	if (resolve())
    		return resolved;
    	return null;
    }
    
    //-----------------------------------------------------------------------
    public void setQuality(Quality attr) {
        this.resolved = attr;
    }
     
    //-----------------------------------------------------------------------
    public Object clone() {
        return new QualityRequirement(resolved);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof QualityRequirement) {
            QualityRequirement amod = (QualityRequirement)o;
            return (amod.getQuality()==resolved);
        } else
            return false;
    }

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.requirements.Requirement#resolve()
	 */
	@Override
	public boolean resolve() {
		if (resolved!=null)
			return true;
		if (id==null)
			return false;

		try {
			resolved = (new QualityConverter()).read(id);
			return true;
		} catch (Exception e) {
			logger.error(e);
		}

		return false;
	}
 
}

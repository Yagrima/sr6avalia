/**
 * 
 */
package org.prelle.shadowrun6.requirements;

import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.persist.ItemHookConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="slotreq")
public class ItemHookRequirement extends Requirement {
	
	@Attribute(required=true)
	@AttribConvert(ItemHookConverter.class)
	private ItemHook slot;
	@Attribute
	private int capacity;
	
	//-------------------------------------------------------------------
	public ItemHookRequirement() {
	}
	
	//-------------------------------------------------------------------
	public ItemHookRequirement(ItemHook hook, int capa) {
		this.slot = hook;
		this.capacity = capa;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "requires "+slot+" with capacity "+capacity;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the slot
	 */
	public ItemHook getSlot() {
		return slot;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the capacity
	 */
	public int getCapacity() {
		return capacity;
	}

	//-------------------------------------------------------------------
	/**
	 * @param slot the slot to set
	 */
	public void setSlot(ItemHook slot) {
		this.slot = slot;
	}

	//-------------------------------------------------------------------
	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

}

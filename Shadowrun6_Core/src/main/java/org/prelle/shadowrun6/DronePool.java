package org.prelle.shadowrun6;

public class DronePool {
	public int autonomous;
	public int viaRCC;
	public int rigged;
	
	public String toString() {
		return rigged+"/"+viaRCC+"/"+autonomous;
	}
}
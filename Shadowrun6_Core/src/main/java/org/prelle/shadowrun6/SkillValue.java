/**
 *
 */
package org.prelle.shadowrun6;

import java.util.ArrayList;
import java.util.List;

import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.EdgeModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.SkillModification;
import org.prelle.shadowrun6.persist.SkillConverter;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

import de.rpgframework.genericrpg.ModifyableValue;
import de.rpgframework.genericrpg.NumericalValue;
import de.rpgframework.genericrpg.SelectedValue;
import de.rpgframework.genericrpg.SelfExplainingNumericalValue;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ModifyableImpl;

/**
 * @author prelle
 *
 */
@Root(name = "skillval")
public class SkillValue extends ModifyableImpl implements ModifyableValue<Skill>, Comparable<SkillValue>, SelfExplainingNumericalValue<Skill>, SelectedValue<Skill> {

	public final static int LANGLEVEL_EXISTING = 1;
	public final static int LANGLEVEL_SPECIALIST = 2;
	public final static int LANGLEVEL_EXPERT   = 3;
	public final static int LANGLEVEL_NATIVE   = 4;
	

	@Attribute(name="skill")
	@AttribConvert(SkillConverter.class)
	private Skill skill;
	@Attribute(name="custname")
	private String customName;
	/**
	 * The current value on the skill without the attributes
	 */
	@Attribute(name="val")
	private int value;
	@Attribute
	private int start;
	@ElementList(entry="skillspec",type=SkillSpecializationValue.class,inline=true)
	private List<SkillSpecializationValue> specializations;

	//-------------------------------------------------------------------
	public SkillValue() {
		specializations = new ArrayList<SkillSpecializationValue>();
	}

	//-------------------------------------------------------------------
	public SkillValue(Skill skill, int val) {
		this();
		this.skill = skill;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s = %d (%d+%d, max %d)",
				getName(),
				getModifiedValue(),
				value,
				getModifier(),
				getMaximumModifier()
				);
		//		return skill+"="+value+" ("+masterships+")";
	}

//	//-------------------------------------------------------------------
//	public boolean equals(Object o) {
//		if (o instanceof SkillValue) {
//			SkillValue other = (SkillValue)o;
//			if (skill!=other.getSkill()) return false;
//			if (value!=other.getValue()) return false;
//			return masterships.equals(other.getMasterships());
//		}
//		return false;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifyable()
	 */
	@Override
	public Skill getModifyable() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getPoints()
	 */
	@Override
	public int getPoints() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getModifiedValue()
	 */
	@Override
	public int getModifiedValue() {
		return value + getModifier();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#setPoints(int)
	 */
	@Override
	public void setPoints(int value) {
		this.value = value;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SkillValue other) {
		if (other==null) return 0;
		if (skill==null) return 0;
		return skill.compareTo(other.getModifyable());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the masterships
	 */
	public List<SkillSpecializationValue> getSkillSpecializations() {
		return specializations;
	}

	//-------------------------------------------------------------------
	public boolean hasSpecialization(SkillSpecialization master) {
		for (SkillSpecializationValue ref : specializations)
			if (ref.getSpecial()==master)
				return true;
		return false;
	}

	//-------------------------------------------------------------------
	public SkillSpecializationValue getSpecialization(SkillSpecialization master) {
		for (SkillSpecializationValue ref : specializations)
			if (ref.getSpecial()==master)
				return ref;
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param specializations the masterships to set
	 */
	public void addSpecialization(SkillSpecializationValue mastership) {
		if (!specializations.contains(mastership))
			specializations.add(mastership);
	}

	//-------------------------------------------------------------------
	public void removeSpecialization(SkillSpecializationValue mastership) {
		for (SkillSpecializationValue ref : specializations)
			if (ref==mastership) {
				specializations.remove(ref);
				return;
			}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.ModifyableValue#getNormalModifier()
	 */
	@Override
	public int getModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getSkill()==skill && !sMod.isConditional() && sMod.getModificationType()!=ModificationValueType.MAX)
					count += sMod.getValue();
			}
		}
		
		// Modifier cannot be more then 4 (Core Page 39)
		return Math.min(count, 4);
	}

	//-------------------------------------------------------------------
	public int getMaximumModifier() {
		int count = 0;
		for (Modification mod : modifications) {
			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getSkill()==skill && !sMod.isConditional() && sMod.getModificationType()==ModificationValueType.MAX)
					count += sMod.getValue();
			}
		}
		return count;
	}

	//-------------------------------------------------------------------
	/**
	 * @param customName the customName to set
	 */
	public void setName(String customName) {
		this.customName = customName.replace("&", "+").replace("<", "").replace(">", "").replace("\"", "'");
	}

	//-------------------------------------------------------------------
	public String getName() {
		if (customName!=null)
			return customName;
		if (skill==null)
			return "Unknown";
		return skill.getName();
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}
	
	//-------------------------------------------------------------------
	public List<BasePluginData> getInfluences() {
		List<BasePluginData> ret = new ArrayList<>();
		for (Modification mod : modifications) {
			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getSkill()==skill && sMod.isConditional() && sMod.getModificationType()==ModificationValueType.NATURAL) {
					if (sMod.getSource()==null) {
						System.err.println("SkillValue.getInfluences: No source for SkillModification "+sMod);
					} else if (! (sMod.getSource() instanceof BasePluginData)) {
						System.err.println("SkillValue.getInfluences: Source of SkillModification "+sMod+" is of type "+sMod.getSource().getClass());
					} else {
						ret.add( (BasePluginData)sMod.getSource());
					}
				}
			} else if (mod instanceof EdgeModification) {
				EdgeModification eMod = (EdgeModification)mod;
				if (eMod.getSource()==null) {
					System.err.println("SkillValue.getInfluences: No source for EdgeModification "+eMod);
				} else if (! (eMod.getSource() instanceof BasePluginData)) {
					System.err.println("SkillValue.getInfluences: Source of EdgeModification "+eMod+" is of type "+eMod.getSource().getClass());
				} else {
					ret.add( (BasePluginData)eMod.getSource());
				}
				
			}
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public List<String> getModifierExplanation() {
		List<String> data = new ArrayList<>();
		int count = 0;
		for (Modification mod : getModifications()) {
			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (!sMod.isConditional() && sMod.getModificationType()!=ModificationValueType.MAX) {
					count += sMod.getValue();
					data.add(ShadowrunTools.getModificationSourceString(sMod.getSource(), sMod.getValue()));
				}
			}
		}
		
		// Modifier cannot be more then 4 (Core Page 39)
		if (count>4) {
			data.add((count-4)+" skill bonus +4 cap (uncapped="+count+")");
			count = Math.min(count, 4);
		}
		return data;
	}

}

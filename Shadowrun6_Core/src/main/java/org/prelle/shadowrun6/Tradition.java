/**
 * 
 */
package org.prelle.shadowrun6;

import java.text.Collator;
import java.util.MissingResourceException;

import javax.naming.directory.ModificationItem;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

/**
 * @author prelle
 *
 */
public class Tradition extends BasePluginData implements Comparable<Tradition> {
	
	@Attribute
	private String id;
	@Attribute(name="drain1")
	private org.prelle.shadowrun6.Attribute drainAttribute1;
	@Attribute(name="drain2")
	private org.prelle.shadowrun6.Attribute drainAttribute2;
	@Element
	private ModificationItem modifications;

	//-------------------------------------------------------------------
	public Tradition() {
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getName()
	 */
	@Override
	public String getName() {
    	if (id==null)
    		return "Tradition(id=null)";
    	String key = "tradition."+id.toLowerCase()+".title";
    	if (i18n==null)
    		return key;
        try {
			return i18n.getString(key);
		} catch (MissingResourceException mre) {
			logger.error("Missing property '"+key+"' in "+i18n.getBaseBundleName());
			if (MISSING!=null) 
				MISSING.println(mre.getKey()+"=");
		}
        return key;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "tradition."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun5.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "tradition."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Tradition o) {
		return Collator.getInstance().compare(this.getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drainAttribute1
	 */
	public org.prelle.shadowrun6.Attribute getDrainAttribute1() {
		return drainAttribute1;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the drainAttribute2
	 */
	public org.prelle.shadowrun6.Attribute getDrainAttribute2() {
		return drainAttribute2;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public ModificationItem getModifications() {
		return modifications;
	}

}

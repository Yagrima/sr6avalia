/**
 * 
 */
package org.prelle.shadowrun6;

import java.util.NoSuchElementException;
import java.util.UUID;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;

/**
 * @author prelle
 *
 */
public class SIN  {
	
	public enum Quality {
		ANYONE(1),
		ROUGH_MATCH(2),
		GOOD_MATCH(3),
		SUPERFICIALLY_PLAUSIBLE(4),
		HIGHLY_PLAUSIBLE(5),
		SECOND_LIFE(6),
		REAL_SIN(-1)
		;
		private int value;
		private Quality(int val) { this.value = val; }
		public int getValue() { return value; }
		
		public String toString() {
			return ShadowrunCore.getI18nResources().getString("sinquality."+name().toLowerCase());
		}
		public static Quality[] getSelectableValues() {
			return new Quality[]{ANYONE, ROUGH_MATCH, GOOD_MATCH, SUPERFICIALLY_PLAUSIBLE, HIGHLY_PLAUSIBLE, SECOND_LIFE};
		}
		public static Quality getByIntValue(int val) {
			for (Quality tmp : Quality.values()) {
				if (tmp.getValue()==val)
					return tmp;
			}
			throw new NoSuchElementException();
		}
	}

	@Attribute(name="uniqueid")
	private UUID uniqueId;
	@Attribute(name="name")
	private String name;
	@Attribute(name="quality")
	private Quality quality;
	@Element
	private String description;

	//-------------------------------------------------------------------
	public SIN() {
		uniqueId = UUID.randomUUID();
		quality = Quality.ANYONE;
	}

	//-------------------------------------------------------------------
	public SIN(Quality qual) {
		this();
		quality = qual;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the uniqueId
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * @param uniqueId the uniqueId to set
	 */
	public void setUniqueId(UUID uniqueId) {
		this.uniqueId = uniqueId;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public Quality getQuality() {
		return quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the quality
	 */
	public int getQualityValue() {
		return quality.getValue();
	}

	//-------------------------------------------------------------------
	/**
	 * @param quality the quality to set
	 */
	public void setQuality(Quality quality) {
		this.quality = quality;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		if (name==null)
			return "?";
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	//-------------------------------------------------------------------
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}

This repository contains multiple artifacts, that have different licensing models.

| Artifact               |  License     |
| ---------------------- |:------------:|
| Shadowrun6_Core        | AGPL-3.0-only OR Unlicensed |
| Shadowrun6_CharGen     | AGPL-3.0-only OR Unlicensed |
| Shadowrun6_CharGen_JFX | AGPL-3.0-only OR Unlicensed |
| Shadowrun6_ProductData | AGPL-3.0-only OR Unlicensed |
| Shadowrun6_JFX         | AGPL-3.0-only OR Unlicensed |
| Shadowrun6_Data        | Unlicensed    |

The reason for this is that *Shadowrun6_Data* contains copyrighted material from "Pegasus Spiele", for which the publisher has granted the RPGFramework usage permissions. Outside this project, the content of *Shadowrun6_Data* may not be used without consent of the copyright owner and the project owner.
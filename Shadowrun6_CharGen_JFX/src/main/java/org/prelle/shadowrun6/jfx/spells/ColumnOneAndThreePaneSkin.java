package org.prelle.shadowrun6.jfx.spells;

import org.prelle.rpgframework.jfx.ThreeColumnPane;

import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ColumnOneAndThreePaneSkin extends SkinBase<ThreeColumnPane> {
	
	private HBox content;
	private VBox column1;
	private VBox column3;
	
	private Label header1;
	private Label header3;

	//-------------------------------------------------------------------
	public ColumnOneAndThreePaneSkin(ThreeColumnPane control) {
		super(control);
		initComponents();
		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		column1 = new VBox();
		column3 = new VBox();
		
		header1 = new Label();
		header3 = new Label();
		
		content = new HBox();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		header1.getStyleClass().addAll("text-subheader","section-head");
		header3.getStyleClass().addAll("text-subheader","section-head");
		
		column1.getStyleClass().add("column1");
		column3.getStyleClass().add("column3");
		
		content.getStyleClass().add("one-three-column-pane");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content.setSpacing(40);
		content.setMaxWidth(Double.MAX_VALUE);
		
		column1.setSpacing(20);
		column3.setSpacing(20);
		
		column1.setMaxHeight(Double.MAX_VALUE);
		column3.setMaxHeight(Double.MAX_VALUE);
		
		content.getChildren().addAll(column1, column3);
		content.setMaxHeight(Double.MAX_VALUE);
		
		HBox.setHgrow(column1, Priority.SOMETIMES);
		HBox.setHgrow(column3, Priority.ALWAYS);
		
		getChildren().add(content);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		header1.textProperty().bind(getSkinnable().column1HeaderProperty());
		header3.textProperty().bind(getSkinnable().column3HeaderProperty());
		
		getSkinnable().headersVisibleProperty().addListener( (ov,o,n) -> {
			if (n) {
				column1.getChildren().add(0, header1);
				column3.getChildren().add(0, header3);
			} else {
				column1.getChildren().remove(header1);
				column3.getChildren().remove(header3);
			}
		});
		
		getSkinnable().column1NodeProperty().addListener( (ov,o,n) -> {
			if (o!=null) { column1.getChildren().remove(o); }
			if (n!=null) { 
				column1.getChildren().add(n);
				VBox.setVgrow(n, Priority.ALWAYS);
			}
		});
		getSkinnable().column3NodeProperty().addListener( (ov,o,n) -> {
			if (o!=null) { column3.getChildren().remove(o); }
			if (n!=null) { 
				column3.getChildren().add(n); 
				VBox.setVgrow(n, Priority.ALWAYS);
			}
		});
	}

}

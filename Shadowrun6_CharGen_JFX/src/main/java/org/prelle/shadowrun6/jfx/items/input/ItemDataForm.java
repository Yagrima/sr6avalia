/**
 * 
 */
package org.prelle.shadowrun6.jfx.items.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.shadowrun6.WriteablePropertyResourceBundle;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.modifications.ItemAttributeModification;

import de.rpgframework.genericrpg.modification.Modification;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ItemDataForm extends HBox {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;


	public interface FormBlock {
		public static String ERROR_CLASS = "invalid-input";
		public Parent getContent();
		public CheckBox getCheckBox();
		public void readInto(ItemTemplate item);
		public void fillFrom(ItemTemplate item);
		public boolean checkInput();
	}

	public enum FormEventType {
		TYPE_CHANGED, 
		APPLY_MODIFICATION,
	}
	
	
	private GridPane colData;
	private VBox colI18n;
	
	private TextField tfID;
	
	private BasicFormBlock  blkBasic;
	private AccessoryFormBlock blkAccessory;
	private RatingFormBlock blkRating;
	private AugmentationFormBlock blkAugment;
	private ModificationsBlock blkModifications;
	private WeaponFormBlock blkWeapon;
	private VehicleFormBlock blkVehicle;
	private List<FormBlock> blocks;
	
	private TextField tfName;
	private TextField tfPage;
	private TextArea  taDesc;
	private TextArea  taWifi;

	//-------------------------------------------------------------------
	public ItemDataForm(ScreenManager manager) {
		initComponents(manager);
		initDataLayout();
		initI18nLayout();
		initInteractivity();
		
		getChildren().addAll(colData, colI18n);
		HBox.setHgrow(colI18n, Priority.ALWAYS);
		HBox.setHgrow(colData, Priority.NEVER);
		setStyle("-fx-spacing: 2em");
		
		blkBasic.initStartData();
	}

	//-------------------------------------------------------------------
	private void initComponents(ScreenManager manager) {
		tfID = new TextField();
		 
		/*
		 * Blocks
		 */
		blkBasic     = new BasicFormBlock(this);
		blkAccessory = new AccessoryFormBlock();
		blkRating    = new RatingFormBlock();
		blkAugment   = new AugmentationFormBlock();
		blkModifications = new ModificationsBlock(this, manager);
		blkWeapon    = new WeaponFormBlock();
		blkVehicle   = new VehicleFormBlock();
		blocks = new ArrayList<ItemDataForm.FormBlock>();
		blocks.add(blkBasic);
		blocks.add(blkRating);
		blocks.add(blkModifications);
		blocks.add(blkWeapon);
		blocks.add(blkAccessory);
		blocks.add(blkAugment);
		blocks.add(blkVehicle);
		
		/*
		 * I18n
		 */
		tfName = new TextField();
		tfPage = new TextField();
		taDesc = new TextArea();
		taWifi = new TextArea();
	}

	//-------------------------------------------------------------------
	private void initDataLayout() {
		Label heaID      = new Label(UI.getString("label.id"));
		
		HBox lineID = new HBox();
		lineID.getChildren().addAll(heaID, tfID);
		lineID.setStyle("-fx-spacing: 0.3em");
		
		colData = new GridPane();
//		colData.setGridLinesVisible(true);
		colData.setStyle("-fx-hgap: 0.3em; -fx-vgap: 0.4em;");
		colData.add(lineID    , 1, 0);
		
		colData.add(    blkBasic.getContent(), 1, 1);
		colData.add(blkAccessory.getContent(), 2, 1);
		
		colData.add(blkRating.getCheckBox()  , 0, 2);
		colData.add(blkRating.getContent()   , 1, 2);
		colData.add(blkAugment.getContent()  , 2, 2);
		
		colData.add(blkModifications.getContent()   , 1, 3);
		colData.add(blkWeapon.getCheckBox()  , 0, 4);
		colData.add(blkWeapon.getContent()   , 1, 4);
		colData.add(blkVehicle.getContent()  , 1, 5, 2,1);
		
//		int y=0;
		for (FormBlock block : blocks) {
//			y++;
			if (block.getCheckBox()!=null) {
//				colData.add(block.getCheckBox()  , 0, y);
				block.getCheckBox().setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
				block.getCheckBox().getStyleClass().add("content");
				GridPane.setFillHeight(block.getCheckBox(), true);
			}
			if (block.getContent()!=null) {
//				colData.add(block.getContent(), 1, y);
				block.getContent().getStyleClass().add("content");
				if (block==blkVehicle)
					block.getContent().setStyle("-fx-max-width: 56em");
				else if (block==blkAccessory)
					block.getContent().setStyle("-fx-max-width: 25em");
				else if (block==blkAugment)
					block.getContent().setStyle("-fx-max-width: 25em");
				else
					block.getContent().setStyle("-fx-max-width: 35em");
			}
		}
		
		ColumnConstraints col1 = new ColumnConstraints();
		ColumnConstraints col2 = new ColumnConstraints();
		ColumnConstraints col3 = new ColumnConstraints();
		col3.setPrefWidth(300);
		col3.setMaxWidth(300);
		colData.getColumnConstraints().addAll(col1, col2, col3);
	}

	//-------------------------------------------------------------------
	private void initI18nLayout() {
		Label heaName    = new Label(UI.getString("label.name"));
		Label heaPage    = new Label(UI.getString("label.page"));
		Label heaDesc    = new Label(UI.getString("label.description"));
		Label heaWifi    = new Label(UI.getString("label.wifiadvantage"));
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-hgap: 0.3em; -fx-vgap: 0.4em;");
		grid.add(heaName  , 0, 0);
		grid.add(tfName   , 1, 0);
		grid.add(heaPage  , 0, 1);
		grid.add(tfPage   , 1, 1);
		grid.add(heaDesc  , 0, 2, 2,1);
		grid.add( taDesc  , 0, 3, 2,1);
		grid.add(heaWifi  , 0, 4, 2,1);
		grid.add( taWifi  , 0, 5, 2,1);
		
		colI18n = new VBox();
		colI18n.getChildren().addAll(grid);
//		colI18n.setStyle("-fx-pref-width: 30em");
	}

	//-------------------------------------------------------------------
	void handleFormBlockEvent(FormEventType event, Object data) {
		logger.debug("handleFormBlockEvent "+event+" "+data);
		
		switch (event) {
		case TYPE_CHANGED:
			logger.debug("RCV "+event+" "+data);
			if (blkAccessory!=null)
				blkAccessory.getContent().setVisible(data==ItemType.ACCESSORY);
			if (blkWeapon!=null)
				blkWeapon.getCheckBox().setSelected(ItemType.WEAPON==data);
			if (blkAugment!=null)
				blkAugment.getContent().setVisible(Arrays.asList(ItemType.bodytechTypes()).contains(data));
			if (blkVehicle!=null)
				blkVehicle.getContent().setVisible(data==ItemType.VEHICLES || data==ItemType.DRONES);
			
			blkModifications.clear();
			switch ((ItemType)data) {
			case VEHICLES:
				blkModifications.addHook(ItemHook.VEHICLE_DRIVE);
				blkModifications.addHook(ItemHook.VEHICLE_PROTECTION);
				blkModifications.addHook(ItemHook.VEHICLE_WEAPON);
				blkModifications.addHook(ItemHook.VEHICLE_BODY);
				blkModifications.addHook(ItemHook.VEHICLE_ELECTRONICS);
				blkModifications.addHook(ItemHook.VEHICLE_COSMETICS);
				break;
			}
			break;
		case APPLY_MODIFICATION:
			apply((Modification) data);
			break;
		}
	}
	
	//-------------------------------------------------------------------
	private void apply(Modification mod) {
		if (mod instanceof ItemAttributeModification) {
			ItemAttributeModification aMod = (ItemAttributeModification)mod;
			switch (aMod.getAttribute()) {
			case ATTACK_RATING:
			case AMMUNITION:
			case DAMAGE:
			case MODE:
			case SKILL:
				blkWeapon.addModification(aMod);
				break;
			default:
				logger.warn("Dont know ho to apply item attribute "+aMod.getAttribute());
			}
		} else
			logger.warn("TODO: apply "+mod);
	}
	
	//-------------------------------------------------------------------
	private static String asIdentifier(String value) {
		boolean markNextUpperCase = false;
		StringBuffer buf = new StringBuffer();
		for (int i=0; i<value.length(); i++) {
			char c = value.charAt(i);
			if (Character.isWhitespace(c)) {
				markNextUpperCase = true;
				continue;
			} else {
				if (markNextUpperCase)
					buf.append(Character.toUpperCase(c));
				else
					buf.append(Character.toLowerCase(c));
				markNextUpperCase = false;
			}
			
		}
		return buf.toString();
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfName.textProperty().addListener( (ov,o,n) -> {
			if (n!=null && n.length()>0) {
				try {
					if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); tfName.setText(n); }
					if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); tfName.setText(n); }
					if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); tfName.setText(n); }
					if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); tfName.setText(n); }
				} catch (Exception e) {
					logger.error("Failed setting name",e);
				}
				
				StringBuffer id = new StringBuffer();
				for (int i=0; i<tfName.getText().length(); i++) {
					char c = tfName.getText().charAt(i);
					switch (c) {
					case 'ü': id.append("ue"); break;
					case 'Ü': id.append("Ue"); break;
					case 'ä': id.append("ae"); break;
					case 'Ä': id.append("Ae"); break;
					case 'ö': id.append("oe"); break;
					case 'Ö': id.append("Oe"); break;
					case 'ß': id.append("ss"); break;
					default:
						id.append(c);
					}
				}
				tfID.setPromptText(asIdentifier(id.toString()));
			}
		});

		
	}
	
	//-------------------------------------------------------------------
	public void preselectTypes(ItemType type, ItemSubType sub) {
		blkBasic.preselectTypes(type, sub);
	}

	//-------------------------------------------------------------------
	public void readInto(ItemTemplate item) {
		logger.debug("call readInto");
		if (tfID.getText()!=null && tfID.getText().length()>0)
			item.setId(tfID.getText());
		else
			item.setId(asIdentifier(tfName.getText()));
		
		((WriteablePropertyResourceBundle)item.getResourceBundle()).removePrefix("item."+item.getId());
		((WriteablePropertyResourceBundle)item.getResourceBundle()).set("item."+item.getId(), tfName.getText());
		((WriteablePropertyResourceBundle)item.getResourceBundle()).set(item.getPageI18NKey(), tfPage.getText());
		if (taWifi.getText()!=null && !taWifi.getText().isEmpty())
			((WriteablePropertyResourceBundle)item.getResourceBundle()).set("item."+item.getId()+".wifi", taWifi.getText());
		
		if (taDesc.getText()!=null && !taDesc.getText().isEmpty())
			((WriteablePropertyResourceBundle)item.getHelpResourceBundle()).set(item.getHelpI18NKey(), taDesc.getText());
		
		for (FormBlock tmp : blocks) {
			if (!tmp.getContent().isVisible())
				continue;
			try {
				tmp.readInto(item);
			} catch (Exception e) {
				logger.error("Failed reading from "+tmp,e);
			}
		}
	}

	//-------------------------------------------------------------------
	public void fillFrom(ItemTemplate item) {
		tfID.setText(item.getId());
		tfName.setText(item.getName());
		tfPage.setText(String.valueOf(item.getPage()));
		taDesc.setText(item.getHelpText());
		taWifi.setText(String.join("\n", item.getWiFiAdvantageStrings()));
		
		blkWeapon.getCheckBox().setSelected(item.getWeaponData()!=null);
		blkRating.getCheckBox().setSelected(item.hasRating());
		
		for (FormBlock tmp : blocks)
			tmp.fillFrom(item);
	}

	//-------------------------------------------------------------------
	public boolean isInputOkay() {
		boolean isOkay = true;
		
		// Name
		if (tfName.getText().isEmpty()) {
			if (!tfName.getStyleClass().contains(BasicFormBlock.ERROR_CLASS))
				tfName.getStyleClass().add(BasicFormBlock.ERROR_CLASS);
			isOkay=false;
		} else
			tfName.getStyleClass().remove(BasicFormBlock.ERROR_CLASS);
		
		// page
		if (tfPage.getText().isEmpty()) {
			if (!tfPage.getStyleClass().contains(BasicFormBlock.ERROR_CLASS))
				tfPage.getStyleClass().add(BasicFormBlock.ERROR_CLASS);
			isOkay=false;
		} else {
			try {
				Integer.parseInt(tfPage.getText());
				tfPage.getStyleClass().remove(BasicFormBlock.ERROR_CLASS);
			} catch (NumberFormatException e) {
				if (!tfPage.getStyleClass().contains(BasicFormBlock.ERROR_CLASS))
					tfPage.getStyleClass().add(BasicFormBlock.ERROR_CLASS);
				isOkay=false;
			}
		}
		
		
		for (FormBlock tmp : blocks) {
			if (!tmp.getContent().isVisible())
				continue;
			if (!tmp.checkInput()) {
				isOkay = false;
				logger.warn("Input within "+tmp.getClass().getSimpleName()+" is not okay");
			}
		}
		logger.debug("isOkay = "+isOkay);		
		return isOkay;
	}

}

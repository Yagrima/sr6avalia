/**
 * 
 */
package org.prelle.shadowrun6.jfx.items;

import java.util.Arrays;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.SelectionModel;

/**
 * @author prelle
 *
 */
public class ItemTemplateSelector extends Control implements IListSelector<ItemTemplate>{
	
	private ItemType[] allowedTypes;
	private ItemSubType[] allowedSubTypes;
	private ObjectProperty<ItemTemplate> selected;

	//-------------------------------------------------------------------
	public ItemTemplateSelector(ItemType[] allowed, CharacterController control) {
		allowedTypes = allowed;
		selected = new SimpleObjectProperty<ItemTemplate>();
		
		ItemTemplateSelectorSkin skin = new ItemTemplateSelectorSkin(this, control);
		setSkin(skin);
	}

	//-------------------------------------------------------------------
	public ItemTemplateSelector(ItemType[] allowed, ItemSubType[] subAllowed, CharacterController control) {
		allowedTypes = allowed;
		allowedSubTypes = subAllowed;
		selected = new SimpleObjectProperty<ItemTemplate>();
		
		ItemTemplateSelectorSkin skin = new ItemTemplateSelectorSkin(this, control);
		setSkin(skin);
	}

	//-------------------------------------------------------------------
	public ItemType[] getAllowedTypes() {
		return allowedTypes;
	}

	//-------------------------------------------------------------------
	public ItemSubType[] getAllowedSubTypes() {
		return allowedSubTypes;
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<ItemTemplate> selectedItemProperty() {
		return selected;
	}

	//-------------------------------------------------------------------
	public ItemTemplate getSelectedItem() {
		return selected.get();
	}

	//-------------------------------------------------------------------
	void setSelectedItem(ItemTemplate data) {
		selected.set(data);
	}

	//-------------------------------------------------------------------
	ObservableList<Node> impl_getChildren() {
		return super.getChildren();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<ItemTemplate> getSelectionModel() {
		return ((ItemTemplateSelectorSkin)getSkin()).getSelectionModel();
	}
	
}

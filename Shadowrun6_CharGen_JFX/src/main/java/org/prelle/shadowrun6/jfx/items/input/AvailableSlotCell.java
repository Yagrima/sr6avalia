package org.prelle.shadowrun6.jfx.items.input;

import java.util.Iterator;

import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class AvailableSlotCell extends ListCell<AvailableSlot> {
	
	private CheckBox cbUsed;
	private Label lblSlotName;
	private Label lblEmbedded;
	private Button btnSelect;
	private HBox  layout;
	
	//-------------------------------------------------------------------
	public AvailableSlotCell() {
		initComponents();
		initLayout();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbUsed = new CheckBox();
		lblSlotName = new Label();
		lblEmbedded = new Label();
		btnSelect   = new Button("\uE0D8");
		
		lblSlotName.getStyleClass().add("text-subheader");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		lblEmbedded.setMaxWidth(Double.MAX_VALUE);
		HBox line = new HBox(lblEmbedded, btnSelect);
		line.setStyle("-fx-spacing: 1em");
		HBox.setHgrow(lblEmbedded, Priority.ALWAYS);
		
		VBox box = new VBox(lblSlotName, line);
		box.setStyle("-fx-spacing: 0.5em");
		
		layout = new HBox(cbUsed, box);
		box.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(box, Priority.ALWAYS);
		
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(AvailableSlot item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setGraphic(null);
		} else {
			lblSlotName.setText(item.getSlot().getName());
			if (item.getAllEmbeddedItems().isEmpty()) {
				lblEmbedded.setText("-");
			} else {
				StringBuffer buf = new StringBuffer();
				Iterator<CarriedItem> it = item.getAllEmbeddedItems().iterator();
				while (it.hasNext()) {
					buf.append(it.next().getName());
					if (it.hasNext())
						buf.append("\n");
				}
				lblEmbedded.setText(buf.toString());
			}
			setGraphic(layout);
		}
	}
	
}
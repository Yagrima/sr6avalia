package org.prelle.shadowrun6.jfx.fluent;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.AttentionMenuItem;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.PathIcon;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.WindowMode;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.Controller;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6AugmentationPage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6DevelopmentPage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6EquipmentPage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6MagicPage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6MatrixPage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6OverviewPage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6ResonancePage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6SINLifestylePage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6SkillPage;
import org.prelle.shadowrun6.chargen.jfx.pages.SR6VehiclePage;
import org.prelle.shadowrun6.chargen.jfx.wizard.CharGenWizardNG;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.ResourceI18N;
import de.rpgframework.character.Attachment;
import de.rpgframework.character.CharacterHandle;
import de.rpgframework.character.CharacterHandle.Format;
import de.rpgframework.character.CharacterHandle.Type;
import de.rpgframework.character.CharacterProvider;
import de.rpgframework.character.CharacterProviderLoader;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.ToDoElement;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class CharacterViewScreenSR6Fluent extends ManagedScreen implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(CharacterViewScreenSR6Fluent.class.getName());

	private final static String CSS_COMMON = "css/shadowrun-common.css";
	private final static String CSS = "css/shadowrun-light.css";

	private ShadowrunCharacter model;
	private CharacterHandle handle;
	private CharacterController control;
	private ViewMode mode;
//	private CharGenWizardNG wizard;

	private SR6OverviewPage  pgOverview;
	private SR6SkillPage     pgSkills;
	private SR6MagicPage     pgMagic;
	private SR6ResonancePage pgResonance;
	private SR6MatrixPage    pgMatrix;
	private SR6AugmentationPage pgAugment;
	private SR6EquipmentPage  pgEquipment;
	private SR6VehiclePage   pgVehicles;
	private SR6SINLifestylePage pgLiving;
	private SR6DevelopmentPage  pgDevelop;

	private AttentionMenuItem navOverview;
	private AttentionMenuItem navSkill;
	private AttentionMenuItem navMagic;
	private AttentionMenuItem navResonance;
	private AttentionMenuItem navAugment;
	private AttentionMenuItem navEquipment;
	private MenuItem navMatrix;
	private AttentionMenuItem navVehicles;
	private AttentionMenuItem navLiving;
	private MenuItem navExperience;

	//-------------------------------------------------------------------
	public CharacterViewScreenSR6Fluent(CharacterController control, ViewMode mode, CharacterHandle handle) {
		this.setId("genesis/shadowrun");
		this.control = control;
		this.handle  = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		model = control.getCharacter();

		initComponents();
		initLayout();
		initNavigation();
		initInteractivity();

		refresh();
		GenerationEventDispatcher.addListener(this);
		logger.info("Created screen for "+handle+" and controller "+control);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#getStyleSheets()
	 */
	@Override
	public String[] getStyleSheets() {
		return new String[] {
				SR6Constants.class.getResource(CSS_COMMON).toExternalForm(), 
				SR6Constants.class.getResource(CSS).toExternalForm()};
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		pgOverview  = new SR6OverviewPage    (control, mode, handle, this);
		pgSkills    = new SR6SkillPage       (control, mode, handle, this);
		pgMagic     = new SR6MagicPage       (control, mode, handle, this);
		pgResonance = new SR6ResonancePage   (control, mode, handle, this);
		pgMatrix    = new SR6MatrixPage      (control, mode, handle, this);
		pgAugment   = new SR6AugmentationPage(control, mode, handle, this);
		pgEquipment = new SR6EquipmentPage   (control, mode, handle, this);
		pgVehicles  = new SR6VehiclePage     (control, mode, handle, this);
		pgLiving    = new SR6SINLifestylePage(control, mode, handle, this);
		pgDevelop   = new SR6DevelopmentPage (control, handle, this);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.setLandingPage(pgOverview);
	}

	//-------------------------------------------------------------------
	private void initNavigation() {
		SymbolIcon overviewIcon = new SymbolIcon("RedEye");
		String boltIcon = "M485.846 30l-172.967 74.424 64.283 20.32-129.627 65.186 83.637 19.414-96.996 62.219 219.133-69.7-95.29-28.326L471.192 112.8l-72.115-15.024L485.846 30zm-280.46 45.766c-28.066-.117-49.926 56.532-57.726 90.607-11.26 49.19-14.529 83.515-.828 133.059l-17.348 4.798c-15.463-55.917-8.245-94.75 2.301-142.341 10.547-47.592 14.52-70.403-4.459-74.182C85.244 79.328 82.04 178.17 79.57 222.604c-1.396 25.808.71 57.017 6.54 77.552l-16.901 6.196c-14.43-53.35-6.657-97.957-1.693-150.77 2.493-15.582-1.787-25.677-19.102-25.166-15.833.467-27.015 143.362-13.275 179.041 8.713 53.061 31.247 130.572 10.955 152.766L18 494h205.973l19.986-28.592c23.08-5.008 28.42-19.86 37.023-33.787 25.291-40.946 82.384-83.166 129.114-99.226 21.142-7.51-21.912-48.546-53.836-32.782-55.005 27.162-81.646 56.298-117.772 38.295-55.855-27.834-47.245-100.648-35.861-162.83 6.141-33.544 40.41-89.602 7.156-98.824a21.158 21.158 0 0 0-4.396-.488z";
		PathIcon icon = new PathIcon(boltIcon);
		double scale = 0.075;
		icon.setScaleX(scale);
		icon.setScaleY(scale);
		Group grp = new Group(icon);
		grp.setMouseTransparent(true);
//		overviewIcon.widthProperty().addListener( (ov,o,n) -> {
//			System.err.println("Reference icon width = "+n+"   "+icon.getWidth()+"  pref="+icon.getPrefWidth());
//			System.err.println("  new scale would be "+( (double)n)/ icon.getWidth());
//		});
		
		navOverview   = new AttentionMenuItem(Resource.get(UI,"navItem.overview"), overviewIcon);
		navSkill      = new AttentionMenuItem(Resource.get(UI,"navItem.skills"), new SymbolIcon("Education"));
		navMagic      = new AttentionMenuItem(Resource.get(UI,"navItem.magic"), new SymbolIcon("Mage"));
		navMatrix     = new          MenuItem(Resource.get(UI,"navItem.matrix"), new SymbolIcon("TiltDown"));
		navAugment = new AttentionMenuItem(Resource.get(UI,"navItem.augmentation"), new SymbolIcon("Contact"));
		navEquipment  = new AttentionMenuItem(Resource.get(UI,"navItem.equipment"), new FontIcon("\uD83D\uDD2B"));
		navResonance  = new AttentionMenuItem(Resource.get(UI,"navItem.resonance"), new SymbolIcon("Robot"));
		navVehicles   = new AttentionMenuItem(Resource.get(UI,"navItem.vehicles"), new SymbolIcon("Car"));
		navLiving     = new AttentionMenuItem(Resource.get(UI,"navItem.living"), new SymbolIcon("Home"));
		navExperience = new MenuItem(Resource.get(UI,"navItem.experience"), new FontIcon("\uD83D\uDCC8"));

		this.getNavigationItems().addAll(navOverview, navSkill, navMagic, navResonance, navAugment, navEquipment, navMatrix, navVehicles, navLiving, navExperience);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		setCanBeLeftCallback( screen -> userTriesToLeave());
	}

	//-------------------------------------------------------------------
	private boolean saveCharacter() {
		logger.debug("START: saveCharacter");

		if (mode==ViewMode.MODIFICATION) {
			/*
			 * Write all made modifications to character
			 */
			logger.debug("Add modifications to character log");
			((CharacterLeveller)control).updateHistory();
		}

		try {
			/*
			 * 1. Convert character into Byte Buffer - or fail
			 */
			byte[] encoded = null;
			try { encoded = ShadowrunCore.save(model); } catch (IOException e) {
				logger.error("Cannot save character, since encoding failed: "+e);
				StringWriter out = new StringWriter();
				e.printStackTrace(new PrintWriter(out));
				getManager().showAlertAndCall(
						AlertType.ERROR,
						ResourceI18N.get(UI,"error.encoding.title"),
						ResourceI18N.format(UI,"error.encoding.content", System.getProperty("logdir")+"\\genesis-logs.txt")+"\n"+out
						);
				return false;
			}

			/*
			 * 2. Use character service to save character
			 */
			try {
				if (handle==null) {
					logger.debug("CharacterHandle does not exist yet - prepare it");
					handle = CharacterProviderLoader.getCharacterProvider().createCharacter(model.getName(), RoleplayingSystem.SHADOWRUN6);
				}
				logger.info("Save character "+model.getName());
				CharacterProviderLoader.getCharacterProvider().addAttachment(handle, Type.CHARACTER, Format.RULESPECIFIC, null, encoded);
				logger.info("Saved character "+model.getName()+" successfully");
			} catch (IOException e) {
				logger.error("Failed saving character",e);
				StringWriter out = new StringWriter();
				e.printStackTrace(new PrintWriter(out));
				getManager().showAlertAndCall(
						AlertType.ERROR,
						ResourceI18N.get(UI,"error.saving_character.title"),
						ResourceI18N.format(UI,"error.saving_character.message", System.getProperty("logdir")+"\\genesis-logs.txt")+"\n"+out
						);
				return false;
			}

			// 3. Eventually rename
			try {
				if (handle!=null && !handle.getName().equals(model.getName())) {
					logger.info("Character has been renamed");
					CharacterProviderLoader.getCharacterProvider().renameCharacter(handle, model.getName());
				}
			} catch (IOException e) {
				logger.error("Renaming failed",e);
				BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE, 2, "Renaming failed: "+e);
			}

			/*
			 * 4. Update portrait
			 */
			logger.debug("Update portrait");
			CharacterProvider charServ = CharacterProviderLoader.getCharacterProvider();
			try {
				if (model.getImage()!=null && handle!=null) {
					Attachment attach = handle.getFirstAttachment(Type.CHARACTER, Format.IMAGE);
					if (attach!=null) {
						logger.info("Update character image");
						attach.setData(model.getImage());
						charServ.modifyAttachment(handle, attach);
					} else {
						charServ.addAttachment(handle, Type.CHARACTER, Format.IMAGE, null, model.getImage());
					}
				} else if (handle!=null) {
					Attachment attach = handle.getFirstAttachment(Type.CHARACTER, Format.IMAGE);
					if (attach!=null) {
						logger.info("Delete old character image");
						charServ.removeAttachment(handle, attach);
					}
				}
			} catch (IOException e) {
				logger.error("Failed modifying portrait attachment",e);
			}
		} finally {
			logger.debug("STOP : saveCharacter");
		}
		return true;
	}

	//-------------------------------------------------------------------
	private static VBox getToDoBox(String mess, List<ToDoElement> todos) {
		VBox bxToDos = new VBox(10);
		Label lbMess = new Label(mess);
		lbMess.setWrapText(true);
		bxToDos.getChildren().add(lbMess);
		VBox.setMargin(bxToDos, new Insets(0, 0, 20, 0));
		for (ToDoElement todo : todos) {
			Label label = new Label(todo.getMessage());
			label.setWrapText(true);
			label.setStyle("-fx-text-fill: textcolor-"+todo.getSeverity().name().toLowerCase());
			bxToDos.getChildren().add(label);
		}
		return bxToDos;
	}

	//-------------------------------------------------------------------
	private boolean userTriesToLeave() {
		logger.info("userTriesToLeave");
		
		if (mode==ViewMode.GENERATION) {
			logger.warn("TODO: Check if creation is finished");
			if ( ((CharacterGenerator)control).hasEnoughData() ) {
				// Generator has no more stoppers, but may have warnings
				logger.debug("warnings = "+ ((CharacterGenerator)control).getToDos());
				if ( ((CharacterGenerator)control).getToDos().size()>0 ) {
					// There are warnings
					CloseType result = getManager().showAlertAndCall(
							AlertType.CONFIRMATION,
							Resource.get(UI, "alert.finish_creation_with_warning.title"),
							getToDoBox(Resource.get(UI, "alert.finish_creation_with_warning.mess"), ((CharacterGenerator)control).getToDos())
							);
					if (result!=CloseType.YES)
						return false;
				} 
				
				logger.info("User wants to leave and generator is finished - try to save character");
				((CharacterGenerator)control).stop();
				return saveCharacter();
			} else {
				logger.info("User wants to leave the generation early.");
				CloseType result = getManager().showAlertAndCall(
						AlertType.CONFIRMATION,
						UI.getString("alert.cancel_creation.title"),
						UI.getString("alert.cancel_creation.message")
						);
				return result==CloseType.YES;
			}
		} else {
			logger.info("User wants to leave character modifiction");
			logger.debug("warnings = "+ ((CharacterLeveller)control).getToDos());
			CloseType result = getManager().showAlertAndCall(
					AlertType.CONFIRMATION,
					UI.getString("alert.save_character.title"),
					getToDoBox(Resource.get(UI, "alert.save_character.message"), ((CharacterLeveller)control).getToDos())
					);
			if (result==CloseType.YES) {
				logger.debug("User confirmed saving character");
				saveCharacter();
			} else if (result==CloseType.CANCEL) {
				logger.debug("User cancelled leaving");
				return false;
			} else {
				logger.debug("User denied saving character - reload it");
				try {
					if (handle!=null) {
						handle.setCharacter(null);
						handle.getCharacter();
					}
				} catch (IOException e) {
					logger.error("Failed reloading character",e);
					getManager().showAlertAndCall(AlertType.ERROR, "", UI.getString("alert.reloading.char"));
				}
			}
		}
		
		GenerationEventDispatcher.clear();
		return true;
	}

	//-------------------------------------------------------------------
	public void continueGeneration() {
		logger.info("continueGeneration for "+model.getName()+" with controller "+control+" and its model "+control.getCharacter().getName());

		CharGenWizardNG wizard = new CharGenWizardNG(model, false);
		wizard.getStylesheets().addAll(
				SR6Constants.class.getResource(CSS_COMMON).toExternalForm(),
				SR6Constants.class.getResource(CSS).toExternalForm()
				);
//		wizard.setCurrentPage(wizard.getPages().get(1));
		logger.debug("CharGenWizardNG.style= "+wizard.getStylesheets());
		CloseType close = (CloseType)getManager().showAndWait(wizard);
		logger.info("Closed with "+close);

		if (close==CloseType.FINISH) {
			logger.info("Wizard finished");
			refresh();
		} else {
			logger.info("Wizard left with "+close);
			GenerationEventDispatcher.clear();
			if (handle!=null) {
				try {
					handle.setCharacter(null);
				} catch (IOException e) {
					logger.error("Error resetting character",e);
				}
				handle.getCharacter();
			}
			getScreenManager().closeScreen();
		}

	}

	//-------------------------------------------------------------------
	public void startGeneration() {
		logger.info("startGeneration for "+model.getName()+" and controller has "+control.getCharacter().getName());
		if (model!=control.getCharacter())
			throw new IllegalStateException("Displayed model does not match controllers model");

		CharGenWizardNG wizard = new CharGenWizardNG(model, true);
		wizard.getStylesheets().addAll(
				SR6Constants.class.getResource(CSS_COMMON).toExternalForm(),
				SR6Constants.class.getResource(CSS).toExternalForm()
				);
		logger.debug("CharGenWizardNG.style= "+wizard.getStylesheets());
		CloseType close = (CloseType)getManager().showAndWait(wizard);
		logger.info("Closed with "+close);

		if (close==CloseType.FINISH) {
			logger.info("Wizard finished");
			refresh();
		} else {
			logger.info("Wizard left with "+close);
			GenerationEventDispatcher.clear();
			getScreenManager().closeScreen();
		}

	}

	//-------------------------------------------------------------------
	public void reopenWizard() {
		logger.info("reopenWizard");
		CharGenWizardNG wizard = new CharGenWizardNG(model, false);
		wizard.getStylesheets().addAll(
				SR6Constants.class.getResource(CSS_COMMON).toExternalForm(),
				SR6Constants.class.getResource(CSS).toExternalForm()
				);
		wizard.setCurrentPage(wizard.getPages().get(1));

		CloseType close = (CloseType)getManager().showAndWait(wizard);
		logger.info("Reopened wizard closed with "+close);
		if (close==CloseType.FINISH) {
			logger.info("Wizard finished");
			refresh();
		} else {
			logger.info("Wizard left with "+close);
			getScreenManager().closeScreen();
		}
	}

	//-------------------------------------------------------------------
	public void saveInCreationMode() {
		logger.info("saveInCreationMode");
		CloseType result = getManager().showAlertAndCall(
				AlertType.CONFIRMATION,
				Resource.get(UI, "alert.save_creation.title"),
				Resource.get(UI, "alert.save_creation.message")
				);
		if (result==CloseType.YES) {
			try {
				((CharacterGenerator)control).saveCreation();
				GenerationEventDispatcher.clear();
				getScreenManager().closeScreen();
			} catch (IOException e) {
				logger.error("Failed saving character",e);
				getManager().showAlertAndCall(AlertType.ERROR, "", Resource.format(UI, "error.save_creation", e.toString()));
			}
		}
	}

	//--------------------------------------------------------------------
	private static List<String> getAsTooltip(List<ToDoElement> todos) {
		List<String> ret = new ArrayList<>();
		for (ToDoElement tmp : todos) {
			ret.add(tmp.getMessage());
		}
		return ret;
	}

	//--------------------------------------------------------------------
	private static List<ToDoElement> combineToDos(Controller...ctrl) {
		List<ToDoElement> ret = new ArrayList<>();
		for (Controller tmp : ctrl) {
			ret.addAll(tmp.getToDos());
		}
		return ret;
	}

	//--------------------------------------------------------------------
	private void updateAttentionFlags() {
		logger.debug("updateAttentionFlags");
		List<ToDoElement> list = combineToDos(control.getAttributeController(), control.getQualityController());
		navOverview.setAttentionFlag(!list.isEmpty());
		navOverview.setAttentionToolTip(getAsTooltip(list));
		
		list = combineToDos(control.getSpellController(), control.getRitualController(), control.getPowerController());
		navMagic.setAttentionFlag(!list.isEmpty());
		navMagic.setAttentionToolTip(getAsTooltip(list));

		navResonance.setAttentionFlag(!control.getComplexFormController().getToDos().isEmpty());
		navResonance.setAttentionToolTip(getAsTooltip(control.getComplexFormController().getToDos()));

		navSkill.setAttentionFlag(!control.getSkillController().getToDos().isEmpty());
		navSkill.setAttentionToolTip(getAsTooltip(control.getSkillController().getToDos()));
		
		navEquipment.setAttentionFlag(!control.getEquipmentController().getToDos().isEmpty());
		navEquipment.setAttentionToolTip(getAsTooltip(control.getEquipmentController().getToDos()));

		list = combineToDos(control.getLifestyleController(), control.getSINController());
		navLiving.setAttentionFlag(!list.isEmpty());
		navLiving.setAttentionToolTip(getAsTooltip(list));

	}

	//--------------------------------------------------------------------
	private void refresh() {
		logger.debug("refresh");

		pgOverview.refresh();
		pgSkills.refresh();
		pgMagic.refresh();
		pgResonance.refresh();
		pgMatrix.refresh();
		pgAugment.refresh();
		pgEquipment.refresh();
		pgVehicles.refresh();
		pgLiving.refresh();
		pgDevelop.refresh();
		
		navMagic.setDisable(model.getMagicOrResonanceType()==null || !model.getMagicOrResonanceType().usesMagic());
		navMagic.setVisible(!(model.getMagicOrResonanceType()==null || !model.getMagicOrResonanceType().usesMagic()));
		navResonance.setDisable(model.getMagicOrResonanceType()==null || !model.getMagicOrResonanceType().usesResonance());
		navResonance.setVisible(!(model.getMagicOrResonanceType()==null || !model.getMagicOrResonanceType().usesResonance()));
		
		updateAttentionFlags();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedScreen#navigationItemChanged(javafx.scene.control.MenuItem, javafx.scene.control.MenuItem)
	 */
	@Override
	public void navigationItemChanged(MenuItem oldValue, MenuItem newValue) {
		logger.info("Navigation changed to "+newValue);
		
		if (newValue==navOverview) {
			setContent(pgOverview);
			setHeader(pgOverview.getTitle());
		} else if (newValue==navSkill) {
			setContent(pgSkills);
			setHeader(pgSkills.getTitle());
		} else if (newValue==navMagic) {
			setContent(pgMagic);
			setHeader(pgMagic.getTitle());
		} else if (newValue==navResonance) {
			setContent(pgResonance);
			setHeader(pgResonance.getTitle());
		} else if (newValue==navMatrix) {
			setContent(pgMatrix);
			setHeader(pgMatrix.getTitle());
		} else if (newValue==navAugment) {
			setContent(pgAugment);
			setHeader(pgAugment.getTitle());
		} else if (newValue==navEquipment) {
			setContent(pgEquipment);
			setHeader(pgEquipment.getTitle());
		} else if (newValue==navVehicles) {
			setContent(pgVehicles);
			setHeader(pgVehicles.getTitle());
		} else if (newValue==navLiving) {
			setContent(pgLiving);
			setHeader(pgLiving.getTitle());
		} else if (newValue==navExperience) {
			setContent(pgDevelop);
			setHeader(pgDevelop.getTitle());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ResponsiveControl#setResponsiveMode(org.prelle.javafx.WindowMode)
	 */
	@Override
	public void setResponsiveMode(WindowMode value) {
		logger.trace("......"+value);
		pgOverview.setResponsiveMode(value);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		logger.warn("RCV "+event.getType());
		switch (event.getType()) {
		case FINISH_REQUESTED:
			logger.info("FINISH_REQUESTED");
			if (userTriesToLeave()) {
				logger.info("Saved successfully");
				GenerationEventDispatcher.clear();
				getScreenManager().closeScreen();
			}
			break;
		case CHARACTER_CHANGED:
		default:
			refresh();
			break;
		}
	}

}

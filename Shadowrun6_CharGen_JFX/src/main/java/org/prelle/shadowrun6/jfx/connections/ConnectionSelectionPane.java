/**
 *
 */
package org.prelle.shadowrun6.jfx.connections;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.ConnectionsController;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class ConnectionSelectionPane extends HBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".conn");

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private ConnectionsController ctrl;
//	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private Label hdSelected;
	private Button btnAdd;
	private Label hdDescript;
	private ListView<Connection> lvSelected;
	private VBox colDescription;
	private TextField tfName;
	private TextField tfType;
	private TextArea  taDesc;

	private FontIcon imgDelete;
	private Connection selectedItem;

	//--------------------------------------------------------------------
	public ConnectionSelectionPane(ConnectionsController ctrl, ScreenManagerProvider provider) {
		this.ctrl = ctrl;
//		this.provider = provider;
		initCompoments();
		initLayout();
		colDescription.setVisible(false);
		initInteractivity();

		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		btnAdd = new Button("\uE0C8");
		btnAdd.setStyle("-fx-border-width: 0; -fx-font-family: \"Segoe UI Symbol\"");
		hdSelected  = new Label(UI.getString("connselectpane.selected"));
		hdDescript  = new Label(UI.getString("connselectpane.description"));
		hdSelected.getStyleClass().add("text-subheader");
		hdDescript.getStyleClass().add("text-subheader");
		//		hdAvailable.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdSelected.setStyle("-fx-text-fill: textcolor-highlight-primary");
		//		hdDescript.setStyle("-fx-text-fill: textcolor-highlight-primary");
		hdSelected.getStyleClass().add("text-subheader");
		lvSelected  = new ListView<>();

		lvSelected.setCellFactory(new Callback<ListView<Connection>, ListCell<Connection>>() {
			public ListCell<Connection> call(ListView<Connection> param) {
				ConnectionListCell cell = new ConnectionListCell(ctrl, lvSelected);
				cell.setOnDragDetected(event -> dragCell(event, cell));
				return cell;
			}
		});

		Label phSelected = new Label(UI.getString("connselectpane.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);

		/*
		 * Column 3
		 */
		tfName = new TextField();
		tfType = new TextField();
		taDesc = new TextArea();
		tfName.setPromptText(UI.getString("connselectpane.prompt.name"));
		tfType.setPromptText(UI.getString("connselectpane.prompt.type"));
		taDesc.setPromptText(UI.getString("connselectpane.prompt.desc"));

		imgDelete = new FontIcon("\uE107");

		colDescription = new VBox(5);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvSelected.setStyle("-fx-min-width: 25em; -fx-pref-width: 32em");
		colDescription.setStyle("-fx-pref-width: 25em");

		HBox lineSelected = new HBox();
		lineSelected.setMaxWidth(Double.MAX_VALUE);
		lineSelected.getChildren().addAll(hdSelected, btnAdd);
		hdSelected.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(hdSelected, Priority.ALWAYS);

		Region spacing = new Region();
		VBox.setVgrow(spacing, Priority.ALWAYS);
		spacing.setMaxHeight(Double.MAX_VALUE);

		setSpacing(20);
		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		column2.getChildren().addAll(lineSelected, lvSelected);
		VBox column3 = new VBox(10);
		VBox.setVgrow(colDescription, Priority.ALWAYS);
		column3.getChildren().addAll(hdDescript, colDescription, spacing, imgDelete);

		column2.setMaxHeight(Double.MAX_VALUE);
		column3.setMaxHeight(Double.MAX_VALUE);
		lvSelected.setMaxHeight(Double.MAX_VALUE);

		// Description
		Label heaName = new Label(UI.getString("label.name"));
		Label heaType = new Label(UI.getString("label.type"));
		Label heaDesc = new Label(UI.getString("label.description"));

		GridPane descForm = new GridPane();
		descForm.add(heaName, 0, 0);
		descForm.add(tfName , 1, 0);
		descForm.add(heaType, 0, 1);
		descForm.add(tfType , 1, 1);

		colDescription.getChildren().addAll(descForm, heaDesc, taDesc);
		colDescription.setMaxHeight(Double.MAX_VALUE);
//		imgDelete.setAlignment(Pos.CENTER);

		getChildren().addAll(column2, column3);
		column3.setStyle("-fx-max-width: 28em");

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
					selectionChanged( n);
			}
		});
//		lvSelected.setOnDragDropped(event -> dragFromSkillDropped(event));
//		lvSelected.setOnDragOver(event -> dragFromSkillOver(event));

		btnAdd.setOnAction(ev -> addConnection());

		tfName.textProperty().addListener( (ov,o,n) -> {
			if (selectedItem!=null)
				selectedItem.setName(n);
		});
		tfType.textProperty().addListener( (ov,o,n) -> {
			if (selectedItem!=null)
				selectedItem.setType(n);
		});
		taDesc.textProperty().addListener( (ov,o,n) -> {
			if (selectedItem!=null)
				selectedItem.setDescription(n);
		});
		tfName.setOnAction(ev -> refresh());
		tfName.focusedProperty().addListener( (ov,o,n) -> refresh());
		tfType.setOnAction(ev -> refresh());
		tfType.focusedProperty().addListener( (ov,o,n) -> refresh());

		imgDelete.setOnDragOver(event -> dragOverTrash(event));
		imgDelete.setOnDragEntered(event -> dragEnteredTrash(event));
		imgDelete.setOnDragExited(event -> dragExitedTrash(event));
		imgDelete.setOnDragDropped(event -> dragDroppedTrash(event));
	}

	//--------------------------------------------------------------------
	private void addConnection() {
		logger.debug("addConnection");
		ctrl.createConnection();
	}

	//--------------------------------------------------------------------
	private void selectionChanged(Connection data) {
		selectedItem = data;

		colDescription.setVisible(selectedItem!=null);
		if (data!=null) {
			logger.debug("Selected "+data);
			tfName.setText(data.getName());
			tfType.setText(data.getType());
			taDesc.setText(data.getDescription());
		}
	}

	//--------------------------------------------------------------------
	private void refresh()  {
		Connection oldSelect = lvSelected.getSelectionModel().getSelectedItem();
		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(model.getConnections());
		if (oldSelect!=null)
			lvSelected.getSelectionModel().select(oldSelect);
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CONNECTION_ADDED:
		case CONNECTION_CHANGED:
		case CONNECTION_REMOVED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

	//--------------------------------------------------------------------
	private void dragCell(MouseEvent event, ConnectionListCell cell) {
		logger.debug("drag "+cell.getData());
		Node source = (Node) event.getSource();
		Connection data = cell.getData();

		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.MOVE);

        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        String id = "connection:"+data.getId();
        content.putString(id);
        db.setContent(content);

        /* Drag image */
        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);

        event.consume();
	}

	//-------------------------------------------------------------------
	private Connection getConnectionByDragboard(DragEvent event) {
		Dragboard db = event.getDragboard();
		if (db.hasString()) {
			String enhanceID = db.getString();
			// Get reference for ID
			if (enhanceID.startsWith("connection:")) {
				String search = enhanceID.substring(enhanceID.indexOf(":")+1);
				for (Connection con : lvSelected.getItems()) {
					if (con.getId().toString().equals(search)) {
						return con;
					}
				}
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private Object dragOverTrash(DragEvent event) {
		Connection ref = getConnectionByDragboard(event);
		if (ref!=null) {
            event.acceptTransferModes(TransferMode.MOVE);
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param event
	 * @return
	 */
	private Object dragEnteredTrash(DragEvent event) {
		Connection ref = getConnectionByDragboard(event);
		if (ref!=null) {
            imgDelete.getStyleClass().add("glow");
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @param event
	 * @return
	 */
	private Object dragExitedTrash(DragEvent event) {
		Connection ref = getConnectionByDragboard(event);
		if (ref!=null) {
			imgDelete.getStyleClass().remove("glow");
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragDroppedTrash(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	logger.debug("Dropped to trash: "+enhanceID);
        	Connection ref = getConnectionByDragboard(event);
        	if (ref!=null) {
    			logger.info("Trash connection "+ref);
    			ctrl.removeConnection(ref);
    		} else {
    			logger.warn("Cannot trash unknown resource reference: "+enhanceID);

        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

}

class ConnectionListCell extends ListCell<Connection> {

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private final static String NORMAL_STYLE = "connection-cell";

	private ConnectionsController charGen;
	private ListView<Connection> parent;

	private transient Connection data;

	private HBox layout;
	private Label lblName;
	private Label lblType;
	private Button btnEdit;
	private Button btnDecInfl;
	private Label  lblValInfl;
	private Button btnIncInfl;
	private Button btnDecLoyl;
	private Label  lblValLoyl;
	private Button btnIncLoyl;

	//-------------------------------------------------------------------
	public ConnectionListCell(ConnectionsController charGen, ListView<Connection> parent) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content
		lblName  = new Label();
		lblType  = new Label();
		btnEdit = new Button("\uE1C2");
		btnEdit.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnDecInfl  = new Button("\uE0C9");
		lblValInfl  = new Label("?");
		btnIncInfl  = new Button("\uE0C8");

		btnDecLoyl  = new Button("\uE0C9");
		lblValLoyl  = new Label("?");
		btnIncLoyl  = new Button("\uE0C8");

		layout = new HBox(10);

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		getStyleClass().add(NORMAL_STYLE);

		btnDecInfl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnIncInfl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnDecLoyl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		btnIncLoyl.setStyle("-fx-background-color: transparent; -fx-border-width: 0px; -fx-font-family: \"Segoe UI Symbol\"");
		lblName.setStyle("-fx-font-weight: bold");
		lblType.setStyle("-fx-font-weight: bold");
		lblValInfl.getStyleClass().add("text-subheader");
		lblValLoyl.getStyleClass().add("text-subheader");

		btnEdit.setStyle("-fx-background-color: transparent");

		//		setStyle("-fx-pref-width: 24em");
//		layout.getStyleClass().add("content");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaName = new Label(UI.getString("label.name"));
		Label heaType = new Label(UI.getString("label.type"));
		Label heaInfl = new Label(UI.getString("label.influence.short"));
		Label heaLoyl = new Label(UI.getString("label.loyalty.short"));

		GridPane col1 = new GridPane();
		col1.setStyle("-fx-hgap: 0.4em; -fx-vgap: 0.4em;");
		col1.add(heaName, 0, 0);
		col1.add(lblName, 1, 0);
		col1.add(heaType, 0, 1);
		col1.add(lblType, 1, 1);
		col1.setMaxWidth(Double.MAX_VALUE);

		GridPane col2 = new GridPane();
		col2.setStyle("-fx-hgap: 0.2em; -fx-vgap: 0.2em;");
		col2.add(heaInfl   , 0, 0);
		col2.add(btnDecInfl, 1, 0);
		col2.add(lblValInfl, 2, 0);
		col2.add(btnIncInfl, 3, 0);
		col2.add(heaLoyl   , 0, 1);
		col2.add(btnDecLoyl, 1, 1);
		col2.add(lblValLoyl, 2, 1);
		col2.add(btnIncLoyl, 3, 1);

		HBox.setHgrow(col1, Priority.ALWAYS);
		layout.getChildren().addAll(col1, col2);

		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnIncInfl .setOnAction(event -> {
			charGen.increaseInfluence(data);
			updateItem(data, false);
			parent.refresh();
		});
		btnDecInfl .setOnAction(event -> {
			charGen.decreaseInfluence(data);
			updateItem(data, false);
			parent.refresh();
		});
		btnIncLoyl .setOnAction(event -> {
			charGen.increaseLoyalty(data);
			updateItem(data, false);
			parent.refresh();
		});
		btnDecLoyl .setOnAction(event -> {
			charGen.decreaseLoyalty(data);
			updateItem(data, false);
			parent.refresh();
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Connection item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			data = item;

			lblName.setText(item.getName());
			lblType.setText(item.getType());
			btnEdit.setText("\uE1C2");
			btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.special")));

			lblValInfl.setText(String.valueOf(item.getInfluence()));
			btnDecInfl.setDisable(!charGen.canDecreaseInfluence(item));
			btnIncInfl.setDisable(!charGen.canIncreaseInfluence(item));

			lblValLoyl.setText(String.valueOf(item.getLoyalty()));
			btnDecLoyl.setDisable(!charGen.canDecreaseLoyalty(item));
			btnIncLoyl.setDisable(!charGen.canIncreaseLoyalty(item));

			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	public Connection getData() {
		return data;
	}

}

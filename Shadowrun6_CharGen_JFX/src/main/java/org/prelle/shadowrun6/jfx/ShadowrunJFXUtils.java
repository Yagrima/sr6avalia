/**
 * 
 */
package org.prelle.shadowrun6.jfx;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.ElementType;
import org.prelle.shadowrun6.MentorSpirit;
import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Sense;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.Spirit;
import org.prelle.shadowrun6.Sprite;
import org.prelle.shadowrun6.actions.ShadowrunAction;
import org.prelle.shadowrun6.actions.ShadowrunAction.Category;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.ModificationValueType;
import org.prelle.shadowrun6.modifications.SkillModification;

import de.rpgframework.character.HardcopyPluginData;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.Modifyable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class ShadowrunJFXUtils {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	//-------------------------------------------------------------------
	public static String getAttributeModPane(MetaType type) {
		List<String> ret = new ArrayList<>();
		for (Attribute attr : Attribute.primaryAndSpecialValues()) {
			if (type.isSpecialRacialAttribute(attr))
				ret.add(attr.getName());
		}
		
		if (ret.isEmpty())
			return Resource.get(UI, "label.none");
		
		return String.join(", ", ret);
	}

	//-------------------------------------------------------------------
	public static String getModificationTooltip(Modifyable mods) {
		StringBuffer buf = new StringBuffer();
		for (Iterator<Modification> it=mods.getModifications().iterator(); it.hasNext();) {
			Modification mod = it.next();
			if (mod instanceof AttributeModification) {
				if ( ((AttributeModification)mod).getType()!=ModificationValueType.NATURAL)
					continue;
				buf.append( ((AttributeModification)mod).getValue());
			} else if (mod instanceof SkillModification) {
					buf.append( ((SkillModification)mod).getValue());
			} else {
				logger.warn("Unsupported modification "+mod.getClass());
				buf.append(mod.toString());
			}
			buf.append(" (");
			if (mod.getSource()==null) {
				buf.append("?");
			} else if (mod.getSource().getClass()==CarriedItem.class) {
				buf.append(  ((CarriedItem)mod.getSource()).getName() );
			} else if (mod.getSource().getClass()==String.class) {
//				if (mod.getSource()==SplitterTools.LEVEL2)
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.level2"));
//				else if (mod.getSource()==SplitterTools.LEVEL3)
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.level3"));
//				else if (mod.getSource()==SplitterTools.LEVEL4)
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.level4"));
//				else if (((String)mod.getSource()).startsWith(EquipmentTools.TOTAL_HANDICAP))
//					buf.append(SplitterMondCore.getI18nResources().getString("modification.source.handicap"));
//				else
					logger.warn("Unknown modification source string '"+mod.getSource()+"/"+mod.getClass()+"'");
			} else if (mod.getSource().getClass()==Attribute.class) {
				buf.append(  ((Attribute)mod.getSource()).getName() );
			} else if (mod.getSource().getClass()==MetaType.class) {
				buf.append( ((MetaType)mod.getSource()).getName() );
			} else if (mod.getSource().getClass()==AdeptPowerValue.class) {
				buf.append( ((AdeptPowerValue)mod.getSource()).getName() );
//			} else if (mod.getSource().getClass()==Power.class) {
//				buf.append(  ((Power)mod.getSource()).getName() );
//			} else if (mod.getSource().getClass()==PowerReference.class) {
//				buf.append(  ((PowerReference)mod.getSource()).getPower().getName()+" "+((PowerReference)mod.getSource()).getCount() );
//			} else if (mod.getSource().getClass()==MastershipReference.class) {
//				buf.append(  ((MastershipReference)mod.getSource()).getMastership().getName());
//			} else if (mod.getSource().getClass()==Mastership.class) {
//				buf.append(  ((Mastership)mod.getSource()).getName());
			} else {
				logger.warn("Unsupported modification source "+mod.getSource().getClass());
				buf.append(String.valueOf(mod.getSource()));
			}
			buf.append(")");
			
			if (it.hasNext())
				buf.append("\n");
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public static Object choose(CharacterController charGen, ScreenManagerProvider provider, ShadowrunCharacter model, ChoiceType selectFrom, String choiceFor) {
		logger.debug("choose "+selectFrom);
		
		ChoiceBox<Object> options = new ChoiceBox<>();
		Label lbDesc = new Label();
		lbDesc.setWrapText(true);
		Label lbType = new Label();
		switch (selectFrom) {
		case ATTRIBUTE:
			for (Attribute attr : Attribute.primaryValues())
				options.getItems().add(attr);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Attribute)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			lbDesc.setText(Resource.get(UI, "dialog.choicetype.option.attribute.text"));
			lbType.setText(Resource.get(UI, "dialog.choicetype.option.attribute.type"));
			break;
		case COMBAT_SKILL:
			options.getItems().addAll(ShadowrunCore.getSkills(SkillType.COMBAT));
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Skill)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case ELEMENTAL:
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) {return ((ElementType)object).getName();}
				public Object fromString(String string) {return null;}
			});
			options.getItems().addAll(ElementType.values());
			break;
		case MATRIX_ACTION:
			options.getItems().addAll(ShadowrunCore.getActions(Category.MATRIX));
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((ShadowrunAction)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case MATRIX_ATTRIBUTE:
			for (Attribute attr : Attribute.matrixValues())
				options.getItems().add(attr);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Attribute)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			lbDesc.setText(Resource.get(UI, "dialog.choicetype.option.matrixattribute.text"));
			lbType.setText(Resource.get(UI, "dialog.choicetype.option.matrixattribute.type"));
			break;
		case MENTOR_SPIRIT:
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) {return ((MentorSpirit)object).getName();}
				public Object fromString(String string) {return null;}
			});
			options.getItems().addAll(ShadowrunCore.getMentorSpirits());
			break;
		case NAME:
			String name = letUserEnterText(provider, Resource.get(UI, "qualityselectpane.choice.name"));
			return name;
		case NONCOMBAT_SKILL:
			options.getItems().addAll(ShadowrunCore.getSkills());
			options.getItems().removeAll(ShadowrunCore.getSkills(SkillType.COMBAT));
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Skill)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case PHYSICAL_ATTRIBUTE:
			for (Attribute attr : Attribute.physicalValues())
				options.getItems().add(attr);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Attribute)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case PROGRAM:
			options.getItems().addAll(ShadowrunCore.getItems(ItemType.ACCESSORY, 
					ItemSubType.BASIC_PROGRAM, 
					ItemSubType.HACKING_PROGRAM,
					ItemSubType.AUTOSOFT,
					ItemSubType.OTHER_PROGRAMS
					));
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((ItemTemplate)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case SKILL:
//			options.getItems().addAll(ShadowrunCore.getSkills());
			options.getItems().addAll(charGen.getSkillController().getAllowedSkills());
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Skill)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		case SPIRIT:
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Spirit)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			options.getItems().addAll(ShadowrunCore.getSpirits());
			break;
		case SPRITE:
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Sprite)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			options.getItems().addAll(ShadowrunCore.getSprites());
			break;
		case SENSE:
			for (Sense sense : Sense.values())
				options.getItems().addAll(sense);
			options.setConverter(new StringConverter<Object>() {
				public String toString(Object object) { return ((Sense)object).getName(); }
				public Object fromString(String string) {return null;}
			});
			break;
		default:
			System.err.println("Choosing "+selectFrom+" not implemented");
			logger.error("Choosing "+selectFrom+" not implemented");
		}
		
		HBox line = new HBox(10, lbType, options);
		options.getSelectionModel().select(0);
		
		VBox content = new VBox(20);
		content.getChildren().addAll(lbDesc, line);
		DescriptionPane description = new DescriptionPane();
		
		OptionalDescriptionPane withDescr = new OptionalDescriptionPane(content, description);
		options.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n instanceof HardcopyPluginData)
				description.setText((HardcopyPluginData)n);});
		String title = Resource.format(UI, "dialog.choicetype.title", choiceFor);
		provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, title, withDescr);
		
		return options.getSelectionModel().getSelectedItem();
	}

	//-------------------------------------------------------------------
	private static String letUserEnterText(ScreenManagerProvider provider, String prompt) {
		Label label = new Label(Resource.get(UI,"dialog.choicetype.option.label"));
		label.getStyleClass().add("base");
		TextField tfInput = new TextField();
		
		VBox content = new VBox(5);
		content.getChildren().addAll(label, tfInput);
		
		NavigButtonControl buttonCtrl = new NavigButtonControl();
		buttonCtrl.setDisabled(CloseType.OK, true);
		tfInput.textProperty().addListener( (ov,o,n) -> {
			buttonCtrl.setDisabled(CloseType.OK, (n!=null && n.length()<3));
			});
		tfInput.setOnAction(ev -> buttonCtrl.fireEvent(CloseType.OK));

		CloseType close = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, Resource.get(UI,"dialog.choicetype.option.name"), content, buttonCtrl);
		if (close==CloseType.OK) {
			return tfInput.getText();
		}
		return null;
	}

}

/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.chargen.jfx.AttributePanePrimaryGen;
import org.prelle.shadowrun6.chargen.jfx.AttributePaneSecondaryGen;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageAttributes extends WizardPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageAttributes.class.getName());

	private CharacterGenerator charGen;

	private Label lbExplain;
	private AttributePanePrimaryGen primary;
	private AttributePaneSecondaryGen derived;
	private Label description;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageAttributes(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		logger.info("--> charGen is "+charGen);

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectAttr.title"));

		lbExplain = new Label(Resource.get(UI, "explain"));
		lbExplain.setWrapText(true);
		primary = new AttributePanePrimaryGen(charGen);
		derived = new AttributePaneSecondaryGen(charGen);

		content = new HBox();
		content.setSpacing(20);

		description = new Label();
		description.setWrapText(true);

		String fName = "images/wizard/Attributes.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, true);
		side.setPoints(charGen.getAttributeController().getPointsLeft());
		side.setPoints2(charGen.getAttributeController().getPointsLeftSpecial());
		side.setKarma(charGen.getCharacter().getKarmaFree());
		side.setPoints2Name(Resource.get(UI,"wizard.selectAttr.adjust"));
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);

		content.getChildren().addAll(primary, derived, description);

		VBox layout = new VBox();
		layout.getChildren().addAll(content);
		VBox.setVgrow(layout, Priority.NEVER);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		primary.getStyleClass().add("text-small-secondary");
		derived.getStyleClass().add("text-small-secondary");
		description.getStyleClass().add("text-body");
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {

	}

	//-------------------------------------------------------------------
	private void refresh() {
		logger.warn("REFRESH = "+charGen.getAttributeController()+ " for "+charGen.getClass());
		primary.refresh();
		derived.refresh();
		side.setPoints(charGen.getAttributeController().getPointsLeft());
		side.setPoints2(charGen.getAttributeController().getPointsLeftSpecial());
		side.setKarma(charGen.getCharacter().getKarmaFree());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CONSTRUCTIONKIT_CHANGED:
			logger.debug("RCV "+event);
			charGen = CharacterGeneratorRegistry.getSelected();
			logger.debug("change chargen to "+charGen);
			primary.updateCharacterGenerator(charGen);
			derived.updateController(charGen);
			break;
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event);
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		case POINTS_LEFT_ATTRIBUTES:
			logger.debug("RCV "+event);
			side.setPoints(charGen.getAttributeController().getPointsLeft());
			side.setKarma(charGen.getCharacter().getKarmaFree());
			break;
		default:
		}
	}

}

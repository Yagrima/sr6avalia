package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.MetamagicOrEcho;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.MetamagicOrEchoValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.ConfigOption;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class MetamagicOrEchoesSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(MetamagicOrEchoesSection.class.getName());

	private CharacterController ctrl;
	
	private Label ptsToSpend;
	private ListView<MetamagicOrEchoValue> list;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public MetamagicOrEchoesSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(),  null);
		if (provider==null)
			throw new NullPointerException("Missing ScreenManagerProvider");
		this.ctrl = ctrl;
		list = new ListView<MetamagicOrEchoValue>();
		if (ctrl instanceof CharacterLeveller)
			setSettingsButton( new Button(null, new SymbolIcon("setting")) );
		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		ptsToSpend = new Label();
		ptsToSpend.getStyleClass().add("base");
		ptsToSpend.setStyle("-fx-font-size: 133%");
		todos = FXCollections.observableArrayList();
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		setDeleteButton(btnDel);
		setAddButton(btnAdd);
		btnDel.setDisable(true);
		list.setCellFactory(new Callback<ListView<MetamagicOrEchoValue>, ListCell<MetamagicOrEchoValue>>() {
			public ListCell<MetamagicOrEchoValue> call(ListView<MetamagicOrEchoValue> param) {
				MetamagicOrEchoValueListCell cell =  new MetamagicOrEchoValueListCell(ctrl.getMetamagicOrEchoController(), list, getManagerProvider());
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getMetamagicOrEchoController().deselect(cell.getItem());
				});
				return cell;
			}
		});
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbToSpend = new Label(RES.getString(ctrl.getCharacter().getMagicOrResonanceType().usesMagic()?"label.initiategrade":"label.submersiongrade"));
		HBox line = new HBox(20, lbToSpend, ptsToSpend);
		line.setAlignment(Pos.CENTER_LEFT);
		
		VBox layout = new VBox(20, line, list);
		layout.setStyle("-fx-padding: 0.4em");
		setContent(layout);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		if (getSettingsButton()!=null)
			getSettingsButton().setOnAction(ev -> onSettings());
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			btnDel.setDisable(n==null);
			if (n!=null)
				showHelpFor.set(n.getModifyable());
		});
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening metamagic/echoes selection dialog");
		
		MetamagicOrEchoSelector selector = new MetamagicOrEchoSelector(ctrl.getMetamagicOrEchoController());
		selector.setPlaceholder(new Label(Resource.get(RES,"metaechosection.selector.placeholder")));
		SelectorWithHelp<MetamagicOrEcho> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES,"metaechosection.selector.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			MetamagicOrEcho data = selector.getSelectionModel().selectedItemProperty().get();
			logger.debug("Selected metamagic or echo: "+data);
			ctrl.getMetamagicOrEchoController().select(data);
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		MetamagicOrEchoValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("MetamagicOrEcho to deselect: "+selected);
		ctrl.getMetamagicOrEchoController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");
		todos.clear();
		todos.addAll(ctrl.getMetamagicOrEchoController().getToDos());
		
		ptsToSpend.setText( String.valueOf( ctrl.getCharacter().getInitiateSubmersionLevel()));
		list.getItems().clear();
		list.getItems().addAll(ctrl.getCharacter().getMetamagicOrEchoes());
		
		boolean canSelect = (!ctrl.getMetamagicOrEchoController().getAvailableMetamagics().isEmpty() && ctrl.getMetamagicOrEchoController().canBeSelected(ctrl.getMetamagicOrEchoController().getAvailableMetamagics().get(0)));
		btnAdd.setDisable(!canSelect);
	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<MetamagicOrEchoValue> getSelectionModel() {
		return list.getSelectionModel();
	}

	//-------------------------------------------------------------------
	private void onSettings() {
		CharacterLeveller ctrl = (CharacterLeveller) this.ctrl;
		
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : ctrl.getMetamagicOrEchoController().getConfigOptions()) {
//			CheckBox cb = new CheckBox(Resource.get(RES, "dialog.settings."+opt.getLocalId()));
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
	}

}

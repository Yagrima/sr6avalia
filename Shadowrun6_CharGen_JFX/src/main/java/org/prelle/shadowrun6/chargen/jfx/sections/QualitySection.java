/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.QualityValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ShadowrunJFXUtils;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.ConfigOption;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class QualitySection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(QualitySection.class.getName());

	private CharacterController control;
	private ShadowrunCharacter model;

	private ListView<QualityValue> list;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public QualitySection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		control = ctrl;
		model = ctrl.getCharacter();

		initComponents();
		initLayoutNormal();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		list = new ListView<QualityValue>();
		list.setStyle("-fx-min-height: 8em; -fx-pref-height: 30em; -fx-pref-width: 25em"); 
		list.setCellFactory(cell -> new QualityValueListCell(control, control.getQualityController(), model, list, getManagerProvider()));
		
		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
		if (control instanceof CharacterLeveller)
			setSettingsButton( new Button(null, new SymbolIcon("setting")) );
	}

	//-------------------------------------------------------------------
	private void initLayoutNormal() {
		initLayoutMinimal();
//		VBox col1 = new VBox(5, hdPositive, bxPositive);
//		VBox col2 = new VBox(5, hdNegative, bxNegative);
//		HBox columns = new HBox(col1, col2);
//		columns.setStyle("-fx-spacing: 1em");
//
//		getChildren().clear();
//		getChildren().add(columns);
	}

	//-------------------------------------------------------------------
	private void initLayoutMinimal() {
		setContent(list);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		if (getSettingsButton()!=null)
			getSettingsButton().setOnAction(ev -> onSettings());

		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info(n+" canBeDeselected = "+control.getQualityController().canBeDeselected(n));
			getDeleteButton().setDisable(n==null || !control.getQualityController().canBeDeselected(n));
			if (n!=null) showHelpFor.set(n.getModifyable());
			else
				showHelpFor.set(null);
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");

		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getQualities());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening quality selection dialog");
		
		QualitySelector selector = new QualitySelector(control.getQualityController());
//		selector.setPlaceholder(new Label(Resource.get(RES,"metaechosection.selector.placeholder")));
//		SelectorWithHelp<MetamagicOrEcho> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES,"qualitysection.selector.title"), selector, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			Quality data = selector.getSelected();
			logger.debug("Selected quality: "+data);
			if (data.needsChoice()) {
				Object choosen = ShadowrunJFXUtils.choose(control, managerProvider, model, data.getSelect(), data.getName());
				control.getQualityController().select(data, choosen);
			} else {						
				control.getQualityController().select(data);
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		QualityValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("Quality to deselect: "+selected);
		control.getQualityController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void onSettings() {
		CharacterLeveller ctrl = (CharacterLeveller) this.control;
		
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : ctrl.getQualityController().getConfigOptions()) {
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
	}

}

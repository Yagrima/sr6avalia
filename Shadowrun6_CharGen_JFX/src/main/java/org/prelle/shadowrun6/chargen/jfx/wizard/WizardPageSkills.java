/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.chargen.jfx.panels.SkillSelectionPane;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageSkills extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageSkills.class.getName());

	private CharacterGenerator charGen;

	private Label lbInfoText;
	private SkillSelectionPane skillPane;
	private DescriptionPane bxDescription;

	private OptionalDescriptionPane content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageSkills(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(Resource.get(UI, "wizard.selectSkills.title"));
		
		lbInfoText = new Label(Resource.get(UI, "wizard.selectSkills.info"));
		lbInfoText.setWrapText(true);

		skillPane = new SkillSelectionPane(charGen, this);
		skillPane.setData(charGen.getCharacter());

		bxDescription = new DescriptionPane();
		

		String fName = "images/wizard/Skills.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, true);
		int pointsSK = charGen.getSkillController().getPointsLeftSkills();
		int pointsLA = charGen.getSkillController().getPointsLeftInKnowledgeAndLanguage();
		side.setPoints(pointsSK);
		side.setPoints2Name(Resource.get(UI, "wizard.points.knowledge"));
		side.setPoints2(pointsLA);
		side.setKarma(charGen.getCharacter().getKarmaFree());

		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		content = new OptionalDescriptionPane(skillPane, bxDescription);
		super.setContent(new VBox(10,lbInfoText, content));
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		skillPane.selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null) {
				bxDescription.setText(null, null, null);
			} else {
				bxDescription.setText(n.getName(), n.getProductName()+" "+n.getPage(), n.getHelpText());
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			side.setPoints ( charGen.getSkillController().getPointsLeftSkills());
			side.setPoints2( charGen.getSkillController().getPointsLeftInKnowledgeAndLanguage());
			side.setKarma  ( charGen.getCharacter().getKarmaFree());
			skillPane.refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}

package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.shadowrun6.MetamagicOrEchoValue;
import org.prelle.shadowrun6.charctrl.MetamagicOrEchoController;
import org.prelle.shadowrun6.chargen.jfx.panels.PowerSpecializationPane;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

public class MetamagicOrEchoValueListCell extends ListCell<MetamagicOrEchoValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private final static String NORMAL_STYLE = "metaecho-cell";

	private MetamagicOrEchoController charGen;
	private ListView<MetamagicOrEchoValue> parent;
	private ScreenManagerProvider provider;

	private transient MetamagicOrEchoValue data;

	private HBox layout;
	private Label name;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private SymbolIcon lblLock;
	private StackPane stack;
	private ImageView imgRecommended;
	private Label lblType;

	private VBox  bxSpecial;

	//-------------------------------------------------------------------
	public MetamagicOrEchoValueListCell(MetamagicOrEchoController charGen, ListView<MetamagicOrEchoValue> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.provider = provider;

		lblType = new Label();
		lblType.getStyleClass().add("type-label");
		StackPane.setAlignment(lblType, Pos.BOTTOM_CENTER);

		// Content		
		layout  = new HBox(5);
		name    = new Label();
		bxSpecial = new VBox(1);
		btnDec = new Button("\uE0C6");
		btnDec.getStyleClass().addAll("mini-button");
		btnInc = new Button("\uE0C5");
		btnInc.getStyleClass().addAll("mini-button");
		lblVal  = new Label("?");
		lblLock = new SymbolIcon("lock");
		lblLock.setMaxWidth(50);

		// Recommended icon
//		imgRecommended = new ImageView(new Image(SR6Constants.class.getResourceAsStream("images/recommendation.png")));
//		imgRecommended.setFitHeight(16);
//		imgRecommended.setFitWidth(16);

		stack = new StackPane();
		stack.setMaxWidth(Double.MAX_VALUE);
		//		stack.setAlignment(Pos.TOP_RIGHT);
		stack.getChildren().addAll(lblLock, lblType, layout);
		StackPane.setAlignment(lblLock, Pos.TOP_RIGHT);

		initStyle();
		initLayout();
		initInteractivity();
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		btnDec.setStyle("-fx-background-color: transparent");
		btnInc.setStyle("-fx-background-color: transparent");
//		name.setStyle("-fx-font-weight: bold");
		name.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");

		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		//		setStyle("-fx-pref-width: 24em");
		layout.getStyleClass().add("content");		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line2 = new HBox(5);
		line2.getChildren().addAll(bxSpecial);

		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(name, line2);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		TilePane tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(bxCenter, tiles);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			charGen.increase(data);
			updateItem(data, false); 
			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			charGen.decrease(data);
			updateItem(data, false); 
			parent.refresh();
		});

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;

		//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		String id = "metaecho:"+data.getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MetamagicOrEchoValue item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;
		
		if (empty || item==null) {
			setText(null);
			setGraphic(null);			
		} else {
			data = item;
			name.setText(item.getName().toUpperCase());
			lblVal.setText(String.valueOf(item.getLevel()));
//			total.setText(String.valueOf(item.getModifyable().getCostForLevel(item.getLevel())));
			btnDec.setVisible(item.getModifyable().hasLevels());
			lblVal.setVisible(item.getModifyable().hasLevels());
			btnInc.setVisible(item.getModifyable().hasLevels());
			btnDec.setDisable(!charGen.canBeDecreased(item));
			btnInc.setDisable(!charGen.canBeIncreased(item));
			lblLock.setVisible( !charGen.canBeDeselected(item));
			//			tfDescr.setText(item.getModifyable().getAttribute1().getName());
			bxSpecial.getChildren().clear();
//			imgRecommended.setVisible(false);
			setGraphic(stack);
		}
	}

}
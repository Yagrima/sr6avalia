package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.CarriedItemOrItemEnhancementValueListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.ItemTemplateOrEnhancementListCell;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.character.HardcopyPluginData;
import de.rpgframework.genericrpg.ToDoElement;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class EditWeaponDialog extends ManagedDialog {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(EditWeaponDialog.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterController control;

	private NavigButtonControl btnControl;

	private FlowPane bxToDos;
	private ChoiceBox<ItemSubType> cbType;
	private ListView<BasePluginData> lvAvailable;
	private ListView<Object> lvSelected;
	private OptionalDescriptionPane content;
	private DescriptionPane description;
	private CarriedItem selectedItem;

	//--------------------------------------------------------------------
	public EditWeaponDialog(CharacterController ctrl, CarriedItem data) {
		super(UI.getString("dialog.title"), null, CloseType.APPLY);
		this.control = ctrl;
		this.selectedItem    = data;
		btnControl = new NavigButtonControl();
		
		initCompoments();
		initLayout();
		initInteractivity();
		refresh();
		
		description.setText(data.getItem());
		
		cbType.setValue(ItemSubType.ACCESSORY);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		bxToDos = new FlowPane();
		cbType = new ChoiceBox<ItemSubType>();
		cbType.getItems().addAll(ItemSubType.ACCESSORY, ItemSubType.MODIFICATION);
		cbType.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType value) { return value.getName(); }
			public ItemSubType fromString(String string) {return null;}
		});
		lvAvailable = new ListView<BasePluginData>();
		lvAvailable.setCellFactory( lv -> new ItemTemplateOrEnhancementListCell(control.getEquipmentController()));
		lvAvailable.setPlaceholder(new Label(UI.getString("list.available.placeholder")));
		lvAvailable.setStyle("-fx-min-width: 23em;");
		lvSelected  = new ListView<>();
		lvSelected.setCellFactory( lv -> new CarriedItemOrItemEnhancementValueListCell(control.getEquipmentController(), lvSelected));
		lvSelected.setPlaceholder(new Label(UI.getString("list.selected.placeholder")));
		lvSelected.setStyle("-fx-min-width: 25em;");
		description = new DescriptionPane();
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		// Column 1
		VBox column1 = new VBox(10, cbType, lvAvailable);
		
		// Column 2
		VBox column2 = new VBox(20, lvSelected);
		
		// Both lists
		HBox bxLists = new HBox(20, column1, column2);
		
		// Content without description
		VBox layout = new VBox(20);
		layout.getChildren().addAll(bxToDos, bxLists);
		
		// Layout
		content = new OptionalDescriptionPane(layout, description);

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(content);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			lvAvailable.getItems().clear();
			logger.warn("control = "+control);
			logger.warn("eq.ctrl = "+control.getEquipmentController());
			if (n==ItemSubType.ACCESSORY) {
				lvAvailable.getItems().addAll(control.getEquipmentController().getAvailableAccessoriesFor(selectedItem));
			} else {
				lvAvailable.getItems().addAll(control.getEquipmentController().getAvailableEnhancementsFor(selectedItem));
			}
			Collections.sort(lvAvailable.getItems(), new Comparator<BasePluginData>() {
				public int compare(BasePluginData o1, BasePluginData o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
		});
		
		lvAvailable.setOnDragOver(ev -> dragOverAvailable(ev));
		lvAvailable.setOnDragDropped(ev -> dragDroppedOnAvailable(ev));
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null)
				description.setText(selectedItem.getItem());
			else if (n instanceof HardcopyPluginData) {
				lvSelected.getSelectionModel().clearSelection();
				description.setText((HardcopyPluginData)n);
			} 
		});
		
		lvSelected.setOnDragOver(ev -> dragOverSelected(ev));
		lvSelected.setOnDragDropped(ev -> dragDroppedOnSelected(ev));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.warn("Selected "+n);
			if (n==null)
				description.setText(selectedItem.getItem());
			else if (n instanceof CarriedItem) {
				lvAvailable.getSelectionModel().clearSelection();
				description.setText( ((CarriedItem)n).getItem() );
			} else if (n instanceof ItemEnhancementValue) {
				lvAvailable.getSelectionModel().clearSelection();
				description.setText( ((ItemEnhancementValue)n).getModifyable() );
			} else if (n instanceof AvailableSlot) {
				lvAvailable.getSelectionModel().clearSelection();
				Iterator<CarriedItem> it = ((AvailableSlot)n).getAllEmbeddedItems().iterator();
				if (it.hasNext()) {
					description.setText( it.next().getItem() );
				}
			} 
		});
	}

//	//-------------------------------------------------------------------
//	private void updateOKButton() {
//		btnControl.setDisabled(CloseType.OK, tfName.getText()!=null && tfType.getText()!=null);
//	}

	//--------------------------------------------------------------------
	private void refresh()  {
		bxToDos.getChildren().clear();
		for (ToDoElement todo : selectedItem.getToDos()) {
			Label label = new Label(todo.getMessage());
			switch (todo.getSeverity()) {
			case INFO: break;
			case STOPPER: label.setStyle("-fx-text-fill: red"); break;
			case WARNING: label.setStyle("-fx-text-fill: orange"); break;
			}
			bxToDos.getChildren().add(label);
		}
		
		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(selectedItem.getEnhancements());
//		switch (selectedItem.getItem().getType()) {
//		case WEAPON:
//			lvSelected.getItems().addAll(selectedItem.getSlots());
//			break;
//		default:
//			lvSelected.getItems().addAll(selectedItem.getEffectiveAccessories());
//		}
		
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("gear:") || enhanceID.startsWith("gearmod:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("gear:") || enhanceID.startsWith("gearmod:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragDroppedOnSelected(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
        	if (enhanceID.startsWith("gear:")) {
        		String id = enhanceID.substring("gear:".length());
        		ItemTemplate master = ShadowrunCore.getItem(id);
         		if (master!=null) {
         			logger.debug("embed "+master+" in "+selectedItem);
         			CarriedItem added = control.getEquipmentController().embed(selectedItem, master);
         			logger.debug("embedding "+master+" in "+selectedItem+" returned "+added);
         			if (added!=null) {
         				lvSelected.getItems().add(added);
//         				refresh();
         			}
        		}
        	} else if (enhanceID.startsWith("gearmod:")) {
        		String id = enhanceID.substring("gearmod:".length());
        		ItemEnhancement master = ShadowrunCore.getItemEnhancement(id);
         		if (master!=null) {
         			ItemEnhancementValue itemMod = new ItemEnhancementValue(master);
         			selectedItem.addEnhancement(itemMod);
       				refresh();
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	/*
	 * DeSelect
	 */
	private void dragDroppedOnAvailable(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
        	if (enhanceID.startsWith("gear:")) {
        		String id = enhanceID.substring("gear:".length());
        		UUID uuid = UUID.fromString(id);
        		CarriedItem toDeselect = selectedItem.getEmbeddedItem(uuid);
         		if (toDeselect!=null) {
         			logger.info("try remove "+toDeselect+" from "+selectedItem);
         			boolean removed = control.getEquipmentController().remove(selectedItem, toDeselect);
         			if (removed) {
         				refresh();
         			}
        		}
        	} else if (enhanceID.startsWith("gearmod:")) {
        		String id = enhanceID.substring("gearmod:".length());
        		ItemEnhancement master = ShadowrunCore.getItemEnhancement(id);
         		if (master!=null) {
         			for (ItemEnhancementValue mod : selectedItem.getEnhancements()) {
         				if (mod.getModifyable()==master) {
         					selectedItem.removeEnhancement(mod);
               				refresh();
        				}
         			}
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

}

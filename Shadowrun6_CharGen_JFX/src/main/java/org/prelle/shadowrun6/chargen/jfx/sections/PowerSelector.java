package org.prelle.shadowrun6.chargen.jfx.sections;

import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.charctrl.AdeptPowerController;
import org.prelle.shadowrun6.chargen.jfx.listcells.PowerListCell;

import javafx.scene.Node;
import javafx.scene.control.ListView;

/**
 * @author Stefan Prelle
 *
 */
public class PowerSelector extends ListView<AdeptPower> implements IListSelector<AdeptPower> {
	
	//-------------------------------------------------------------------
	public PowerSelector(AdeptPowerController ctrl) {
		getItems().addAll(ctrl.getAvailablePowers());
		this.setCellFactory(lv -> new PowerListCell(ctrl));
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

}

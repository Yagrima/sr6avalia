package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.HashMap;
import java.util.Map;

import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.Spell.Category;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class SpellListCell extends ListCell<Spell> {
	
	private final static Map<Spell.Category, Image> IMAGES = new HashMap<>();
	
	private ImageView iview;
	private Label lblName;
	private Label lblLine1;
	
	private HBox bxLayout;
	private VBox bxData;
	private Spell data;
	
	//-------------------------------------------------------------------
	static {
		IMAGES.put(Category.COMBAT   , new Image(SR6Constants.class.getResourceAsStream("images/shadowrun/spelltype.combat.png")));
		IMAGES.put(Category.HEALTH   , new Image(SR6Constants.class.getResourceAsStream("images/shadowrun/spelltype.health.png")));
		IMAGES.put(Category.DETECTION, new Image(SR6Constants.class.getResourceAsStream("images/shadowrun/spelltype.detection.png")));
		IMAGES.put(Category.ILLUSION , new Image(SR6Constants.class.getResourceAsStream("images/shadowrun/spelltype.illusion.png")));
		IMAGES.put(Category.MANIPULATION, new Image(SR6Constants.class.getResourceAsStream("images/shadowrun/spelltype.manipulation.png")));
	}
	
	//-------------------------------------------------------------------
	public SpellListCell() {
		initComponents();
		initLayout();
		initInteractivity();
		
		getStyleClass().add("spell-list-cell");
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();
		
		iview = new ImageView();
		iview.setFitHeight(48);
		iview.setFitWidth(48);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		bxData = new VBox();
		bxData.getChildren().addAll(lblName, lblLine1);
		
		bxLayout = new HBox(5);
		bxLayout.getChildren().addAll(iview, bxData);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnDragDetected(event -> dragStarted(event));
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Spell item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(bxLayout);
			lblName.setText(item.getName().toUpperCase());
			lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
			lblLine1.setText(ShadowrunTools.makeFeatureString(item));
			iview.setImage(IMAGES.get(item.getCategory()));
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("spell:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}
}
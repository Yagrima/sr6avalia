package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Spell;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class SpellDescriptionPane extends VBox {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SpellSelectionDoublePane.class.getName());
	
	private boolean small;
	private Label descHeading;
	private Label descRef;
	private Label descFeat;
	private Label descType, descRange, descDur, descDrain;
	private Label description;

	//-------------------------------------------------------------------
	public SpellDescriptionPane(boolean small) {
		this.small = small;
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	public SpellDescriptionPane() {
		this(false);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		descFeat    = new Label();
		descType    = new Label();
		descRange   = new Label();
		descDur     = new Label();
		descDrain   = new Label();
		
		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblType  = new Label(Resource.get(UI, "label.type")+":");
		Label lblRange = new Label(Resource.get(UI, "label.range")+":");
		Label lblDur   = new Label(Resource.get(UI, "label.duration")+":");
		Label lblDrain = new Label(Resource.get(UI, "label.drain")+":");
		lblType.setStyle("-fx-font-weight: bold");
		lblRange.setStyle("-fx-font-weight: bold");
		lblDur.setStyle("-fx-font-weight: bold");
		lblDrain.setStyle("-fx-font-weight: bold");
		GridPane descGrid = new GridPane();
		if (small) {
			descGrid.add( lblType , 0, 0);
			descGrid.add(descType , 1, 0);
			descGrid.add( lblRange, 0, 1);
			descGrid.add(descRange, 1, 1);
			descGrid.add( lblDur  , 0, 2);
			descGrid.add(descDur  , 1, 2);
			descGrid.add( lblDrain, 0, 3);
			descGrid.add(descDrain, 1, 3);
		} else {
			descGrid.add( lblType , 0, 0);
			descGrid.add(descType , 1, 0);
			descGrid.add( lblRange, 2, 0);
			descGrid.add(descRange, 3, 0);
			descGrid.add( lblDur  , 0, 1);
			descGrid.add(descDur  , 1, 1);
			descGrid.add( lblDrain, 2, 1);
			descGrid.add(descDrain, 3, 1);
		}
		descGrid.setHgap(5);
		descGrid.setStyle("-fx-hgap: 1em; -fx-vgap: 1em");
		
		setSpacing(10);
		getChildren().addAll(descHeading, descRef, descFeat, descGrid, description);
		VBox.setVgrow(description, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	public void setData(Spell data) {
		if (data==null) {
			descHeading.setText(null);
			descRef.setText(null);
			descFeat.setText(null);
			descType.setText(null);
			descDur .setText(null);
			descDrain.setText(null);
			descRange.setText(null);
			description.setText(null);
		} else {
			descHeading.setText(data.getName().toUpperCase());
			descRef.setText(data.getProductName()+" "+data.getPage());
			descFeat.setText(ShadowrunTools.makeFeatureString(data));
			descType.setText(data.getType().getName());
			descDur .setText(data.getDuration().getName());
			if (data.getRange()==null) {
				System.err.println("No range for "+data);
			} else {
				descRange.setText(data.getRange().getName());
			}
			descDrain.setText(String.valueOf(data.getDrain()));

			description.setText(data.getHelpText());
		}
	}

}

/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.chargen.PriorityVariant;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class WizardPageGenerator extends WizardPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;
	
	private ShadowrunCharacter model;
	private ListView<CharacterGenerator> options;
	private Label descHeading;
	private Label description;
	private VBox bxSettings;
	private ChoiceBox<PriorityVariant> cbPrioVariant;

	//--------------------------------------------------------------------
	public WizardPageGenerator(Wizard wizard, ShadowrunCharacter model) {
		super(wizard);
		this.model = model;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		options.getSelectionModel().select(CharacterGeneratorRegistry.getSelected());
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		options= new ListView<CharacterGenerator>();
		options.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		options.setCellFactory(new Callback<ListView<CharacterGenerator>, ListCell<CharacterGenerator>>() {
			public ListCell<CharacterGenerator> call(ListView<CharacterGenerator> arg0) {
				return new CharacterGeneratorListCell();
			}
		});

		setTitle(UI.getString("wizard.selectGenerator.title"));

		// Fill with data
		for (CharacterGenerator cpt : CharacterGeneratorRegistry.getGenerators()) {
			logger.info("Found "+cpt);
//			RadioButton radio = new RadioButton(cpt.getName());
//			radio.setUserData(cpt);
			options.getItems().add(cpt);
		}

		descHeading = new Label();

		description = new Label();
		description.setWrapText(true);

		cbPrioVariant = new ChoiceBox<>();
		cbPrioVariant.getItems().addAll(PriorityVariant.values());
		cbPrioVariant.setConverter(new StringConverter<PriorityVariant>() {
			public String toString(PriorityVariant variant) { return variant.getName(); }
			public PriorityVariant fromString(String string) { return null; }
		});
		cbPrioVariant.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (CharacterGeneratorRegistry.getSelected()!=null && CharacterGeneratorRegistry.getSelected() instanceof NewPriorityCharacterGenerator)
				((NewPriorityCharacterGenerator)CharacterGeneratorRegistry.getSelected()).setVariant(n);
		});

		String fName = "images/wizard/Type_of_Creation.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		bxSettings = new VBox(10);
		
		VBox descBox = new VBox(20);
		descBox.getChildren().addAll(descHeading, description);
		description.setPrefWidth(400);


		HBox content = new HBox();
		content.setSpacing(20);
		content.getChildren().addAll(options, bxSettings, descBox);
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		descHeading.getStyleClass().add("text-small-subheader");
		description.getStyleClass().add("text-body");
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		options.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CharacterGenerator>() {
			public void changed(ObservableValue<? extends CharacterGenerator> observable,
					CharacterGenerator oldValue, CharacterGenerator newValue) {
					update(newValue);
			}
		});
	}

	//-------------------------------------------------------------------
	private void update(CharacterGenerator charGen) {
		if (charGen!=null) {
			descHeading.setText(charGen.getName());
			description.setText( charGen.getHelpText() );
			CharacterGeneratorRegistry.select(charGen, model);
			
			bxSettings.getChildren().clear();
			if (charGen instanceof NewPriorityCharacterGenerator) {
				Label lbVariant = new Label(Resource.get(UI, "label.variant"));
				lbVariant.getStyleClass().add("base");
				NewPriorityCharacterGenerator prio = (NewPriorityCharacterGenerator)charGen;
				bxSettings.getChildren().addAll(lbVariant, cbPrioVariant);
				cbPrioVariant.getSelectionModel().select(prio.getVariant());
			}
			
		} else {
			descHeading.setText(null);
			description.setText(null);
		}
	}

}

class CharacterGeneratorListCell extends ListCell<CharacterGenerator> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private VBox box;
	private Label lblHeading;
	private Label lblHardcopy;

	//--------------------------------------------------------------------
	public CharacterGeneratorListCell() {
		lblHeading = new Label();
		lblHeading.getStyleClass().add("base");
		lblHardcopy = new Label();
		box = new VBox(5);
		box.getChildren().addAll(lblHeading, lblHardcopy);

//		lblHardcopy.setOnAction(event -> {
//			HardcopyPluginData hardcopy = (HardcopyPluginData) ((Hyperlink)event.getSource()).getUserData();
//			logger.warn("TODO: open "+hardcopy.getPlugin().getID()+" on page "+hardcopy.getPage());
//		});
	}


	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CharacterGenerator item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
		} else {
			setGraphic(box);
			lblHeading.setText(item.getName());
			lblHardcopy.setText(item.getProductName()+" "+item.getPage());
			lblHardcopy.setUserData(item);
		}
	}
}
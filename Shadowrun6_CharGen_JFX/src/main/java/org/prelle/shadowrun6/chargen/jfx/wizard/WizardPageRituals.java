/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.chargen.jfx.panels.RitualDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.RitualSelectionPane;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageRituals extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageRituals.class.getName());

	private CharacterGenerator charGen;

	private RitualSelectionPane spellPane;
	private RitualDescriptionPane description;
	private Label instruction;

	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageRituals(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		/*
		 * Enable or disable page
		 */
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(Resource.get(UI, "wizard.selectRituals.title"));

		spellPane = new RitualSelectionPane(charGen.getRitualController(), this);
		spellPane.setData(charGen.getCharacter());
		
		description = new RitualDescriptionPane();

		instruction = new Label(Resource.get(UI, "wizard.selectRituals.instruct"));
		instruction.setWrapText(true);

		String fName = "images/wizard/Rituals.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, false);
		int pointsSK = charGen.getRitualController().getRitualsLeft();
		side.setPoints(pointsSK);
		side.setPointsName(Resource.get(UI, "points.rituals"));
		side.setKarma(charGen.getCharacter().getKarmaFree());
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		OptionalDescriptionPane content = new OptionalDescriptionPane(spellPane, description);
		
		VBox layout = new VBox(5, instruction, content);
		spellPane.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		instruction.getStyleClass().add("text-body");
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		spellPane.showHelpForProperty().addListener( (ov,o,n) -> description.setData(n));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

	//-------------------------------------------------------------------
	private void refresh() {
		/*
		 * Enable or disable page
		 */
		MagicOrResonanceType type = charGen.getCharacter().getMagicOrResonanceType();
		if (type!=null) {
			// Enable or disable page
			boolean mayUseSorcery = charGen.getSkillController().getAvailableSkills().contains(ShadowrunCore.getSkill("sorcery"));
			boolean hasSorcery = charGen.getCharacter().getSkillValue(ShadowrunCore.getSkill("sorcery"))!=null;
			boolean hasMatchingSkill = (hasSorcery || mayUseSorcery);
			if (type.usesSpells() && hasMatchingSkill) {
				logger.debug(type+" uses spells - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(type+" does not use spells - disable page");
				activeProperty().set(false);
			}
		} else {
			logger.warn("No magic or resonance type selected yet");
			activeProperty().set(false);
		}

		side.setKarma(charGen.getCharacter().getKarmaFree());
		side.setPoints( charGen.getRitualController().getRitualsLeft() );
		spellPane.refresh();
	}

}

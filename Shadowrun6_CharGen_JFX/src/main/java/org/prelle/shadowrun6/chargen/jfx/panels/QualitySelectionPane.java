/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.Quality.QualityType;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.chargen.jfx.listcells.QualityListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.QualityValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ShadowrunJFXUtils;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class QualitySelectionPane extends OptionalDescriptionPane {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".qualities");

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private CharacterController charGen;
	private QualityController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ChoiceBox<Quality.QualityType> cbFilter;
	private Label hdAvailable;
	private Label hdSelected;
	private ListView<Quality> lvAvailable;
	private ListView<QualityValue> lvSelected;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<BasePluginData>();

	//--------------------------------------------------------------------
	public QualitySelectionPane(CharacterController charGen, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.ctrl = charGen.getQualityController();
		this.provider = provider;
		initCompoments();
		initLayout();
		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		hdAvailable = new Label(UI.getString("label.available"));
		hdSelected  = new Label(UI.getString("label.selected"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		cbFilter = new ChoiceBox<>();
		cbFilter.getItems().addAll(Quality.QualityType.values());
		cbFilter.setConverter(new StringConverter<Quality.QualityType>() {
			public String toString(QualityType object) { return object.getName(); }
			public QualityType fromString(String string) { return null; }
		});
		lvAvailable = new ListView<>();
		lvSelected  = new ListView<>();

		lvAvailable.setCellFactory(lv -> {
			QualityListCell cell = new QualityListCell(ctrl);
			cell.setOnMouseClicked(event -> {
				if (event.getClickCount()==2) {
					Quality data = cell.getItem();
					if (data.needsChoice()) {
						Object choosen = ShadowrunJFXUtils.choose(charGen, provider, model, data.getSelect(), data.getName());
						ctrl.select(data, choosen);
					} else {
						ctrl.select(data);
					}

				}
			});
			return cell;
		});
		lvSelected.setCellFactory(lv -> {
			QualityValueListCell cell = new QualityValueListCell(charGen, ctrl, model, lv, provider);
			cell.setOnMouseClicked(event -> {
				if (event.getClickCount()==2) {
					ctrl.deselect(cell.getItem());
				}
			});
			return cell;
			});

		Label phSelected = new Label(UI.getString("qualityvaluelistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-min-width: 20em; -fx-pref-width: 24em");
		lvSelected.setStyle("-fx-min-width: 15em; -fx-pref-width: 24em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, cbFilter, lvAvailable);
		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		column2.getChildren().addAll(hdSelected, lvSelected);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected.setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		column1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column1, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			if (n!=null) {
				showHelpFor.set( ((QualityValue)n).getModifyable());
			}
		});
		lvSelected.setOnDragDropped(event -> dragFromQualityDropped(event));
		lvSelected.setOnDragOver(event -> dragFromQualityOver(event));
		lvAvailable.setOnDragDropped(event -> dragFromQualityValueDropped(event));
		lvAvailable.setOnDragOver(event -> dragFromQualityValueOver(event));
		cbFilter.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		if (cbFilter.getValue()!=null) {
			lvAvailable.getItems().addAll(ctrl.getAvailableQualities().stream().filter(qual -> qual.getType()==cbFilter.getValue()).collect(Collectors.toList()));
		} else {
			lvAvailable.getItems().addAll(ctrl.getAvailableQualities());
		}

		lvSelected.getItems().clear();
		lvSelected.getItems().addAll(model.getQualities());
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

//	//-------------------------------------------------------------------
//	private <T> T letUserSelect(ChoiceBox<T> choices) {
//		Label label = new Label(Resource.get(UI,"qualityselectpane.qualityoption_dialog.label"));
//		label.getStyleClass().add("text-small-subheader");
//		VBox content = new VBox(5);
//		content.getChildren().addAll(label, choices);
//
//		CloseType close = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, Resource.get(UI,"qualityselectpane.qualityoption_dialog.title"), content);
//		if (close==CloseType.OK) {
//			return choices.getValue();
//		}
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	private String letUserEnterText(String prompt) {
//		Label label = new Label(Resource.get(UI,"qualityselectpane.qualitytext_dialog.label"));
//		label.getStyleClass().add("text-small-subheader");
//		TextField tfInput = new TextField();
//		
//		VBox content = new VBox(5);
//		content.getChildren().addAll(label, tfInput);
//		
//		NavigButtonControl buttonCtrl = new NavigButtonControl();
//		buttonCtrl.setDisabled(CloseType.OK, true);
//		tfInput.textProperty().addListener( (ov,o,n) -> {
//			logger.debug("input now "+n);
//			buttonCtrl.setDisabled(CloseType.OK, (n!=null && n.length()>3));
//			});
//		tfInput.setOnAction(ev -> buttonCtrl.fireEvent(CloseType.OK));
//
//		CloseType close = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, Resource.get(UI,"qualityselectpane.qualityoption_dialog.title"), content, buttonCtrl);
//		if (close==CloseType.OK) {
//			return tfInput.getText();
//		}
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	private void trySelect(Quality data) {
//		ChoiceType type = data.getSelect();
//		logger.debug("select option "+type+" later");
//		logger.debug("select option "+type+" now");
//		switch (type) {
//		case ATTRIBUTE:
//			ChoiceBox<Attribute> choicesA = new ChoiceBox<Attribute>();
//			choicesA.setConverter(new StringConverter<Attribute>() {
//				public String toString(Attribute object) {return object.getName();}
//				public Attribute fromString(String string) {return null;}
//			});
//			choicesA.getItems().addAll(Attribute.primaryValues());
//			choicesA.getItems().addAll(Attribute.RESONANCE, Attribute.MAGIC);
//			Attribute selectA = letUserSelect(choicesA);
//			ctrl.select(data, selectA);
//			break;
//		case SKILL:
//			ChoiceBox<Skill> choicesS = new ChoiceBox<Skill>();
//			choicesS.setConverter(new StringConverter<Skill>() {
//				public String toString(Skill object) {return object.getName();}
//				public Skill fromString(String string) {return null;}
//			});
//			choicesS.getItems().addAll(ShadowrunCore.getSkills());
//			Skill selectS = letUserSelect(choicesS);
//			ctrl.select(data, selectS);
//			break;
//		case MATRIX_ACTION:
//			ChoiceBox<ShadowrunAction> choicesAct = new ChoiceBox<ShadowrunAction>();
//			choicesAct.setConverter(new StringConverter<ShadowrunAction>() {
//				public String toString(ShadowrunAction object) {return object.getName();}
//				public ShadowrunAction fromString(String string) {return null;}
//			});
//			choicesAct.getItems().addAll(ShadowrunCore.getActions(Category.MATRIX));
//			ShadowrunAction selectAct = letUserSelect(choicesAct);
//			ctrl.select(data, selectAct);
//			break;
//		case MENTOR_SPIRIT:
//			ChoiceBox<MentorSpirit> choicesMSpirit = new ChoiceBox<MentorSpirit>();
//			choicesMSpirit.setConverter(new StringConverter<MentorSpirit>() {
//				public String toString(MentorSpirit object) {return object.getName();}
//				public MentorSpirit fromString(String string) {return null;}
//			});
//			choicesMSpirit.getItems().addAll(ShadowrunCore.getMentorSpirits());
//			MentorSpirit selectMSpirit = letUserSelect(choicesMSpirit);
//			ctrl.select(data, selectMSpirit);
//			break;
//		case ELEMENTAL:
//			ChoiceBox<ElementType> choicesElem = new ChoiceBox<ElementType>();
//			choicesElem.setConverter(new StringConverter<ElementType>() {
//				public String toString(ElementType object) {return object.getName();}
//				public ElementType fromString(String string) {return null;}
//			});
//			choicesElem.getItems().addAll(ElementType.values());
//			ElementType selectElement = letUserSelect(choicesElem);
//			if (selectElement!=null)
//				ctrl.select(data, selectElement);
//			break;
//		case SPIRIT:
//			ChoiceBox<Spirit> choicesSpirit = new ChoiceBox<Spirit>();
//			choicesSpirit.setConverter(new StringConverter<Spirit>() {
//				public String toString(Spirit object) {return object.getName();}
//				public Spirit fromString(String string) {return null;}
//			});
//			choicesSpirit.getItems().addAll(ShadowrunCore.getSpirits());
//			Spirit selectSpirit = letUserSelect(choicesSpirit);
//			if (selectSpirit!=null)
//				ctrl.select(data, selectSpirit);
//			break;
//		case SPRITE:
//			ChoiceBox<Sprite> choicesSprite = new ChoiceBox<Sprite>();
//			choicesSprite.setConverter(new StringConverter<Sprite>() {
//				public String toString(Sprite object) {return object.getName();}
//				public Sprite fromString(String string) {return null;}
//			});
//			choicesSprite.getItems().addAll(ShadowrunCore.getSprites());
//			Sprite selectSprite = letUserSelect(choicesSprite);
//			if (selectSprite!=null)
//				ctrl.select(data, selectSprite);
//			break;
//		case NAME:
//			String name = letUserEnterText(Resource.get(UI, "qualityselectpane.choice.name"));
//			if (name!=null)
//				ctrl.select(data, name);
//			break;
//		default:
//			logger.error("Don't know how to select "+type);
//		}
//	}

	//-------------------------------------------------------------------
	private void dragFromQualityDropped(DragEvent event) {
		Runnable runner = null;
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("quality")) {
					Quality data = ShadowrunCore.getQuality(tail);
					if (data==null) {
						logger.warn("Cannot find quality for dropped quality id '"+tail+"'");						
					} else {
						// Some qualities need to be provide options first
						if (data.needsChoice()) {
							/*
							 * The following code needs to be in an extra platform thread
							 * to prevent the DragView image to stay visible when selecting
							 * the option - and prevent an exception later
							 */
							runner = new Runnable() {
								public void run() {
									Object choosen = ShadowrunJFXUtils.choose(charGen, provider, model, data.getSelect(), data.getName());
									if (choosen!=null)
										ctrl.select(data, choosen);
								}
							};
						} else {
							ctrl.select(data);
						}
					}

				}
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();

		if (runner!=null)
			Platform.runLater(runner);
	}

	//-------------------------------------------------------------------
	private Quality getQualityByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;

		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.startsWith("quality")) {
				return ShadowrunCore.getQuality(tail);
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromQualityOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Quality qual = getQualityByEvent(event);
			if (qual!=null && qual.isFreeSelectable()) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	private void dragFromQualityValueDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			Quality data = getQualityByEvent(event);
			if (data==null) {
				logger.warn("Cannot find quality for dropped quality");						
			} else if (!data.isFreeSelectable()) {
				logger.warn("Cannot deselect dropped racial quality id");						
			} else {
				event.setDropCompleted(success);
				event.consume();
				if (model.hasQuality(data.getId())) {
					QualityValue ref = model.getQuality(data.getId());
					ctrl.deselect(ref);
				}
				return;
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromQualityValueOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Quality qual = getQualityByEvent(event);
			if (qual!=null && qual.isFreeSelectable()) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

}

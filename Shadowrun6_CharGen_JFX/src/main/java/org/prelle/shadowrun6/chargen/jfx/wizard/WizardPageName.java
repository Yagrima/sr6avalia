/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter.Gender;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.genericrpg.ToDoElement.Severity;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class WizardPageName extends WizardPage implements GenerationEventListener, ChangeListener<String>  {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private CharacterGenerator charGen;

	private GridPane content;
	private TextField runnerName;
	private TextField realName;
	private ChoiceBox<Gender> gender;
//	private TextField hairColor;
//	private TextField eyeColor;
	private TextField weight;
	private TextField size_tf;
	private Slider size;
	private ImageView portrait;
	private byte[] imgData;
	private Label lblToDosHead;
	private Label lblToDos;

//	private Button randomHair;
//	private Button randomEyes;
//	private Button randomSize;
	private Button openFileChooser;

	//--------------------------------------------------------------------
	public WizardPageName(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		
		refresh();
		initInteractivity();

//		GenerationEventDispatcher.addListener(this);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(Resource.get(UI,"wizard.selectName.title"));

		runnerName= new TextField();
		realName  = new TextField();
//		hairColor = new TextField();
//		eyeColor  = new TextField();
		weight    = new TextField();
		size_tf   = new TextField();
		weight.setPromptText(Resource.get(UI,"prompt.weight"));
		size_tf.setPromptText(Resource.get(UI,"prompt.size"));

		int min = 50;
		int max = 250;
		int avg = (max-min)/2 + min;
		size      = new Slider(min,max,avg);
		size.setShowTickLabels(true);
		size.setShowTickMarks(true);
//		size.setMajorTickUnit(50);
		size.setMinorTickCount(5);
		gender    = new ChoiceBox<Gender>(FXCollections.observableArrayList(Gender.MALE, Gender.FEMALE));
		gender.setConverter(new StringConverter<Gender>() {
			public String toString(Gender val) { return Resource.get(UI, "gender."+val.name().toLowerCase()); }
			public Gender fromString(String string) { return null; }
		});
		portrait = new ImageView();
		portrait.setFitHeight(200);
		portrait.setFitWidth(200);
		portrait.setPreserveRatio(true);
		portrait.setImage(new Image(SR6Constants.class.getResourceAsStream("images/guest-256.png")));
		portrait.setStyle("-fx-border-width: 1px; -fx-border-style: solid; -fx-border-color: -fx-box-border;");
		lblToDos = new Label("-");
		lblToDos.setStyle("-fx-text-fill: red;");
		lblToDos.setWrapText(true);

		/*
		 * Buttons
		 */
//		randomHair = new Button(Resource.get(UI,"button.roll"));
//		randomEyes = new Button(Resource.get(UI,"button.roll"));
//		randomSize = new Button(Resource.get(UI,"button.roll"));

		/*
		 * Button portrait selection
		 */
		openFileChooser = new Button(Resource.get(UI,"button.select"));


		String fName = "images/wizard/Personal.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox foo = new HBox(portrait);
		foo.getStyleClass().add("bordered");

		VBox portBox = new VBox(10);
		portBox.setAlignment(Pos.TOP_CENTER);
		portBox.getChildren().addAll(foo, openFileChooser);

		lblToDosHead = new Label(Resource.get(UI,"label.todos"));

		content = new GridPane();
		content.setVgap(5);
		content.setHgap(5);
		content.add(new Label(Resource.get(UI,"label.streetname")), 0, 0);
		content.add(new Label(Resource.get(UI,"label.realname")), 0, 1);
		content.add(new Label(Resource.get(UI,"label.gender")), 0, 2);
		content.add(new Label(Resource.get(UI,"label.size"  )), 0, 3);
		content.add(new Label(Resource.get(UI,"label.weight")), 0, 4);
//		content.add(new Label(Resource.get(UI,"label.hair"  )), 0, 5);
//		content.add(new Label(Resource.get(UI,"label.eyes"  )), 0, 6);
		content.add(lblToDosHead, 0, 9);
		content.add(runnerName, 1, 0);
		content.add(realName  , 1, 1);
		content.add(gender    , 1, 2);
		content.add(size_tf   , 1, 3);
		content.add(weight    , 1, 4);
//		content.add(hairColor , 1, 5);
//		content.add(eyeColor  , 1, 6);
		content.add(lblToDos  , 1, 9, 3,1);
//		content.add(randomSize, 2, 3);
//		content.add(randomHair, 2, 5);
//		content.add(randomEyes, 2, 6);

		Region padding = new Region();
		content.add(padding, 1, 8);
		GridPane.setVgrow(padding, Priority.ALWAYS);

		content.add(portBox  , 3, 0, 1,7);
		GridPane.setValignment(portBox, VPos.TOP);

		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		runnerName.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setName(n));
		realName.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setRealName(n));
		gender.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> charGen.getCharacter().setGender(n));
//		hairColor.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setHairColor(n));
//		eyeColor.textProperty().addListener( (ov,o,n) -> charGen.getCharacter().setEyeColor(n));
		size_tf.textProperty().addListener( (ov,o,n) -> {
			try {
				if (n!=null)
					charGen.getCharacter().setSize(Integer.parseInt(n));
			} catch (NumberFormatException e) {
			}
		});
		weight.textProperty().addListener( (ov,o,n) -> {
			try {
				if (n!=null)
					charGen.getCharacter().setWeight(Integer.parseInt(n));
			} catch (NumberFormatException e) {
			}
		});

//		randomHair.setOnAction(new EventHandler<ActionEvent>() {
//			public void handle(ActionEvent event) {
//				hairColor.setText(charGen.rollHair());
//			}
//		});
//		randomEyes.setOnAction(new EventHandler<ActionEvent>() {
//			public void handle(ActionEvent event) {
//				eyeColor.setText(charGen.rollEyes());
//			}
//		});
//		randomSize.setOnAction(new EventHandler<ActionEvent>() {
//			public void handle(ActionEvent event) {
//				int[] sAw = charGen.rollSizeAndWeight();
////				size.setValue(sAw[0]);
//				size_tf.setText(String.valueOf(sAw[0]));
//				weight.setText(sAw[1]+"");
//			}
//		});
		openFileChooser.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				FileChooser chooser = new FileChooser();
				chooser.setInitialDirectory(new File(System.getProperty("user.home")));
				chooser.getExtensionFilters().addAll(
		                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
		                new FileChooser.ExtensionFilter("PNG", "*.png")
		            );
				File selection = chooser.showOpenDialog(new Stage());
				if (selection!=null) {
					try {
						byte[] imgBytes = Files.readAllBytes(selection.toPath());
						portrait.setImage(new Image(new ByteArrayInputStream(imgBytes)));
						imgData = imgBytes;
					} catch (IOException e) {
						logger.warn("Failed loading image from "+selection+": "+e);
					}
				}
			}
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.beans.value.ChangeListener#changed(javafx.beans.value.ObservableValue, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void changed(ObservableValue<? extends String> textfield, String oldVal,	String newVal) {
		if (logger.isTraceEnabled())
			logger.trace("changed "+textfield+" from "+oldVal+" to "+newVal);

		String n = runnerName.getText();
		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); runnerName.setText(n); }
		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); runnerName.setText(n); }
		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); runnerName.setText(n); }
		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); runnerName.setText(n); }

		n = realName.getText();
		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); realName.setText(n); }
		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); realName.setText(n); }
		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); realName.setText(n); }
		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); realName.setText(n); }

//		n = ethnicity.getText();
//		if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); ethnicity.setText(n); }
//		if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); ethnicity.setText(n); }
//		if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); ethnicity.setText(n); }
//		if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); ethnicity.setText(n); }

		List<String> todo = new ArrayList<>();
		charGen.getToDos().stream().filter(tmp -> tmp.getSeverity()==Severity.STOPPER).forEach(tmp -> todo.add(tmp.getMessage()));
		lblToDos.setText(String.join(", ", todo));
		lblToDosHead.setVisible(!charGen.getToDos().isEmpty());
	}

	//-------------------------------------------------------------------
	private void refresh() {
		List<String> todo = new ArrayList<>();
		charGen.getToDos().forEach(tmp -> {
			if (tmp.getSeverity()==Severity.STOPPER)
				todo.add(tmp.getMessage());
			});
		lblToDos.setText(String.join(", ",todo));
		lblToDosHead.setVisible(!charGen.getToDos().isEmpty());
		
		realName.setText(charGen.getCharacter().getRealName());
		runnerName.setText(charGen.getCharacter().getName());
		gender.setValue(charGen.getCharacter().getGender());
		size_tf.setText( String.valueOf(charGen.getCharacter().getSize()) );
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CONSTRUCTIONKIT_CHANGED:
			logger.debug("RCV "+event);
			charGen = (CharacterGenerator) event.getKey();
			break;
		default:
			refresh();
		}
	}

}

package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan Prelle
 *
 */
public class SR6AugmentationPage extends SR6ManagedScreenPage {

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6AugmentationPage.class.getName());

	private GearSection  cyberware;
	private GearSection  bioware;
	
	private Section secAugmentation;

	//-------------------------------------------------------------------
	public SR6AugmentationPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-augmentation");
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.augmentation"));
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;

		initComponents();
		initCommandBar();
		expLine = new SR6EquipmentFirstLine(mode, control, provider);
		getCommandBar().setContent(expLine);

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
	}

	//-------------------------------------------------------------------
	private void initAugmentation() {
		cyberware = new GearSection(UI.getString("section.cyberware"), control, provider, ItemType.CYBERWARE);
		bioware   = new GearSection(UI.getString("section.bioware"  ), control, provider, ItemType.BIOWARE);

		secAugmentation = new DoubleSection(cyberware, bioware);
		getSectionList().add(secAugmentation);

		// Interactivity
		cyberware.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		bioware  .showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initAugmentation();
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		super.refresh();
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.augmentation"));
		bioware.getToDoList().clear();
		for (ToDoElement tmp : control.getEquipmentController().getToDos()) {
			bioware.getToDoList().add(tmp);
		}
	}

}

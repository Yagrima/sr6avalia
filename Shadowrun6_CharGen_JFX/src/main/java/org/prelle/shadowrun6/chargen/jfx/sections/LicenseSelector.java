/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.LicenseType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.SIN.Quality;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class LicenseSelector implements IListSelector<LicenseType> {
	
	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(LicenseSection.class.getName());

	private CharacterController charGen;
	private SINController control;
	private NavigButtonControl btnControl;

	private GridPane layout;
	private ChoiceBox<SIN> cbSIN;
	private ChoiceBox<LicenseType> cbType;
	private ChoiceBox<Quality> cbQual;

	//-------------------------------------------------------------------
	public LicenseSelector(CharacterController charGen) {
		this.charGen = charGen;
		this.control = charGen.getSINController();
		btnControl   = new NavigButtonControl();
		btnControl.setDisabled(CloseType.OK, true);

		initComponents();
		initLayout();
		initInteractivity();

//		cbTypes.getSelectionModel().select(0);
//		cbSubTypes.getSelectionModel().select(0);
		refreshOkayButton();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		cbType = new ChoiceBox<>();
		cbType.getItems().addAll(ShadowrunCore.getLicenseTypes());
		cbType.setConverter( new StringConverter<LicenseType>() {
			public String toString(LicenseType value) { return value.getName(); }
			public LicenseType fromString(String string) {return null;}
		});
		cbSIN = new ChoiceBox<>();
		cbSIN.getItems().addAll(charGen.getCharacter().getSINs());
		cbSIN.setConverter( new StringConverter<SIN>() {
			public String toString(SIN value) { return value.getName(); }
			public SIN fromString(String string) {return null;}
		});
		cbQual = new ChoiceBox<>();
		cbQual.getItems().addAll(Quality.getSelectableValues());
		cbQual.setConverter( new StringConverter<SIN.Quality>() {
			public String toString(Quality value) { return value.getValue()+" - "+value.toString(); }
			public Quality fromString(String string) {return null;}
		});
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Elements for second column
		Label hdType = new Label(Resource.get(UI,"label.licensetype"));
		Label hdSIN  = new Label(Resource.get(UI,"label.sin"));
		Label hdQual = new Label(Resource.get(UI,"label.rating"));
		hdType.getStyleClass().add("base");
		hdSIN .getStyleClass().add("base");
		hdQual.getStyleClass().add("base");
		
		layout = new GridPane();
		layout.setStyle("-fx-vgap: 1em; -fx-hgap:0.5em"); 
		layout.add(hdType, 0,0);
		layout.add(cbType, 1,0);
		layout.add(hdSIN , 0,1);
		layout.add(cbSIN , 1,1);
		layout.add(hdQual, 0,2);
		layout.add(cbQual, 1,2);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbType.getSelectionModel().selectedItemProperty().addListener( (ovo,o,n) -> refreshOkayButton());
		cbSIN.getSelectionModel().selectedItemProperty().addListener( (ovo,o,n) -> { 
			if (n==null) cbQual.getSelectionModel().clearSelection();
			else if (n.getQuality()==Quality.REAL_SIN) {
				cbQual.getSelectionModel().clearSelection();
				cbQual.getItems().clear();
				cbQual.getItems().addAll(Quality.getSelectableValues());
			}
			else {
				cbQual.getItems().clear();
				for (Quality q : Quality.getSelectableValues()) {
					if (q.ordinal() <= n.getQuality().ordinal())
						cbQual.getItems().add(q);
				}
				cbQual.setValue(n.getQuality());
			}
			refreshOkayButton(); });
		cbQual.getSelectionModel().selectedItemProperty().addListener( (ovo,o,n) -> refreshOkayButton());
	}

	//-------------------------------------------------------------------
	private void refreshOkayButton() {
		btnControl.setDisabled(CloseType.OK, 
				cbType.getValue()==null || 
				cbSIN.getValue()==null ||
				cbQual.getValue()==null ||
				!control.canCreateNewLicense(cbType.getValue(), cbSIN.getValue(), cbQual.getValue()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return layout;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<LicenseType> getSelectionModel() {
		return cbType.getSelectionModel();
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	public LicenseType getSelectedLicenseType() {
		return cbType.getValue();
	}

	//-------------------------------------------------------------------
	public SIN getSelectedSIN() {
		return cbSIN.getValue();
	}

	//-------------------------------------------------------------------
	public Quality getSelectedQuality() {
		return cbQual.getValue();
	}

}

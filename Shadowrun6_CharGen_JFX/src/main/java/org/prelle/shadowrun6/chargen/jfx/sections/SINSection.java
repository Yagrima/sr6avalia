package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.SIN.Quality;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditSINDialog;
import org.prelle.shadowrun6.chargen.jfx.listcells.SINListCell;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import javafx.scene.control.Label;

/**
 * @author Stefan Prelle
 *
 */
public class SINSection extends GenericListSection<SIN> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SINSection.class.getName());

	//-------------------------------------------------------------------
	public SINSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
		list.setCellFactory( lv -> new SINListCell(ctrl, provider));
		initPlaceholder();
		
		refresh();
		list.setStyle("-fx-pref-height: 12em; -fx-pref-width: 25em;");
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(RES.getString("sinsection.listview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.trace("onAdd");
		
		SIN toAdd = new SIN();
		EditSINDialog dialog = new EditSINDialog(control, toAdd, false);
		CloseType result = (CloseType) provider.getScreenManager().showAndWait(dialog);
		logger.debug("Adding SIN dialog closed with "+result);
		if (result==CloseType.OK || result==CloseType.APPLY) {
			toAdd.setName((String)dialog.getData()[0]);
			toAdd.setQuality((Quality)dialog.getData()[1]);
			toAdd.setDescription((String)dialog.getData()[2]);
			logger.info("Adding SIN: "+toAdd);
			SIN added = control.getSINController().createNewSIN(toAdd.getName(), toAdd.getQuality());
			added.setDescription(toAdd.getDescription());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		logger.trace("onDelete");
		SIN toDelete = list.getSelectionModel().getSelectedItem();
		
		if (toDelete!=null) {
			CloseType close = CloseType.OK;
			if (control instanceof CharacterLeveller) {
				// Safety question
				close = provider.getScreenManager().showAlertAndCall(AlertType.CONFIRMATION, Resource.get(RES, "confirm.sin-delete.title"), Resource.format(RES, "confirm.sin-delete.content", toDelete.getName()));
				logger.debug("Answer was "+close);
			}
			if (close==CloseType.YES || close==CloseType.OK) {
				logger.info("Try remove sin: "+toDelete);
				control.getSINController().deleteSIN(toDelete);
				list.getItems().remove(toDelete);
				list.getSelectionModel().clearSelection();
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		setData(control.getCharacter().getSINs());
	}

}

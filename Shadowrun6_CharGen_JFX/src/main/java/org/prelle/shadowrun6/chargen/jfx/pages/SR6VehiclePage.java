package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.ToDoElement;
import javafx.scene.layout.Region;

/**
 * @author Stefan Prelle
 *
 */
public class SR6VehiclePage extends SR6ManagedScreenPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6VehiclePage.class.getName());

	private GearSection vehicles;
	private GearSection drones;

	//-------------------------------------------------------------------
	public SR6VehiclePage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-vehicles");
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.vehicles"));

		initComponents();
		expLine = new SR6EquipmentFirstLine(mode, control, provider);
		getCommandBar().setContent(expLine);
		refresh();
	}

	//-------------------------------------------------------------------
	private void initVehicles() {
		vehicles = new GearSection(UI.getString("section.vehicles"), control, provider, ItemType.VEHICLES);
		vehicles.getContent().setStyle("-fx-pref-width: 60em");
		((Region)vehicles.getContent()).setMaxWidth(Double.MAX_VALUE);
		
		getSectionList().add(vehicles);

		// Interactivity
		vehicles.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initDrones() {
		drones   = new GearSection(UI.getString("section.drones"), control, provider, ItemType.DRONES);
		getSectionList().add(drones);

		// Interactivity
		drones.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initVehicles();
		initDrones();
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");
		super.refresh();

//		vehicles.refresh();
//		drones.refresh();
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.vehicles"));
		vehicles.getToDoList().clear();
		for (ToDoElement tmp : control.getEquipmentController().getToDos()) {
			vehicles.getToDoList().add(tmp);
		}
		drones.getToDoList().clear();
		for (ToDoElement tmp : control.getEquipmentController().getToDos()) {
			drones.getToDoList().add(tmp);
		}
	}

}

package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.panels.SpellDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.sections.FociSection;
import org.prelle.shadowrun6.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun6.chargen.jfx.sections.MetamagicOrEchoesSection;
import org.prelle.shadowrun6.chargen.jfx.sections.PowersSection;
import org.prelle.shadowrun6.chargen.jfx.sections.RitualSection;
import org.prelle.shadowrun6.chargen.jfx.sections.SpellSection;
import org.prelle.shadowrun6.chargen.jfx.sections.TraditionSection;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan Prelle
 *
 */
public class SR6MagicPage extends SR6ManagedScreenPage {

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6MagicPage.class.getName());

	private TraditionSection tradition;
	private SpellSection spellsNormal;
	private SpellSection spellsAlchemistic;
	private PowersSection  powers;
	private RitualSection rituals;
	private GearSection  gear;
	private MetamagicOrEchoesSection meta;
	private FociSection foci;
	
	private List<Section> currentSections;
	private DoubleSection secLine1;
	private DoubleSection secLine2;
	private DoubleSection secLine3;
	private DoubleSection secLine4;
	private MagicOrResonanceType lastMOR;

	//-------------------------------------------------------------------
	public SR6MagicPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-magic");
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		currentSections = new ArrayList<>();

		initComponents();
		initCommandBar();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
	}

	//-------------------------------------------------------------------
	private void initTradition() {
		tradition = new TraditionSection(UI.getString("section.tradition"), control, provider);

		getSectionList().add(tradition);

		// Interactivity
		tradition.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initAllSections() {
		spellsNormal = new SpellSection(Resource.get(UI, "section.spells.normal"), control, false, provider);
		spellsAlchemistic = new SpellSection(Resource.get(UI, "section.spells.alchemistic"), control, true, provider);
		powers  = new PowersSection(Resource.get(UI, "section.powers"), control, provider);
		rituals = new RitualSection(Resource.get(UI, "section.rituals"), control, provider);
		gear    = new GearSection  (Resource.get(UI, "section.gear"), control, provider, ItemType.MAGICAL);
		meta    = new MetamagicOrEchoesSection(Resource.get(UI, "section.initiation"), control, provider);
		foci    = new FociSection(Resource.get(UI, "section.foci"), control, provider);

		secLine1= new DoubleSection(null, null);
		secLine2= new DoubleSection(null, null);
		secLine3= new DoubleSection(null, null);
		secLine4= new DoubleSection(null, null);

		// Interactivity
		spellsNormal.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		spellsAlchemistic.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		powers.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		rituals.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		gear.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		meta.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		foci.showHelpForProperty().addListener( (ov,o,n) -> updateHelp( (n!=null)?n.getModifyable():null));
	}

//	//-------------------------------------------------------------------
//	private void initSpells() {
//		spellsNormal = new SpellSection(UI.getString("section.spells.normal"), control, false, provider);
//		spellsAlchemistic = new SpellSection(UI.getString("section.spells.alchemistic"), control, true, provider);
//
//		secSpells = new DoubleSection(spellsNormal, spellsAlchemistic);
//		getSectionList().add(secSpells);
//
//		// Interactivity
//		spellsNormal.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
//		spellsAlchemistic.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
//	}

//	//-------------------------------------------------------------------
//	private void initPowers() {
//		powers = new PowersSection(UI.getString("section.powers"), control, provider);
//		getSectionList().add(powers);
//
//		// Interactivity
//		powers.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
//	}

//	//-------------------------------------------------------------------
//	private void initRitualsAndGear() {
//		rituals = new RitualSection(UI.getString("section.rituals"), control, provider);
//		gear    = new GearSection(UI.getString("section.gear"), control, provider, ItemType.MAGICAL);
//
//		secRitualsAndGear = new DoubleSection(rituals, gear);
//		getSectionList().add(secRitualsAndGear);
//
//		// Interactivity
//		rituals.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
//		gear.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
//	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initTradition();
		initAllSections();
//		initSpells();
//		initPowers();
//		initRitualsAndGear();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		spellsNormal.showHelpForProperty().addListener( (ov,o,n) -> {
			spellsAlchemistic.getSelectionModel().clearSelection();
			gear.getSelectionModel().clearSelection();
			powers.getSelectionModel().clearSelection();
			rituals.getSelectionModel().clearSelection();
			meta.getSelectionModel().clearSelection();
			foci.getSelectionModel().clearSelection();
		});
		spellsAlchemistic.showHelpForProperty().addListener( (ov,o,n) -> {
			spellsNormal.getSelectionModel().clearSelection();
			gear.getSelectionModel().clearSelection();
			powers.getSelectionModel().clearSelection();
			rituals.getSelectionModel().clearSelection();
			meta.getSelectionModel().clearSelection();
			foci.getSelectionModel().clearSelection();
		});
		gear.showHelpForProperty().addListener( (ov,o,n) -> {
			spellsNormal.getSelectionModel().clearSelection();
			spellsAlchemistic.getSelectionModel().clearSelection();
			powers.getSelectionModel().clearSelection();
			rituals.getSelectionModel().clearSelection();
			meta.getSelectionModel().clearSelection();
			foci.getSelectionModel().clearSelection();
		});
		powers.showHelpForProperty().addListener( (ov,o,n) -> {
			spellsNormal.getSelectionModel().clearSelection();
			spellsAlchemistic.getSelectionModel().clearSelection();
			gear.getSelectionModel().clearSelection();
			rituals.getSelectionModel().clearSelection();
			meta.getSelectionModel().clearSelection();
			foci.getSelectionModel().clearSelection();
		});
		rituals.showHelpForProperty().addListener( (ov,o,n) -> {
			spellsNormal.getSelectionModel().clearSelection();
			spellsAlchemistic.getSelectionModel().clearSelection();
			gear.getSelectionModel().clearSelection();
			powers.getSelectionModel().clearSelection();
			meta.getSelectionModel().clearSelection();
			foci.getSelectionModel().clearSelection();
		});
		meta.showHelpForProperty().addListener( (ov,o,n) -> {
			spellsNormal.getSelectionModel().clearSelection();
			spellsAlchemistic.getSelectionModel().clearSelection();
			gear.getSelectionModel().clearSelection();
			powers.getSelectionModel().clearSelection();
			rituals.getSelectionModel().clearSelection();
			foci.getSelectionModel().clearSelection();
		});
		foci.showHelpForProperty().addListener( (ov,o,n) -> {
			spellsNormal.getSelectionModel().clearSelection();
			spellsAlchemistic.getSelectionModel().clearSelection();
			gear.getSelectionModel().clearSelection();
			powers.getSelectionModel().clearSelection();
			rituals.getSelectionModel().clearSelection();
			meta.getSelectionModel().clearSelection();
		});
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			if (data instanceof Spell) {
				this.setDescriptionHeading(null);
				this.setDescriptionPageRef(null);
				this.setDescriptionText(null);
				SpellDescriptionPane pane = new SpellDescriptionPane(true);
				pane.setData((Spell) data);
				this.setDescriptionNode(pane);
			} else {
				this.setDescriptionHeading(data.getName());
				this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
				this.setDescriptionText(data.getHelpText());
			}
		} else {
//			this.setDescriptionHeading(null);
//			this.setDescriptionPageRef(null);
//			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	private void updateSectionOrder() {
		MagicOrResonanceType magic = control.getCharacter().getMagicOrResonanceType();
		if (magic==lastMOR)
			return;
		
		currentSections.clear();
		getSectionList().clear();
		if (magic!=null) {

			// Normal spells always first
			if (magic.usesSpells())
				currentSections.add(spellsNormal);
			if (magic.usesPowers())
				currentSections.add(powers);
			currentSections.add(meta);
			if (magic.usesSpells()) {
				currentSections.add(rituals);
				currentSections.add(spellsAlchemistic);
			}
		}
		currentSections.add(foci);
		currentSections.add(gear);
		
		int i=-1;
		DoubleSection doubSect = null;
		for (Section sec : currentSections) {
			i++;
			switch (i/2) {
			case 0: doubSect = secLine1; break;
			case 1: doubSect = secLine2; break;
			case 2: doubSect = secLine3; break;
			case 3: doubSect = secLine4; break;
			}
//			logger.trace("Set "+sec.getClass()+" at "+(i%2)+" x "+(i/2));

			if ((i%2)==0) {
				doubSect.setLeftSection(sec);
				doubSect.setRightSection(null);
			} else
				doubSect.setRightSection(sec);

			if (!getSectionList().contains(doubSect))
				getSectionList().add(doubSect);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
//		logger.debug("START refresh");
		super.refresh();
		this.setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.magic"));
		meta.refresh();
		rituals.getToDoList().clear();
		for (ToDoElement tmp : control.getRitualController().getToDos()) {
			rituals.getToDoList().add(tmp);
		}
		spellsNormal.getToDoList().clear();
		for (ToDoElement tmp : control.getSpellController().getToDos()) {
			spellsNormal.getToDoList().add(tmp);
		}
		powers.getToDoList().clear();
		for (ToDoElement tmp : control.getPowerController().getToDos()) {
			powers.getToDoList().add(tmp);
		}
		foci.getToDoList().clear();
		for (ToDoElement tmp : control.getFocusController().getToDos()) {
			foci.getToDoList().add(tmp);
		}
		
		updateSectionOrder();
//		logger.debug("STOP refresh");
	}

}

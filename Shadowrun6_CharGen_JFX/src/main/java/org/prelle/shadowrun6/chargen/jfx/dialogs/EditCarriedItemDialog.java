package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController.EmbedOption;
import org.prelle.shadowrun6.chargen.jfx.listcells.AvailableSlotPane;
import org.prelle.shadowrun6.chargen.jfx.listcells.ItemTemplateListCell2;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemEnhancement;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.character.HardcopyPluginData;
import de.rpgframework.genericrpg.ToDoElement;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class EditCarriedItemDialog extends ManagedDialog implements GenerationEventListener {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(EditCarriedItemDialog.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterController control;
	private ScreenManagerProvider provider;

	private NavigButtonControl btnControl;

	private FlowPane bxToDos;
	private TabPane tabs;
	private AccessoriesTab tabAccess;
	private ModificationsTab tabModif;
	private OptionalDescriptionPane content;
	private DescriptionPane description;
	private CarriedItem selectedItem;

	//--------------------------------------------------------------------
	public EditCarriedItemDialog(CharacterController ctrl, CarriedItem data, ScreenManagerProvider prov) {
		super(UI.getString("dialog.title"), null, CloseType.APPLY);
		if (prov==null)
			throw new NullPointerException("ScreenManagerProvider");
		this.control = ctrl;
		this.selectedItem    = data;
		this.provider = prov;
		btnControl = new NavigButtonControl();
		
		initCompoments();
		initLayout();
		initInteractivity();
		refresh();
		
		description.setText(data.getItem());
		
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		bxToDos = new FlowPane();
		
		tabAccess = new AccessoriesTab(control, selectedItem, provider, this);
		tabModif  = new ModificationsTab(control, selectedItem);
		tabs = new TabPane();
		tabs.getTabs().addAll(tabAccess, tabModif);
		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
//		lvAvailable = new ListView<BasePluginData>();
//		lvAvailable.setCellFactory( lv -> new ItemTemplateOrEnhancementListCell(control.getEquipmentController()));
//		lvAvailable.setPlaceholder(new Label(UI.getString("list.available.placeholder")));
//		lvAvailable.setStyle("-fx-min-width: 23em;");
//		lvSelected  = new ListView<>();
//		lvSelected.setCellFactory( lv -> new CarriedItemOrItemEnhancementValueListCell(control.getEquipmentController(), lvSelected));
//		lvSelected.setPlaceholder(new Label(UI.getString("list.selected.placeholder")));
//		lvSelected.setStyle("-fx-min-width: 25em;");
		description = new DescriptionPane();
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		// Content without description
		VBox layout = new VBox(20);
		layout.getChildren().addAll(bxToDos, tabs);
		
		// Layout
		content = new OptionalDescriptionPane(layout, description);

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(content);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
//		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			lvAvailable.getItems().clear();
//			logger.warn("control = "+control);
//			logger.warn("eq.ctrl = "+control.getEquipmentController());
//			if (n==ItemSubType.ACCESSORY) {
//				lvAvailable.getItems().addAll(control.getEquipmentController().getAvailableAccessoriesFor(selectedItem));
//			} else {
//				lvAvailable.getItems().addAll(control.getEquipmentController().getAvailableEnhancementsFor(selectedItem));
//			}
//			Collections.sort(lvAvailable.getItems(), new Comparator<BasePluginData>() {
//				public int compare(BasePluginData o1, BasePluginData o2) {
//					return o1.getName().compareTo(o2.getName());
//				}
//			});
//		});
		
//		lvAvailable.setOnDragOver(ev -> dragOverAvailable(ev));
//		lvAvailable.setOnDragDropped(ev -> dragDroppedOnAvailable(ev));
		tabAccess.selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null)
				description.setText(selectedItem.getItem());
			else if (n instanceof HardcopyPluginData) {
//				lvSelected.getSelectionModel().clearSelection();
				description.setText((HardcopyPluginData)n);
			} 
		});
		
//		lvSelected.setOnDragOver(ev -> dragOverSelected(ev));
//		lvSelected.setOnDragDropped(ev -> dragDroppedOnSelected(ev));
//		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
//			logger.warn("Selected "+n);
//			if (n==null)
//				description.setText(selectedItem.getItem());
//			else if (n instanceof CarriedItem) {
//				lvAvailable.getSelectionModel().clearSelection();
//				description.setText( ((CarriedItem)n).getItem() );
//			} else if (n instanceof ItemEnhancementValue) {
//				lvAvailable.getSelectionModel().clearSelection();
//				description.setText( ((ItemEnhancementValue)n).getModifyable() );
//			} else if (n instanceof AvailableSlot) {
//				lvAvailable.getSelectionModel().clearSelection();
//				Iterator<CarriedItem> it = ((AvailableSlot)n).getAllEmbeddedItems().iterator();
//				if (it.hasNext()) {
//					description.setText( it.next().getItem() );
//				}
//			} 
//		});
	}

//	//-------------------------------------------------------------------
//	private void updateOKButton() {
//		btnControl.setDisabled(CloseType.OK, tfName.getText()!=null && tfType.getText()!=null);
//	}

	//--------------------------------------------------------------------
	public void refresh()  {
		bxToDos.getChildren().clear();
		for (ToDoElement todo : selectedItem.getToDos()) {
			Label label = new Label(todo.getMessage());
			switch (todo.getSeverity()) {
			case INFO: break;
			case STOPPER: label.setStyle("-fx-text-fill: red"); break;
			case WARNING: label.setStyle("-fx-text-fill: orange"); break;
			}
			bxToDos.getChildren().add(label);
		}
		
		tabAccess.refresh();
		tabModif.refresh();
//		lvSelected.getItems().clear();
//		lvSelected.getItems().addAll(selectedItem.getEnhancements());
//		switch (selectedItem.getItem().getType()) {
//		case WEAPON:
//			lvSelected.getItems().addAll(selectedItem.getSlots());
//			break;
//		default:
//			lvSelected.getItems().addAll(selectedItem.getEffectiveAccessories());
//		}
		
	}

	//--------------------------------------------------------------------
	public void refreshAccessoryContent()  {
		tabAccess.refreshAvailableContent();
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOverSelected(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("gear:") || enhanceID.startsWith("gearmod:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOverAvailable(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("gear:") || enhanceID.startsWith("gearmod:")) {
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragDroppedOnSelected(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
        	if (enhanceID.startsWith("gear:")) {
        		String id = enhanceID.substring("gear:".length());
        		ItemTemplate master = ShadowrunCore.getItem(id);
         		if (master!=null) {
         			logger.debug("embed "+master+" in "+selectedItem);
         			CarriedItem added = control.getEquipmentController().embed(selectedItem, master);
         			logger.debug("embedding "+master+" in "+selectedItem+" returned "+added);
         			if (added!=null) {
//         				lvSelected.getItems().add(added);
//         				refresh();
         			}
        		}
        	} else if (enhanceID.startsWith("gearmod:")) {
        		String id = enhanceID.substring("gearmod:".length());
        		ItemEnhancement master = ShadowrunCore.getItemEnhancement(id);
         		if (master!=null) {
         			ItemEnhancementValue itemMod = new ItemEnhancementValue(master);
         			selectedItem.addEnhancement(itemMod);
       				refresh();
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//-------------------------------------------------------------------
	/*
	 * DeSelect
	 */
	private void dragDroppedOnAvailable(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
        	if (enhanceID.startsWith("gear:")) {
        		String id = enhanceID.substring("gear:".length());
        		UUID uuid = UUID.fromString(id);
        		CarriedItem toDeselect = selectedItem.getEmbeddedItem(uuid);
         		if (toDeselect!=null) {
         			logger.info("try remove "+toDeselect+" from "+selectedItem);
         			boolean removed = control.getEquipmentController().remove(selectedItem, toDeselect);
         			if (removed) {
         				refresh();
         			}
        		}
        	} else if (enhanceID.startsWith("gearmod:")) {
        		String id = enhanceID.substring("gearmod:".length());
        		ItemEnhancement master = ShadowrunCore.getItemEnhancement(id);
         		if (master!=null) {
         			for (ItemEnhancementValue mod : selectedItem.getEnhancements()) {
         				if (mod.getModifyable()==master) {
         					selectedItem.removeEnhancement(mod);
               				refresh();
        				}
         			}
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	//--------------------------------------------------------------------
	/**
	 * @param event
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		logger.warn("RCV "+event+"-----------------------------------");
		switch (event.getType()) {
		case EQUIPMENT_ADDED:
		case EQUIPMENT_CHANGED:
		case EQUIPMENT_REMOVED:
			refresh();
			break;
		default:
		}
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ManagedDialog#onClose(org.prelle.javafx.CloseType)
	 */
	@Override
	public void onClose(CloseType type) {
		logger.debug("close");
		GenerationEventDispatcher.removeListener(this);
	}

}

class AccessoriesTab extends Tab {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".items");
	
	private static String STATE_IMPOSSIBLE = "embed-impossible";
	private static String STATE_AFTER_REMOVE = "embed-after-remove";
	private static String STATE_POSSIBLE = "embed-possible";
	private static Map<EmbedOption,String> STYLES = new HashMap<>();

	private CharacterController control;
	private CarriedItem selectedItem;
	private ScreenManagerProvider provider;
	private EditCarriedItemDialog parent;
	
	private ChoiceBox<ItemSubType> cbSubTypes;
	private ListView<ItemTemplate> lvAvailable;
	private VBox bxSelected;
	private Map<ItemSubType, List<ItemTemplate>> mapByType;

	private SimpleObjectProperty<HardcopyPluginData> selected;
	private Map<ItemHook, AvailableSlotPane> panes = new HashMap<>();
	
	static {
		STYLES.put(EmbedOption.NOT_POSSIBLE, STATE_IMPOSSIBLE);
		STYLES.put(EmbedOption.AFTER_REMOVING, STATE_AFTER_REMOVE);
		STYLES.put(EmbedOption.POSSIBLE, STATE_POSSIBLE);
	}
	
	//--------------------------------------------------------------------
	public AccessoriesTab(CharacterController ctrl, CarriedItem item, ScreenManagerProvider prov, EditCarriedItemDialog parent) {
		super(ItemSubType.ACCESSORY.getName());
		this.control = ctrl;
		this.selectedItem = item;
		this.provider = prov;
		this.parent   = parent;
		selected = new SimpleObjectProperty<>();
		initComponents();
		initLayout();
		initInteractivtiy();
		refresh();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		mapByType = new HashMap<>();

		cbSubTypes = new ChoiceBox<ItemSubType>();
		cbSubTypes.setMaxWidth(Double.MAX_VALUE);
		cbSubTypes.setConverter(new StringConverter<ItemSubType>() {
			public String toString(ItemSubType data) {return (data!=null)?data.getName():"null";}
			public ItemSubType fromString(String data) {return null;}
		});

		lvAvailable = new ListView<ItemTemplate>();
		lvAvailable.setStyle("-fx-min-width: 23em;");
		lvAvailable.setCellFactory( lv -> new ItemTemplateListCell2(control.getCharacter(), control.getEquipmentController(), true, selectedItem, null));
//		lvAvailable.setPlaceholder(new Label(UI.getString("list.available.placeholder")));

		
		bxSelected = new VBox(20);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		VBox colAvailable = new VBox(10, cbSubTypes, lvAvailable);
		HBox layout = new HBox(20, colAvailable, bxSelected);
		this.setContent(layout);
	}

	//--------------------------------------------------------------------
	private void initInteractivtiy() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			selected.set(n);
			panes.forEach( (hook,pane) -> pane.getStyleClass().clear());
			if (n==null)
				return;
			Map<ItemHook,EmbedOption> states = control.getEquipmentController().getEmbeddingSlotState(selectedItem, n);
			logger.info("Slot states: "+states);
			panes.keySet().forEach(slot -> {
				EmbedOption opt = states.get(slot);
				if (opt==null) opt=EmbedOption.NOT_POSSIBLE;
				panes.get(slot).getStyleClass().add(STYLES.get(opt));
			});
			
		});		
		
		cbSubTypes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			lvAvailable.getItems().clear();
			if (n!=null) {
				lvAvailable.getItems().addAll( mapByType.get(n));
			}
		});
	}
	
	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<HardcopyPluginData> selectedItemProperty() {
		return selected;
	}

	//--------------------------------------------------------------------
	private void recalculeTypeMaps() {
		logger.info("RecalculateTypeMaps for "+selectedItem);
		// Refresh map of items
		mapByType.clear();
		for (ItemTemplate templ : control.getEquipmentController().getAvailableAccessoriesFor(selectedItem)) {
			boolean oldXMLDataAdding = true;
			for (UseAs usage : templ.getUseAs()) {
				// Is required slot present in item
				if (!selectedItem.hasHook(usage.getSlot()))
					continue;
				ItemSubType sub = usage.getSubtype();
				if (sub!=null) {
					List<ItemTemplate> list = mapByType.get(sub);
					if (list==null) {
						list = new ArrayList<>();
						mapByType.put(sub, list);
					}
					if (!list.contains(templ))
						list.add(templ);
					oldXMLDataAdding = false;
				}
			}
			// The following should only be used for items missing a <useas> element
			if (oldXMLDataAdding) {
				ItemSubType sub = templ.getSubtype(null);
				if (sub==null) {
					// Cannot find a matching place to sort the item, since the XML data is missing a subtype
					logger.error("Missing subtype for "+templ.getId());
					System.err.println("EditCarriedItemDialog: Missing subtype for "+templ.getId());
					// Sort it in the first slot you find
					if (!mapByType.isEmpty())
						mapByType.values().iterator().next().add(templ);
				} else {
					List<ItemTemplate> list = mapByType.get(sub);
					if (list==null) {
						list = new ArrayList<>();
						mapByType.put(sub, list);
					}
					list.add(templ);
				}
			}
		}
		
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.warn("AccessoryTab.refresh***********************************************");
		recalculeTypeMaps();
		
		// Now refresh categories
		List<ItemSubType> list = new ArrayList<>(mapByType.keySet());
		try {
			Collections.sort(list);
		} catch (Exception e) {
			logger.error("Failed sorting",e);
		}
		cbSubTypes.getItems().clear();
		cbSubTypes.getItems().addAll(list);
		lvAvailable.getItems().clear();
		if (!list.isEmpty())
			cbSubTypes.getSelectionModel().select(0);
		
		
//		lvAvailable.getItems().addAll(control.getEquipmentController().getAvailableAccessoriesFor(selectedItem));
		
		bxSelected.getChildren().clear();
		panes.clear();
		for (AvailableSlot slot : selectedItem.getSlots()) {
			AvailableSlotPane pane = new AvailableSlotPane(control, provider, parent);
			pane.setData(selectedItem, slot);
			panes.put(slot.getSlot(), pane);
			bxSelected.getChildren().add(pane);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Update the list view depending on the current type selection
	 */
	public void refreshAvailableContent() {
		lvAvailable.getItems().clear();
		if (cbSubTypes.getValue()!=null) {
			lvAvailable.getItems().addAll( mapByType.get(cbSubTypes.getValue()));
		}
	}
	
}

class ModificationsTab extends Tab {
	
	private CharacterController control;
	private CarriedItem selectedItem;
	
	private ListView<ItemEnhancement> lvAvailable;
	private ListView<ItemEnhancementValue> lvSelected;

	private SimpleObjectProperty<HardcopyPluginData> selected;

	//--------------------------------------------------------------------
	public ModificationsTab(CharacterController ctrl, CarriedItem item) {
		super(ItemSubType.MODIFICATION.getName());
		this.control = ctrl;
		this.selectedItem = item;
		selected = new SimpleObjectProperty<>();
		initComponents();
		initLayout();
		initInteractivtiy();
		refresh();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lvAvailable = new ListView<ItemEnhancement>();
		lvAvailable.setStyle("-fx-min-width: 23em;");
		lvSelected  = new ListView<ItemEnhancementValue>();
		lvSelected .setStyle("-fx-min-width: 23em;");
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		HBox layout = new HBox(20, lvAvailable, lvSelected);
		this.setContent(layout);		
	}

	//--------------------------------------------------------------------
	private void initInteractivtiy() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> selected.set(n));		
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null)
				selected.set(n.getModifyable());
			else
				selected.set(null);
		});
	}
	
	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<HardcopyPluginData> selectedItemProperty() {
		return selected;
	}

	//--------------------------------------------------------------------
	public void refresh() {
		lvAvailable.getItems().clear();
		lvAvailable.getItems().addAll(control.getEquipmentController().getAvailableEnhancementsFor(selectedItem));
	}

}

package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.ShadowrunTools;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class RitualDescriptionPane extends VBox {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(RitualSelectionPane.class.getName());
	
	private Label descHeading;
	private Label descRef;
	private Label descFeat;
	private Label descThr;
	private Label description;

	//-------------------------------------------------------------------
	public RitualDescriptionPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		descFeat    = new Label();
		descThr     = new Label();
		
		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblThr  = new Label(Resource.get(UI, "label.threshold")+":");
		lblThr.setStyle("-fx-font-weight: bold");
		GridPane descGrid = new GridPane();
		descGrid.add( lblThr , 0, 0);
		descGrid.add(descThr , 1, 0);
		descGrid.setHgap(5);
		
		setSpacing(10);
		getChildren().addAll(descHeading, descRef, descFeat, descGrid, description);
		VBox.setVgrow(description, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	public void setData(Ritual data) {
		if (data==null) {
			descHeading.setText(null);
			descRef.setText(null);
			descFeat.setText(null);
			descThr .setText(null);
			description.setText(null);
		} else {
			descHeading.setText(data.getName().toUpperCase());
			descRef.setText(data.getProductName()+" "+data.getPage());
			descFeat.setText(ShadowrunTools.makeFeatureString(data));
			descThr .setText(data.getThreshold());
			description.setText(data.getHelpText());
		}
	}

}

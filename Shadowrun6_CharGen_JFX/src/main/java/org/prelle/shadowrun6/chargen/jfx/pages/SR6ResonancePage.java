package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.ComplexFormSection;
import org.prelle.shadowrun6.chargen.jfx.sections.MetamagicOrEchoesSection;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan Prelle
 *
 */
public class SR6ResonancePage extends SR6ManagedScreenPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6ResonancePage.class.getName());

	private ComplexFormSection complexForms;
	private MetamagicOrEchoesSection echoes;

	//-------------------------------------------------------------------
	public SR6ResonancePage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-resonance");
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;

		initComponents();
		initInteractivity2();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		complexForms = new ComplexFormSection(Resource.get(UI, "section.complexforms"), control, provider);
		echoes       = new MetamagicOrEchoesSection(Resource.get(UI, "section.submersion"), control, provider);
		echoes.setStyle("-fx-min-width: 25em");
		DoubleSection sect = new DoubleSection(complexForms, echoes);
		getSectionList().add(sect);

		// Interactivity
		complexForms.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		echoes.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initLine1();
	}

	//-------------------------------------------------------------------
	protected void initInteractivity2() {
		complexForms.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				echoes.getSelectionModel().clearSelection();
			}
		});
		echoes.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				complexForms.getSelectionModel().clearSelection();
			}
		});
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");
		super.refresh();
		this.setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.resonance"));

		complexForms.refresh();
		echoes.refresh();
		
		MagicOrResonanceType magic = control.getCharacter().getMagicOrResonanceType();
		if (magic!=null) {
			if (magic.usesResonance()) {
				complexForms.setVisible(true);
			} else if (magic.usesPowers()) {
				complexForms.setVisible(false);
			}
		}
		complexForms.getToDoList().clear();
		for (ToDoElement tmp : control.getComplexFormController().getToDos()) {
			complexForms.getToDoList().add(tmp);
		}
		echoes.getToDoList().clear();
		for (ToDoElement tmp : control.getMetamagicOrEchoController().getToDos()) {
			echoes.getToDoList().add(tmp);
		}
	}

}

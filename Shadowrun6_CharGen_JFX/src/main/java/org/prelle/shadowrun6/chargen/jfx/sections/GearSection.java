package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun6.chargen.jfx.listcells.BigCarriedItemListCell;
import org.prelle.shadowrun6.common.SR6ConfigOptions;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.items.ItemUtilJFX;

import de.rpgframework.ConfigOption;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class GearSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(GearSection.class.getName());

	private CharacterController ctrl;
	private ItemType[] allowedItemTypes;
	protected ListView<CarriedItem> lvSelected;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public GearSection(String title, CharacterController ctrl, ScreenManagerProvider provider, ItemType... allowedItemTypes) {
		super(provider, title.toUpperCase(), null);
		if (provider==null)
			throw new NullPointerException("ScreenManagerProvider");
		this.ctrl = ctrl;
		this.allowedItemTypes = allowedItemTypes;
		initComponents();
		initCellFactories();
		initLayout();
		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initComponents() {
		lvSelected  = new ListView<>();
		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
		getDeleteButton().setDisable(true);
		setSettingsButton( new Button(null, new SymbolIcon("setting")) );

		Label phSelected = new Label(UI.getString("equipmentlistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvSelected.setPlaceholder(phSelected);
	}

	//--------------------------------------------------------------------
	protected  void initCellFactories() {
		lvSelected.setCellFactory(new Callback<ListView<CarriedItem>, ListCell<CarriedItem>>() {
			public ListCell<CarriedItem> call(ListView<CarriedItem> param) {
				BigCarriedItemListCell cell = new BigCarriedItemListCell(ctrl, lvSelected, getManagerProvider());
//				cell.setStyle("-fx-max-width: 28em; -fx-pref-width: 26em");
				return cell;
			}
		});
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvSelected.setStyle("-fx-min-width: 22em; -fx-pref-width: 29em");
		this.setMaxHeight(Double.MAX_VALUE);

		setContent(lvSelected);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		getSettingsButton().setOnAction(ev -> onSettings());

//		selector.selectedItemProperty().addListener( (ov,o,n) -> templateSelectionChanged(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> {
			getDeleteButton().setDisable(n==null);
			if (n!=null) {
				showHelpFor.set(n.getItem());
			}
		});
	}

	//-------------------------------------------------------------------
	private void onSettings() {
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : ctrl.getEquipmentController().getConfigOptions()) {
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(UI,  "dialog.settings.title"), content);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		lvSelected.getItems().clear();
		List<CarriedItem> list = ctrl.getCharacter().getItems(true, allowedItemTypes);
		if ((Boolean)SR6ConfigOptions.HIDE_AUTOGEAR.getValue()) {
			list = list.stream().filter(item -> !item.isCreatedByModification()).collect(Collectors.toList());
		}
		
		lvSelected.getItems().addAll(list);
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<CarriedItem> getSelectionModel() {
		return lvSelected.getSelectionModel();
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening gear selection dialog");
		
		EquipmentSelector selector = new EquipmentSelector(ctrl, allowedItemTypes);
//		SelectorWithHelp<ItemTemplate> pane = new SelectorWithHelp<ItemTemplate>(selector);
		ManagedDialog dialog = new ManagedDialog(UI.getString("selectiondialog.title"), selector, CloseType.OK, CloseType.CANCEL);
		
		logger.info("Show an EquipmentSelector");
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.debug("Closed with "+close);
		if (close==CloseType.OK) {
			ItemTemplate selected = selector.getTemplate(); //selector.getSelectedItem();
			logger.debug("Selected gear: "+selected);
			if (selected!=null) {
				ItemType useAs = selected.getNonAccessoryType(allowedItemTypes);
				CarriedItem item = null;
				List<SelectionOptionType> options = ctrl.getEquipmentController().getOptions(selected, selected.getDefaultUsage());
				logger.debug("Required options for "+selected.getId()+": "+options);
				if (!options.isEmpty()) {
					SelectionOption[] opts = ItemUtilJFX.askOptionsFor( managerProvider, ctrl.getEquipmentController(), selected, null, useAs, 18, selected.getDefaultUsage());
					if  (opts!=null)
						item = ctrl.getEquipmentController().select(selected, opts);
				} else 
					item = ctrl.getEquipmentController().select(selected);
				if (item==null) {
					getManagerProvider().getScreenManager().showAlertAndCall(AlertType.ERROR, 
							Resource.get(UI, "selectiondialog.failed.title"), Resource.format(UI, "selectiondialog.failed.format",selected.getName()));
				}
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		CarriedItem selected = lvSelected.getSelectionModel().getSelectedItem();
		logger.debug("CarriedItem to deselect: "+selected);
		boolean removed = ctrl.getEquipmentController().deselect(selected);
		logger.info("Item removed = "+removed);
		refresh();
	}

}

package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.charctrl.EquipmentController.EmbedOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOption;
import org.prelle.shadowrun6.charctrl.EquipmentController.SelectionOptionType;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditCarriedItemDialog;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.items.ItemUtilJFX;

import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class AvailableSlotPane extends VBox {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(EditCarriedItemDialog.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".items");
	
	private CharacterController charControl;
	private EquipmentController control;
	private ScreenManagerProvider provider;
	private EditCarriedItemDialog parent;
	
	private Label lbLoc;
	private Label lbCap;
	private VBox  bxContent;
	
	private transient CarriedItem container;
	private transient AvailableSlot slot;	

	//--------------------------------------------------------------------
	public AvailableSlotPane(CharacterController ctrl, ScreenManagerProvider prov, EditCarriedItemDialog parent) {
		if (prov==null)
			throw new NullPointerException("ScreenManagerProvider");
		this.charControl = ctrl;
		this.control = ctrl.getEquipmentController();
		this.provider= prov;
		this.parent  = parent;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}
	
	//--------------------------------------------------------------------
	private void initComponents() {
		lbLoc = new Label();
		lbCap = new Label();
		bxContent = new VBox(5);
	}
	
	//--------------------------------------------------------------------
	private void initStyle() {
		lbLoc.getStyleClass().add("base");
//		lbLoc.setStyle("-fx-text-fill: textcolor-highlight-primary; ");
		bxContent.setStyle("-fx-spacing: 0.5em; -fx-padding: 0.2em 0.7em 0.3em 0.5em");
		
		this.setStyle("-fx-border-color: black; -fx-border-radius: 1em; -fx-background-radius: 1em;");
		this.setFillWidth(true);
	}
	
	//--------------------------------------------------------------------
	private void initLayout() {
		Region buf = new Region();
		buf.setStyle("-fx-pref-width: 0.5em");
		HBox line1 = new HBox(5, buf, lbLoc, lbCap);
		line1.setStyle("-fx-border-style: none none solid none; -fx-border-width: 0 0 1px 0; -fx-border-color: #C0C0C0; -fx-padding: 0.2em 0.1em 0.2em 0");
		line1.setMaxWidth(Double.MAX_VALUE);
		
		getChildren().addAll(line1, bxContent);
	}
	
	//--------------------------------------------------------------------
	private void initInteractivity() {
		setOnDragOver(ev -> dragOver(ev));
		setOnDragDropped(ev -> droppedOn(ev));
	}
	
	//--------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh "+slot+" / "+slot.getAllEmbeddedItems());
		lbLoc.setText(slot.getSlot().getName());
		lbCap.setVisible(slot.getSlot().hasCapacity());
		lbCap.setManaged(slot.getSlot().hasCapacity());
		lbCap.setText(Resource.format(UI, "availableslotpane.free_capacity",slot.getFreeCapacity(),slot.getCapacity()));
		
		bxContent.getChildren().clear();
		for (CarriedItem item : slot.getAllEmbeddedItems()) {
			Label lbName = new Label(item.getNameWithRating());
			lbName.setMaxWidth(Double.MAX_VALUE);
			lbName.setStyle("-fx-font-size: 120%");
			// Delete button
			FontIcon icoPopup  = new FontIcon("\uE17E\uE107");
			Button btnDel = new Button(null, icoPopup);
			btnDel.getStyleClass().add("mini-button");
			btnDel.setVisible(!item.isCreatedByModification());
//			btnDel.setOnAction(ev -> {
//				logger.debug("remove accessory "+item+" from "+container.getName());
//				if (control.remove(container, item)) {
//					slot.removeEmbeddedItem(item);
//					refresh();
//				}
//			});
			// Context menu
			FontIcon icoDelete = new FontIcon("\uE17E\uE107");
			FontIcon icoSell   = new FontIcon("\uE17E \u00A5");
			FontIcon icoUndo   = new FontIcon("\uE17E\uE10E");
			icoDelete.setStyle("-fx-padding: 3px");
			icoSell.setStyle("-fx-padding: 3px");
			icoUndo.setStyle("-fx-padding: 3px");
			MenuItem menUndo = new MenuItem(Resource.get(UI, "label.removeitem.undo"), icoUndo);
			MenuItem menSell = new MenuItem(Resource.get(UI, "label.removeitem.sell"), icoSell);
			MenuItem menDelete = new MenuItem(Resource.get(UI, "label.removeitem.trash"), icoDelete);
			ContextMenu menu = new ContextMenu();
			menu.getItems().add(menUndo);
			menu.getItems().add(menSell);
			menu.getItems().add(menDelete);
			btnDel.setContextMenu(menu);
//			btnPopup.setOnMouseEntered(event -> {
//				logger.debug("onMouseEntered");
//				menu.show(btnPopup, Side.BOTTOM, 0, 0);
//				});

			btnDel.setOnAction(event -> menu.show(btnDel, Side.BOTTOM, 0, 0));
			menDelete.setOnAction(event -> { logger.info("Trash "+item); control.sell(item, 0f); refresh();});
			menSell  .setOnAction(event -> { logger.info("Sell  "+item); control.sell(item, 0.5f); refresh();});
			menUndo  .setOnAction(event -> { logger.info("Undo  "+item); control.deselect(item); refresh();});

			
			HBox line = new HBox(15, lbName, btnDel);
			line.setAlignment(Pos.CENTER_LEFT);
			HBox.setHgrow(lbName, Priority.ALWAYS);
			if (item.getItem().getSlots().size()>0) {
				FontIcon icoEdit  = new FontIcon("\uE17E\uE104");
				Button btnEdit = new Button(null, icoEdit);
				btnEdit.getStyleClass().add("mini-button");
				btnEdit.setOnAction(ev -> {
					logger.warn("TODO: edit accessory "+item+" in "+container.getName());
					EditCarriedItemDialog dialog = new EditCarriedItemDialog(charControl, item, provider);
					CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());
					if (result==CloseType.OK) {
						this.requestLayout();
					}
				});
				line.getChildren().add(1, btnEdit);
			}
			
			bxContent.getChildren().add(line);
		}
		// Minimum content line
		if (slot.getAllEmbeddedItems().isEmpty()) {
			Label empty = new Label(Resource.get(UI, "availableslot.empty"));
			bxContent.getChildren().add(empty);
		}
	}
	
	//--------------------------------------------------------------------
	public void setData(CarriedItem container, AvailableSlot data) {
		this.container = container;
		this.slot      = data;
		
		refresh();
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void dragOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			String enhanceID = event.getDragboard().getString();
			// Get reference for ID
			if (enhanceID.startsWith("gear:")) {
        		String id = enhanceID.substring("gear:".length());
        		ItemTemplate master = ShadowrunCore.getItem(id);
        		
          		if (master!=null && control.canBeEmbedded(container, master, slot.getSlot()) && control.canBePayed(master, true)) {
    				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);         			
         		}
			}
		}
	}

	//-------------------------------------------------------------------
	/*
	 * Select
	 */
	private void droppedOn(DragEvent event) {
       /* if there is a string data on dragboard, read it and use it */
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasString()) {
            String enhanceID = db.getString();
        	// Get reference for ID
        	if (enhanceID.startsWith("gear:")) {
        		String id = enhanceID.substring("gear:".length());
        		ItemTemplate master = ShadowrunCore.getItem(id);
         		if (master!=null) {
         			logger.debug("embed "+master+" in "+container);
         			UseAs usage = master.getUsageFor(slot.getSlot());
         			logger.info("use for slot "+slot.getSlot()+" is "+usage);
         			ItemType useAs = (usage==null)?master.getNonAccessoryType():usage.getType();
         			logger.info("ItemType is "+useAs);
         			List<SelectionOptionType> options = control.getOptions(master, usage);
    				try {
						if (!options.isEmpty()) {
							Platform.runLater( () -> {
								SelectionOption[] opts = ItemUtilJFX.askOptionsFor( provider, control, master, container, useAs, slot.getFreeCapacity(), usage);
								CarriedItem added = control.embed(container, master, slot.getSlot(), opts);
								afterTryingToAdd(master, added);
							});
						} else {         			
		    				CarriedItem added = control.embed(container, master, slot.getSlot());
							afterTryingToAdd(master, added);
						}
					} catch (Exception e) {
						logger.error("Failed asking for Options",e);
						StringWriter out = new StringWriter();
						e.printStackTrace(new PrintWriter(out));
						BabylonEventBus.fireEvent(BabylonEventType.UI_MESSAGE,2,out.toString());
					}
        		}
        	}
        }
        /* let the source know whether the string was successfully
         * transferred and used */
        event.setDropCompleted(success);

        event.consume();
	}

	private void afterTryingToAdd(ItemTemplate master, CarriedItem added) {
		if (added==null) {
			provider.getScreenManager().showAlertAndCall(AlertType.ERROR, 
				Resource.get(UI, "selectiondialog.failed.title"), Resource.format(UI, "selectiondialog.failed.format",master.getName()));
		}
			logger.debug("embedding "+master+" in "+container+" returned "+added);
			if (added!=null) {
				refresh();
				if (parent!=null)
					parent.refreshAccessoryContent();
			}
		
	}
}
	
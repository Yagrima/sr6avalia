package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.SkillController;
import org.prelle.shadowrun6.chargen.jfx.listcells.KnowledgeSkillValueListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.SkillValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class SkillSelectionPane extends HBox {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".skills");

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSelectionPane.class.getName());

	private CharacterController charGen;
	private SkillController ctrl;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ChoiceBox<Skill> cbAvailNormal;
	private ChoiceBox<Skill> cbAvailKnowl;
	private Button btnAddNorm;
	private Button btnAddKnow;
	private Button btnDelNorm;
	private Button btnDelKnow;
	private Label hdAvailable;
	private Label hdSelected;
	private ListView<SkillValue> lvNormal;
	private ListView<SkillValue> lvKnowledge;
	private boolean ignoreRefresh;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<BasePluginData>();

	//--------------------------------------------------------------------
	public SkillSelectionPane(CharacterController charGen, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.ctrl = charGen.getSkillController();
		this.provider = provider;
		initCompoments();
		initLayout();
		initInteractivity();
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		hdAvailable = new Label(Resource.get(UI, "label.skills"));
		hdSelected  = new Label(Resource.get(UI, "label.knowledgeskills"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdSelected.setStyle("-fx-font-size: 130%");

		cbAvailNormal = new ChoiceBox<>();
		cbAvailNormal.setConverter(new StringConverter<Skill>() {
			public String toString(Skill type) { return type.getName();	}
			public Skill fromString(String arg0) {return null;}
		});
		cbAvailNormal.getItems().addAll(ctrl.getAvailableSkills());
		cbAvailNormal.getItems().removeAll(ctrl.getAvailableSkills(SkillType.LANGUAGE));
		cbAvailNormal.getItems().removeAll(ctrl.getAvailableSkills(SkillType.KNOWLEDGE));
		cbAvailNormal.getSelectionModel().select(0);
		cbAvailKnowl = new ChoiceBox<>();
		cbAvailKnowl.setConverter(new StringConverter<Skill>() {
			public String toString(Skill type) { return type.getName();	}
			public Skill fromString(String arg0) {return null;}
		});
		cbAvailKnowl.getItems().addAll(ctrl.getAvailableSkills(SkillType.LANGUAGE));
		cbAvailKnowl.getItems().addAll(ctrl.getAvailableSkills(SkillType.KNOWLEDGE));
		cbAvailKnowl.getSelectionModel().select(0);
		
		btnAddNorm = new Button("\uE0C5");
		btnAddNorm.getStyleClass().add("mini-button");
		btnAddKnow = new Button("\uE0C5");
		btnAddKnow.getStyleClass().add("mini-button");
		btnDelNorm = new Button("\uE0C6");
		btnDelNorm.getStyleClass().add("mini-button");
		btnDelNorm.setDisable(false);
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		lvNormal    = new ListView<>();
		lvKnowledge = new ListView<>();
		
		lvNormal.setUserData(this);
		lvKnowledge.setUserData(this);

		lvNormal.setCellFactory(new Callback<ListView<SkillValue>, ListCell<SkillValue>>() {
			public ListCell<SkillValue> call(ListView<SkillValue> param) {
				return new SkillValueListCell(charGen, lvNormal);
			}
		});
		lvKnowledge.setCellFactory(new Callback<ListView<SkillValue>, ListCell<SkillValue>>() {
			public ListCell<SkillValue> call(ListView<SkillValue> param) {
				return new KnowledgeSkillValueListCell(charGen, provider);
			}
		});

		Label phSelected = new Label(Resource.get(UI,"skillvaluelistview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		lvKnowledge.setPlaceholder(phSelected);
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		lvNormal   .setStyle("-fx-pref-width: 24em;");
		lvKnowledge.setStyle("-fx-pref-width: 17em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvNormal, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		HBox lineNorm = new HBox(5, cbAvailNormal, btnAddNorm, grow1, btnDelNorm);
		column1.getChildren().addAll(hdAvailable, lineNorm, lvNormal);

		VBox column2 = new VBox(10);
		VBox.setVgrow(lvKnowledge, Priority.ALWAYS);
		Region grow2 = new Region();
		grow2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow2, Priority.ALWAYS);
		HBox lineKnow = new HBox(5, cbAvailKnowl, btnAddKnow,  grow2, btnDelKnow);
		column2.getChildren().addAll(hdSelected, lineKnow, lvKnowledge);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		lvNormal.setMaxHeight(Double.MAX_VALUE);
		lvKnowledge.setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		lvNormal.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> skillSelectionChanged(lvNormal, n));
		lvKnowledge.getSelectionModel().selectedItemProperty().addListener( (ov,o,n)  -> skillSelectionChanged(lvKnowledge, n));
		lvKnowledge.setOnDragDropped(event -> dragFromSkillDropped(event));
		lvKnowledge.setOnDragOver(event -> dragFromSkillOver(event));
		
		cbAvailNormal.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> btnAddNorm.setDisable(n==null));
		cbAvailKnowl.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> btnAddKnow.setDisable(n==null));
		btnAddNorm.setOnAction(ev -> {
			String customName = null;
//			if (cbAvailNormal.getValue().isToSpecify()) {
//				customName = askForName();
//			}
			SkillValue val = ctrl.select(cbAvailNormal.getValue());
			if (val!=null && customName!=null)
				val.setName(customName);
		});
		btnDelNorm.setOnAction(ev -> ctrl.deselect(lvNormal.getSelectionModel().getSelectedItem()));
		btnAddKnow.setOnAction(ev -> ctrl.select(cbAvailKnowl.getValue()));
		btnDelKnow.setOnAction(ev -> ctrl.deselect(lvKnowledge.getSelectionModel().getSelectedItem()));
	}

	//--------------------------------------------------------------------
	private String askForName() {
		Label explain = new Label(Resource.get(UI, "namedialog.explain"));
		
		TextField tfName = new TextField();
		tfName.setPrefColumnCount(20);
		VBox layout = new VBox(20, explain, tfName);
		
		CloseType closed = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, Resource.get(UI, "namedialog.title"), layout);
		if (closed==CloseType.OK) {
			return tfName.getText();
		}
		return null;
	}
	
	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> selectedItemProperty() {
		return showHelpFor;
	}

	//--------------------------------------------------------------------
	private void skillSelectionChanged(ListView<SkillValue> list, SkillValue data) {
		if (data!=null) {
			logger.debug("Selected "+data+"   - canBeDeselected="+ctrl.canBeDeselected(data));
			showHelpFor.set(data.getModifyable());
			if (list==lvNormal)
				btnDelNorm.setDisable(!ctrl.canBeDeselected(data));
			else
				btnDelKnow.setDisable(!ctrl.canBeDeselected(data));
		} else {
			showHelpFor.set(null);
			if (list==lvNormal)
				btnDelNorm.setDisable(true);
			else
				btnDelKnow.setDisable(true);
		}
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		logger.debug("refresh     ignore="+ignoreRefresh);
		if (ignoreRefresh)
			return;
		lvNormal.getItems().clear();
		lvNormal.getItems().addAll(model.getSkillValues(SkillType.ACTION, SkillType.COMBAT, SkillType.MAGIC, SkillType.PHYSICAL, SkillType.RESONANCE, SkillType.SOCIAL, SkillType.TECHNICAL, SkillType.VEHICLE));

		lvKnowledge.getItems().clear();
		lvKnowledge.getItems().addAll(model.getSkillValues(SkillType.LANGUAGE, SkillType.KNOWLEDGE));
		
		cbAvailNormal.getItems().clear();
		cbAvailNormal.getItems().addAll(ctrl.getAvailableSkills());
		cbAvailNormal.getItems().removeAll(ctrl.getAvailableSkills(SkillType.LANGUAGE));
		cbAvailNormal.getItems().removeAll(ctrl.getAvailableSkills(SkillType.KNOWLEDGE));
		cbAvailNormal.getSelectionModel().select(0);
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private void dragFromSkillDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("skill")) {
					Skill data = ShadowrunCore.getSkill(tail);
					if (data==null) {
						logger.warn("Cannot find skill for dropped skill id '"+tail+"'");
					} else {
						event.setDropCompleted(success);
						event.consume();
						// Special handling for knowledge and language skills
						if (data.getType()==SkillType.KNOWLEDGE || data.getType()==SkillType.LANGUAGE) {
							logger.debug(data.getName()+" must be entered by user");
							Platform.runLater(new Runnable(){
								public void run() {
									String name = askForName();
									logger.debug("returned "+name);
									if (name!=null) {
										ctrl.selectKnowledgeOrLanguage(data, name);
									} else
										logger.debug("Ignore knowledge or language skill because user did not enter a name");
								}});
						} else {
							ctrl.select(data);
						}
						return;
					}
				}
			}
		}
		/* let the source know whether the string was successfully
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromSkillOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			/* allow for both copying and moving, whatever user chooses */
			event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @param ignoreRefresh the ignoreRefresh to set
	 */
	public void setIgnoreRefresh(boolean ignoreRefresh) {
		logger.info("********Ignore refresh = "+ignoreRefresh);
		this.ignoreRefresh = ignoreRefresh;
	}

}

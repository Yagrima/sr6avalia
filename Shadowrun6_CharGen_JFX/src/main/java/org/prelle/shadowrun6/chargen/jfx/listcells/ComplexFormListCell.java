package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.ResourceBundle;

import org.prelle.shadowrun6.ComplexForm;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class ComplexFormListCell extends ListCell<ComplexForm> {

	private final static ResourceBundle CORE = ShadowrunCore.getI18nResources();

	private Label lblName;
	private Label lblLine1;
	private StackPane stack;
	private ImageView imgRecommended;

	private ComplexForm data;

	//-------------------------------------------------------------------
	public ComplexFormListCell() {
		initComponents();
		initLayout();
		initStyle();

		imgRecommended.setVisible(false);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();
		stack    = new StackPane();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox layout   = new VBox(3);
		layout.getChildren().addAll(lblName, lblLine1);

		// Recommended icon
		imgRecommended = new ImageView(new Image(SR6Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);

		stack.getChildren().addAll(layout, imgRecommended);
		StackPane.setAlignment(imgRecommended, Pos.TOP_RIGHT);
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		setStyle("-fx-pref-width: 16em");
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ComplexForm item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(stack);
			lblName.setText(item.getName().toUpperCase());
			lblLine1.setText(makeFeatureString(item));
		}
	}

	//-------------------------------------------------------------------
	static String makeFeatureString(ComplexForm spell) {
		StringBuffer buf = new StringBuffer();
		buf.append(Resource.get(CORE,"label.duration")+": "+spell.getDuration().getName());
//		buf.append("  \t"+CORE.getString("label.complexform.target")+": "+spell.getTarget().getName());
		return buf.toString();
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("complexform:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}
}
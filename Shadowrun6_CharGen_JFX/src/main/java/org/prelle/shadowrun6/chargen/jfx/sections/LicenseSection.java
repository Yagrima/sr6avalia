package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.AdeptPowerValue;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.LicenseType;
import org.prelle.shadowrun6.LicenseValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.charctrl.CharGenMode;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditConnectionDialog;
import org.prelle.shadowrun6.chargen.jfx.listcells.ConnectionListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.LicenseValueListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.PowerValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ShadowrunJFXUtils;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;

import de.rpgframework.core.License;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class LicenseSection extends GenericListSection<LicenseValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(LicenseSection.class.getName());
	
	private Label note;
//	private Label ptsTotal;
//	private Button btnDec;
//	private Button btnInc;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public LicenseSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
		list.setCellFactory( lv -> {
			LicenseValueListCell cell = new LicenseValueListCell(ctrl, provider);
//			cell.setOnMouseClicked(ev -> {
//				if (ev.getClickCount()==2)
//					onEdit(cell.getItem(), cell);
//			});
			return cell;
		});
		initPlaceholder();
		
		refresh();
		list.setStyle("-fx-pref-height: 25em; -fx-min-width: 22em; -fx-pref-width: 36em");
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(Resource.get(RES, "licensesection.listview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.warn("onAdd");
//		if (!control.getSINController().canCreateNewLicense(SIN.Quality.ANYONE)) {
//			logger.warn("UI connection creation initated, but generator does not allow it");
//			return;
//		}
		
		LicenseSelector selector = new LicenseSelector(control);
		SelectorWithHelp<LicenseType> pane = new SelectorWithHelp<LicenseType>(selector);
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "licensesection.selector.title"), pane, CloseType.OK, CloseType.CANCEL);
		CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, selector.getButtonControl());
		logger.debug("Adding license dialog closed with "+result);
		if (result==CloseType.OK || result==CloseType.APPLY) {
			logger.info("Adding License: "+selector.getSelectedLicenseType()+" for "+selector.getSelectedSIN());
			LicenseValue lic = control.getSINController().createNewLicense(selector.getSelectedLicenseType(), selector.getSelectedSIN(), selector.getSelectedQuality());
			logger.debug("Created license was "+lic);
//			Connection added = control.getConnectionController().createConnection();
//			added.setName(toAdd.getName());
//			added.setType(toAdd.getType());
//			added.setDescription(toAdd.getDescription());
		}
	}

//	//-------------------------------------------------------------------
//	private void onDelete() {
//		AdeptPowerValue selected = list.getSelectionModel().getSelectedItem();
//		logger.debug("Spell to deselect: "+selected);
//		ctrl.getPowerController().deselect(selected);
//		refresh();
//	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<LicenseValue> getSelectionModel() {
		return list.getSelectionModel();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		todos.clear();
		todos.addAll(control.getSINController().getToDos());
		
//		ptsToSpend.setText( String.valueOf( ctrl.getPowerController().getPowerPointsLeft()));
//		ptsTotal.setText( String.valueOf( ctrl.getCharacter().getPowerPoints()));
//		btnDec.setDisable(!ctrl.getPowerController().canDecreasePowerPoints());
//		btnInc.setDisable(!ctrl.getPowerController().canIncreasePowerPoints());
		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getLicenses());
	}

	@Override
	protected void onDelete() {
		// TODO Auto-generated method stub
		
	}

}

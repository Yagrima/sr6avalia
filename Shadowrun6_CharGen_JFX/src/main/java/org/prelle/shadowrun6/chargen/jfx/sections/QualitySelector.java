/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Ritual;
import org.prelle.shadowrun6.Quality.QualityType;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.charctrl.RitualController;
import org.prelle.shadowrun6.chargen.jfx.listcells.QualityListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.RitualListCell;
import org.prelle.shadowrun6.chargen.jfx.panels.RitualDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.RitualSelectionPane;

import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class QualitySelector extends OptionalDescriptionPane {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(QualitySection.class.getName());
	
	private QualityController ctrl;

	private ChoiceBox<Quality.QualityType> cbFilter;
	private ListView<Quality> lvAvailable;
	private DescriptionPane description;
	private Label hdAvailable;
	private Label hdInfo;
	private Button btnDelKnow;

	//-------------------------------------------------------------------
	public QualitySelector(QualityController ctrl) {
		this.ctrl = ctrl;

		initComponents();
		initLayout();
		initInteractivity();
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		hdAvailable = new Label(Resource.get(UI, "label.available"));
		hdInfo  = new Label(Resource.get(UI, "label.info"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdInfo.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdInfo.setStyle("-fx-font-size: 130%");

		/* Column 1 */
		lvAvailable = new ListView<Quality>();
		lvAvailable.setCellFactory(new Callback<ListView<Quality>, ListCell<Quality>>() {
			public ListCell<Quality> call(ListView<Quality> param) {
				QualityListCell cell =  new QualityListCell(ctrl);
//				cell.setOnMouseClicked(event -> {
//					if (event.getClickCount()==2) ctrl.select(cell.getItem());
//				});
				return cell;
			}
		});
		cbFilter = new ChoiceBox<>();
		cbFilter.getItems().addAll(Quality.QualityType.values());
		cbFilter.setConverter(new StringConverter<Quality.QualityType>() {
			public String toString(QualityType object) { return object.getName(); }
			public QualityType fromString(String string) { return null; }
		});
		
		description = new DescriptionPane();
		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		
		Label phAvailable = new Label(Resource.get(UI, "qualitysection.selector.placeholder"));
		phAvailable.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-pref-width: 24em;");
		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, cbFilter, lvAvailable);

		column1.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);

		setChildren(column1, description);

		description.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(description, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> description.setText(n));
		cbFilter.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		if (cbFilter.getValue()!=null) {
			lvAvailable.getItems().addAll(ctrl.getAvailableQualities().stream().filter(qual -> qual.getType()==cbFilter.getValue()).collect(Collectors.toList()));
		} else {
			lvAvailable.getItems().addAll(ctrl.getAvailableQualities());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public Quality getSelected() {
		return lvAvailable.getSelectionModel().getSelectedItem();
	}

}

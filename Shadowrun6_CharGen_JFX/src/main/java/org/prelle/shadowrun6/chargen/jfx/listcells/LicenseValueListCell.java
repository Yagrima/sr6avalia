package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.LicenseValue;
import org.prelle.shadowrun6.SIN;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.SINController;
import org.prelle.shadowrun6.chargen.jfx.sections.LicenseSection;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class LicenseValueListCell extends ListCell<LicenseValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(LicenseSection.class.getName());
	
	private CharacterController control;
	private SINController sinCtrl;
	private ScreenManagerProvider provider;
	
	private Label  lbName;
	private Label  lbDesc;
	
	private VBox layout;

	//-------------------------------------------------------------------
	public LicenseValueListCell(CharacterController control, ScreenManagerProvider provider) {
		this.control = control;
		this.sinCtrl = control.getSINController();
		this.provider= provider;

		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lbName = new Label();
		lbName.getStyleClass().add("base");
		lbDesc = new Label();
		lbDesc.setWrapText(true);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		this.layout = new VBox(lbName, lbDesc);
		this.layout.setAlignment(Pos.CENTER_LEFT);
		this.layout.setStyle("-fx-spacing: 0.2em");
		HBox.setHgrow(layout, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
	}

	//-------------------------------------------------------------------
	public void updateItem(LicenseValue item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			
			lbName.setText(item.getName());
			SIN sin = control.getCharacter().getSIN(item.getSIN()); 
			if (sin!=null) {
				lbDesc.setText(sin.getName());
			} else
				lbDesc.setText("-?-");
		}
	}

}

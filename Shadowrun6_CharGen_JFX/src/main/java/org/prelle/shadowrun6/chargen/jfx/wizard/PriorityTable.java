package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.MagicOrResonanceOption;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.MetaTypeOption;
import org.prelle.shadowrun6.Priority;
import org.prelle.shadowrun6.PriorityOption;
import org.prelle.shadowrun6.PriorityTableEntry;
import org.prelle.shadowrun6.PriorityType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.chargen.PrioritySettings;
import org.prelle.shadowrun6.chargen.PriorityVariant;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.BlendMode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

/**
 * @author prelle
 *
 */
public class PriorityTable extends GridPane implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(PriorityTable.class.getName());
	
	private ToggleButton[] btnMeta;
	private ToggleButton[] btnAttrib;
	private ToggleButton[] btnMagic;
	private ToggleButton[] btnSkill;
	private ToggleButton[] btnResrc;
	
	private ToggleGroup tgMeta;
	private ToggleGroup tgAttrib;
	private ToggleGroup tgMagic;
	private ToggleGroup tgSkill;
	private ToggleGroup tgResrc;
	
	private NewPriorityCharacterGenerator charGen;
	
	//-------------------------------------------------------------------
	public PriorityTable(NewPriorityCharacterGenerator charGen) {
		this.charGen = charGen;
		
		initCompontents();
		initLayout();
		
		update();
		initInteractivity();
		
		GenerationEventDispatcher.addListener(this);
	}
	
	//-------------------------------------------------------------------
	private String getMetatypeString(Priority prio) {
//		if (charGen.getCharacter().getTemporaryChargenSettings(PrioritySettings.class).variant==PriorityVariant.STREET_LEVEL &&  prio.ordinal()<Priority.E.ordinal()) {
//			prio = Priority.values()[prio.ordinal()+1];
//			System.err.println("Reduced");
//		}
		
		PriorityTableEntry entry = ShadowrunCore.getPriorityTableEntry(PriorityType.METATYPE, prio);
		List<String> names = new ArrayList<>();
		entry.forEach(opt -> names.add(((MetaTypeOption)opt).getType().getName()));
		logger.info("names = "+names);
		return String.join(", ", names)+" ("+entry.getAdjustmentPoints()+")";
	}
	
	//-------------------------------------------------------------------
	private String getAttributeString(Priority prio) {
		switch (prio) {
		case A: return "24";
		case B: return "16";
		case C: return "12";
		case D: return "8";
		case E: return "2";
		}
		return "?";
	}
	
	//-------------------------------------------------------------------
	private VBox getMagicString(Priority prio) {
		logger.info("------------"+prio+"--------------");
		PriorityTableEntry entry = ShadowrunCore.getPriorityTableEntry(PriorityType.MAGIC, prio);
//		logger.info("----"+entry);
		VBox ret = new VBox();
		
		int oldPoints = -1;
		StringBuffer buf = null;
		MagicOrResonanceType lastType = null;
		for ( PriorityOption opt : entry ) {
//			logger.info("*******"+opt);
			MagicOrResonanceType type = ((MagicOrResonanceOption)opt).getType();
			int points = ((MagicOrResonanceOption)opt).getValue();
//			logger.info("  type="+type+"  points="+points);
			if (points!=oldPoints) {
//				logger.info(" change from "+oldPoints+" to "+points+"   Buffer is "+buf);
				// Print old
				if (buf!=null) {
					Label part1 = new Label(buf.toString()+":");
					part1.setStyle("-fx-font-weight: bold");
					Label part2 = new Label(Resource.get(RES, (lastType.getId().equals("technomancer")?"label.resonance":"label.magic"))+" "+oldPoints);
					HBox line = new HBox(5, part1, part2);
					ret.getChildren().add(line);
					buf = new StringBuffer(type.getName());
				} else {
					buf = new StringBuffer(type.getName());
				}
			} else {
				// Same points
				if (buf!=null) {
					
				}
				buf.append(", "+type.getName());
			}
			oldPoints = points;
			lastType = type;
		};
//		logger.info(" after loop buffer is "+buf+" and points were "+oldPoints+"   and lastType="+lastType);
		Label part1 = new Label(buf.toString()+":");
		part1.setStyle("-fx-font-weight: bold");
		Label part2 = new Label(Resource.get(RES, (lastType.getId().equals("technomancer")?"label.resonance":"label.magic"))+" "+oldPoints);
		HBox line = new HBox(5, part1, part2);
		if (lastType.getId().equals("mundane"))
			line = new HBox(5, new Label(lastType.getName()));
		ret.getChildren().add(line);
		
		return ret;
	}
	
	//-------------------------------------------------------------------
	private String getSkillString(Priority prio) {
		switch (prio) {
		case A: return "32";
		case B: return "24";
		case C: return "20";
		case D: return "16";
		case E: return "10";
		}
		return "?";
	}
	
	//-------------------------------------------------------------------
	private int getResourcesVal(Priority prio) {
		switch (prio) {
		case A: return 450000;
		case B: return 275000;
		case C: return 150000;
		case D: return 50000;
		case E: return 8000;
		}
		return 0;
	}
	
	//-------------------------------------------------------------------
	private void initCompontents() {
		getStyleClass().add("priority-table");
		
		/* 
		 * Metatypes
		 */
		btnMeta = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			btnMeta[prio.ordinal()] = new ToggleButton(getMetatypeString(prio));
			btnMeta[prio.ordinal()].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnMeta[prio.ordinal()].setUserData(prio);
			btnMeta[prio.ordinal()].getStyleClass().addAll("priobutton","priobutton-metatype");
			btnMeta[prio.ordinal()].setWrapText(true);
		}
		
		tgMeta = new ToggleGroup();
		tgMeta.getToggles().addAll(btnMeta);
		tgMeta.setUserData(PriorityType.METATYPE);

		/* 
		 * Attributes
		 */
		btnAttrib = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			btnAttrib[prio.ordinal()] = new ToggleButton(getAttributeString(prio));
			btnAttrib[prio.ordinal()].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnAttrib[prio.ordinal()].getStyleClass().addAll("priobutton","attribute");
			btnAttrib[prio.ordinal()].setUserData(prio);
		}
		tgAttrib = new ToggleGroup();
		tgAttrib.getToggles().addAll(btnAttrib);
		tgAttrib.setUserData(PriorityType.ATTRIBUTE);

		/*
		 * Magic or resonance
		 */
		btnMagic = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			int i = prio.ordinal();
			btnMagic[i] = new ToggleButton(null, getMagicString(prio));
			btnMagic[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnMagic[i].setUserData(prio);
			btnMagic[i].getStyleClass().addAll("priobutton","magic");
			switch (i) {
			case 0: btnMagic[i].setPrefHeight(100); break;
			case 1: btnMagic[i].setPrefHeight(100); break;
			case 2: btnMagic[i].setPrefHeight(100); break;
			case 3: btnMagic[i].setPrefHeight(80); break;
			case 4: btnMagic[i].setPrefHeight(40); break;
			}
		}
		tgMagic = new ToggleGroup();
		tgMagic.getToggles().addAll(btnMagic);
		tgMagic.setUserData(PriorityType.MAGIC);

		/*
		 * Skills
		 */
		btnSkill = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			int i = prio.ordinal();
			btnSkill[i] = new ToggleButton(getSkillString(prio));
			btnSkill[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnSkill[i].setUserData(prio);
			btnSkill[i].getStyleClass().addAll("priobutton","skill");
		}
		tgSkill = new ToggleGroup();
		tgSkill.getToggles().addAll(btnSkill);
		tgSkill.setUserData(PriorityType.SKILLS);

		/*
		 * Resources
		 */
		btnResrc = new ToggleButton[5];
		for (Priority prio : Priority.values()) {
			int i = prio.ordinal();
			btnResrc[i] = new ToggleButton(Resource.format(RES, "format.nuyen", getResourcesVal(prio)));
			btnResrc[i].setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			btnResrc[i].setUserData(prio);
			btnResrc[i].getStyleClass().addAll("priobutton","resources");
		}
		tgResrc = new ToggleGroup();
		tgResrc.getToggles().addAll(btnResrc);
		tgResrc.setUserData(PriorityType.RESOURCES);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		setGridLinesVisible(true);
		
		/*
		 * Headings
		 */
		// Heading row
		Label lblPrio  = new Label(Resource.get(RES, "priotable.label.prio"));
		Label lblMeta  = new Label(Resource.get(RES, "priotable.label.meta"));
		Label lblAttrib= new Label(Resource.get(RES, "priotable.label.attrib"));
		Label lblMagic = new Label(Resource.get(RES, "priotable.label.magic"));
		Label lblSkill = new Label(Resource.get(RES, "priotable.label.skill"));
		Label lblResrc = new Label(Resource.get(RES, "priotable.label.resrc"));
		lblPrio.getStyleClass().add("table-head");
		lblMeta.getStyleClass().add("table-head");
		lblAttrib.getStyleClass().add("table-head");
		lblMagic.getStyleClass().add("table-head");
		lblSkill.getStyleClass().add("table-head");
		lblResrc.getStyleClass().add("table-head");
		lblPrio.setMaxWidth(Double.MAX_VALUE);
		lblMeta.setMaxWidth(Double.MAX_VALUE);
		lblAttrib.setMaxWidth(Double.MAX_VALUE);
		lblMagic.setMaxWidth(Double.MAX_VALUE);
		lblSkill.setMaxWidth(Double.MAX_VALUE);
		lblResrc.setMaxWidth(Double.MAX_VALUE);
		GridPane.setHgrow(lblPrio, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblMeta, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblAttrib, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblMagic, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblSkill, javafx.scene.layout.Priority.ALWAYS);
		GridPane.setHgrow(lblResrc, javafx.scene.layout.Priority.ALWAYS);
		add(lblPrio  , 0,0);
		add(lblMeta  , 1,0);
		add(lblAttrib, 2,0);
		add(lblMagic , 3,0);
		add(lblSkill , 4,0);
		add(lblResrc , 5,0);
		
		// Heading column
		for (Priority prio : Priority.values()) {
			Label label = new Label(prio.name());
			label.getStyleClass().addAll("text-subheader","row-heading");
			add(label, 0, prio.ordinal()+1);
		}
//		for (int i=0; i<5; i++) {
//			Label label = new Label(String.valueOf((char)('A'+i)));
//			label.getStyleClass().addAll("text-subheader","row-heading");
//			add(label, 0,i+1);
//		}
		
		/*
		 * Content
		 */
		for (int i=0; i<5; i++) add(btnMeta  [i], 1,i+1);
		for (int i=0; i<5; i++) add(btnAttrib[i], 2,i+1);
		for (int i=0; i<5; i++) add(btnMagic [i], 3,i+1);
		for (int i=0; i<5; i++) add(btnSkill [i], 4,i+1);
		for (int i=0; i<5; i++) add(btnResrc [i], 5,i+1);
		
		// Column layout
		ColumnConstraints center = new ColumnConstraints();
		center.setHalignment(HPos.CENTER);
		ColumnConstraints magicOr = new ColumnConstraints();
//		magicOr.setHgrow(javafx.scene.layout.Priority.ALWAYS);
		getColumnConstraints().add(center);
		getColumnConstraints().add(new ColumnConstraints());
		getColumnConstraints().add(new ColumnConstraints());
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		tgMeta.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.METATYPE, (Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgAttrib.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.ATTRIBUTE, (Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgMagic.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.MAGIC, (Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgSkill.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.SKILLS, (Priority) ((ToggleButton)n).getUserData());
			update();
			});
		tgResrc.selectedToggleProperty().addListener((ov,o,n) -> {
			if (n!=null)
				charGen.setPriority(PriorityType.RESOURCES, (Priority) ((ToggleButton)n).getUserData());
			update();
			});
	}
	
	//-------------------------------------------------------------------
	private void update() {
		if (charGen.getPriority(PriorityType.METATYPE)!=null)
			tgMeta.selectToggle(btnMeta[charGen.getPriority(PriorityType.METATYPE).ordinal()]);
		if (charGen.getPriority(PriorityType.ATTRIBUTE)!=null)
			tgAttrib.selectToggle(btnAttrib[charGen.getPriority(PriorityType.ATTRIBUTE).ordinal()]);
		if (charGen.getPriority(PriorityType.MAGIC)!=null)
			tgMagic.selectToggle(btnMagic[charGen.getPriority(PriorityType.MAGIC).ordinal()]);
		if (charGen.getPriority(PriorityType.SKILLS)!=null)
			tgSkill.selectToggle(btnSkill[charGen.getPriority(PriorityType.SKILLS).ordinal()]);
		if (charGen.getPriority(PriorityType.RESOURCES)!=null)
			tgResrc.selectToggle(btnResrc[charGen.getPriority(PriorityType.RESOURCES).ordinal()]);
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case PRIORITY_CHANGED:
			logger.debug("RCV "+event.getType());
			PriorityType option = (PriorityType)event.getKey();
			int priority = ((Priority)event.getValue()).ordinal();
			switch (option) {
			case METATYPE : tgMeta  .selectToggle(btnMeta[priority]); break;
			case ATTRIBUTE: tgAttrib.selectToggle(btnAttrib[priority]); break;
			case MAGIC    : tgMagic .selectToggle(btnMagic[priority]); break;
			case SKILLS   : tgSkill .selectToggle(btnSkill[priority]); break;
			case RESOURCES: tgResrc .selectToggle(btnResrc[priority]); break;
			}
		}
	}
	
}

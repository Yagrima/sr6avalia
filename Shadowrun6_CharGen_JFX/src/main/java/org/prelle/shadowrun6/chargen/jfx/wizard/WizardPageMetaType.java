/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.HelpTextPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.MetaType;
import org.prelle.shadowrun6.MetaTypeOption;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.MetatypeController;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ShadowrunJFXUtils;
import org.prelle.shadowrun6.modifications.AttributeModification;

import de.rpgframework.genericrpg.ToDoElement.Severity;
import de.rpgframework.genericrpg.modification.Modification;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author prelle
 *
 */
public class WizardPageMetaType extends WizardPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".metatype");

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageMetaType.class.getName());

	private MetatypeController charGen;
	private ShadowrunCharacter model;

	private static Map<MetaType,Image> imageByRace;

	private Label lbExplain;
//	private FreePointsNode karma;
	private ListView<MetaTypeOption> metaList;
	private HelpTextPane description;
	private VBox bxQualities;
	private Label bxAttributes;
	private VBox content;
	private WizardPointsPane side;

	private boolean ignoreGUI;
	private Label lblToDosHead;
	private Label lblToDos;
	
	//-------------------------------------------------------------------
	static {
		imageByRace = new HashMap<MetaType, Image>();
	}

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public WizardPageMetaType(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen.getMetatypeController();
		if (this.charGen==null)
			throw new NullPointerException("Metatype controller not set");
		model = charGen.getCharacter();

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		if (charGen.getCharacter().getMetatype()!=null) {
			// Find matching option
			for (MetaTypeOption opt : metaList.getItems()) {
				if (opt.getType()==charGen.getCharacter().getMetatype()) {
					metaList.getSelectionModel().select(opt);
					break;
				}
			}
		} else
			metaList.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(Resource.get(UI, "wizard.selectMetatype.title"));
		lbExplain = new Label(Resource.get(UI, "wizard.selectMetatype.explain"));

		metaList = new ListView<MetaTypeOption>();
		metaList.setCellFactory(new Callback<ListView<MetaTypeOption>, ListCell<MetaTypeOption>>() {
			@Override
			public ListCell<MetaTypeOption> call(ListView<MetaTypeOption> arg0) {
				return new MetatypeListCell();
			}
		});

		metaList.getItems().addAll(charGen.getAvailable());

//		karma = new FreePointsNode();
//		karma.setName(Resource.get(UI, "label.karma"));
//		karma.setPoints(model.getKarmaFree());
		
		bxQualities = new VBox(3);
		bxAttributes= new Label();

		description = new HelpTextPane();

		lblToDos = new Label("-");
		lblToDos.setStyle("-fx-text-fill: red;");
		lblToDos.setWrapText(true);

		side = new WizardPointsPane(false, false);
		side.setKarma(model.getKarmaFree());
//		side.setExtra(karma);
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdQualities = new Label(Resource.get(UI, "label.qualities"));
		hdQualities.getStyleClass().add("base");
		Label hdAttributes = new Label(Resource.get(UI, "label.attributes"));
		hdAttributes.getStyleClass().add("base");

		VBox bxInfo = new VBox();
		bxInfo.getChildren().add(description);
		bxInfo.getChildren().addAll(hdQualities, bxQualities);
		bxInfo.getChildren().addAll(hdAttributes, bxAttributes);
		VBox.setMargin(hdQualities , new Insets(20,0,0,0));
		VBox.setMargin(hdAttributes, new Insets(20,0,0,0));

		OptionalDescriptionPane line2 = new OptionalDescriptionPane(metaList, bxInfo);
//		line2.setSpacing(5);
//		line2.getChildren().addAll(metaList, bxInfo);
		
		content = new VBox();
		content.setSpacing(5);
		content.getChildren().addAll(lbExplain, line2, lblToDos);
		setImageInsets(new Insets(-40,0,0,0));
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		metaList.getStyleClass().add("metatype-list");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		metaList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> changed(ov,o,n));
	}

	//-------------------------------------------------------------------
	private void update(MetaTypeOption option) {
		MetaType newRace = option.getType();
		if (newRace.getVariantOf()!=null)
			newRace = newRace.getVariantOf();
		Image img = imageByRace.get(newRace);
		if (img==null) {
			String fname = "images/shadowrun/race_"+newRace.getKey()+".png";
			logger.trace("Load "+fname);
			InputStream in = SR6Constants.class.getResourceAsStream(fname);
			if (in!=null) {
				img = new Image(in);
				imageByRace.put(newRace, img);
			} else
				logger.warn("Missing image at "+fname);
		}
		setImage(img);
		newRace = option.getType();

		try {
			description.setData(newRace);
			bxAttributes.setText(ShadowrunJFXUtils.getAttributeModPane(newRace));
			bxQualities.getChildren().clear();
			for (Modification tmp : newRace.getModifications()) {
				if (tmp instanceof AttributeModification) 
					continue;
				bxQualities.getChildren().add(new Label(ShadowrunTools.getModificationString(tmp)));
			}

		} catch (Exception e) {
			logger.error(e.toString());
		}
	}

	//-------------------------------------------------------------------
	public void changed(ObservableValue<? extends MetaTypeOption> property, MetaTypeOption oldRace,
			MetaTypeOption newRace) {
		logger.debug("Currently display metatype "+newRace);

		if (newRace!=null) {
			charGen.select(newRace.getType());
			update(newRace);
		}
	}

	//-------------------------------------------------------------------
	private void refresh() {
		side.setKarma(model.getKarmaFree());
		if (charGen==null)
			return;
		if (!metaList.getItems().equals(charGen.getAvailable())) {
			metaList.getItems().clear();
			metaList.getItems().addAll(charGen.getAvailable());
			if (model.getMetatype()==null) {
				logger.info("Autoselect metatype "+metaList.getItems().get(0).getType());
				model.setMetatype(metaList.getItems().get(0).getType());
			}
			
			for (int i=0; i<metaList.getItems().size(); i++) {
				if (metaList.getItems().get(i).getType()==model.getMetatype()) {
					metaList.getSelectionModel().select(metaList.getItems().get(i));
				}
			}
		}
		
		if (model.getMetatype()==null) {
			List<String> todo = new ArrayList<>();
			charGen.getToDos().forEach(tmp -> {
				if (tmp.getSeverity()==Severity.STOPPER)
					todo.add(tmp.getMessage());
				});
			lblToDos.setText(String.join(", ",todo));
		} else {
			lblToDos.setText(null);
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		logger.info("RCV "+event);
		if (ignoreGUI)
			return;
		ignoreGUI = true;
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			side.setKarma(model.getKarmaFree());
			refresh();
			break;
		case CONSTRUCTIONKIT_CHANGED:
			logger.info("*****RCV "+event);
			charGen = ((CharacterGenerator)event.getKey()).getMetatypeController();
			model = ((CharacterGenerator)event.getKey()).getCharacter();
			refresh();
			break;
		default:
		}
		ignoreGUI = false;
	}

}

class MetatypeListCell extends ListCell<MetaTypeOption> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".metatype");

	private HBox layout;
	private Label lblKarma;
	private ImageView ivMetatype;
	private Label lblName;

	//--------------------------------------------------------------------
	public MetatypeListCell() {
		lblKarma = new Label();
		lblKarma.getStyleClass().add("text-subheader");
		lblName  = new Label();
		lblName.getStyleClass().add("text-small-subheader");
		ivMetatype = new ImageView();
		ivMetatype.setFitHeight(64);
		ivMetatype.setFitWidth(64);

		layout = new HBox(5);
		layout.getChildren().addAll(ivMetatype, lblName, lblKarma);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		lblName.setMaxWidth(Double.MAX_VALUE);
		
		getStyleClass().add("metatype-cell");
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MetaTypeOption item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
			ivMetatype.setImage(null);
		} else {
			setGraphic(layout);
			lblName.setText(item.getType().getName());
			lblKarma.setText(""+item.getAdditionalKarmaKost());

			String metaID = (item.getType().getVariantOf()!=null)?item.getType().getVariantOf().getId():item.getType().getId();
			String fName = "images/metaicon_"+metaID+"_rike.png";
			InputStream in = SR6Constants.class.getResourceAsStream(fName);
			if (in==null) {
				logger.warn("Missing "+fName);
			} else {
				Image img = new Image(in);
				ivMetatype.setImage(img);
			}
		}
	}
}
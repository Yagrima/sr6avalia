package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.FocusValueListCell;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;

import javafx.scene.control.Label;
import javafx.scene.control.MultipleSelectionModel;

/**
 * @author Stefan Prelle
 *
 */
public class FociSection extends GenericListSection<FocusValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(FociSection.class.getName());

	//-------------------------------------------------------------------
	public FociSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title.toUpperCase(), ctrl, provider);
		list.setCellFactory( lv -> new FocusValueListCell());
		initPlaceholder();
		refresh();
		list.setStyle("-fx-pref-height: 12em; ");
		
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null || !ctrl.getFocusController().canBeDeselected(n)));
	}

	//--------------------------------------------------------------------
	private void initPlaceholder() {
		Label phSelected = new Label(RES.getString("listview.placeholder"));
		phSelected.setStyle("-fx-text-fill: -fx-text-base-color");
		phSelected.setWrapText(true);
		list.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	public void refresh() {
		list.getItems().clear();
		list.getItems().addAll(control.getCharacter().getFoci());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.debug("opening focus selection dialog");
		
		FocusSelector selector = new FocusSelector(control, provider);
		SelectorWithHelp<Focus> pane = new SelectorWithHelp<Focus>(selector);
		ManagedDialog dialog = new ManagedDialog(RES.getString("selector.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAlertAndCall(dialog, selector.getButtonControl());
		logger.debug("Closed with "+close);
		if (close==CloseType.OK) {
			Focus selected = selector.getSelected();
			int rating = selector.getRating();
			Object choice = selector.getChoice();
			logger.debug("Selected focus: "+selected+"   rating "+rating+"   choice is "+choice);
			if (selected!=null) {
				FocusValue item = null;
				if (selected.needsChoice()) {
					item = control.getFocusController().select(selected, rating, choice);
				} else 
					item = control.getFocusController().select(selected, rating);
				if (item==null) {
					getManagerProvider().getScreenManager().showAlertAndCall(AlertType.ERROR, 
							Resource.get(RES, "selectiondialog.failed.title"), Resource.format(RES, "selectiondialog.failed.format",selected.getName()));
				} else {
					if (selector.getChoicesSelect()!=null)
						item.setChoicesChoiceReference(ShadowrunTools.getChoiceReference(selector.getChoicesSelect(), selector.getChoicesChoice()));
				}
				refresh();
			}
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splittermond.chargen.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		FocusValue selected = list.getSelectionModel().getSelectedItem();
		logger.debug("Focus to deselect: "+selected);
		control.getFocusController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<FocusValue> getSelectionModel() {
		return list.getSelectionModel();
	}

}

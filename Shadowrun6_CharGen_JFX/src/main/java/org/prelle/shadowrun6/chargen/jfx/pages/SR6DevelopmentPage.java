/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DevelopmentPage;
import org.prelle.shadowrun6.HistoryElementImpl;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.RewardImpl;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.develop.RewardBox;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.ProductServiceLoader;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SR6DevelopmentPage extends DevelopmentPage {

	private static Logger logger = LogManager.getLogger("shadowrun6.jfx");

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private ScreenManagerProvider provider;
	private CharacterController control;
	private ShadowrunCharacter model;

	private SR6FirstLine expLine;
	
	//-------------------------------------------------------------------
	/**
	 */
	public SR6DevelopmentPage(CharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		super(UI, RoleplayingSystem.SHADOWRUN6);
		this.setId("shadowrun-development");
		this.setTitle(control.getCharacter().getName());
		this.provider = provider;
		this.control = control;
		history.setTitle("Development".toUpperCase());
		model = control.getCharacter();
		logger.info("<init>()");
		
		expLine = new SR6FirstLine(ViewMode.MODIFICATION, control, provider);
		getCommandBar().setContent(expLine);
		setConverter(new StringConverter<Modification>() {
			public String toString(Modification arg0) {  return ShadowrunTools.getModificationString(arg0);}
			public Modification fromString(String arg0) {return null;}
		});
		refresh();
	}

	//-------------------------------------------------------------------
	@Override
	public void refresh() {
		logger.trace("refresh");
		setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.development"));
		history.setData(ShadowrunTools.convertToHistoryElementList(model, super.shallBeAggregated()));
		expLine.setData(control.getCharacter());
		
		getSectionList().forEach(sect -> sect.refresh());
		setPointsFree(control.getCharacter().getKarmaFree());
	}

	//-------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
//		setTitle(model.getName()+" / "+UI.getString("label.development"));
		
//		history.getItems().clear();
//		history.getItems().addAll(SplitterTools.convertToHistoryElementList(model));
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openAdd()
	 */
	@Override
	public HistoryElement openAdd() {
		RewardBox content = new RewardBox();
		logger.debug("openAdd");
		
		ManagedDialog dialog = new ManagedDialog(UI.getString("dialog.reward.title"), content, CloseType.OK, CloseType.CANCEL);
		NavigButtonControl btnControl = new NavigButtonControl();
		btnControl.setDisabled(CloseType.OK, true);
		btnControl.setCallback( (ct) -> {
			if (ct==CloseType.OK)
				return content.hasEnoughData();
			else
				return true;
		});
		content.enoughDataProperty().addListener( (ov,o,n) -> btnControl.setDisabled(CloseType.OK, n!=true));
		CloseType closed = (CloseType)provider.getScreenManager().showAlertAndCall(dialog, btnControl);
		
		if (closed==CloseType.OK) {
			RewardImpl reward = content.getDataAsReward();
			logger.debug("Add reward "+reward);
			ShadowrunTools.reward(model, reward);
			HistoryElementImpl elem = new HistoryElementImpl();
			elem.setName(reward.getTitle());
			elem.addGained(reward);
			if (reward.getId()!=null)
				elem.setAdventure(ProductServiceLoader.getInstance().getAdventure(RoleplayingSystem.SHADOWRUN6, reward.getId()));
			control.runProcessors();
			return elem;
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.DevelopmentScreen#openEdit(de.rpgframework.genericrpg.HistoryElement)
	 */
	@Override
	public boolean openEdit(HistoryElement elem) {
		logger.warn("TODO: openEdit");
		/*
		 * Currently only elements
		 */
		if (elem.getGained().size()>1) {
			getScreenManager().showAlertAndCall(
					AlertType.ERROR, 
					UI.getString("error.not-possible"), 
					UI.getString("error.only-single-rewards-editable"));
			return false;
		}
		
		Reward toEdit = elem.getGained().get(0);
		RewardBox dialog = new RewardBox(toEdit);
		
//		ManagedScreen screen = new ManagedScreen() {
//			@Override
//			public boolean close(CloseType closeType) {
//				logger.debug("close("+closeType+") overwritten  "+dialog.hasEnoughData());
//				if (closeType==CloseType.OK) {
//					return dialog.hasEnoughData();
//				}
//				return true;
//			}
//		};
//		ManagedScreenDialogSkin skin = new ManagedScreenDialogSkin(screen);
//		screen.setTitle(UI.getString("dialog.reward.title"));
//		screen.setContent(dialog);
//		screen.getNavigButtons().addAll(CloseType.OK, CloseType.CANCEL);
//		screen.setSkin(skin);
//		screen.setOnAction(CloseType.OK, event -> manager.close(screen, CloseType.OK));
//		screen.setOnAction(CloseType.CANCEL, event -> manager.close(screen, CloseType.CANCEL));
//		CloseType closed = (CloseType)manager.showAndWait(screen);
//		
//		if (closed==CloseType.OK) {
//			RewardImpl reward = dialog.getDataAsReward();
//			logger.debug("Copy edited data: ORIG = "+toEdit);
//			toEdit.setTitle(reward.getTitle());
//			toEdit.setId(reward.getId());
//			toEdit.setDate(reward.getDate());
//			// Was single reward. Changes in reward title, change element title
//			((HistoryElementImpl)elem).setName(reward.getTitle());
//			ProductService sessServ = RPGFrameworkLoader.getInstance().getProductService();
//			if (reward.getId()!=null)  {
//				Adventure adv = sessServ.getAdventure(RoleplayingSystem.SPLITTERMOND, reward.getId());
//				if (adv==null) {
//					logger.warn("Reference to an unknown adventure: "+reward.getId());
//				} else
//					((HistoryElementImpl)elem).setAdventure(adv);
//
//			}
////			((HistoryElementImpl)elem).set(reward.getTitle());
//			logger.debug("Copy edited data: NEW  = "+toEdit);
//			logger.debug("Element now   = "+elem);
//			return true;
//		}
		return false;
	}

}
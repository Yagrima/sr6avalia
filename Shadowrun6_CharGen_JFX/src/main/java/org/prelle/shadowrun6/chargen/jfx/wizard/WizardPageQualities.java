/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.HelpTextPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.chargen.jfx.panels.QualitySelectionPane;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageQualities extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private CharacterGenerator charGen;

	private QualitySelectionPane qualityPane;
	private ImageView imgRec;
	private Label lbNetto;
	private Label lbNumQual;
//	private Label instruction;
	private HBox lineInstr;
	private Label lbWarnings;

	private OptionalDescriptionPane content;
	private HelpTextPane description;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageQualities(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(Resource.get(UI,"wizard.selectQualities.title"));

		qualityPane = new QualitySelectionPane(charGen, this);
		qualityPane.setData(charGen.getCharacter());

		description = new HelpTextPane();

		imgRec = new ImageView(new Image(SR6Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRec.setStyle("-fx-fit-height: 2em");
		lbNetto = new Label("0");
		lbNumQual = new Label("0");
		lbWarnings = new Label("");
		lbWarnings.setStyle("-fx-text-fill: textcolor-stopper");
//		instruction = new Label(Resource.get(UI, "wizard.selectQualities.instruct"));
//		instruction.setWrapText(true);

		String fName = "images/wizard/Qualities.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, false);
//		side.setPoints2Name(Resource.get(UI,"wizard.selectQualities.pointsNegQualities"));
//		side.setPoints2(20-charGen.getQualityController().getNetBonusKarma());
		side.setKarma(charGen.getCharacter().getKarmaFree());
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		description.setPrefWidth(500);
		Label hdNetto = new Label(Resource.get(UI, "wizard.selectQualities.netto"));
		Label hdNumQual = new Label(Resource.get(UI, "wizard.selectQualities.numQual"));
		lineInstr = new HBox(10);
		lineInstr.getChildren().addAll(hdNetto, lbNetto, hdNumQual, lbNumQual);
		HBox.setMargin(hdNumQual, new Insets(0,0,0,20));

		VBox layout = new VBox(5);
		layout.getChildren().addAll(lineInstr, qualityPane, lbWarnings);
		VBox.setVgrow(layout, Priority.ALWAYS);

		content = new OptionalDescriptionPane(layout, description);
		super.setContent(content);
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initStyle() {
//		instruction.getStyleClass().add("text-body");
		lbNetto.getStyleClass().add("base");
		lbNetto.setStyle("-fx-font-size: 120%");
		lbNumQual.getStyleClass().add("base");
		lbNumQual.setStyle("-fx-font-size: 120%");
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		qualityPane.showHelpForProperty().addListener( (ov,o,n) -> description.setData(n));
	}

	//-------------------------------------------------------------------
	public void refresh() {
		qualityPane.refresh();
		if (side==null)
			return;
//		side.setPoints2(20-charGen.getQualityController().getNetBonusKarma());
		side.setKarma(charGen.getCharacter().getKarmaFree());
		lbNetto.setText(String.valueOf(charGen.getQualityController().getNetBonusKarma()));
		lbNumQual.setText(String.valueOf(charGen.getCharacter().getUserSelectedQualities().size()));
		
		List<String> warnings = new ArrayList<>();
		charGen.getQualityController().getToDos().forEach( todo -> warnings.add(todo.getMessage()));
		lbWarnings.setText(String.join(", ", warnings));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		case CHARACTERCONCEPT_ADDED:
		case CHARACTERCONCEPT_REMOVED:
//			if (charGen.getConcept()==null) {
				lineInstr.setVisible(false);
//			} else {
//				lineInstr.setVisible(true);
//				instruction.setText(String.format(UI.getString("wizard.selectQualities.instruct"), charGen.getConcept().getName()));
//			}
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}

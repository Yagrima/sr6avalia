package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.ConnectionSection;
import org.prelle.shadowrun6.chargen.jfx.sections.LicenseSection;
import org.prelle.shadowrun6.chargen.jfx.sections.LifestyleSection;
import org.prelle.shadowrun6.chargen.jfx.sections.NotesSection;
import org.prelle.shadowrun6.chargen.jfx.sections.SINSection;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan Prelle
 *
 */
public class SR6SINLifestylePage extends SR6ManagedScreenPage {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6SINLifestylePage.class.getName());

	private SINSection sins;
	private LifestyleSection lifestyles;
	private ConnectionSection connections;
	private NotesSection notes;
	private LicenseSection licenses;
	
	private Section secLine1;
	private Section secLine2;
	private Section secLine3;
	
	//-------------------------------------------------------------------
	/**
	 * @param charGen
	 * @param handle
	 */
	public SR6SINLifestylePage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-life");
		this.setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.life"));
		this.provider = provider;
		
		initComponents();
		initInteractivity2();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		sins = new SINSection(UI.getString("section.sins").toUpperCase(), control, provider);
		lifestyles = new LifestyleSection(UI.getString("section.lifestyles").toUpperCase(), control, provider);

		secLine1 = new DoubleSection(lifestyles, sins);
		getSectionList().add(secLine1);

		// Interactivity
//		sins.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.getSIN()); else updateHelp(null); });
		lifestyles.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.getLifestyle()); else updateHelp(null); });
	}

	//-------------------------------------------------------------------
	private void initLine2() {
		connections = new ConnectionSection(Resource.get(UI,"section.connections").toUpperCase(), control, provider);
		licenses = new LicenseSection(Resource.get(UI,"section.licenses").toUpperCase(), control, provider);

		secLine2 = new DoubleSection(connections, licenses);
		getSectionList().add(secLine2);

		// Interactivity
//		sins.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.getSIN()); else updateHelp(null); });
//		connections.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.g); else updateHelp(null); });
	}

	//-------------------------------------------------------------------
	private void initLine3() {
		notes       = new NotesSection(Resource.get(UI,"section.notes").toUpperCase(), control, provider);

		secLine3 = notes;
		getSectionList().add(notes);

		// Interactivity
//		sins.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.getSIN()); else updateHelp(null); });
//		connections.showHelpForProperty().addListener( (ov,o,n) -> { if (n!=null) updateHelp(n.g); else updateHelp(null); });
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		initLine1();
		initLine2();
		initLine3();
	}

	//-------------------------------------------------------------------
	protected void initInteractivity2() {
		sins.getListView().getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				lifestyles.getListView().getSelectionModel().clearSelection();
				connections.getListView().getSelectionModel().clearSelection();
			}
		});
		lifestyles.getListView().getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				sins.getListView().getSelectionModel().clearSelection();
				connections.getListView().getSelectionModel().clearSelection();
			}
		});
		connections.getListView().getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				sins.getListView().getSelectionModel().clearSelection();
				lifestyles.getListView().getSelectionModel().clearSelection();
			}
		});
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		super.refresh();
		this.setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.life"));
		secLine1.getToDoList().clear();
		for (ToDoElement tmp : control.getSINController().getToDos()) {
			secLine1.getToDoList().add(tmp);
		}
		secLine2.getToDoList().clear();
		for (ToDoElement tmp : control.getConnectionController().getToDos()) {
			secLine2.getToDoList().add(tmp);
		}
	}

}

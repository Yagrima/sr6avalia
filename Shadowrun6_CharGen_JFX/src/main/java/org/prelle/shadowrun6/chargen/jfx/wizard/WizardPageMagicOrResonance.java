/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.HelpTextPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.MagicOrResonanceOption;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Tradition;
import org.prelle.shadowrun6.charctrl.MagicOrResonanceController;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.beans.value.ObservableValue;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class WizardPageMagicOrResonance extends WizardPage implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageMagicOrResonance.class.getName());

	private ShadowrunCharacter model;
	private CharacterGenerator charGen;
	private MagicOrResonanceController magicGen;

	private ListView<MagicOrResonanceOption> morOptList;
	private HelpTextPane description;

	private OptionalDescriptionPane content;
	private WizardPointsPane side;

	private boolean ignoreEvents;
	
	/* For mystic adepts */
	private Label lbTotal;
	private Label lbMagic;
	private Label lbPower;
	private Button btnDec;
	private Button btnInc;
	private GridPane mysticGrid;
	
	/* For aspected magicians */
	private ChoiceBox<Skill> cbAspectSkill;
	private GridPane aspectGrid;
	
	/* For (aspected) magicians and mystic adepts*/
	private ChoiceBox<Tradition> cbTradition;
	private GridPane traditionGrid;
	
	private Label lblToDos;
	private Label lblToDosHead;
	private HBox  bxToDo;

	//-------------------------------------------------------------------
	/**
	 * @param wizard
	 */
	public WizardPageMagicOrResonance(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		this.magicGen = charGen.getMagicOrResonanceController();
		if (this.magicGen==null)
			throw new NullPointerException("MagicOrResonance controller not set");
		model = charGen.getCharacter();

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		//		morOptList.getSelectionModel().select(0);
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectMagicOrResonance.title"));

		morOptList = new ListView<MagicOrResonanceOption>();
		morOptList.setCellFactory(new Callback<ListView<MagicOrResonanceOption>, ListCell<MagicOrResonanceOption>>() {
			@Override
			public ListCell<MagicOrResonanceOption> call(ListView<MagicOrResonanceOption> arg0) {
				return new MagicOrResonanceCell();
			}
		});

		morOptList.getItems().addAll(magicGen.getAvailable());

		/* For mystic adepts */
		lbTotal = new Label();
		lbMagic = new Label();
		lbPower = new Label();
		btnDec  = new Button("<");
		btnInc  = new Button(">");
		/* For aspected magicians */
		cbAspectSkill = new ChoiceBox<>();
		cbAspectSkill.getItems().addAll(null, ShadowrunCore.getSkill("sorcery"), ShadowrunCore.getSkill("conjuring"), ShadowrunCore.getSkill("enchanting"));
		cbAspectSkill.setConverter(new StringConverter<Skill>() {
			public String toString(Skill value) {
				if (value==null) return "-";
				return value.getName();
			}
			public Skill fromString(String string) { return null; }
		});
		/* For (aspected) magicians and mystic adepts */
		cbTradition = new ChoiceBox<>();
		cbTradition.getItems().addAll(ShadowrunCore.getTraditions());
		cbTradition.setConverter(new StringConverter<Tradition>() {
			public String toString(Tradition value) {
				if (value==null) return "-";
				return value.getName();
			}
			public Tradition fromString(String string) { return null; }
		});
		
		
		description = new HelpTextPane();

		side = new WizardPointsPane(false, false);
		side.setKarma(model.getKarmaFree());
//		side.setExtra(karma);
		setSide(side);

		String fName = "images/wizard/MagicResonance.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		/* Mystic adept */
		Label lbMysticExplain = new Label(Resource.get(UI, "explain.mystic"));
		lbMysticExplain.setWrapText(true);
//		lbMysticExplain.setStyle("-fx-max-width: 20em;");
		Label hdMagician = new Label(Resource.get(UI, "label.magician"));
		Label hdAdept    = new Label(Resource.get(UI, "label.adept"));
		hdMagician.getStyleClass().add("base");
		hdAdept.getStyleClass().add("base");
		lbTotal.setAlignment(Pos.CENTER);
		lbTotal.setMaxWidth(Double.MAX_VALUE);
		mysticGrid = new GridPane();
		mysticGrid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 1em;"); 
		mysticGrid.add(lbMysticExplain, 0, 0, 4,1);
		mysticGrid.add(hdMagician, 0, 1);
		mysticGrid.add(   lbTotal, 1, 1, 2,1);
		mysticGrid.add(   hdAdept, 3, 1);
		mysticGrid.add(   lbMagic, 0, 2);
		mysticGrid.add(    btnDec, 1, 2);
		mysticGrid.add(    btnInc, 2, 2);
		mysticGrid.add(   lbPower, 3, 2);
		GridPane.setHalignment(lbMagic, HPos.RIGHT);
		GridPane.setFillWidth(lbTotal, true);
		
		/* Aspected magician */
		Label lbAspectExplain = new Label(Resource.get(UI, "explain.aspected"));
		lbAspectExplain.setWrapText(true);
//		lbAspectExplain.setStyle("-fx-max-width: 20em;");
		aspectGrid = new GridPane();
		aspectGrid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 1em;"); 
		aspectGrid.add(lbAspectExplain, 0, 0, 4,1);
		aspectGrid.add(cbAspectSkill, 0, 1);
		
		/* Spell users */
		Label lbSpell = new Label(Resource.get(UI, "explain.spelluser"));
		lbSpell.setWrapText(true);
		traditionGrid = new GridPane();
		traditionGrid.setStyle("-fx-vgap: 0.5em; -fx-hgap: 1em;"); 
		traditionGrid.add(lbSpell, 0, 0, 4,1);
		traditionGrid.add(cbTradition, 0, 1);

		/* To Do*/	
		lblToDosHead = new Label(Resource.get(UI,"label.todos"));
		lblToDos = new Label("-");
		lblToDos.setStyle("-fx-text-fill: red;");
		lblToDos.setWrapText(true);
		bxToDo = new HBox(20, lblToDosHead, lblToDos);
		
		description.setStyle("-fx-max-width: 35em;");

		
		mysticGrid.setVisible(false);
		mysticGrid.setManaged(false);
		aspectGrid.setVisible(false);
		aspectGrid.setManaged(false);
		traditionGrid.setVisible(false);
		traditionGrid.setManaged(false);
		VBox listAndChoices = new VBox(morOptList, mysticGrid, aspectGrid, traditionGrid, bxToDo);
		listAndChoices.setStyle("-fx-spacing: 1em");
		
		content = new OptionalDescriptionPane(listAndChoices, description);
		setImageInsets(new Insets(-40,0,0,0));
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		morOptList.setStyle("-fx-pref-height: 20em");
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		morOptList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> changed(ov,o,n));
		btnDec.setOnAction(ev -> magicGen.mysticAdeptDecreasePowerPoint());
		btnInc.setOnAction(ev -> magicGen.mysticAdeptIncreasePowerPoint());
		cbAspectSkill.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> magicGen.setAspectSkill(n));
		cbTradition.getSelectionModel().selectedItemProperty().addListener((ov,o,n) -> {
			magicGen.selectTradition(n);
			if (n==null) {
				// No matching option selected
				description.setData(null);
			} else {
				description.setData(n);
			}
		}); 
	}

	//-------------------------------------------------------------------
	public void changed(ObservableValue<? extends MagicOrResonanceOption> property, MagicOrResonanceOption oldRace,
			MagicOrResonanceOption newOpt) {
		logger.debug("Currently display metatype "+newOpt);
		if (ignoreEvents)
			return;
		
		ignoreEvents = true;
		try {
			magicGen.select(newOpt);
			refresh();
		} finally {
			ignoreEvents = false;
		}
	}

	//-------------------------------------------------------------------
	public void refresh() {
		
		
		if (!morOptList.getItems().equals(charGen.getMagicOrResonanceController().getAvailable())) {
			logger.debug("************************MOR changed******************");
			logger.debug("****old = "+morOptList.getItems());
			logger.debug("****new = "+charGen.getMagicOrResonanceController().getAvailable());
			morOptList.getItems().clear();
			morOptList.getItems().addAll(charGen.getMagicOrResonanceController().getAvailable());
		}
		
		MagicOrResonanceOption option = null;
		for (MagicOrResonanceOption opt : morOptList.getItems()) {
			if (opt.getType()==charGen.getCharacter().getMagicOrResonanceType()) {
				option = opt;
				morOptList.getSelectionModel().select(opt);
				break;
			}
		}
		
		MagicOrResonanceType choice = charGen.getCharacter().getMagicOrResonanceType();
		/* Mystic adept */
		mysticGrid.setVisible(choice!=null && choice.paysPowers());
		mysticGrid.setManaged(choice!=null && choice.paysPowers());
		if (choice!=null && choice.paysPowers()) {
			logger.debug("Show mystic adept box");
			lbTotal.setText(String.valueOf(option.getValue()));
			lbMagic.setText(String.valueOf(magicGen.mysticAdeptGetSpellPoints()));
			lbPower.setText(String.valueOf(magicGen.mysticAdeptGetPowerPoints()));
			btnDec.setDisable(!magicGen.mysticAdeptCanDecreasePowerPoint());
			btnInc.setDisable(!magicGen.mysticAdeptCanIncreasePowerPoint());
		}
		
		/* Aspected magician */
		aspectGrid.setVisible(choice!=null && choice.isAspected());
		aspectGrid.setManaged(choice!=null && choice.isAspected());
		if (choice!=null && choice.isAspected()) {
			logger.debug("Show aspected magician box");
			cbAspectSkill.setValue(charGen.getCharacter().getAspectSkill());
		}
		
		/* (Aspected) magician and mystic adepts*/
		traditionGrid.setVisible(choice!=null && choice.usesSpells());
		traditionGrid.setManaged(choice!=null && choice.usesSpells());
		if (choice!=null && choice.usesSpells()) {
			logger.debug("Show tradition box");
			cbTradition.setValue(charGen.getCharacter().getTradition());
		}
		
		/* To Do */
		List<String> todo = new ArrayList<>();
		magicGen.getToDos().forEach(tmp -> todo.add(tmp.getMessage()));
		lblToDos.setText(String.join(", ", todo));
		lblToDosHead.setVisible(!magicGen.getToDos().isEmpty());
		
		if (option==null) {
			// No matching option selected
			description.setData(null);
		} else {
			description.setData(option.getType());
		}

		side.setKarma(model.getKarmaFree());
		// Deactivate page if there is no choice
		setActive(magicGen.getAvailable().size()>1);
		// If there is only one choice, pick it
		if (magicGen.getAvailable().size()==1) {
			magicGen.select(magicGen.getAvailable().get(0));
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (ignoreEvents)
			return;
		ignoreEvents = true;
		try {
			switch (event.getType()) {
			case CHARACTER_CHANGED:				
				refresh();
				break;
			default:
			}
		} finally {
			ignoreEvents = false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is shown to user
	 */
	@Override
	public void pageVisited() {
		//		refresh();
	}

}

class MagicOrResonanceCell extends ListCell<MagicOrResonanceOption> {

	private VBox layout;
	private Label lblHeading;
	private Label lblHardcopy;
//	private Label lblKarma;

	//--------------------------------------------------------------------
	public MagicOrResonanceCell() {
		lblHeading = new Label();
		lblHardcopy = new Label();
//		lblKarma = new Label();
		lblHeading.getStyleClass().add("base");
		lblHardcopy.setStyle("-fx-text-fill: derive(#efeee9, -30%)");
		layout = new VBox(5);
		layout.getChildren().addAll(lblHeading, lblHardcopy);

//		layout.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(MagicOrResonanceOption item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			lblHeading.setText(item.getType().getName());
			lblHardcopy.setText(item.getType().getProductName()+" "+item.getType().getPage());
			lblHardcopy.setUserData(item);
		}
	}
}
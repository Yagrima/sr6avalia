package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.chargen.jfx.panels.ComplexFormDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.ComplexFormSelectionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.PowerDescriptionPane;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPageComplexForms extends WizardPage implements GenerationEventListener, ScreenManagerProvider {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPageComplexForms.class.getName());

	private CharacterGenerator charGen;

	private ComplexFormDescriptionPane description;
	private ComplexFormSelectionPane selectPane;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPageComplexForms(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectComplexForms.title"));

		selectPane = new ComplexFormSelectionPane(charGen, this);

		description = new ComplexFormDescriptionPane();

		content = new HBox();
		content.setSpacing(20);

		String fName = "images/wizard/ComplexForms.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, false);
		int pointsSK = charGen.getSpellController().getSpellsLeft();
		side.setPoints(pointsSK);
		side.setPoints2Name(null);
		side.setPoints2(-1);
		side.setKarma(charGen.getCharacter().getKarmaFree());
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		OptionalDescriptionPane content = new OptionalDescriptionPane(selectPane, description);
		
//		VBox layout = new VBox(5, firstLine, content);
//		powerPane.setMaxHeight(Double.MAX_VALUE);
//		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selectPane.showHelpForProperty().addListener( (ov,o,n)-> {
			description.setData(n);
		});
	}

	//-------------------------------------------------------------------
	private void refresh() {
		side.setPoints(charGen.getComplexFormController().getComplexFormsLeft());
		side.setKarma(charGen.getCharacter().getKarmaFree());

		/*
		 * Enable or disable page
		 */
		MagicOrResonanceType type = charGen.getCharacter().getMagicOrResonanceType();
		if (type!=null) {
			// Enable or disable page
			if (type.usesResonance()) {
				logger.debug(type+" uses resonance - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(type+" does not use resonance - disable page");
				activeProperty().set(false);
			}
		} else {
			logger.warn("No magic or resonance type selected yet");
			activeProperty().set(false);
		}

		selectPane.refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.debug("RCV "+event);
			refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}

package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.Resource;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class PowerDescriptionPane extends VBox {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(PowerSelectionPane.class.getName());
	
	private Label descHeading;
	private Label descRef;
	private Label descCost;
	private Label descActi;
	private Label description;

	//-------------------------------------------------------------------
	public PowerDescriptionPane() {
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		descHeading = new Label();
		descHeading.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
		descRef     = new Label();
		descCost    = new Label();
		descActi    = new Label();
		
		description = new Label();
		description.setWrapText(true);
		description.setStyle("-fx-max-width: 20em");
		
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lblCost = new Label(Resource.get(UI, "label.cost")+":");
		Label lblActi = new Label(Resource.get(UI, "label.activation")+":");
		lblCost.setStyle("-fx-font-weight: bold");
		lblActi.setStyle("-fx-font-weight: bold");
		GridPane descGrid = new GridPane();
		descGrid.add( lblCost, 0, 0);
		descGrid.add(descCost, 1, 0);
		descGrid.add( lblActi, 0, 1);
		descGrid.add(descActi, 1, 1);
		descGrid.setHgap(5);
		
		setSpacing(10);
		getChildren().addAll(descHeading, descRef, descGrid, description);
		VBox.setVgrow(description, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	public void setData(AdeptPower data) {
		if (data==null) {
			descHeading.setText(null);
			descRef.setText(null);
			descCost.setText(null);
			descActi.setText(null);
			description.setText(null);
		} else {
			descHeading.setText(data.getName().toUpperCase());
			descRef.setText(data.getProductName()+" "+data.getPage());
			descCost.setText(String.valueOf(data.getCost()));
			descActi.setText(data.getActivation().toString());
			description.setText(data.getHelpText());
		}
	}

}

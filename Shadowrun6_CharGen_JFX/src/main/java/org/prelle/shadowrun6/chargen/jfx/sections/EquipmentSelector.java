/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.items.ItemTemplateSelector;
import org.prelle.shadowrun6.jfx.items.ItemUtilJFX;

import javafx.scene.Node;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class EquipmentSelector extends HBox implements IListSelector<ItemTemplate> {

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private CharacterController charGen;
	private EquipmentController control;
//	private TextField tfName;
//	private ChoiceBox<ItemType> cbTypes;
//	private ChoiceBox<ItemSubType> cbSubTypes;
//	private ListView<ItemTemplate> lvItems;
	private ItemTemplateSelector selector;

	private Node detailNode;
	private ChoiceBox<Integer> cbRating;
	private HBox ratingNode;

	//-------------------------------------------------------------------
	public EquipmentSelector(CharacterController charGen, ItemType... allowedTypes) {
		this.charGen = charGen;
		this.control = charGen.getEquipmentController();

		initComponents(allowedTypes);
		initLayout();
		initInteractivity();

//		cbTypes.getSelectionModel().select(0);
//		cbSubTypes.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents(ItemType... allowedTypes) {
//		tfName = new TextField();
//		tfName.setPromptText(UI.getString("itemselector.name.prompt"));
//
//		cbTypes = new ChoiceBox<ItemType>();
//		cbTypes.setMaxWidth(Double.MAX_VALUE);
//		cbTypes.getItems().addAll(allowedTypes);
//		cbTypes.setConverter(new StringConverter<ItemType>() {
//			public String toString(ItemType data) {return data.getName();}
//			public ItemType fromString(String data) {return null;}
//		});
//
//		cbSubTypes = new ChoiceBox<ItemSubType>();
//		cbSubTypes.setMaxWidth(Double.MAX_VALUE);
//		cbSubTypes.setConverter(new StringConverter<ItemSubType>() {
//			public String toString(ItemSubType data) {return data.getName();}
//			public ItemSubType fromString(String data) {return null;}
//		});
//
//		lvItems = new ListView<ItemTemplate>();
//		lvItems.setCellFactory(new Callback<ListView<ItemTemplate>, ListCell<ItemTemplate>>() {
//
//			@Override
//			public ListCell<ItemTemplate> call(ListView<ItemTemplate> param) {
//				return new ItemTemplateListCell2(control);
//			}
//		});
		selector = new ItemTemplateSelector(allowedTypes, charGen);

		cbRating = new ChoiceBox<>();
		cbRating.getItems().addAll(1,2,3,4,5,6);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
//		VBox column1 = new VBox();
//		column1.setStyle("-fx-spacing: 0.5em");
//		column1.getChildren().addAll(tfName, cbTypes, cbSubTypes, lvItems);
//
//		lvItems.setStyle("-fx-pref-width: 30em");

		setSpacing(20);
		getChildren().add(selector);

		// Elements for second column
		Label lblRating = new Label(Resource.get(UI,"label.rating"));
		ratingNode = new HBox(10, lblRating, cbRating);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		selector.selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				if (detailNode!=null)
					getChildren().remove(detailNode);
				VBox column2 = new VBox(20);
				column2.getChildren().add(ItemUtilJFX.getItemInfoNode(n, charGen.getCharacter()));
				Region spacing = new Region();
				spacing.setMaxHeight(Double.MAX_VALUE);
				column2.getChildren().add(spacing);
				VBox.setVgrow(spacing, Priority.ALWAYS);
//				if (n.hasRating()) {
//					column2.getChildren().add(ratingNode);
//				}

				detailNode = column2;
				getChildren().add(detailNode);
			}
		});
	}

	//--------------------------------------------------------------------
	public ItemTemplate getTemplate() {
		return selector.getSelectedItem();
//		return lvItems.getSelectionModel().getSelectedItem();
	}

	//--------------------------------------------------------------------
	public int getRating() {
		Integer val = cbRating.getValue();
		if (val==null)
			return 0;
		return val;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<ItemTemplate> getSelectionModel() {
		return selector.getSelectionModel();
	}

	//-------------------------------------------------------------------
	@Override
	public Node getNode() {
		return this;
	}

}

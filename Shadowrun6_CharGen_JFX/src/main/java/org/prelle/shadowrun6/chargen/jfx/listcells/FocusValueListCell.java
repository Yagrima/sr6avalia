package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.FocusValue;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.character.HardcopyPluginData;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class FocusValueListCell extends ListCell<FocusValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".focus");

	private HBox layout;
	private Label line1;
	private Label line2;
	private Label force;

	//-------------------------------------------------------------------
	public FocusValueListCell() {
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		line1 = new Label();
		line2 = new Label();
		force = new Label();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		line1.getStyleClass().add("base");
		force.getStyleClass().add("text-subheader");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		VBox col1 = new VBox(5, line1, line2);
		col1.setMaxWidth(Double.MAX_VALUE);
		layout = new HBox(10, col1, force);
		HBox.setHgrow(col1, Priority.ALWAYS);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+this.getItem());
		if (this.getItem()==null)
			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = "focusval:"+this.getItem().getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(FocusValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);			
		} else {
			setGraphic(layout);
			line1.setText(item.getName());
			line2.setText(null);
			if (item.getChoice()!=null) {
				if (item.getChoice() instanceof HardcopyPluginData) {
					line2.setText( ((HardcopyPluginData)item.getChoice()).getName() );
				} else if (item.getChoice() instanceof Spell.Category) {
					line2.setText( ((Spell.Category)item.getChoice()).getName() );
				} else {
					logger.warn("Don't know how to get name for "+item.getChoice().getClass());
					line2.setText(String.valueOf(item.getChoice()));
				}
			}
			
			force.setText(String.valueOf(item.getLevel()));
		}
	}

}
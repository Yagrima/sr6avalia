/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.PropertyResourceBundle;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.jfx.SR6Constants;

import de.rpgframework.RPGFramework;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class PortraitSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static Preferences CONFIG = Preferences.userRoot().node(RPGFramework.LAST_OPEN_DIR);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private CharacterController control;
	private ShadowrunCharacter model;

	private TextArea tfFace;
	private TextArea tfClothing;
	private ImageView ivPortrait;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public PortraitSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		control = ctrl;
		model = ctrl.getCharacter();

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		ivPortrait = new ImageView();
		ivPortrait.setFitHeight(150);
		ivPortrait.setFitWidth(150);
		ivPortrait.setPreserveRatio(true);
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));

		setDeleteButton( btnDel );
		setAddButton( btnAdd );
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		StackPane stack = new StackPane(ivPortrait);
		setContent(stack);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnAdd.setOnAction(ev -> openImage());
		btnDel.setOnAction(ev -> removeImage());
	}

	//-------------------------------------------------------------------
	public void refresh() {
		ShadowrunCharacter model = control.getCharacter();

		if (model.getImage()!=null) {
			ivPortrait.setImage(new Image(new ByteArrayInputStream(model.getImage())));
		} else {
			ivPortrait.setImage(null);
		}
	}

	//-------------------------------------------------------------------
	private void openImage() {
		logger.debug("openImage");
		FileChooser chooser = new FileChooser();
		chooser.setTitle(UI.getString("appearance.filechooser.title"));
		String lastDir = CONFIG.get(RPGFramework.PROP_LAST_OPEN_IMAGE_DIR, System.getProperty("user.home"));
		chooser.setInitialDirectory(new File(lastDir));
		chooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("All", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"),
				new FileChooser.ExtensionFilter("PNG", "*.png")
				);
		File selection = chooser.showOpenDialog(new Stage());
		if (selection!=null) {
			CONFIG.put(RPGFramework.PROP_LAST_OPEN_IMAGE_DIR, selection.getParentFile().getAbsolutePath().toString());
			try {
				byte[] imgBytes = Files.readAllBytes(selection.toPath());
				ivPortrait.setImage(new Image(new ByteArrayInputStream(imgBytes)));
				control.getCharacter().setImage(imgBytes);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null));
			} catch (IOException e) {
				logger.warn("Failed loading image from "+selection+": "+e);
			}
		}
	}

	//-------------------------------------------------------------------
	private void removeImage() {
		ivPortrait.setImage(null);
		control.getCharacter().setImage(null);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.CHARACTER_CHANGED, null));
	}

}

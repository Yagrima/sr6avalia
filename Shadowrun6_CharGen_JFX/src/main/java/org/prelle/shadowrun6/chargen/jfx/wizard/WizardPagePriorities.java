/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.chargen.NewPriorityCharacterGenerator;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPagePriorities extends WizardPage {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());

	private CharacterGenerator charGen;

	private PriorityTable table;
	
	private VBox content;

	//--------------------------------------------------------------------
	public WizardPagePriorities(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(Resource.get(UI,"wizard.selectPrio.title"));

		table = new PriorityTable((NewPriorityCharacterGenerator) CharacterGeneratorRegistry.getSelected());
		
		content = new VBox();
		content.setSpacing(5);
		
		String fName = "images/wizard/Priorities.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		super.setContent(table);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		table.getStyleClass().add("text-small-secondary");
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		
	}

}

package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.items.AvailableSlot;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemEnhancementValue;
import org.prelle.shadowrun6.items.ItemHook;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class CarriedItemOrItemEnhancementValueListCell extends ListCell<Object> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".items");

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private final static String NORMAL_STYLE = "carrieditem-cell";

	private EquipmentController charGen;
	private ListView<Object> parent;

	private GridPane layout;
	private Label name;
	private Label location;
	private HBox  locationLine;
	private Label price;
	private Button btnEdit;

	//-------------------------------------------------------------------
	public CarriedItemOrItemEnhancementValueListCell(EquipmentController charGen, ListView<Object> parent) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();

		initComponents();
		initLayoutFront();
		initStyle();
		initInteractivity();
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		price = new Label();
		price.getStyleClass().add("itemprice-label");

		// Content
		name    = new Label();
		location    = new Label();
		btnEdit = new Button("\uE1C2");
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		price.getStyleClass().add("text-secondary-info");
		name.setStyle("-fx-font-weight: bold");

		btnEdit.setStyle("-fx-background-color: transparent");

		layout.getStyleClass().add("content");
	}

	//-------------------------------------------------------------------
	private void initLayoutFront() {
		Label locLabel = new Label(Resource.get(UI, "label.item.location")+": ");
		locationLine = new HBox(10);
		locationLine.getChildren().addAll(locLabel, location);
		
		/*
		 *       |  Name              |  Edit-Button
		 *  Icon |  Location: XYZ     |  Price
		 */
		
		layout = new GridPane();
		
		layout.add(name        , 1, 0);
		layout.add(locationLine, 1, 1);
		layout.add(btnEdit , 2, 0);
		layout.add(price   , 2, 1);
		
		GridPane.setFillWidth(name, true);
		GridPane.setFillWidth(locLabel, true);
		GridPane.setHgrow(locationLine, Priority.ALWAYS);
		layout.getColumnConstraints().add(new ColumnConstraints());
//		layout.getColumnConstraints().add(new ColumnConstraints(100, 0, Double.MAX_VALUE, Priority.ALWAYS, Align));

		layout.setMaxWidth(Double.MAX_VALUE);
		name.setMaxWidth(Double.MAX_VALUE);
		locationLine.setMaxWidth(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(event -> {
			if (getItem() instanceof CarriedItem)
				editClicked((CarriedItem) this.getItem());
		});
		
		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+getItem());
		if (getItem()==null)
			return;

		//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		String id = null;
		if (getItem() instanceof CarriedItem)
			id = "gear:"+((CarriedItem) this.getItem()).getUniqueId();
		else
			id = "gearmod:"+((ItemEnhancementValue) this.getItem()).getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Object _item, boolean empty) {
		super.updateItem(_item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (_item instanceof CarriedItem) {
				locationLine.setVisible(true);
				locationLine.setManaged(true);
				CarriedItem item = (CarriedItem)_item;
				item.refreshVirtual();
				name.setText(item.getName());
				if (item.isCreatedByModification())
					name.setText(item.getName()+" (fixed)");
				price.setText("\u00A5 "+item.getAsValue(ItemAttribute.PRICE).getModifiedValue());
				ItemHook slot = item.getSlot();
				location.setText( (slot!=null) ? slot.getName() : "?");
				// TODO: can embedded item be configured itself?
				btnEdit.setVisible(false);
			} else if (_item instanceof AvailableSlot ) {
				AvailableSlot slot = (AvailableSlot)_item;
				location.setText(slot.getSlot().getName());
				List<String> names = new ArrayList<String>();
				slot.getAllEmbeddedItems().forEach(item -> names.add(item.getNameWithRating()));
				name.setText(String.join(",", names));
				if (slot.getAllEmbeddedItems().isEmpty()) {
					name.setText(Resource.get(UI, "label.item.emptySlot"));
					name.setStyle("-fx-font-weight: normal; -fx-font-style: italic");
				} else {
					name.setStyle("-fx-font-weight: bold");
				}
			} else if (_item instanceof ItemEnhancementValue ){
				locationLine.setVisible(false);
				locationLine.setManaged(true);
				ItemEnhancementValue enhance = (ItemEnhancementValue)_item;
				name.setText(enhance.getModifyable().getName());
			}
			
			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(CarriedItem data) {
		logger.debug("editClicked");

//		PowerSpecializationPane dia = new PowerSpecializationPane(charGen, data);
//
//		provider.getScreenManager().showAlertAndCall(
//				AlertType.NOTIFICATION,
//				UI.getString("powerselectpane.specialization.dialog.header"),
//				dia);
//		GenerationEventDispatcher.removeListener(dia);
//		updateItem(data, false);
	}

}
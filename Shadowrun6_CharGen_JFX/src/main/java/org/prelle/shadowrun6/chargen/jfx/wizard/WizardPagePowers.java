package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManager;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.shadowrun6.MagicOrResonanceType;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.chargen.jfx.panels.PowerDescriptionPane;
import org.prelle.shadowrun6.chargen.jfx.panels.PowerSelectionPane;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ValueField;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class WizardPagePowers extends WizardPage implements ScreenManagerProvider, GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(WizardPagePowers.class.getName());

	private CharacterGenerator charGen;

	private PowerSelectionPane powerPane;
	private PowerDescriptionPane description;
	private HBox firstLine;

	private ValueField vfPointsTotal;
	private Label lbPointsLeft;

	private HBox content;
	private WizardPointsPane side;

	//--------------------------------------------------------------------
	public WizardPagePowers(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(Resource.get(UI, "wizard.selectPowers.title"));

		powerPane = new PowerSelectionPane(charGen, this);

		description = new PowerDescriptionPane();

		vfPointsTotal = new ValueField();
		lbPointsLeft  = new Label();
//		lbPointsLeft.setStyle("-fx-font-size: 140%; -fx-text-fill: textcolor-highlight-primary");
		lbPointsLeft.getStyleClass().add("text-subheader");
		
		content = new HBox();
		content.setSpacing(20);

		String fName = "images/wizard/Spells.png";
		InputStream in = SR6Constants.class.getResourceAsStream(fName);
		if (in==null) {
			logger.warn("Missing "+fName);
		} else {
			Image img = new Image(in);
			setImage(img);
		}

		// Side
		side = new WizardPointsPane(true, false);
		float pointsSK = charGen.getPowerController().getPowerPointsLeft();
		side.setPoints(pointsSK);
		side.setPoints2Name(UI.getString("label.powerpoints"));
		side.setPoints2(charGen.getCharacter().getPowerPoints());
		side.setKarma(charGen.getCharacter().getKarmaFree());
		setSide(side);

	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label lbPPTotal = new Label(Resource.get(UI, "line.points.total"));
		Label lbPPFree  = new Label(Resource.get(UI, "line.points.free"));
		firstLine = new HBox(10,lbPPTotal, vfPointsTotal, lbPPFree, lbPointsLeft);
		firstLine.setAlignment(Pos.CENTER_LEFT);
		HBox.setMargin(lbPPFree, new Insets(0, 0, 0, 20));
		
		OptionalDescriptionPane content = new OptionalDescriptionPane(powerPane, description);
		
		VBox layout = new VBox(5, firstLine, content);
		powerPane.setMaxHeight(Double.MAX_VALUE);
		VBox.setVgrow(layout, Priority.ALWAYS);
		super.setContent(layout);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		setImageInsets(new Insets(-40,0,0,0));
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		vfPointsTotal.dec.setOnAction(ev -> charGen.getPowerController().decreasePowerPoint());
		vfPointsTotal.inc.setOnAction(ev -> charGen.getPowerController().increasePowerPoints());
		powerPane.showHelpForProperty().addListener( (ov,o,n)-> {
			description.setData(n);
		});
	}

	//-------------------------------------------------------------------
	private void refresh() {
		/*
		 * Enable or disable page
		 */
		MagicOrResonanceType type = charGen.getCharacter().getMagicOrResonanceType();
		if (type!=null) {
			// Enable or disable page
			if (type.usesPowers()) {
				logger.debug(type+" uses powers - enable page");
				activeProperty().set(true);
			} else {
				logger.debug(type+" does not use powers - disable page");
				activeProperty().set(false);
			}
		} else {
			logger.warn("No magic or resonance type selected yet");
			activeProperty().set(false);
		}

		powerPane.refresh();
		
		side.setKarma(charGen.getCharacter().getKarmaFree());
		side.setPoints(charGen.getPowerController().getPowerPointsLeft());
//		vfPointsTotal.setValue(charGen.getCharacter().getBoughtPowerPoints());
		vfPointsTotal.setValue( (int)(charGen.getPowerController().getPowerPointsLeft()+charGen.getPowerController().getPowerPointsInvested()) );
		vfPointsTotal.dec.setDisable(!charGen.getPowerController().canDecreasePowerPoints());
		vfPointsTotal.inc.setDisable(!charGen.getPowerController().canIncreasePowerPoints());
		lbPointsLeft.setText(String.valueOf(charGen.getPowerController().getPowerPointsLeft()));
		side.setPoints2(charGen.getCharacter().getPowerPoints());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case CHARACTER_CHANGED:
			logger.info("RCV "+event);
			refresh();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.ScreenManagerProvider#getScreenManager()
	 */
	@Override
	public ScreenManager getScreenManager() {
		return getWizard().getScreenManager();
	}

}

/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.FontIcon;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.dialogs.EditCarriedItemDialog;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.items.ItemUtilJFX;

import javafx.geometry.Insets;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class BigCarriedItemListCell extends ListCell<CarriedItem> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private final static String NORMAL_STYLE = "carrieditem-cell";

	private CharacterController charGen;
	private ListView<CarriedItem> parent;
	private ScreenManagerProvider provider;

	private transient CarriedItem data;

	private Label lblName;
	private Label lblPrice2;
	private VBox dataLayout;
	private HBox layout;
	private HBox bxAction;
	private VBox bxIncDec;
	
	private Button btnInc;
	private Button btnDec;
	private Button btnEdit;

	//-------------------------------------------------------------------
	public BigCarriedItemListCell(CharacterController charGen, ListView<CarriedItem> parent, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		if (provider==null)
			throw new NullPointerException("ScreenManagerProvider");
		this.provider = provider;

		getStyleClass().add(NORMAL_STYLE);
		initComponents();
//		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblName = new Label();
		lblName.getStyleClass().add("text-small-subheader");

		dataLayout = new VBox();
		dataLayout.getChildren().add(lblName);

		/*
		 * per Item Action Bar
		 */
		FontIcon icoPopup  = new FontIcon("\uE17E\uE107");
		FontIcon icoDelete = new FontIcon("\uE17E\uE107");
		FontIcon icoSell   = new FontIcon("\uE17E \u00A5");
		FontIcon icoUndo   = new FontIcon("\uE17E\uE10E");
		btnEdit   = new Button(null, new FontIcon("\uE17E\uE104"));
		Button btnPopup  = new Button(null, icoPopup);
//		Button btnDelete = new Button(null, icoDelete);
//		Button btnSell   = new Button(null, icoSell);
//		Button btnUndo   = new Button(null, icoUndo);
		btnEdit.getStyleClass().add("mini-button");
//		btnEdit.setStyle("-fx-padding-right: 20px");
		btnPopup.getStyleClass().add("mini-button");
		btnPopup.setStyle("-fx-border-width: 20px");
		icoDelete.setStyle("-fx-padding: 3px");
		icoSell.setStyle("-fx-padding: 3px");
		icoUndo.setStyle("-fx-padding: 3px");

		MenuItem menUndo = new MenuItem(UI.getString("label.removeitem.undo"), icoUndo);
		MenuItem menSell = new MenuItem(UI.getString("label.removeitem.sell"), icoSell);
		MenuItem menDelete = new MenuItem(UI.getString("label.removeitem.trash"), icoDelete);
		ContextMenu menu = new ContextMenu();
		menu.getItems().add(menUndo);
		menu.getItems().add(menSell);
		menu.getItems().add(menDelete);
		btnPopup.setContextMenu(menu);
//		btnPopup.setOnMouseEntered(event -> {
//			logger.debug("onMouseEntered");
//			menu.show(btnPopup, Side.BOTTOM, 0, 0);
//			});

		btnPopup.setOnAction(event -> menu.show(btnPopup, Side.BOTTOM, 0, 0));
		btnEdit.setOnAction(event -> openEditDialog(data));
		menDelete.setOnAction(event -> {logger.debug("Trash "+data); charGen.getEquipmentController().sell(data, 0f);});
		menSell.setOnAction  (event -> {logger.debug("Sell  "+data); charGen.getEquipmentController().sell(data, 0.5f);});
		menUndo.setOnAction  (event -> {logger.debug("Undo  "+data); charGen.getEquipmentController().deselect(data);});

		VBox bxButtons = new VBox();
		bxButtons.getChildren().addAll(btnEdit, btnPopup);
		bxButtons.setStyle("-fx-spacing: 1.0em; -fx-padding: 0.4em 0.55em 0em 0.55em;");
		
		/*
		 * Decrement / Increment buttons
		 */
		btnInc = new Button(null, new FontIcon("\uE17E\uE109"));
		btnInc.getStyleClass().add("mini-button");
		btnInc.setOnAction( ev -> charGen.getEquipmentController().increase(BigCarriedItemListCell.this.getItem()));
		btnDec = new Button(null, new FontIcon("\uE17E\uE108"));
		btnDec.getStyleClass().add("mini-button");
		btnDec.setOnAction( ev -> charGen.getEquipmentController().decrease(BigCarriedItemListCell.this.getItem()));
		bxIncDec = new VBox();
		bxIncDec.setStyle("-fx-spacing: 1.0em; -fx-padding: 0.4em 0.55em 0em 0.55em;");
		bxIncDec.getChildren().addAll(btnInc, btnDec);
		
		

		lblPrice2 = new Label();
		lblPrice2.setRotate(90);
//		lblPrice2.getStyleClass().add("itemprice-label");
		Group grp = new Group();
		grp.getChildren().add(lblPrice2);

		bxAction = new HBox();
		bxAction.getStyleClass().add("action-bar");
		bxAction.getChildren().addAll(bxButtons, grp);
		bxAction.setStyle("-fx-spacing: 0.2em");



		layout = new HBox();
		HBox.setHgrow(dataLayout, Priority.ALWAYS);
		layout.getChildren().addAll(dataLayout, bxIncDec, bxAction);
		HBox.setMargin(btnInc, new Insets(0, 20, 0, 20));

		layout.prefWidthProperty().bind(parent.widthProperty().subtract(50));
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CarriedItem item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (item.getCount()==1)
				lblName.setText(item.getNameWithRating());
			else
				lblName.setText(item.getNameWithRating()+"  ("+item.getCount()+"x)");
			// Special handling for some item types
			switch (item.getUsedAsType()) {
			case AMMUNITION:
				lblName.setText(item.getItem().getName());
				break;
			}
			
			int price = item.getAsValue(ItemAttribute.PRICE).getModifiedValue();
			price = Math.round(ShadowrunTools.getCostWithMetatypeModifier(charGen.getCharacter(), item.getItem(), price));
			lblPrice2.setText(String.valueOf(price)+" \u00A5");
			dataLayout.getChildren().retainAll(lblName);
			dataLayout.getChildren().add(ItemUtilJFX.getItemInfoNode(data, charGen));
			bxAction.setVisible(!item.isCreatedByModification());
			
			btnInc.setDisable(!charGen.getEquipmentController().canChangeCount(item, item.getCount()+1));
			btnDec.setDisable(!charGen.getEquipmentController().canChangeCount(item, item.getCount()-1));
			bxIncDec.setVisible(item.getItem().isCountable());
			bxIncDec.setManaged(item.getItem().isCountable());
			
			// If an item does not have any accessory slots, it is not editable
			btnEdit.setDisable(item.getSlots().isEmpty());
			btnEdit.setVisible(!item.getSlots().isEmpty());

			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	private void openEditDialog(CarriedItem data) {
		logger.debug("User clicked edit button");
		logger.debug("slots of "+data+" = "+data.getSlots());
		logger.debug("slots of raw "+data.getItem()+" = "+data.getItem().getSlots());
		data.refreshVirtual();
		EditCarriedItemDialog dialog = new EditCarriedItemDialog(charGen, data, provider);
		CloseType result = (CloseType) provider.getScreenManager().showAlertAndCall(dialog, dialog.getButtonControl());

		if (result==CloseType.OK) {
			this.requestLayout();
		}
	}

}

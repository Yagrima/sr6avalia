/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.SkillSection;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class SkillSpecializationSelectionPane extends VBox implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".skills");

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private CharacterController ctrl;
	private SkillValue skillVal;

	private CheckBox[] cbSpecial;
	private CheckBox[] cbExpert;
	private Label[] lbName;
	
	private boolean refresh;

	//--------------------------------------------------------------------
	public SkillSpecializationSelectionPane(CharacterController control, SkillValue sval) {
		this.ctrl = control;
		this.skillVal = sval;
		initCompoments();
		initLayout();
		initInteractivity();
		
		refresh();
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		
		List<SkillSpecialization> allSpecs = skillVal.getModifyable().getSpecializations();
		lbName    = new Label[allSpecs.size()];
		cbSpecial = new CheckBox[allSpecs.size()];
		cbExpert  = new CheckBox[allSpecs.size()];
		
		int i=-1;
		for (SkillSpecialization spec : allSpecs) {
			i++;
			lbName   [i] = new Label(spec.getName());
			lbName   [i].setUserData(spec);
			cbSpecial[i] = new CheckBox();
			cbExpert [i] = new CheckBox();
		}
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		Label lbExplain = new Label(Resource.get(RES, "dialog.skillspec.explain"));
		lbExplain.setWrapText(true);
		
		Label hdSpecial= new Label("Special.");
		Label hdExpert = new Label("Expert.");
		hdSpecial.setRotate(-90);
		hdExpert.setRotate(-90);
		Group gSpec = new Group(hdSpecial);
		Group gExp  = new Group(hdExpert);
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-hgap: 1em; -fx-vgap: 0.5em;");
		
		grid.add(gSpec, 1, 0);
		if (!skillVal.getModifyable().getId().equals("exotic_weapons"))
			grid.add(gExp , 2, 0);
		for (int i=0; i<lbName.length; i++) {
			grid.add(   lbName[i], 0, i+1);
			grid.add(cbSpecial[i], 1, i+1);
			if (!skillVal.getModifyable().getId().equals("exotic_weapons"))
				grid.add( cbExpert[i], 2, i+1);
		}
		
		if (!skillVal.getModifyable().getId().equals("exotic_weapons"))
			getChildren().add(lbExplain);
		getChildren().add(grid);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		for (int i=0; i<lbName.length; i++) {
			final int pos = i;
			cbSpecial[i].selectedProperty().addListener( (ov,o,n) -> {
				if (refresh)
					return;
				// If selected, deselect - otherwise select
				logger.debug("Clicked on special "+lbName[pos].getUserData()+"  new selection state is "+cbSpecial[pos].isSelected()+"     "+o+" --> "+n);
				if (n) { 
					ctrl.getSkillController().select(skillVal, (SkillSpecialization)lbName[pos].getUserData(), false);
				} else {
					ctrl.getSkillController().deselect(skillVal, (SkillSpecialization)lbName[pos].getUserData(), false);
				}
			});
			cbExpert[i].setOnAction(ev -> {
				// If selected, deselect - otherwise select
				logger.debug("Clicked on expertise "+lbName[pos].getUserData()+"  new selection state is "+cbExpert[pos].isSelected());
				if (!cbExpert[pos].isSelected()) {// Was previously not selected
					ctrl.getSkillController().deselect(skillVal, (SkillSpecialization)lbName[pos].getUserData(), true);
				} else {
					ctrl.getSkillController().select(skillVal, (SkillSpecialization)lbName[pos].getUserData(), true);
				}
			});
		}
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		logger.trace("refresh");
		this.refresh = true;
		try {
			for (int i=0; i<lbName.length; i++) {
				SkillSpecialization spec = (SkillSpecialization) lbName[i].getUserData();
				cbSpecial[i].setSelected(skillVal.hasSpecialization(spec));
				if (skillVal.hasSpecialization(spec)) {
					SkillSpecializationValue sVal = skillVal.getSpecialization(spec);
					cbSpecial[i].setUserData(sVal);
					cbSpecial[i].setDisable(!ctrl.getSkillController().canBeDeselected(skillVal, spec, false));
					if (sVal.isExpertise()) {
						cbExpert[i].setUserData(sVal);
						cbExpert[i].setSelected(true);
						cbExpert[i].setDisable(!ctrl.getSkillController().canBeDeselected(skillVal, spec, true));
					} else {
						cbExpert[i].setUserData(null);
						cbExpert[i].setSelected(false);
						cbExpert[i].setDisable(!ctrl.getSkillController().canBeSelected(skillVal, spec, true));
					}
				} else {
					cbSpecial[i].setUserData(null);
					cbSpecial[i].setDisable(!ctrl.getSkillController().canBeSelected(skillVal, spec, false));
					cbExpert[i].setUserData(null);
					cbExpert[i].setSelected(false);
					cbExpert[i].setDisable(true);
				}
			}
		} finally {
			refresh = false;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun6.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun6.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		refresh();
	}

}

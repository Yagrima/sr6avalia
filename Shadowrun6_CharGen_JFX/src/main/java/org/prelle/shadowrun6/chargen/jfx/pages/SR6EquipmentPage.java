package org.prelle.shadowrun6.chargen.jfx.pages;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.GearSection;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.ViewMode;
import org.prelle.shadowrun6.jfx.fluent.CharacterViewScreenSR6Fluent;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.genericrpg.ToDoElement;

/**
 * @author Stefan Prelle
 *
 */
public class SR6EquipmentPage extends SR6ManagedScreenPage {

	private static PropertyResourceBundle COMMON = (PropertyResourceBundle) ResourceBundle.getBundle(SR6Constants.class.getName());
	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SR6EquipmentPage.class.getName());

	private GearSection  weapons;
	private GearSection  armor;
	private GearSection  otherGear;
	private GearSection  ammunition;
	
	private Section secArmorAndOther;

	//-------------------------------------------------------------------
	public SR6EquipmentPage(CharacterController control, ViewMode mode, CharacterHandle handle, CharacterViewScreenSR6Fluent provider) {
		super(control, mode, handle, provider);
		this.setId("shadowrun-equipment");
		this.setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.gear"));
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;

		initComponents();
		initCommandBar();
		expLine = new SR6EquipmentFirstLine(mode, control, provider);
		getCommandBar().setContent(expLine);
		
		refresh();
	}

	//-------------------------------------------------------------------
	private void initCommandBar() {
	}

	//-------------------------------------------------------------------
	private void initWeapons() {
		weapons = new GearSection(UI.getString("section.weapons"), control, provider, ItemType.WEAPON);

		getSectionList().add(weapons);

		// Interactivity
		weapons.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		weapons.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				armor.getSelectionModel().clearSelection();
				otherGear.getSelectionModel().clearSelection();
				ammunition.getSelectionModel().clearSelection();
			}
		});
	}

	//-------------------------------------------------------------------
	private void initArmorAndOther() {
		armor      = new GearSection(Resource.get(UI,"section.armor"), control, provider, ItemType.ARMOR);
		otherGear  = new GearSection(Resource.get(UI,"section.other"), control, provider, ItemType.CHEMICALS, ItemType.SURVIVAL, ItemType.BIOLOGY, ItemType.TOOLS, ItemType.MAGICAL);

		secArmorAndOther = new DoubleSection(armor, otherGear);
		getSectionList().add(secArmorAndOther);

		// Interactivity
		armor.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		otherGear.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		armor.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				weapons.getSelectionModel().clearSelection();
				otherGear.getSelectionModel().clearSelection();
				ammunition.getSelectionModel().clearSelection();
			}
		});
		otherGear.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				weapons.getSelectionModel().clearSelection();
				armor.getSelectionModel().clearSelection();
				ammunition.getSelectionModel().clearSelection();
			}
		});
	}

	//-------------------------------------------------------------------
	private void initAmmunition() {
		ammunition = new GearSection(Resource.get(UI,"section.ammunition"), control, provider, ItemType.AMMUNITION);

		getSectionList().add(new DoubleSection(ammunition, null));

		// Interactivity
		ammunition.showHelpForProperty().addListener( (ov,o,n) -> updateHelp(n));
		ammunition.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
				weapons.getSelectionModel().clearSelection();
				armor.getSelectionModel().clearSelection();
				otherGear.getSelectionModel().clearSelection();
			}
		});
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setPointsNameProperty(COMMON.getString("label.ep.karma"));

		initWeapons();
		initArmorAndOther();
		initAmmunition();
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductNameShort()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

	//--------------------------------------------------------------------
	public void refresh() {
		super.refresh();
		this.setTitle(control.getCharacter().getName()+" - "+Resource.get(UI,"title.gear"));
		secArmorAndOther.getToDoList().clear();
		for (ToDoElement tmp : control.getEquipmentController().getToDos()) {
			secArmorAndOther.getToDoList().add(tmp);
		}
	}

}

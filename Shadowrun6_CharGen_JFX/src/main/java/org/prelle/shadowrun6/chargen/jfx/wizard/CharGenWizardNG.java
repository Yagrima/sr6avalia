/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.wizard;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.gen.CharacterGenerator;
import org.prelle.shadowrun6.gen.CharacterGeneratorRegistry;
import org.prelle.shadowrun6.gen.WizardPageType;
import org.prelle.shadowrun6.gen.event.GenerationEvent;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.gen.event.GenerationEventListener;
import org.prelle.shadowrun6.gen.event.GenerationEventType;
import org.prelle.shadowrun6.jfx.SR6Constants;

/**
 * @author prelle
 *
 */
public class CharGenWizardNG extends Wizard implements GenerationEventListener {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterGenerator charGen;
	private ShadowrunCharacter generated;

	//-------------------------------------------------------------------
	/**
	 * @param nodes
	 */
	public CharGenWizardNG(ShadowrunCharacter model, boolean showGeneratorPage) {
		if (showGeneratorPage)
		getPages().addAll(
				new WizardPageGenerator(this, model)
//				new WizardPageCharacterConcept(this,charGen)
				);

		// Add new pages
		charGen = CharacterGeneratorRegistry.getSelected();
		logger.debug("  create pages and add to listener from "+charGen);
		for (WizardPageType type : charGen.getWizardPages()) {
			WizardPage page = createPage(type, charGen);
			if (page!=null) {
				logger.info("  Add page for "+type+" and generator "+charGen);
				getPages().add(page);
				if (page instanceof GenerationEventListener)
					GenerationEventDispatcher.addListener((GenerationEventListener) page);
			}
		}

		GenerationEventDispatcher.addListener(this);

		addExtraButton(CloseType.SAVE, ev -> save());

		this.setConfirmCancelCallback( wizardParam -> {
			CloseType answer = getScreenManager().showAlertAndCall(
					AlertType.CONFIRMATION,
					SR6Constants.RES.getString("wizard.cancelconfirm.header"),
					SR6Constants.RES.getString("wizard.cancelconfirm.content"));
			return answer==CloseType.OK || answer==CloseType.YES;
		});

	}

	//-------------------------------------------------------------------
	public WizardPage createPage(WizardPageType type, CharacterGenerator charGen) {
		if (charGen==null)
			throw new NullPointerException("CharacterGenerator not set");

		switch (type) {
		case PRIORITIES: return new WizardPagePriorities(this,charGen);
		case METATYPE  : return new WizardPageMetaType(this,charGen);
		case ATTRIBUTES: return new WizardPageAttributes(this,charGen);
		case SKILLS    : return new WizardPageSkills(this,charGen);
		case ALCHEMY   : return new WizardPageSpells(this,charGen, true);
		case MAGIC_OR_RESONANCE: return new WizardPageMagicOrResonance(this,charGen);
		case SPELLS    : return new WizardPageSpells(this,charGen, false);
		case RITUALS   : return new WizardPageRituals(this,charGen);
		case QUALITIES : return new WizardPageQualities(this,charGen);
		case POWERS    : return new WizardPagePowers(this,charGen);
		case COMPLEX_FORMS: return new WizardPageComplexForms(this,charGen);
		case NAME      : return new WizardPageName(this,charGen);
		default:
			logger.error("Don't know how to create page for "+type);
			throw new IllegalArgumentException("Don't know how to create page for "+type);
		}
	}

	//-------------------------------------------------------------------
	public ShadowrunCharacter getGenerated() {
		return generated;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.shadowrun.gen.event.GenerationEventListener#handleGenerationEvent(org.prelle.shadowrun.gen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.CONSTRUCTIONKIT_CHANGED) {
			logger.debug("RCV "+event.getType());
			charGen = (CharacterGenerator) event.getKey();
			logger.info("New construction kit = "+charGen);
			// Clear all pages above 1 (2 if character concepts are used)
			logger.debug("  remove all pages from listener");
			while (getPages().size()>1) {
				WizardPage removed = getPages().remove(1);
				if (removed instanceof GenerationEventListener)
					GenerationEventDispatcher.removeListener((GenerationEventListener) removed);
			}
			// Add new pages
			logger.debug("  create pages and add to listener");
			for (WizardPageType type : charGen.getWizardPages()) {
				WizardPage page = createPage(type, charGen);
				if (page!=null) {
					logger.info("  Add page for "+type);
					getPages().add(page);
					if (page instanceof GenerationEventListener)
						GenerationEventDispatcher.addListener((GenerationEventListener) page);
				}
			}
//		} else if (event.getType()==GenerationEventType.CHARACTER_CHANGED) {
//			getPages().forEach(pg -> pg.refresh());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.javafx.Wizard#canBeFinished()
	 */
	@Override
	public boolean canBeFinished() {
		if (charGen!=null)
			return charGen.hasEnoughDataIgnoreMoney();
		logger.warn("Cannot finish - no chargen set");
		return false;
	}

	//--------------------------------------------------------------------
	@Override
	public boolean isDisabled(CloseType save) {
		return charGen==null;
	}

	//-------------------------------------------------------------------
	private void save() {
		logger.info("User chose to save during generation");
		try {
			charGen.saveCreation();
			getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, 
					Resource.get(SR6Constants.RES, "dialog.saved_on_creation.title"), 
					Resource.format(SR6Constants.RES, "dialog.saved_on_creation.message", charGen.getCharacter().getName()));
		} catch (IOException e) {
			StringWriter out = new StringWriter();
			e.printStackTrace(new PrintWriter(out));
			getScreenManager().showAlertAndCall(
					AlertType.ERROR,
					Resource.get(SR6Constants.RES,"error.chargen.save.title"),
					Resource.get(SR6Constants.RES,"error.chargen.save.content")+"\n"+out
					);
		}
	}

}

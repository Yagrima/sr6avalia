/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.stream.Collectors;

import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.charctrl.EquipmentController;
import org.prelle.shadowrun6.items.Availability;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemAttribute;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.items.UseAs;

import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

/**
 * @author prelle
 *
 */
public class ItemTemplateListCell2 extends ListCell<ItemTemplate> {

	private final static String NORMAL_STYLE = "equipment-cell";
	private final static String NOT_MET_STYLE = "requirement-not-met-text";

	private EquipmentController control;
	private ShadowrunCharacter model;
	private boolean asAccessory;
	private ItemTemplate data;
	private CarriedItem embedIn;
	private ItemType type;

	private Label lblName;
	private Label lblPrice;
	private Label lblDesc;
	private VBox layout;

	//-------------------------------------------------------------------
	/**
	 * @param asAccessory The item shall be treated as an accessory
	 */
	public ItemTemplateListCell2(ShadowrunCharacter model, EquipmentController ctrl, boolean asAccessory, CarriedItem embedIn, ItemType forceType) {
		this.control = ctrl;
		this.embedIn = embedIn;
		this.model   = model;
		this.type    = forceType;
		lblName = new Label();
		lblName.setStyle("-fx-font-weight: bold;");
		lblName.setMaxWidth(Double.MAX_VALUE);

		Region buffer = new Region();
		buffer.setMaxWidth(Double.MAX_VALUE);

		lblPrice = new Label();
		lblPrice.getStyleClass().add("itemprice-label");

		lblDesc = new Label();
//		lblAvail.setStyle("-fx-pref-width: 4em");
//		lblAvail.getStyleClass().add("itemprice-label");

		HBox line1 = new HBox(lblName, lblPrice);
		HBox.setHgrow(lblName, Priority.ALWAYS);
		
		layout = new VBox();
		layout.getChildren().addAll(line1, lblDesc);
		layout.setMaxWidth(Double.MAX_VALUE);
		this.setOnDragDetected(event -> dragStarted(event));
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ItemTemplate item, boolean empty) {
		super.updateItem(item, empty);
		data = item;

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			lblName.setText(((ItemTemplate)item).getName());
			
			StringBuffer desc = new StringBuffer();
			Availability avail = item.getAvailability();
			UseAs usage = null;
			if (embedIn!=null) {
				usage = item.getBestAccessoryUsage(embedIn.getSlots().stream().map(as -> as.getSlot()).collect(Collectors.toList()));
				lblPrice.setText(item.getPriceString(model, usage));
			} else if (type!=null) {
				usage = item.getBestAccessoryUsage(type);
				lblPrice.setText(item.getPriceString(model, usage));
			} else {
				usage = item.getDefaultUsage();
				lblPrice.setText(item.getPriceString(model, usage));
			}
			if (usage!=null) {
				if (usage.getAlternateAvailability()!=null)
					avail = usage.getAlternateAvailability();
				if (usage.getEssence()>0) {
					desc.append("    "+ItemAttribute.ESSENCECOST.getShortName()+": "+usage.getEssence());
				}
				if (usage.getCapacity()>0) {
					desc.append("    "+ItemAttribute.CAPACITY.getShortName()+": "+usage.getCapacity());
				}
//				if (usage.getSlot()!=null) {
//					desc.append("    "+usage.getSlot().getName());
//				}
			}			
			
			desc.insert(0,ItemAttribute.AVAILABILITY.getShortName()+":"+avail.toString());
			
			lblDesc.setText(desc.toString());
			if (control.canBePayed(item, asAccessory)) {
				lblPrice.getStyleClass().remove(NOT_MET_STYLE);
			} else {
				lblPrice.getStyleClass().add(NOT_MET_STYLE);
			}
			setGraphic(layout);
		}
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		if (data==null)
			return;
		content.putString("gear:"+data.getId());
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();
	}


}

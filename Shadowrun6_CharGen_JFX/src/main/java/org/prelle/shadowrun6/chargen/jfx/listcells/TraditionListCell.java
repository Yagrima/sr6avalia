package org.prelle.shadowrun6.chargen.jfx.listcells;

import org.prelle.shadowrun6.Tradition;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class TraditionListCell extends ListCell<Tradition> {
	
	private ImageView iview;
	private Label lblName;
	private Label lblLine1;
	private Label lblLine2;
	
	private HBox bxLayout;
	private VBox bxData;
	
	//-------------------------------------------------------------------
	public TraditionListCell() {
		initComponents();
		initLayout();
		initStyle();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		lblName  = new Label();
		lblLine1 = new Label();
		lblLine2 = new Label();
		
		iview = new ImageView();
		iview.setFitHeight(48);
		iview.setFitWidth(48);
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		bxData = new VBox();
		bxData.getChildren().addAll(lblName, lblLine1, lblLine2);
		
		bxLayout = new HBox(5);
		bxLayout.getChildren().addAll(iview, bxData);
	}
	
	//-------------------------------------------------------------------
	private void initStyle() {
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Tradition item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(bxLayout);
			lblName.setText(item.getName().toUpperCase());
			lblName.setStyle("-fx-font-family: ArmaduraSolidW00-Regular; ");
			lblLine1.setText(makeShortInfoString(item));
//			iview.setImage(IMAGES.get(item.getCategory()));
		}
	}
	
	static String makeShortInfoString(Tradition item) {
		StringBuffer buf = new StringBuffer();
		buf.append(item.getDrainAttribute1().getName());
		return buf.toString();
	}
}
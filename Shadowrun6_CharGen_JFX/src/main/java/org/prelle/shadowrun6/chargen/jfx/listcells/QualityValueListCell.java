package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.ArrayList;
import java.util.List;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.MentorSpirit;
import org.prelle.shadowrun6.Quality;
import org.prelle.shadowrun6.QualityValue;
import org.prelle.shadowrun6.QualityValue.MentorSpiritMods;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.charctrl.ProcessorRunner;
import org.prelle.shadowrun6.charctrl.QualityController;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.modifications.ModificationChoice;

import de.rpgframework.genericrpg.modification.Modification;
import javafx.application.Platform;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

public class QualityValueListCell extends ListCell<QualityValue> {

	private static PropertyResourceBundle UI = SR6Constants.RES;

	private final static String NORMAL_STYLE = "quality-cell";

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".qualities");

	private ProcessorRunner parentCharGen;
	private QualityController charGen;
	private ListView<QualityValue> parent;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private HBox layout;
	private Label name;
	private Label total;
	private Button btnEdit;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	private SymbolIcon lblLock;
	private ImageView imgRecommended;

	private TilePane tiles;

	private Label  tfDescr;
	private ChoiceBox<QualityValue.MentorSpiritMods> cbMystic;
	private ChoiceBox<Modification> cbMod;
	private HBox line4;

	//-------------------------------------------------------------------
	public QualityValueListCell(ProcessorRunner parentCharGen, QualityController charGen, ShadowrunCharacter model, ListView<QualityValue> parent, ScreenManagerProvider provider) {
		this.parentCharGen = parentCharGen;
		this.charGen = charGen;
		this.parent  = parent;
		if (parent==null)
			throw new NullPointerException();
		this.model    = model;
		this.provider = provider;

		// Content		
		layout  = new HBox(5);
		name    = new Label();
		total   = new Label();
		tfDescr = new Label();
		btnEdit = new Button("\uE1C2");
		btnEdit.getStyleClass().add("mini-button");
		btnDec  = new Button("\uE0C6");
		btnDec.getStyleClass().add("mini-button");
		lblVal  = new Label("?");
		btnInc  = new Button("\uE0C5");
		btnDec.getStyleClass().add("mini-button");
		// Line 3
		cbMystic= new ChoiceBox<>();
		cbMystic.getItems().addAll(MentorSpiritMods.values());
		cbMystic.setConverter(new StringConverter<QualityValue.MentorSpiritMods>() {
			public String toString(MentorSpiritMods value) {
				return Resource.get(UI, "mentorspirit.mystic.option."+value.name().toLowerCase());
			}
			public MentorSpiritMods fromString(String value) {return null;}
		});
		// Line 4
		cbMod   = new ChoiceBox<>();
		cbMod.setConverter(new StringConverter<Modification>() {
			public Modification fromString(String arg0) {return null;}
			public String toString(Modification mod) {
				return ShadowrunTools.getModificationString(mod);
			}
		});

		// Recommended icon
		imgRecommended = new ImageView(new Image(SR6Constants.class.getResourceAsStream("images/recommendation.png")));
		imgRecommended.setFitHeight(16);
		imgRecommended.setFitWidth(16);
		lblLock = new SymbolIcon("lock");
		lblLock.setMaxWidth(50);

		initStyle();
		initLayout();
		initInteractivity();
		
		getStyleClass().add("qualityvalue-list-cell");
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		total.getStyleClass().add("text-secondary-info");
		btnInc.getStyleClass().add("mini-button");
		btnDec.getStyleClass().add("mini-button");
//		btnDec.setStyle("-fx-background-color: transparent; ");
//		btnInc.setStyle("-fx-background-color: transparent; -fx-border-color: -fx-outer-border;");
		name.setStyle("-fx-font-weight: bold");
		lblVal.getStyleClass().add("text-subheader");
		//		lblVal.setStyle("-fx-text-fill: -fx-focus-color");

		btnEdit.setStyle("-fx-background-color: transparent; -fx-border-color: -fx-outer-border;");

		//		setStyle("-fx-pref-width: 24em");
//		layout.getStyleClass().add("content");		
		
		getStyleClass().add(NORMAL_STYLE);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line1 = new HBox(10);
		line1.getChildren().addAll(name, lblLock);		

		HBox line2 = new HBox(5);
		line2.getChildren().addAll(btnEdit, tfDescr, cbMystic);
		line2.setAlignment(Pos.CENTER_LEFT);

		line4 = new HBox(5);
		line4.getChildren().addAll(cbMod);

		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(line1, line2, line4);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(bxCenter, tiles);

		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);


		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {
			logger.debug("Increase "+this.getItem());
			charGen.increase(this.getItem());
			updateItem(this.getItem(), false); 
			parent.refresh();
		});
		btnDec .setOnAction(event -> {
			logger.debug("Decrease "+this.getItem());
			logger.debug("Call "+charGen.getClass());
			charGen.decrease(this.getItem());
			updateItem(this.getItem(), false); 
			parent.refresh();
		});
		btnEdit.setOnAction(event -> {
			editClicked(this.getItem());
		});
		cbMystic.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==getItem().getMysticAdeptUses())
				return;
			getItem().setMysticAdeptUses(n);
			logger.debug("Treat mentor spirit as "+n);
			Platform.runLater( () -> parentCharGen.runProcessors());			
		});
		cbMod.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			ModificationChoice cmod = (ModificationChoice)cbMod.getUserData();
			if (n!=null && n!=model.getDecision(cmod)) {
				logger.debug("Switch ModificationChoice selection to "+n);
				model.decide(cmod, n);
				parentCharGen.runProcessors();
			}
		});
		

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+this.getItem());
		if (this.getItem()==null)
			return;

		//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
		//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
		//			return;

		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
		/* allow any transfer mode */
		Dragboard db = source.startDragAndDrop(TransferMode.ANY);

		/* Put a string on a dragboard */
		ClipboardContent content = new ClipboardContent();
		//        String id = "skill:"+data.getModifyable().getId()+"|desc="+data.getDescription()+"|idref="+data.getIdReference();
		String id = "qualityval:"+this.getItem().getModifyable().getId();
		content.putString(id);
		db.setContent(content);

		/* Drag image */
		WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
		db.setDragView(snapshot);

		event.consume();	
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(QualityValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty || item==null) {
			setText(null);
			setGraphic(null);			
		} else {
			Quality data = item.getModifyable();
			
			boolean isMentorSpirit = data.getSelect()==ChoiceType.MENTOR_SPIRIT;
			// Toggle Line 3
			cbMystic.setVisible( isMentorSpirit && model.getMagicOrResonanceType().paysPowers() );
			cbMystic.setManaged( isMentorSpirit && model.getMagicOrResonanceType().paysPowers() );
			cbMystic.setValue(item.getMysticAdeptUses());
			// Toggle Line 4
			boolean line4Needed = false;
//			System.err.println("QualityValueListCell.update----"+data+"---------------");
			if (isMentorSpirit) {
				List<Modification> allMods = new ArrayList<>(item.getModifications());
				MentorSpirit spirit = (MentorSpirit)item.getChoice();
				if (model.getMagicOrResonanceType()!=null) {
					if (model.getMagicOrResonanceType().paysPowers()) {
						// Mystic adept
						if (item.getMysticAdeptUses()==MentorSpiritMods.ADEPT) {
							allMods.addAll(spirit.getAdeptModifications());						
						} else if (item.getMysticAdeptUses()==MentorSpiritMods.MAGICIAN) {
							allMods.addAll(spirit.getMagicianModifications());							
						}
					} else if (model.getMagicOrResonanceType().usesPowers()) {
						// Adept
						allMods.addAll(spirit.getAdeptModifications());						
					} else if (model.getMagicOrResonanceType().usesSpells()) {
						// Magician
						allMods.addAll(spirit.getMagicianModifications());
					}						
				} // allMods depending on MagicOrResonance
				
				for (Modification mod : allMods) {
					if (mod instanceof ModificationChoice) {
						ModificationChoice cmod = (ModificationChoice)mod;
						line4Needed = true;
						cbMod.getItems().clear();
						cbMod.setUserData(cmod);
						cbMod.getItems().addAll(cmod);
						// See if the char already made a choice
						Modification selected = model.getDecision(cmod);
						cbMod.setValue(selected);
					}
				}		
			}
			line4.setVisible( line4Needed );
			line4.setManaged( line4Needed );
			
			
			name.setText(item.getName());
//			attrib.setText(attr.getShortName()+" "+charGen.getModel().getAttribute(attr).getModifiedValue());
			//			tfDescr.setText(item.getModifyable().getAttribute1().getName());
			tfDescr.setText(item.getNameSecondLine());
			imgRecommended.setVisible(charGen.isRecommended(item.getModifyable()));
			boolean removeable = data.isFreeSelectable() && !model.getRacialQualities().contains(item);
			lblLock.setVisible( !removeable);
//			lblType.setText(model.getRacialQualities().contains(item)?Resource.get(UI,"label.racial_quality"):null);
			if (data.isFreeSelectable()) {
				btnEdit.setText("\uE1C2");
//				btnEdit.setTooltip(new Tooltip(UI.getString("skillvaluelistview.tooltip.special")));
			}
			btnEdit.setVisible(data.getSelect()==ChoiceType.NAME);
//			logger.debug("Item = "+item);
//			logger.debug("Data = "+data);
			tiles.setVisible(data.getMax()>1 && data.isFreeSelectable());
			lblVal.setText(" "+String.valueOf(item.getPoints())+" ");
//			int sum = item.getModifiedValue();
//			total.setText(String.format(UI.getString("skillvaluelistview.skillvaluelistcell.total"), sum));
			tiles.setVisible(data.getMax()>1);
			tiles.setManaged(data.getMax()>1);
			
			btnDec.setDisable(!charGen.canBeDecreased(item));
			btnInc.setDisable(!charGen.canBeIncreased(item));
			setGraphic(layout);
			
//			logger.debug("btnEdit = "+btnEdit.getStyleClass());
//			if (btnEdit.getScene()!=null)
//				logger.debug("CSS1 = "+btnEdit.getScene().getStylesheets());
//			if (getScene()!=null)
//				logger.debug("CSS2 = "+getScene().getStylesheets());
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(QualityValue ref) {
		logger.debug("editClicked");

		TextField tf = new TextField(ref.getDescription());
		// Prevent entering XML relevant chars
		tf.textProperty().addListener( (ov,o,n) -> {
			if (n.indexOf('<')>0) { n = n.substring(0, n.indexOf('<')); tf.setText(n); }
			if (n.indexOf('>')>0) { n = n.substring(0, n.indexOf('>')); tf.setText(n); }
			if (n.indexOf('"')>0) { n = n.substring(0, n.indexOf('"')); tf.setText(n); }
			if (n.indexOf('&')>0) { n = n.substring(0, n.indexOf('&')); tf.setText(n); }
		});
		tf.setOnAction(event -> {
			ManagedScreen screen = (ManagedScreen) tf.getParent().getParent().getParent().getParent();
			logger.debug("Action on "+screen);
//			screen.impl_navigClicked(CloseType.OK, event);
		});
		CloseType close = provider.getScreenManager().showAlertAndCall(
				AlertType.QUESTION, 
				Resource.get(UI,"dialog.choicetype.option.label"), 
				tf);
		if (close==CloseType.OK) {
			logger.info("Change description for "+ref);
			ref.setDescription(tf.getText());
//			ref.setChoice(tf.getText());
			tfDescr.setText(tf.getText());
			parentCharGen.runProcessors();
		}

		
//		QualitySpecializationPane dia = new QualitySpecializationPane(charGen, data);
//
//		provider.getScreenManager().showAlertAndCall(
//				AlertType.NOTIFICATION, 
//				UI.getString("skillselectpane.specialization.dialog.header"), 
//				dia);
//		GenerationEventDispatcher.removeListener(dia);
//		updateItem(data, false);
	}

}
/**
 * 
 */
package org.prelle.shadowrun6.chargen.jfx.panels;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.Spell.Category;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.charctrl.SpellController;
import org.prelle.shadowrun6.chargen.jfx.listcells.SpellListCell;
import org.prelle.shadowrun6.chargen.jfx.listcells.SpellValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class SpellSelectionDoublePane extends HBox {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SpellSelectionDoublePane.class.getName());
	
	private SpellController ctrl;
	private boolean alchemistic;
	private ScreenManagerProvider provider;
	private ShadowrunCharacter model;

	private ChoiceBox<Spell.Category> cbCategory;
	private ListView<Spell> lvAvailable;
	private ListView<SpellValue> lvSelected;
	private Label hdAvailable;
	private Label hdSelected;
	private Button btnDelKnow;
	
	private Spell selectedAvailable;

	private ObjectProperty<Spell> showHelpFor = new SimpleObjectProperty<Spell>();

	//-------------------------------------------------------------------
	/**
	 */
	public SpellSelectionDoublePane(SpellController ctrl, ScreenManagerProvider provider, boolean alchemistic) {
		this.ctrl = ctrl;
		this.provider = provider;
		this.alchemistic = alchemistic;

		initComponents();
		initLayout();
		initInteractivity();

		cbCategory.getSelectionModel().select(0);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		hdAvailable = new Label(Resource.get(UI, "label.available"));
		hdSelected  = new Label(Resource.get(UI, "label.selected"));
		hdAvailable.getStyleClass().add("text-subheader");
		hdSelected.getStyleClass().add("text-subheader");
		hdAvailable.setStyle("-fx-font-size: 120%");
		hdSelected.setStyle("-fx-font-size: 130%");

		/* Column 1 */
		cbCategory = new ChoiceBox<>();
		for (Spell.Category cat : Spell.Category.values())
			cbCategory.getItems().add(cat);
		cbCategory.setConverter(new StringConverter<Spell.Category>() {
			public String toString(Category object) { return object.getName();}
			public Category fromString(String string) { return null; }
		});
		cbCategory.setMaxWidth(Double.MAX_VALUE);

		lvAvailable = new ListView<Spell>();
		lvAvailable.setCellFactory(new Callback<ListView<Spell>, ListCell<Spell>>() {
			public ListCell<Spell> call(ListView<Spell> param) {
				SpellListCell cell =  new SpellListCell();
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.select(cell.getItem(), alchemistic);
				});
				return cell;
			}
		});
		
		/* Column 2 */
		btnDelKnow = new Button("\uE0C6");
		btnDelKnow.getStyleClass().add("mini-button");
		btnDelKnow.setDisable(false);

		lvSelected  = new ListView<SpellValue>();
		lvSelected.setCellFactory(new Callback<ListView<SpellValue>, ListCell<SpellValue>>() {
			public ListCell<SpellValue> call(ListView<SpellValue> param) {
				SpellValueListCell cell =  new SpellValueListCell(ctrl);
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.deselect(cell.getItem());
				});
				return cell;
			}
		});
		
		Label phAvailable = new Label(UI.getString("spellselectpane.placeholder.available"));
		Label phSelected  = new Label(UI.getString("spellselectpane.placeholder.selected"));
		phAvailable.setWrapText(true);
		phSelected.setWrapText(true);
		lvAvailable.setPlaceholder(phAvailable);
		lvSelected.setPlaceholder(phSelected);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		lvAvailable.setStyle("-fx-pref-width: 24em;");
		lvSelected .setStyle("-fx-pref-width: 17em");

		setSpacing(20);
		VBox column1 = new VBox(10);
		VBox.setVgrow(lvAvailable, Priority.ALWAYS);
		Region grow1 = new Region();
		grow1.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow1, Priority.ALWAYS);
		column1.getChildren().addAll(hdAvailable, cbCategory, lvAvailable);

		VBox column2 = new VBox(10);
		VBox.setVgrow(lvSelected, Priority.ALWAYS);
		Region grow2 = new Region();
		grow2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(grow2, Priority.ALWAYS);
		HBox lineKnow = new HBox(5, grow2, btnDelKnow);
		column2.getChildren().addAll(hdSelected, lineKnow, lvSelected);

		column1.setMaxHeight(Double.MAX_VALUE);
		column2.setMaxHeight(Double.MAX_VALUE);
		lvAvailable.setMaxHeight(Double.MAX_VALUE);
		lvSelected .setMaxHeight(Double.MAX_VALUE);

		getChildren().addAll(column1, column2);

		column2.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(column2, Priority.ALWAYS);

		this.setMaxHeight(Double.MAX_VALUE);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbCategory.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());
		lvAvailable.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set(n));
		lvSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> showHelpFor.set((n!=null)?n.getModifyable():null));
	}

	//--------------------------------------------------------------------
	public void refresh()  {
		lvAvailable.getItems().clear();
		if (cbCategory.getSelectionModel().getSelectedItem()==null)
			lvAvailable.getItems().addAll(ctrl.getAvailableSpells(alchemistic));
		else
			lvAvailable.getItems().addAll(ctrl.getAvailableSpells(cbCategory.getSelectionModel().getSelectedItem(), alchemistic));

		lvSelected.getItems().clear();
		if (model!=null)
			lvSelected.getItems().addAll(model.getSpells(alchemistic));

		lvSelected.setOnDragDropped(event -> dragFromSpellDropped(event));
		lvSelected.setOnDragOver(event -> dragFromSpellOver(event));
		lvAvailable.setOnDragDropped(event -> dragFromSpellValueDropped(event));
		lvAvailable.setOnDragOver(event -> dragFromSpellValueOver(event));
	}

	//--------------------------------------------------------------------
	public void setData(ShadowrunCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	private void dragFromSpellDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			String enhanceID = db.getString();
			logger.debug("Dropped "+enhanceID);

			int pos = enhanceID.indexOf(":");
			if (pos>0) {
				String head = enhanceID.substring(0, pos);
				String tail = enhanceID.substring(pos+1);
				if (head.equals("spell")) {
					Spell data = ShadowrunCore.getSpell(tail);
					if (data==null) {
						logger.warn("Cannot find quality for dropped quality id '"+tail+"'");						
					} else {
						ctrl.select(data, alchemistic);
					} // if data==null else
				}
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private Spell getSpellByEvent(DragEvent event) {
		if (!event.getDragboard().hasString())
			return null;
	
		String enhanceID = event.getDragboard().getString();
		int pos = enhanceID.indexOf(":");
		if (pos>0) {
			String head = enhanceID.substring(0, pos);
			String tail = enhanceID.substring(pos+1);
			if (head.startsWith("spell")) {
				return ShadowrunCore.getSpell(tail);
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void dragFromSpellOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Spell qual = getSpellByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	private void dragFromSpellValueDropped(DragEvent event) {
		/* if there is a string data on dragboard, read it and use it */
		Dragboard db = event.getDragboard();
		boolean success = false;
		if (db.hasString()) {
			Spell data = getSpellByEvent(event);
			if (data==null) {
				logger.warn("Cannot find quality for dropped quality");						
//			} else if (!data.isFreeSelectable()) {
//				logger.warn("Cannot deselect dropped racial quality id");						
			} else {
				event.setDropCompleted(success);
				event.consume();
				if (model.hasSpell(data.getId())) {
					SpellValue ref = model.getSpell(data.getId());
					ctrl.deselect(ref);
				}
				return;
			}
		}
		/* let the source know whether the string was successfully 
		 * transferred and used */
		event.setDropCompleted(success);

		event.consume();
	}

	//-------------------------------------------------------------------
	private void dragFromSpellValueOver(DragEvent event) {
		Node target = (Node) event.getSource();
		if (event.getGestureSource() != target && event.getDragboard().hasString()) {
			Spell qual = getSpellByEvent(event);
			if (qual!=null) {
				/* allow for both copying and moving, whatever user chooses */
				event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @return the selectedAvailable
	 */
	public Spell getSelectedAvailable() {
		return selectedAvailable;
	}
	
	//--------------------------------------------------------------------
	public ReadOnlyObjectProperty<Spell> showHelpForProperty() {
		return showHelpFor;
	}

}

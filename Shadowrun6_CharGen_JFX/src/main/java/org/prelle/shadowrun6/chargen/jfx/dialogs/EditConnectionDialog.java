/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.dialogs;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.shadowrun6.Connection;
import org.prelle.shadowrun6.Resource;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class EditConnectionDialog extends ManagedDialog {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(EditConnectionDialog.class.getName());

	private NavigButtonControl btnControl;

	private VBox colDescription;
	private TextField tfName;
	private TextField tfType;
	private TextArea  taDesc;

	private Connection selectedItem;

	//--------------------------------------------------------------------
	public EditConnectionDialog(Connection data, boolean isEdit) {
		super(UI.getString("dialog.title"), null, CloseType.APPLY);
		System.out.println("isEdit = "+isEdit);
		if (!isEdit) {
			buttons.setAll(CloseType.OK, CloseType.CANCEL);
		}
		this.selectedItem    = data;
		btnControl = new NavigButtonControl();
		
		initCompoments();
		initLayout();
		initInteractivity();
		if (isEdit)
			refresh();
	}

	//--------------------------------------------------------------------
	private void initCompoments() {
		tfName = new TextField();
		tfType = new TextField();
		taDesc = new TextArea();
		tfName.setPromptText(Resource.get(UI, "prompt.name"));
		tfType.setPromptText(Resource.get(UI, "prompt.type"));
		taDesc.setPromptText(Resource.get(UI, "prompt.desc"));
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		// Description
		Label heaName = new Label(Resource.get(UI, "label.name"));
		Label heaType = new Label(Resource.get(UI, "label.type"));
		Label heaDesc = new Label(Resource.get(UI, "label.desc"));

		GridPane descForm = new GridPane();
		descForm.add(heaName, 0, 0);
		descForm.add(tfName , 1, 0);
		descForm.add(heaType, 0, 1);
		descForm.add(tfType , 1, 1);
		descForm.setStyle("-fx-vgap: 1em; -fx-hgap: 1em");

		colDescription = new VBox();
		colDescription.getChildren().addAll(descForm, heaDesc, taDesc);
		colDescription.setMaxHeight(Double.MAX_VALUE);
		VBox.setMargin(heaDesc, new Insets(20, 0, 0, 0));

		this.setMaxHeight(Double.MAX_VALUE);
		setContent(colDescription);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		tfName.textProperty().addListener( (ov,o,n) -> {
			if (selectedItem!=null)
				selectedItem.setName(n);
			updateOKButton();
		});
		tfType.textProperty().addListener( (ov,o,n) -> {
			if (selectedItem!=null)
				selectedItem.setType(n);
			updateOKButton();
		});
		taDesc.textProperty().addListener( (ov,o,n) -> {
			if (selectedItem!=null)
				selectedItem.setDescription(n);
		});
		tfName.setOnAction(ev -> refresh());
		tfName.focusedProperty().addListener( (ov,o,n) -> refresh());
		tfType.setOnAction(ev -> refresh());
		tfType.focusedProperty().addListener( (ov,o,n) -> refresh());
	}

	//-------------------------------------------------------------------
	private void updateOKButton() {
		btnControl.setDisabled(CloseType.OK, tfName.getText()!=null && tfType.getText()!=null);
	}

	//--------------------------------------------------------------------
	private void refresh()  {
		tfName.setText(selectedItem.getName());
		tfType.setText(selectedItem.getType());
		taDesc.setText(selectedItem.getDescription());
	}

	//-------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

}

/**
 *
 */
package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.Collections;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.NavigButtonControl;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.IListSelector;
import org.prelle.shadowrun6.AdeptPower;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ChoiceType;
import org.prelle.shadowrun6.Focus;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Sense;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.Spell.Category;
import org.prelle.shadowrun6.Spirit;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.charctrl.FocusController;
import org.prelle.shadowrun6.chargen.jfx.listcells.FocusListCell;
import org.prelle.shadowrun6.items.ItemSubType;
import org.prelle.shadowrun6.items.ItemTemplate;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;
import org.prelle.shadowrun6.jfx.items.ItemTemplateSelector;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionModel;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class FocusSelector extends VBox implements IListSelector<Focus> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(FociSection.class.getName());

	private CharacterController charGen;
	private FocusController control;
	private ScreenManagerProvider provider;

	private Label curKarma, curNuyen;
	private Label costKarma, costNuyen;
	private ListView<Focus> lvItems;

	private ChoiceBox<Integer> cbRating;
	private Button btnChoice;
	private Label lblChoice;
	private ChoiceType choicesSelectFrom;
	private Object choicesChoice;

	private NavigButtonControl btnControl;

	//-------------------------------------------------------------------
	public FocusSelector(CharacterController charGen, ScreenManagerProvider provider) {
		this.charGen = charGen;
		this.control = charGen.getFocusController();
		this.provider= provider;

		initComponents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		curNuyen = new Label(charGen.getCharacter().getNuyen()+"\u00A5");
		curNuyen.setStyle("-fx-text-fill: darkred; -fx-font-size: 120%"); 
		curKarma = new Label(charGen.getCharacter().getKarmaFree()+"");
		curKarma.setStyle("-fx-text-fill: darkred; -fx-font-size: 120%"); 
		
		lvItems = new ListView<Focus>();
		lvItems.setCellFactory( (lv) -> new FocusListCell(control) );
		lvItems.getItems().addAll(ShadowrunCore.getFoci());

		cbRating = new ChoiceBox<>();
		cbRating.getItems().addAll(1,2,3,4,5,6,7,8,9,10,11,12);
		btnChoice = new Button(Resource.get(UI,"button.select"));
		lblChoice = new Label();
		lblChoice.setStyle("-fx-font-size: 150%");

		costNuyen = new Label();
		costKarma = new Label();
		costNuyen.setStyle("-fx-text-fill: darkred;"); 
		costKarma.setStyle("-fx-text-fill: darkred;");
		costNuyen.getStyleClass().add("base");
		costKarma.getStyleClass().add("base");

		btnControl = new NavigButtonControl();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setSpacing(20);
		Label hdNuyen = new Label(Resource.get(UI, "label.nuyen"));
		Label hdKarma = new Label(Resource.get(UI, "label.karma"));
		HBox bxFirst = new HBox(5, hdKarma, curKarma, hdNuyen, curNuyen);
		HBox.setMargin(hdNuyen, new Insets(0,0,0,20));
		
		HBox inner = new HBox(20);
		lvItems.setStyle("-fx-pref-width: 25em");
		inner.getChildren().add(lvItems);

		Label hdRating = new Label(Resource.get(UI, "label.rating"));
		hdRating.getStyleClass().add("base");
		Label hdChoice = new Label(Resource.get(UI, "label.choice"));
		hdChoice.getStyleClass().add("base");
		HBox bxChoice = new HBox(10, lblChoice, btnChoice);
		// Pay line
		Label payNuyen = new Label(Resource.get(UI, "label.pay.nuyen"));
		Label payKarma = new Label(Resource.get(UI, "label.pay.karma"));
		payNuyen.getStyleClass().add("base");
		payKarma.getStyleClass().add("base");
		
		GridPane grid = new GridPane();
		grid.setStyle("-fx-vgap: 1em; -fx-hgap: 0.5em");
		grid.add(hdRating, 0, 0);
		grid.add(cbRating, 1, 0);
		grid.add(hdChoice, 0, 1);
		grid.add(bxChoice, 1, 1);
		
		grid.add(payKarma, 0, 3);
		grid.add(costKarma, 1, 3);
		grid.add(payNuyen, 0, 4);
		grid.add(costNuyen, 1, 4);
		inner.getChildren().add(grid);
		
		getChildren().addAll(bxFirst, inner);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		lvItems.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			cbRating.setDisable(n==null);
			btnChoice.setDisable(n==null);
			logger.info("selected "+n);
			cbRating.getSelectionModel().select(1);
			if (n!=null) {
				lblChoice.setText("?");
				btnChoice.setDisable(!n.needsChoice());
				lblChoice.setDisable(!n.needsChoice());
			} 
			refresh();
		});
		cbRating.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> refresh());

		btnChoice.setOnAction(ev -> {
			Focus focus = lvItems.getSelectionModel().getSelectedItem();
			switch (focus.getChoice()) {
			case MELEE_WEAPON:
				/*
				 * Ask user to select a melee weapon
				 */
				ItemTemplateSelector selector = new ItemTemplateSelector(
						new ItemType[] {ItemType.WEAPON},
						new ItemSubType[] {ItemSubType.CLOSE_COMBAT},
						charGen);
				SelectorWithHelp<ItemTemplate> pane = new SelectorWithHelp<ItemTemplate>(selector);
				ManagedDialog dialog = new ManagedDialog(Resource.get(UI,"selectiondialog.title"), pane, CloseType.OK, CloseType.CANCEL);
				
				CloseType close = (CloseType) provider.getScreenManager().showAndWait(dialog);
				logger.debug("Closed with "+close);
				if (close==CloseType.OK) {
					ItemTemplate item = selector.getSelectedItem();
					logger.debug("User selected "+item+" for WEAPON");
					lblChoice.setText(item.getName());
					lblChoice.setUserData(item);
					refresh();
				}
				break;
			case SPELL_CATEGORY:
				/*
				 * Ask user to select a spell category
				 */
				ChoiceBox<Spell.Category> cbCategory = new ChoiceBox<>();
				cbCategory.setConverter(new StringConverter<Spell.Category>() {
					public String toString(Category value) { return value.getName(); }
					public Category fromString(String string) { return null;}
					});
				cbCategory.getItems().addAll(Spell.Category.values());
				cbCategory.getSelectionModel().select(0);
				Label hdCat = new Label(Resource.get(UI, "label.category"));
				hdCat.getStyleClass().add("base");
				HBox lineCat = new HBox(10, hdCat, cbCategory);
				close = provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, 
						Resource.get(UI, "selectiondialog.category.title"), lineCat);
				logger.debug("Closed with "+close);
				if (close==CloseType.OK && cbCategory.getValue()!=null) {
					Spell.Category cat = cbCategory.getValue();
					logger.debug("User selected "+cat+" for SPELL_CATEGORY");
					lblChoice.setText(cat.getName());
					lblChoice.setUserData(cat);
					refresh();
				}
				break;
			case SPIRIT:
				/*
				 * Ask user to select a spirit category
				 */
				ChoiceBox<Spirit> cbSpirit = new ChoiceBox<>();
				cbSpirit.setConverter(new StringConverter<Spirit>() {
					public String toString(Spirit value) { return value.getName(); }
					public Spirit fromString(String string) { return null;}
					});
				cbSpirit.getItems().addAll(ShadowrunCore.getSpirits());
				cbSpirit.getSelectionModel().select(0);
				hdCat = new Label(Resource.get(UI, "label.spirittype"));
				hdCat.getStyleClass().add("base");
				lineCat = new HBox(10, hdCat, cbSpirit);
				close = provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, 
						Resource.get(UI, "selectiondialog.spirit.title"), lineCat);
				logger.debug("Closed with "+close);
				if (close==CloseType.OK && cbSpirit.getValue()!=null) {
					Spirit cat = cbSpirit.getValue();
					logger.debug("User selected "+cat+" for SPIRIT");
					lblChoice.setText(cat.getName());
					lblChoice.setUserData(cat);
					refresh();
				}
				break;
			case ADEPT_POWER:
				/*
				 * Ask user to select an adept power
				 */
				ChoiceBox<AdeptPower> cbPower = new ChoiceBox<>();
				cbPower.setConverter(new StringConverter<AdeptPower>() {
					public String toString(AdeptPower value) {
						if (value.hasLevels()) {
							int rtg = cbRating.getValue() / (int)(value.getCost()*4);
							return value.getName()+" "+rtg;
						} else
							return value.getName();
					}
					public AdeptPower fromString(String string) { return null;}
					});
				ChoiceBox<Object> cbChoice = new ChoiceBox<>();
				cbChoice.setConverter(new StringConverter<Object>() {
					public String toString(Object value) {
						if (value instanceof Attribute) return ((Attribute)value).getName();
						if (value instanceof Skill) return ((Skill)value).getName();
						if (value instanceof Sense) return ((Sense)value).getName();
						return String.valueOf(value);
					}
					public Object fromString(String string) { return null;}
					});
				cbPower.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
					cbChoice.getItems().clear();
					if (n!=null && n.needsChoice()) {
						cbChoice.setVisible(true);
						choicesSelectFrom = n.getSelectFrom();
						cbChoice.getItems().addAll(ShadowrunTools.getChoices(n.getSelectFrom()));
					} else
						cbChoice.setVisible(false);
				});
				for (AdeptPower power : ShadowrunCore.getAdeptPowers()) {
					int minRating = (int)(power.getCost()*4);
					int effectivePowerRating = cbRating.getValue()/minRating;
					int rest = cbRating.getValue()%minRating;
//					logger.debug(String.format("%20s  cost=%f  min=%d  eff=%d  rest=%d  hasLvl=%s max=%d", power.getId(), power.getCost(), minRating, effectivePowerRating, rest, power.hasLevels(), power.getMaxLevel()));
					// Check Minimum rating
					if (cbRating.getValue()<minRating)
						continue;
					if (power.hasLevels()) {
						if (power.getMaxLevel()>0 && effectivePowerRating>power.getMaxLevel())
							continue;
						// 	If rating cannot be diveded without rest by base rating (cost*0.4), ignore it 
						if ( (cbRating.getValue()>=minRating) && rest==0 ) {
							cbPower.getItems().add(power);
						}
					} else {
						cbPower.getItems().add(power);
					}
				}
				Collections.sort(cbPower.getItems());
				cbPower.getSelectionModel().select(0);
				Label hdPow = new Label(Resource.get(UI, "label.adeptpower"));
				hdPow.getStyleClass().add("base");
				HBox linePow = new HBox(10, hdPow, cbPower, cbChoice);
				close = provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, 
						Resource.get(UI, "selectiondialog.adeptpower.title"), linePow);
				logger.debug("Closed with "+close);
				if (close==CloseType.OK && cbPower.getValue()!=null) {
					AdeptPower pow = cbPower.getValue();
					choicesChoice = cbChoice.getValue();
					
					logger.debug("User selected "+pow+" for Adept Power");
					lblChoice.setText(pow.getName());
					lblChoice.setUserData(pow);
					refresh();
				}
				break;
			default:
				logger.warn("TODO: choose "+focus.getChoice());
			}
		});
	}

	//--------------------------------------------------------------------
	public Focus getSelected() {
		return lvItems.getSelectionModel().getSelectedItem();
	}

	//--------------------------------------------------------------------
	public int getRating() {
		Integer val = cbRating.getValue();
		if (val==null)
			return 0;
		return val;
	}

	//--------------------------------------------------------------------
	public Object getChoice() {
		return lblChoice.getUserData();
	}

	//--------------------------------------------------------------------
	public ChoiceType getChoicesSelect() {
		return choicesSelectFrom;
	}

	//--------------------------------------------------------------------
	public Object getChoicesChoice() {
		return choicesChoice;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getSelectionModel()
	 */
	@Override
	public SelectionModel<Focus> getSelectionModel() {
		return lvItems.getSelectionModel();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.IListSelector#getNode()
	 */
	@Override
	public Node getNode() {
		return this;
	}

	//--------------------------------------------------------------------
	public NavigButtonControl getButtonControl() {
		return btnControl;
	}

	//--------------------------------------------------------------------
	private void refresh() {
		logger.debug("refresh");
		// Ensure focus selection
		Focus focus = lvItems.getSelectionModel().getSelectedItem(); 
		if (focus==null) {
			btnControl.setDisabled(CloseType.OK, true);
			costKarma.setText(null);
			costNuyen.setText(null);
			return;
		}
		// Ensure rating selection
		Integer rating = cbRating.getValue();
		if (rating==null) {
			btnControl.setDisabled(CloseType.OK, true);
			costKarma.setText(null);
			costNuyen.setText(null);
			System.err.println("Refresh: No rating");
			return;
		}
		
		int myKarma = rating*focus.getBondingMultiplier();
		int myNuyen = rating*focus.getNuyenCost();
		costKarma.setText(String.valueOf(myKarma));
		costNuyen.setText(String.valueOf(myNuyen));
		// Is eventually necessary choice made?
		if (focus.needsChoice()) {
			Object choice = lblChoice.getUserData();
			btnControl.setDisabled(CloseType.OK, !control.canBeSelected(focus, rating, choice));
			logger.info("Set OK to "+control.canBeSelected(focus, rating, choice)+" for "+focus.getId());
		} else {
			btnControl.setDisabled(CloseType.OK, !control.canBeSelected(focus, rating));
			logger.info("Set OK to "+control.canBeSelected(focus, rating)+" for "+focus.getId());
		}
	}

}

package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.javafx.skin.GridPaneTableViewSkin;
import org.prelle.rpgframework.jfx.NumericalValueTableCell;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.panels.SkillSpecializationSelectionPane;
import org.prelle.shadowrun6.gen.event.GenerationEventDispatcher;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.jfx.fluent.SelectorWithHelp;

import de.rpgframework.ConfigOption;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan Prelle
 *
 */
public class SkillSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private CharacterController ctrl;

	private SkillTable table1;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	/**
	 */
	public SkillSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		this.ctrl = ctrl;

		table1 = new SkillTable(ctrl, provider);

		setDeleteButton( new Button(null, new SymbolIcon("delete")) );
		setAddButton( new Button(null, new SymbolIcon("add")) );
		setSettingsButton( new Button(null, new SymbolIcon("setting")) );

		layoutOneColumn();
		initInteractivity();

		table1.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {if (n!=null) showHelpFor.set(n.getModifyable()); });
	}

	//-------------------------------------------------------------------
	private void layoutOneColumn() {
		refresh();
		setContent(table1);
	}

	//--------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		getSettingsButton().setOnAction(ev -> onSettings());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		table1.getItems().clear();
		List<SkillValue> toShow = new ArrayList<>();
		for (Skill skill : ShadowrunCore.getSkills()) {
			SkillValue val = ctrl.getCharacter().getSkillValue(skill);
			if (val!=null  && val.getModifyable().getType()!=SkillType.KNOWLEDGE && val.getModifyable().getType()!=SkillType.LANGUAGE)
				toShow.add(val);
		}
		Collections.sort(toShow, new Comparator<SkillValue>() {
			public int compare(SkillValue o1, SkillValue o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		table1.getItems().addAll(toShow);
		
		getAddButton().setDisable(ctrl.getCharacter().getKarmaFree()<5);
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening skill selection dialog");
		
		SkillSelector selector = new SkillSelector(ctrl.getSkillController(), SkillType.regularValues());
		selector.setPlaceholder(new Label(Resource.get(RES, "skillsection.selector.placeholder")));
		SelectorWithHelp<Skill> pane = new SelectorWithHelp<>(selector);
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "skillsection.selector.title"), pane, CloseType.OK, CloseType.CANCEL);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			Skill data = selector.getSelectionModel().selectedItemProperty().get();
			logger.debug("Selected skill: "+data);
			if (data!=null) {
				ctrl.getSkillController().select(data);
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		SkillValue selected = table1.getSelectionModel().getSelectedItem();
		logger.debug("Skill to deselect: "+selected);
		ctrl.getSkillController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	private void onSettings() {
		
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : ctrl.getSkillController().getConfigOptions()) {
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
	}

}

class SkillTable extends TableView<SkillValue> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);

	private CharacterController ctrl;
	private ScreenManagerProvider provider;

	private TableColumn<SkillValue, Boolean> recCol;
	private TableColumn<SkillValue, String> nameCol;
	private TableColumn<SkillValue, String> attCol;
	private TableColumn<SkillValue, SkillValue> valCol;
	private TableColumn<SkillValue, Number> ratCol;
	private TableColumn<SkillValue, List<SkillSpecializationValue>> specCol;

	//--------------------------------------------------------------------
	public SkillTable(CharacterController ctrl, ScreenManagerProvider provider) {
		if (ctrl==null)
			throw new NullPointerException();
		this.ctrl = ctrl;
		this.provider = provider;
		setSkin(new GridPaneTableViewSkin<>(this));
		initColumns();
		initValueFactories();
		initCellFactories();
		initLayout();
	}

	//--------------------------------------------------------------------
	private void initColumns() {
		recCol = new TableColumn<>();
		nameCol = new TableColumn<>(RES.getString("column.name"));
		valCol  = new TableColumn<>(RES.getString("column.value"));
		attCol  = new TableColumn<>(RES.getString("column.attribute"));
		ratCol  = new TableColumn<>(RES.getString("column.pool"));
		specCol = new TableColumn<>(RES.getString("column.special"));

		recCol .setId("attrtable-rec");
		nameCol.setId("attrtable-name");
		attCol .setId("attrtable-attr");
		valCol .setId("attrtable-val");
		ratCol .setId("attrtable-sum");

		attCol.setStyle("-fx-alignment: center; -fx-text-alignment: center");
		valCol.setStyle("-fx-alignment: center; -fx-text-alignment: center");
		ratCol.setStyle("-fx-alignment: center; -fx-text-alignment: center");

		recCol.setPrefWidth(40);
		nameCol.setMinWidth(100);
		nameCol.setPrefWidth(140);
		attCol.setMinWidth(40);
		valCol.setPrefWidth(110);
		ratCol.setMinWidth(30);
		specCol.setMinWidth(200);
		specCol.setPrefWidth(400);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initLayout() {
		getColumns().addAll(recCol, nameCol, attCol, valCol, ratCol, specCol);
	}

	//--------------------------------------------------------------------
	private void initValueFactories() {
		//		recCol.setCellValueFactory(cdf -> new SimpleBooleanProperty(ctrl.getSkillController().isConceptSkill(cdf.getValue().getModifyable())));
		nameCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getName()));
		attCol.setCellValueFactory(cdf -> new SimpleStringProperty(cdf.getValue().getModifyable().getAttribute1().getShortName()));
		valCol .setCellValueFactory(cdf -> new SimpleObjectProperty<SkillValue>(cdf.getValue()));
		ratCol .setCellValueFactory(cdf -> new SimpleIntegerProperty(ShadowrunTools.getSkillPool(ctrl.getCharacter(), cdf.getValue().getModifyable())));
		specCol.setCellValueFactory(cdf -> new SimpleObjectProperty<List<SkillSpecializationValue>>(cdf.getValue().getSkillSpecializations()));
	}

	//--------------------------------------------------------------------
	private void initCellFactories() {
		valCol.setCellFactory( (col) -> new NumericalValueTableCell<Skill,SkillValue,SkillValue>(ctrl.getSkillController()));
		recCol.setCellFactory(col -> new TableCell<SkillValue,Boolean>(){
			public void updateItem(Boolean item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					if (item) {
						Label lb = new Label("\uE735");
						lb.setStyle("-fx-text-fill: recommendation; -fx-font-family: 'Segoe MDL2 Assets';");
						setGraphic(lb);
					} else
						setGraphic(null);
				}
			}
		});
		ratCol.setCellFactory( col -> new TableCell<SkillValue,Number>() {
			public void updateItem(Number item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					Label lb = new Label(String.valueOf(item));
					SkillValue foo = this.getTableRow().getItem();
					Tooltip tt = new Tooltip(ShadowrunTools.getSkillPoolExplanation(ctrl.getCharacter(), foo.getModifyable()));
//					Tooltip tt = new Tooltip(getTableRow().getItem().getVolatileModificationString());
					lb.setTooltip(tt);
					if (foo==null || foo.getModifier()==0) {
						lb.setStyle("-fx-alignment: center; -fx-text-alignment: center;");
					} else {
						lb.setStyle("-fx-alignment: center; -fx-text-alignment: center; -fx-font-weight: bold;");
					}
					setGraphic(lb);
				}
			}
		});
		specCol.setCellFactory(col -> new TableCell<SkillValue,List<SkillSpecializationValue>>(){
			public void updateItem(List<SkillSpecializationValue> item, boolean empty) {
				super.updateItem(item, empty);
				if (empty) {
					setGraphic(null);
				} else {
					Button btnEdit = new Button(null, new SymbolIcon("edit"));
					btnEdit.setOnAction(ev -> editSpecializations(getTableRow().getItem()));
					List<String> elems = new ArrayList<>();
					for (SkillSpecializationValue val : item) {
						if (val.isExpertise()) {
							elems.add(Resource.format(RES, "format.expertise", val.getSpecial().getName()));
						} else {
							elems.add(Resource.format(RES, "format.special", val.getSpecial().getName()));
						}
					}
						Label lb = new Label(String.join(", ", elems));
						lb.setMaxWidth(Double.MAX_VALUE);
//						lb.setStyle("-fx-text-fill: recommendation; -fx-font-family: 'Segoe MDL2 Assets';");
						setGraphic(null);
					HBox line = new HBox(10, btnEdit, lb);
					HBox.setHgrow(lb, Priority.ALWAYS);
					setGraphic(line);
				}
			}
		});
	}

	//-------------------------------------------------------------------
	private void editSpecializations(SkillValue value) {
		logger.debug("edit specializations of "+value);
		SkillSpecializationSelectionPane pane = new SkillSpecializationSelectionPane(ctrl, value);
		GenerationEventDispatcher.addListener(pane);
		provider.getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.format(RES, "dialog.skillspec.title", value.getModifyable().getName()), pane);
		GenerationEventDispatcher.removeListener(pane);
		logger.debug("Done editing specializations");
	}
}
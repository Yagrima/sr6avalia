package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Tradition;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class TraditionSection extends SingleSection {

	private static PropertyResourceBundle RES = SR6Constants.RES;

	private CharacterController ctrl;
	
	private ChoiceBox<Tradition> cbTradition;
	private Label lbAttr1;
	private Label lbAttr2;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();
	
	//-------------------------------------------------------------------
	public TraditionSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(), null);
		this.ctrl = ctrl;
		initComponents();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		cbTradition = new ChoiceBox<Tradition>();
		cbTradition.setConverter(new StringConverter<Tradition>() {
			public String toString(Tradition val) { return val.getName(); }
			public Tradition fromString(String val) { return null;}
		});
		cbTradition.getItems().addAll(ShadowrunCore.getTraditions());
		
		lbAttr1 = new Label("?");
		lbAttr2 = new Label("?");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		Label hdDrain = new Label(RES.getString("label.drain"));
		hdDrain.getStyleClass().add("base");
		Label plus = new Label("+");
		
		HBox layout = new HBox(5);
		layout.setAlignment(Pos.CENTER_LEFT);
		layout.getChildren().addAll(cbTradition, hdDrain, lbAttr1, plus, lbAttr2);
		HBox.setMargin(hdDrain, new Insets(0, 0, 0, 20));
		layout.setStyle("-fx-padding: 0.2em;");
		
		setContent(layout);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		cbTradition.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			ctrl.getCharacter().setTradition(n);
			showHelpFor.set(n);
			if (n!=null) {
				lbAttr1.setText(n.getDrainAttribute1().getName());
				lbAttr2.setText(n.getDrainAttribute2().getName());
			} else {
				lbAttr1.setText("?");
				lbAttr2.setText("?");
			}
		});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		cbTradition.getSelectionModel().select(ctrl.getCharacter().getTradition());;
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

}

package org.prelle.shadowrun6.chargen.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedDialog;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.shadowrun6.BasePluginData;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Spell;
import org.prelle.shadowrun6.SpellValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.listcells.SpellValueListCell;
import org.prelle.shadowrun6.jfx.SR6Constants;
import org.prelle.shadowrun6.levelling.CharacterLeveller;

import de.rpgframework.ConfigOption;
import de.rpgframework.genericrpg.ToDoElement;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class SpellSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME);
	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SpellSection.class.getName());

	private CharacterController ctrl;
	private boolean alchemistic;
	private ListView<SpellValue> listN;
	private Button btnAdd;
	private Button btnDel;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public SpellSection(String title, CharacterController ctrl, boolean alchemistic, ScreenManagerProvider provider) {
		super(provider, title.toUpperCase(),  new ListView<SpellValue>());
		if (provider==null)
			throw new NullPointerException("Missing ScreenManagerProvider");
		this.alchemistic = alchemistic;
		this.ctrl = ctrl;
		listN = (ListView<SpellValue>)getContent();
		initComponents();
		initInteractivity();
		
		refresh();
	}
	
	//-------------------------------------------------------------------
	private void initComponents() {
		todos = FXCollections.observableArrayList();
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));
		setDeleteButton(btnDel);
		setAddButton(btnAdd);
		btnDel.setDisable(true);
		listN.setCellFactory(new Callback<ListView<SpellValue>, ListCell<SpellValue>>() {
			public ListCell<SpellValue> call(ListView<SpellValue> param) {
				SpellValueListCell cell =  new SpellValueListCell(ctrl.getSpellController());
				cell.setOnMouseClicked(event -> {
					if (event.getClickCount()==2) ctrl.getSpellController().deselect(cell.getItem());
				});
				return cell;
			}
		});
		if (ctrl instanceof CharacterLeveller)
			setSettingsButton( new Button(null, new SymbolIcon("setting")) );
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		getAddButton().setOnAction(ev -> onAdd());
		getDeleteButton().setOnAction(ev -> onDelete());
		if (getSettingsButton()!=null)
			getSettingsButton().setOnAction(ev -> onSettings());
		
		listN.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			logger.info(n+" canBeDeselected = "+ctrl.getSpellController().canBeDeselected(n));
			btnDel.setDisable(n==null || !ctrl.getSpellController().canBeDeselected(n));
			if (n!=null)
				showHelpFor.set(n.getModifyable());
		});
	}

	//-------------------------------------------------------------------
	private void onAdd() {
		logger.debug("opening spell selection dialog");
		
		SpellSelector pane = new SpellSelector(ctrl.getSpellController(), alchemistic);
		ManagedDialog dialog = new ManagedDialog(Resource.get(RES, "spellsection.selector.title"), pane, CloseType.OK);
		
		CloseType close = (CloseType) getManagerProvider().getScreenManager().showAndWait(dialog);
		logger.info("Closed with "+close);
		if (close==CloseType.OK) {
			Spell selected = pane.getSelected();
			logger.debug("Selected spell: "+selected);
			if (selected!=null) {
				ctrl.getSpellController().select(selected, alchemistic);
				refresh();
			}
		}
	}

	//-------------------------------------------------------------------
	private void onDelete() {
		SpellValue selected = listN.getSelectionModel().getSelectedItem();
		logger.debug("Spell to deselect: "+selected);
		ctrl.getSpellController().deselect(selected);
		refresh();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void onSettings() {
		VBox content = new VBox(20);
		for (ConfigOption<?> opt : ctrl.getSpellController().getConfigOptions()) {
			CheckBox cb = new CheckBox(opt.getName());
			cb.setSelected((Boolean)opt.getValue());
			cb.selectedProperty().addListener( (ov,o,n) -> ((ConfigOption<Boolean>)opt).set((Boolean)n));
			content.getChildren().add(cb);
		}
		
		getManagerProvider().getScreenManager().showAlertAndCall(AlertType.NOTIFICATION, Resource.get(RES,  "dialog.settings.title"), content);
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

	//-------------------------------------------------------------------
	public MultipleSelectionModel<SpellValue> getSelectionModel() {
		return listN.getSelectionModel();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.trace("refresh");
		todos.clear();
		for (ToDoElement mess : ctrl.getSpellController().getToDos()) {
			todos.add(mess);
		}
		
		listN.getItems().clear();
		listN.getItems().addAll(ctrl.getCharacter().getSpells(alchemistic));
	}

}

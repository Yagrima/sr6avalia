package org.prelle.shadowrun6.chargen.jfx.listcells;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.shadowrun6.Resource;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.Skill.SkillType;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.charctrl.CharacterController;
import org.prelle.shadowrun6.chargen.jfx.sections.SkillSection;
import org.prelle.shadowrun6.jfx.SR6Constants;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class KnowledgeSkillValueListCell extends ListCell<SkillValue> {

	private final static Logger logger = LogManager.getLogger(SR6Constants.BASE_LOGGER_NAME+".skills");

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(SkillSection.class.getName());

	private CharacterController charGen;
	private ScreenManagerProvider provider;

	private Button btnEdit;
	private Label lbName;
	private Label lbType;
	private Button btnDec;
	private Label  lbLevel;
	private Button btnInc;
	private HBox bxLevel;
	private VBox layout;
	
	//-------------------------------------------------------------------
	public KnowledgeSkillValueListCell(CharacterController charGen, ScreenManagerProvider provider) {
		this.charGen  = charGen;
		this.provider = provider;

		lbName  = new Label();
		lbType = new Label();
		lbLevel = new Label();
		btnEdit = new Button("\uE1C2");
		btnEdit.getStyleClass().add("mini-button");
		btnDec  = new Button("\uE0C6");
		btnDec.getStyleClass().add("mini-button");
		btnInc  = new Button("\uE0C5");
		btnInc.getStyleClass().add("mini-button");

		initStyle();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		lbName.getStyleClass().add("base");
		lbType.setStyle("-fx-font-style: italic;");
		btnEdit.setStyle("-fx-background-color: transparent;");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		HBox line1 = new HBox(10);
		line1.getChildren().addAll(btnEdit, lbName, lbType);
		
		bxLevel = new HBox(5);
		bxLevel.getChildren().addAll(btnDec, lbLevel, btnInc);

		layout = new VBox(2);
		layout.getChildren().addAll(line1, bxLevel);
		layout.setStyle("-fx-min-width: 13em; -fx-pref-width: 16em");

		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnEdit.setOnAction(event -> {
			editClicked(super.getItem());
		});
		btnDec.setOnAction(event -> {
			logger.debug("Try decrease "+getItem());
			charGen.getSkillController().decrease(getItem());
		});
		btnInc.setOnAction(event -> {
			logger.debug("Try increase "+getItem());
			charGen.getSkillController().increase(getItem());
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(SkillValue item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			setGraphic(layout);
			
			lbName.setText(item.getName());
			btnDec.setDisable(!charGen.getSkillController().canBeDecreased(item));
			btnInc.setDisable(!charGen.getSkillController().canBeIncreased(item));
			switch (item.getPoints()) {
			case 1: lbLevel.setText(Resource.get(RES, "language.level.1")); break;
			case 2: lbLevel.setText(Resource.get(RES, "language.level.2")); break;
			case 3: lbLevel.setText(Resource.get(RES, "language.level.3")); break;
			}				
			Skill skill = item.getModifyable();
			if (skill.getType()==SkillType.KNOWLEDGE) {
				bxLevel.setVisible(false);
				bxLevel.setManaged(false);
				lbType.setText("("+skill.getName()+")");
			} else if (skill.getType()==SkillType.LANGUAGE) {
				bxLevel.setVisible(true);
				bxLevel.setManaged(true);
				lbType.setText("("+skill.getName()+")");
				if (item.getPoints()==SkillValue.LANGLEVEL_NATIVE) {
					bxLevel.setVisible(false);
					bxLevel.setManaged(false);
					lbType.setText("("+Resource.get(RES, "skill.native_language")+")");
				} 
			}
		}
	}

	//-------------------------------------------------------------------
	private void editClicked(SkillValue data) {
		logger.debug("editClicked");

		Label explain = new Label(Resource.get(RES, "namedialog.explain"));
		
		TextField tfName = new TextField();
		tfName.setPrefColumnCount(20);
		VBox layout = new VBox(20, explain, tfName);
		
		CloseType closed = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, Resource.get(RES, "namedialog.title"), layout);
		if (closed==CloseType.OK) {
			logger.info("Set custom name of skill to "+tfName.getText());
			data.setName(tfName.getText());
			updateItem(data, false);
		}
		
//		SkillSpecializationPane dia = new SkillSpecializationPane(charGen, data);
//
//		provider.getScreenManager().showAlertAndCall(
//				AlertType.NOTIFICATION,
//				UI.getString("skillselectpane.specialization.dialog.header"),
//				dia);
//		GenerationEventDispatcher.removeListener(dia);
//		updateItem(data, false);
	}

}